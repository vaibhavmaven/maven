<apex:page standardController="Case" extensions="InteractionLogControllerMVN" showHeader="false" sidebar="false" standardStylesheets="false">
    <apex:includeScript value="{!URLFOR($Resource.GlobalAssetsMVN, 'js/jquery-2.2.2.min.js')}"></apex:includeScript>
    <apex:includeScript value="{!URLFOR($Resource.GlobalAssetsMVN, 'js/bootstrap.min.js')}"></apex:includeScript>
    <apex:includeScript value="/support/console/37.0/integration.js"/>
    
    <apex:stylesheet value="{!URLFOR($Resource.GlobalAssetsMVN, 'css/bootstrap.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.InteractionLogCSSMVN)}"/>

    <div id="container">
        
            <apex:outputPanel id="scripts">
                <script>
                    var MAX_TEXTAREA_HEIGHT = 250;

                    var parentTabId;
                    var subTabId;
                    var thisWindow;

                    window.onload = getTabIds;

                    function getTabIds() {
                        sforce.console.getEnclosingPrimaryTabId(assignParentTabId);
                        sforce.console.getEnclosingTabId(assignSubTabId);
                    }

                    var assignParentTabId = function assignTabId(result) {
                        parentTabId = result.id;
                        thisWindow = window;
                    };

                    var assignSubTabId = function assignSubTabId(result) {
                        subTabId = result.id;
                    };

                    function openChildRequest(){
                        var newEventId = '{!newEvent.Id}';
                        sforce.console.openSubtab(parentTabId, '/'+newEventId, true);
                        refreshPrimaryTab();
                    }

                    function refreshPrimaryTab(){
                        sforce.console.refreshPrimaryTabById(parentTabId, false);
                    }

                    function markChanged() {
                        sforce.console.getFocusedSubtabId(setUnsaved);
                        $("[id$='notesWrapper']").attr('class', 'form-group has-warning has-feedback');
                        $("[id='notesFeedbackIcon']").attr('class', 'glyphicon glyphicon-warning-sign form-control-feedback');
                    }
                    
                    function markSaving(){
                        $("[id='notesFeedbackIcon']").attr('class', 'glyphicon glyphicon-hourglass form-control-feedback');
                        $('#close-button').prop('disabled', true)
                    }

                    var setUnsaved = function setUnsaved(result){
                        sforce.console.setTabUnsavedChanges(true, displayResult,result.id);
                    }

                    function markSaved() {
                        sforce.console.getFocusedSubtabId(setSaved);
                        $("[id$='notesWrapper']").attr('class', 'form-group has-success has-feedback');
                        $("[id='notesFeedbackIcon']").attr('class', 'glyphicon glyphicon-ok form-control-feedback');
                        initTextAreaResize();
                        $('#close-button').prop('disabled', saveAndCloseNoteState || saveNoteState)
                    }

                    function onInteractedWithChange() {
                        markSaving();
                        saveNoteState = true;
                        saveNote()
                    }

                    var setSaved = function setSaved(result){
                        sforce.console.setTabUnsavedChanges(false, displayResult,result.id);
                    }

                    var displayResult = function displayResult(result){
                    }

                    function initTextAreaResize() {
                        var observe;
                        if (window.attachEvent) {
                            observe = function (element, event, handler) {
                                element.attachEvent('on'+event, handler);
                            };
                        }
                        else {
                            observe = function (element, event, handler) {
                                element.addEventListener(event, handler, false);
                            };
                        }

                        var text = document.getElementById($("[id$='notesBox']").attr('id'));
                        function resize () {
                            text.style.height = 'auto';

                            if(text.scrollHeight < MAX_TEXTAREA_HEIGHT)
                                text.style.height = text.scrollHeight+'px';
                            else
                                text.style.height = String(MAX_TEXTAREA_HEIGHT) + 'px';
                        }
                        /* 0-timeout to get the already changed text */
                        function delayedResize () {
                            window.setTimeout(resize, 0);
                        }
                        observe(text, 'change',  resize);
                        observe(text, 'cut',     delayedResize);
                        observe(text, 'paste',   delayedResize);
                        observe(text, 'drop',    delayedResize);
                        observe(text, 'keydown', delayedResize);

                        resize();
                    }

                    var saveNoteState = false;
                    var saveAndCloseNoteState = false;

                    $(document).ready(function () {
                        initTextAreaResize();

                        $(document).on('click', function(event) {
                            var text = $(event.target).text();
                            if($("div[id$='notesWrapper']").attr('class') == 'form-group has-warning has-feedback' && text != ' Close Note'){
                                if(saveNoteState || saveAndCloseNoteState) return;
                                markSaving();
                                saveNoteState = true;
                                saveNote();
                            } else if(text == ' Close Note'){
                                if(saveNoteState || saveAndCloseNoteState) return;
                                markSaving();
                                saveAndCloseNoteState = true;
                                saveAndCloseNote();
                            }

                            // $("div[id$='textArea']").mouseleave(function(event) {
                            //     markSaving();
                            //     mouseLeaveState = true;
                            //     saveNote();
                            // });

                            // $(window).on('blur', function(event) {
                            //     markSaving();
                            //     blurState = true;
                            //     saveNote();
                            // });

                            var timeoutId;
                            $("div[id$='textArea']").on('input propertychange', function() {                                
                                clearTimeout(timeoutId);
                                timeoutId = setTimeout(function() {
                                    if(saveNoteState || saveAndCloseNoteState) return;
                                    markSaving();
                                    saveNoteState = true;
                                    saveNote();
                                }, 1000);
                            });
                        });
                    });
                </script>
            </apex:outputPanel>

            <apex:outputPanel id="errorMessagePanel">
                <apex:outputPanel rendered="{!hasSaveError}">
                    <div class="alert alert-warning">
                        <apex:messages />
                    </div>
                </apex:outputPanel>
            </apex:outputPanel>

            <apex:outputPanel rendered="{!NOT(caseIsLocked)}">
                <div style="clear:both;width:100%">
                    <apex:form id="theForm">
                    <div id="buttonContainer">
                        <nav class="{!IF(hasCaseNoteButtons, 'navbar navbar-default','')}">
                          <div class="container">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            
                            <apex:outputPanel rendered="{!hasCaseNoteButtons}" styleClass="navbar-header" layout="block">
                                <button type="button" id="nav-bar-toggle-button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false" aria-controls="navbar-collapse-1">
                                </button>
                            
                                <span class="navbar-brand" >{!$Label.Interaction_Log_Header_Label}</span>
                            </apex:outputPanel>
                            
                            <apex:outputPanel rendered="{!hasCaseNoteButtons}">
                                <div class="collapse navbar-collapse" id="navbar-collapse-1" data-parent="#nav-bar-toggle-button">
                                    <apex:actionRegion >
                                        <apex:outputPanel style="padding-top:10px;padding-bottom:10px" layout="block">

                                            <!-- New AE button -->
                                            <apex:outputPanel rendered="{!$Setup.Patient_Service_Settings_MVN__c.Display_AE_Button_MVN__c && $ObjectType.Event_MVN__c.Createable}" layout="inline" styleClass="icon divider">
                                                <apex:actionStatus id="new-ae-status">
                                                    <apex:facet name="stop">
                                                        <apex:commandLink title="{!$Label.New_Adverse_Event_Button}" action="{!createChildCaseFromInteractionLog}" rerender="scripts, errorMessagePanel" oncomplete="openChildRequest();" status="new-ae-status" >
                                                            <apex:param name="aeparam" assignTo="{!requestType}" value="AE" />
                                                            <apex:image value="{!URLFOR($Resource.GlobalAssetsMVN, 'images/adverseEvent.png')}"/>
                                                            <apex:outputText value="{!$Label.New_Adverse_Event_Button}" styleClass="iconLabel"/>
                                                        </apex:commandLink>
                                                    </apex:facet>
                                                    <apex:facet name="start">
                                                        <img src="{!URLFOR($Resource.GlobalAssetsMVN, 'images/loading.gif')}" height="18px" width="18px"/>
                                                    </apex:facet>
                                                </apex:actionStatus>
                                            </apex:outputPanel>

                                            <!-- New PQC button -->
                                            <apex:outputPanel rendered="{!$Setup.Patient_Service_Settings_MVN__c.Display_PQC_Button_MVN__c && $ObjectType.Event_MVN__c.Createable}" layout="inline" styleClass="icon divider">
                                                <apex:actionStatus id="new-pqc-status" >
                                                    <apex:facet name="stop">
                                                        <apex:commandLink title="{!$Label.New_Product_Complaint_Button}" action="{!createChildCaseFromInteractionLog}" rerender="scripts, errorMessagePanel" oncomplete="openChildRequest();" status="new-pqc-status" >
                                                            <apex:param name="pqcparam" assignTo="{!requestType}" value="PQC" />
                                                            <apex:image value="{!URLFOR($Resource.GlobalAssetsMVN, 'images/pqc.png')}"/>
                                                            <apex:outputText value="{!$Label.New_Product_Complaint_Button}" styleClass="iconLabel"/>
                                                        </apex:commandLink>
                                                    </apex:facet>
                                                    <apex:facet name="start">
                                                        <img src="{!URLFOR($Resource.GlobalAssetsMVN, 'images/loading.gif')}" height="18px" width="18px"/>
                                                    </apex:facet>
                                                </apex:actionStatus>
                                            </apex:outputPanel>

                                            <!-- New Missed Dose button -->
                                            <apex:outputPanel rendered="{!$Setup.Patient_Service_Settings_MVN__c.Display_MD_Button_MVN__c && $ObjectType.Event_MVN__c.Createable}" layout="inline" styleClass="icon divider">
                                                <apex:actionStatus id="new-md-status" >
                                                    <apex:facet name="stop">
                                                        <apex:commandLink title="{!$Label.New_Missed_Dose_Button_MVN}" action="{!createChildCaseFromInteractionLog}" rerender="scripts, errorMessagePanel" oncomplete="openChildRequest();" status="new-md-status" >
                                                            <apex:param name="mdparam" assignTo="{!requestType}" value="MD" />
                                                            <apex:image value="{!URLFOR($Resource.GlobalAssetsMVN, 'images/pill.png')}" width="33px"/>
                                                            <apex:outputText value="{!$Label.New_Missed_Dose_Button_MVN}" styleClass="iconLabel"/>
                                                        </apex:commandLink>
                                                    </apex:facet>
                                                    <apex:facet name="start">
                                                        <img src="{!URLFOR($Resource.GlobalAssetsMVN, 'images/loading.gif')}" height="18px" width="18px"/>
                                                    </apex:facet>
                                                </apex:actionStatus>
                                            </apex:outputPanel>
                                        </apex:outputPanel>
                                    </apex:actionRegion>
                                  
                                </div><!-- /.navbar-collapse -->
                            </apex:outputPanel>
                            
                          </div><!-- /.container-fluid -->
                        </nav>
                        
                    </div>

                    <apex:outputPanel layout="block" styleClass="note-text-area">
                        <div id="textArea">
                            <apex:outputPanel rendered="{!NOT(disableInteractionLog)}" styleClass="form-group has-feedback" layout="block" id="notesWrapper">
                                <!-- START: Case Note -->
                                <apex:inputTextArea value="{!currentNote.Notes_MVN__c}" html-maxlength="32768"
                                    styleClass="form-control" id="notesBox" html-aria-describedby="notesFeedback" rows="5"
                                    html-placeholder="{!$ObjectType.Case_Note_MVN__c.fields.Notes_MVN__c.label}"
                                />
                                <!-- END: Case Note -->

                                <span aria-hidden="true" id="notesFeedbackIcon"></span>
                                <span id="notesFeedback" class="sr-only">(warning)</span>
                            </apex:outputPanel>

                            <apex:actionFunction action="{!saveInteractionNotes}" name="saveNote"
                                rerender="lastSavedContainer, lastSave"
                                oncomplete="saveNoteState = false; markSaved()"/>

                            <apex:actionFunction action="{!closeNote}" name="saveAndCloseNote"
                                rerender="lockedNotes, notesWrapper, errorMessagePanel, interactedWithWrapper"
                                oncomplete="saveAndCloseNoteState = false; markSaved()"/>
                            
                            <apex:outputPanel rendered="{!disableInteractionLog}">
                                <div class="form-group has-feedback" style="overflow-y:auto; background-color: whitesmoke; text-shadow: 1px 1px white; color: gray; white-space:pre-wrap; padding:2px; word-break: break-all;">
                                    <apex:outputText value="{!currentNote.Notes_MVN__c}" escape="TRUE" style="height:370px;" styleClass="form-control" />
                                </div>
                            </apex:outputPanel>
                            
                        </div>

                        <apex:outputPanel layout="block" id="lastSavedContainer" style="text-align: center;">
                            <apex:outputText id="saveStatusMessage" value="{!IF(hasSaveError, $Label.Interaction_Log_Unsaved_Status, '')}" styleclass="{!IF(hasSaveError && !isSaving, 'editListError', IF(needsToBeSaved || isSaving, 'inlineEditModified', ''))}" style="font-weight:bold;" />
                            <apex:outputText value="{!$Label.Interaction_Log_Closed_Cases_Locked}" styleclass="errorStyle" rendered="{!IF(caseIsLocked, 'true', 'false')}"/>
                        </apex:outputPanel>

                        <button id="close-button" class="btn btn-primary" type="button"><span class="glyphicon glyphicon-lock"></span> {!$Label.Close_Note_Button}</button>
                    </apex:outputPanel>
                </apex:form>

            </div>
        </apex:outputPanel>

        <!-- START: Header, on Case Locked -->
        <apex:outputPanel rendered="{!caseIsLocked}">
            <nav class="navbar navbar-default">
                  <div class="container">
                    <div class="navbar-header">
                        <span class="navbar-brand" >{!$Label.Interaction_Log_Header_Label}</span>
                    </div>
                </div>
            </nav>
        </apex:outputPanel>
        <!-- END: Header, on Case Locked -->

        <!-- START: List of Notes -->
        <apex:outputPanel id="lockedNotes">
            <div style="padding-top:10px">
                <apex:repeat var="note" value="{!caseNotes}">
                    <c:InteractionLogNoteMVN note="{!note}" rendered="{!note.Locked_MVN__c}"/>
                </apex:repeat>
            </div>
        </apex:outputPanel>
        <!-- END: List of Notes -->

    </div>
</apex:page>