
function makeActive(item) {
    var active = document.getElementsByClassName('slds-active')[0];
    var oldContentId = active.children[0].id.split('__')[0];
    active.className = active.className.replace(' slds-active', '');
    item.parentElement.className += ' slds-active';
    var viewId = item.id.split('__')[0];
    document.getElementById(viewId).className = document.getElementById(viewId).className.replace('slds-hide','slds-show');
    item.style.color = "#fff";
    item.style.backgroundColor = "#00396B";
    document.getElementById(oldContentId).className = document.getElementById(oldContentId).className.replace('slds-show','slds-hide');
    document.getElementById(oldContentId).style.color = "#0070d2";
    document.getElementById(oldContentId).style.backgroundColor = "#f4f6f9";
}

function makeEditable(recordId) {
    var inputs = document.getElementsByName(recordId);
    var i;
    for (i = 0; i < inputs.length; i++) {
        inputs[i].disabled = false;    
    }

    document.getElementById(recordId + '-edit').className = document.getElementById(recordId + '-edit').className.replace('slds-show', 'slds-hide');
    document.getElementById(recordId + '-save').className = document.getElementById(recordId + '-save').className.replace('slds-hide','slds-show');
    document.getElementById(recordId + '-cancel').className = document.getElementById(recordId + '-cancel').className.replace('slds-hide','slds-show');
}

function cancel(recordId) {
    disableInputs(recordId);
    hideSaveButton(recordId);
    
    document.getElementById(recordId + '-cancel').className = document.getElementById(recordId + '-cancel').className.replace('slds-show', 'slds-hide');
    document.getElementById(recordId + '-edit').className = document.getElementById(recordId + '-edit').className.replace('slds-hide','slds-show');
}

function save(recordId, recordTypeId, programmeMemberId) {
    var newRecord = recordId === null;

    var value = {
        'RecordTypeId': recordTypeId,
        'Program_Member_MVN__c': programmeMemberId
    };

    if(recordId) {
        value['Id'] = recordId;
    }

    var inputs = newRecord ? document.getElementsByName(recordTypeId) :document.getElementsByName(recordId);
    var i;
    for (i = 0; i < inputs.length; i++) {
        if(inputs[i].type == 'checkbox') {
            value[inputs[i].id.split('-')[1]] = inputs[i].checked;
        } else {
            var previous = inputs[i].getAttribute('data-prev');
            var newVal = inputs[i].value;
            var change =  newVal !== previous;
            var nullChange = previous === null && newVal === '';

            if(change && !nullChange) {
                value[inputs[i].id.split('-')[1]] = inputs[i].value;    
            } 
        }
        
    }

    MedicalHistoryRemotingControllerMVN.saveMedicalHistory(
        JSON.stringify(value),
        function(result, event) {
            if(event.status) {
                if(newRecord) {
                    cloneItem(recordTypeId, result.Id);
                }
                updateData(result);
                cancel(result.Id);
                document.getElementById(recordTypeId + '-errors').className = document.getElementById(recordTypeId + '-errors').className.replace('slds-show','slds-hide');
            } else if (event.type === 'exception') {
                var fieldErrorIndex = event.message.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION');
                if(fieldErrorIndex !== -1) {
                    var msgSplit = event.message.split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')[1].split(':');

                    if(msgSplit[1] === ' []') {
                        document.getElementById(recordTypeId + '-errors').className = document.getElementById(recordTypeId + '-errors').className.replace('slds-hide','slds-show');
                        document.getElementById(recordTypeId + '-errors').innerText = msgSplit[0];
                    } else {
                        var fieldName = msgSplit[1].replace(' [', '').replace(']', '');
                        var element;
                        if(newRecord) {
                            element = document.getElementById('error-new-' + fieldName);
                        } else {
                            element = document.getElementById('error-' + recordId + '-' + fieldName);
                        }

                        element.className = element.className.replace('slds-hide','slds-show');
                        element.innerText = msgSplit[0];
                        element.parentNode.className += ' slds-has-error'; 
                    }
                } else {
                    document.getElementById(recordTypeId + '-errors').className = document.getElementById(recordTypeId + '-errors').className.replace('slds-hide','slds-show');
                    document.getElementById(recordTypeId + '-errors').innerText = '{!$Label.Medical_History_Save_Error_MVN}';
                }
            
            }
        }
    );

    

}

function updateData(result, namespace, recordId) {
    if(namespace == undefined)
        namespace = '';

    if(recordId == undefined)
        recordId = result.Id;

    for (var property in result) {
        var id = recordId + '-' + namespace + property;

        if(Object.prototype.toString.call(result[property]) === '[object Object]') {
            updateData(result[property], namespace + property + '.', recordId)
            continue;
        }

        var el = document.getElementById(id);

        if (result.hasOwnProperty(property) && el != null) {
            if (el.tagName === 'INPUT' || el.tagName === 'SELECT' || el.tagName === 'TEXTAREA') {
                if(el.classList.contains('type-datetime')) {
                    el.value = formatDatetime(new Date(result[property]));
                } else if(el.classList.contains('type-date')) {
                    el.value = formatDate(new Date(result[property]));
                } else {
                    el.value = result[property];
                }
            }
            else {
                if(el.classList.contains('type-datetime')) {
                    el.innerText = formatDatetime(new Date(result[property]));
                } else if(el.classList.contains('type-date')) {
                    el.innerText = formatDate(new Date(result[property]));
                } else {
                    el.innerText = result[property];
                }
            }

        }
    }
}

function hideSaveButton(recordId) {
    document.getElementById(recordId + '-save').className = document.getElementById(recordId + '-save').className.replace('slds-show', 'slds-hide');
}

function disableInputs(recordId) {
    var inputs = document.getElementsByName(recordId);
    var i;
    for (i = 0; i < inputs.length; i++) {
        console.log(inputs[i].id + ':: ' + inputs[i].type);
        if(inputs[i].type !== 'submit') {
            inputs[i].disabled = true;
        }
    }
}

function resetValues(recordId) {
    var inputs = document.getElementsByName(recordId);
    var i;
    for (i = 0; i < inputs.length; i++) {
        var prevVal = inputs[i].getAttribute('data-prev');
        if(inputs[i].type === 'checkbox') {
            inputs[i].checked = prevVal;
        } else if(inputs[i].type === 'date' && prevVal !== ''){
            inputs[i].value = new Date(prevVal).toISOString().substring(0, 10);
        } else if(inputs[i].tagName === 'SELECT') {
            for(var j = 0; j < inputs[i].options.length; j++) {
                if(inputs[i].options[j].value === prevVal) {
                    inputs[i].selectedIndex = j;
                    break;
                }
            }
        } else {
            inputs[i].value = prevVal;
        }
    }
}

function cloneItem(recordTypeId, newId) {
    var item = document.getElementById(recordTypeId);
    var clone = item.cloneNode(true);
    item.removeAttribute("style");
    item.removeAttribute("id");

    var childItems = document.getElementsByName(recordTypeId);
    var nodeArray = [].slice.call(childItems);
    nodeArray.forEach( function(item) {
        if(item.name !== undefined) {
            item.name = item.name.replace(recordTypeId, newId);
        }
        else {
            item.name = item.setAttribute('name', newId);
        }

        item.id = item.id.replace('new', newId);
        item.id = item.id.replace(recordTypeId, newId);

        if(item.id.includes('error')) {
            item.className = item.className.replace('slds-show', 'slds-hide');
            item.innerText = '';
            item.parentNode.className += item.parentNode.className.replace('slds-has-error', ' '); 
        } else {
            item.setAttribute('data-prev', item.value);
        }
    });

    var btnGroup = document.getElementById(recordTypeId + '-group');
    var buttons = '<button id="' + newId + '-edit" class="slds-button slds-button--neutral slds-show" onclick="makeEditable(\'' + newId + '\');">Edit</button>' +
        '<button id="' + newId + '-cancel" class="slds-button slds-button--neutral slds-hide" onclick="cancel(\''+ newId + '\');">Cancel</button>' +
        '<button id="' + newId + '-save" class="slds-button slds-button--neutral slds-hide" onclick="save(\'' + newId + '\', \'' + recordTypeId + '\');">Save</button>';
    btnGroup.innerHTML += buttons;
    var saveBtn = document.getElementById(recordTypeId + '-save');
    btnGroup.removeChild(saveBtn);

    item.parentNode.insertBefore(clone, item);

    var clonedChildren = document.getElementsByName(recordTypeId);
    for(var i = 0, j = clonedChildren.length; i < j; i++) {
        
        if(clonedChildren[i].type == 'checkbox') {
            clonedChildren[i].checked = false;
        } else if(clonedChildren[i].type == 'select-one') {
            clonedChildren[i].selectedIndex = 0;
        } else if(clonedChildren[i].id.includes('error')) {
            clonedChildren[i].className = clonedChildren[i].className.replace('slds-show', 'slds-hide');
            clonedChildren[i].innerText = '';
            clonedChildren[i].parentNode.className += clonedChildren[i].parentNode.className.replace('slds-has-error', ' '); 
        } else {
            clonedChildren[i].value = '';
        }
    }
}

function expand(item) {
    var itemId = item.split('-')[0];
    document.getElementById(itemId + '-collapse').className = document.getElementById(itemId + '-collapse').className.replace('slds-hide', 'slds-show');
    document.getElementById(itemId + '-expand').className = document.getElementById(itemId + '-expand').className.replace('slds-show', 'slds-hide');
    document.getElementById(itemId + '-detail').className = document.getElementById(itemId + '-detail').className.replace('slds-hide', 'slds-show');
}

function collapse(item) {
    var itemId = item.split('-')[0];
    document.getElementById(itemId + '-collapse').className = document.getElementById(itemId + '-collapse').className.replace('slds-show', 'slds-hide');
    document.getElementById(itemId + '-expand').className = document.getElementById(itemId + '-expand').className.replace('slds-hide', 'slds-show');
    document.getElementById(itemId + '-detail').className = document.getElementById(itemId + '-detail').className.replace('slds-show', 'slds-hide');
}

function formatDate(date) {
    return date.toISOString().substring(0, 10);
}

function formatDatetime(date) {
    var months = ('0' + (date.getMonth() + 1)).slice(-2);
    var hours = ('0' + date.getHours()).slice(-2);
    var minutes = ('0' + date.getMinutes()).slice(-2);
    var strTime = hours + ':' + minutes;
    return date.getFullYear() + '-' + months + '-' + date.getDate() + ' ' + strTime;
}