ready();

/**
 * Executed when page is loaded / ready
 */
function ready() {
    var el = document.getElementById('items');
    var sortable = Sortable.create(el, {
        ghostClass: "sortable-ghost",
        onUpdate: onUpdate,
        animation: 150
    });
}

/**
 * Updating record details
 * @param   {Object}  DOM Element
 * @param   {String}  Record ID
 * @param   {String}  SObject Name
 * @param   {String}  Field Name
 */
function update(el, id, sobject, field) {
    var ci;

    if(sobject == 'Contact_Information_MVN__c') {
        ci = new SObjectModel.ContactInformation({ 'Id': id });
    } else if(sobject == 'Communication_Preference_MVN__c') {
        ci = new SObjectModel.CommunicationPreference({ 'Id': id });
    } else {
        ci = new SObjectModel.Address({ 'Id': id });
    }

    if(el.type == 'checkbox') {
        ci.set(field, el.checked);
    } else {
        ci.set(field, el.value);
    }

    console.log('Updating ' + sobject + '.' + field + ' (' + id + ') with following:');
    console.log(ci._props);

    ci.update(function(err, result) {
        if(err) console.log(err);
        else console.log('Success! Result: ' + result);
    });
}

/**
 * Deleting Contact Information record by ID
 * @param  {String} recordId
 */
function deleteRecord(recordId) {
    var confirmed = window.confirm(DELETE_CONFIRMATION);

    if(confirmed) {
        var element = document.getElementById(recordId);
        element.parentNode.removeChild(element);
        var ci = new SObjectModel.ContactInformation({ 'Id': recordId });
        ci.del();
    }
}

/**
 * Executed on Table Order Update
 * @param  {Object} evt Event
 */
function onUpdate(evt) {
    var tbody = document.getElementById('items');
    tbody.children

    for(var i = 0; i < tbody.children.length; i++) {
        var element = tbody.children[i];

        var ci = new SObjectModel.ContactInformation();
        ci.set('Id', element.id);
        ci.set('Sequence_Number_MVN__c', i);
        ci.update();
    }
}
