/**
 * @author Mavens
 * @date 08/2018
 * @description Class used to build fake data for custom metadata type for Eligibility Engine Settings
 */ 
@isTest
public with sharing class TestFactoryEligibilityEngineSettingMVN {

    public static void setMocks() {
        List<Eligibility_Engine_Setting_MVN__mdt> settings = new List<Eligibility_Engine_Setting_MVN__mdt>();
        Map<String, Object> setting;

        setting = new Map<String, Object>{
            'Assure_Result_Codes_MVN__c' => 'D,SLN-D,DEA-D,H',
            'DocGen_Package_filter_MVN__c' => 'EligibilityLetters-Denial',
            'Engine_Result_Value_MVN__c' => 'Denied',
            'Result_Color_MVN__c' => '#ef6e64',
            'Result_Icon_Name_MVN__c' => 'close',
            'Result_Label_MVN__c' => 'DENIED',
            'DeveloperName' => 'Denied_MVN'
        };

        settings.add(
            (Eligibility_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Eligibility_Engine_Setting_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Assure_Result_Codes_MVN__c' => 'A,SLN-A,DEA-A,B,C,DEA-C',
            'DocGen_Package_filter_MVN__c' => 'EligibilityLetters-MissingInfo',
            'Engine_Result_Value_MVN__c' => 'Missing Information',
            'Result_Color_MVN__c' => '#fbb439',
            'Result_Icon_Name_MVN__c' => 'priority',
            'Result_Label_MVN__c' => 'MISSING INFORMATION',
            'DeveloperName' => 'Missing_Information_MVN'
        };

        settings.add(
            (Eligibility_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Eligibility_Engine_Setting_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Assure_Result_Codes_MVN__c' => 'E',
            'DocGen_Package_filter_MVN__c' => 'EligibilityLetters-Approval',
            'Engine_Result_Value_MVN__c' => 'Passed',
            'Result_Color_MVN__c' => '#00c6b7',
            'Result_Icon_Name_MVN__c' => 'approval',
            'Result_Label_MVN__c' => 'PASSED',
            'DeveloperName' => 'Passed_MVN'
        };

        settings.add(
            (Eligibility_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Eligibility_Engine_Setting_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Assure_Result_Codes_MVN__c' => '',
            'DocGen_Package_filter_MVN__c' => '',
            'Engine_Result_Value_MVN__c' => 'Invalid Rule',
            'Result_Color_MVN__c' => '#ef6e64',
            'Result_Icon_Name_MVN__c' => 'close',
            'Result_Label_MVN__c' => 'INVALID RULE',
            'DeveloperName' => 'Invalid_Rule_MVN'
        };

        settings.add(
            (Eligibility_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Eligibility_Engine_Setting_MVN__mdt', setting)
        );

        EligibilityEngineSettingMVN.setMocks(settings);
    }

    public static void setMocks(List<Map<String, Object>> fieldValuesList) {
        List<Eligibility_Engine_Setting_MVN__mdt> settings = new List<Eligibility_Engine_Setting_MVN__mdt>();
        for (Map<String, Object> setting : fieldValuesList) {
            settings.add(
                (Eligibility_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                    'Eligibility_Engine_Setting_MVN__mdt', setting)
            );
        }
        EligibilityEngineSettingMVN.setMocks(settings);
    }

}