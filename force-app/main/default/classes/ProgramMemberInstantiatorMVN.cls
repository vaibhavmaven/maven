/**
 *	ProgramMemberInstantiatorMVN
 *	Created By:		Aggen
 *	Created On:		10/25/2015
 *  Modified:     March 3, 2017 to bulkify code (Kyle Thornton)
 *  Description:	This class is responsible for looking at the program member, determining the associated program
 *					      and creating program member stage and program member stage dependency records.
 *					      It will also call the dependency manager to instantiate the "journey."
 *
 **/
public class ProgramMemberInstantiatorMVN implements TriggerHandlerMVN.HandlerInterface {

	private static Decimal addedStageSequenceNumber;
    private static List<Program_Member_MVN__c> programMembers;
    private static Set<Program_Member_Stage_MVN__c> programMemberStagesToUpdate;
    private static Map<Id, List<Program_Stage_MVN__c>> parentProgramStagesByProgram;
    private static Map<Id, List<Program_Stage_MVN__c>> childStagesByParentId;
    private static Map<Id, List<Program_Stage_Dependency_MVN__c>> dependenciesByProgramStage;

    private static String programStageQuery = 'SELECT Id, Name, Exclude_Activity_MVN__c, Program_MVN__c,'
                                                 + ' (SELECT Id, Exclude_Activity_MVN__c, Program_MVN__c'
                                                 + '    FROM Child_Stages_MVN__r'
                                                 + '   WHERE Library_Only_MVN__c = false)'
                                            + ' FROM Program_Stage_MVN__c';


    /* Trigger handler */
    public void handle() {
        programMembers = new List<Program_Member_MVN__c>();
        programMemberStagesToUpdate = new Set<Program_Member_Stage_MVN__c>();
        if(trigger.isInsert) {
            programMembers = (List<Program_Member_MVN__c>) Trigger.new;
        } else if(trigger.isUpdate) {
            for(Id programMemberId : trigger.newMap.keySet()) {
                if(((Program_Member_MVN__c)trigger.oldMap.get(programMemberId)).Active_Application_MVN__c != ((Program_Member_MVN__c)trigger.newMap.get(programMemberId)).Active_Application_MVN__c) {
                    if(((Program_Member_MVN__c)trigger.oldMap.get(programMemberId)).Active_Application_MVN__c == null) {
                        programMemberStagesToUpdate.addAll([SELECT Id FROM Program_Member_Stage_MVN__c WHERE Program_Member_MVN__c IN :trigger.newMap.keySet()]);
                    } else {
                        programMembers.add((Program_Member_MVN__c)trigger.newMap.get(programMemberId));
                    }
                }
            }
        }

        if(!programMemberStagesToUpdate.isEmpty()) {
            update new List<Program_Member_Stage_MVN__c>(programMemberStagesToUpdate);
        }

        if(!programMembers.isEmpty()) {
            /**
            * close all old stages
            *
            Set<Id> programMemberIds = new Set<Id>();
            Set<Id> activeApplicationIds = new Set<Id>();
            for(Program_Member_MVN__c programMember : programMembers) {
                programMemberIds.add(programMember.Id);
                activeApplicationIds.add(programMember.Active_Application_MVN__c);
            }

            Set<Id> programMemberStageIds = new Set<Id>();
            List<Program_Member_Stage_MVN__c> programMemberStagesToClose = new List<Program_Member_Stage_MVN__c>();
            for(Program_Member_Stage_MVN__c programMemberStageToClose : getProgramMemberStagesToCancel(programMemberIds, activeApplicationIds)) {
                programMemberStageToClose.Status_MVN__c = 'Cancelled';
                programMemberStagesToClose.add(programMemberStageToClose);
                programMemberStageIds.add(programMemberStageToClose.Id);
            }
            update programMemberStagesToClose;

            List<Case> casesToClose = new List<Case>();
            for(Case caseToClose : [SELECT Id, Status FROM Case WHERE Program_Member_Stage_MVN__c IN :programMemberStageIds]) {
                caseToClose.Status = 'Cancelled';
                casesToClose.add(caseToClose);
            }
            update casesToClose;
            */

            addedStageSequenceNumber = null;
            setupRecordMaps(true);
            execute();
        }
    }

    /* initate a newly added stage for a given program member */
    public static void addStageForProgramMember(Id stageId, Program_Member_MVN__c pm) {
        programMembers = new List<Program_Member_MVN__c>{pm};
        addedStageSequenceNumber = [SELECT count()
                                      FROM Program_Member_Stage_MVN__c
                                     WHERE Program_Member_MVN__r.Program_MVN__c = :pm.Program_MVN__c
                                       AND Program_Member_MVN__c = :pm.Id
                                       AND Parent_Program_Member_Stage_MVN__c = null] + 1;


        setupRecordMaps(false);
        //overwrite parentProgramStagesByProgram with just the program and stage in question
        Program_Stage_MVN__c stageToAdd = Database.query(String.escapeSingleQuotes(programStageQuery) + ' where Id = :stageId');
        parentProgramStagesByProgram = new Map<Id, List<Program_Stage_MVN__c>> {
            stageToAdd.Program_MVN__c => new List<Program_Stage_MVN__c>{stageToAdd}
        };

        execute();
    }

    private static void execute() {

        /************************************************
            create all of the parent member stages
        ************************************************/
        List<Program_Member_Stage_MVN__c> parentMemberStages = new List<Program_Member_Stage_MVN__c>();
        for (Program_Member_MVN__c programMember : programMembers) {
            if(parentProgramStagesByProgram.containsKey(programMember.Program_MVN__c)) {
                List<Program_Stage_MVN__c> programStages = parentProgramStagesByProgram.get(programMember.Program_MVN__c);

                for (Integer stageIndex=0; stageIndex<programStages.size(); stageIndex++) {
                    //if addedStageSequenceNumber is not null then only one parent stage is being inserted and
                    //the sequence number is pre-determined
                    Decimal stageIndexToPass = addedStageSequenceNumber != null ? addedStageSequenceNumber : stageIndex + 1;
                    Program_Member_Stage_MVN__c parentStage = buildProgramMemberStage(programStages[stageIndex],
                                                                                      programMember.Id,
                                                                                      stageIndexToPass);
                    parentMemberStages.add(parentStage);
                }
            }
        }

        insert parentMemberStages;

        Map<String, Program_Member_Stage_MVN__c> memberStageByPmAndStage = new Map<String, Program_Member_Stage_MVN__c>();

        /************************************************
            create all of the child member stages
        ************************************************/
        List<Program_Member_Stage_MVN__c> childMemberStages = new List<Program_Member_Stage_MVN__c>();
        for (Program_Member_Stage_MVN__c parentMemberStage : parentMemberStages) {
            List<Program_Stage_MVN__c> childStages = childStagesByParentId.get(parentMemberStage.Program_Stage_MVN__c);

            for (Integer stageIndex=0; stageIndex<childStages.size(); stageIndex++) {

                Program_Member_Stage_MVN__c childStage = buildProgramMemberStage(childStages[stageIndex],
                                                                                 parentMemberStage.Id,
                                                                                 parentMemberStage.Program_Member_MVN__c,
                                                                                 stageIndex+1);

                childMemberStages.add(childStage);
            }

            /************************************************************************************************************
            create a map of the member stages. The key is a combination of the program member and the program stage
            it comes from. This allows for retrieval of the controller program member stage.
            *************************************************************************************************************/

            String stageKey = (String) parentMemberStage.Program_Member_MVN__c + (String) parentMemberStage.Program_Stage_MVN__c;
            memberStageByPmAndStage.put(stageKey, parentMemberStage);
        }

        insert childMemberStages;

        for (Program_Member_Stage_MVN__c childMemberStage : childMemberStages) {
            String stageKey = (String) childMemberStage.Program_Member_MVN__c + (String) childMemberStage.Program_Stage_MVN__c;
            memberStageByPmAndStage.put(stageKey, childMemberStage);
        }

        /************************************************************************************************
            Use the map just created to figure out the dependencies that need to be created
        ************************************************************************************************/
        List<Program_Member_Stage_Dependency_MVN__c> memberStageDependencies = new List<Program_Member_Stage_Dependency_MVN__c>();
        for (Program_Member_Stage_MVN__c childMemberStage : childMemberStages) {
            Id programStageId = childMemberStage.Program_Stage_MVN__c;

            if (!dependenciesByProgramStage.containsKey(programStageId)) {
                continue; //no dependencies to create for this stage
            }

            List<Program_Stage_Dependency_MVN__c> stageDependencies = dependenciesByProgramStage.get(programStageId);

            Id programMemberId = childMemberStage.Program_Member_MVN__c;

            for (Program_Stage_Dependency_MVN__c  dependency : stageDependencies) {
                String dependencyKey = (String) programMemberId + (String) dependency.Program_Stage_Dependency_MVN__c;

                if (memberStageByPmAndStage.containsKey(dependencyKey)) {
                    //a member child stage exists which the current member child stage should be dependent on.
                    Program_Member_Stage_MVN__c dependencyChildStage = memberStageByPmAndStage.get(dependencyKey);
                    Program_Member_Stage_Dependency_MVN__c memberStageDependency = new Program_Member_Stage_Dependency_MVN__c();

                    memberStageDependency.Program_Stage_Dependency_MVN__c = dependency.Id;
                    memberStageDependency.Program_Member_Stage_MVN__c = childMemberStage.Id;
                    memberStageDependency.Program_Member_Stage_Dependency_MVN__c = dependencyChildStage.Id;

                    memberStageDependencies.add(memberStageDependency);
                }
            }
        }

        insert memberStageDependencies;

        /*******************************************************
            pass the new program members off for initialiation
        *******************************************************/
        ProgramMemberStageManagerMVN.initializeProgramMembers(programMembers);
    }

    private static List<Program_Member_Stage_MVN__c> getProgramMemberStagesToCancel(Set<Id> programMemberIds, Set<Id> activeApplicationIds) {
        return [
            SELECT
                Id,
                Status_MVN__c
            FROM
                Program_Member_Stage_MVN__c
            WHERE
                Program_Member_MVN__c IN :programMemberIds
            AND
                Application_MVN__c NOT IN :activeApplicationIds
        ];
    }

    /**
     * parse out the program ids and pass them off to appropriate methods to create various data maps
     */
    private static void setupRecordMaps(Boolean excludeLibraryStages) {
        //Get the Program Ids
        Set<Id> programIds = new Set<Id>();
        for(Program_Member_MVN__c pm : programMembers) {
            programIds.add(pm.Program_MVN__c);
        }

        generateProgramStageToProgramMap(programIds, excludeLibraryStages);

        //create a map of all child stages to parent ids
        childStagesByParentId = new Map<Id, List<Program_Stage_MVN__c>>();
        for (List<Program_Stage_MVN__c> parentStages : parentProgramStagesByProgram.values()) {
            for (Program_Stage_MVN__c parentStage : parentStages) {
                childStagesByParentId.put(parentStage.Id, parentStage.Child_Stages_MVN__r);
            }
        }

        //Get the dependencies for the programs
        generateDependenciesByProgramStage(programIds);
    }

    /**
     * Create a map of programs to ordered list of program stages
     * @param programIds : program ids currently in context
     */
    private static void generateProgramStageToProgramMap(Set<Id> programIds, Boolean excludeLibraryStages) {
        parentProgramStagesByProgram = new Map<Id, List<Program_Stage_MVN__c>>();

        String programStageAndChildrenQuery = programStageQuery
                                            + ' WHERE Program_MVN__c in :programIds'
                                            + ' AND Is_Parent_Stage_MVN__c = true';
        if (excludeLibraryStages) {
        	programStageAndChildrenQuery += ' AND Library_Only_MVN__c = false';
        }
		programStageAndChildrenQuery += ' ORDER BY Stage_Sequence_Number_MVN__c, CreatedDate';

        for(Program_Stage_MVN__c programStage : Database.query(programStageAndChildrenQuery)) {
            Id programId = programStage.Program_MVN__c;

            if(!parentProgramStagesByProgram.containsKey(programId)) {
                parentProgramStagesByProgram.put(programId, new List<Program_Stage_MVN__c>());
            }

            parentProgramStagesByProgram.get(programId).add(programStage);
        }
    }

    /**
     * Crate a map of program stages to any dependencies which exist under the program stage
     * @param programIds : program ids currently in context
     */
    private static void generateDependenciesByProgramStage(Set<Id> programIds) {
        List<Program_Stage_Dependency_MVN__c> dependencies;
        dependencies = [SELECT Id,
                               Name,
                               Program_Stage_Dependency_MVN__c,
                               Program_Stage_Dependency_MVN__r.Name,
                               Program_Stage_MVN__c,
                               Program_Stage_MVN__r.Name
                          FROM Program_Stage_Dependency_MVN__c
                         WHERE Program_Stage_MVN__r.Program_MVN__c in :programIds
                           AND Program_Stage_MVN__r.Library_Only_MVN__c = false
                           AND Program_Stage_Dependency_MVN__r.Library_Only_MVN__c = false];

        dependenciesByProgramStage = new Map<Id, List<Program_Stage_Dependency_MVN__c>>();

        for(Program_Stage_Dependency_MVN__c dependency : dependencies) {
            Id programStageId = dependency.Program_Stage_MVN__c;

            if(!dependenciesByProgramStage.containsKey(programStageId)) {
                 dependenciesByProgramStage.put(programStageId, new List<Program_Stage_Dependency_MVN__c>());
            }
            dependenciesByProgramStage.get(programStageId).add(dependency);
        }
    }

    /**
     * build a program member stage
     * @param  programStage    The stage his program member stage is being cloned from
     * @param  programMemberId The id of the program member this stage is being created for
     * @param  sequenceNumber  The sequence this stage falls under
     * @return                 a program member stage object populated with necessary information
     */
    private static Program_Member_Stage_MVN__c buildProgramMemberStage(Program_Stage_MVN__c programStage, Id programMemberId, Decimal sequenceNumber) {
        return buildProgramMemberStage(programStage, null, programMemberId, sequenceNumber);
    }

    /**
      * build a program member stage
     * @param  programStage        The stage his program member stage is being cloned from
     * @param  parentMemberStageId Id of the program member stage this stage should be related to
     * @param  programMemberId     The id of the program member this stage is being created for
     * @param  sequenceNumber      The sequence this stage falls under
     * @return                     a program member stage object related to another program member stage object and
     *                             populated with necessary information
     */
    private static Program_Member_Stage_MVN__c buildProgramMemberStage(Program_Stage_MVN__c programStage, Id parentMemberStageId, Id programMemberId, Decimal sequenceNumber) {
        Program_Member_Stage_MVN__c programMemberStage = new Program_Member_Stage_MVN__c();

        programMemberStage.Exclude_Activity_MVN__c = programStage.Exclude_Activity_MVN__c;
        programMemberStage.Program_Stage_MVN__c = programStage.Id;
        programMemberStage.Program_Member_MVN__c = programMemberId;
        programMemberStage.Parent_Program_Member_Stage_MVN__c = parentMemberStageId;
        programMemberStage.Stage_Sequence_Number_MVN__c = sequenceNumber;

        return programMemberStage;
    }
}