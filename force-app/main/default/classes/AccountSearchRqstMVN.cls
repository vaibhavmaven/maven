/**
 * AccountSearchRqstMVN.cls
 * @created By: Kyle Thornton
 * @created Date: June 2016
 * @edited By: Pablo Roldan
 * @edited Date: November 2017
 *
 * @description: This class models the search request. account and contactInformation fields are used to bind inputs
 *               on the search page. search field settings indicate the configuration that was used when searching.
 *               recordType is the configured record type to search for (All is valid and means all valid record types).
 *               If a search should be restricted to an affiliation the restrictToAccountId variable will be an Id.
 *               resultFieldSettings are also maintained so that they are available to the search results.
 *
 * @edit description: Adapted for Patient Support.
 */
public class AccountSearchRqstMVN {

    public Account                account { get; set;}
    public List<String>           accountExternalIds {get; set;}
    public AccountTypeSettingsMVN    acctTypeSettings;
    public Address_vod__c address { get; set;}
    public Boolean                isPersonSearch {get; set;}
    public AccountFieldSettingsMVN   searchFieldSettings { get; private set;}
    public String                 recordType {get; set;}
    public String                 restrictToAccountId {get; set;}
    public AccountFieldSettingsMVN   resultFieldSettings {get; private set;}
    public Integer                  searchLimit { get; set; }

    public AccountSearchRqstMVN(List<String> externalIds) {
        accountExternalIds = externalIds;
    }

    public AccountSearchRqstMVN(AccountFieldSettingsMVN searchSettings, AccountFieldSettingsMVN resultSettings, String country) {
        clearSearchFields(country);
        resetFields(searchSettings, resultSettings);
    }

    public void clearSearchFields(String country) {
        account = new Account(Country_MVN__c = country);
        address = new Address_vod__c(Country_vod__c = country);
    }

    public void resetFields(AccountFieldSettingsMVN searchSettings, AccountFieldSettingsMVN resultSettings) {
        searchFieldSettings = searchSettings;
        resultFieldSettings = resultSettings;
    }

    public Boolean noSearchFieldsPopulated(AccountFieldSettingsMVN searchFields) {
        Boolean hasPopulatedTermsBelowThreshold = false;
        for (DCR_Field_Setting_MVN__mdt setting : searchFields.getSettings()) {
            if (setting.Field_MVN__c.toLowerCase() == 'blank') {
                continue;
            }

            Schema.DisplayType fieldType = AccountFieldSettingsMVN.getFieldType(setting);

            Object value;
            if (setting.Object_MVN__c.toLowerCase() == 'account') {
                value = account.get(setting.Field_MVN__c);
            } else if (setting.Object_MVN__c.toLowerCase() == 'address_vod__c') {
                value = address.get(setting.Field_MVN__c);
            }   

            if(value != null && fieldType == DisplayType.Boolean && (Boolean)value) {
                return false;
            } else if(value != null) {
                String strValue = (String)value;
                if (String.isNotBlank(strValue)) {
                  strValue = strValue.replaceAll('\\*', '');
                    if (strValue.length() > 1) {
                      return false;
                    } else {
                      hasPopulatedTermsBelowThreshold = true;
                    }
                }
            }
        }
        if (hasPopulatedTermsBelowThreshold) {
          throw new UtilitiesMVN.utilException(Label.Search_Term_Must_Have_2_Characters_MVN);
        }

        return true;
    }

    public Boolean populateSearchFieldIfExists(String fieldName, String fieldValue, Map<String, String> searchFields) {
      if (searchFields.containsKey(fieldName.toLowerCase())) {
          if (searchFields.get(fieldname.toLowerCase()) == 'account') {
            account.put(fieldName, fieldValue);
          } else {
            address.put(fieldName, fieldValue);
          }
          return true;
      }
      return false;
  }

    /**
     * Updates account record with contact info email and phone to be able to filter any search in Veeva Network
     * @param  contactInfo Contact Information record
     */
    public void addContactInfo(Contact_Information_MVN__c contactInfo) {
        if (contactInfo != null) {
            if (String.IsNotBlank(contactInfo.Email_MVN__c)) {
                account.PersonEmail = contactInfo.Email_MVN__c;
            }

            if (String.IsNotBlank(contactInfo.Phone_MVN__c)) {
                account.Phone = contactInfo.Phone_MVN__c.replaceAll('[^0-9]', '');
            }
        }
    }
}