@isTest
private class RandomEnrollmentAuditTestMVN {

    static Program_MVN__c program;
    static Program_Stage_MVN__c auditStage;
    static List<Program_Member_MVN__c> programMembers;
    static List<Program_Member_Stage_MVN__c> pmStages;
    static List<Application_MVN__c> applications;
    static List<Case> cases;
    static Patient_Service_Settings_MVN__c settings;

    static final Integer DATA_SIZE = 10;

    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        return programFactory.create(
            new Map<String,Object>{
                'Name' => 'A Program',
                'Program_Id_MVN__c' => 'Knipper_PAP_MVN',
                'Active_MVN__c' => true,
                'Job_No_MVN__c' => '12345',
                'Daily_Audit_MVN__c' => 50
            }
        );
    }

    static Program_Stage_MVN__c createAuditStages() {
        Program_Stage_MVN__c stage = new Program_Stage_MVN__c (
            Name = 'Audit Request',
            Program_MVN__c = program.Id
        );
        insert stage;
        return stage;
    }

    static List<Program_Member_MVN__c> createProgramMembers() {
        return TestDataFactoryMVN.createProgramMembers(program, auditStage.Id, DATA_SIZE);
    }

    static List<Application_MVN__c> createApplications() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        List<Application_MVN__c> testApplications = new List<Application_MVN__c>();
        for (Program_Member_MVN__c programMember : programMembers) {
            testApplications.add(
                applicationFactory.construct(new Map<String, Object>{
                    'Program_Member_MVN__c' => programMember.Id
                })
            );
        }
        insert testApplications;

        return testApplications;
    }

    static List<Program_Member_Stage_MVN__c> createPMStages() {
        List<Program_Member_Stage_MVN__c> memStages = new List<Program_Member_Stage_MVN__c>();
        for (Program_Member_MVN__c programMember : programMembers) {
            memStages.add(
                new Program_Member_Stage_MVN__c(
                    Program_Member_MVN__c = programMember.Id,
                    Program_Stage_MVN__c = auditStage.Id
                )
            );
        }
        insert memStages;
        return memStages;
    }

    static List<Case> createCases() {
        List<Case> newCases = new List<Case>();
        for (Integer i=0; i < DATA_SIZE; i++) {
            newCases.add(
                new Case(
                    Program_Member_MVN__c = programMembers[i].Id,
                    Application_MVN__c = applications[i].Id,
                    Program_Member_Stage_MVN__c = pmStages[i].Id
                )
            );
        }
        insert newCases;
        return newCases;
    }

    static {
        User caseManager = TestDataFactoryMVN.generateCaseManager();
        System.runAs(caseManager) {
            settings = TestDataFactoryMVN.buildPSSettings();
            settings.Name = 'default';
            settings.SetupOwnerId=UserInfo.getOrganizationId();
            insert settings;
            program = createProgram();
            auditstage = createAuditStages();
            programMembers = createProgramMembers();
            applications = createApplications();
            pmStages = createPMStages();
            cases = createCases();
        }
    }

    @isTest
    static void itShouldLeave50PercentOfCasesOpenFromLastHour() {
        for (Case c : cases) {
            Test.setCreatedDate(c.Id, DateTime.now().addHours(-1));
        }

        Test.startTest();
        RandomEnrollmentAuditMVN.run();
        Test.stopTest();

        assertSuccess();
    }

    @isTest
    static void itShouldRunForAProvidedTimeFrame() {
        settings.Audit_Batch_Job_Lower_Bound_MVN__c = DateTime.now().addMinutes(-1);
        settings.Audit_Batch_Job_Upper_Bound_MVN__c = DateTime.now().addMinutes(1);
        update settings;

        Test.startTest();
        RandomEnrollmentAuditMVN.run();
        Test.stopTest();

        assertSuccess();
    }

    @isTest
    static void itShouldBeScheduleableForHourlyRun() {
        Test.startTest();
        RandomEnrollmentAuditMVN.scheduleHourlyRun('Test Random Enrollment Audit');
        Test.stopTest();

        System.assertEquals(1, [
            SELECT
                Count()
            FROM
                CronJobDetail
            WHERE
                Name = 'Test Random Enrollment Audit'
        ]);
    }

    @isTest
    static void itShouldErrorWhenBatchSizeGreaterThanOne() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        programFactory.create(
            new Map<String,Object>{
                'Name' => 'A second auditable Program',
                'Program_Id_MVN__c' => 'Other_PAP_MVN',
                'Active_MVN__c' => true,
                'Job_No_MVN__c' => '12346',
                'Daily_Audit_MVN__c' => 50
            }
        );

        Boolean caught = false;
        try {
            Test.startTest();
            Database.executeBatch( new RandomEnrollmentAuditMVN(), 2);
            Test.stopTest();
        } catch (PatientServicesMVNException ex) {
            caught = true;
            System.assertEquals(ex.getMessage(), RandomEnrollmentAuditMVN.BATCH_SIZE_EXCEPTION);
        }
        System.assert(caught);
    }

    private static void assertSuccess() {
        System.assertEquals(DATA_SIZE / 2, [SELECT Count() FROM Case WHERE Status = 'System Cancelled']);
        System.assertEquals(DATA_SIZE / 2, [
            SELECT
                Count()
            FROM
                Application_MVN__c
            WHERE
                Audit_Result_MVN__C = 'System Cancelled'
        ]);
    }
}