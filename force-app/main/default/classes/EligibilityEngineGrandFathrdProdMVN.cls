/**
 * @author      Mavens
 * @date        November 2018
 * @group       EligibilityEngine
 * @description Implements the validation of applications with
 *              prescriptions for grandfathered products
 */
public with sharing class EligibilityEngineGrandFathrdProdMVN extends EligibilityEngineRuleHdlrMVN {

    private EligibilityEngineQueryConfigMVN config;

    private static final Set<String> prescriptionFieldsToQuery = new Set<String> {
        'Product_MVN__c',
        'Product_MVN__r.Product_Status_MVN__c'
    };

    /**
     * @description Return a map of object api names to set of fields to be queried
     * @return  Map<String, Set<String>> map of object api names to set of fields needed
     */
    public override Map<String, Set<String>> getExtraFieldsToQuery() {
        return new Map<String, Set<String>>{'Prescriptions_MVN__r' => prescriptionFieldsToQuery};
    }

    /**
     * @description Configure the rule to request Schedulers from the parent program member
     * @return EligibilityEngineQueryConfigMVN the configured query
     */
    public override EligibilityEngineQueryConfigMVN getAdditionalDataConfig() {
        if (config == null) {
            config = new EligibilityEngineQueryConfigMVN(
                'Program_Member_MVN__c',
                'Program_Member_MVN__c'
            );
            config.addChildConfiguration(
                new Map<String, Set<String>>{
                    'Order_Schedulers__r' => new Set<String> {
                        'Prescription_MVN__r.Product_MVN__c',
                        'Grandfathered_Cutoff_Date_MVN__c'
                    }
                }
            );
        }
        return config;
    }

     /**
     * @description checks if rule is fired. This rule requires a prior order within the specified grace period
     * for any product that is marked as grandfathered. Order Schedules have a roll-up summary field
     * that indicates the cutoff date for an order based on the latest order date for all child
     * orders. For a given product the last date for ALL orders for that product is used to compare
     * to today's date.
     * @return  Boolean     flag if rule is fired
     */
    private Boolean isRuleFired() {
        List<Program_Member_MVN__c> programMembers
            = (List<Program_Member_MVN__c>) super.requestedData.get(getAdditionalDataConfig().getKey());


        List<Order_Scheduler_MVN__c> orderSchedulers = programMembers[0].Order_Schedulers__r;

        for (Prescription_MVN__c prescription : super.application.Prescriptions_MVN__r) {
            /* only fire rule for Grandfathered Products */
            if (prescription.Product_MVN__r.Product_Status_MVN__c != 'Grandfathered') {
                continue;
            }

            Date maxOrderDate;
            for (Order_Scheduler_MVN__c scheduler : orderSchedulers) {
                /* skip check if prescription product doesn't match scheduler product */
                if (scheduler.Prescription_MVN__r.Product_MVN__c != prescription.Product_MVN__c
                    || scheduler.Grandfathered_Cutoff_Date_MVN__c == null) {
                    continue;
                }

                if (maxOrderDate == null || scheduler.Grandfathered_Cutoff_Date_MVN__c > maxOrderDate) {
                    maxOrderDate = scheduler.Grandfathered_Cutoff_Date_MVN__c;
                }
            }

            /* If no order exists within the grace period for grandfatherd product, fire rule */
            if (maxOrderDate == null || maxOrderDate < Date.Today()) {
                super.resultList.get(0).Result_MVN__c = EligibilityEngineMVN.RULEDENIED;
                super.resultList.get(0).Result_Message_MVN__c = super.rule.Result_Message_MVN__c;
                return true;
            }
        }

        return false;
    }
}