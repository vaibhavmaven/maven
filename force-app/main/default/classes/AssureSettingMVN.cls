/**
 * @author Mavens
 * @date November 2018
 * @description Class to manage retrieving the fields from the Assure Setting custom metadata type
 */ 
public with sharing class AssureSettingMVN {

    private static List<Assure_Setting_MVN__mdt> testMock;

    public static List<Assure_Setting_MVN__mdt> allSettings {
        get {
            if (allSettings == null) {
                Map<String, Schema.SObjectField> fieldMapping = 
                    Assure_Setting_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
                List<String> fields = new List<String>(fieldMapping.keySet());

                String queryString = 'SELECT ' + String.escapeSingleQuotes(String.join(fields, ','))
                                    + ' FROM Assure_Setting_MVN__mdt';
                allSettings = Database.query(queryString);
            }
            if(testMock != null){
                allSettings = testMock;
            }
            return allSettings;
        }
        private set;
    }

    public static Map<String, Assure_Setting_MVN__mdt> allSettingsByCode {
        get {
            if (allSettingsByCode == null) {
                allSettingsByCode = new Map<String, Assure_Setting_MVN__mdt>();
                for (Assure_Setting_MVN__mdt setting : allSettings) {
                    allSettingsByCode.put(setting.Assure_Result_Code_MVN__c, setting);
                }
            } 
            return allSettingsByCode;
        }
        private set;
    }

    @TestVisible
    private static void setMocks(List<Assure_Setting_MVN__mdt> mocks){
        testMock = mocks;
    }
}