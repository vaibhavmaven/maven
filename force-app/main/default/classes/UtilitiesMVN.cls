/**
 * UtilitiesMVN
 * @created By: Roman Lerman
 * @created Date: 3/4/2013
 * @edited by: Pablo Roldan
 * @edited date: November 2017
 * @description: This class contains various utility and convience methods used throughout the accelerator
 * @edit description: Included common DCR and Search functionalities adapted to PS.
 */
public with sharing class UtilitiesMVN {

    public static final String ALL_PICKLIST_VALUE = 'All';

    public static Boolean medicalHistorySaveSuccess = false;
    public static Map<Schema.SObjectType, Map<Id, RecordType>> recordTypes { get; private set; }

    private static final Set<String> recordTypesToRetrieve = new Set<String>{'Account',
                                                                             'Address_vod__c',
                                                                             'Adverse_Event_MVN__c',
                                                                             'Case',
                                                                             'Contact_Information_MVN__c',
                                                                             'Data_Change_Request_MVN__c',
                                                                             'Product_MVN__c',
                                                                             'Product_Quality_Complaint_MVN__c',
                                                                             'Request_MVN__c',
                                                                             'Loop__DDP__c',
                                                                             'Loop__DDP_Integration_Option__c'};

    public class utilException extends Exception {}

    public static String getRandomString(Integer count) {
        String chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
        String hash = '';
        for (integer i = 0; i < count; i++) {
            Integer rnum = Math.floor(Math.random() * chars.length()).intValue();
            hash += chars.substring(rnum,rnum+1);
        }
        return hash;
    }

    public static Map<String, String> getPicklistValues(String objectApiName, String fieldApiName) {
        Map<String, String> options = new Map<String, String>();
        List<Schema.DescribeSObjectResult> describeResults =
            Schema.describeSObjects( new String[]{ objectApiName } );
        Schema.DescribeSObjectResult objectDescribe = describeResults.get(0);
        Map<String, Schema.SObjectField> fieldMap = objectDescribe.fields.getMap();

        for (String fieldName : fieldMap.keySet()) {
            if (fieldName == fieldApiName) {
                Schema.SObjectField field = fieldMap.get( fieldName );
                Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
                List<Schema.PicklistEntry> ple = fieldDescribe.getPicklistValues();
                for (Schema.PicklistEntry f : ple) {
                    options.put(f.getLabel(), f.getValue());
                }
                break;
            }
        }

        return options;
    }

    private static List<RecordType> recordTypeList {
        get {
            if (recordTypeList == null) {
                recordTypeList = [SELECT Id, toLabel(Name), DeveloperName, IsPersonType, SObjectType
                                    FROM RecordType
                                   WHERE SObjectType IN :recordTypesToRetrieve
                                     AND IsActive = true];
            }
            return recordTypeList;
        }
        set;
    }

    @TestVisible
    private static Map<String, Map<String,RecordType>> recordTypesByDeveloperNameByObject {
        get {
            if (recordTypesByDeveloperNameByObject == null) {
                recordTypesByDeveloperNameByObject = new Map<String, Map<String,RecordType>>();

                for (RecordType recordType : recordTypeList) {

                    if (!recordTypesByDeveloperNameByObject.containsKey(recordType.SObjectType)) {
                        recordTypesByDeveloperNameByObject.put(recordType.SObjectType, new Map<String,RecordType>());
                    }

                    recordTypesByDeveloperNameByObject.get(recordType.SObjectType).put(recordType.DeveloperName, RecordType);
                }
            }

            return recordTypesByDeveloperNameByObject;
        }
        private set;
    }

    public static Map<String,RecordType> getRecordTypesForObject(String objectName) {
        if (recordTypesByDeveloperNameByObject.containsKey(objectName)) {
            return recordTypesByDeveloperNameByObject.get(objectName);
        }
        return new Map<String,RecordType>();
    }

    public static Map<String,RecordType> getPersonAccountRecordTypes() {
        if (recordTypesByDeveloperNameByObject.containsKey('Account')) {
            Map<String, RecordType> personAccountRecordTypes = recordTypesByDeveloperNameByObject.get('Account').clone();
            for(String recordTypeName : personAccountRecordTypes.keySet()){
                if(!personAccountRecordTypes.get(recordTypeName).IsPersonType){
                    personAccountRecordTypes.remove(recordTypeName);
                }
            }
            return personAccountRecordTypes;
        }
        return new Map<String,RecordType>();
    }

    private static Map<String, Map<Id, RecordType>> recordTypesByIdByObject {
        get {
            if (recordTypesByIdByObject == null) {
                recordTypesByIdByObject = new Map<String, Map<Id, RecordType>>();
                for (RecordType recType : recordTypeList) {
                    String theObject = recType.sObjectType;
                    if (!recordTypesByIdByObject.containsKey(theObject)) {
                        recordTypesByIdByObject.put(theObject, new Map<Id, RecordType>());
                    }
                    recordTypesByIdByObject.get(theObject).put(recType.Id, recType);
                }
            }
            return recordTypesByIdByObject;
        }
        set;
    }

    public static Map<Id, RecordType> getRecordTypesByIdForObject(String objectName) {
        if (recordTypesByIdByObject.containsKey(objectName)) {
            return recordTypesByIdByObject.get(objectName);
        }
        return new Map<Id, RecordType>();
    }

    public static Map<Id, RecordTypeLocalization> recordTypeLocalizations {
        get {
            if (recordTypeLocalizations == null) {
                recordTypeLocalizations = new Map<Id, RecordTypeLocalization>();
                for (RecordTypeLocalization rtLocalization : [SELECT Id, Value, ParentId
                                                                FROM RecordTypeLocalization
                                                               WHERE Language = :UserInfo.getLanguage()]) {
                    recordTypeLocalizations.put(rtLocalization.ParentId, rtLocalization);
                }
            }
            return recordTypeLocalizations;
        }
        private set;
    }

    public static Boolean isAnonymizing {
        get {
            if (isAnonymizing == null) {
                isAnonymizing = false;
            }
            return isAnonymizing;
        }
        set;
    }

    public static Boolean knowledgeInstalled {
        get {
            if (knowledgeInstalled == null) {
                knowledgeInstalled = true;
                try {
                    SObject cs = globalDescribe.get('CaseArticle').newSObject();
                } catch (Exception e) {
                    knowledgeInstalled = false;
                }
            }
            return knowledgeInstalled;
        }
        private set;
    }

    @TestVisible
    public static Boolean isReadOnlyUser {
        get {
            if (isReadOnlyUser == null) {
                isReadOnlyUser = !Schema.sObjectType.Case.isUpdateable();
            }
            return isReadOnlyUser;
        }
        private set;
    }

    public static DCRGlobalSettingMVN dcrGlobalSetting {
        get {
            if (dcrGlobalSetting == null) {
                dcrGlobalSetting = new DCRGlobalSettingMVN();
            }

            return dcrGlobalSetting;
        }
        private set;
    }

    public static Enhanced_Account_Search_Settings_MVN__c accountSearchSettings {
        get {
            if (accountSearchSettings == null) {
                accountSearchSettings = Enhanced_Account_Search_Settings_MVN__c.getInstance();
            }

            return accountSearchSettings;
        }
        private set;
    }

    public static Boolean matchCaseRecordTypeIdToNameMVN(Id recordTypeId, String recordTypeName){
        if(String.isNotBlank(recordTypeName)){
            recordTypeName = recordTypeName.trim();
            return getRecordTypesForObject('Case').get(recordTypeName).Id == recordTypeId;
        }
        return false;
    }

    public static List<String> splitCommaSeparatedString(String stringToSplit){
        return splitString(stringToSplit,',');
    }

    public static List<String> splitString(String stringToSplit, String stringToSplitBy){
        if(String.isBlank(stringToSplit)){
            return new List<String>();
        } else{
            List<String> splitStrings = stringToSplit.split(stringToSplitBy);
            List<String> stringsToReturn = new List<String>();
            for(String splitString : splitStrings){
                stringsToReturn.add(splitString.trim());
            }
            return stringsToReturn;
        }
    }

    public static String sanitizeString(String stringToSanitize) {
        if (stringToSanitize != null) {
            return String.escapeSingleQuotes(stringToSanitize);
        }
        return stringToSanitize;
    }

    /**
    * Clone Objects
    * @description: Clones an arbitrary object(s). Does not insert it in case changes need to be made first.
    */
    public static List<SObject> cloneObjects(String objectName, String whereClause){
        List<SObject> originals = getClonableObjects(objectName,whereClause);

        return originals.deepClone(false, false, false);
    }

    /**
    * Get Clonable Objects
    * @description: Sometimes you need the Ids from the originals before you clone
    *               This gets a list of objects with every writable field queried so that a full clone can be done.
    */
    public static List<SObject> getClonableObjects(String objectName, String whereClause){

        String selects = '';

        if (whereClause == null || whereClause == ''){ return null; }

        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = globalDescribe.get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();

        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }

        if (!selectFields.isEmpty()){
                selects = String.join(selectFields,',');

        }
        String queryString = 'SELECT ' + String.escapeSingleQuotes(selects) +
                            ' FROM ' + String.escapeSingleQuotes(objectName) +
                            ' WHERE ' + String.escapeSingleQuotes(whereClause);
        System.debug(queryString);

        return Database.query(queryString);
    }

    /**
    * Return as string
    * @description: Returns an object parsed from JSON as a string.
    *               Detects the type and converts to a string
    */
    public static String returnAsString(Object valAsObj){
        String value = '';
        if ( valAsObj == null ) {
            value = '';
        } else if ( valAsObj instanceOf List<Object> ) {
            List<String> strList = new List<String>();

            for (Object eachVal : (List<Object>)valAsObj) {
                strList.add((String)eachVal);
            }
            value  = String.join( strList, ';');
        } else {
            value = JSON.serialize(valAsObj);
        }

        return value;
    }

    /**
    * Get Related Field Value
    * @description: Takes a relationship field string such as Account_MVN__r.name and gets the value of Name from the source object
    *               Source object must have queried for the related field already
    */
    public static Object getRelatedFieldValue(String fieldString, SObject source) {
        Object value ;
        SObject relatedObject = source;
        List<String> fields = UtilitiesMVN.splitString(fieldString, '\\.');
        for(Integer i = 0; i < fields.size(); i++) {
            if(relatedObject != null) {
                try {
                    if((i+1) < fields.size()) {
                        relatedObject = relatedObject.getSObject(fields[i]);
                    } else {
                        value = relatedObject.get(fields[i]);
                    }
                } catch (Exception e) {
                    //Errors when the field wasn't queried or isn't available
                    return null;
                }
            }
        }
        return value;
    }

    private static final Integer SFDC_SOQL_LIMIT = 200;
    public static Integer getSearchLimit() {
        Integer searchLimit = Integer.valueOf(accountSearchSettings.Account_Search_Max_Results_MVN__c);

        if(searchLimit == null || searchLimit < 1) {
            searchLimit = 50;
        }

        return Math.min(searchLimit, SFDC_SOQL_LIMIT);
    }

    public static Map<String, Schema.SObjectType> globalDescribe {
        get {
            if (globalDescribe == null) {
                globalDescribe = Schema.getGlobalDescribe();
            }
            return globalDescribe;
        }
        private set;
    }

    public static Schema.DisplayType getDisplayType(String objectName, String fieldName) {
        return globalDescribe.get(objectName)
                             .getDescribe()
                             .fields.getMap()
                             .get(fieldName)
                             .getDescribe()
                             .getType();
    }

    public static Map<String, AccountSearchIntfMVN> accountSearchUtilities {
        get {
            if (accountSearchUtilities == null) {
                accountSearchUtilities = new Map<String, AccountSearchIntfMVN>();
                for (String utilityClass: UtilitiesMVN.splitCommaSeparatedString(accountSearchSettings.Account_Search_Handler_Classes_MVN__c)) {
                    try {
                        if(utilityClass != 'None') {
                            Type t = Type.forName(utilityClass);
                            AccountSearchIntfMVN utilityInstance = (AccountSearchIntfMVN) t.newInstance();
                            accountSearchUtilities.put(utilityClass, utilityInstance);
                        }
                    } catch(Exception e) {
                        ApexPages.addMessages(e);
                    }
                }
            }
            return accountSearchUtilities;
        }
        set;
    }

    public static List<Messaging.SendEmailResult> emailAdministrators(String subject, String body) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        Profile profile = [SELECT Id, Name FROM Profile WHERE Name='System Administrator' LIMIT 1];

        List<String> adminEmails = new List<String>();
        for (User user : [SELECT Id, Email FROM User WHERE ProfileId = :profile.Id]) {
            adminEmails.add(user.Email);
        }

        mail.setToAddresses(adminEmails);
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        mail.setBccSender(false);
        mail.setUseSignature(false);

        try {
            Messaging.reserveSingleEmailCapacity(1);
            return Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } catch (System.NoAccessException ex) {
            return new List<Messaging.SendEmailResult>();
        }
    }

    /**
    * customPermissionsForCurrentUser
    * @description static set of custom permission API names that the current user has
    */
    private static Set<String> customPermissionsForCurrentUser;

    /**
    * hasBypassValidation
    * @description Checks if the running user has the Bypass_Validation_MVN permission
    * @return Boolean if the user has Bypass_Validation_MVN. True means the user has the permission.
    */
    public static Boolean hasBypassValidation() {
        return hasCustomPermission('Bypass_Validation_MVN');
    }

    /**
    * hasCustomPermission
    * @description Checks if the running user has a given custom permission
    * @param API name of the permission to check if the user has it
    * @return Boolean informing if the user has the permission
    * @example
    * hasCustomPermission('Bypass_Validation_MVN');
    */
    public static Boolean hasCustomPermission(String permissionToCheck) {
        if(customPermissionsForCurrentUser == null) {
            customPermissionsForCurrentUser = new Set<String>();

            // Query the full set of Custom Permissions
            Map<Id, String> customPermissionNamesById = new Map<Id, String>();
            List<CustomPermission> customPermissions = [SELECT Id, DeveloperName FROM CustomPermission];
            for(CustomPermission customPermission : customPermissions) {
                customPermissionNamesById.put(customPermission.Id, customPermission.DeveloperName);
            }

            // Query to determine which of these custome settings are assigned to this user
            List<SetupEntityAccess> setupEntities =
                [SELECT SetupEntityId
                    FROM SetupEntityAccess
                    WHERE SetupEntityId IN :customPermissionNamesById.keySet()
                    AND ParentId
                            IN (SELECT PermissionSetId
                                FROM PermissionSetAssignment
                                WHERE AssigneeId = :UserInfo.getUserId())];
            for(SetupEntityAccess setupEntity : setupEntities) {
                customPermissionsForCurrentUser.add(customPermissionNamesById.get(setupEntity.SetupEntityId));
            }
        }

        return customPermissionsForCurrentUser.contains(permissionToCheck);
    }

    private static Map<String, Schema.DescribeSObjectResult> sObjectNameToDescribe = new Map<String, Schema.DescribeSObjectResult>();

    public static Schema.DescribeSObjectResult getSobjectDescribe(Schema.SobjectType sObjectType) {
        String sobjectName = String.valueOf(sObjectType);

        if (!sObjectNameToDescribe.containsKey(sobjectName)) {
            sObjectNameToDescribe.put(sobjectName, sObjectType.getDescribe());
        }

        return sObjectNameToDescribe.get(sobjectName);
    }

}