@isTest   // make this class visible to Test Runtime. 
public class EligibilitySuccessHttpCalloutMock implements HttpCalloutMock {
    
    // we need to implement the "respond" method of interface.
    public HttpResponse respond(HttpRequest httpReq)
    {
        HttpResponse httpRes = new HttpResponse(); 
        httpRes.setHeader('Content-Type', 'application/json'); 
        httpRes.setStatusCode(200); 
        return httpRes; 
    }
}