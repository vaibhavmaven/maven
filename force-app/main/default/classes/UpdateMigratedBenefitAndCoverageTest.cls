@isTest
public class UpdateMigratedBenefitAndCoverageTest{
    
    public static Map<String, RecordType> accountRecordTypesByName {
        get {
            if (accountRecordTypesByName == null) {
                accountRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
                    accountRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return accountRecordTypesByName;
        }
        private set;
    }        
    
    public static List<Account> createPatients(Integer numOfPatients) {
        List<Account> patientsList = new List<Account>();
        
        for (Integer i=0; i<numOfPatients; i++) {
            Account patient= new Account();
            patient.FirstName = 'Test';
            patient.LastName = 'Patient' + i;
            System.debug(patient.LastName);
            patient.RecordTypeId = accountRecordTypesByName.get('Patient_MVN').id;
            patientsList.add(patient);
        }
        
        insert patientsList;
        return patientsList;
   	}    
    
    @testSetup
    private static void setup(){
	    Integer testSize = 6;        
        // create program 
        Program_MVN__c program = new Program_MVN__c();
        insert program;
        //create members 
        List<Account> patientsList = createPatients(testsize); 	
        system.debug('Created Patient Accounts : ' + patientsList); 
		// create programmembers
        List<Program_Member_MVN__c> pgmemberList = new List<Program_Member_MVN__c>{}; 
        for(Account acc : patientsList){
            Program_Member_MVN__c programMember = new Program_Member_MVN__c();
            programMember.Program_MVN__c = program.id;
            programMember.Member_MVN__c = acc.id;
            programMember.Do_Not_Initialize_MVN__c = false;  
            pgmemberList.add(programMember);             
        }
        insert pgmemberList; 		
        // create applications 
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        List<Application_MVN__c> applicationList = new List<Application_MVN__c>();        
        for (Program_Member_MVN__c prgmem : pgmemberList) {
            applicationList.add(applicationFactory.construct(new Map<String, Object>{'Program_Member_MVN__c' => prgmem.Id}));
        }
        insert applicationList;         
        // create benefits and coverages
        List<Benefits_Coverage_MVN__c> bncList = new List<Benefits_Coverage_MVN__c>{}; 
        for(Integer counter = 0; counter < testSize; counter++ )
        {
            Benefits_Coverage_MVN__c benefitCoverage= new  Benefits_Coverage_MVN__c();
            benefitCoverage.Program_Member_MVN__c = pgmemberList[counter].Id;
            benefitCoverage.Application_MVN__c = applicationList[counter].Id;
            bncList.add(benefitCoverage);
        }
        insert bncList;              
        // link patient insurances to benefits and coverages. 
		List<Patient_Insurance__c> patientInsrList = new List<Patient_Insurance__c>{}; 	
            List<String> planTypeList = new List<String>{'Commercial', 'Medicaid', 'Medicare', 'Medicare Supplement', 
                                                       'Other State & Government', 'Uninsured'}; 
		for (Integer counter = 0; counter < testSize; counter++){
		    Patient_Insurance__c patientInsr = new Patient_Insurance__c(); 
			patientInsr.Person_Account__c = patientsList[counter].ID; 	                     //from above loop 
			patientInsr.Plan_Type__c = planTypeList[counter]; 						 // plan type commercial 
			patientInsr.Plan_Subtype__c = 'EPO';							 // plan subtype commercial 
			patientInsr.Relationship_to_Policyholder__c = 'Self';			 // relationship self 
			patientInsr.Status__c = 'Active';								 // status = 'Acitve'
			patientInsr.Policy_Holder_Last_Name__c = patientsList[counter].LastName;			 // policy holder last name is compulsory
	        patientInsr.GroupID__c = bncList[counter].ID; 
            patientInsrList.add(patientInsr); 
		}
        insert patientInsrList; 
        
		system.debug('Created patientinsurance list : ' + patientInsrList); 	
	}
    
    @istest
    private static void testbatchExecution(){
        Test.startTest();
        UpdateMigratedBenefitAndCoverage obj = new UpdateMigratedBenefitAndCoverage(); 
        ID jobID = Database.executeBatch(obj);
        Test.stopTest();
    }


}