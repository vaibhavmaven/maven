/*
 * AccountSearchRqstMVN.cls
 * @created By: Kyle Thornton
 * @created Date: June 2016
 * @edited By: Pablo Roldan
 * @edited Date: November 2017
 * @description: This class test AccountSearchRqstsMVN
 * @edit description: Adapted for Patient Support.
 */
@isTest
private class AccountSearchRqstTestMVN
{
	static List<String>             externalIds;
    static AccountSearchRqstMVN    request;
    static User                     testUser;
    static AccountFieldSettingsMVN searchSettings;
    static AccountFieldSettingsMVN resultSettings;
    static Boolean                  populateSearchTermResult;

    static TestFactoryUserMVN userFactory = new TestFactoryUserMVN();

    static {
        TestFactoryCustomMetadataMVN.setMocks();
        testUser = userFactory.create();
    }

    /**
    * It should instantiate a new request MVN with a list of External Ids
    * @description: Unit tests for constructors
    */
    @isTest
	static void itShouldInstantiateANewRequestMVNWithAListOfExternalIds()
	{
		 System.runAs(testUser) {
            Test.startTest();
            givenAListOfExternalIdStrings();
            whenTheSearchRequestIsConstructedUsingThoseIds();
            thenAccountExernalIdsIsPopulated();
            Test.stopTest();
        }
	}
    
    @isTest
    static void itShouldInstantiateANewRequestMVNWithSearchSettingsResultSettingsAndCountry() {
        System.runAs(testUser) {
            Test.startTest();
            givenSearchFieldSettingsAndResultFieldSettings();
            whenTheSearchRequestIsConstructedWithFieldSettings();
            thenAccountCountryIsSetAndSettingsAreSet();
            Test.stopTest();
        }
    }

    /**
    * It should indicate when fields are not populated
    * @description: Unit tests for noSearchFieldsPopulated
    */
    @isTest
    static void itShouldIndicateWhenFieldsAreNotPopulated() {
        System.runAs(testUser) {
            Test.startTest();
            givenSearchFieldSettingsAndResultFieldSettings();
            whenTheSearchRequestIsConstructedWithFieldSettings();
            thenNoSearchFieldsPopulatedIs(true); //since nothing has been entered yet
            Test.stopTest();
        }
    }

    @isTest
    static void itShouldIndicateThatSomeFieldsArePopulated() {
        System.runAs(testUser) {
            Test.startTest();
            givenSearchFieldSettingsAndResultFieldSettings();
            whenTheSearchRequestIsConstructedWithFieldSettings();
            whenLastNameSearchTermHasBeenPopulatedWith('Maven');
            thenNoSearchFieldsPopulatedIs(false);
            Test.stopTest();
        }
    }

    @isTest
    static void itShouldIndicateWhenAddressFieldsArePopulated() {
        TestFactoryAccountFieldSettingsMVN.setPersonPhoneMock();
        System.runAs(testUser) {
            Test.startTest();
            givenSearchFieldSettingsAndResultFieldSettings();
            whenTheSearchRequestIsConstructedWithFieldSettings();
            whenPhoneSearchTermHasBeenPopulated();
            thenNoSearchFieldsPopulatedIs(false);
            Test.stopTest();
        }
    }

    @isTest
    static void itShouldIndicateWhenSearchTermsAreTooShort() {
        System.runAs(testUser) {
            Test.startTest();
            givenSearchFieldSettingsAndResultFieldSettings();
            whenTheSearchRequestIsConstructedWithFieldSettings();
            whenLastNameSearchTermHasBeenPopulatedWith('M');
            thenAnExceptionIsThrownForSearchTermTooShort();
            Test.stopTest();
        }
    }

    @isTest
    static void itShouldIndicateSearchTermsPopulatedIfABooleanIsTrue() {
        TestFactoryAccountFieldSettingsMVN.setPersonPhoneMock(); //should include primary checkbox
        System.runAs(testUser) {
            Test.startTest();
            givenSearchFieldSettingsAndResultFieldSettings();
            whenTheSearchRequestIsConstructedWithFieldSettings();
            whenPrimaryPhoneSearchTermIsChecked();
            thenNoSearchFieldsPopulatedIs(false);
            Test.stopTest();
        }
    }

    @isTest
    static void itShouldSkipBlankFieldsOnNoSearchFieldPopulated() {
        TestFactoryAccountFieldSettingsMVN.setInstitutionBlankMock();
        System.runAs(testUser) {
            Test.startTest();
            givenSearchFieldSettingsAndResultFieldSettings();
            whenTheSearchRequestIsConstructedWithFieldSettings();
            //we are using setInstitutionBlankMock which has just a blank field configured
            //just looking to cover the continue statement in noSearchFieldsPopulated
            thenNoSearchFieldsPopulatedIs(true);
            Test.stopTest();
        }
    }

    /**
    * It should populated a search field if it exists
    * @description: Unit tests for populateSearchFieldIfExists
    */
    @isTest
    static void itShouldPopulateASearchFieldIfItExists() {
        System.runAs(testUser) {
            Test.startTest();
            givenSearchFieldSettingsAndResultFieldSettings();
            whenTheSearchRequestIsConstructedWithFieldSettings();
            whenFirstAndPhoneAreConfiguredAndPassedForPrepopulation();
            thenTheSearchTermsAreSetCorrectly();
            Test.stopTest();
        }
    }

    @isTest
    static void itShouldReturnFalseIfPrepopulateFieldDoesntExist() {
        System.runAs(testUser) {
            Test.startTest();
            givenSearchFieldSettingsAndResultFieldSettings();
            whenTheSearchRequestIsConstructedWithFieldSettings();
            whenANonConfiguredFieldIsPassedIn();
            thenPopulateSearchFieldIfExistsReturnsFalse();
            Test.stopTest();
        }
    }

    /**
    * It should be able to store values for various variables
    * @description: Unit tests to cover other variables that aren't covered above
    */
    @isTest
    static void itShouldBeAbleToStoreValuesForVariousVariables() {
        System.runAs(testUser) {
            Test.startTest();
            givenSearchFieldSettingsAndResultFieldSettings();
            whenTheSearchRequestIsConstructedWithFieldSettings();

            request.isPersonSearch = true;
            request.recordType = 'aRectype';
            request.restrictToAccountId = 'someId';

            System.assertEquals(true,request.isPersonSearch);
            System.assertEquals('aRectype',request.recordType);
            System.assertEquals('someId',request.restrictToAccountId);

            Test.stopTest();
        }
    }

    static void givenAListOfExternalIdStrings() {
        externalIds = new List<String>{'k7jJ1r7edU',
                                       'pwSpBmOWAo',
                                       'JtwTAl62di',
                                       '2d7fDwGE2J',
                                       'ec451DV6ge',
                                       'W15LALgcav',
                                       'SQNmi2pX2i',
                                       'ptRWgtrcDu',
                                       'GRl4ykFU2G',
                                       'g3UlYF05c5'};
    }

    static void givenSearchFieldSettingsAndResultFieldSettings() {
        searchSettings = new AccountFieldSettingsMVN('US', 'HCP', 'Search_Field_Order_MVN__c', false);
        resultSettings = new AccountFieldSettingsMVN('US', 'HCP', 'Search_Results_Order_MVN__c', false);
    }

    static void whenTheSearchRequestIsConstructedUsingThoseIds() {
        request = new AccountSearchRqstMVN(externalIds);
    }

    static void whenTheSearchRequestIsConstructedWithFieldSettings() {
        request = new AccountSearchRqstMVN(searchSettings, resultSettings, 'US');
    }

    static void whenLastNameSearchTermHasBeenPopulatedWith(String term) {
        request.account.LastName = term;
    }

    static void whenPhoneSearchTermHasBeenPopulated() {
        request.address.Phone_vod__c = '5555555555';
    }

    static void whenFirstAndPhoneAreConfiguredAndPassedForPrepopulation() {
        Map<String, String> prepopConfig = new Map<String, String> {
            'firstname' => 'Account',
            'phone_vod__c' => 'Address_vod__c'
        };
        request.populateSearchFieldIfExists('FirstName', 'Manny', prepopConfig);
        request.populateSearchFieldIfExists('Phone_vod__c', '555555555', prepopConfig);
    }

    static void whenPrimaryPhoneSearchTermIsChecked() {
        request.address.primary_vod__c = true;
    }

    static void whenANonConfiguredFieldIsPassedIn() {
        populateSearchTermResult = request.populateSearchFieldIfExists('noField', 'whateverValue', new Map<String, String>());
    }

    static void thenAccountExernalIdsIsPopulated() {
        System.assertEquals(externalIds, request.accountExternalIds);
    }

    static void thenAccountCountryIsSetAndSettingsAreSet() {
        System.assertEquals('US', request.account.Country_MVN__c);
        System.assertEquals(searchSettings, request.searchFieldSettings);
        System.assertEquals(resultSettings, request.resultFieldSettings);
    }

    static void thenNoSearchFieldsPopulatedIs(Boolean expectedValue) {
        System.assertEquals(expectedValue, request.noSearchFieldsPopulated(searchSettings));
    }

    static void thenAnExceptionIsThrownForSearchTermTooShort() {
        Boolean exceptionCaught = false;
        try {
            request.noSearchFieldsPopulated(searchSettings);
        } catch (Exception ex) {
            exceptionCaught = true;
            System.assertEquals(Label.Search_Term_Must_Have_2_Characters_MVN, ex.getMessage());
        }
        System.assert(exceptionCaught);
    }

    static void thenTheSearchTermsAreSetCorrectly() {
        System.assertEquals('Manny', request.account.FirstName);
        System.assertEquals('555555555', request.address.Phone_vod__c);
    }

    static void thenPopulateSearchFieldIfExistsReturnsFalse() {
        System.assert(!populateSearchTermResult);
    }
}