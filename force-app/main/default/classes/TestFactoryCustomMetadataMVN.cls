/**
 * TestFactoryCustomMetadataMVN
 * @author: Pablo Roldan
 * @created Date: October 2017
 * @description: This class is used to construct any custom metadata type
 */
@isTest
public class TestFactoryCustomMetadataMVN {
    
    public static SObject createMetadata(String sObjectName, Map<String, Object> fields) {
        Map<String, Object> jsonFields = new Map<String, Object> {
            'attributes' => new Map<String, Object> {
                'type' => sObjectName
            }
        };

        if(fields != null) {
            jsonFields.putAll(fields);
        }

        return (SObject) JSON.deserialize(JSON.serialize(jsonFields), SObject.class);
    }

    public static void setMocks() {
        TestFactoryAccountFieldSettingsMVN.setPersonAccountMock();

        TestFactoryAccountTypeSettingsMVN.setMock();

        //calling this for good measure. already called many other times
        TestFactorySegmentsMVN.createMockSegments();

        TestFactoryDCRGlobalSettingMVN.setMock();

        TestFactoryMDMConnectionsMVN.setMock();

        TestFactoryMDMFieldMappingMVN.setMocks();
    
        TestFactoryMDMValueMappingMVN.setMocks();

        TestFactoryVeevaNetworkSettingsMVN.setMock();

        TestFactoryVeevaNetworkStatusSettingsMVN.setMock();
    }
}