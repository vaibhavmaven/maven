/**
 * @author      Mavens
 * @date        December 2018
 * @group       ReenrollmentBatchJob
 * @description Unit tests for ReenrollmentBatchJobMVN
 */
@isTest
private class ReenrollmentBatchJobTestMVN
{
    static Program_MVN__c program;
    static Program_Stage_MVN__c auditStage;
    static List<Program_Member_MVN__c> programMembers;
    static List<Program_Member_Stage_MVN__c> pmStages;
    static List<Application_MVN__c> applications;
    static List<Case> cases;
    static Patient_Service_Settings_MVN__c settings;

    static final Integer DATA_SIZE = 10;

    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        return programFactory.create(
            new Map<String,Object>{
                'Name' => 'A Program',
                'Program_Id_MVN__c' => 'Knipper_PAP_MVN',
                'Active_MVN__c' => true,
                'Job_No_MVN__c' => '12345',
                'Daily_Audit_MVN__c' => 50
            }
        );
    }

    static Program_Stage_MVN__c createAuditStages() {
        Program_Stage_MVN__c stage = new Program_Stage_MVN__c (
            Name = 'Audit Request',
            Program_MVN__c = program.Id
        );
        insert stage;
        return stage;
    }

    static List<Program_Member_MVN__c> createProgramMembers() {
        return TestDataFactoryMVN.createProgramMembers(program, auditStage.Id, DATA_SIZE);
    }

    static List<Application_MVN__c> createApplications() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        List<Application_MVN__c> testApplications = new List<Application_MVN__c>();
        for (Program_Member_MVN__c programMember : programMembers) {
            testApplications.add(
                applicationFactory.construct(new Map<String, Object>{
                    'Program_Member_MVN__c' => programMember.Id,
                    'Status_MVN__c' => 'Fulfilled',
                    'Application_Reminder_Date_MVN__c' => Date.today().addDays(-10),
                    'Application_Reminder_Follow_Up_Date_MVN__c' => Date.today().addDays(-5)
                })
            );
        }
        insert testApplications;

        return testApplications;
    }

    static void createNintexPackages() {
        List<Loop__DDP__c> nintexPackages = new List<Loop__DDP__c>();
        nintexPackages.add(
            new Loop__DDP__c(
                Name = 'test package',
                Loop__Object_Name__c = 'Program_Member_MVN__c',
                Loop__Filter__c = 'Knipper_PAP_MVN-PM-Re-Enrollment'
            )
        );
        nintexPackages.add(
            new Loop__DDP__c(
                Name = 'test package',
                Loop__Object_Name__c = 'Program_Member_MVN__c',
                Loop__Filter__c = 'Knipper_PAP_MVN-PM-Re-Enrollment-Followup'
            )
        );

        insert nintexPackages;

        List<Loop__DDP_Integration_Option__c> ddpOptions = new List<Loop__DDP_Integration_Option__c>();
        ddpOptions.add(
            new Loop__DDP_Integration_Option__c(
                Loop__DDP__c = nintexPackages[0].Id,
                RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c
                                    .getRecordTypeInfosByName()
                                    .get('Attach').getRecordTypeId(),
                Loop__Attach_As__c = 'SFTP'
            )
        );
        ddpOptions.add(
            new Loop__DDP_Integration_Option__c(
                Loop__DDP__c = nintexPackages[1].Id,
                RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c
                                    .getRecordTypeInfosByName()
                                    .get('Attach').getRecordTypeId(),
                Loop__Attach_As__c = 'SFTP'
            )
        );

        insert ddpOptions;
    }

    static {
        createNintexPackages();
        settings = TestDataFactoryMVN.buildPSSettings();
        settings.Name = 'default';
        settings.SetupOwnerId=UserInfo.getOrganizationId();
        insert settings;
        program = createProgram();
        auditstage = createAuditStages();
        programMembers = createProgramMembers();
        applications = createApplications();
    }

    @isTest
    static void itShouldBeScheduleable() {
        Test.startTest();
        System.schedule(
            'Test reenrollment schedule',
            '0 0 * * * ?',
            new ReenrollmentBatchJobMVN()
        );
        Test.stopTest();

        System.assertEquals(1, [
            SELECT
                Count()
            FROM
                CronJobDetail
            WHERE
                Name = 'Test reenrollment schedule'
        ]);
    }

    @isTest
    static void itShouldSendLetters() {
        Test.startTest();
        ReenrollmentBatchJobMVN.run();
        Test.stopTest();
    }
}