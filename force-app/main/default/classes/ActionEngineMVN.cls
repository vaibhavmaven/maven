/**
 * @author Mavens
 * @description action engine class
 * @group ActionEngine
 */
public with sharing class ActionEngineMVN {

    public static String PASSEDACTION = 'Passed_Action_MVN';
    public static String DENIEDACTION = 'Denied_Action_MVN';
    public static String MISSINGINFOACTION = 'Missing_Information_Action_MVN';
    public static String ENROLMENTACTION = 'Enrollment_And_Renewal_Action_MVN';
    public static String BENEFITACTION = 'Benefit_And_Investigation_Action_MVN';
    public static String ORDERACTION = 'Order_Creation_Action_MVN';
    public static String AUDITACTION = 'Audit_Action_MVN';
    public static String APPEALACTION = 'Appeal_Action_MVN';
    public static String MISSINGINFOPASSEDACTION= 'Missing_Information_Passed_Action_JKC';

    private static final Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();
    private Program_Member_MVN__c programMember;

    /**
     * Constructor that assigns the program member record passed as parameter
     * @param  programMember program Member record
     */
    public ActionEngineMVN(Program_Member_MVN__c programMember) {
        this.programMember = programMember;
    }

    /**
     * Constructor that retrieves the program member record
     * @param  programMemberId program Member Id
     */
    public ActionEngineMVN(Id programMemberId) {
        this.programMember = [
            SELECT
                Id,
                Active_Application_MVN__c,
                Program_Id_MVN__c,
                Program_MVN__c
            FROM
                Program_Member_MVN__c
            WHERE
                Id = :programMemberId
        ];
    }

    /**
     * Based on the action name retrieved from the database (custom metadata) the actions to perfom
     * and run them. The actions are:
     *   - Close stage
     *   - Open next one and create it if it does not exist
     * @param  actionDevName developer name for the action to perform
     */
    public void run(String actionDevName) {

        Action_Engine_Setting_MVN__mdt setting = ActionEngineSettingsMVN.getSettingByProgramIdAndActionDevName(
            this.programMember.Program_Id_MVN__c, actionDevName);

        if (setting != null) {

            Program_Member_Stage_MVN__c activeStage = getProgramMemberStage(setting.Child_Stage_Name_To_Close_MVN__c.trim());

            // Check that the stage defined in the setting to be closed is active and started
            if (activeStage != null && activeStage.Status_MVN__c == 'Started') {

                if (String.isNotBlank(setting.Next_Stage_Name_MVN__c)) {

                    Program_Member_Stage_MVN__c nextStage = getProgramMemberStage(setting.Next_Stage_Name_MVN__c.trim());

                    // If the next stage doesn't exist on the program member or is already closed,
                    // then create new Stage and add it to the Program Member
                    if (nextStage == null || nextStage.IsClosed_MVN__c) {
                        this.addStage(setting.Next_Stage_Name_MVN__c);
                        nextStage = getProgramMemberStage(setting.Next_Stage_Name_MVN__c.trim());
                        this.startStage(nextStage.Id);
                    // If the next stage is not the next in the sequence, then move it
                    } else if (activeStage.Stage_Sequence_Number_MVN__c + 1 != nextStage.Stage_Sequence_Number_MVN__c) {
                            this.startStage(nextStage.Id);
                    }
                }
                // Close active stage
                this.closeCaseForStage(activeStage);
            }
        }
    }

    /**
     * Retrieves the program member stage record
     * @param  stageName name of the program member stage
     * @return Program Member Stage record
     */
    private Program_Member_Stage_MVN__c getProgramMemberStage(String stageName) {
        List<Program_Member_Stage_MVN__c> programMemberStages = [
            SELECT
                Id,
                Program_Stage_Name_MVN__c,
                Status_MVN__c,
                IsClosed_MVN__c,
                Activity_ID_MVN__c,
                Stage_Sequence_Number_MVN__c
            FROM
                Program_Member_Stage_MVN__c
            WHERE
                Program_Member_MVN__c = :programMember.Id
           AND
                Application_MVN__c = :programMember.Active_Application_MVN__c
            AND
                Program_Stage_Name_MVN__c = :stageName
            ORDER BY
                Stage_Sequence_Number_MVN__c
        ];
        // We return the one that is open
        for (Program_Member_Stage_MVN__c pmStage : programMemberStages) {
            if (!pmStage.IsClosed_MVN__c) {
                return pmStage;
            }
        }
        // If there is no one open, then we return the last in the seq number that was closed
        if (!programMemberStages.isEmpty()) {
            return programMemberStages.get(programMemberStages.size()-1);
        }
        return null;
    }

    /**
     * Close Case
     * @param  programMemberStage Program Member Stage that is the parent of the case to be closed
     */
    private void closeCaseForStage(Program_Member_Stage_MVN__c programMemberStage) {
        try {
            Case caseToClose = new Case(
                Id = programMemberStage.Activity_ID_MVN__c,
                Status = 'Closed'
            );
            update caseToClose;
        } catch(Exception e) {
            System.debug('ActionEngineMVN-closeCaseForStage :: ' + e);
        }

    }

    /**
     * Add new Stage to the program member
     * @param  stageName name of the stage to add
     */
    private void addStage(String stageName) {
        Program_Stage_MVN__c programStageToAdd = [
            SELECT
                Id
            FROM
                Program_Stage_MVN__c
            WHERE
                Program_MVN__c = :programMember.Program_MVN__c
            AND
                Name = :stageName
            LIMIT
                1
        ];
        ProgramMemberInstantiatorMVN.addStageForProgramMember(programStageToAdd.Id, this.programMember);
    }

    /**
     * Start program member stage.
     * If the stage is NOT the next one to the one that was closed, then we need to move it.
     * @param  startStageId id of program member stage to be started
     */
    private void startStage(Id startStageId) {
        List<Program_Member_Stage_MVN__c> updatePMSs = new List<Program_Member_Stage_MVN__c>();
        // used to readjust the stage sequence numbers
        Double startStageSeqNum;
        Double unstartedStageSeqNum;

        List<Program_Member_Stage_MVN__c> openMemberParentStages = [SELECT Id,
                                                                            Initiated_Date_MVN__c,
                                                                            Status_MVN__c,
                                                                            Stage_Sequence_Number_MVN__c
                                                                        FROM Program_Member_Stage_MVN__c
                                                                        WHERE Program_Member_MVN__c = :programMember.Id
                                                                        AND IsClosed_MVN__c = false
                                                                        AND Parent_Program_Member_Stage_MVN__c = null
                                                                    ORDER BY Stage_Sequence_Number_MVN__c];

        for(Program_Member_Stage_MVN__c parentMemberStage : openMemberParentStages) {
            if (parentMemberStage.Initiated_Date_MVN__c == null) {
                //found the starting point
                startStageSeqNum = parentMemberStage.Stage_Sequence_Number_MVN__c;
                unstartedStageSeqNum = parentMemberStage.Stage_Sequence_Number_MVN__c + 1;
                break;
            }
        }

        for(Program_Member_Stage_MVN__c parentMemberStage : openMemberParentStages) {
            if(parentMemberStage.Id == startStageId) {
                parentMemberStage.Status_MVN__c = settings.Program_Member_Stage_Start_Status_MVN__c;
                parentMemberStage.Initiated_Date_MVN__c = System.today();
                parentMemberStage.Stage_Sequence_Number_MVN__c = startStageSeqNum;
                updatePMSs.add(parentMemberStage);
            } else if(parentMemberStage.Initiated_Date_MVN__c == null) {
                parentMemberStage.Stage_Sequence_Number_MVN__c = unstartedStageSeqNum;
                unstartedStageSeqNum++;
                updatePMSs.add(parentMemberStage);
            }
        }

        Savepoint sp = Database.setSavepoint();
        try {
            update updatePMSs;
            ProgramMemberStageManagerMVN.initializeParentStage(this.programMember.Id, startStageId);
        } catch(Exception e){
            System.debug('\n\n\n### ' + e.getStackTraceString() + '\n\n\n');
            Database.rollback(sp);
        }
    }
}