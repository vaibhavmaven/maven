/**
 * @author Mavens
 * @group Cache
 * @description Interface to define caches in Apex
 */
public interface CacheManagerMVN {

    /**
    * @description Method used to change the value of the cache enabled property
    * @result   cache enabled property after change it
    * @example
    * * Boolean cacheEnabled = myCacheConnection.toggleEnabled();
    */
    Boolean toggleEnabled();

    /**
    * @description Get from cache the value from the token key name passed as parameter
    * @param    key token key variable name in the cache
    * @result       value stored in the cache using that token passed
    * @example
    * * Object cacheValue = myCacheConnection.get(myTokenKey);
    */
    Object get(String key);

    /**
    * @description Put into cache the value passed in the token key name passed as parameter
    * @param    key     string token cache key
    * @param    value   object value to store
    * @param    ttl     time in milliseconds which will be stored
    * @result           value stored in the cache using that token passed
    * @example
    * * Object cacheValue = myCacheConnection.put(myTokenKey, myValue, timeLeft);
    */
    void put(String key, Object value, Integer ttl);

    /**
    * @description Remove everything in cache for the token key passed
    * @param    key string token cache key
    * @result       boolean which shows if the value was removed
    * @example
    * * Boolean valueRemoved = myCacheConnection.remove(myTokenKey);
    */
    Boolean remove(String key);
}