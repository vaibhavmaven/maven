@isTest   // making this class visible to testing framework. 
public class EligibilityFailureHttpCalloutMock implements HttpCalloutMock{
    
    public HttpResponse respond(HttpRequest httpReq)
    {
        HttpResponse responseObj = new HttpResponse();
        responseObj.setHeader('Content-Type', 'application/json'); 
        responseObj.setStatusCode(200); 
        // Load the json response from Static Resource.
        //List<StaticResource> srList = [SELECT Id, Body FROM StaticResource WHERE Name = 'EligibilityResponse_Failure' LIMIT 1];
        //String jsonResponse = srList[0].Body.toString(); 
        //System.debug('Failure Eligibility Response is : ' + jsonResponse);        
        //responseObj.setBody(jsonResponse); 
        return responseObj; 
    }

}