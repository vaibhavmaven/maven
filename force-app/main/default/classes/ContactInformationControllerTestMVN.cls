/**
 *  @author Mavens
 *  @date August 2016
 *  @description Test class for the ContactInformationControllerMVN Apex class
 */
@isTest
private class ContactInformationControllerTestMVN {
    private static User caseManager;
    
    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        DCRGlobalSettingMVN.setMock(
            (DCR_Global_Setting_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata(
                'DCR_Global_Setting_MVN__mdt',
                new Map<String, Object>{'DCR_Affiliation_Processing_MVN__c' => false,
                                        'DCR_Approval_Required_for_Update_MVN__c' => false,
                                        'DCR_Create_Handler_Class_MVN__c' => 'DCRCreationHdlrMVN',
                                        'DCR_Excluded_Profile_Names_MVN__c' => 'Test Profile',
                                        'DCR_Globally_Active_MVN__c' => false}
            )
        );
        TestDataFactoryMVN.createPSSettings();
        
        System.runAs(caseManager) {
            Account member = TestDataFactoryMVN.createMember();
            Program_MVN__c program = TestDataFactoryMVN.createProgram();
            Program_Member_MVN__c programMember = TestDataFactoryMVN.createProgramMember(program, member);
    
            RecordTypesMVN.cache(Contact_Information_MVN__c.SObjectType);
    
            List<Contact_Information_MVN__c> infos = new Contact_Information_MVN__c[]{
                new Contact_Information_MVN__c(
                    Account_MVN__c = member.Id,
                    Email_MVN__c = 'member@example.com',
                    RecordTypeId = RecordTypesMVN.getId(Contact_Information_MVN__c.SObjectType, 'Email_MVN')
                ),
                new Contact_Information_MVN__c(
                    Account_MVN__c = member.Id,
                    Email_MVN__c = 'member@example.com',
                    RecordTypeId = RecordTypesMVN.getId(Contact_Information_MVN__c.SObjectType, 'Email_MVN')
                ),
                new Contact_Information_MVN__c(
                    Account_MVN__c = member.Id,
                    Email_MVN__c = 'member@example.com',
                    RecordTypeId = RecordTypesMVN.getId(Contact_Information_MVN__c.SObjectType, 'Email_MVN')
                ),
                new Contact_Information_MVN__c(
                    Account_MVN__c = member.Id,
                    Email_MVN__c = 'member@example.com',
                    RecordTypeId = RecordTypesMVN.getId(Contact_Information_MVN__c.SObjectType, 'Email_MVN')
                ),
                new Contact_Information_MVN__c(
                    Account_MVN__c = member.Id,
                    Email_MVN__c = 'member@example.com',
                    RecordTypeId = RecordTypesMVN.getId(Contact_Information_MVN__c.SObjectType, 'Email_MVN')
                )
            };
            insert infos;
        }
    }

    @isTest
    static void testGetInformationUsingAccountId() {
        System.runAs(caseManager) {
            Account member = [SELECT Id FROM Account LIMIT 1];
            ContactInformationControllerMVN controller = new ContactInformationControllerMVN();
            controller.accountId = member.Id;

            Test.startTest();
            List<ContactInformationControllerMVN.ContactInfoWrapper> info = controller.getInfoList();
            Test.stopTest();

            System.assertEquals(5, info.size());
        }
    }

    @isTest
    static void testGetInformationUsingProgramMemberId() {
        System.runAs(caseManager) {
            Program_Member_MVN__c programMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
            ContactInformationControllerMVN controller = new ContactInformationControllerMVN();
            controller.memberId = programMember.Id;

            Test.startTest();
            List<ContactInformationControllerMVN.ContactInfoWrapper> info = controller.getInfoList();
            Test.stopTest();

            System.assertEquals(5, info.size());
        }
    }

    @isTest
    static void testRefreshTable() {
        System.runAs(caseManager) {
            Program_Member_MVN__c programMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
            ContactInformationControllerMVN controller = new ContactInformationControllerMVN();
            controller.memberId = programMember.Id;

            Test.startTest();
            controller.onRefreshTable();
            List<ContactInformationControllerMVN.ContactInfoWrapper> info = controller.getInfoList();
            Test.stopTest();

            System.assertEquals(5, info.size());
        }
    }

    @isTest
    static void testCheckFieldSetColumns() {
        System.runAs(caseManager) {
            Program_Member_MVN__c programMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
            ContactInformationControllerMVN controller = new ContactInformationControllerMVN();
            controller.memberId = programMember.Id;

            Test.startTest();
            List<ContactInformationControllerMVN.ContactInfoWrapper> info = controller.getInfoList();
            Test.stopTest();

            System.assertEquals(5, info.size());
            System.assertEquals(true, info[0].getHasType());
            System.assertEquals(true, info[0].getHasLabel());
            System.assertEquals(false, info[0].getHasValue());
            System.assertEquals(true, info[0].getHasPrimary());
            System.assertEquals(false, info[0].getHasShipping());
            System.assertEquals(false, info[0].getHasMessagesOK());
            System.assertEquals(false, info[0].getHasBestTime());
        }
    }

    @isTest
    static void testAddressInitialization() {
        Account member = TestDataFactoryMVN.createMember();

        Address_vod__c address = new Address_vod__c();
        address.Account_vod__c = member.Id;
        address.Name = '123 Main St';
        address.DCR_Override_MVN__c = true;
        insert address;

        System.runAs(caseManager) {
            ContactInformationControllerMVN controller = new ContactInformationControllerMVN();
            controller.accountId = member.Id;

            Test.startTest();
            List<ContactInformationControllerMVN.ContactInfoWrapper> info = controller.getInfoList();
            Test.stopTest();

            System.assertEquals(1, info.size());
            System.assertEquals(address.Id, info[0].address.Id);
        }
    }
}