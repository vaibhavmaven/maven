/*
 * RelationshipGraphCtrlMVN
 * Created By:      Jen Wyher
 * Created Date:    Dec 2015
 * Description:     Controller for the RelationshipGraph page. View, Create, Delete relationships.
 *
 */
global with sharing class RelationshipGraphCtrlMVN {

    public static Id accId {get; set;}
    public static String fromRole = 'Patient';  // default selected value
    public static String toRole {get; set;}
    public static String program {get;  set;}

    public String fieldSetKeys {get; private set;}
    public String fieldSetTypes {get; private set;}

    global RelationshipGraphCtrlMVN(){

        Map<String, String> fieldKeys = new Map<String, String>();
        Map<String, String> fieldTypes = new Map<String, String>();
        List<Schema.FieldSetMember> returnFields = Schema.SObjectType.Account.fieldSets.getMap().get('Relationship_Account_Search_Result_MVN').getFields();
        for(Schema.FieldSetMember member : returnFields) {
            fieldKeys.put(member.getLabel(), member.getFieldPath());
            fieldTypes.put(member.getLabel(), String.valueOf(member.getType()));
        }

        fieldSetKeys = JSON.serialize(fieldKeys);
        fieldSetTypes = JSON.serialize(fieldTypes);
    }

    @RemoteAction
    global static Node getTree(String nId, Id aId) {
        // add the parent and get the relationships
        Account account = [SELECT name, id FROM account WHERE id = :aId];
        Node tree = new Node(nId, aId, account.name, null);
        List<Relationship_MVN__c> relationships = [select Id, To_Account_MVN__c, To_Account_MVN__r.name, To_Role_MVN__c, From_Role_MVN__c, Comments_MVN__c, Program_Member_MVN__c, Program_Member_MVN__r.Program_MVN__r.Name from Relationship_MVN__c where From_Account_MVN__c =: aId];

        Set<Id> accountIds = new Set<Id>();
        for(Integer i = 0, j = relationships.size(); i < j; i++) {
            accountIds.add(relationships[i].To_Account_MVN__c);
        }

        List<Contact_Information_MVN__c> contactInfos = [SELECT Id, RecordType.Name, Phone_MVN__c, Email_MVN__c, Account_MVN__c FROM Contact_Information_MVN__c WHERE Account_MVN__c in :accountIds AND Primary_MVN__c = true];

        Map<Id, String> phoneDetails = new Map<Id, String>();
        Map<Id, String> emailDetails = new Map<Id, String>();

        for(Integer i = 0, j = contactInfos.size(); i < j; i++) {
            Contact_Information_MVN__c info = contactInfos[i];
            if(info.RecordType.Name == 'Phone') {
                phoneDetails.put(info.Account_MVN__c, info.Phone_MVN__c);
            } else if(info.RecordType.Name == 'Email') {
                emailDetails.put(info.Account_MVN__c, info.Email_MVN__c);
            }
        }

        // map the roles and relationships in each
        Map<String, List<Node>> children = new Map<String, List<Node>>();

        Map<String, String> attributes;
        for(Relationship_MVN__c r: relationships) {

            attributes = new Map<String, String>();
            String role = r.To_Role_MVN__c;
            String frole = r.From_Role_MVN__c;

            attributes.put('Role', role);
            attributes.put('Program', r.Program_Member_MVN__r.Program_MVN__r.Name == null ? '' : r.Program_Member_MVN__r.Program_MVN__r.Name);
            attributes.put('ProgramId', r.Program_Member_MVN__c == null ? '' : r.Program_Member_MVN__c);
            attributes.put('Comments', r.Comments_MVN__c == null ? '' : r.Comments_MVN__c);
            attributes.put('Phone', phoneDetails.get(r.To_Account_MVN__c) == null ? '' : phoneDetails.get(r.To_Account_MVN__c));
            attributes.put('Email', emailDetails.get(r.To_Account_MVN__c) == null ? '' : emailDetails.get(r.To_Account_MVN__c));
            attributes.put('FromRole', frole);

            Node n = new Node(r.To_Account_MVN__c+'-'+r.Id, r.To_Account_MVN__c, r.To_Account_MVN__r.name, null);
            n.attributes = attributes;
            n.relId = r.Id;
            if (children.get(role) == null) {
                children.put(role, new List<Node>());
            }
            children.get(role).add(n);
        }

        List<Node> roles = new List<Node>();
        for (String role: children.keySet()) {
            Node roleNode = new Node(null, null, role, children.get(role));
            roles.add(roleNode);
        }

        tree.children = roles;

        return tree;
    }

    @RemoteAction
    global static List<Account> getSearchResults(String searchTerm) {
        List<Schema.FieldSetMember> returnFields = Schema.SObjectType.Account.fieldSets.getMap().get('Relationship_Account_Search_Result_MVN').getFields();

        String s = 'select id';

        for(Schema.FieldSetMember member : returnFields) {
            s += ', ' + String.escapeSingleQuotes(member.getFieldPath());
        }

        s += ' from Account where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'';

        return Database.query(s);
    }

    public List<SelectOption> getOptions(Schema.DescribeFieldResult fieldResult) {
        List<SelectOption> options = new List<SelectOption>();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }

        return options;
    }

    public List<SelectOption> getFromRoles() {
        return getOptions(Relationship_MVN__c.From_Role_MVN__c.getDescribe());
    }
    public List<SelectOption> getToRoles() {
        return getOptions(Relationship_MVN__c.To_Role_MVN__c.getDescribe());
    }

    public List<SelectOption> getPrograms() {
        List<SelectOption> options = new List<SelectOption>();
        List<Program_Member_MVN__c> pm = [select Id, Status_MVN__c, Program_MVN__r.Name from Program_Member_MVN__c where Member_MVN__c = :accId];

        for (Program_Member_MVN__c p: pm) {
            if(p.Program_MVN__r.Name != null) {
                options.add(new SelectOption(p.Id, p.Program_MVN__r.Name));
            } else {
                options.add(new SelectOption(p.Id, Label.Program_Deleted));
            }
        }
        return options;
    }


    public String getFromRole() {
        return fromRole;
    }
    public void setFromRole(String s) {
        fromRole = s;
    }
    public String getToRole() {
        return toRole;
    }
    public void setToRole(String s) {
        toRole = s;
    }


    @RemoteAction
    global static void createRelationship(Id fAccount, Id tAccount, String fRole, String tRole, Id p, String comment) {
        Relationship_MVN__c relationship = new Relationship_MVN__c();
        relationship.From_Account_MVN__c = fAccount;
        relationship.To_Account_MVN__c = tAccount;
        relationship.From_Role_MVN__c = fRole;
        relationship.To_Role_MVN__c = tRole;
        relationship.Program_Member_MVN__c = p;
        relationship.Comments_MVN__c = comment;
        insert relationship;
    }

    @RemoteAction
    global static void updateRelationship(Id relId, Id progId, String comments, String fRole, String tRole) {
        Relationship_MVN__c rel = new Relationship_MVN__c();
        rel.Id = relId;
        rel.Program_Member_MVN__c = progId;
        rel.Comments_MVN__c = comments;
        rel.From_Role_MVN__c = fRole;
        rel.To_Role_MVN__c = tRole;
        update rel;
    }

    @RemoteAction
    global static void removeRelationship(Id relationship) {
        Relationship_MVN__c r = new Relationship_MVN__c(Id=relationship) ;
        delete r;
    }


     global class Node{
        public String id;
        public String acctId;
        public String relId;
        public String name;
        public String url;

        public List<Node> children;
        public List<Node> hiddenChildren;

        public Map<String, String> attributes = new Map<String, String>();
        public boolean showButton;
        public boolean warn;

        public Node(String id, String acctId, String name, List<Node> children) {
            this.id = id;
            this.acctId = acctId;
            if (acctId != null) {
                url = '/'+ acctId;
            }
            this.name = name;
            this.children = children;
        }

        public String getURL() {
            return '/' + acctId;
        }

    }

}