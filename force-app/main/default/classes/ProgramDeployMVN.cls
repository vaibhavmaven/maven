/**
*   ProgramDeployMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 19th, 2017
*   Description:    functionality to enable download of a program with all related records as JSON
*                   also add the ability to parse a JSON into those records
*/
public class ProgramDeployMVN {
    /**
    * @description program with all related records (JSON structure)
    */
    @testVisible private ProgramDeployWrapperMVN programToDeploy { get; set; }

    /**
    * @description program with all related records (JSON structure)
    */
    @testVisible private ProgramDeployWrapperMVN programToBeOverridden { get; set; }

    /**
    * @description Program Stage RecordTypes by DeveloperName
    */
    private List<sObject> programItemsToDelete;

    /**
    * @description Program Stage RecordTypes by DeveloperName
    */
    private Map<String, Id> programStageRecordTypeIdsByDeveloperName;

    /**
    * @description constructor
    */
    @testVisible private ProgramDeployMVN() {
        programItemsToDelete = new List<sObject>();
    }

    /**
    * @description constructor
    *              - collect program with all related records
    * @param String externalProgramId
    */
    public ProgramDeployMVN(String externalProgramId) {
        this();
        this.programToDeploy = new ProgramDeployWrapperMVN(externalProgramId);
    }

    /**
    * @description constructor
    *              - collect program with all related records
    * @param String externalProgramId
    */
    public ProgramDeployMVN(ProgramDeployWrapperMVN programToDeploy) {
        this();
        this.programToDeploy = programToDeploy;
    }

    /**
    * @description insert/update program and all related records
    */
    public void deploy() {
        this.compare();
        this.programToDeploy.program.Cloned_MVN__c = true;
        this.setProgramStageRecordTypeIdsByDeveloperName();
        for(Program_Stage_MVN__c thisProgramStage : this.programToDeploy.programStageParents.values()) {
            thisProgramStage = this.setRecordType(thisProgramStage);
        }
        for(Program_Stage_MVN__c thisProgramStage : this.programToDeploy.programStageChildren.values()) {
            thisProgramStage = this.setRecordType(thisProgramStage);
        }
        upsert this.programToDeploy.program Program_ID_MVN__c;
        upsert this.programToDeploy.programStageParents.values() Program_Stage_ID_MVN__c;
        upsert this.programToDeploy.programStageChildren.values() Program_Stage_ID_MVN__c;
        upsert this.programToDeploy.programStageDependencies.values() Program_Stage_Dependency_ID_MVN__c;
        upsert this.programToDeploy.programStageFieldMaps.values() Program_Stage_Field_Map_ID_MVN__c;
        upsert this.programToDeploy.medicalHistoryTypes.values() Medical_History_Type_Id_MVN__c;
        delete this.programItemsToDelete;
    }

    /**
    * @description return the detail page of the program
    * @return PageReference detailProgramPage
    */
    public PageReference view() {
        return this.programToDeploy.view();
    }

    /**
    * @description compares old & new
    */
    private void compare() {
        try {
            this.programToBeOverridden = new ProgramDeployWrapperMVN(
                this.programToDeploy.program.Program_ID_MVN__c
            );
            this.markOldProgramItemsForDelete(
                this.programToBeOverridden.medicalHistoryTypes,
                this.programToDeploy.medicalHistoryTypes
            );
            this.markOldProgramItemsForDelete(
                this.programToBeOverridden.programStageFieldMaps,
                this.programToDeploy.programStageFieldMaps
            );
            this.markOldProgramItemsForDelete(
                this.programToBeOverridden.programStageDependencies,
                this.programToDeploy.programStageDependencies
            );
            this.markOldProgramItemsForDelete(
                this.programToBeOverridden.programStageChildren,
                this.programToDeploy.programStageChildren
            );
            this.markOldProgramItemsForDelete(
                this.programToBeOverridden.programStageParents,
                this.programToDeploy.programStageParents
            );
        } catch(Exception ex) {
            System.debug('===> ' + ex);
        }
    }

    private void markOldProgramItemsForDelete(Map<String, sObject> oldMapToCompare,
                                              Map<String, sObject> newMapToCompare) {
        if(oldMapToCompare != null) {
            for(String key : oldMapToCompare.keySet()) {
                if(newMapToCompare != null && newMapToCompare.containsKey(key)) {
                    continue;
                } else {
                    this.programItemsToDelete.add(oldMapToCompare.get(key));
                }
            }
        }

    }

    /**
    * @description set recordType map to get Ids by DeveloperName
    */
    private void setProgramStageRecordTypeIdsByDeveloperName() {
        this.programStageRecordTypeIdsByDeveloperName = new Map<String, Id>();
        for(RecordType thisRecordType : [SELECT Id, DeveloperName
                                           FROM RecordType
                                          WHERE SobjectType =: 'Program_Stage_MVN__c']) {
            this.programStageRecordTypeIdsByDeveloperName.put(
                thisRecordType.DeveloperName,
                thisRecordType.Id
            );
        }
    }

    /**
    * @description set recordType for Program Stage
    * @param Program_Stage_MVN__c thisProgramStage
    * @return Program_Stage_MVN__c thisProgramStage
    */
    private Program_Stage_MVN__c setRecordType(Program_Stage_MVN__c thisProgramStage) {
        thisProgramStage.RecordTypeId = this.programStageRecordTypeIdsByDeveloperName.get(
            thisProgramStage.RecordType.DeveloperName
        );
        thisProgramStage.RecordType = null;
        return thisProgramStage;
    }
}