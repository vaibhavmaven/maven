/**
*   ProgramDeployMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 19th, 2017
*   Description:    functionality to enable download of a program with all related records as JSON
*                   also add the ability to parse a JSON into those records
*/
public class ProgramDeployWrapperMVN {
    /**
    * @description program with all related records (JSON structure)
    */
    public Program_MVN__c program { get; set; }
    public Map<String, Program_Stage_MVN__c> programStageParents { get; set; }
    public Map<String, Program_Stage_MVN__c> programStageChildren { get; set; }
    public Map<String, Program_Stage_Dependency_MVN__c> programStageDependencies { get; set; }
    public Map<String, Program_Stage_Field_Map_MVN__c> programStageFieldMaps { get; set; }
    public Map<String, Medical_History_Type_MVN__c> medicalHistoryTypes { get; set; }

    /**
    * @description constructor
    */
    public ProgramDeployWrapperMVN() {}

    /**
    * @description constructor
    *              - collect program with all related records
    * @param String externalProgramId
    */
    public ProgramDeployWrapperMVN(String externalProgramId) {
        this.program = this.getProgram(externalProgramId);
        this.programStageParents = this.getProgramStageParents(externalProgramId);
        this.programStageChildren = this.getProgramStageChildren(externalProgramId);
        this.programStageDependencies = this.getProgramStageDependencies(externalProgramId);
        this.programStageFieldMaps = this.getProgramStageFieldMaps(externalProgramId);
        this.medicalHistoryTypes = this.getMedicalHistoryTypes(externalProgramId);
    }

    /**
    * @description convert this object into JSON
    * @return String json
    */
    public String toJSON() {
        return System.JSON.serialize(this, true);
    }

    /**
    * @description parse JSON into this object
    *              - remove all identifiers from JSON
    * @param String json
    * @return ProgramDeployWrapperMVN programToDeploy
    */
    public ProgramDeployWrapperMVN parse(String json) {
        //json = json.deleteWhitespace();
        json = this.stripFromJson(json, 'Id');
        json = this.stripFromJson(json, 'Program_MVN__c');
        json = this.stripFromJson(json, 'Parent_Stage_MVN__c');
        json = this.stripFromJson(json, 'Program_Stage_MVN__c');
        json = this.stripFromJson(json, 'Program_Stage_Dependency_MVN__c');
        json = this.stripFromJson(json, 'RecordTypeId');
        json = json.replaceAll('(,})', '}');
        return (ProgramDeployWrapperMVN)System.JSON.deserialize(json, ProgramDeployWrapperMVN.class);
    }

    /**
    * @description return the detail page of the program
    * @return PageReference detailProgramPage
    */
    public PageReference view() {
        return new ApexPages.StandardController(this.program).view();
    }

    /**
    * @description strip a key/value pair from the json
    * @param String json
    * @param String key
    * @return String json
    */
    private String stripFromJson(String json, String key) {
        return json.replaceAll('("' + key + '":[^,}]+,?)', '');
    }

    /**
    * @description create program query
    * @param String externalProgramId
    * @return String query
    */
    private String getProgramQuery(String externalProgramId) {
        Set<String> fieldsToQuery = new Set<String> {
            'Name',
            'Program_ID_MVN__c',
            'Active_MVN__c',
            'Country_MVN__c',
            'Description_MVN__c',
            'Display_Color_MVN__c',
            'Graph_X_Coordinate_MVN__c',
            'Graph_Y_Coordinate_MVN__c',
            'Program_Member_Record_Type_MVN__c',
            'Cloned_MVN__c'
        };
        for(Schema.FieldSetMember thisFieldSetMember :
                SObjectType.Program_MVN__c.FieldSets.Program_Deployment_Fields_MVN.getFields()) {
            fieldsToQuery.add(thisFieldSetMember.getFieldPath());
        }
        String query = 'SELECT ' + String.join(new List<String>(fieldsToQuery), ', ') +
                       '  FROM Program_MVN__c' +
                       ' WHERE Program_ID_MVN__c != null' +
                       '   AND Program_ID_MVN__c =: externalProgramId' +
                       ' LIMIT 1';
        return query;
    }

    /**
    * @description collect program records
    * @param String externalProgramId
    * @return Program_MVN__c program
    */
    private Program_MVN__c getProgram(String externalProgramId) {
        return System.Database.query(getProgramQuery(externalProgramId));
    }

    /**
    * @description create program stage parents query
    * @param String externalProgramId
    * @return String query
    */
    private String getProgramStageParentsQuery(String externalProgramId) {
        Set<String> fieldsToQuery = new Set<String> {
            'Name',
            'Program_Stage_ID_MVN__c',
            'Program_MVN__r.Program_ID_MVN__c',
            'Activity_Type_MVN__c',
            'Description_MVN__c',
            'Exclude_Activity_MVN__c',
            'Exclude_From_Library_MVN__c',
            'Flow_Name_MVN__c',
            'Graph_X_Coordinate_MVN__c',
            'Graph_Y_Coordinate_MVN__c',
            'Library_Only_MVN__c',
            'Related_Object_Close_Status_MVN__c',
            'Related_Object_MVN__c',
            'Related_Object_Open_Status_MVN__c',
            'Removeable_MVN__c',
            'Skippable_MVN__c',
            'Stage_Sequence_Number_MVN__c',
            'Startable_MVN__c',
            'RecordType.DeveloperName'
        };
        for(Schema.FieldSetMember thisFieldSetMember :
                SObjectType.Program_Stage_MVN__c.FieldSets.Program_Stage_Builder_Parent_Stage_MVN.getFields()) {
            fieldsToQuery.add(thisFieldSetMember.getFieldPath());
        }
        String query = 'SELECT ' + String.join(new List<String>(fieldsToQuery), ', ') +
                       '  FROM Program_Stage_MVN__c' +
                       ' WHERE Program_Stage_ID_MVN__c != null' +
                       '   AND Parent_Stage_MVN__c = null' +
                       '   AND Program_MVN__r.Program_ID_MVN__c =: externalProgramId';
        return query;
    }

    /**
    * @description collect program stage parents records
    * @param String externalProgramId
    * @return Map<String, Program_Stage_MVN__c> programStageParents
    */
    private Map<String, Program_Stage_MVN__c> getProgramStageParents(String externalProgramId) {
        Map<String, Program_Stage_MVN__c> programStageParents = new Map<String, Program_Stage_MVN__c>();
        for(Program_Stage_MVN__c thisProgramStage : System.Database.query(getProgramStageParentsQuery(externalProgramId))) {
            programStageParents.put(thisProgramStage.Program_Stage_ID_MVN__c, thisProgramStage);
        }
        return programStageParents;
    }

    /**
    * @description create program stage children query
    * @param String externalProgramId
    * @return String query
    */
    private String getProgramStageChildrenQuery(String externalProgramId) {
        Set<String> fieldsToQuery = new Set<String> {
            'Name',
            'Program_Stage_ID_MVN__c',
            'Program_MVN__r.Program_ID_MVN__c',
            'Parent_Stage_MVN__r.Program_Stage_ID_MVN__c',
            'Activity_Type_MVN__c',
            'Description_MVN__c',
            'Exclude_Activity_MVN__c',
            'Exclude_From_Library_MVN__c',
            'Flow_Name_MVN__c',
            'Graph_X_Coordinate_MVN__c',
            'Graph_Y_Coordinate_MVN__c',
            'Library_Only_MVN__c',
            'Related_Object_Close_Status_MVN__c',
            'Related_Object_MVN__c',
            'Related_Object_Open_Status_MVN__c',
            'Removeable_MVN__c',
            'Skippable_MVN__c',
            'Stage_Sequence_Number_MVN__c',
            'Startable_MVN__c',
            'RecordType.DeveloperName'
        };
        for(Schema.FieldSetMember thisFieldSetMember :
                SObjectType.Program_Stage_MVN__c.FieldSets.Program_Stage_Builder_Child_Stage_MVN.getFields()) {
            fieldsToQuery.add(thisFieldSetMember.getFieldPath());
        }
        String query = 'SELECT ' + String.join(new List<String>(fieldsToQuery), ', ') +
                       '  FROM Program_Stage_MVN__c' +
                       ' WHERE Program_Stage_ID_MVN__c != null' +
                       '   AND Parent_Stage_MVN__c != null' +
                       '   AND Program_MVN__r.Program_ID_MVN__c =: externalProgramId';
        return query;
    }

    /**
    * @description collect program stage children records
    * @param String externalProgramId
    * @return Map<String, Program_Stage_MVN__c> programStageParents
    */
    private Map<String, Program_Stage_MVN__c> getProgramStageChildren(String externalProgramId) {
        Map<String, Program_Stage_MVN__c> programStageChildren = new Map<String, Program_Stage_MVN__c>();
        for(Program_Stage_MVN__c thisProgramStage : System.Database.query(getProgramStageChildrenQuery(externalProgramId))) {
            programStageChildren.put(thisProgramStage.Program_Stage_ID_MVN__c, thisProgramStage);
        }
        return programStageChildren;
    }

    /**
    * @description create program stage dependencies query
    * @param String externalProgramId
    * @return String query
    */
    private String getProgramStageDependenciesQuery(String externalProgramId) {
        Set<String> fieldsToQuery = new Set<String> {
            'Name',
            'Program_Stage_Dependency_ID_MVN__c',
            'Program_Stage_MVN__r.Program_Stage_ID_MVN__c',
            'Program_Stage_Dependency_MVN__r.Program_Stage_ID_MVN__c'
        };
        String query = 'SELECT ' + String.join(new List<String>(fieldsToQuery), ', ') +
                       '  FROM Program_Stage_Dependency_MVN__c' +
                       ' WHERE Program_Stage_MVN__r.Program_MVN__r.Program_ID_MVN__c =: externalProgramId';
        return query;
    }

    /**
    * @description collect program stage dependencies records
    * @param String externalProgramId
    * @return Map<String, Program_Stage_Dependency_MVN__c> programStageDependencies
    */
    private Map<String, Program_Stage_Dependency_MVN__c> getProgramStageDependencies(String externalProgramId) {
        Map<String, Program_Stage_Dependency_MVN__c> programStageDependencies = new Map<String, Program_Stage_Dependency_MVN__c>();
        for(Program_Stage_Dependency_MVN__c thisProgramStageDependency : System.Database.query(getProgramStageDependenciesQuery(externalProgramId))) {
            programStageDependencies.put(thisProgramStageDependency.Program_Stage_Dependency_ID_MVN__c, thisProgramStageDependency);
        }
        return programStageDependencies;
    }

    /**
    * @description create program stage field maps query
    * @param String externalProgramId
    * @return String query
    */
    private String getProgramStageFieldMapsQuery(String externalProgramId) {
        Set<String> fieldsToQuery = new Set<String> {
            'Name',
            'Program_Stage_Field_Map_ID_MVN__c',
            'Program_Stage_MVN__r.Program_Stage_ID_MVN__c',
            'Value_MVN__c',
            'Object_MVN__c',
            'Field_MVN__c'
        };
        String query = 'SELECT ' + String.join(new List<String>(fieldsToQuery), ', ') +
                       '  FROM Program_Stage_Field_Map_MVN__c' +
                       ' WHERE Program_Stage_MVN__r.Program_MVN__r.Program_ID_MVN__c =: externalProgramId';
        return query;
    }

    /**
    * @description collect program stage field maps records
    * @param String externalProgramId
    * @return Map<String, Program_Stage_Field_Map_MVN__c> programStageFieldMaps
    */
    private Map<String, Program_Stage_Field_Map_MVN__c> getProgramStageFieldMaps(String externalProgramId) {
        Map<String, Program_Stage_Field_Map_MVN__c> programStageFieldMaps = new Map<String, Program_Stage_Field_Map_MVN__c>();
        for(Program_Stage_Field_Map_MVN__c thisProgramStageFieldMap : System.Database.query(getProgramStageFieldMapsQuery(externalProgramId))) {
            programStageFieldMaps.put(thisProgramStageFieldMap.Program_Stage_Field_Map_ID_MVN__c, thisProgramStageFieldMap);
        }
        return programStageFieldMaps;
    }

    /**
    * @description create medical history types query
    * @param String externalProgramId
    * @return String query
    */
    private String getMedicalHistoryTypesQuery(String externalProgramId) {
        Set<String> fieldsToQuery = new Set<String> {
            'Name',
            'Medical_History_Type_Id_MVN__c',
            'Order_MVN__c',
            'Program_MVN__r.Program_ID_MVN__c',
            'Record_Type_Name_MVN__c'
        };
        String query = 'SELECT ' + String.join(new List<String>(fieldsToQuery), ', ') +
                       '  FROM Medical_History_Type_MVN__c' +
                       ' WHERE Program_MVN__r.Program_ID_MVN__c =: externalProgramId';
        return query;
    }

    /**
    * @description collect medical history types records
    * @param String externalProgramId
    * @return Map<String, Medical_History_Type_MVN__c> medicalHistoryTypes
    */
    private Map<String, Medical_History_Type_MVN__c> getMedicalHistoryTypes(String externalProgramId) {
        Map<String, Medical_History_Type_MVN__c> medicalHistoryTypes = new Map<String, Medical_History_Type_MVN__c>();
        for(Medical_History_Type_MVN__c thisMedicalHistoryType : System.Database.query(getMedicalHistoryTypesQuery(externalProgramId))) {
            medicalHistoryTypes.put(thisMedicalHistoryType.Medical_History_Type_Id_MVN__c, thisMedicalHistoryType);
        }
        return medicalHistoryTypes;
    }
}