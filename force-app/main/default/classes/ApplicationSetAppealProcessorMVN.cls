/**
 * @author Mavens
 * @description when an application Appeal Result changes set the Appeal Processor to the current user
 * @group Application
 */
public with sharing class ApplicationSetAppealProcessorMVN implements TriggerHandlerMVN.HandlerInterface {

    /**
     * @description handle
     */
    public void handle() {
        for (Application_MVN__c application : (List<Application_MVN__c>) Trigger.new) {
            if ( appealResultChanged(application) ) {
                application.Appeal_Processor_MVN__c = UserInfo.getUserId();
                (new ActionEngineMVN(application.Program_Member_MVN__c)).run(ActionEngineMVN.APPEALACTION);
            }
        }
    }


    /**
     * @description returns true if the appeal result value has changed
     * @param Application_MVN__c currentApplication
     * @return Boolean appealResultChanged
     */
    private Boolean appealResultChanged(Application_MVN__c currentApplication) {
        Application_MVN__c oldApplication = (Application_MVN__c) Trigger.oldMap.get(currentApplication.Id);
        return currentApplication.Appeal_Result_MVN__c != oldApplication.Appeal_Result_MVN__c;
    }
}