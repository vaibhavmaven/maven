/**
 * TestFactoryMDMConnectionsMVN
 * @author: Pablo Roldan
 * @created Date: October 2017
 * @description: This class is used to construct MDM Connection custom metadatas
 */
@isTest
public with sharing class TestFactoryMDMConnectionsMVN {

    public static void setMock() {
        setMock(null);
    }

    public static void setMock(Map<String, Object> fieldValues) {
        TestFactorySegmentsMVN.createMockSegments();

        Map<String, Object> setting = new Map<String, Object> {
            'MDM_Named_Credentials_MVN__c' => 'Veeva_Network',
            'MDM_Session_Timeout_MVN__c' => null,
            'MDM_Call_Timeout_MVN__c' => 5000,
            'Segment_MVN__r' => (Segment_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata(
                'Segment_MVN__mdt', 
                new Map<String, Object>{
                    'Id'=>'000000000000001', 
                    'Country_Code_MVN__c'=>'ZZ'
                }
            )
        };
        
        if (fieldValues != null) {
            setting.putAll(fieldValues);
        }

        MDMConnectionsMVN.setMock((MDM_Connection_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata('MDM_Connection_MVN__mdt', setting));
    }

}