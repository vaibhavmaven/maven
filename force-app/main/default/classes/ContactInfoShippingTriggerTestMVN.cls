/**
 *  ContactInfoShippingTriggerMVN
 *  Created By:     Paul Battisson
 *  Created On:     2nd May 2017
 *  Description:    This test class tests the functionality of the ContactInfoShippingTriggerMVN
 **/
@isTest
private class ContactInfoShippingTriggerTestMVN {

    private static Account patient;

    static {
        patient = TestDataFactoryMVN.createMember();
    }

    @isTest
    private static void testInsertNewShippingSetAccountValues_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Shipping_vod__c = true;
        insert address;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.ShippingStreet);
        System.assertEquals(address.City_vod__c, patient.ShippingCity);
        System.assertEquals(address.State_vod__c, patient.ShippingStateCode);
        System.assertEquals(address.Country_vod__c, patient.ShippingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.ShippingPostalCode);
    }

    @isTest
    private static void testInsertNewShippingSetAccountValuesAndRemoveOld_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Shipping_vod__c = true;
        insert address;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.ShippingStreet);
        System.assertEquals(address.City_vod__c, patient.ShippingCity);
        System.assertEquals(address.State_vod__c, patient.ShippingStateCode);
        System.assertEquals(address.Country_vod__c, patient.ShippingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.ShippingPostalCode);

        ContactInfoShippingTriggerMVN.hasRun = false;

        Address_vod__c address2 = new Address_vod__c(Name = '321 Faux Blvd.');
        address2.City_vod__c = 'Chicago';
        address2.Zip_vod__c = '60606';
        address2.State_vod__c = 'IL';
        address2.Country_vod__c = 'US';
        address2.Account_vod__c = patient.Id;
        address2.Shipping_vod__c = true;
        insert address2;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address2.Name, patient.ShippingStreet);
        System.assertEquals(address2.City_vod__c, patient.ShippingCity);
        System.assertEquals(address2.State_vod__c, patient.ShippingStateCode);
        System.assertEquals(address2.Country_vod__c, patient.ShippingCountryCode);
        System.assertEquals(address2.Zip_vod__c, patient.ShippingPostalCode);

        address = [SELECT Shipping_vod__c FROM Address_vod__c WHERE Id = :address.Id];

        System.assertEquals(false, address.Shipping_vod__c);
    }

    @isTest
    private static void testUpdateExistingShippingToUncheckedRemoveDetails_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Shipping_vod__c = true;
        insert address;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.ShippingStreet);
        System.assertEquals(address.City_vod__c, patient.ShippingCity);
        System.assertEquals(address.State_vod__c, patient.ShippingStateCode);
        System.assertEquals(address.Country_vod__c, patient.ShippingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.ShippingPostalCode);

        ContactInfoShippingTriggerMVN.hasRun = false;

        address = [SELECT Shipping_vod__c FROM Address_vod__c WHERE Id = :address.Id];

        address.Shipping_vod__c = false;
        update address;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(null, patient.ShippingStreet);
        System.assertEquals(null, patient.ShippingCity);
        System.assertEquals(null, patient.ShippingStateCode);
        System.assertEquals(null, patient.ShippingCountryCode);
        System.assertEquals(null, patient.ShippingPostalCode);
    }

    @isTest
    private static void testUpdateExistingUncheckedToShippingDetails_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Shipping_vod__c = true;
        insert address;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.ShippingStreet);
        System.assertEquals(address.City_vod__c, patient.ShippingCity);
        System.assertEquals(address.State_vod__c, patient.ShippingStateCode);
        System.assertEquals(address.Country_vod__c, patient.ShippingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.ShippingPostalCode);

        ContactInfoShippingTriggerMVN.hasRun = false;

        Address_vod__c address2 = new Address_vod__c(Name = '321 Faux Blvd.');
        address2.City_vod__c = 'Chicago';
        address2.Zip_vod__c = '60606';
        address2.State_vod__c = 'IL';
        address2.Country_vod__c = 'US';
        address2.Account_vod__c = patient.Id;
        address2.Shipping_vod__c = false;
        insert address2;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.ShippingStreet);
        System.assertEquals(address.City_vod__c, patient.ShippingCity);
        System.assertEquals(address.State_vod__c, patient.ShippingStateCode);
        System.assertEquals(address.Country_vod__c, patient.ShippingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.ShippingPostalCode);

        ContactInfoShippingTriggerMVN.hasRun = false;

        address2 = [SELECT Name, City_vod__c, Zip_vod__c, State_vod__c, Country_vod__c, Shipping_vod__c FROM Address_vod__c WHERE Id = :address2.Id];

        ContactInfoShippingTriggerMVN.hasRun = false;

        address2.Shipping_vod__c = true;
        update address2;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address2.Name, patient.ShippingStreet);
        System.assertEquals(address2.City_vod__c, patient.ShippingCity);
        System.assertEquals(address2.State_vod__c, patient.ShippingStateCode);
        System.assertEquals(address2.Country_vod__c, patient.ShippingCountryCode);
        System.assertEquals(address2.Zip_vod__c, patient.ShippingPostalCode);
    }

    @isTest
    private static void testDeleteExistingShippingRemoveDetails_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Shipping_vod__c = true;
        insert address;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.ShippingStreet);
        System.assertEquals(address.City_vod__c, patient.ShippingCity);
        System.assertEquals(address.State_vod__c, patient.ShippingStateCode);
        System.assertEquals(address.Country_vod__c, patient.ShippingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.ShippingPostalCode);

        ContactInfoShippingTriggerMVN.hasRun = false;

        address = [SELECT Shipping_vod__c FROM Address_vod__c WHERE Id = :address.Id];
        delete address;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(null, patient.ShippingStreet);
        System.assertEquals(null, patient.ShippingCity);
        System.assertEquals(null, patient.ShippingStateCode);
        System.assertEquals(null, patient.ShippingCountryCode);
        System.assertEquals(null, patient.ShippingPostalCode);
    }

    @isTest
    private static void testUpdateExistingShippingValueDetails_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Shipping_vod__c = true;
        insert address;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.ShippingStreet);
        System.assertEquals(address.City_vod__c, patient.ShippingCity);
        System.assertEquals(address.State_vod__c, patient.ShippingStateCode);
        System.assertEquals(address.Country_vod__c, patient.ShippingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.ShippingPostalCode);

        ContactInfoShippingTriggerMVN.hasRun = false;

        address = [SELECT Shipping_vod__c FROM Address_vod__c WHERE Id = :address.Id];

        address.Name = '321 Faux Blvd.';
        address.City_vod__c = 'Chicago';
        address.Zip_vod__c = '60606';
        address.State_vod__c = 'IL';
        address.Country_vod__c = 'US';
        update address;

        patient = [SELECT ShippingStreet, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.ShippingStreet);
        System.assertEquals(address.City_vod__c, patient.ShippingCity);
        System.assertEquals(address.State_vod__c, patient.ShippingStateCode);
        System.assertEquals(address.Country_vod__c, patient.ShippingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.ShippingPostalCode);
    }
}