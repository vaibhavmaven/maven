/**
* @author Mavens
* @group Network API
* @description Test class for VeevaNetworkConnectionUtilTestMVN
*/
@isTest
private class VeevaNetworkConnectionUtilTestMVN {

    /**
    * @description Quantity records for bulk test
    */
    private static final Integer TEST_BULK = 48;

    /**
    * @description Account Test Factory
    */
    private static TestFactoryAccountMVN accTestFactory = new TestFactoryAccountMVN();

    /**
    * @description Address Test Factory
    */
    private static TestFactoryAddressMVN addressTestFactory = new TestFactoryAddressMVN();

    /**
    * @description DCR Test Factory
    */
    private static TestFactoryDCRMVN dcrTestFactory = new TestFactoryDCRMVN();

    /**
    * @description User used for test methods
    */
    private static User testUser;

    /**
    * @description static block which defines the test user and custom metadatas
    */
    static {
        createCustomMetadatas();

        TestFactoryUserMVN testUserFactory = new TestFactoryUserMVN();
        testUser = testUserFactory.create();

        List<Account> institutions = createInstitutions(TEST_BULK / 3);

        List<Account> personAccounts = createPersonAccounts(institutions);

        List<Address_vod__c> addresses = createAddresses(personAccounts);

        Integer totalAccountDCRs = personAccounts.size() + institutions.size() + addresses.size();
        System.assertEquals(TEST_BULK, totalAccountDCRs);

        List<AggregateResult> agDCRs = [SELECT Count(Id) countId
                                          FROM Data_Change_Request_MVN__c
                                         WHERE Status_MVN__c =: VeevaNetworkConnectionUtilMVN.DCR_PENDING];

        Integer totalDCRs = totalAccountDCRs * 2;
        System.assertEquals(agDCRs[0].get('countId'), totalDCRs);

        List<AggregateResult> agDCRLines = [SELECT Count(Id) countId
                                              FROM Data_Change_Request_Line_MVN__c
                                             WHERE Data_Change_Request_MVN__c != null];

        System.assertNotEquals(0, agDCRLines[0].get('countId'));
    }

    /**
    * @description Creates all necessary custom metadatas
    * @example
    * * createCustomMetadatas();
    */
    private static void createCustomMetadatas() {
        TestFactoryCustomMetadataMVN.setMocks();

        DCRUtilityMVN dcrUtility = new DCRUtilityMVN();
        DCRFieldSettingsMVN.setMockList(dcrUtility.buildMockFieldSettingsForTest());
        DCRGlobalSettingMVN.setMock(dcrUtility.buildMockDCRSettingsForTest());
        TestFactoryDCRSettingsMVN.setMocks();
    }

    /**
    * @description Creates all Intitution accounts
    * @param    numOfInstitutions   Number of Institutions to create
    * @result                       List of Institutions created
    * @example
    * * List<Account> institutions = createInstitutions(numOfInstitutions);
    */
    private static List<Account> createInstitutions(Integer numOfInstitutions) {
        List<Account> institutions = new List<Account>();
        String institutionRecordTypeId = TestFactoryAccountMVN.accountRecordTypesByName.get('Hospital_vod').Id;

        for(Integer institutionIndex = 0; institutionIndex < numOfInstitutions; institutionIndex++) {
            institutions.add(accTestFactory.construct(
                    new Map<String, Object> {
                            'RecordTypeId' => institutionRecordTypeId,
                            'Name' => 'Test Institution'+ institutionIndex,
                            'Country_MVN__c' => 'US'
                    }
                )
            );
        }

        insert institutions;

        return institutions;
    }

    /**
    * @description Creates all Person Accounts
    * @param    institutions    List of Institution accounts
    * @result                   List of Person Accounts
    * @example
    * * List<Account> institutions = createPersonAccounts(institutions);
    */
    private static List<Account> createPersonAccounts(List<Account> institutions) {
        Map<String,Object> values = new Map<String,Object> {
            'Salutation' => 'Dr.',
            'FirstName' => 'Robert',
            'LastName' => 'Avery',
            'Suffix' => 'MD',
            'Credentials_vod__c' => 'MD',
            'Country_MVN__c' => 'US',
            'RecordTypeId' => TestFactoryAccountMVN.accountRecordTypesByName.get('Professional_vod').Id
        };

        List<Account> personAccounts = new List<Account>();

        for (Account institution : institutions) {
            personAccounts.add(accTestFactory.construct(values));
        }

        insert personAccounts;

        createAffiliations(institutions, personAccounts);

        return personAccounts;
    }

    /**
    * @description Creates all Affiliations between Institution and person account
    * @param    institutions    List of Institution accounts
    * @param    personAccounts  List of Person Accounts
    * @example
    * * createAffiliations(institutions, personAccounts);
    */
    private static void createAffiliations(List<Account> institutions, List<Account> personAccounts) {
        List<Affiliation_MVN__c> affiliations = new List<Affiliation_MVN__c>();

        System.assertEquals(institutions.size(), personAccounts.size(), 'Instutions size: ' + institutions.size() + ', Person accounts size: ' + personAccounts.size());

        for (Integer accountIndex = 0; accountIndex < institutions.size(); accountIndex++) {
            affiliations.add(new Affiliation_MVN__c(
                    Parent_Account_MVN__c = institutions[accountIndex].Id,
                    Child_Account_MVN__c = personAccounts[accountIndex].Id
            ));
        }

        insert affiliations;
    }

    /**
    * @description Creates all Addresses linked to a person account
    * @param    personAccounts  List of Person accounts used to create addresses
    * @result                       List of Addresses created
    * @example
    * * List<Address_vod__c> addresses = createAddresses(personAccounts);
    */
    private static List<Address_vod__c> createAddresses(List<Account> personAccounts) {
        List<Address_vod__c> addresses = new List<Address_vod__c>();

        for(Account personAccount : personAccounts) {
            addresses.add(addressTestFactory.constructAddress(personAccount, new Map<String, Object>{'Primary_vod__c' => true}));
        }

        insert addresses;
        return addresses;
    }

    /**
    * @testMethod
    * @description checks that gets correct Session Id (Auth token)
    */
    @isTest static void testAuthToken() {
        String token;

        System.runAs(testUser) {
            Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityAuthenticationSuccess());

            Test.startTest();
            token = VeevaNetworkConnectionUtilMVN.getAuthToken(getRequest(), false);
            Test.stopTest();
        }

        System.assertEquals('newSession12345', token);
    }

    /**
    * @testMethod
    * @description checks that gets Inactive User error
    */
    @isTest static void testGetsInactiveErrorResults() {
        System.runAs(testUser) {
            Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkUtilityMockInactiveService());
            getErrorResults(Label.Veeva_Network_Inactive_User_Error_MVN);
        }
    }

    /**
    * @testMethod
    * @description checks that gets Locked error
    */
    @isTest static void testGetsLockedErrorResults() {
        System.runAs(testUser) {
            Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkUtilityMockUserLockedOut());
            getErrorResults(Label.User_Locked_Out_Veeva_Network_Error_MVN);
        }
    }

    /**
    * @testMethod
    * @description checks that gets Session Id error
    */
    @isTest static void testGetsSessionErrorResults() {
        System.runAs(testUser) {
            Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkUtilityMockInvalidSession());
            getErrorResults(Label.Invalid_Session_Veeva_Network_Error_MVN);
        }
    }

    /**
    * @testMethod
    * @description checks that gets Access Privileges error
    */
    @isTest static void testGetsAccessErrorResults() {
        System.runAs(testUser) {
            Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkUtilityMockInsufficientAccess());
            getErrorResults(Label.Access_Privileges_Veeva_Network_Error_MVN);
        }
    }

    /**
    * @testMethod
    * @description checks that gets Other error
    */
    @isTest static void testGetsOtherErrorResults() {
        System.runAs(testUser) {
            Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkUtilityMockOther());
            getErrorResults(Label.Other_Veeva_Network_Error_MVN);
        }
    }

    /**
    * @testMethod
    * @description checks that gets Veeva Network DCR Request bodies correctly
    */
    @isTest static void testGetsNetworkDCRRequestsBodyCorrectly() {
        List<Data_Change_Request_MVN__c> accountDCRs = [SELECT Id
                                                          FROM Data_Change_Request_MVN__c
                                                         WHERE Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_PENDING
                                                           AND DCR_External_ID_MVN__c = null
                                                           AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount];

        System.assert(!accountDCRs.isEmpty(), 'No pending Account DCRs without DCR External Id in the database.');

        List<VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN> networkDCRrequests;

        System.runAs(testUser) {
            Test.startTest();
            networkDCRrequests = VeevaNetworkConnectionUtilMVN.getNetworkDCRRequestsBody(accountDCRs, VeevaNetworkConnectionUtilMVN.DCR_PENDING);
            Test.stopTest();
        }

        System.assertNotEquals(null, networkDCRrequests, 'Veeva Network DCR Requests Body cannot be null.');
        System.assert(!networkDCRrequests.isEmpty(), 'Veeva Network DCR Requests Body cannot be empty.');

        System.assertEquals(accountDCRs.size(), networkDCRrequests.size(), 'List of DCR requests body must be same size as pending Account DCRs without External Id.');

        Boolean hasParent = false;
        Boolean hasAddress = false;

        for (VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN networkDCRrequest : networkDCRrequests) {
            System.assertNotEquals(null, networkDCRrequest.accountDCR, 'Account DCR cannot be null.');

            hasAddress = hasAddress || networkDCRrequest.addressDCR != null;
            hasParent = hasParent || networkDCRrequest.parentHcoDCR != null;
        }

        System.assert(hasAddress, 'None has Address DCR linked.');
        System.assert(hasParent, 'None has Parent DCR linked.');
    }

    /**
    * @testMethod
    * @description checks that calls Create Change Requests correctly
    */
    @isTest static void testCreateChangeRequestCorrectly() {
        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockSuccess());

        List<Data_Change_Request_MVN__c> accountDCRs = [SELECT Id,
                                                               Record_External_ID_MVN__c,
                                                               DCR_External_ID_MVN__c,
                                                               Status_MVN__c,
                                                               Country_MVN__c,
                                                               Notes_MVN__c,
                                                               Account_MVN__c,
                                                               Account_MVN__r.RecordType.DeveloperName,
                                                               Address_MVN__c,
                                                               RecordType.DeveloperName,
                                                               Parent_Account_MVN__c,
                                                               Parent_Account_MVN__r.RecordType.DeveloperName,
                                                               Account_MVN__r.IsPersonAccount,
                                                               (SELECT Id,
                                                                       Field_API_Name_MVN__c,
                                                                       New_Value_MVN__c,
                                                                       Old_Value_MVN__c
                                                                  FROM Data_Change_Request_Lines_MVN__r)
                                                          FROM Data_Change_Request_MVN__c
                                                         WHERE Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_PENDING
                                                           AND DCR_External_ID_MVN__c = null
                                                           AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount];

        System.assert(!accountDCRs.isEmpty(), 'No pending Account DCRs without DCR External Id in the database.');

        List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> createChangeRequestRslts = new List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN>();

        List<Data_Change_Request_MVN__c> accountDCRsWithLines = new List <Data_Change_Request_MVN__c>();

        for (Data_Change_Request_MVN__c accountDCR : accountDCRs) {
            if (accountDCR.Data_Change_Request_Lines_MVN__r != null && !accountDCR.Data_Change_Request_Lines_MVN__r.isEmpty()) {
                accountDCRsWithLines.add(accountDCR);
            }
        }

        System.assert(!accountDCRsWithLines.isEmpty(), 'No pending Account DCRs without DCR External Id and with Lines in the database.');

        System.runAs(testUser) {
            List<VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN> networkChangeRequestRecords =
                    VeevaNetworkConnectionUtilMVN.getNetworkDCRRequestsBody(accountDCRsWithLines, VeevaNetworkConnectionUtilMVN.DCR_PENDING);

            Test.startTest();
            for (VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN networkChangeRequestRecord : networkChangeRequestRecords) {
                System.assertNotEquals(null, networkChangeRequestRecord);
                System.assertNotEquals(null, networkChangeRequestRecord.accountDCR);
                System.assertNotEquals(null, networkChangeRequestRecord.accountDCR.Data_Change_Request_Lines_MVN__r);
                System.assert(!networkChangeRequestRecord.accountDCR.Data_Change_Request_Lines_MVN__r.isEmpty());

                VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRequest = new VeevaNetworkConnectionUtilMVN.NetworkRequestMVN(networkChangeRequestRecord);

                System.assertNotEquals(null, networkRequest);
                System.assertNotEquals(null, networkRequest.dcrRequest);
                System.assertNotEquals(null, networkRequest.dcrRequest.accountDCR);
                System.assertNotEquals(null, networkRequest.dcrRequest.accountDCR.Data_Change_Request_Lines_MVN__r);
                System.assert(!networkRequest.dcrRequest.accountDCR.Data_Change_Request_Lines_MVN__r.isEmpty());

                if (networkRequest.dcrRequest.parentHcoDCR != null) {
                    System.assertNotEquals(null, networkRequest.dcrRequest.parentHcoDCR.Data_Change_Request_Lines_MVN__r);
                    System.assert(!networkRequest.dcrRequest.parentHcoDCR.Data_Change_Request_Lines_MVN__r.isEmpty());
                }

                if (networkRequest.dcrRequest.addressDCR != null) {
                    System.assertNotEquals(null, networkRequest.dcrRequest.addressDCR.Data_Change_Request_Lines_MVN__r);
                    System.assert(!networkRequest.dcrRequest.addressDCR.Data_Change_Request_Lines_MVN__r.isEmpty());
                }
                createChangeRequestRslts.add(VeevaNetworkConnectionUtilMVN.createChangeRequest(networkRequest));
            }
            Test.stopTest();
        }

        System.assert(!createChangeRequestRslts.isEmpty(), 'Create Change Requests cannot be empty.');

        for (VeevaNetworkConnectionUtilMVN.NetworkResultMVN createChangeRequestRslt : createChangeRequestRslts) {
            System.assert(String.IsNotBlank(createChangeRequestRslt.Id), 'Error: ' + createChangeRequestRslt.error);
            System.assert(String.IsBlank(createChangeRequestRslt.error));
        }
    }

    /**
    * @testMethod
    * @description checks that call Create CHange Requests and gets an error
    */
    @isTest static void testCreateChangeRequestNoDCRLines() {
        List<Data_Change_Request_Line_MVN__c> dcrLines = [SELECT Id
                                                            FROM Data_Change_Request_Line_MVN__c
                                                           WHERE Data_Change_Request_MVN__c != null];

        delete dcrLines;

        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockSuccess());

        List<Data_Change_Request_MVN__c> accountDCRs = [SELECT Id,
                                                               Record_External_ID_MVN__c
                                                          FROM Data_Change_Request_MVN__c
                                                         WHERE Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_PENDING
                                                           AND DCR_External_ID_MVN__c = null
                                                           AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount];

        System.assert(!accountDCRs.isEmpty(), 'No pending Account DCRs without DCR External Id in the database.');

        List<VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN> networkChangeRequestRecords =
                VeevaNetworkConnectionUtilMVN.getNetworkDCRRequestsBody(accountDCRs, VeevaNetworkConnectionUtilMVN.DCR_PENDING);

        List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> createChangeRequestRslts = new List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN>();

        System.runAs(testUser) {
            Test.startTest();
            for (VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN networkChangeRequestRecord : networkChangeRequestRecords) {
                VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRequest = new VeevaNetworkConnectionUtilMVN.NetworkRequestMVN(networkChangeRequestRecord);
                createChangeRequestRslts.add(VeevaNetworkConnectionUtilMVN.createChangeRequest(networkRequest));
            }
            Test.stopTest();
        }

        System.assert(!createChangeRequestRslts.isEmpty(), 'Create Change Requests cannot be empty.');

        for (VeevaNetworkConnectionUtilMVN.NetworkResultMVN createChangeRequestRslt : createChangeRequestRslts) {
            System.assert(String.IsBlank(createChangeRequestRslt.Id));
            System.assertEquals(Label.No_DCR_Lines_Configured_Message_MVN, createChangeRequestRslt.error);
        }
    }

    /**
    * @testMethod
    * @description checks that calls Retrieve Change Requests correctly
    */
    @isTest static void testRetrieveChangeRequestCorrectly() {
        updateDCRsToSubmitted();

        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockCheckSuccess());

        List<Data_Change_Request_MVN__c> accountDCRs = [SELECT Id,
                                                               Record_External_ID_MVN__c,
                                                               DCR_External_ID_MVN__c,
                                                               Status_MVN__c,
                                                               Country_MVN__c,
                                                               Notes_MVN__c,
                                                               Account_MVN__c,
                                                               Account_MVN__r.RecordType.DeveloperName,
                                                               Address_MVN__c,
                                                               RecordType.DeveloperName,
                                                               Parent_Account_MVN__c,
                                                               Parent_Account_MVN__r.RecordType.DeveloperName,
                                                               Account_MVN__r.IsPersonAccount,
                                                               (SELECT Id,
                                                                       Field_API_Name_MVN__c,
                                                                       New_Value_MVN__c,
                                                                       Old_Value_MVN__c
                                                                  FROM Data_Change_Request_Lines_MVN__r)
                                                          FROM Data_Change_Request_MVN__c
                                                         WHERE Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_SUBMITTED
                                                           AND DCR_External_ID_MVN__c != null
                                                           AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount];

        System.assert(!accountDCRs.isEmpty(), 'No pending Account DCRs without DCR External Id in the database.');

        List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> retrieveChangeRequestRslts = new List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN>();

        List<Data_Change_Request_MVN__c> accountDCRsWithLines = new List <Data_Change_Request_MVN__c>();

        for (Data_Change_Request_MVN__c accountDCR : accountDCRs) {
            if (accountDCR.Data_Change_Request_Lines_MVN__r != null && !accountDCR.Data_Change_Request_Lines_MVN__r.isEmpty()) {
                accountDCRsWithLines.add(accountDCR);
            }
        }

        System.assert(!accountDCRsWithLines.isEmpty(), 'No pending Account DCRs without DCR External Id and with Lines in the database.');

        System.runAs(testUser) {
            List<VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN> networkChangeRequestRecords =
                    VeevaNetworkConnectionUtilMVN.getNetworkDCRRequestsBody(accountDCRsWithLines, VeevaNetworkConnectionUtilMVN.DCR_PENDING);

            Test.startTest();
            for (VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN networkChangeRequestRecord : networkChangeRequestRecords) {
                System.assertNotEquals(null, networkChangeRequestRecord);
                System.assertNotEquals(null, networkChangeRequestRecord.accountDCR);
                System.assertNotEquals(null, networkChangeRequestRecord.accountDCR.Data_Change_Request_Lines_MVN__r);
                System.assert(!networkChangeRequestRecord.accountDCR.Data_Change_Request_Lines_MVN__r.isEmpty());

                VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRequest = new VeevaNetworkConnectionUtilMVN.NetworkRequestMVN(networkChangeRequestRecord);

                System.assertNotEquals(null, networkRequest);
                System.assertNotEquals(null, networkRequest.dcrRequest);
                System.assertNotEquals(null, networkRequest.dcrRequest.accountDCR);
                System.assertNotEquals(null, networkRequest.dcrRequest.accountDCR.Data_Change_Request_Lines_MVN__r);
                System.assert(!networkRequest.dcrRequest.accountDCR.Data_Change_Request_Lines_MVN__r.isEmpty());

                if (networkRequest.dcrRequest.parentHcoDCR != null) {
                    System.assertNotEquals(null, networkRequest.dcrRequest.parentHcoDCR.Data_Change_Request_Lines_MVN__r);
                    System.assert(!networkRequest.dcrRequest.parentHcoDCR.Data_Change_Request_Lines_MVN__r.isEmpty());
                }

                if (networkRequest.dcrRequest.addressDCR != null) {
                    System.assertNotEquals(null, networkRequest.dcrRequest.addressDCR.Data_Change_Request_Lines_MVN__r);
                    System.assert(!networkRequest.dcrRequest.addressDCR.Data_Change_Request_Lines_MVN__r.isEmpty());
                }
                retrieveChangeRequestRslts.add(VeevaNetworkConnectionUtilMVN.retrieveChangeRequest(networkRequest));
            }
            Test.stopTest();
        }

        System.assert(!retrieveChangeRequestRslts.isEmpty(), 'Retrieve Change Requests cannot be empty.');

        for (VeevaNetworkConnectionUtilMVN.NetworkResultMVN retrieveChangeRequestRslt : retrieveChangeRequestRslts) {
            System.assertNotEquals(null, retrieveChangeRequestRslt.dcrs);
        }
    }

    /**
    * @testMethod
    * @description checks that calls Retrieve Change Requests and gets an error
    */
    @isTest static void testRetrieveChangeRequestNoDCRLines() {
        List<Data_Change_Request_Line_MVN__c> dcrLines = [SELECT Id
                                                            FROM Data_Change_Request_Line_MVN__c
                                                           WHERE Data_Change_Request_MVN__c != null];

        delete dcrLines;

        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockSuccess());

        List<Data_Change_Request_MVN__c> accountDCRs = [SELECT Id,
                                                               Record_External_ID_MVN__c
                                                          FROM Data_Change_Request_MVN__c
                                                         WHERE Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_PENDING
                                                           AND DCR_External_ID_MVN__c = null
                                                           AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount];

        System.assert(!accountDCRs.isEmpty(), 'No pending Account DCRs without DCR External Id in the database.');

        List<VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN> networkChangeRequestRecords =
                VeevaNetworkConnectionUtilMVN.getNetworkDCRRequestsBody(accountDCRs, VeevaNetworkConnectionUtilMVN.DCR_PENDING);

        List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> retrieveChangeRequestRslts = new List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN>();

        System.runAs(testUser) {
            Test.startTest();
            for (VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN networkChangeRequestRecord : networkChangeRequestRecords) {
                VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRequest = new VeevaNetworkConnectionUtilMVN.NetworkRequestMVN(networkChangeRequestRecord);
                retrieveChangeRequestRslts.add(VeevaNetworkConnectionUtilMVN.retrieveChangeRequest(networkRequest));
            }
            Test.stopTest();
        }

        System.assert(!retrieveChangeRequestRslts.isEmpty(), 'Create Change Requests cannot be empty.');

        for (VeevaNetworkConnectionUtilMVN.NetworkResultMVN retrieveChangeRequestRslt : retrieveChangeRequestRslts) {
            System.assert(String.IsBlank(retrieveChangeRequestRslt.Id));
            System.assertEquals(Label.No_DCR_Lines_Configured_Message_MVN, retrieveChangeRequestRslt.error);
        }
    }

    /**
    * @testMethod
    * @description checks that search correctly in Veeva Network
    */
    @isTest static void testGetsNetworkSearchResultCorrectly() {
        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockSearchSuccess());

        VeevaNetworkConnectionUtilMVN.NetworkSearchFilterMVN searchFilter = new VeevaNetworkConnectionUtilMVN.NetworkSearchFilterMVN(
            false,
            'LastName',
            'Test'
        );

        VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRqst = new VeevaNetworkConnectionUtilMVN.NetworkRequestMVN(
            'Account',
            null,
            'US',
            100,
            new List<VeevaNetworkConnectionUtilMVN.NetworkSearchFilterMVN> {searchFilter},
            true
        );

        List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> networkRslts;

        System.runAs(testUser) {
            Test.startTest();
            networkRslts = VeevaNetworkConnectionUtilMVN.searchNetworkData(networkRqst);
            Test.stopTest();
        }

        System.assertNotEquals(null, networkRslts, 'Veeva Network Result cannot be null.');
        System.assert(!networkRslts.isEmpty(), 'Veeva Network Results cannot be empty.');

        for (VeevaNetworkConnectionUtilMVN.NetworkResultMVN networkRslt : networkRslts) {
            System.assertNotEquals(null, networkRslt.entity, 'Entity cannot be null.');
            System.assertEquals('Test', String.valueOf(networkRslt.entity.get('LastName')), 'LastName must be \'Test\'.');
        }
    }

    /**
    * @testMethod
    * @description checks that search correctly in Veeva Network
    */
    @isTest static void testRetrievesRecordsCorrectly() {
        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockSearchExternalIdSuccess());

        VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRqst = new VeevaNetworkConnectionUtilMVN.NetworkRequestMVN(
            'Account',
            null,
            'US',
            new List<String>{
                '0000000000001',
                '0000000000002'
            }
        );

        List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> networkRslts;

        System.runAs(testUser) {
            Test.startTest();
            networkRslts = VeevaNetworkConnectionUtilMVN.retrieveEntities(networkRqst);
            Test.stopTest();
        }

        System.assertNotEquals(null, networkRslts, 'Veeva Network Result cannot be null.');
        System.assert(!networkRslts.isEmpty(), 'Veeva Network Results cannot be empty.');

        Set<String> resultIds = new Set<String> {
            'Network:Entity:0000000000001',
            'Network:Entity:0000000000002'
        };

        for (VeevaNetworkConnectionUtilMVN.NetworkResultMVN networkRslt : networkRslts) {
            System.assertNotEquals(null, networkRslt.entity, 'Entity cannot be null. Error: ' + networkRslt.error);
            String extId = String.valueOf(networkRslt.entity.get(VeevaNetworkConnectionUtilMVN.networkSettings.networkExternalIdFieldName));
            System.assert(resultIds.contains(extId), 'Wrong id: ' + extId + '.');
        }
    }

    /**
    * @description Gets and checks error message from callout response
    * @param    errorMessage    Expected contained error message
    * @example
    * * getErrorResults(errorMessage);
    */
    private static void getErrorResults(String errorMessage) {
        Boolean calloutFailed = false;

        Test.startTest();
        try {
            Map<String, Object> results = VeevaNetworkConnectionUtilMVN.getVeevaNetworkResponse(getRequest());
        } catch (UtilitiesMVN.utilException e) {
            System.assert(e.getMessage().contains(errorMessage), 'Expected contained message: '+ errorMessage +', All messages: '+ e.getMessage());
            calloutFailed = true;
        }
        Test.stopTest();

        System.assert(calloutFailed, 'Callout did not throw a UtilitiesMVN.utilException on error like it should');
    }

    /**
    * @description Gets callout request to Veeva Network
    * @result   Veeva Network Callout inner class data
    * @example
    * * VeevaNetworkConnectionUtilMVN.NetworkCalloutSettingMVN request = getRequest();
    */
    private static VeevaNetworkConnectionUtilMVN.NetworkCalloutSettingMVN getRequest() {
        VeevaNetworkConnectionUtilMVN.NetworkCalloutSettingMVN request = new VeevaNetworkConnectionUtilMVN.NetworkCalloutSettingMVN('Veeva_Network', 5000, null, null);
        request.addCalloutRequest('/change_request', 'POST', null);
        request.body = '{}';

        return request;
    }

    private static void updateDCRsToSubmitted() {
        List<Data_Change_Request_MVN__c> dcrs = [SELECT Id,
                                                        Status_MVN__c,
                                                        Account_MVN__c,
                                                        Parent_Account_MVN__c,
                                                        Parent_Data_Change_Request_MVN__c,
                                                        (SELECT Id
                                                           FROM Data_Change_Requests_MVN__r)
                                                   FROM Data_Change_Request_MVN__c
                                                  WHERE Status_MVN__c =: VeevaNetworkConnectionUtilMVN.DCR_PENDING];

        String extId;
        Set<String> extIdsVisited = new Set<String>();
        for (Data_Change_Request_MVN__c dcr : dcrs) {
            dcr.Status_MVN__c = VeevaNetworkConnectionUtilMVN.DCR_SUBMITTED;

            if (dcr.Address_MVN__c != null) {
                extId = 'dadi' + dcr.Address_MVN__c;
            } else if (dcr.Parent_Account_MVN__c != null) {
                extId = 'dpai' + dcr.Parent_Account_MVN__c;
            } else if (dcr.Account_MVN__c != null) {
                extId = 'dai' + dcr.Account_MVN__c;

                if(dcr.Data_Change_Requests_MVN__r != null && !dcr.Data_Change_Requests_MVN__r.isEmpty()) {
                    extId = 'daadi' + dcr.Account_MVN__c;
                }
            }

            if (!extIdsVisited.contains(extId)) {
                dcr.DCR_External_ID_MVN__c = extId;
            } else {
                while (extIdsVisited.contains(extId)) {
                    extId = extId + 1;
                }

                dcr.DCR_External_ID_MVN__c = extId;
            }

            extIdsVisited.add(extId);
        }

        update dcrs;

        List<AggregateResult> agDCRs = [SELECT Count(Id) countId
                                            FROM Data_Change_Request_MVN__c
                                           WHERE Status_MVN__c =: VeevaNetworkConnectionUtilMVN.DCR_SUBMITTED];

        System.assertEquals(agDCRs[0].get('countId'), dcrs.size());
    }
}