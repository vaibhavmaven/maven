/**
 * TestFactoryMDMValueMappingMVN
 * @author: Pablo Roldan
 * @created Date: October 2017
 * @description: This class is used to construct MDM Field Value mapping custom metadatas
 */
 @isTest
public with sharing class TestFactoryMDMValueMappingMVN {

    public static void setMocks() {
        setMocks(null);
    }
    
    public static void setMocks(Map<String, Object> fieldValues) {
        List<String> mdmFieldMappingIds = new List<String>{'000000000000001',
                                                           '000000000000002',
                                                           '000000000000003',
                                                           '000000000000004',
                                                           '000000000000005',
                                                           '000000000000006',
                                                           '000000000000007',
                                                           '000000000000008',
                                                           '000000000000009',
                                                           '000000000000010',
                                                           '000000000000011',
                                                           '000000000000012'};

        TestFactoryMDMFieldMappingMVN.setMocks(mdmFieldMappingIds, null);
    
        List<MDM_Value_Mapping_MVN__mdt> settings = buildMockSettings(mdmFieldMappingIds, fieldValues);

        MDMValueMappingMVN.setMocks(settings);
    }

    private static List<MDM_Value_Mapping_MVN__mdt> buildMockSettings(List<String> mdmFieldMappingIds, Map<String, Object> fieldValues) {
        List<MDM_Value_Mapping_MVN__mdt> settings = new List<MDM_Value_Mapping_MVN__mdt>();
        
        Integer mdmFieldMappingIdsIndex = 0;
        // Address: Name - address_line_1__v
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'StreetSF', 'StreetMDM', true, true));
        mdmFieldMappingIdsIndex++;
        // Account: RecordTypeId - entityType
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'Professional_vod', 'HCP', true, false));
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'Hospital_vod', 'HCO', true, false));
        mdmFieldMappingIdsIndex++;
        // Address: RecordTypeId - entityType
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'Address_MVN', 'Address', true, false));
        mdmFieldMappingIdsIndex++;
        // Account: Name - corporate_name__v
        // none
        mdmFieldMappingIdsIndex++;
        // Account: LastName - last_name__v
        // none
        mdmFieldMappingIdsIndex++;
        // Account: MiddleName - middle_name__v
        // none
        mdmFieldMappingIdsIndex++;
        // Account: FirstName - first_name__v
        // none
        mdmFieldMappingIdsIndex++;
        // Account: Gender_MVN__c - gender__v
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'M', 'M', true, true));
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'F', 'F', true, true));
        mdmFieldMappingIdsIndex++;
        // Account: Specialty_1_vod__c - specialty_1__v
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'Internal Medicine', 'IM', true, true));
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'General Practice', 'GS', true, true));
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'Family Medicine', 'FM', true, true));
        mdmFieldMappingIdsIndex++;
        // Account: Country_MVN__c - primary_country__v
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'US', 'US', true, true));
        mdmFieldMappingIdsIndex++;
        // Address: Country_vod__c - country__v
        settings.add(buildMockSetting(mdmFieldMappingIds[mdmFieldMappingIdsIndex], fieldValues, 'US', 'US', true, true));
        mdmFieldMappingIdsIndex++;
        // Address: Phone - phone_1__v
        // none

        return settings;
    }

    private static MDM_Value_Mapping_MVN__mdt buildMockSetting(String mdmFieldMappingId, 
                                                 Map<String, Object> fieldValues, 
                                                              String sfValue, 
                                                              String mdmValue, 
                                                             Boolean isInbound,
                                                             Boolean isOutbound) {
        Map<String, Object> setting = new Map<String, Object> {
            'MDM_Field_Mapping_MVN__c' => mdmFieldMappingId,
            'Salesforce_Value_MVN__c' => sfValue,
            'MDM_Value_MVN__c' => mdmValue,
            'Inbound_MVN__c' => isInbound,
            'Outbound_MVN__c' => isOutbound
        };

        if (fieldValues != null) {
            setting.putAll(fieldValues);
        }

        return (MDM_Value_Mapping_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata('MDM_Value_Mapping_MVN__mdt', setting);
    }

}