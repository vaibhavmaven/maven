global class PAP_BatchNotificationEmail  implements Database.Batchable<sObject> , Database.allowsCallouts{

    global  List<Program_Member_MVN__c> start(Database.BatchableContext bc){
        
       
        
        
        List<Program_Member_MVN__c> pgmOmniChannelList =  [SELECT Id,
                                                           Fax_Delivery_JKC__c,
                                                           Fax_Retry_Identifier_JKC__c, 
                                                           Fax_Sent_Time_JKC__c 
                                                           FROM Program_Member_MVN__c
                                                           where  Id In (Select Program_Member_JKC__c from Omnichannel_JKC__c
                                                                         where Status_JKC__c Like '%Error%' ) ];
        
        List<Program_Member_MVN__c> pgmMemberContactList =  [SELECT Id, Fax_Delivery_JKC__c, Fax_Retry_Identifier_JKC__c, Fax_Sent_Time_JKC__c
                                                             FROM Program_Member_MVN__c where Program_Member_MVN__c.Member_MVN__c
                                                             In (SELECT AccountId FROM Contact where IsEmailBounced = true ) 
                                                             and Notified_Patient_JKC__c = true  ];
        
        
        List<Program_Member_MVN__c> pgmListFinal = new List<Program_Member_MVN__c>(); 
      
        pgmListFinal.addAll(pgmOmniChannelList);
        pgmListFinal.addAll(pgmMemberContactList);
        return pgmListFinal;
        
    }
    global void execute(Database.BatchableContext BC, List<Program_Member_MVN__c> pgmMember) {
        
        
        
        // Patient
        List<Program_Member_MVN__c> pgmOmniChannelList =  [SELECT Id,
                                                           Fax_Delivery_JKC__c,
                                                           Fax_Retry_Identifier_JKC__c, 
                                                           Fax_Sent_Time_JKC__c 
                                                           FROM Program_Member_MVN__c
                                                           where  Id In (Select Program_Member_JKC__c from Omnichannel_JKC__c
                                                                         where Status_JKC__c Like '%Error%' ) ];
        
        for(Program_Member_MVN__c pgm: pgmOmniChannelList){
            system.debug('88888 ');
            sendNotification_Patient(pgm.Id,'Email');
        }
        
        List<Program_Member_MVN__c> pgmMemberContactList =  [SELECT Id,
                                                             Fax_Delivery_JKC__c,
                                                             Fax_Retry_Identifier_JKC__c, 
                                                             Fax_Sent_Time_JKC__c 
                                                             FROM Program_Member_MVN__c
                                                             where  Program_Member_MVN__c.Member_MVN__c
                                                             In (SELECT AccountId FROM Contact  
                                                                 where IsEmailBounced  = true ) ];
        
        for(Program_Member_MVN__c pgm: pgmMemberContactList){
            sendNotification_Patient(pgm.Id,'Mail');
        }
    }    
    global void finish(Database.BatchableContext bc) {
        
    }  
    
    webservice static void sendNotification_Patient(String pgmMemberID, String Delivery){   
        
        Program_Member_MVN__c pgmMemberObj = getPgmMemberDetails(pgmMemberID);
        Boolean IsSMS ;
        Boolean IsAutoDial ;  
        String deliveryType ='';
        Boolean terminationFlag = false;
        Contact_Information_MVN__c conInfo;
        String sendPatientResult ='';
        String invokeMethod ='';
        
        try{ 
            if ((String.isNotEmpty(pgmMemberObj.Member_MVN__r.Primary_Email_MVN__c)) 
                && (pgmMemberObj.Program_MVN__r.Email_JKC__c == true)
                && pgmMemberObj.Reprocessed_Patient_JKC__c ==false // do we need to check Email_JKC__c 
                && Delivery == 'Email'
               ){
                   SYSTEM.debug('Email: '); 
                   deliveryType ='Email';
                   sendToNintex_Patient(pgmMemberObj,deliveryType);                
                   sendPatientResult = 'Success';
                   
               }
            if(sendPatientResult == 'Success'){
                updateProgramMember(pgmMemberObj.Id,True);
                
            }
            /*  else If (String.IsNotBlank(pgmMemberObj.Member_MVN__r.Phone) && pgmMemberObj.Program_MVN__r.AutoDialer_JKC__c == true) {
SYSTEM.debug('AutoDial: '); 
IsSMS = false;
IsAutoDial = true; 
CreateOmniChannelRecords(pgmMemberObj, IsSMS, IsAutoDial);
sendPatientResult = 'Success';
}
*/
            if (String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingStreet) 
                && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingCity) 
                && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingPostalCode) 
                && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingStateCode) 
                && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingCountryCode)
                && (pgmMemberObj.Program_MVN__r.Mail_JKC__c == true )
                && pgmMemberObj.Reprocessed_Patient_JKC__c ==true
                && deliveryType == 'Mail'
               ){
                   SYSTEM.debug('Mail: '); 
                   deliveryType ='Mail';
                   sendToNintex_Patient(pgmMemberObj,deliveryType);                        
                   sendPatientResult = 'Success';
               } 
            else{
                sendPatientResult = 'Invalid Address';
                //Update the Notification flag to true if invoked from button
            }
            if(sendPatientResult == 'Success'){
                updateProgramMember(pgmMemberObj.Id,True);
                
            }
            
            system.debug('*********** sendPatientResult '+sendPatientResult);
            
        }
        catch(Exception ex){ 
            
            System.debug('ErrorMessage' +ex.getMessage());
        }
        
    }
    
    
    public static Program_Member_MVN__c getPgmMemberDetails(String pgmMemberID){
        system.debug('Program member ID '+pgmMemberID);
        return [SELECT Id,
                Name,
                Program_Name_MVN__c,
                Program_Id_MVN__c,
                Fax_Attempt_Counter_JKC__c,
                Program_MVN__r.SMS_JKC__c,
                Program_MVN__r.AutoDialer_JKC__c,
                Program_MVN__r.Email_JKC__c,
                Program_MVN__r.Mail_JKC__c,
                Program_MVN__r.Fax_JKC__c,
                Patient_Status_MVN__c,                
                Patient_Status_Date_JKC__c,
                Reprocessed_HCP_JKC__c,
                Reprocessed_Patient_JKC__c,
                Active_Application_MVN__c,
                Active_Application_Status_MVN__c,
                Active_Application_Expiry_Date_JKC__c,
                Member_MVN__r.FirstName,
                Member_MVN__r.LastName,
                Member_MVN__r.phone,
                Member_MVN__r.PersonMobilePhone,
                Member_MVN__r.Primary_Email_MVN__c,
                Member_MVN__r.Fax,
                Member_MVN__r.BillingStreet,
                Member_MVN__r.BillingCity,
                Member_MVN__r.BillingStateCode,
                Member_MVN__r.BillingPostalCode,                
                Member_MVN__r.BillingCountry,
                Member_MVN__r.BillingState,
                Member_MVN__r.BillingCountryCode,
                Physician_MVN__r.Id,
                Physician_MVN__r.FirstName,
                Physician_MVN__r.LastName,
                Physician_MVN__r.Fax,
                Physician_MVN__r.Primary_Email_MVN__c,
                Physician_MVN__r.BillingStreet,
                Physician_MVN__r.BillingCity,
                Physician_MVN__r.BillingStateCode,
                Physician_MVN__r.BillingPostalCode,
                Physician_MVN__r.BillingCountryCode
                from Program_Member_MVN__c where Id=:pgmMemberID] ;      
    }
    
    
    private static void sendToNintex_Patient(Program_Member_MVN__c pgmMemberObj,String deliveryType){
        
        System.debug('delivery type '+deliveryType);
        String appStatus = pgmMemberObj.Active_Application_Status_MVN__c;        
        
        if (appStatus == System.Label.Application_Status_Fulfilled_MVN)
        {
            appStatus = 'Approval';
        }
        else if(appStatus == System.Label.Application_Status_Denied_JKC)
        {
            appStatus = 'Denial';
        }
        else if (appStatus == System.Label.Application_Status_MissingInformation_JKC)
        {
            appStatus = 'MissingInformation';
        }
        //	When “Patient Status” changes to “Opt-Out” or like “Terminated%” send the termination notification 
        if (pgmMemberObj.Patient_Status_MVN__c != System.Label.PM_Patient_Status_Opt_In_MVN)
        {
            appStatus = 'Termination';
        }
        system.debug('app status '+appStatus);
        String Filter = pgmMemberObj.Program_Id_MVN__c+'-PM-'+  appStatus + '-Patient';     
        //  String Filter =  'Bausch_PAP_JKC-PM-Approval-Patient';
        
        Application_MVN__c  applications = [SELECT Id,
                                            Application_Reminder_Date_MVN__c,
                                            Application_Reminder_Follow_Up_Date_MVN__c
                                            FROM
                                            Application_MVN__c
                                            WHERE
                                            Id =: pgmMemberObj.Active_Application_MVN__c limit 1];
        if (applications.Application_Reminder_Date_MVN__c <= Date.today()){
            Filter =  pgmMemberObj.Program_Id_MVN__c + '-PM-Re-Enrollment-Patient';
        }
        if (applications.Application_Reminder_Follow_Up_Date_MVN__c <= Date.today()) {
            Filter =  pgmMemberObj.Program_Id_MVN__c + '-PM-Re-Enrollment-Patient-Followup';
        }
        
        Contact contactObj = [SELECT Id,Email FROM contact WHERE AccountId =: pgmMemberObj.Member_MVN__c];
        system.debug('Contactemail '+contactObj.Email);
        system.debug('ContactId '+contactObj.Id);
        system.debug('Filter '+Filter);
        Loop__DDP__c docGenPackage = [SELECT ID FROM Loop__DDP__c WHERE Loop__Filter__c =: Filter];
        
        SYSTEM.debug('docGen '+docGenPackage);
        system.debug('delivery type '+deliveryType);
        Loop__DDP_Integration_Option__c docGenDelivery = [SELECT Id FROM Loop__DDP_Integration_Option__c 
                                                          WHERE Name =: deliveryType 
                                                          AND Loop__DDP__c IN 
                                                          (SELECT ID FROM Loop__DDP__c WHERE 
                                                           Loop__Filter__c =: Filter)];
        system.debug('****** docGenDelivery '+docGenDelivery);
        Loop.loopMessage thisLoopMessage = new Loop.loopMessage(); 
        thisLoopMessage  = new Loop.loopMessage();     
        thisLoopMessage.sessionId = UserInfo.getSessionId();        
        thisLoopMessage.requests.add(new Loop.loopMessage.loopMessageRequest(
            pgmMemberObj.Id,           
            docGenPackage.Id,
            new Map<string, string>{                 
                'deploy' =>  docGenDelivery.Id,
                    'SFContact'=>  contactObj.Id
                    }
        ));
        thisLoopMessage.sendAllRequests();
        
    }
    
    private static void updateProgramMember(String PgmMemberId, Boolean Reprocess){
        List<Program_Member_MVN__c> PgmMemberList = [select id,Reprocessed_HCP_JKC__c,
                                                     Queued_Fax_Status_JKC__c, 
                                                     Queued_Fax_Result_JKC__c, 
                                                     Fax_Delivery_JKC__c,
                                                     Fax_Attempt_Counter_JKC__c,
                                                     Sub_Status_JKC__c,
                                                     Fax_Retry_Identifier_JKC__c,Fax_Sent_Time_JKC__c 
                                                     from 
                                                     Program_Member_MVN__c 
                                                     where 
                                                     ID =:PgmMemberId
                                                     LIMIT 1];
        
        PgmMemberList[0].Reprocessed_HCP_JKC__c = true;
        update PgmMemberList[0];
    }
    
    private static void updateProgramMembers(String PgmMemberId){
        List<Program_Member_MVN__c> PgmMemberList = [select id,Reprocessed_HCP_JKC__c,
                                                     Queued_Fax_Status_JKC__c, 
                                                     Queued_Fax_Result_JKC__c, 
                                                     Fax_Delivery_JKC__c,
                                                     Fax_Attempt_Counter_JKC__c,
                                                     Sub_Status_JKC__c,
                                                     Fax_Retry_Identifier_JKC__c,Fax_Sent_Time_JKC__c 
                                                     from 
                                                     Program_Member_MVN__c 
                                                     where 
                                                     ID =:PgmMemberId
                                                     LIMIT 1];
        
        PgmMemberList[0].Is_Prescription_Generated_JKC__c = true;
        PgmMemberList[0].Document_Generated_Date_JKC__c =system.now();
        
        update PgmMemberList[0];
    }
    

}