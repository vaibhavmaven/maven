/**
 *	EnrollmentFlowControllerTestMVN
 *	Created By:		Roman Lerman
 *	Created On:		3/30/2016
 *  Description:	this class is the controller for the EnrollmentFlowMVN VF page
 *
 **/
@isTest
private class EnrollmentFlowControllerTestMVN {
    static User manager;
    static Case cs;
    static Program_MVN__c program;
    static Account patient;
    static Address_vod__c patientAddress;
    static Program_Member_MVN__c member;

    static {
        TestDataFactoryMVN.createPSSettings();
        manager = TestDataFactoryMVN.generateCaseManager();
        insert manager;

        System.runAs(manager) {
            cs = TestDataFactoryMVN.createTestCase();

            program = TestDataFactoryMVN.createProgram();
            program.Active_MVN__c = true;
            update program;

            patient = TestDataFactoryMVN.createMember();

            patientAddress = TestDataFactoryMVN.createTestAddress(patient);

            Attachment attachment = new Attachment();
            attachment.Name = 'Unit Test Attachment';
            attachment.Body = Blob.valueOf('Unit Test Attachment Body');
            attachment.ParentId = cs.id;
            insert attachment;

            member = TestDataFactoryMVN.createProgramMember(program, patient);
        }
    }

    @isTest static void testEnrollmentFlow() {
        System.CurrentPageReference().getParameters().put('caseId', cs.Id);

        ApexPages.StandardController stdController = new ApexPages.StandardController(cs);
        EnrollmentFlowControllerMVN enrollmentController = new EnrollmentFlowControllerMVN(stdController);

        enrollmentController.selectedProgramId = program.Id;
        enrollmentController.thisCase.No_Physician_MVN__c = true;

        enrollmentController.updateCase();

        System.assertEquals(program.Id, enrollmentController.viewCase.Program_MVN__c);
        System.assert(!enrollmentController.hasMedicalHistoryTypes);
        System.assertEquals(true, enrollmentController.viewCase.No_Physician_MVN__c);

        enrollmentController.thisCase.Program_Member_MVN__c = member.Id;
        System.assertEquals(program.Id, enrollmentController.selectedProgramId);
    }

    @isTest
    static void testItShouldGetIsEnrollmentFromURL() {
        System.runAs(manager) {
            System.CurrentPageReference().getParameters().put('caseId', cs.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(cs);
            EnrollmentFlowControllerMVN controller = new EnrollmentFlowControllerMVN(sc);

            Test.startTest();
            System.assertEquals(false, controller.isEnrolling);
            System.currentPageReference().getParameters().put('isEnrollmentFlow', 'true');
            System.assertEquals(true, controller.isEnrolling);
            Test.stopTest();
        }
    }

    @isTest
    static void testItShouldThrowExceptionOnCaseUpdate() {
        EnrollmentFlowControllerMVN controller;

        System.runAs(manager) {
            System.CurrentPageReference().getParameters().put('caseId', cs.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(cs);
            controller = new EnrollmentFlowControllerMVN(sc);
        }

        delete controller.viewCase;

        System.runAs(manager) {
            Test.startTest();
            controller.updateCase();
            Test.stopTest();

            System.assertEquals(1, ApexPages.getMessages().size());
        }
    }
}