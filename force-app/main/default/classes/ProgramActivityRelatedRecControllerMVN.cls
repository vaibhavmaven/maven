/**
 *  ProgramActivityRelatedRecControllerMVN
 *  Created By:  Rick Locke
 *  Created On:  August 2017
 *  Description: Controller for the Program Activity Related Records component.
 */
public with sharing class ProgramActivityRelatedRecControllerMVN extends ProgramActivityComponentControllerMVN {

    public List<SObject> records {get;set;}

    public String relatedRecordId {get;set;}
    public SObject relatedRecord {
        get {
            if(masterController.componentObjectMap.containsKey(componentRecord.Id)) {
                relatedRecord = masterController.componentObjectMap.get(componentRecord.Id);
                relatedRecordId = relatedRecord.Id;
            } else {
                Schema.SobjectType objectType = globalDescribe.get(componentRecord.SObject_MVN__c);
                relatedRecord = objectType.newSObject();
                relatedRecord.put(componentRecord.Program_Member_Id_Field_MVN__c, masterController.programMember.Id);
                if(hasRecordType) {
                    relatedRecord.put('RecordTypeId',RecordTypeId);
                }
            }            
            return relatedRecord;
        }
        set {
            relatedRecord = value;
            masterController.componentObjectMap.put(componentRecord.Id, relatedRecord);
        }
    }

    /**
     * Did the update in the modal generate an error(s)?
     * @return Boolean
     */    
    public Boolean hasErrors { 
        get {
            return ApexPages.hasMessages();
        }
        set; 
    } 

    /**
     * Constructor
     */
    public ProgramActivityRelatedRecControllerMVN() {
    }

    /**
     * Queries the related records to set the records that appear in the component. 
     */
    public override void setObjectRecords() {
        records = Database.query(getFieldSetSOQL());
    }

    /**
     * Called from the modal save button to update the related record in the modal.
     * @return PageReference
     */
    public PageReference saveRelatedRecord() {

        Map<String,Object> fieldToValueMap = relatedRecord.getPopulatedFieldsAsMap();

        Boolean hasError = false;
        Boolean hasNoFieldsFilled = true;

        for(FieldSetMember field: componentFieldSet.getFields()) {
            if((field.getDBRequired() || field.getRequired()) && fieldToValueMap.get(field.getFieldPath()) == null) {
                // add a message emulating Salesforce error.
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, field.getLabel() + ': You must enter a value.')); 
                hasError = true;
            }
            if(fieldToValueMap.get(field.getFieldPath()) != null) {
                hasNoFieldsFilled = false;
            }
        }

        if (hasNoFieldsFilled) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Activity_No_Fields_Filled)); 
            hasError = true;
        }

        if (hasError) {
            return null;
        } else {
            Savepoint sp = Database.setSavepoint();
            try {
                update relatedRecord;
             } catch (Exception e) {
                Database.rollback(sp);
                System.debug('#### Save from saveRelatedRecord failed : ' + e);
                ApexPages.addMessages(e);
            }
            return null;
        }
    }



    /**
     * This method is called when a user clicks on the command link in the table of records. It is used to find the relatedRecord for editing.
     */
    public void findRelatedRecord() {
        if(relatedRecordId != null) {
            if(masterController.componentObjectMap.containsKey(componentRecord.Id) && masterController.componentObjectMap.get(componentRecord.Id).Id == relatedRecordId) {
                // grab the related record out of the masterController if it's already there
                relatedRecord = masterController.componentObjectMap.get(componentRecord.Id);
            } else {
                for(SObject record : records) {
                    if(record.Id == relatedRecordId) {
                        relatedRecord = record;
                    }
                }
            }
        }
    }
}