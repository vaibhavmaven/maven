/**
 * @author Mavens
 * @date Sept 2018
 * @description When a Nintex doc is generated and sent via SFTP, a field on the program member is updated.
 *              We use this field update to fire the attachment of that doc on the Program Member.
 *              We need to do this due to a Nintex limitation.
 *              For re-enrollment notifications we also update datetime fields on the program member.
 * @group Documents
 */
public with sharing class CreateNintexAttachmentMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
     * @description handle
     */
    public void handle() {
        Set<Id> programMemberApplicationReminder = new Set<Id>();
        Set<Id> programMemberApplicationReminderFollowUp = new Set<Id>();
        for (Id programMemberId : trigger.newMap.keySet()) {
            if (this.nintexDeliveryIdHasChanged(programMemberId)) {
                String docGenPackageFilter = this.generateAttachment(programMemberId);
                if (docGenPackageFilter.contains('-PM-Re-Enrollment')) {
                    if (docGenPackageFilter.contains('-Followup')) {
                        programMemberApplicationReminderFollowUp.add(programMemberId);
                    } else {
                        programMemberApplicationReminder.add(programMemberId);
                    }
                }
            }
        }
        updateApplicationReminderSentFields(programMemberApplicationReminder, programMemberApplicationReminderFollowUp);
    }

    /**
     * Update Datetime fields on the application that store when the re-enrollment reminder and followup
     * letters have been sent.
     * @param  programMemberIdsReminders Program Member Ids of the Active Applications to be updated for reminder letters
     * @param  programMemberIdsFollowUps Program Member Ids of the Active Applications to be updated for followup letters
     */ 
    private void updateApplicationReminderSentFields(Set<Id> programMemberIdsReminders, Set<Id> programMemberIdsFollowUps) {
        List<Application_MVN__c> applicationListReminder = [
            SELECT
                Id
            FROM 
                Application_MVN__c
            WHERE
                Program_Member_MVN__c = :programMemberIdsReminders
            AND 
                Is_Application_Active_MVN__c = true
        ];
        for (Application_MVN__c application : applicationListReminder) {
            application.Application_Reminder_Sent_MVN__c = Datetime.now();
            application.Application_Reminder_Date_MVN__c = null;
        }
        List<Application_MVN__c> applicationListFollowUp = [
            SELECT
                Id
            FROM 
                Application_MVN__c
            WHERE
                Program_Member_MVN__c = :programMemberIdsFollowUps
            AND 
                Is_Application_Active_MVN__c = true
        ];
        for (Application_MVN__c application : applicationListFollowUp) {
            application.Application_Reminder_Follow_Up_Sent_MVN__c = Datetime.now();
            application.Application_Reminder_Follow_Up_Date_MVN__c = null;
        }
        applicationListReminder.addAll(applicationListFollowUp);
        update applicationListReminder;
    }

    /**
     * @description returns true if the Nintex delivery Datetime has changed
     * @param Id of the program member
     * @return Boolean true if the field has changed
     */
    private Boolean nintexDeliveryIdHasChanged(Id programMemberId) {
        Program_Member_MVN__c oldProgramMember = (Program_Member_MVN__c)trigger.oldMap.get(programMemberId);
        Program_Member_MVN__c newProgramMember = (Program_Member_MVN__c)trigger.newMap.get(programMemberId);
        return ((oldProgramMember.Nintex_Delivery_Datetime_MVN__c != newProgramMember.Nintex_Delivery_Datetime_MVN__c) 
            && (newProgramMember.Nintex_Delivery_Datetime_MVN__c != null) 
            && (newProgramMember.Nintex_Delivery_Id_MVN__c != null));
    }

    /**
     * Generate the Nintex document and attach it to the program member
     * @param  programMemberId Program Member Id
     * @return  filter value of the package generating this attachment
     */
    private String generateAttachment(Id programMemberId) {
        Id storedDeliveryId = getDeliveryIdFromProgramMember(programMemberId);
        Loop__DDP_Integration_Option__c deliveryOptionRecord = getPackageInformation(storedDeliveryId);
        Id docGenPackageId;
        if (deliveryOptionRecord != null) {
            docGenPackageId = deliveryOptionRecord.Loop__DDP__c;
        }
        Id deliveryOptionId = getDeliveryId(docGenPackageId);

        if (deliveryOptionId != null) {
            callNintex(programMemberId, docGenPackageId, deliveryOptionId, UserInfo.getSessionId());
            return deliveryOptionRecord.Loop__DDP__r.Loop__Filter__c;
        }
        return null;
    }

    /**
     * Retrieve the SFTP delivery Id stored on the program member
     * @param  programMemberId program member Id
     * @return delivery Id for the SFTP method 
     */ 
    private Id getDeliveryIdFromProgramMember(Id programMemberId) {
        return [
            SELECT 
                Nintex_Delivery_Id_MVN__c
            FROM
                Program_Member_MVN__c
            WHERE
                Id = :programMemberId
        ].Nintex_Delivery_Id_MVN__c;
    }

    /**
     * Retrieve the DocGen Package data
     * @param  deliveryId Id of the delivery Id option linked to DocGen Package
     * @return DocGen Package Delivery Option record
     */
    private Loop__DDP_Integration_Option__c getPackageInformation (Id deliveryId) {
        return [
            SELECT 
                Loop__DDP__c,
                Loop__DDP__r.Loop__Filter__c
            FROM 
                Loop__DDP_Integration_Option__c
            WHERE 
                Id = :deliveryId
            LIMIT 1
        ];
    }

    /**
    * Get delivery Id for the attachment delivery option
    * @param  packageId Package Id 
    * @return delivery option Id for Attachment
    */ 
    private Id getDeliveryId(Id packageId) {
        List<Loop__DDP_Integration_Option__c> deliveryOptions = [
            SELECT
                Id
            FROM
                Loop__DDP_Integration_Option__c
            WHERE 
                Loop__DDP__c = :packageId
            AND 
                RecordType.Name = 'Attach'
            AND 
                Loop__Attach_As__c = 'Attachment'
            LIMIT 1
        ];
        if (!deliveryOptions.isEmpty()) {
            return deliveryOptions.get(0).Id;
        }
        return null;
    }

    /**
     * Callout to Nintex server to generate the attachment doc on the program member
     * @param  programMemberId  program member Id
     * @param  docGenPackageId  DocGen Package Id to be exeucted
     * @param  deliveryOptionId delivery option id for the attachment method 
     */ 
    @future(callout=true)
    private static void callNintex(Id programMemberId, Id docGenPackageId, Id deliveryOptionId, String sessionId) {
        Loop.loopMessage thisLoopMessage = new Loop.loopMessage();
        thisLoopMessage.sessionId = sessionId;
        thisLoopMessage.requests.add(new Loop.loopMessage.loopMessageRequest(
            programMemberId, 
            docGenPackageId, 
            new Map<string, string>{
                'deploy' => deliveryOptionId 
            }
        ));
        thisLoopMessage.sendAllRequests();
    }
}