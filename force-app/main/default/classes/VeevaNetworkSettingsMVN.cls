/**
* @author Mavens
* @group Network API
* @description: Wrapper class to manage retrieving the Veeva Network Settings custom metadata type
*/
public with sharing class VeevaNetworkSettingsMVN {

    /**
    * @description Custom metadata wrapper for VeevaNetworkSettings custom metadata
    */
    public Veeva_Network_Settings_MVN__mdt setting { get; private set; }

    /**
    * @description Custom metadata wrapper for VeevaNetworkStatusSettings custom metadata
    */
    public VeevaNetworkStatusSettingsMVN statusSettings { get; private set; }

    /**
    * @description Test mock Custom metadata wrapper for VeevaNetworkSettings custom metadata
    */
    private static Veeva_Network_Settings_MVN__mdt testMock;

    /**
    * @description Default VeevaNetworkSettings name for custom metadata
    */
    public static final String DEFAULT_NAME = 'Default';

    /**
    * @description Constructor which gets VeevaNetworkSettings custom metadata according Name passed.
    * @param    settingName VeevaNetworkSettings custom metadata name
    * @example
    * * VeevaNetworkSettingsMVN setting = new VeevaNetworkSettingsMVN(settingName);
    */
    public VeevaNetworkSettingsMVN(String settingName) {
        Map<String, Schema.SObjectField> networkSettingSchemaFields = Veeva_Network_Settings_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
        List<String> networkSettingFields = new List<String>(networkSettingSchemaFields.keySet());


        Map<String, Schema.SObjectField> networkStatusSettingSchemaFields = Veeva_Network_Status_Settings_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
        List<String> networkStatusSettingFields = new List<String>(networkStatusSettingSchemaFields.keySet());

        String queryString = 'SELECT ' + String.escapeSingleQuotes(String.join(networkSettingFields, ',')) +
                ' , (SELECT ' + String.escapeSingleQuotes(String.join(networkStatusSettingFields, ',')) + ' FROM Veeva_Network_Status_Settings__r)' +
                ' FROM Veeva_Network_Settings_MVN__mdt' +
                ' LIMIT 1000';

        List<Veeva_Network_Settings_MVN__mdt> allSettings = Database.query(queryString);

        setting = getSettingByName(allSettings, settingName);

        statusSettings = setting != null ?
                new VeevaNetworkStatusSettingsMVN(setting.Veeva_Network_Status_Settings__r) :
                null;

        if(testMock != null) {
            setting = testMock;
        }
    }

    /**
    * @description Sets test mock custom metadata wrapper for VeevaNetworkSettings
    * @param    mock    Test custom metadata wrapper for VeevaNetworkSettings
    * @example
    * * setMock(mock);
    */
    @TestVisible
    private static void setMock(Veeva_Network_Settings_MVN__mdt mock){
        testMock = mock;
    }

    /**
    * @description Gets Custom metadata from a list using a custom metadata name
    * @param    allSettings List of custom metadata wrappers for VeevaNetworkSettings
    * @param    settingName VeevaNetworkStatusSettings custom metadata name
    * @return               Custom metadata wrapper for VeevaNetworkSettings
    * @example
    * * Veeva_Network_Settings_MVN__mdt setting = getSettingByName(allSettings, settingName);
    */
    @TestVisible
    private static Veeva_Network_Settings_MVN__mdt getSettingByName(List<Veeva_Network_Settings_MVN__mdt> allSettings, String settingName) {
        Veeva_Network_Settings_MVN__mdt setting;

       if(allSettings != null && !allSettings.isEmpty()) {
            for(Veeva_Network_Settings_MVN__mdt vSetting : allSettings) {
                if(vSetting.DeveloperName == settingName) {
                    setting = vSetting;
                    break;
                }
            }

            if (setting == null && settingName == DEFAULT_NAME) {
                setting = allSettings[0];
            }
        }

        return setting;
    }

    /**
    * @description Gets field value converted to upper case
    * @param    fieldValue  Custom metadata field value
    * @return               Upper case string
    * @example
    * * String fieldValueUpperCase = upperCaseFieldValue(fieldValue);
    */
    private static String upperCaseFieldValue(String fieldValue) {
        if (String.IsNotBlank(fieldValue)) {
            fieldValue = fieldValue.toUpperCase();
        }

        return fieldValue;
    }

    /**
    * @description Gets field value converted to lower case
    * @param    fieldValue  Custom metadata field value
    * @return               Lower case string
    * @example
    * * String fieldValueLowerCase = lowerCaseFieldValue(fieldValue);
    */
    private static String lowerCaseFieldValue(String fieldValue) {
        if (String.IsNotBlank(fieldValue)) {
            fieldValue = fieldValue.toLowerCase();
        }

        return fieldValue;
    }

    /**
    * @description Gets recursive rank filters
    * @param    filter  Numbers in order which will be used
    * @return           List of numbers for rank
    * @example
    * * List<Integer> rankValues = getRankValuesFromRange(range);
    */
    private static List<Integer> getRankValuesFromRange(String range) {
        List<Integer> rankValues = new List<Integer>();

        if (String.IsNotBlank(range) && range.contains('-')) {
            List<String> rangeNumbers = range.split('-');

            Integer currentRankValue = Integer.valueOf(rangeNumbers[0].normalizeSpace());
            Integer maxRankValue = Integer.valueOf(rangeNumbers[1].normalizeSpace());

            if (currentRankValue <= maxRankValue) {
                while (currentRankValue <= maxRankValue) {
                    rankValues.add(currentRankValue);
                    currentRankValue++;
                }
            }
        }

        return rankValues;
    }

    /**
    * @description Veeva Network API version
    */
    public String apiVersion {
        get { return 'v' + setting.API_Version_MVN__c; }
    }

    /**
    * @description Veeva Network Search limit to return records
    */
    public Decimal searchLimit {
        get { return setting.Search_Limit_MVN__c; }
    }

    /**
    * @description Veeva Network Source name
    */
    public String sourceName {
        get { return setting.Source_MVN__c; }
    }

    /**
    * @description Veeva Network System name
    */
    public String systemName {
        get { return setting.System_MVN__c; }
    }

    /**
    * @description Type for DCR Account
    */
    public String dcrAccount {
        get { return setting.DCR_Account_MVN__c; }
    }

    /**
    * @description Type for DCR Address
    */
    public String dcrAddress {
        get { return setting.DCR_Address_MVN__c; }
    }

    /**
    * @description Type for DCR Parent Account
    */
    public String dcrParent {
        get { return setting.DCR_Parent_MVN__c; }
    }

    /**
    * @description Type for DCR License
    */
    public String dcrLicense {
        get { return setting.DCR_License_MVN__c; }
    }

    /**
    * @description Veeva Network Success call status
    */
    public String successCalloutStatus {
        get { return upperCaseFieldValue(setting.Success_Callout_Status_MVN__c); }
    }

    /**
    * @description Veeva Network Inactive User call status
    */
    public String inactiveUserCalloutStatus {
        get { return upperCaseFieldValue(setting.Inactive_User_Callout_Status_MVN__c); }
    }

    /**
    * @description Veeva Network Invalid Session Id call status
    */
    public String invalidSessionIdCalloutStatus {
        get { return upperCaseFieldValue(setting.Invalid_Session_Id_Callout_Status_MVN__c); }
    }

    /**
    * @description Veeva Network User Locked call status
    */
    public String userLockedOutCalloutStatus {
        get { return upperCaseFieldValue(setting.User_Locked_Out_Callout_Status_MVN__c); }
    }

    /**
    * @description Veeva Network Insufficient Access call status
    */
    public String insufficientAccessCalloutStatus {
        get { return upperCaseFieldValue(setting.Insufficient_Access_Callout_Status_MVN__c); }
    }

    /**
    * @description Veeva Network Field value for Active status
    */
    public String activeCodeStatus {
        get { return upperCaseFieldValue(setting.Active_Code_Status_MVN__c); }
    }

    /**
    * @description Veeva Network HCP Record Type
    */
    public String hcpType {
        get { return setting.HCP_Type_MVN__c; }
    }

    /**
    * @description Veeva Network HCO Record Type
    */
    public String hcoType {
        get { return setting.HCO_Type_MVN__c; }
    }

    /**
    * @description Veeva Network Address Record Type
    */
    public String addressType {
        get { return setting.Address_Type_MVN__c; }
    }

    /**
    * @description Veeva Network entity record name
    */
    public String entityRecordName {
        get { return lowerCaseFieldValue(setting.Entity_Record_Name_MVN__c); }
    }

    /**
    * @description Veeva Network parent HCO record name
    */
    public String parentRecordName {
        get { return lowerCaseFieldValue(setting.Parent_HCO_Record_Name_MVN__c); }
    }

    /**
    * @description Veeva Network parent HCO attributes record name
    */
    public String parentAttrRecordName {
        get { return setting.Parent_HCO_Attributes_Record_Name_MVN__c; }
    }

    /**
    * @description Veeva Network address record name
    */
    public String addressRecordName {
        get { return lowerCaseFieldValue(setting.Address_Record_Name_MVN__c); }
    }

    /**
    * @description Veeva Network license record name
    */
    public String licenseRecordName {
        get { return lowerCaseFieldValue(setting.License_Record_Name_MVN__c); }
    }

    /**
    * @description Veeva Network Field name for Id
    */
    public String entityIdFieldName {
        get { return lowerCaseFieldValue(setting.Entity_Id_Field_Name_MVN__c); }
    }

    /**
    * @description Veeva Network Field name for Id in parent HCOs
    */
    public String parentIdFieldName {
        get { return lowerCaseFieldValue(setting.Parent_HCO_Id_Field_Name_MVN__c); }
    }

    /**
    * @description Veeva Network Field name for Address status
    */
    public String addressStatusFieldName {
        get { return lowerCaseFieldValue(setting.Address_Status_Field_Name_MVN__c); }
    }

    /**
    * @description Veeva Network Field name for Address Rank
    */
    public String addressRankFieldName {
        get { return lowerCaseFieldValue(setting.Address_Rank_Field_Name_MVN__c); }
    }

    /**
    * @description The rank filter to apply to Address records pulled from Veeva Network
    */
    public Set<Integer> addressRankFilter {
        get {
            Set<Integer> addressRankFilter = new Set<Integer>();
            String rankValuesString = setting.Address_Rank_Filter_MVN__c;

            if(String.IsNotBlank(rankValuesString)) {
                List<String> rankValues = rankValuesString.split(',');

                for(String rankValue : rankValues) {
                    if (rankValue.contains('-')) {
                        addressRankFilter.addAll(getRankValuesFromRange(rankValue));
                    } else {
                        addressRankFilter.add(Integer.valueOf(rankValue.normalizeSpace()));
                    }
                }
            }

            return addressRankFilter;
        }
    }

    /**
    * @description Veeva Network default Search country
    */
    public String defaultSearchCountry {
        get { return setting.Default_Search_Country_MVN__c; }
    }

    /**
    * @description Veeva Network Completed Date Field Name
    */
    public String completedDateFieldName {
        get { return lowerCaseFieldValue(setting.Completed_Date_Field_Name_MVN__c); }
    }

    /**
    * @description Salesforce Field Name for Veeva Network Id
    */
    public String networkExternalIdFieldName {
        get { return lowerCaseFieldValue(setting.External_Id_Field_Name_MVN__c); }
    }
}