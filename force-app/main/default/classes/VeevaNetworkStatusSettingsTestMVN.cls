/**
* @author Mavens
* @group Network API
* @description test class for VeevaNetworkStatusSettingsTestMVN
*/
@isTest
private class VeevaNetworkStatusSettingsTestMVN {

    private static User testUser;

    static {
        testUser = TestDataFactoryMVN.generateCaseManager();
        insert testUser;
    }

    @isTest
    static void itShouldGetDefaultValuesCorrectly() {
        VeevaNetworkStatusSettingsMVN vnStatusSettings;

        System.runAs(testUser) {
            TestFactoryCustomMetadataMVN.setMocks();

            Test.startTest();
            vnStatusSettings = new VeevaNetworkStatusSettingsMVN();
            Test.stopTest();
        }

        System.assertNotEquals(null, vnStatusSettings);
        System.assert(!vnStatusSettings.settings.isEmpty());

        System.assertNotEquals(null, vnStatusSettings.networkDCRStatusApplied);
        System.assert(!vnStatusSettings.networkDCRStatusApplied.keySet().isEmpty());

        System.assert(vnStatusSettings.networkDCRStatusApplied.get('change_accepted'));
        System.assert(vnStatusSettings.networkDCRStatusApplied.get('change_cancelled'));
        System.assert(!vnStatusSettings.networkDCRStatusApplied.get('change_new'));
        System.assert(!vnStatusSettings.networkDCRStatusApplied.get('change_inqueue'));
        System.assert(vnStatusSettings.networkDCRStatusApplied.get('change_partial'));
        System.assert(vnStatusSettings.networkDCRStatusApplied.get('change_partiallyprocessed'));
        System.assert(!vnStatusSettings.networkDCRStatusApplied.get('change_pendingreview'));
        System.assert(vnStatusSettings.networkDCRStatusApplied.get('change_processed'));
        System.assert(vnStatusSettings.networkDCRStatusApplied.get('change_rejected'));

        System.assertNotEquals(null, vnStatusSettings.networkToSalesforceStatus);
        System.assert(!vnStatusSettings.networkToSalesforceStatus.keySet().isEmpty());

        System.assertEquals('Accepted', vnStatusSettings.networkToSalesforceStatus.get('change_accepted'));
        System.assertEquals('Cancelled', vnStatusSettings.networkToSalesforceStatus.get('change_cancelled'));
        System.assertEquals('Partial', vnStatusSettings.networkToSalesforceStatus.get('change_partial'));
        System.assertEquals('Processed', vnStatusSettings.networkToSalesforceStatus.get('change_processed'));
        System.assertEquals('Rejected', vnStatusSettings.networkToSalesforceStatus.get('change_rejected'));

        System.assertNotEquals(null, vnStatusSettings.networkFieldStatusApplied);
        System.assert(!vnStatusSettings.networkFieldStatusApplied.keySet().isEmpty());

        System.assert(vnStatusSettings.networkFieldStatusApplied.get('change_accepted'));
        System.assert(vnStatusSettings.networkFieldStatusApplied.get('change_added'));
        System.assert(vnStatusSettings.networkFieldStatusApplied.get('change_alreadyapplied'));
        System.assert(vnStatusSettings.networkFieldStatusApplied.get('change_modified'));
        System.assert(!vnStatusSettings.networkFieldStatusApplied.get('change_rejected'));
    }
}