/*
 *  ProgramMemberStageControllerMVN
 *  Created By:     Aggen
 *  Created On:     12/4/2015
 *  Description:    Controller for the ProgramMemberStageMVN page
 *  Modified:       March 24, 2016 - Thomas Hajcak (thomas@mavensconsulting.com)
 *                  Refactoring and code cleanup.
 */

public with sharing class ProgramMemberStageControllerMVN {
    public Program_Member_MVN__c pm { get; set; }
    public Id addStageId { get; set; }
    public Id startStageId { get; set; }
    public Id skipStageId { get; set; }
    public Id removeStageId { get; set; }
    public String programMemberStatus { get; set; }
    public Event_MVN__c newStatusEvent { get; set; }
    public Boolean hasError { get; set; }
    public String errorMessage { get; set; }
    public Map<String,String> eventRecordTypeMap;
    private final Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();

    public List<Program_Stage_MVN__c> programParentStages {
        get {
            programParentStages = new List<Program_Stage_MVN__c>();
            for(Program_Stage_MVN__c ps : [select Activity_Type_MVN__c, Description_MVN__c, Exclude_Activity_MVN__c, Flow_Name_MVN__c, Id,
                                                    Is_Parent_Stage_MVN__c, Name, Parent_Stage_MVN__c, Program_MVN__c, RecordTypeId,
                                                    Stage_Sequence_Number_MVN__c
                                           from Program_Stage_MVN__c
                                           where Program_MVN__c = :pm.Program_MVN__c and Is_Parent_Stage_MVN__c = true and Exclude_From_Library_MVN__c = false
                                           order by Name]) {
                programParentStages.add(ps);
            }
            return programParentStages;
        } set;
    }

    public List<Program_Member_Stage_MVN__c> parentStages {
        get {
            parentStages = new List<Program_Member_Stage_MVN__c>();
            for(Program_Member_Stage_MVN__c pms : [select Program_Stage_MVN__r.Name, Activity_ID_MVN__c, Closed_Date_MVN__c, Exclude_Activity_MVN__c,
                                                            Id, Initiated_Date_MVN__c, IsClosed_MVN__c, Parent_Program_Member_Stage_MVN__c,
                                                            Program_Member_MVN__c, Program_Stage_MVN__c, Stage_Sequence_Number_MVN__c,
                                                            Status_MVN__c, Program_Stage_MVN__r.Skippable_MVN__c, Program_Stage_MVN__r.Startable_MVN__c,
                                                            Program_Stage_MVN__r.Removeable_MVN__c, Application_MVN__r.Name
                                                   from Program_Member_Stage_MVN__c
                                                   where Program_Member_MVN__c = :pm.Id and Parent_Program_Member_Stage_MVN__c = null
                                                   and Application_MVN__c = :pm.Active_Application_MVN__c
                                                   order by Stage_Sequence_Number_MVN__c, Closed_Date_MVN__c nulls last, Initiated_Date_MVN__c nulls last]) {

                parentStages.add(pms);
            }
            return parentStages;
        } set;
    }

    public ProgramMemberStageControllerMVN(ApexPages.StandardController stdController) {
        if (stdController.getId() == null) {
            return;
        }

        this.pm = [select Allergies_MVN__c, Coverage_Secured_MVN__c, Coverage_Status_MVN__c, Enrollment_Date_MVN__c, Id, Active_Application_MVN__c,
                            Member_Contact_ID_MVN__c, Member_MVN__c, Member_MVN__r.Name, Name, OwnerId, Physician_MVN__c, Physician_MVN__r.Name,
                            Program_MVN__c, Program_MVN__r.Name, Starting_Program_Stage_ID_MVN__c, Status_MVN__c, Treating_Address_MVN__c, Treatment_Start_Date_MVN__c,
                            Program_MVN__r.Display_Color_MVN__c, Patient_Status_MVN__c
                   from Program_Member_MVN__c
                   where Id = :stdController.getId()];

        eventRecordTypeMap = new Map<String,String>();

        for(RecordType rt : [select Id, DeveloperName from RecordType where sObjectType = 'Event_MVN__c']) {
            eventRecordTypeMap.put(rt.DeveloperName,rt.Id);
        }

        hasError = false;
    }

    public void removeStage() {
        if(removeStageId != null) {
            Double stageNum;

            // get stage to delete and all children
            List<Program_Member_Stage_MVN__c> stagesToDelete = [SELECT Id,
                                                                      Stage_Sequence_Number_MVN__c,
                                                                      (SELECT Id FROM Program_Member_Stages__r)
                                                                 FROM Program_Member_Stage_MVN__c
                                                                WHERE Id = :removeStageId];

            if (!stagesToDelete.isEmpty()) {
                stageNum = stagesToDelete[0].Stage_Sequence_Number_MVN__c;
                stagesToDelete.addAll(stagesToDelete[0].Program_Member_Stages__r); //move children into main list
                try {
                    delete stagesToDelete;
                } catch(Exception e){
                    ApexPages.addMessages(e);
                    hasError = true;
                    errorMessage = e.getMessage();
                    return;
                }
            }

            // adjust the other stage sequence numbers
            if(stageNum != null) {
                List<Program_Member_Stage_MVN__c> updateParentStages = [SELECT Id,
                                                                               Stage_Sequence_Number_MVN__c
                                                                          FROM Program_Member_Stage_MVN__c
                                                                         WHERE Program_Member_MVN__c = :pm.Id
                                                                           AND Parent_Program_Member_Stage_MVN__c = null
                                                                           AND Stage_Sequence_Number_MVN__c > :stageNum
                                                                      ORDER BY Stage_Sequence_Number_MVN__c];

                for(Program_Member_Stage_MVN__c updatePMS : updateParentStages) {

                    updatePMS.Stage_Sequence_Number_MVN__c = stageNum++; //assign THEN increment
                }

                update updateParentStages;
            }

        }
    }

    public void addStage() {
        if(addStageId != null) {
            Savepoint sp = Database.setSavepoint();
            try {
                ProgramMemberInstantiatorMVN.addStageForProgramMember(addStageId, pm);
            } catch(Exception e){
                System.debug('\n\n\n### ' + e.getStackTraceString() + '\n\n\n');
                ApexPages.addMessages(e);
                Database.rollback(sp);
                hasError = true;
                errorMessage = e.getMessage();
            }
        }
    }

    public void startStage() {
        if(startStageId != null) {
            List<Program_Member_Stage_MVN__c> updatePMSs = new List<Program_Member_Stage_MVN__c>();
            // used to readjust the stage sequence numbers
            Double startStageSeqNum;
            Double unstartedStageSeqNum;

            List<Program_Member_Stage_MVN__c> openMemberParentStages = [SELECT Id,
                                                                               Initiated_Date_MVN__c,
                                                                               Status_MVN__c,
                                                                               Stage_Sequence_Number_MVN__c
                                                                          FROM Program_Member_Stage_MVN__c
                                                                         WHERE Program_Member_MVN__c = :pm.Id
                                                                           AND IsClosed_MVN__c = false
                                                                           AND Parent_Program_Member_Stage_MVN__c = null
                                                                      ORDER BY Stage_Sequence_Number_MVN__c];

            for(Program_Member_Stage_MVN__c parentMemberStage : openMemberParentStages) {
                if (parentMemberStage.Initiated_Date_MVN__c == null) {
                    //found the starting point
                    startStageSeqNum = parentMemberStage.Stage_Sequence_Number_MVN__c;
                    unstartedStageSeqNum = parentMemberStage.Stage_Sequence_Number_MVN__c + 1;
                    break;
                }
            }

            for(Program_Member_Stage_MVN__c parentMemberStage : openMemberParentStages) {
                if(parentMemberStage.Id == startStageId) {
                    parentMemberStage.Status_MVN__c = settings.Program_Member_Stage_Start_Status_MVN__c;
                    parentMemberStage.Initiated_Date_MVN__c = System.today();
                    parentMemberStage.Stage_Sequence_Number_MVN__c = startStageSeqNum;
                    updatePMSs.add(parentMemberStage);
                } else if(parentMemberStage.Initiated_Date_MVN__c == null) {
                    parentMemberStage.Stage_Sequence_Number_MVN__c = unstartedStageSeqNum;
                    unstartedStageSeqNum++;
                    updatePMSs.add(parentMemberStage);
                }
            }

            Savepoint sp = Database.setSavepoint();
            try {
                update updatePMSs;
                ProgramMemberStageManagerMVN.initializeParentStage(pm.Id, startStageId);
            } catch(Exception e){
                System.debug('\n\n\n### ' + e.getStackTraceString() + '\n\n\n');
                ApexPages.addMessages(e);
                Database.rollback(sp);
                hasError = true;
                errorMessage = e.getMessage();
            }
        }
    }

    public void skipStage() {
        if(skipStageId != null) {
            List<Program_Member_Stage_MVN__c> skipPMSs = new List<Program_Member_Stage_MVN__c>();
            List<Case> skipCases = new List<Case>();
            List<Task> skipTasks = new List<Task>();
            String casePrefix = Case.sObjectType.getDescribe().getKeyPrefix();
            String taskPrefix = Task.sObjectType.getDescribe().getKeyPrefix();

            // query for all open child stages
            for(Program_Member_Stage_MVN__c childPMS : [select Activity_ID_MVN__c, Closed_Date_MVN__c, Id, IsClosed_MVN__c, Status_MVN__c
                                                        from Program_Member_Stage_MVN__c
                                                        where Parent_Program_Member_Stage_MVN__c = :skipStageId and IsClosed_MVN__c = false]) {

                // set the child program member stage statuses to cancelled
                childPMS.Status_MVN__c = settings.Program_Member_Stage_Canceled_Status_MVN__c;
                childPMS.Closed_Date_MVN__c = System.today();
                childPMS.IsClosed_MVN__c = true;
                skipPMSs.add(childPMS);

                // set the activity statuses to cancelled
                if(childPMS.Activity_ID_MVN__c != null) {
                    if(String.valueOf(childPMS.Activity_ID_MVN__c).startsWith(casePrefix)) {
                        Case skipCase = new Case();
                        skipCase.Id = childPMS.Activity_ID_MVN__c;
                        skipCase.Status = settings.Case_Cancelled_Status_MVN__c;
                        skipCases.add(skipCase);
                    }
                    if(String.valueOf(childPMS.Activity_ID_MVN__c).startsWith(taskPrefix)) {
                        Task skipTask = new Task();
                        skipTask.Id = childPMS.Activity_ID_MVN__c;
                        skipTask.Status = settings.Task_Cancelled_Status_MVN__c;
                        skipTasks.add(skipTask);
                    }
                }
            }

            // update the parent stage status to cancelled
            Program_Member_Stage_MVN__c parentPMS = new Program_Member_Stage_MVN__c();
            parentPMS.Id = skipStageId;
            parentPMS.Status_MVN__c = settings.Program_Member_Stage_Canceled_Status_MVN__c;
            parentPMS.Closed_Date_MVN__c = System.today();
            parentPMS.IsClosed_MVN__c = true;
            skipPMSs.add(parentPMS);

            Savepoint sp = Database.setSavepoint();
            try {
                update skipPMSs;
                update skipCases;
                update skipTasks;
            } catch(Exception e){
                ApexPages.addMessages(e);
                Database.rollback(sp);
                hasError = true;
                errorMessage = e.getMessage();
            }
        }
    }

    public void updateProgramMemberStatus() {
        String eventRecordType = settings.Program_Mem_Event_Status_Record_Type_MVN__c;

        if(programMemberStatus != null && eventRecordTypeMap.containsKey(eventRecordType)) {
            pm.Status_MVN__c = programMemberStatus;

            newStatusEvent = new Event_MVN__c();
            newStatusEvent.Program_Member_MVN__c = pm.Id;
            newStatusEvent.Start_Date_MVN__c = System.today();
            newStatusEvent.RecordTypeId = eventRecordTypeMap.get(eventRecordType);
            newStatusEvent.Status_MVN__c = programMemberStatus;

            Savepoint sp = Database.setSavepoint();
            try {
                insert newStatusEvent;
                update pm;
            } catch(Exception e){
                ApexPages.addMessages(e);
                Database.rollback(sp);
                hasError = true;
                errorMessage = e.getMessage();
            }
        }
    }

    public void updatePatientStatus() {
        try {
            update pm;
        } catch(Exception e){
            ApexPages.addMessages(e);
            hasError = true;
            errorMessage = e.getMessage();
        }
    }
}