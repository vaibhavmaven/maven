@isTest
private class EligibilityEngineQueryConfigTestMVN {

	@isTest
	static void itShouldNotBeDefinedWithSimpleConstructor() {
        EligibilityEngineQueryConfigMVN config = new EligibilityEngineQueryConfigMVN();
        System.assertEquals(false, config.isDefined);
	}

    @isTest
    static void itShouldInitializeConfig() {
        EligibilityEngineQueryConfigMVN config = new EligibilityEngineQueryConfigMVN(
            'applicationField',
            'objectApiName'
        );
        System.assert(config.isDefined);
        System.assertEquals('objectApiNameapplicationField', config.getKey());
    }

    @isTest
    static void itShouldAddChildObjectCofigurations() {
        EligibilityEngineQueryConfigMVN config = new EligibilityEngineQueryConfigMVN(
            'applicationField',
            'objectApiName'
        );

        Map<String, Set<String>> childConfig1 = new Map<String, Set<String>> {
            'Child_Relationship_Name__r' => new Set<String> {
                'Field1__c',
                'Field2__c'
            }
        };

        Map<String, Set<String>> childConfig2 = new Map<String, Set<String>> {
            'Child_Relationship_Name__r' => new Set<String> {
                'Field3__c',
                'Field4__c'
            }
        };
        config.addChildConfiguration(childConfig1);
        config.addChildConfiguration(childConfig2);

        System.assertEquals(1, config.childRelationshipToFields.size());
        Set<String> fields = config.childRelationshipToFields.get('Child_Relationship_Name__r');
        System.assert(fields.contains('Field1__c'));
        System.assert(fields.contains('Field2__c'));
        System.assert(fields.contains('Field3__c'));
        System.assert(fields.contains('Field4__c'));
    }

    @isTest
    static void itShouldReturnEmptyListIfNoChildConfigurations() {
        EligibilityEngineQueryConfigMVN config = new EligibilityEngineQueryConfigMVN(
            'applicationField',
            'objectApiName'
        );

        System.assert(config.getData(null).isEmpty());
    }

    @isTest
    static void itShouldReturnRequestedData() {
        program = createProgram();
        programMember = createProgramMember();
        application = createApplication();

        EligibilityEngineQueryConfigMVN config = new EligibilityEngineQueryConfigMVN(
            'Program_Member_MVN__c',
            'Program_Member_MVN__c'
        );
        config.addChildConfiguration(
            new Map<String, Set<String>> {
                'Applications_MVN__r' => new Set<String> {
                    'Id',
                    'Name'
                }
            }
        );

        List<Program_Member_MVN__c> programMembers = (List<Program_Member_MVN__c>) config.getData(application.Id);
        System.assertEquals(1, programMembers.size());
        System.assertEquals(1, programMembers[0].Applications_MVN__r.size());
        System.assertEquals(application.Id, programMembers[0].Applications_MVN__r[0].Id);
    }

    static Program_MVN__c program;
    static Program_Member_MVN__c programMember;
    static Application_MVN__c application;

    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        return programFactory.create(
            new Map<String,Object>{
                'Name' => 'A Program',
                'Program_Id_MVN__c' => 'Knipper_PAP_MVN',
                'Active_MVN__c' => true,
                'Job_No_MVN__c' => '12345',
                'Grandfathered_Product_Grace_Period_MVN__c' => 10
            }
        );
    }

    static Program_Member_MVN__c createProgramMember() {
        return TestDataFactoryMVN.createProgramMember(program, TestDataFactoryMVN.createMember());
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        return testApplication;
    }
}