@isTest
public class NewOrderTest {
    private static Map<String, RecordType> accountRecordTypesByName {
        get {
            if (accountRecordTypesByName == null) {
                accountRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
                    accountRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return accountRecordTypesByName;
        }
        private set;
    }    
    
    
    @testSetup
    public static void setup(){
        // create and insert patient 
        Account patient= new Account();
        patient.FirstName = 'Test';
        patient.LastName = 'Patient';
        patient.RecordTypeId = accountRecordTypesByName.get('Patient_MVN').id;
        insert patient;
        
        // create and insert program 
        Program_MVN__c program;
        program = new Program_MVN__c();
        insert program;
        
        // create and insert program member
        Program_Member_MVN__c programMember = new Program_Member_MVN__c();
        programMember.Program_MVN__c = program.id;
        programMember.Member_MVN__c = patient.id;
        programMember.Do_Not_Initialize_MVN__c = false;
        insert programMember;        
        
        // create and insert product family
        Product_MVN__c productFamily = new Product_MVN__c();
        productFamily.Program_MVN__c = program.Id;
        productFamily.Product_Status_MVN__c = 'Active';
        productFamily.Name = 'Horizant 300mg'; 
        productFamily.Days_Supply_MVN__c = 10; 
        productFamily.DEA_Schedule_MVN__c = '3';
        insert productFamily;     
        
        // create and insert product
        Product_Line_Item_MVN__c product = new Product_Line_Item_MVN__c(); 
        product.Name = 'Horizant 300mg'; 
        product.Product_MVN__c = productFamily.Id; 
        product.Is_Primary_MVN__c = true; 
        product.Packaged_Quantity_MVN__c = 10; 
        product.NDC_MVN__c = '40085071645'; 
        insert product;        
 
        // create and insert application
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c applicationObj = applicationFactory.construct(
                                            new Map<String, Object>{'Program_Member_MVN__c' => programMember.id});
        //applicationObj.Number_of_Medicare_B_C_records_MVN__c = 0; 
        insert applicationObj;
        
        // create a document to be attached to the prescription. 
        Document_MVN__c document = new Document_MVN__c(); 
        document.Program_Member_MVN__c = programMember.ID ; 
        document.Program_Id_MVN__c = program.ID ; 
        insert document;        
       
        //create and insert Prescription
        Prescription_MVN__c prescription = new Prescription_MVN__c();
        prescription.Application_MVN__c = applicationObj.Id;
        prescription.Program_Member_MVN__c = programMember.Id;
        prescription.Product_MVN__c = productFamily.Id;
        prescription.Refills_MVN__c = 0;                // refills
        prescription.Quantity_MVN__c = 10;              // order quantity
        prescription.Timeframe_MVN__c = '10';           // quantity
        prescription.Signal_Code_Quantity_MVN__c = 1.0; // frequency 
        prescription.Signal_Code_MVN__c = 'QD - Once a day';   // signal code        
        prescription.Prescription_doc_JKC__c = document.Id ; 
        insert prescription;        
        
        // create order scheduler, add application, program member, prescription, orderstart date, insurance restraint date.
        Order_Scheduler_MVN__c orderSch = new Order_Scheduler_MVN__c(); 
        orderSch.Application_MVN__c = applicationObj.ID; 
        
       //Updated below line on 6/12/20 by Knipper Admin (MG) to fix test case coverage failure
        orderSch.Order_Start_Date_MVN__c = Date.today().addDays(1); //Date.newInstance(2019, 12, 2);  
        orderSch.Prescription_MVN__c = prescription.ID; 
        orderSch.Program_Member_MVN__c = programMember.ID;  
        insert orderSch;       
    }
    
    @isTest
    public static void testCreateOrder(){
        Test.startTest(); 
        List<Order_Scheduler_MVN__c> orderSchList = [select ID from Order_Scheduler_MVN__c LIMIT 1]; 
        NewOrder.createOrder(orderSchList[0].ID);
        Test.stopTest();
    }
}