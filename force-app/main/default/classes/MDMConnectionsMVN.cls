/*
 * MDMConnectionsMVN
 * @created by: Pablo Roldan
 * @created Date: Oct 20, 2017
 * @description: Wrapper class to manage retrieving the MDM Connections custom metadata type
 */
public with sharing class MDMConnectionsMVN{
    public MDM_Connection_MVN__mdt setting { get; private set;}

    private static MDM_Connection_MVN__mdt testMock;

    public MDMConnectionsMVN(String country) {
        Map<String, Schema.SObjectField> MDMConnectionsMVNFMap = MDM_Connection_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
        List<String> mdmConnectionFields = new List<String>(MDMConnectionsMVNFMap.keySet());

        Set<Id> filterSegments = SegmentsMVN.filterSegments(country);
        String queryString = 'SELECT ' + String.escapeSingleQuotes(String.join(mdmConnectionFields, ','))
                                + ' FROM MDM_Connection_MVN__mdt'
                                + ' WHERE Segment_MVN__c IN :filterSegments';

        List<MDM_Connection_MVN__mdt> allSettings = Database.query(queryString);

        //sort the settings by segment
        allSettings = SegmentsMVN.sortMetadataBySegment(allSettings);

        //Config is invalid if there are no records (as there should ALWAYS be a default record) so no need to protect from failure
        //This is the global record
        if(!allSettings.isEmpty()) {
            setting = allSettings[0];

            //Segment_MVN__c is a unique field and we have at most 2 records in the list so there will be at most 2 query results
            if(allSettings.size() == 2) {
                //merge in country specific mdm connections
                mergeSettings(allSettings[1]);
            }
        }

         if(testMock != null){
            setting = testMock;
        }
    }

    @TestVisible
    private static void setMock(MDM_Connection_MVN__mdt mock){
        testMock = mock;
    }

    @TestVisible
    private void mergeSettings(MDM_Connection_MVN__mdt mdmConnection) {
        List<Schema.SObjectField> settingFields = UtilitiesMVN.globalDescribe
                                                            .get('MDM_Connection_MVN__mdt')
                                                            .getDescribe()
                                                            .fields.getMap()
                                                            .values();

        Map<String, Object> fieldsToValue = setting.getPopulatedFieldsAsMap().clone();

        for (Schema.SObjectField field : settingFields) {
            String fieldName = field.getDescribe().getName();
            fieldsToValue.put(fieldName, mdmConnection.get(fieldName));
        }

        setting = (MDM_Connection_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata('MDM_Connection_MVN__mdt', fieldsToValue);
    }
}