public class AuthenticateAndCallout_PrimeRx {    
    private String username; 
    private String password; 
    private String httpMethod;
    private String httpEndpoint;
    private String httpPayload; 
    private String httpResponse; 
    private Integer httpResponseCode = 400; // BAD REQUEST -> server couldnot understand request due to invalid syntax.
    private Boolean basicAuthRequired; 
    private String clientId; 
    private String clientSecret; 
    private String memberId;
    private String token;  
  
    
    public void setAuthenticationDetails(String clientId, String clientSecret,String memberId,String token){
        this.clientId 		= clientId; 
        this.clientSecret 	= clientSecret; 
        this.memberId 		= memberId; 
        this.token 			= token; 
    }
    
    public void setEndpointDetails(String method, String endpoint, String payload){
        this.httpMethod = method; 
        this.httpEndpoint = endpoint; 
        this.httpPayload = payload; 
    }  
    
    /*
    *   Method   : makeCallout_getToken
    *   Desc     : make callout to get token by passing ClientId, ClientSecret and MemberId.
    *   @param   : none   
    *   @return  : none
    */ 
    public void makeCallout_getToken(){
        try{
            // confirm whether callouts can be made. 
            if (Limits.getCallouts() >= Limits.getLimitCallouts()){
                throw new CustomException('Maximum number of Callouts reached..');
            }
            if (String.isBlank(this.httpMethod) || String.isBlank(this.httpEndpoint) ){
                throw new CustomException('Http method or endpoint is invalid.'); 
            }            
            
            // create required objects. 
            HttpRequest request = new HttpRequest(); 
            Http httpObj = new Http(); 
            System.debug('Successfully created http objects.');
            
            //configure the request 
            request.setEndpoint(this.httpEndpoint); 
            request.setMethod(this.httpMethod); 
            System.debug('Successfully configured the request : ' + this.httpEndpoint + ' ' + this.httpMethod);                    
            
            // Configure standard headers. Accepting only in json format.
            request.setHeader('Accept', 'application/json');
            // This tells the API that we are sending the data in json format.
            request.setHeader('Content-Type', 'application/json');
            // set the payload
            request.setBody(this.httpPayload);
            // set timeout, max 2 min 
            request.setTimeout(120000); 
            system.debug('payload is : ' + this.httpPayload); 
            
            System.debug('making the callout.');
            HttpResponse response = httpObj.send(request);
            this.httpResponseCode = response.getStatusCode(); 
            this.httpResponse = response.getBody(); 
        }catch (Exception e){
            this.httpResponse = e.getMessage(); 
            System.debug('Exception in AuthenticateAndCallout, Message: ' + e.getMessage());
            System.debug('Exception in AuthenticateAndCallout, reason: ' + e.getCause());
            System.debug('Exception in AuthenticateAndCallout, on line # : ' + e.getLineNumber());
        }
    } // makeCallout_getToken()
    
    /*
    *   Method   : makeCallout_withToken
    *   Desc     : make callout to the actual endpoint with token 
    *   @param   : none  
    *   @return  : none
    */ 
     public void makeCallout_withToken(){
        try{
            // confirm whether callouts can be made. 
            if (Limits.getCallouts() >= Limits.getLimitCallouts()){
                throw new CustomException('Maximum number of Callouts reached..');
            }
            if (String.isBlank(this.httpMethod) || String.isBlank(this.httpEndpoint) ){
                throw new CustomException('Http method or endpoint is invalid.'); 
            }            
            
            // create required objects. 
            HttpRequest request = new HttpRequest(); 
            Http httpObj = new Http(); 
            System.debug('Successfully created http objects.');
            
            //configure the request 
            request.setEndpoint(this.httpEndpoint); 
            request.setMethod(this.httpMethod); 
            System.debug('Successfully configured the request : ' + this.httpEndpoint + ' ' + this.httpMethod);                    
             // Configure autherization header with the generated token.
            request.setHeader('Authorization', 'Bearer ' + Token);
            // Configure standard headers. Accepting only in json format.
            request.setHeader('Accept', 'application/json');
            // This tells the API that we are sending the data in json format.
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('x-apiVersion', '2.0');
            // set the payload
            request.setBody(this.httpPayload);
            // set timeout, max 2 min 
            request.setTimeout(120000); 
            system.debug('payload is : ' + this.httpPayload); 
            
            System.debug('making the callout.');
            HttpResponse response = httpObj.send(request);
            this.httpResponseCode = response.getStatusCode(); 
            this.httpResponse = response.getBody(); 
        }catch (Exception e){
            this.httpResponse = e.getMessage(); 
            System.debug('Exception in AuthenticateAndCallout, Message: ' + e.getMessage());
            System.debug('Exception in AuthenticateAndCallout, reason: ' + e.getCause());
            System.debug('Exception in AuthenticateAndCallout, on line # : ' + e.getLineNumber());
        }
    } // makeCallout_withToken()
            
    
    public Integer getResponseCode(){   // return the response code. 
        return this.httpResponseCode;
    }
    
    public String getResponse(){        // return the response. 
        return this.httpResponse; 
    }
    
    // should be used only for testing. 
    public void setMockResponse(Integer respCode, String respBody){
        this.httpResponseCode = respCode; 
        this.httpResponse = respBody; 
    }
} // Class declaration.