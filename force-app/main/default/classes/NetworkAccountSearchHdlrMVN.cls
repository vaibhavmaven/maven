/*
 * NetworkAccountSearchHdlrMVN
 * @created by: Pablo Roldan
 * @created Date: Oct 9th, 2017
 * @description: This class implements the functionality to Account and Address connecting to Veeva Network.
 *
 * @API
 *    Constructor
 *     - NetworkAccountSearchHdlrMVN()
 *    Methods
 *     - search(AccountSearchRqstMVN)
 *     - insertAccountDetails(AccountSearchRsltMVN)
 *     - insertAccountDetails(List<AccountSearchRsltMVN>)
 */
public with sharing class NetworkAccountSearchHdlrMVN implements AccountSearchIntfMVN {
    private final Integer CI_QUERY_MAX_RECORDS = 20000;
    private String accountFields;
    private String affiliationFields;
    private String addressFields;
    private Set<String> populatedAddressFields;
    private AccountSearchRqstMVN request;
    private Integer searchLimit;
    private Id selectedRecordTypeId;
    private Set<Id> validRecordTypeIds;

    private Map<String, RecordType> addressRecordTypes = UtilitiesMVN.getRecordTypesForObject('Address_vod__c');

    private String globalSearchCountry;

    private Set<String> accountExternalIds;

    private static final Set<String> countryFieldNames = new Set<String> {
        'country_vod__c',
        'country_mvn__c'
    };

    public NetworkAccountSearchHdlrMVN() {
        searchLimit = UtilitiesMVN.getsearchLimit();
    }

    public List<AccountSearchRsltMVN> search(AccountSearchRqstMVN searchRequest) {
        List<AccountSearchRsltMVN> finalResults = new List<AccountSearchRsltMVN>();

        System.debug('*** search Req: ' + searchRequest);
        request = searchRequest;

        if(searchRequest.searchLimit != null && searchRequest.searchLimit > 0 && searchRequest.searchLimit < searchLimit) {
            searchLimit = searchRequest.searchLimit;
        }

        Map<String, String> childSObjectsByNetworkObject = new Map<String, String>{
            VeevaNetworkConnectionUtilMVN.networkSettings.parentRecordName => 'Account',
            VeevaNetworkConnectionUtilMVN.networkSettings.addressRecordName => 'Address_vod__c'
        };

        globalSearchCountry = String.IsNotBlank(request.address.Country_vod__c) ?
                request.address.Country_vod__c :
                VeevaNetworkConnectionUtilMVN.networkSettings.defaultSearchCountry;

        VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRequest = new VeevaNetworkConnectionUtilMVN.NetworkRequestMVN('Account',
                                                                                                                                childSObjectsByNetworkObject,
                                                                                                                                globalSearchCountry,
                                                                                                                                searchLimit,
                                                                                                                                getNetworkSearchFilters(),
                                                                                                                                searchRequest.isPersonSearch);

        List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> searchNetworkDataRslts = VeevaNetworkConnectionUtilMVN.searchNetworkData(networkRequest);
        System.debug('### searchNetworkDataRslts Search result: ' + searchNetworkDataRslts);

        finalResults = getFinalAccountSearchRslts(searchNetworkDataRslts);

        System.debug('### NetworkAccountSearchHdlrMVN Search result: ' + finalResults);

        return finalResults;
    }

    public AccountSearchRsltMVN insertAccountDetails(AccountSearchRsltMVN asr) {
        if(asr != null && asr.accountDetail != null && asr.accountDetail.Id == null) {
            List<Account> accountRslts = insertAllAccountDetails(asr);
            Map<String, Id> accountIdByExtId = getAccountIdByExtId(accountRslts);
            Map<Id, List<Address_vod__c>> addressesByAccountId = insertAllAddresses(asr, accountRslts, accountIdByExtId);
            Map<Id, List<Affiliation_MVN__c>> affiliationsByAccountId = insertAllAffiliations(asr, accountRslts, accountIdByExtId);

            asr = updateAccountSearchRslt(asr, accountRslts, addressesByAccountId, affiliationsByAccountId);
        }

        return asr;
    }

    public List<AccountSearchRsltMVN> getAccountsWithDetails(AccountSearchRqstMVN searchRequest) {
        List<AccountSearchRsltMVN> results = new List<AccountSearchRsltMVN>();

        if (searchRequest.accountExternalIds != null && !searchRequest.accountExternalIds.isEmpty()) {
            request = searchRequest;

            Map<String, String> childSObjectsByNetworkObject = new Map<String, String>{
                VeevaNetworkConnectionUtilMVN.networkSettings.parentRecordName => 'Account',
                VeevaNetworkConnectionUtilMVN.networkSettings.addressRecordName => 'Address_vod__c'
            };

            VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRequest = new VeevaNetworkConnectionUtilMVN.NetworkRequestMVN('Account',
                                                                                                                                childSObjectsByNetworkObject,
                                                                                                                                request.address.Country_vod__c,
                                                                                                                                request.accountExternalIds);

            List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> searchNetworkDataRslts = VeevaNetworkConnectionUtilMVN.retrieveEntities(networkRequest);

            results = getFinalAccountSearchRslts(searchNetworkDataRslts);
        }

        return results;
    }

    private List<VeevaNetworkConnectionUtilMVN.NetworkSearchFilterMVN> getNetworkSearchFilters() {
        List<VeevaNetworkConnectionUtilMVN.NetworkSearchFilterMVN> networkSearchFilters = new List<VeevaNetworkConnectionUtilMVN.NetworkSearchFilterMVN>();

        networkSearchFilters.addAll(getRecordFilters(request.address, true));
        networkSearchFilters.addAll(getRecordFilters(request.account, false));

        return networkSearchFilters;
    }

    private List<VeevaNetworkConnectionUtilMVN.NetworkSearchFilterMVN> getRecordFilters(SObject record, Boolean isAddress) {
        List<VeevaNetworkConnectionUtilMVN.NetworkSearchFilterMVN> networkSearchFilters = new List<VeevaNetworkConnectionUtilMVN.NetworkSearchFilterMVN>();

        if(record != null) {
            Map<String, Object> fieldsPopulated = record.getPopulatedFieldsAsMap();

            if(fieldsPopulated != null && !fieldsPopulated.keySet().isEmpty()) {
                for(String fieldName : fieldsPopulated.keySet()) {
                    Object populatedFieldValue = fieldsPopulated.get(fieldName);

                    if(String.IsNotBlank(fieldName) && populatedFieldValue != null) {
                        if(String.IsNotBlank(globalSearchCountry) && countryFieldNames.contains(fieldName)) {
                            populatedFieldValue = globalSearchCountry;
                        }

                        networkSearchFilters.add(new VeevaNetworkConnectionUtilMVN.NetworkSearchFilterMVN(isAddress, fieldName, populatedFieldValue));
                    }
                }
            }
        }

        return networkSearchFilters;
    }

    private List<AccountSearchRsltMVN> getFinalAccountSearchRslts(List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> searchNetworkDataRslts) {
        List<AccountSearchRsltMVN> accSearchRslts = new List<AccountSearchRsltMVN>();

        if(searchNetworkDataRslts != null && searchNetworkDataRslts.size() == 1 && String.IsNotBlank(searchNetworkDataRslts[0].error)) {
            throw new UtilitiesMVN.utilException(searchNetworkDataRslts[0].error);
        }

        for(VeevaNetworkConnectionUtilMVN.NetworkResultMVN searchNetworkDataRslt : searchNetworkDataRslts) {
            Account acountNetworkRslt = (Account) searchNetworkDataRslt.entity;
            List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> ciNetworkObjRslts = searchNetworkDataRslt.childSObjectsByName.get(VeevaNetworkConnectionUtilMVN.networkSettings.addressRecordName);

            List<Address_vod__c> ciNetworkRslts = new List<Address_vod__c>();
            if(ciNetworkObjRslts != null) {
                for(VeevaNetworkConnectionUtilMVN.NetworkResultMVN ciNetworkObjRslt : ciNetworkObjRslts) {
                    ciNetworkRslts.add((Address_vod__c) ciNetworkObjRslt.entity);
                }
            }

            AccountSearchRsltMVN accSearchRslt = new AccountSearchRsltMVN(acountNetworkRslt,
                                                ciNetworkRslts,
                                                'NetworkAccountSearchHdlrMVN',
                                                request);

            List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> parentHCONetworkObjRslts = searchNetworkDataRslt.childSObjectsByName.get(VeevaNetworkConnectionUtilMVN.networkSettings.parentRecordName);
            if(parentHCONetworkObjRslts != null) {
                accSearchRslt.addParentAccounts(getParentAccountRslts(parentHCONetworkObjRslts));
            }

            accSearchRslts.add(accSearchRslt);
        }

        return accSearchRslts;
    }

    private List<AccountSearchRsltMVN> getParentAccountRslts(List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> parentHCONetworkRslts) {
        List<AccountSearchRsltMVN> parentAccountRslts = new List<AccountSearchRsltMVN>();

        for(VeevaNetworkConnectionUtilMVN.NetworkResultMVN parentHCONetworkRslt : parentHCONetworkRslts) {
            List<Address_vod__c> addresses = new List<Address_vod__c>();

            if(parentHCONetworkRslt != null && parentHCONetworkRslt.childSObjectsByName != null
                    && parentHCONetworkRslt.childSObjectsByName.containsKey(VeevaNetworkConnectionUtilMVN.networkSettings.addressRecordName)) {
                List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> childParentAddressesResults = parentHCONetworkRslt.childSObjectsByName.get(VeevaNetworkConnectionUtilMVN.networkSettings.addressRecordName);

                if(childParentAddressesResults != null && !childParentAddressesResults.isEmpty()) {
                    for(VeevaNetworkConnectionUtilMVN.NetworkResultMVN childParentAddressesResult : childParentAddressesResults) {
                        addresses.add((Address_vod__c) childParentAddressesResult.entity);
                    }
                }
            }

            parentAccountRslts.add(new AccountSearchRsltMVN((Account) parentHCONetworkRslt.entity,
                                                            addresses,
                                                            'NetworkAccountSearchHdlrMVN',
                                                            request));
        }

        return parentAccountRslts;
    }

    private List<Account> insertAllAccountDetails(AccountSearchRsltMVN asr) {
        List<Account> accountRslts = asr.getAccountAndParentAccounts();
        accountRslts = getAccountsRemovingDuplicates(asr.accountDetail, accountRslts);
        accountRslts = getAccountsAvoidingNewDCR(accountRslts);
        upsert accountRslts Network_Id_MVN__c;
        return accountRslts;
    }

    private Map<Id, List<Address_vod__c>> insertAllAddresses(AccountSearchRsltMVN asr,
                                                            List<Account> accountRslts,
                                                            Map<String, Id> accountIdByExtId) {
        Map<Id, List<Address_vod__c>> addressesByAccountId = new Map<Id, List<Address_vod__c>>();

        Id mainAccountId = accountIdByExtId.get(asr.accountDetail.Network_Id_MVN__c);
        Map<String, Address_vod__c> addressesByExtId = addRelationBetweenAccountAndAddress(
                                                                mainAccountId,
                                                                asr.getAllAddressRecords()
                                                        );

        List<AccountSearchRsltMVN> parentASRs = asr.getParentAccounts();
        Set<Id> accountIdVisited = new Set<Id> {
            mainAccountId
        };

        if(parentASRs != null && !parentASRs.isEmpty()) {
            for(AccountSearchRsltMVN parentASR : parentASRs) {
                Id parentAccountId = accountIdByExtId.get(parentASR.accountDetail.Network_Id_MVN__c);

                if(!accountIdVisited.contains(parentAccountId)) {
                    addressesByExtId.putAll(addRelationBetweenAccountAndAddress(
                                                parentAccountId,
                                                parentASR.getAllAddressRecords()
                                        ));
                }
            }
        }

        if(addressesByExtId != null && !addressesByExtId.keySet().isEmpty()) {
            List<Address_vod__c> addresses = getAddressesToBeUpserted(addressesByExtId);
            upsert addresses Network_Id_MVN__c;

            for(Address_vod__c address : addresses) {
                List<Address_vod__c> relatedAddresses = addressesByAccountId.containsKey(address.Account_vod__c) ?
                    addressesByAccountId.get(address.Account_vod__c) : new List<Address_vod__c>();

                relatedAddresses.add(address);
                addressesByAccountId.put(address.Account_vod__c, relatedAddresses);
            }
        }

        return addressesByAccountId;
    }

    private Map<String, Id> getAccountIdByExtId(List<Account> accountRslts) {
        Map<String, Id> accountIdByExtId = new Map<String, Id>();

        for(Account accountRslt : accountRslts) {
            if(accountRslt.Id != null && accountRslt.Network_Id_MVN__c != null) {
                accountIdByExtId.put(accountRslt.Network_Id_MVN__c, accountRslt.Id);
            }
        }

        return accountIdByExtId;
    }

    private Map<String, Address_vod__c> addRelationBetweenAccountAndAddress(Id accountId, List<Address_vod__c> addressRslts) {
        Map<String, Address_vod__c> addressesByExtId = new Map<String, Address_vod__c>();

        if(addressRslts != null && !addressRslts.isEmpty()) {
            for(Address_vod__c addressRslt : addressRslts) {
                if(addressRslt.Network_Id_MVN__c != null && (addressRslt.Account_vod__c == null || addressRslt.Account_vod__c != accountId)) {
                    addressRslt.Account_vod__c = accountId;
                    addressesByExtId.put(addressRslt.Network_Id_MVN__c, addressRslt);
                }
            }
        }

        return addressesByExtId;
    }

    private List<Address_vod__c> getAddressesToBeUpserted(Map<String, Address_vod__c> addressesByExtId) {
        List<Address_vod__c> newAddresses = new List<Address_vod__c>();

        List<Address_vod__c> existingAddresses = [SELECT Id,
                                                         Network_Id_MVN__c
                                                    FROM Address_vod__c
                                                   WHERE Network_Id_MVN__c IN :addressesByExtId.keySet()];

        Set<String> existingExternalIds = new Set<String>();

        for (Address_vod__c existingAddress : existingAddresses) {
            existingExternalIds.add(existingAddress.Network_Id_MVN__c);
        }

        for (Address_vod__c newAddress : addressesByExtId.values()) {
            newAddress.DCR_Override_MVN__c = true;

            newAddresses.add(newAddress);
        }

        return newAddresses;
    }

    private Map<Id, List<Affiliation_MVN__c>> insertAllAffiliations(AccountSearchRsltMVN asr,
                                                                    List<Account> accountRslts,
                                                                    Map<String, Id> accountIdByExtId) {
        Map<Id, List<Affiliation_MVN__c>> affiliationsByAccountId = new Map<Id, List<Affiliation_MVN__c>>();

        Id mainAccountId = accountIdByExtId.get(asr.accountDetail.Network_Id_MVN__c);

        List<Affiliation_MVN__c> existingAffiliations = [SELECT Parent_Account_MVN__c
                                                           FROM Affiliation_MVN__c
                                                          WHERE Child_Account_MVN__c =: mainAccountId
                                                            AND Parent_Account_MVN__c IN :accountIdByExtId.values()];

        Set<Id> existingAffiliationParentIds = new Set<Id>();
        List<Affiliation_MVN__c> affiliations = new List<Affiliation_MVN__c>();

        for(Affiliation_MVN__c existingAffiliation : existingAffiliations) {
            existingAffiliation.DCR_Override_MVN__c = true;
            affiliations.add(existingAffiliation);
            existingAffiliationParentIds.add(existingAffiliation.Parent_Account_MVN__c);
        }

        for(Account accountRslt : accountRslts) {
            if(mainAccountId != accountRslt.Id) {
                affiliations.add(new Affiliation_MVN__c(Child_Account_MVN__c =mainAccountId,
                                                        Parent_Account_MVN__c = accountRslt.Id,
                                                        DCR_Override_MVN__c = true));
            }
        }

        upsert affiliations;

        for(Affiliation_MVN__c affiliation : affiliations) {
            List<Affiliation_MVN__c> childAccountAffiliations = affiliationsByAccountId.containsKey(affiliation.Child_Account_MVN__c) ?
                    affiliationsByAccountId.get(affiliation.Child_Account_MVN__c) : new List<Affiliation_MVN__c>();
            List<Affiliation_MVN__c> parentAccountAffiliations = affiliationsByAccountId.containsKey(affiliation.Parent_Account_MVN__c) ?
                    affiliationsByAccountId.get(affiliation.Parent_Account_MVN__c) : new List<Affiliation_MVN__c>();

            childAccountAffiliations.add(affiliation);
            parentAccountAffiliations.add(affiliation);

            affiliationsByAccountId.put(affiliation.Child_Account_MVN__c, childAccountAffiliations);
            affiliationsByAccountId.put(affiliation.Parent_Account_MVN__c, parentAccountAffiliations);
        }

        return affiliationsByAccountId;
    }

    private AccountSearchRsltMVN updateAccountSearchRslt(AccountSearchRsltMVN asr,
                                                        List<Account> accountRslts,
                                                        Map<Id, List<Address_vod__c>> addressesByAccountId,
                                                        Map<Id, List<Affiliation_MVN__c>> affiliationsByAccountId) {
        List<AccountSearchRsltMVN> parentAccounts = new List<AccountSearchRsltMVN>();
        request = asr.getRequest();

        for(Account accountRslt : accountRslts) {
            if(asr.accountDetail.Network_Id_MVN__c == accountRslt.Network_Id_MVN__c) {
                asr.accountDetail = accountRslt;
                asr = getAddressesSearchRslt(asr, addressesByAccountId);
                asr = getAffiliationsSearchRslt(asr, affiliationsByAccountId);
            } else if(accountRslt.Id != null) {
                AccountSearchRsltMVN parentRslt = new AccountSearchRsltMVN(
                    accountRslt,
                    'NetworkSearchHdlrMVN',
                    request
                );
                parentRslt = getAddressesSearchRslt(parentRslt, addressesByAccountId);
                parentRslt = getAffiliationsSearchRslt(parentRslt, affiliationsByAccountId);
                parentAccounts.add(parentRslt);
            }
        }

        if(!parentAccounts.isEmpty()) {
            asr.addParentAccounts(parentAccounts);
        }

        return asr;
    }

    private AccountSearchRsltMVN getAddressesSearchRslt(AccountSearchRsltMVN asr,
                                                        Map<Id, List<Address_vod__c>> addressesByAccountId) {
        List<Address_vod__c> addresses = addressesByAccountId.get(asr.accountDetail.Id);

        if(addresses != null && !addresses.isEmpty()) {
            asr.addAddressRecords(addresses);
        }

        return asr;
    }

    private AccountSearchRsltMVN getAffiliationsSearchRslt(AccountSearchRsltMVN asr,
                                                            Map<Id, List<Affiliation_MVN__c>> affiliationsByAccountId) {
        List<Affiliation_MVN__c> affiliations = affiliationsByAccountId.get(asr.accountDetail.Id);

        if(affiliations != null && !affiliations.isEmpty()) {
            asr.addAffiliations(affiliations);
        }

        return asr;
    }

    private List<Account> getAccountsRemovingDuplicates(Account accountDetail, List<Account> accountRslts) {
        List<Account> accountWithoutDuplicateRslts = new List<Account>{accountDetail};
        accountExternalIds = new Set<String>{accountDetail.Network_Id_MVN__c};

        for(Account accountRslt : accountRslts) {
            if(!accountExternalIds.contains(accountRslt.Network_Id_MVN__c)) {
                accountWithoutDuplicateRslts.add(accountRslt);
                accountExternalIds.add(accountRslt.Network_Id_MVN__c);
            }
        }

        return accountWithoutDuplicateRslts;
    }

    private List<Account> getAccountsAvoidingNewDCR(List<Account> accountRslts) {
        Set<String> existingExternalIds = new Set<String>();

        if (accountExternalIds != null && !accountExternalIds.isEmpty()) {
            List<Account> existingAccounts = [SELECT Id,
                                                    Network_Id_MVN__c
                                                FROM Account
                                            WHERE Network_Id_MVN__c IN :accountExternalIds];

            for (Account existingAccount : existingAccounts) {
                existingExternalIds.add(existingAccount.Network_Id_MVN__c);
            }
        }

        for (Account accountRslt : accountRslts) {
            accountRslt.DCR_Override_MVN__c = true;
        }

        return accountRslts;
    }
}