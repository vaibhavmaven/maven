/**
* @author      : Persistent Systems.
* @date        : September 13 2019
* @description : Test Class to test PAP_ProductUpdateHandler class.                 
*/


@isTest
private class PAP_ProductUpdateHandlerTest {
    private static User caseManager;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;
    private static Product_MVN__c product; 
    private static Prescription_MVN__C prscp ; 
    
    static {  // static initialization block. 
        TestFactoryCustomMetadataMVN.setMocks();
        TestDataFactoryMVN.createPSSettings();
        
        caseManager = TestDataFactoryMVN.generateCaseManager();
        System.runAs(caseManager) {
            Program_MVN__c program = TestDataFactoryMVN.createProgram(); // program 
            Account testAccount = TestDataFactoryMVN.createMember();     // member 
            programMember = TestDataFactoryMVN.createProgramMember(program, testAccount); // program_member
            application = new Application_MVN__c(                        // application       
                Program_Member_MVN__c = programMember.Id
            );
            insert application;                                          // application 
            product = new Product_MVN__c(                 				 // product_family
                Program_MVN__c = programMember.Program_MVN__c,
                Product_Status_MVN__c = 'Active');
            insert product;

            prscp = new Prescription_MVN__c(							 // Prescription. 				
                Program_Member_MVN__c = programMember.Id,
                Product_MVN__c = product.Id
            );            
            insert prscp;                       
        } // system.runAS
    } // static    

    // Scenario1 : when "Update Existing Product Type" in "Product_Update_Setting_PAP__mdt" is set to true. 
    @isTest
    private static void testChangeProductTypeForTrue(){
        Test.startTest(); 
        PAP_ProductUpdateHandler.updateSettings = true; 
        List<Product_MVN__C> prod = [select id, Product_Type_PAP__c 
                                     from Product_MVN__C 
                                     where Program_MVN__c = :programMember.Program_MVN__c 
                                     limit 1];
        system.assert(prod != null || prod.size() != 0 , 'Query in testChangeProductType Failed.');
        prod[0].Product_Type_PAP__c = 'First Product';
        update prod;
        Test.stopTest();         
    }// testChangeProductType1(). 
    
    // Scenario2 : when "Update Existing Product Type" in "Product_Update_Setting_PAP__mdt" is set to false.     
    @isTest
    private static void testChangeProductTypeForFalse(){
        Test.startTest(); 
        PAP_ProductUpdateHandler.updateSettings = false; 
        List<Product_MVN__C> prod = [select id, Product_Type_PAP__c 
                                     from Product_MVN__C 
                                     where Program_MVN__c = :programMember.Program_MVN__c 
                                     limit 1];
        system.assert(prod != null || prod.size() != 0 , 'Query in testChangeProductType Failed.');
        prod[0].Product_Type_PAP__c = 'First Product';
        update prod;
        Test.stopTest();         
    }// testChangeProductType2().     
}// test class