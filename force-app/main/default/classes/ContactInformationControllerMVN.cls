/**
 *  @author Mavens
 *  @date August 2016
 *  @description Controller responsible for allowing the editing, creation and
 *               deletion of Contact Information records as well as setting
 *               the opt status for related Communication Preference records.
 */
 public with sharing class ContactInformationControllerMVN {

    // Representing the Account ID component parameter
    public Id   accountId   { get; set; }

    // Representing the Program Member ID component parameter
    public Id   memberId    { get; set; }

    // Representing the List of Contact Information records
    private List<ContactInfoWrapper> infoList;

    // Representing the Patient Services custom settings
    public final Patient_Service_Settings_MVN__c settings;

    // Representing the related Member record
    public Account member {
        get {
            if(member == null) {
                member = getAccount();
            }

            return member;
        }
        set;
    }

    // Representing a Map of Contact Information fieldsets
    private static Map<String, Set<String>> fieldsets {
        get {
            if(fieldsets == null) {
                fieldsets = new Map<String, Set<String>>();

                Schema.DescribeSObjectResult describe = Contact_Information_MVN__c.SObjectType.getDescribe();

                for(Schema.FieldSet fieldset : describe.FieldSets.getMap().values()) {
                    fieldsets.put(fieldset.getName(), new Set<String>());
                    
                    for(Schema.FieldSetMember field : fieldset.getFields()) {
                        fieldsets.get(fieldset.getName()).add(field.getFieldPath());
                    }
                } 
            }

            return fieldsets;
        }
        private set;
    }

    /**
     * Class Constructor
     * @access  public
     */
    public ContactInformationControllerMVN() {
        this.settings = Patient_Service_Settings_MVN__c.getInstance();
    }

    /**
     * Getting list of Contact Information Records
     * @return  List<ContactInfoWrapper>    List of Contact Informations
     * @access  public
     */
    public List<ContactInfoWrapper> getInfoList() {
        if(infoList != null) {
            return infoList;
        }

        infoList = new List<ContactInfoWrapper>();

        for(Contact_Information_MVN__c info : queryInformation()) {
            infoList.add(new ContactInfoWrapper(info));
        }

        return infoList;
    }

    /**
     * Getting related Account record
     * @return  Account
     * @access  private
     */
    private Account getAccount() {
        if(memberId != null) {
            return [SELECT Member_MVN__r.Id, Member_MVN__r.Name FROM Program_Member_MVN__c WHERE Id = :memberId].Member_MVN__r;
        } else {
            return [select Id, Name from Account where Id = :accountId];
        }
    }

    /**
     * Getting Contact Information records
     * @return  List<Contact_Information_MVN__c>    List of Contact Information records
     * @access  private
     */
    private List<Contact_Information_MVN__c> queryInformation() {
        return [
            SELECT Label_MVN__c,
                   Value_MVN__c,
                   Primary_MVN__c,
                   Address_MVN__c,
                   Address_MVN__r.Id,
                   Address_MVN__r.Shipping_vod__c,
                   Address_MVN__r.Inactive_vod__c,
                   Messages_OK_MVN__c,
                   Best_Time_MVN__c,
                   Phone_MVN__c,
                   RecordTypeId,
                   RecordType.Name,
                   RecordType.DeveloperName,
                   (
                        SELECT Status_MVN__c
                          FROM Communication_Preferences__r
                         WHERE Program_Member_MVN__c = :memberId
                           AND Program_Member_MVN__c != null
                         LIMIT 1
                   )
              FROM Contact_Information_MVN__c
             WHERE Account_MVN__c = :member.Id 
          ORDER BY Sequence_Number_MVN__c NULLS LAST,
                   CreatedDate ASC
        ];
    }

    /**
     * Executed when refreshing Communication Preferences table
     * @return  PageReference
     * @access  public
     */
    public PageReference onRefreshTable() {
        infoList = null;
        return null;
    }

    /**
     * ContactInfoWrapper
     * It is wrapping Contact Information and Communication Preferene records.
     */
    public class ContactInfoWrapper {
        public Address_vod__c                  address      { get; private set; }
        public Contact_Information_MVN__c      contactInfo  { get; private set; }
        public Communication_Preference_MVN__c commPref     { get; private set; }

        private Set<String>                    fieldset     { get; private set; }

        /**
         * ContactInfoWrapper Class Constructor
         * @access  public
         */        
        public ContactInfoWrapper(Contact_Information_MVN__c info) {
            this.contactInfo = info;
            this.fieldset = ContactInformationControllerMVN.fieldsets.get('Contact_Info_' + info.RecordType.DeveloperName);

            if(info.Address_MVN__c != null) {
                this.address = new Address_vod__c(
                    Id = info.Address_MVN__r.Id,
                    Shipping_vod__c = info.Address_MVN__r.Shipping_vod__c
                );
            }
            else {
                this.address = new Address_vod__c();
            }

            if(!info.Communication_Preferences__r.isEmpty()) {
                this.commPref = info.Communication_Preferences__r[0];
            }
        }

        // Fieldset has field or not
        public Boolean getHasType()       { return fieldset.contains('RecordTypeId'); }
        public Boolean getHasLabel()      { return fieldset.contains('Label_MVN__c'); }
        public Boolean getHasValue()      { return fieldset.contains('Value_MVN__c'); }
        public Boolean getHasPrimary()    { return fieldset.contains('Primary_MVN__c'); }
        public Boolean getHasShipping()    { return fieldset.contains('Address_MVN__r.Shipping_vod__c'); }
        public Boolean getHasMessagesOK() { return fieldset.contains('Messages_OK_MVN__c'); }
        public Boolean getHasBestTime()   { return fieldset.contains('Best_Time_MVN__c'); }
    }

}