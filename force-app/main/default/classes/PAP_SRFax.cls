public class PAP_SRFax {  
    private static final Integer faxNumberLength = 10;
    
     /*
    *   Method   : QueueFax
    *   Desc     : Invocable method from Process Builder.  
    *   @param   : Program Member IDList - List of Program MemberID;s. they will be provided by the process builder. 
    *   @return  : Does not return any parameter.
    */   
    @InvocableMethod
    public static void QueueFax(List<ID> PgmMemberId){ 
       // This method will be invoked from a process builder. we will queue one fax at a time.
        for(Integer i =0;i<PgmMemberId.size();i++){ 
        calloutSRFaxAPI(PgmMemberId[i]);
        }
    }
    /*
*   Method   : calloutSRFaxAPI
*   Desc     : Queues a fax to be delivered to HCP. 
*   @param   : PgmMemberId - List of PgmMemberId;s.
*   @return  : Does not return any parameter Program Member
*/       
  @future(callout=true)
    public static void calloutSRFaxAPI(ID PgmMemberId){
        // Implementing the SRFax callout as future method because some DML operation is performed before this Process
        // builder is triggered. DO NOT make this function call a synchronous call. 
       
        Program_Member_MVN__c prgmMember = [select id , name, Program_MVN__r.name from Program_Member_MVN__c where id=:PgmMemberId ];
        // variable declarations
        List<SRFax_API_ConfigurationSettings__mdt> srfaxConfig ;
        String hcpFaxNumber ; 
        Map<String, String> faxDetails ;         
        String faxPayload ; 
        AuthenticateAndCallout calloutObj ;  
        String responseString='';
        try{    
            // get SRFax endpoint details.
            srfaxConfig = getEndpointConfigurations(prgmMember);
            // Get HCP details to get the destination fax number.
            hcpFaxNumber = getHCPFaxNumber(PgmMemberId); 
            // Fetch the fax contents to be sent.
            faxDetails = getFaxDetails(PgmMemberId);
            // Populate the payload for POST method.
            faxPayload = populatePayload(PgmMemberId, srfaxConfig, hcpFaxNumber, faxDetails);
            // Make the callout to SRFax URL HTTP POST method.            
            responseString=callSRFaxEndpoint(srfaxConfig, faxPayload, PgmMemberId); 
            
        }catch(CustomException excp){          // handling exceptions            
            //updateProgramMember('Failed', excp.getMessage() , 'Fax Failed', PgmMemberId[0], false);
            catchException(excp, 'calloutSRFaxAPI',400,prgmMember.Name);
        }catch(Exception ex){            
            // updateProgramMember('Failed', ex.getMessage(), 'Fax Failed', PgmMemberId[0], false);
            catchException(ex, 'calloutSRFaxAPI',400,prgmMember.Name);
        }
        // return responseString;
    }
    
    /*
*   Method   : getEndpointConfigurations
*   Desc     : Get SRFax configuration details from SRFax_API_ConfigurationSettings__mdt
*   @param   : Does not except any parameters.
*   @return  : returns the srfax endpoint configurations.
*/       
    private static List<SRFax_API_ConfigurationSettings__mdt> getEndpointConfigurations(Program_Member_MVN__c prgmMember){
        String programName = prgmMember.Program_MVN__r.name;
        // get configuration details from SRFax_API_ConfigurationSettings__mdt
        List<SRFax_API_ConfigurationSettings__mdt> srfaxConfig = [select 
                                                                  Access_ID_JKC__C, 
                                                                  Access_Password_JKC__C, 
                                                                  Endpoint_URL_JKC__C, 
                                                                  Fax_Action_JKC__C, 
                                                                  Fax_Caller_ID_JKC__C, 
                                                                  Fax_Sender_Email_JKC__C, 
                                                                  Fax_Type_JKC__C,
                                                                  HTTP_Method_JKC__C, 
                                                                  Country_Code_Prefix_JKC__C 
                                                                  from 
                                                                  SRFax_API_ConfigurationSettings__mdt
                                                                  where 
                                                                  Type_JKC__C = 'SRFax_Queue_Fax'
                                                                  AND Program__c=: programName
                                                                  LIMIT 1]; 
        // validations
        if(srfaxConfig == null || srfaxConfig.size() == 0)
        {
            throw new CustomException('SRFax endoints not configured correctly in \"SRFAX_API_ConfigurationSettings\".'); 
        }
        if(String.isBlank(srfaxConfig[0].Access_ID_JKC__C) || String.isBlank(srfaxConfig[0].Access_Password_JKC__C) || 
           String.isBlank(srfaxConfig[0].Endpoint_URL_JKC__C) || String.isBlank(srfaxConfig[0].HTTP_Method_JKC__C) ||
           String.isBlank(srfaxConfig[0].Fax_Action_JKC__C) || String.isBlank(srfaxConfig[0].Fax_Caller_ID_JKC__C) ||
           String.isBlank(srfaxConfig[0].Fax_Sender_Email_JKC__C) || String.isBlank(srfaxConfig[0].Fax_Type_JKC__C) ||
           String.isBlank(srfaxConfig[0].Country_Code_Prefix_JKC__C) ){
               throw new CustomException('SRFax configuration fields empty in \"PAP_ConfigurationSettings\".');             
           }
        system.debug('SRFax configuration details fetched for Queue_FAX API : ' + srfaxConfig[0]);   
        return srfaxConfig;         
    }
    
    
    /*
*   Method   : getHCPFaxNumber
*   Desc     : Get fax number of HCP from the Program Member object.
*   @param   : Program MemberIDList - List of Program MemberId's 
*   @return  : returns HCP's fax number as a string.
*/     
    private static String getHCPFaxNumber(ID PgmMemberObj){ 
        String hcpFaxNumber = ''; 
        List<Program_Member_MVN__c> prgmMemberList = [select 
                                                      Physician_MVN__r.Fax 
                                                      from  
                                                      Program_Member_MVN__c
                                                      where 
                                                      ID = :PgmMemberObj
                                                      LIMIT 1                               
                                                     ];  
        // validations. 
        if(prgmMemberList == null || prgmMemberList.size() == 0){                    // validations.             
            throw new CustomException('Unable to fetch fax number.'); 
        }
        hcpFaxNumber = prgmMemberList[0].Physician_MVN__r.Fax  ; 
        if(String.isBlank(hcpFaxNumber)){
            throw new CustomException('Fax number missing!!'); 
        }
        // converting the number into simple string. 
        hcpFaxNumber = hcpFaxNumber.replace('(', '').replace(')', '').replace('-', '').replace(' ', '').trim();
        if(hcpFaxNumber.length() < faxNumberLength){
            throw new CustomException('Fax number should be atleast 10 digits long.'); 
        }
        system.debug('HCP\'s Fax number is : ' + hcpFaxNumber);    
        return hcpFaxNumber; 
    }
    
    
    /*
*   Method   : getFaxDetails
*   Desc     : Get the details like name of the fax file and contents of fax file in base64encoded format 
*              from document object related to this Program Member. 
*   @param   : PgmMemberObj - List of PgmMemberObj ID's. 
*   @return  : Map containing the file name and file contents.
*/     
    private static Map<String, String> getFaxDetails(ID PgmMemberObj){       
        system.debug('Fetching fax file name and contents for Program Member : ' + PgmMemberObj); 
        // get fax details from document object related to Program Member. 
        Map<String, String> faxDetails = new Map<String, String>{}; 
            String PgmMemberID = PgmMemberObj; 
        PgmMemberID = PgmMemberID.substring(0, 15);  // limiting the length from 18 to 15 
        system.debug('15 digit Program Member Id is : ' + PgmMemberID);
        List<Document_MVN__C >doclist =[select ID from Document_MVN__C where Program_Member_MVN__c = :PgmMemberID AND title_MVN__c LIKE '%Fax%' ORDER BY createddate desc LIMIT 1];
        List<Attachment> attachmentList = [select
                                           Name, 
                                           Body 
                                           from 
                                           Attachment 
                                           where 
                                           ParentId =:doclist[0].Id                                                  
                                           LIMIT 1                                         
                                          ]; 
        // validations 
        if(attachmentList == null || attachmentList.size() == 0){
            throw new CustomException('Attachment/Fax not available for this Program Member.'); 
        }        
        faxDetails.put('faxFileName', attachmentList[0].Name) ;    
        system.debug('Fax file name is : ' + faxDetails.get('faxFileName'));         
        faxDetails.put('faxFileContents', EncodingUtil.base64Encode(attachmentList[0].Body));
        return faxDetails ;
    }
    
    /*
*   Method   : populatePayload
*   Desc     : Populate the payload, which is to be sent to the Queue_SRFax api.  
*   @param   : ProgramMemberIDList - list of Program MemberID's. 
*            : srfaxConfig - SRFax configurations. 
*            : hcpFaxNumber - HCP's fax number. 
*            : faxDetails - Fax file name and Fax contents in Base64Encoded format.    
*   @return  : Payload in JSON-String format.
*/      
    private static String populatePayload(ID PgmMemberObj, List<SRFax_API_ConfigurationSettings__mdt> srfaxConfig,
                                          String hcpFaxNumber, Map<String, String> faxDetails){    
                                              system.debug('Populating the payload for Program Member ID : ' + PgmMemberObj); 
                                              // Pre-pending the country code to HCP's fax number. if its an internaltional fax, its also taken care here.
                                              if(hcpFaxNumber.length() == faxNumberLength){
                                                  system.debug('Country code needs to be prepended to this fax number.');
                                                  hcpFaxNumber = srfaxConfig[0].Country_Code_Prefix_JKC__C.trim() + hcpFaxNumber.trim() ;                                                 
                                                  system.debug('HCP\'s fax number after adding country code is : ' + hcpFaxNumber);             
                                              }
                                              String faxPayload ; 
                                              JSONGenerator generator = JSON.createGenerator(false);                              //instantiation of the generator
                                              generator.writeStartObject();                                                       // Initial '{'
                                              generator.writeStringField('access_id', srfaxConfig[0].Access_ID_JKC__C.trim());            // access_id 
                                              generator.writeStringField('access_pwd', srfaxConfig[0].Access_Password_JKC__C.trim());     // access_pwd 
                                              generator.writeStringField('action', srfaxConfig[0].Fax_Action_JKC__C.trim());              // action 
                                              generator.writeStringField('sCallerID', srfaxConfig[0].Fax_Caller_ID_JKC__C.trim());        // callerID
                                              generator.writeStringField('sSenderEmail', srfaxConfig[0].Fax_Sender_Email_JKC__C.trim());  // senderEmail
                                              generator.writeStringField('sFaxType', srfaxConfig[0].Fax_Type_JKC__C.trim());              // FaxType 
                                              generator.writeStringField('sToFaxNumber', hcpFaxNumber.trim());                               // toFaxNumber 
                                              generator.writeStringField('sFileName_1', faxDetails.get('faxFileName'));           // filename to be faxed 
                                              generator.writeStringField('sFileContent_1', faxDetails.get('faxFileContents'));    // base64Encoded file contents to be faxed                                                    
                                              generator.writeEndObject();                                                         // final '}'
                                              faxPayload = generator.getAsString(); 
                                              system.debug('Fax Payload : ' + faxPayload);  
                                              return faxPayload;                                             
                                          }
    
    
    /*
*   Method   : callSRFaxEndpoint
*   Desc     : Call SRFax api to queue a fax to be sent. The fax is queued and is not actually sent right away.
*   @param   : srfaxConfig - contains the config metadata of SRFax endpoint.
*            : faxPayload -  contains the fax data to be faxed. 
*            : Program MemberIdList -  the current Program Member id.  
*   @return  : none
*/      
    private static String callSRFaxEndpoint(List<SRFax_API_ConfigurationSettings__mdt> srfaxConfig, 
                                            String faxPayload, ID PgmMemberObj){     
                                                system.debug('Calling the SRFax endpoint for Program Member : ' + PgmMemberObj); 
                                                // create new authncall obj 
                                                AuthenticateAndCallout authnCall = new AuthenticateAndCallout();
                                                // we do not set the authentication headers. Setting the endpoints details. 
                                                authnCall.setEndpointDetails(srfaxConfig[0].HTTP_Method_JKC__C, 
                                                                             srfaxConfig[0].Endpoint_URL_JKC__C, 
                                                                             faxPayload);
                                                // make the callout
                                                system.debug('Get_MultiFaxstatus response object is :' + authnCall);
                                                authnCall.makeCallout();
                                                // Confirm whether the fax was queued successfuly and update the status accordingly in Program Member object. 
                                                String  responseString = confirmAndUpdateQueuedFaxStatus(authnCall, PgmMemberObj);
                                                return responseString;
                                            }
    
    
    /*
*   Method   : confirmAndUpdateQueuedFaxStatus
*   Desc     : Confirm whether the fax was queued successfuly and update the status accordingly in Program Member object.
*   @param   : authnCall - contains the response of SRFax callout that we made.
*            : Program MemberIDList -  the current Program Member id.  
*   @return  : none
*/     
    private static String confirmAndUpdateQueuedFaxStatus(AuthenticateAndCallout authnCall, ID PgmMemberObj){            
        Integer responseCode   = authnCall.getResponseCode() ; 
        String  responseString = authnCall.getResponse() ;  
        system.debug('Response Code is : ' + responseCode); 
        system.debug('Response String is : ' + responseString);
        Map<String, String> responseMap = (Map<String, String>) Json.deserialize(responseString, Map<String, String>.class);
        // API Callout Succeeded.
        if( (responseCode == 200) && ( !String.isBlank(responseMap.get('Status')) && 
                                      !String.isBlank(responseMap.get('Result')) &&
                                      responseMap.get('Status').equalsIgnoreCase('Success')                                      
                                     ) ){
                                         
                                         system.debug('Queue_Fax api succeeded for for PgmMember id : '  + PgmMemberObj); 
                                         // Set the sub-status to "Fax Queued" and update the FaxDetailsID for this Program Member
                                         updateProgramMember(responseMap.get('Status'), responseMap.get('Result'), 'Fax Queued', PgmMemberObj, true); 
                                         system.debug('updated the \"success\" substatus and the queued fax ID : '+ PgmMemberObj); 
                                         return responseString;
                                     }else{ // API callout failed.             
                                         system.debug('Queue_Fax api Failed for PgmMember  : '  + PgmMemberObj); 
                                         // Set the sub status to "Fax Failed." 
                                          updateProgramMember(responseMap.get('Status'), responseMap.get('Result'), 'Fax Failed', PgmMemberObj, false);
                                         system.debug('updated the \"failed\" substatus for Program Member ID : '+ PgmMemberObj); 
                                         return responseString;
                                     }        
    }
    /*
*   Method   : updateProgramMember
*   Desc     : Update the Program Member object with the status of the Fax received from SRFax api.
*   @param   : queuedFaxStatus - Status of the queued fax.
*            : queuedFaxResult - Result of the queued fax.Program Member
*            : pgmMemberSubStatus   - SubStatus value to be updated. 
*            : PgmMemberId          - ID of the Program Member to be update.
*   @return  : none
*/    
    private static void updateProgramMember(String queuedFaxStatus, String queuedFaxResult, String pgmMemberSubStatus, String PgmMemberId, Boolean faxQueued){
        // set the queuedFaxStatus, queuedFaxResult, and Program Member substatus here
        List<Program_Member_MVN__c> PgmMemberList = [select 
                                                     Queued_Fax_Status_JKC__c, 
                                                     Queued_Fax_Result_JKC__c, 
                                                     Fax_Delivery_JKC__c,
                                                     Fax_Attempt_Counter_JKC__c,
                                                     Sub_Status_JKC__c,
                                                     Fax_Retry_Identifier_JKC__c,Fax_Sent_Time_JKC__c 
                                                     from 
                                                     Program_Member_MVN__c 
                                                     where 
                                                     ID =:PgmMemberId
                                                     LIMIT 1
                                                    ] ; 
        if(PgmMemberList == null || PgmMemberList.size() == 0){
            system.debug('Failed to update Program Member; Program Member with this ID does not exist !!!' + PgmMemberList); 
        }
        // set the requried values 
        PgmMemberList[0].Queued_Fax_Status_JKC__c = queuedFaxStatus; 
        PgmMemberList[0].Queued_Fax_Result_JKC__c = queuedFaxResult; 
        PgmMemberList[0].Sub_Status_JKC__c        = pgmMemberSubStatus;
        PgmMemberList[0].Fax_Delivery_JKC__c      = pgmMemberSubStatus.equalsIgnoreCase('Fax Queued') ? 'In Progress' : 'Failed' ; 
        if( PgmMemberList[0].Sub_Status_JKC__c !='Ready to Fax' ){
            PgmMemberList[0].Fax_Attempt_Counter_JKC__c= PgmMemberList[0].Fax_Attempt_Counter_JKC__c==null?0:PgmMemberList[0].Fax_Attempt_Counter_JKC__c;
            PgmMemberList[0].Fax_Attempt_Counter_JKC__c     += faxQueued == true? 1 : 0;
        }
        
        PgmMemberList[0].Fax_Sent_Time_JKC__c= system.now();
        if( PgmMemberList[0].Fax_Retry_Identifier_JKC__c ==false){
            PgmMemberList[0].Sub_Status_JKC__c        = pgmMemberSubStatus;
        }
        // update 
        update PgmMemberList ; 
       }  
    /*
*   Method   : catchException
*   Desc     : Method to log details of Exception. 
*   @param   : Exception - The exception object 
*   @param   : String - Name of the method in which the exception occured.       
*   @return  : none
*/    
    private static void catchException(Exception ex , String methodName ,Integer responseCode, String ObjectId ){    
        PAP_ErrorLog.logErrorActivity(ex, methodName , responseCode,  ObjectId);       
    }
    
}