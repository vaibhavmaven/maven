/**
* @author      : Persistent Systems.
* @date        : August 13 2019
* @description : Test Class to test PAP_PrescriptionUpdateHandler class.                 
*/

@isTest
public class PAP_UpdatePrescriptionHandlerTest {
    private static User caseManager;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;
    static {
        TestFactoryCustomMetadataMVN.setMocks();
        TestDataFactoryMVN.createPSSettings();
        
        caseManager = TestDataFactoryMVN.generateCaseManager();
        System.runAs(caseManager) {
            Program_MVN__c program = TestDataFactoryMVN.createProgram();
            Account testAccount = TestDataFactoryMVN.createMember();
            programMember = TestDataFactoryMVN.createProgramMember(program, testAccount);
            application = new Application_MVN__c(
                Program_Member_MVN__c = programMember.Id
            );
            insert application;
        }
    }
    
    @isTest
    public static void testPrescriptionUpdateToSecond(){
        Prescription_MVN__c expectedPrescription;
        
        System.runAs(caseManager) {
            Product_MVN__c product = new Product_MVN__c(
                Program_MVN__c = programMember.Program_MVN__c,
                Product_Status_MVN__c = 'Active');
            product.Product_Type_PAP__c = 'First Product';
            insert product;
            Test.startTest();
            expectedPrescription = new Prescription_MVN__c(
                Program_Member_MVN__c = programMember.Id,
                Product_MVN__c = product.Id
            );
            
            insert expectedPrescription;
            Prescription_MVN__c presc = [select id,Product_MVN__c from Prescription_MVN__c Limit 1];
             Product_MVN__c prod = new Product_MVN__c(
                Program_MVN__c = programMember.Program_MVN__c,
                Product_Status_MVN__c = 'Active');
            prod.Product_Type_PAP__c = 'Second Product';
            insert prod;
            presc.Product_MVN__c = prod.id;
            update presc;
            Test.stopTest();
        }
        
        Prescription_MVN__c actualPrescription = [
            SELECT
            Id,
            Application_MVN__c
            FROM
            Prescription_MVN__c
            WHERE
            Id = :expectedPrescription.Id
        ];
        
        System.assertEquals(
            application.Id,
            actualPrescription.Application_MVN__c
        );
    }
    

    @isTest
    public static void testPrescriptionUpdateToFirst(){
        Prescription_MVN__c expectedPrescription;
        
        System.runAs(caseManager) {
            Product_MVN__c product = new Product_MVN__c(
                Program_MVN__c = programMember.Program_MVN__c,
                Product_Status_MVN__c = 'Active');
            product.Product_Type_PAP__c = 'Second Product';
            insert product;
            Test.startTest();
            expectedPrescription = new Prescription_MVN__c(
                Program_Member_MVN__c = programMember.Id,
                Product_MVN__c = product.Id
            );
            
            insert expectedPrescription;
            Prescription_MVN__c presc = [select id,Product_MVN__c from Prescription_MVN__c Limit 1];
             Product_MVN__c prod = new Product_MVN__c(
                Program_MVN__c = programMember.Program_MVN__c,
                Product_Status_MVN__c = 'Active');
            prod.Product_Type_PAP__c = 'First Product';
            insert prod;
            presc.Product_MVN__c = prod.id;
            update presc;
            Test.stopTest();
        }
        
        Prescription_MVN__c actualPrescription = [
            SELECT
            Id,
            Application_MVN__c
            FROM
            Prescription_MVN__c
            WHERE
            Id = :expectedPrescription.Id
        ];
        
        System.assertEquals(
            application.Id,
            actualPrescription.Application_MVN__c
        );
    }    



    @isTest
    public static void testPrescriptionDeletionForFirst(){
        Prescription_MVN__c expectedPrescription;
        
        System.runAs(caseManager) {
            Product_MVN__c product = new Product_MVN__c(
                Program_MVN__c = programMember.Program_MVN__c,
                Product_Status_MVN__c = 'Active');
            product.Product_Type_PAP__c = 'First Product';
            insert product;
            Test.startTest();
            expectedPrescription = new Prescription_MVN__c(
                Program_Member_MVN__c = programMember.Id,
                Product_MVN__c = product.Id
            );
            
            insert expectedPrescription;
            Prescription_MVN__c presc = [select id,Product_MVN__c from Prescription_MVN__c Limit 1];
            delete presc;
            Test.stopTest();
        }
    }
    
    @isTest
    public static void testPrescriptionDeletionForSecond(){
        Prescription_MVN__c expectedPrescription;
        
        System.runAs(caseManager) {
            Product_MVN__c product = new Product_MVN__c(
                Program_MVN__c = programMember.Program_MVN__c,
                Product_Status_MVN__c = 'Active');
            product.Product_Type_PAP__c = 'Second Product';
            insert product;
            Test.startTest();
            expectedPrescription = new Prescription_MVN__c(
                Program_Member_MVN__c = programMember.Id,
                Product_MVN__c = product.Id
            );
            
            insert expectedPrescription;
            Prescription_MVN__c presc = [select id,Product_MVN__c from Prescription_MVN__c Limit 1];
            delete presc;
            Test.stopTest();
        }
    }    

}