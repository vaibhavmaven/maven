/*
* CreateCaseRedirectControllerTestMVN
* Created By: Roman Lerman
* Created Date: 3/6/2013
* Description: Test class for CreateCaseRedirectControllerMVN
*/
@isTest
private class CreateCaseRedirectControllerTestMVN {
	static Program_MVN__c program;
	static Program_Member_MVN__c programMember;
	static Account memberAccount;
	static Account physicianAccount;
	static Case cs;

	static ApexPages.StandardController controller;

	static {
		TestDataFactoryMVN.createPSSettings();

		program = TestDataFactoryMVN.createProgram();

		memberAccount = TestDataFactoryMVN.createMember();
		physicianAccount = TestDataFactoryMVN.createPhysician();

		programMember = TestDataFactoryMVN.createProgramMember(program, memberAccount);

		controller = new ApexPages.StandardController(new Case());
	}
    static testMethod void createActivityCaseWithAccount() {
        ApexPages.currentPage().getParameters().put('def_account_id', memberAccount.Id);
        ApexPages.currentPage().getParameters().put('recordType', 'activity');
        ApexPages.currentPage().getParameters().put('program', program.Id);
        ApexPages.currentPage().getParameters().put('programMember', programMember.Id);
        ApexPages.currentPage().getParameters().put('physician', physicianAccount.Id);

        CreateCaseRedirectControllerMVN extension = new CreateCaseRedirectControllerMVN(controller);

        Test.startTest();
        	extension.getRedirect();
        Test.stopTest();

        Case createdActivity = [select AccountId, Referred_By_MVN__c, Program_MVN__c, Program_Member_MVN__c 
                                from Case 
                                    where RecordType.DeveloperName = :Patient_Service_Settings_MVN__c.getInstance().Case_Activity_Record_Type_MVN__c
                                    and AccountId = :memberAccount.Id];

        System.assertEquals(physicianAccount.Id, createdActivity.Referred_By_MVN__c);
        System.assertEquals(program.Id, createdActivity.Program_MVN__c);
        System.assertEquals(programMember.Id, createdActivity.Program_Member_MVN__c);
    }

    static testMethod void createRequestWithAccount(){
        Account testAccount = TestDataFactoryMVN.createMember();

        ApexPages.currentPage().getParameters().put('def_account_id', memberAccount.Id);
        ApexPages.currentPage().getParameters().put('program', program.Id);
        ApexPages.currentPage().getParameters().put('programMember', programMember.Id);
        ApexPages.currentPage().getParameters().put('physician', physicianAccount.Id);

        CreateCaseRedirectControllerMVN extension = new CreateCaseRedirectControllerMVN(controller);

        Test.startTest();
        	extension.getRedirect();
        Test.stopTest();

        Case createdActivity = [select AccountId, Referred_By_MVN__c, Program_MVN__c, Program_Member_MVN__c 
                                from Case 
                                    where RecordType.DeveloperName = :Patient_Service_Settings_MVN__c.getInstance().Case_Request_Record_Type_MVN__c
                                    and AccountId = :memberAccount.Id];

        System.assertEquals(physicianAccount.Id, createdActivity.Referred_By_MVN__c);
        System.assertEquals(program.Id, createdActivity.Program_MVN__c);
        System.assertEquals(programMember.Id, createdActivity.Program_Member_MVN__c);
    }
}