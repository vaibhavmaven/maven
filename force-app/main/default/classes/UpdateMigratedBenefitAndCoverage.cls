/*
 * This batch class will read all migrated records from Patient Insurance and then will update Benefit&Coverage infromation accordingly.
 * To run this batch
 * UpdateMigratedBenefitAndCoverage updateMigrated = new UpdateMigratedBenefitAndCoverage();
   ID batchprocessid = Database.executeBatch(updateMigrated, 200);
 */
global class UpdateMigratedBenefitAndCoverage implements Database.Batchable<sObject> {

    global Database.QueryLocator start(Database.BatchableContext bc) {
       String status = 'Active';
       String query = 'SELECT Id, Plan_Type__c, GroupID__c, Status__c FROM Patient_Insurance__c';
       return Database.getQueryLocator(query);
   }
 
   global void execute(Database.BatchableContext bc, List<Patient_Insurance__c> records){
       // process each batch of records
       Set<Id> bncSet = new Set<Id>();
       for(Patient_Insurance__c pi:records)
       {
           bncSet.add(pi.GroupID__c);
       }
       Map<Id,Benefits_Coverage_MVN__c> bncList = new Map<Id,Benefits_Coverage_MVN__c>([SELECT id,Health_Insurance_MVN__c,Private_Insurance_MVN__c,Rx_Coverage_MVN__c,Medicaid_Coverage_MVN__c,Other_Government_Insurance_MVN__c,Medicare_Part_A_MVN__c,Medicare_Part_B_MVN__c,Medicare_Part_C_MVN__c,Medicare_Part_D_MVN__c,Plan_Type__c FROM Benefits_Coverage_MVN__c where id in :bncSet ]);
       for(Patient_Insurance__c pi:records)
       {
            Benefits_Coverage_MVN__c bncRecord = bncList.get(pi.GroupID__c);
            if(bncRecord==null) continue;
           String planType = (pi.Plan_Type__c==null)?'Other':pi.Plan_Type__c;
           switch on planType.toUpperCase()
           {
               when 'UNINSURED'
               {
                   bncRecord.Health_Insurance_MVN__c = 'No';
                   bncRecord.Private_Insurance_MVN__c = 'No';
                   bncRecord.Rx_Coverage_MVN__c = 'No';
                   bncRecord.Other_Government_Insurance_MVN__c = 'No';
                   bncRecord.Medicaid_Coverage_MVN__c = false;
                   bncRecord.Medicare_Part_A_MVN__c = 'No';
                   bncRecord.Medicare_Part_B_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_C_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_D_MVN__c = 'No Answer';
               }
               when 'COMMERCIAL'
               {
                   bncRecord.Health_Insurance_MVN__c = 'Yes';
                   bncRecord.Private_Insurance_MVN__c = 'Yes';
                   bncRecord.Rx_Coverage_MVN__c = 'Yes';
                   bncRecord.Other_Government_Insurance_MVN__c = 'No';
                   bncRecord.Medicaid_Coverage_MVN__c = false;
                   bncRecord.Medicare_Part_A_MVN__c = 'No';
                   bncRecord.Medicare_Part_B_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_C_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_D_MVN__c = 'No Answer';                   
               }
               when 'MEDICARE'
               {
                   bncRecord.Health_Insurance_MVN__c = 'Yes';
                   bncRecord.Private_Insurance_MVN__c = 'No';
                   bncRecord.Rx_Coverage_MVN__c = 'No';
                   bncRecord.Other_Government_Insurance_MVN__c = 'No';
                   bncRecord.Medicaid_Coverage_MVN__c = false;
                   bncRecord.Medicare_Part_A_MVN__c = 'Yes';
                   bncRecord.Medicare_Part_B_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_C_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_D_MVN__c = 'No Answer';                   
               }
               when 'MEDICAID'
               {
                   bncRecord.Health_Insurance_MVN__c = 'Yes';
                   bncRecord.Private_Insurance_MVN__c = 'No';
                   bncRecord.Rx_Coverage_MVN__c = 'No';
                   bncRecord.Other_Government_Insurance_MVN__c = 'Yes';
                   bncRecord.Medicaid_Coverage_MVN__c = true;
                   bncRecord.Medicare_Part_A_MVN__c = 'No';
                   bncRecord.Medicare_Part_B_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_C_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_D_MVN__c = 'No Answer';                   
               }
               when 'MEDICARE SUPPLEMENT'
               {
                   bncRecord.Health_Insurance_MVN__c = 'Yes';
                   bncRecord.Private_Insurance_MVN__c = 'Yes';
                   bncRecord.Rx_Coverage_MVN__c = 'No Answer';
                   bncRecord.Other_Government_Insurance_MVN__c = 'No';
                   bncRecord.Medicaid_Coverage_MVN__c = false;
                   bncRecord.Medicare_Part_A_MVN__c = 'Yes';
                   bncRecord.Medicare_Part_B_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_C_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_D_MVN__c = 'No Answer';                   
               }
               when 'OTHER STATE & GOVERNMENT'
               {
                   bncRecord.Health_Insurance_MVN__c = 'Yes';
                   bncRecord.Private_Insurance_MVN__c = 'No';
                   bncRecord.Rx_Coverage_MVN__c = 'No';
                   bncRecord.Other_Government_Insurance_MVN__c = 'Yes';
                   bncRecord.Medicaid_Coverage_MVN__c = false;
                   bncRecord.Medicare_Part_A_MVN__c = 'No';
                   bncRecord.Medicare_Part_B_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_C_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_D_MVN__c = 'No Answer';
               }
               when else
               {
                   bncRecord.Health_Insurance_MVN__c = 'No Answer';
                   bncRecord.Private_Insurance_MVN__c = 'No';
                   bncRecord.Rx_Coverage_MVN__c = 'No';
                   bncRecord.Other_Government_Insurance_MVN__c = 'No';
                   bncRecord.Medicaid_Coverage_MVN__c = false;
                   bncRecord.Medicare_Part_A_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_B_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_C_MVN__c = 'No Answer';
                   bncRecord.Medicare_Part_D_MVN__c = 'No Answer'; 
               }
           }
           bncRecord.Plan_Type__c = pi.Plan_Type__c;
       }
       update bncList.values();
   }    
 
   global void finish(Database.BatchableContext bc){
       // execute any post-processing operations
   }    
}