/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description implements Boolean Comparison rule
 */
public with sharing class EligibilityEngineBooleanCompHdlrMVN extends EligibilityEngineRuleHdlrMVN {
    /**
     * checks if rule is fired for boolean: if source and value are equal
     * @return  Boolean     flag if rule is fired
     */

    protected override Boolean evaluateSingleRecord(SObject obj) {
        Object sourceFieldObject = super.getSource(obj);
        if (sourceFieldObject == null) {
            return true;
        }
        Boolean sourceField = Boolean.valueOf(sourceFieldObject);
        Boolean value = Boolean.valueOf(rule.Value_MVN__c);
        return sourceField == value;
    }
}