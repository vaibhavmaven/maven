@isTest  // making this class visible to Testing framework.
public class EligibilityResponseTest {
    
    private Static String username = 'username';
    private Static String passwd = 'password'; 
    private Static String httpMethod = 'method'; 
    private Static String httpEndpoint = 'endpoint'; 
    private Static String httpPayload = 'payload' ;        
        

    @isTest
    public static void testEligibilitySuccess()
    {
        Test.startTest();    // Resetting the DML limits here. 
        // setting the mock.
        Test.setMock(HttpCalloutMock.class, new EligibilitySuccessHttpCalloutMock());
        AuthenticateAndCallout authnCall = EligibilityResponseTest.testHelper(); 
        // Verify status code
        System.debug('In testEligibilitySuccess : ' + authnCall.getResponseCode());
        System.assertEquals(200, authnCall.getResponseCode(), 'The status code is not 200.');
    }
    
    @isTest
    public static void testEligibilityFailure()
    {
        Test.startTest();    // Resetting the DML limits here. 
        // setting the mock.
        Test.setMock(HttpCalloutMock.class, new EligibilityFailureHttpCalloutMock());
        AuthenticateAndCallout authnCall = EligibilityResponseTest.testHelper(); 
        // Verify status code
        System.assertEquals(200, authnCall.getResponseCode(), 'The status code is not 200.');
    }

    @isTest
    public static void testSuccessWithoutAuthHeaders()
    {
        Test.startTest();    // Resetting the DML limits here. 
        // setting the mock.
        Test.setMock(HttpCalloutMock.class, new EligibilityFailureHttpCalloutMock());
        username = ''; 
        passwd = ''; 
        AuthenticateAndCallout authnCall = EligibilityResponseTest.testHelper(); 
        // Verify status code
        System.debug('In testSuccessWithoutAuthHeaders : ' + authnCall.getResponseCode());
        System.assertEquals(200, authnCall.getResponseCode(), 'The status code is not 200.');        
    }
    
    @isTest
    public static void testFailureWithoutAuthHeaders()
    {
        Test.startTest();    // Resetting the DML limits here. 
        // setting the mock.
        Test.setMock(HttpCalloutMock.class, new FailedHttpCalloutMock());
        username = ''; 
        passwd = ''; 
        AuthenticateAndCallout authnCall = EligibilityResponseTest.testHelper(); 
        // Verify status code
        System.assertNotEquals(200, authnCall.getResponseCode(), 'The status code should not be 200.');        
    }    
    
    
    @isTest
    public static void testAuthHeaderFailure()
    {
        Test.startTest();    // Resetting the DML limits here. 
        // setting the mock.
        Test.setMock(HttpCalloutMock.class, new FailedHttpCalloutMock());
        username = ''; 
        AuthenticateAndCallout authnCall = EligibilityResponseTest.testHelper(); 
        // Verify status code
        System.debug('In testAuthHeaderFailure status code is : '  + authnCall.getResponseCode());
        System.assertNotEquals(200, authnCall.getResponseCode(), 'The status code is 200.');             
        
    }
    
    @isTest
    public static void testEndpointFailure()
    {
        Test.startTest();    // Resetting the DML limits here. 
        // setting the mock.
        Test.setMock(HttpCalloutMock.class, new FailedHttpCalloutMock());
        httpEndpoint = ''; 
        AuthenticateAndCallout authnCall = EligibilityResponseTest.testHelper(); 
        // Verify status code
        System.debug('In testEndpointFailure status code is : '  + authnCall.getResponseCode());
        System.assertNotEquals(200, authnCall.getResponseCode(), 'The status code is 200.');          
    }
    
    public static AuthenticateAndCallout testHelper()
    {
        AuthenticateAndCallout authnCall = new AuthenticateAndCallout();
        authnCall.setBasicAuthenticationDetails(username, passwd); 
        authnCall.setEndpointDetails(httpMethod, httpEndpoint, httpPayload); 
        authnCall.makeCallout();
        System.debug('Response code is : ' + authnCall.getResponseCode()); 
        System.debug('Response is : ' + authnCall.getResponse());           
        return authnCall;
    }
}