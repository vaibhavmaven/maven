/**
*   ProgramDeployTestMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 22nd, 2017
*   Description:    tests ProgramDeployMVN
*/
@isTest public class ProgramDeployTestMVN {
    /**
    * @description setups data for all unit tests
    */
    @testSetup private static void setupData() {
        TestDataFactoryMVN.createPSSettings();
        Program_MVN__c program = TestDataFactoryMVN.createProgram();
        program.Program_Id_MVN__c = ProgramDeployWrapperTestMVN.EXTERNALID;
        update program;
        Program_Stage_MVN__c parentProgramStage = TestDataFactoryMVN.createParentProgramStage(program);
        Program_Stage_MVN__c programStageOne = TestDataFactoryMVN.createChildProgramStage(program,
                                                                                          parentProgramStage,
                                                                                          'Stage ONE');
        Program_Stage_MVN__c programStageTwo = TestDataFactoryMVN.createChildProgramStage(program,
                                                                                          parentProgramStage,
                                                                                          'Stage TWO');
        TestDataFactoryMVN.createStageDependency(programStageOne, programStageTwo);
        Medical_History_Type_MVN__c medHistType = new Medical_History_Type_MVN__c(
            Order_MVN__c = 1,
            Program_MVN__c = program.Id,
            Record_Type_Name_MVN__c = 'TEST'
        );
    }

    /**
    * @description tests the constructor is setting the program and all related records
    */
    @isTest private static void testConstructorIsInitiated() {
        Test.startTest();
            ProgramDeployMVN programToDeploy = new ProgramDeployMVN();
        Test.stopTest();

        System.assertNotEquals(null, programToDeploy);
    }

    /**
    * @description tests the constructor is setting the program and all related records
    */
    @isTest private static void testConstructorIsInitiatedAndProgramAndRecordsAreSet() {
        Test.startTest();
            ProgramDeployMVN programDeploy = new ProgramDeployMVN(ProgramDeployWrapperTestMVN.EXTERNALID);
        Test.stopTest();

        System.assertNotEquals(null, programDeploy.programToDeploy);
        System.assertNotEquals(null, programDeploy.programToDeploy.program);
        System.assertNotEquals(null, programDeploy.programToDeploy.programStageParents);
        System.assertNotEquals(null, programDeploy.programToDeploy.programStageChildren);
        System.assertNotEquals(null, programDeploy.programToDeploy.programStageDependencies);
        System.assertNotEquals(null, programDeploy.programToDeploy.programStageFieldMaps);
        System.assertNotEquals(null, programDeploy.programToDeploy.medicalHistoryTypes);
    }

    /**
    * @description tests deploying from JSON
    */
    @isTest private static void testDeployingFromJson() {
        Program_MVN__c program = [SELECT Id, Program_ID_MVN__c FROM Program_MVN__c LIMIT 1];
        program.Program_ID_MVN__c = 'Something else';
        update program;
        ProgramDeployWrapperMVN programToDeploy = new ProgramDeployWrapperMVN().parse(ProgramDeployWrapperTestMVN.JSON);

        Test.startTest();
            ProgramDeployMVN programDeploy = new ProgramDeployMVN(programToDeploy);
            programDeploy.deploy();
        Test.stopTest();

        System.assertNotEquals(null, programDeploy.programToDeploy.program.Id);
        System.assertEquals(ProgramDeployWrapperTestMVN.EXTERNALID,
                            programDeploy.programToDeploy.program.Program_ID_MVN__c
        );
    }

    /**
    * @description tests deploying from JSON
    */
    @isTest private static void testDeployingFromJsonWithComparison() {
        Program_MVN__c program = [SELECT Id, Program_ID_MVN__c FROM Program_MVN__c LIMIT 1];
        program.Program_ID_MVN__c = 'Something else';
        update program;
        ProgramDeployWrapperMVN programToDeployComp = new ProgramDeployWrapperMVN().parse(ProgramDeployWrapperTestMVN.JSON);
        ProgramDeployMVN programDeployComp = new ProgramDeployMVN(programToDeployComp);
        programDeployComp.deploy();
        TestDataFactoryMVN.createParentProgramStage(programToDeployComp.program);
        ProgramDeployWrapperMVN programToDeploy = new ProgramDeployWrapperMVN().parse(ProgramDeployWrapperTestMVN.JSON);

        Test.startTest();
            ProgramDeployMVN programDeploy = new ProgramDeployMVN(programToDeploy);
            programDeploy.deploy();
        Test.stopTest();

        System.assertNotEquals(null, programDeploy.programToDeploy.program.Id);
        System.assertEquals(ProgramDeployWrapperTestMVN.EXTERNALID,
                            programDeploy.programToDeploy.program.Program_ID_MVN__c
        );
    }

    /**
    * @description tests going to detail view of program
    */
    @isTest private static void testProgramViewPageReference() {
        ProgramDeployMVN programDeploy = new ProgramDeployMVN(ProgramDeployWrapperTestMVN.EXTERNALID);

        Test.startTest();
            PageReference actualPageReference = programDeploy.view();
        Test.stopTest();

        System.assertNotEquals(null, actualPageReference);
        System.assert(
            actualPageReference.getUrl().contains(
                ((String)programDeploy.programToDeploy.program.Id).subString(0,14)
            )
        );
    }
}