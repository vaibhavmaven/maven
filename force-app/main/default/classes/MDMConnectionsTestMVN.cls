/*
 * MDMConnectionsTestMVN
 * @created by: Pablo Roldan
 * @created Date: Oct 20, 2017
 * @description: Test class for MDMConnectionsMVN
 */
@IsTest
private class MDMConnectionsTestMVN {

	@isTest
	static void itShouldMergeMDMConnectionDefaultConfigured() {
		TestFactoryCustomMetadataMVN.setMocks();
		Test.startTest();
		MDMConnectionsMVN mdmConnectionsCtrl = new MDMConnectionsMVN('ZZ');
		System.assertNotEquals(null, mdmConnectionsCtrl.setting);
		mdmConnectionsCtrl.mergeSettings(mdmConnectionsCtrl.setting);
		Test.stopTest();
	}

	@isTest
	static void itShouldGetMDMConnectionConfigured() {
		TestFactoryCustomMetadataMVN.setMocks();
		Test.startTest();
		MDMConnectionsMVN mdmConnectionsCtrl = new MDMConnectionsMVN('US');
		Test.stopTest();

		System.assertNotEquals(null, mdmConnectionsCtrl.setting);
	}
}