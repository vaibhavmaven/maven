/**
 * @author Mavens
 * @date 07/03/2018
 * @description Controller for the SpliFaxMVN.page.
 *  Prompt the user to enter start and end pages to split the document.
 *  Generate new child cases attaching the new split documents.
 *  Only works with Files.
 */ 
public with sharing class SplitFaxCtrlMVN {

    public Id caseId { get; private set; }
    public Case thisCase { get; private set; }
    public Id documentId { get; private set; }
    public Id selectedDoc { get; set; }
    public Id selectedAttachment { get; set; }
    public Map<Id, Document_MVN__c> documentMap { get; private set; }
    public ContentVersion fileVersion { get; private set; }

    public Boolean error { get; private set; }
    public List<ResultCaseWrapper> childCases { get; private set; }
    public List<ParamInfo> nintexParamInfoList { get; set; }
    public String step { get; set; }
    public List<SelectOption> documentOptions { get; set; }

    // Variables for the Split Document table section
    public List<SplitDocumentWrapper> splitDocumentList { get; set; }
    public Integer rowToRemove { get; set; }
    public String nintexErrorMsg { get; set; }

    private String PARENT_CASE_STATUS = 'Cancelled';

    /**
    * @description Wrapper with the information for the split document
    */ 
    public class SplitDocumentWrapper {
        public Integer index { get; set; }
        public Integer startPage { get; set; }
        public Boolean startPageError { get; set; }
        public Integer endPage { get; set; }
        public Boolean endPageError { get; set; }
        public String docName { get; set; }
        public Boolean docNameError { get; set; }
    }

    // Variables for the Child Cases table section
    public String returnUri { get; set; }
    public String fileName { get; set; }

    /**
    * @description Wrapper with the information for the generated case and file
    */ 
    public class ResultCaseWrapper {
        public String caseUrl { get; set; }
        public String caseNumber { get; set; }
        public String fileUrl { get; set; }
        public String fileName { get; set; }
        public Id caseId { get; set; }
        public String caseType { get; set; }
    }

    // Variables to handle component visibility
    public Boolean isChildCasesPanelVisible { get; private set;}
    public Boolean isNintexPanelVisible { get; private set;}
    public Boolean inProgress { get; private set;}

    private Id contentDocumentId;
    private String documentName;
    private Boolean blankFieldError;
    private Boolean pageNumberError;
    private Boolean wrongValueError;

    /**************************************************************************************
     * @description Constructor that initializes the picklist field with the list of documents
     * available for this Case. 
     */ 
    public SplitFaxCtrlMVN(ApexPages.StandardController stdController) {
        this.caseId = stdController.getId();
        this.getCase();
        this.getPdfDocuments();
        if (this.documentMap.isEmpty()) {
            error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                                        Label.No_Files_Found_MVN));
        } else {
            error = false;
            this.buildDocumentPicklistField();
            this.step = 'selection';
        }
    }

    /**************************************************************************************
     * @description Retrieve fields from current case defined in the field set 
     * "Split Fax Child Cases Population"
     */ 
    private void getCase() {
        List<String> fieldSetFields = new List<String>();
        for (Schema.FieldSetMember field : SObjectType.Case.FieldSets.Split_Fax_Child_Cases_Population_MVN.getFields()) {
            fieldSetFields.add(field.getFieldPath());
        }
        String fieldsToQuery = String.join(fieldSetFields, ',');
        String query = 'SELECT ' + fieldsToQuery +
                        ' FROM Case' +
                        ' WHERE Id = \'' + this.caseId + '\'';
        this.thisCase = Database.query(query);
    }

    /**************************************************************************************
     * @description Retrieve map of documents (with its corresponding attachment) linked to the Case
     */ 
    private void getPdfDocuments() {
        this.documentMap = new Map<Id, Document_MVN__c>([
            SELECT Id, Title_MVN__c, Type_MVN__c, Attachment_Id_MVN__c,
                (SELECT Id, ParentId, ContentType
                FROM Attachments
                LIMIT 1)
            FROM Document_MVN__c
            WHERE Case_MVN__c = :caseId
        ]);
    }

    /**************************************************************************************
     * @description Build list of documents to be displayed in a picklist field to the user
     */ 
    private void buildDocumentPicklistField() {
        this.documentOptions = new List<SelectOption>();
        for(Id docId : this.documentMap.keySet()) {
            this.documentOptions.add(
                new SelectOption(
                    String.valueOf(docId),
                    this.documentMap.get(docId).Title_MVN__c
                )
            );
        }
    }

    /**************************************************************************************
     * @description Convert selected document to file to be able to split it with the Nintex
     * variable
     */ 
    public PageReference showSplitScreen() {
        step = 'split';
        fileVersion = FileHdlrMVN.convertAttachmentToFile(this.documentMap.get(selectedDoc));
        selectedAttachment = this.documentMap.get(selectedDoc).Attachment_Id_MVN__c;
        this.initVariables();
        return null;
    }

    /**************************************************************************************
     * @description Initialize variables 
     */ 
    private void initVariables() {
        this.documentId = this.fileVersion.Id;
        this.contentDocumentId = this.fileVersion.ContentDocumentId;
        this.documentName = this.fileVersion.Title;
        this.nintexParamInfoList = new List<ParamInfo>();
        this.splitDocumentList = new List<SplitDocumentWrapper>();
        this.addNewRow();
        this.childCases = new List<ResultCaseWrapper>();
        this.inProgress = false;
    }

    /**************************************************************************************
     * @description Remove an entry row entered by the user from the Split Document table.
     * Remove any previous error notifications.
     */ 
    public void removeRow(){
        this.resetErrors();
        this.splitDocumentList.remove(rowToRemove);
    }

    /****************************************************************************************
     * @description Add an entry row to the Split Document table to add information for 
     * a new split document. Remove any previous error notifications.
     */ 
    public void addNewRow(){
        this.resetErrors();

        SplitDocumentWrapper newRow = new SplitDocumentWrapper();
        newRow.index = this.splitDocumentList.size();
        this.splitDocumentList.add(newRow);
    }

    /****************************************************************************************
     * @description Add an entry row to the Split Document table to add information for 
     * a new split document. Add errors to all the fields in that row.
     */ 
    public void addNewRowWithErrors(){
        this.addNewRow();
        this.splitDocumentList.get(0).startPageError = true;
        this.splitDocumentList.get(0).endPageError = true;
        this.splitDocumentList.get(0).docNameError = true;
    }

    /****************************************************************************************
     * @description Validate parameters entered by the user to split original document. If
     * not valid, then display an error. If valid, create a child case per each document and
     * define the Nintex records needed to start the generation of the documents by the
     * Nintex pacakge.
     */ 
    public PageReference generateDocuments() {
        this.resetErrors();

        if (!this.areParametersValid()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, this.buildErrorMessage()));
            return null;
        }

        Map<Id, Id> drawLoopIdsMap = this.createDdp();
        this.nintexParamInfoList = new List<ParamInfo>();

        for (Id ddpId : drawLoopIdsMap.keySet()) {
            Case childCase = this.createChildCase(this.thisCase);
            this.nintexParamInfoList.add(
                generateParamInfo(ddpId, drawLoopIdsMap.get(ddpId), childCase.Id)
            );
        }
        this.isChildCasesPanelVisible = true;
        this.isNintexPanelVisible = true;
        this.inProgress = true;

        return null;
    }

    /****************************************************************************************
     * @description Create child Case record.
     * Due Date, Subject and Type are set via workflow rule: Set Inbound Fax Case Fields_MVN
     * @return Case new child case record generated
     */ 
    private Case createChildCase(Case parentCase) {
        Case childCase = new Case(
            ParentId = parentCase.Id
        );
        for (Schema.FieldSetMember field : SObjectType.Case.FieldSets.Split_Fax_Child_Cases_Population_MVN.getFields()) {
            childCase.put(field.getFieldPath(), parentCase.get(field.getFieldPath()));
        }
        Datetime now = Datetime.now();		
        childCase.Current_User_Start_Time_JKC__c=now; 
        insert childCase;
        return childCase;
    }

    /****************************************************************************************
     * @description Generate ParamInfo object needed for Nintex library to split document
     * @return ParamInfo ParamInfo record
     */ 
    private ParamInfo generateParamInfo(Id ddpId, Id deliveryOptionId, Id caseId) {
        Loop.ProcessDdpParameters param = new Loop.ProcessDdpParameters(ddpId, deliveryOptionId, 
                                                                        caseId, new List<Id>());
        param.theme = Loop.ProcessDdpParameters.ThemeStyle.BOOTSTRAP_3_2;
        param.onCompleteCallback = 'onCompleteCallback';
        param.onErrorCallback = 'onErrorCallback';
        return new ParamInfo(param);
    }

    /****************************************************************************************
     * @description Remove any errors.
     */ 
    private void resetErrors() {

        this.blankFieldError = false;
        this.pageNumberError = false;
        this.wrongValueError = false;

        for (SplitDocumentWrapper splitDoc : this.splitDocumentList) {
            splitDoc.startPageError = false;
            splitDoc.endPageError = false;
            splitDoc.docNameError = false;
        }
    }

    /****************************************************************************************
     * @description Check if the parameters entered by the user are valid
     * @return True if all parameters entered by the user are valid.
     */ 
    private Boolean areParametersValid() {
        List<Integer> rowsToRemove = new List<Integer>();
        for (SplitDocumentWrapper splitDoc : this.splitDocumentList) {

            if (this.addRowToRemoveListIfEmpty(splitDoc, rowsToRemove)) {
                continue;
            }

            this.hasBlankFieldError(splitDoc);
            this.hasWrongValueError(splitDoc);
            this.hasPageNumberError(splitDoc);
        }

        // Remove empty rows
        for(Integer i = rowsToRemove.size() - 1; i >= 0; i--){
            this.splitDocumentList.remove(rowsToRemove.get(i));
        }

        // If document list is empty, add at least 1 empty row and highlight the errors
        if (this.splitDocumentList.isEmpty()) {
            this.addNewRowWithErrors();
            this.blankFieldError = true;
        }
        return !(this.blankFieldError || this.pageNumberError || this.wrongValueError);
    }

    /******************************************************************************************
     * @description If all the fields in a row are empty, add the index to a list to be removed
     * @param splitDoc doc wrapper with the information entered by the user
     * @param list of indexes of the rows that need to be removed
     * @return True if the whole row is empty
     */ 
    private Boolean addRowToRemoveListIfEmpty(SplitDocumentWrapper splitDoc, List<Integer> rowsToRemove) {
        if ((splitDoc.startPage == null || splitDoc.startPage == 0) && 
            (splitDoc.endPage == null || splitDoc.endPage == 0) &&
            (String.isBlank(splitDoc.docName))) {
            
            rowsToRemove.add(splitDoc.index);
            return true;
        }
        return false;
    }

    /******************************************************************************************
     * @description Check if there is any field empty in the row. If there is, set to true
     * the error attribute for that field; and set the class variable blankFieldError to True.
     * @param splitDoc doc wrapper with the information entered by the user
     */ 
    private void hasBlankFieldError(SplitDocumentWrapper splitDoc) {
        if (splitDoc.startPage == null || splitDoc.startPage == 0) {
            splitDoc.startPageError = true;
            this.blankFieldError = true;
        }
        if (splitDoc.endPage == null || splitDoc.endPage == 0) {
            splitDoc.endPageError = true;
            this.blankFieldError = true;
        }
        if (String.isBlank(splitDoc.docName)) {
            splitDoc.docNameError = true;
            this.blankFieldError = true;
        }
    }

    /******************************************************************************************
     * @description Check if the page numbers are valid numeric values > 0. If they are not,
     * set to true the error attribute for that field; and set the class var wrongValueError to True.
     * @param splitDoc doc wrapper with the information entered by the user
     */ 
    private void hasWrongValueError(SplitDocumentWrapper splitDoc) {
        if (splitDoc.startPage != null && splitDoc.startPage < 0) {
            splitDoc.startPageError = true;
            this.wrongValueError = true;
        }
        if (splitDoc.endPage != null && splitDoc.endPage < 0) {
            splitDoc.endPageError = true;
            this.wrongValueError = true;
        }
    }

    /******************************************************************************************
     * @description Check if the end page number is greater than start page number. 
     * If it is not, set to true the error attribute for those fields;
     * and set the class var hasPageNumberError to True.
     * @param splitDoc doc wrapper with the information entered by the user
     */ 
    private void hasPageNumberError(SplitDocumentWrapper splitDoc) {
        if (splitDoc.startPage != null && splitDoc.startPage >= 0 && 
            splitDoc.endPage != null && splitDoc.endPage >= 0  && 
            splitDoc.startPage > splitDoc.endPage) {

            splitDoc.startPageError = true;
            splitDoc.endPageError = true;
            this.pageNumberError = true;
        }
    }

    /******************************************************************************************
     * @description Build the error message to display when the user clicks the Split Document
     * button. The error message depends on the different validations: blank fields, wrong page
     * numbers or wrong format values.
     * @return String with the error to display
     */ 
    private String buildErrorMessage() {
        String errorMsg = Label.Split_Document_Fix_The_Errors_MVN + ':<br/>';
        if (blankFieldError) {
            errorMsg += '  * ' + Label.Split_Document_Blank_Field_Error_MVN + '<br/>';
        }
        if (pageNumberError) {
            errorMsg += '  * ' + Label.Split_Document_Page_Number_Error_MVN + '<br/>';
        }
        if (wrongValueError) {
            errorMsg += '  * ' + Label.Split_Document_Wrong_Value_Error_MVN + '<br/>';
        }
        return errorMsg;
    }

    /**************************************************************************************
     * @description Creates Nintex records needed for the generation of the split documents
     * @return Map with Ids for the new records that will be processed by Nintex package
     */ 
    public Map<Id, Id> createDdp() {

        Map<Id, Id> drawLoopIdsMap = new Map<Id, Id>();

        Id devRecordTypeId = SObjectType.Loop__Ddp__c.getRecordTypeInfosByName().
            get('Case').getRecordTypeId();
        Id delopRecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Attach').getRecordTypeId();

        for (SplitDocumentWrapper splitDoc : this.splitDocumentList) {

            Loop__Ddp__c newDdp = new Loop__Ddp__c(
                Name = splitDoc.docName, 
                Loop__Output_Filename__c = splitDoc.docName,
                RecordTypeId = devRecordTypeId
            );
            insert newDdp;

            Loop__DDP_Integration_Option__c newDeliveryOption = new Loop__DDP_Integration_Option__c(
                Name = splitDoc.docName, 
                Loop__Ddp__c = newDdp.Id, 
                Loop__Output__c = 'PDF',
                RecordTypeId = delopRecordTypeId
            );
            insert newDeliveryOption;

            Loop__DDP_File__c ddpFile = new Loop__DDP_File__c(
                Loop__Ddp__c = newDdp.Id, 
                Loop__Document_Id__c = contentDocumentId, 
                Loop__Type2__c = 'content', 
                Loop__Order__c = splitDoc.index + 1, 
                Loop__Type__c = 'pdf',
                Loop__First_Page__c = splitDoc.startPage,
                Loop__Last_Page__c = splitDoc.endPage
            );
            insert ddpFile;

            // We are inserting a blank word doc inside the package as a workaround to
            // the Nintex bug that doesn't allow us to split pdf docs containing the 
            // last page.
            Loop__DDP_File__c nintexWorkaround = new Loop__DDP_File__c(
                Loop__Ddp__c = newDdp.Id, 
                Loop__Document_Id__c = Patient_Service_Settings_MVN__c.getInstance().Nintex_blank_doc__c, 
                Loop__Type2__c = 'content', 
                Loop__Type__c = 'word',
                Loop__First_Page__c = 0,
                Loop__Last_Page__c = 0 
            );
            insert nintexWorkaround;

            drawLoopIdsMap.put(newDdp.id, newDeliveryOption.id);
        }
        return drawLoopIdsMap;
    }

    /**
    * @description Nintext class used for splitting the document
    */ 
    public class ParamInfo {
        public Loop.ProcessDdpParameters Parameters { get; set; }
        public ParamInfo(Loop.ProcessDdpParameters parameters) {
            this.Parameters = parameters;
        }
    }

    /*************************************************************************************
     * @description This method is called when the Nintex package finishes the generation
     * of a document. It creates the wrapper record to display the new child case created
     * along its attached document and their urls.
     */ 
    public void onCompleteNintexCallbackApex() {

        ResultCaseWrapper result = new ResultCaseWrapper();
        result.caseUrl = returnUri;
        result.fileName = fileName;

        String caseId = returnUri.substringAfterLast('/');
        if (!String.isBlank(caseId)) {

            Case c = [
                SELECT caseNumber, Type
                FROM Case
                WHERE Id = :caseId
            ];
            if (c != null) {
                result.caseNumber = c.caseNumber;
                result.caseId = c.Id;
                result.caseType = c.Type;
           
                Document_MVN__c doc = [
                    SELECT Id, Title_MVN__c, Type_MVN__c, Attachment_Id_MVN__c
                    FROM Document_MVN__c
                    WHERE Case_MVN__c = :c.Id
                ];
                if (doc != null) {
                    result.fileUrl = '/servlet/servlet.FileDownload?file=' + doc.Attachment_Id_MVN__c;
                }
            }
        }
        this.childCases.add(result);

        // When the last document has been generated, delete the temporary File
        if (this.childCases.size() == this.splitDocumentList.size()) {
            delete (new ContentDocument(Id = this.contentDocumentId));
            this.thisCase.Status = PARENT_CASE_STATUS;
            update this.thisCase;
        }
    }

    /*************************************************************************************
     * @description This method is called when the Nintex package throws an error during 
     * the generation of a document. It displays the error message thrown by Nintex to the 
     * user.
     */ 
    public void onErrorNintexCallbackApex() {
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, nintexErrorMsg));
    }

    /*************************************************************************************
     * @description Go back to the selection screen when you are on the split screen
     */ 
    public PageReference back() {
        this.step = 'selection';
        return null;
    }
}