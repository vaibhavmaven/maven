/**
 * VeevaNetworkCheckDCRBatchScheduleMVN
 * @created By: Pablo Roldan
 * @created Date: September 2017
 * @edited by: Pablo Roldan
 * @edited date: November 2017
 *
 * @description: connects to an external Veeva Network to check DCR submitted status
 *				wait for response to update Accounts linked
 *
 * @edit description: Adapted to PS.
 */
public class VeevaNetworkCheckDCRBatchScheduleMVN implements Database.Batchable<SObject>,
                                                       Database.AllowsCallouts,
                                                       Database.Stateful,
                                                       Schedulable {

    Set<String> errors;

    /**
    * EXECUTE SCHEDULE
    * @description: Call batch when the schedule runs
    */
    public void execute(SchedulableContext sc) {
        VeevaNetworkCheckDCRBatchScheduleMVN vnCheckDCRBatch = new VeevaNetworkCheckDCRBatchScheduleMVN();
        // It must be under 100 due to the Call out limit in Salesforce.
        // Also we need to consider the call to get the session Id
        database.executebatch(vnCheckDCRBatch, 49);
    }

    /**
    * START BATCH
    * @description: It executes a query to get any DCR with status Pending and any DCR Line linked to it
    */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        errors = new Set<String>();
        return Database.getQueryLocator([SELECT Id, Record_External_ID_MVN__c, DCR_External_ID_MVN__c, Status_MVN__c, Country_MVN__c,
                                                Notes_MVN__c, Account_MVN__c, Account_MVN__r.RecordType.DeveloperName, Parent_Account_MVN__c,
                                                RecordTypeId, RecordType.DeveloperName, License_MVN__c,
                                                   (SELECT Id, Field_API_Name_MVN__c, New_Value_MVN__c, Final_Value_MVN__c, Is_Address_Field_MVN__c,
                                                        Old_Value_MVN__c
                                                   FROM Data_Change_Request_Lines_MVN__r)
                                           FROM Data_Change_Request_MVN__c
                                          WHERE Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_SUBMITTED
                                              AND DCR_External_ID_MVN__c != null
                                            AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount]);
    }

    /**
    * EXECUTE BATCH
    * @description: It will execute a Batch job per Remote Veeva org.
    *                Will call these remote veeva orgs to pull any Medical Inquiry Submitted which wasn't converted to Inbound Form yet.
    *                It will deserialize this answer and transform it from Medical Inqury to Inbound Form using the mapping stored in a custom metadata.
    *                It will store all these Inbound Forms into the Database.
    */
    public void execute(Database.BatchableContext BC, List<SObject> scope) {
        if(!scope.isEmpty()){
            List<Data_Change_Request_MVN__c> dcrsToUpdate = new List<Data_Change_Request_MVN__c>();

            List<VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN> networkChangeRequestRecords = VeevaNetworkConnectionUtilMVN.getNetworkDCRRequestsBody((List<Data_Change_Request_MVN__c>) scope,
                                                                                                                                                                  VeevaNetworkConnectionUtilMVN.DCR_SUBMITTED);

            for(VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN networkChangeRequestRecord : networkChangeRequestRecords) {
                VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRequest = new VeevaNetworkConnectionUtilMVN.NetworkRequestMVN(networkChangeRequestRecord);
                VeevaNetworkConnectionUtilMVN.NetworkResultMVN retrieveChangeRequestRslt = VeevaNetworkConnectionUtilMVN.retrieveChangeRequest(networkRequest);
                dcrsToUpdate.addAll(VeevaNetworkConnectionUtilMVN.getDCRsToUpdate(networkRequest, retrieveChangeRequestRslt));
            }

            // Update DCR and DCR Lines with new Status and Final values
            VeevaNetworkConnectionUtilMVN.DcrUpdateResultMVN dcrUpdateResult = VeevaNetworkConnectionUtilMVN.updateDCRsDB(dcrsToUpdate, VeevaNetworkConnectionUtilMVN.DCR_SUBMITTED);
            dcrUpdateResult = VeevaNetworkConnectionUtilMVN.updateRecordsByDCRs(dcrUpdateResult);

            if(dcrUpdateResult != null && dcrUpdateResult.errorsByDcrId != null && !dcrUpdateResult.errorsByDcrId.keySet().isEmpty()) {
                for(Set<String> dcrErrors : dcrUpdateResult.errorsByDcrId.values()) {
                    errors.addAll(dcrErrors);
                }
            }
        }
    }

    /**
    * FINISH BATCH
    * @description: log any error which wasn't store in a record
    */
    public void finish(Database.BatchableContext BC) {
        if(errors != null && !errors.isEmpty()) {
            System.debug('### Pulling DCRs Errors: \n' + String.join(new List<String>(errors), ',\n'));
        }
    }
}