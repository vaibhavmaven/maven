/**
 * @author Mavens
 * @group Network DCR
 * @description:Trigger handler for update records from DCRs
 **/
public class DCRCompletionTriggerHandlerMVN implements TriggerHandlerMVN.HandlerInterface {

    public List<Id> updateReadyDcrIds;

    public void handle() {
        updateReadyDcrIds = new List<Id>();
        for(Data_Change_Request_MVN__c dataChangeRequest : (List<Data_Change_Request_MVN__c>) Trigger.new) {
            Data_Change_Request_MVN__c oldDataChangeRequest = (Data_Change_Request_MVN__c) Trigger.oldMap.get(dataChangeRequest.Id);
            if(dataChangeRequest.Status_MVN__c == 'Updated' && oldDataChangeRequest.Status_MVN__c != 'Updated') {
                updateReadyDcrIds.add(dataChangeRequest.Id);
            }
        }

        accountAddressUpdateHandler();
        affiliationCreationHandler();
    }

    public void accountAddressUpdateHandler() {
        if(updateReadyDcrIds != null && updateReadyDcrIds.size() > 0) {
            Map<Id, List<Data_Change_Request_Line_MVN__c>> dcrLineMap = new Map<Id, List<Data_Change_Request_Line_MVN__c>>();
            for(Data_Change_Request_Line_MVN__c dcrLine : [SELECT Data_Change_Request_MVN__c, Field_API_Name_MVN__c, Final_Value_MVN__c FROM Data_Change_Request_Line_MVN__c WHERE Data_Change_Request_MVN__c in :updateReadyDcrIds ORDER BY CreatedDate]) {
                List<Data_Change_Request_Line_MVN__c> dcrLines;
                if(dcrLineMap.containsKey(dcrLine.Data_Change_Request_MVN__c)) {
                    dcrLines = dcrLineMap.get(dcrLine.Data_Change_Request_MVN__c);
                } else {
                    dcrLines = new List<Data_Change_Request_Line_MVN__c>();
                }
                dcrLines.add(dcrLine);
                dcrLineMap.put(dcrLine.Data_Change_Request_MVN__c, dcrLines);
            }

            Map<Id, Account> accountsToUpdateMap = new Map<Id, Account>();
            Map<Id, Address_vod__c> addressesToUpdateMap = new Map<Id, Address_vod__c>();
            Map<Id, License_MVN__c> licensesToUpdateMap = new Map<Id, License_MVN__c>();
            if(dcrLineMap != null && dcrLineMap.size() > 0) {
                List<DcrWrapper> dcrWrapperList = new List<DcrWrapper>();
                for(Data_Change_Request_MVN__c dataChangeRequest : (List<Data_Change_Request_MVN__c>) Trigger.new) {
                    dcrWrapperList.add(new DcrWrapper(dataChangeRequest));
                }
                dcrWrapperList.sort();
                for(DcrWrapper dcrWrapper : dcrWrapperList) {
                    Boolean dcrLineFound = false;
                    if(dcrWrapper.dcr.Status_MVN__c == 'Updated' && dcrWrapper.dcr.Account_MVN__c != null) {
                        Account dcrAccount;
                        if(accountsToUpdateMap.containsKey(dcrWrapper.dcr.Account_MVN__c)) {
                            dcrAccount = accountsToUpdateMap.get(dcrWrapper.dcr.Account_MVN__c);
                        } else {
                            dcrAccount = new Account();
                        }
                        for(Data_Change_Request_Line_MVN__c dcrLine : dcrLineMap.get(dcrWrapper.dcr.Id)) {
                            dcrLineFound = true;
                            dcrAccount.Id = dcrWrapper.dcr.Account_MVN__c;
                            dcrAccount.DCR_Override_MVN__c = true;
                            Object newValue = convertToAnyType(Account.sObjectType.getDescribe(), dcrLine.Field_API_Name_MVN__c, dcrLine.Final_Value_MVN__c);
                            dcrAccount.put(dcrLine.Field_API_Name_MVN__c, newValue);
                        }
                        if(dcrLineFound) {
                            dcrAccount.Network_Id_MVN__c = dcrWrapper.dcr.Record_External_ID_MVN__c;
                            accountsToUpdateMap.put(dcrWrapper.dcr.Account_MVN__c, dcrAccount);
                        }
                    } else if(dcrWrapper.dcr.Status_MVN__c == 'Updated' && dcrWrapper.dcr.Address_MVN__c != null) {
                        Address_vod__c dcrAddress;
                        if(addressesToUpdateMap.containsKey(dcrWrapper.dcr.Address_MVN__c)) {
                            dcrAddress = addressesToUpdateMap.get(dcrWrapper.dcr.Address_MVN__c);
                        } else {
                            dcrAddress = new Address_vod__c();
                        }
                        for(Data_Change_Request_Line_MVN__c dcrLine : dcrLineMap.get(dcrWrapper.dcr.Id)) {
                            dcrLineFound = true;
                            dcrAddress.Id = dcrWrapper.dcr.Address_MVN__c;
                            dcrAddress.DCR_Override_MVN__c = true;
                            Object newValue = convertToAnyType(Address_vod__c.sObjectType.getDescribe(), dcrLine.Field_API_Name_MVN__c, dcrLine.Final_Value_MVN__c);
                            dcrAddress.put(dcrLine.Field_API_Name_MVN__c, newValue);
                        }
                        if(dcrLineFound) {
                            dcrAddress.Network_Id_MVN__c = dcrWrapper.dcr.Record_External_ID_MVN__c;
                            addressesToUpdateMap.put(dcrWrapper.dcr.Address_MVN__c, dcrAddress);
                        }
                    } else if (dcrWrapper.dcr.Status_MVN__c == 'Updated' && dcrWrapper.dcr.License_MVN__c != null) {
                        License_MVN__c dcrLicense;
                        if (licensesToUpdateMap.containsKey(dcrWrapper.dcr.License_MVN__c)) {
                            dcrLicense = licensesToUpdateMap.get(dcrWrapper.dcr.License_MVN__c);
                        } else {
                            dcrLicense = new License_MVN__c();
                        }
                        Map<String, Schema.SObjectField> fieldMap = License_MVN__c.sObjectType.getDescribe().fields.getMap();
                        for (Data_Change_Request_Line_MVN__c dcrLine : dcrLineMap.get(dcrWrapper.dcr.Id)) {
                            Schema.DescribeFieldResult fieldSchema = fieldMap.containsKey(dcrLine.Field_API_Name_MVN__c) ?
                                fieldMap.get(dcrLine.Field_API_Name_MVN__c).getDescribe() :
                                null;

                            Boolean isValid = fieldSchema != null && !fieldSchema.isCalculated();

                            if (!isValid) {
                                continue;
                            }

                            dcrLineFound = true;
                            dcrLicense.Id = dcrWrapper.dcr.License_MVN__c;
                            dcrLicense.DCR_Override_MVN__c = true;
                            Object newValue = convertToAnyType(License_MVN__c.sObjectType.getDescribe(), dcrLine.Field_API_Name_MVN__c, dcrLine.Final_Value_MVN__c);
                            dcrLicense.put(dcrLine.Field_API_Name_MVN__c, newValue);
                        }
                        if(dcrLineFound) {
                            dcrLicense.Network_Id_MVN__c = dcrWrapper.dcr.Record_External_ID_MVN__c;
                            licensesToUpdateMap.put(dcrWrapper.dcr.License_MVN__c, dcrLicense);
                        }
                    }
                }
            }

            if(accountsToUpdateMap.size() > 0) {
                update accountsToUpdateMap.values();
            }

            if(addressesToUpdateMap.size() > 0) {
                update addressesToUpdateMap.values();
            }

            if(licensesToUpdateMap.size() > 0) {
                update licensesToUpdateMap.values();
            }
        }
    }

    private void affiliationCreationHandler() {

        Map<Id,Id> dcrChildToParentMap = new Map<Id,Id>();

        if(updateReadyDcrIds != null && updateReadyDcrIds.size() > 0) {
            for(Id dcrId : updateReadyDcrIds) {
                Data_Change_Request_MVN__c dcr = (Data_Change_Request_MVN__c) Trigger.newMap.get(dcrId);
                if((dcr.Parent_Account_MVN__c != null || dcr.Account_MVN__c != null) && dcr.Parent_Data_Change_Request_MVN__c != null) {
                    dcrChildToParentMap.put(dcr.Id, dcr.Parent_Data_Change_Request_MVN__c);
                }
            }

            List<Id> affiliationIds = new List<Id>();
            Map<Id,Id> parentDcrToChildAccountMap = new Map<Id,Id>();
            for(Id dcrId : dcrChildToParentMap.keySet()) {
                if(trigger.newMap.containsKey(dcrChildToParentMap.get(dcrid))) {
                    affiliationIds.add((Id) trigger.newMap.get(dcrChildToParentMap.get(dcrid)).get('Affiliation_MVN__c'));
                } else {
                    parentDcrToChildAccountMap.put(dcrChildToParentMap.get(dcrId),dcrId);
                }
            }

            if(parentDcrToChildAccountMap.size() > 0) {
                for(Data_Change_Request_MVN__c dcr : [SELECT Affiliation_MVN__c FROM Data_Change_Request_MVN__c WHERE Id in :parentDcrToChildAccountMap.keySet() AND Status_MVN__c = 'Updated']) {
                    affiliationIds.add(dcr.Affiliation_MVN__c);
                }
            }

            Map<String, Affiliation_MVN__c> affiliationsToUpsertMap = new Map<String, Affiliation_MVN__c>();
            for(Affiliation_MVN__c affiliation : [SELECT Status_MVN__c FROM Affiliation_MVN__c WHERE Id in :affiliationIds]) {
                affiliation.Status_MVN__c = 'Active';
                affiliation.DCR_Override_MVN__c = true;
                affiliationsToUpsertMap.put(affiliation.Related_Account_Key_MVN__c, affiliation);
            }

            update affiliationsToUpsertMap.values();
        }
    }

    private static Object convertToAnyType(Schema.DescribeSObjectResult currentSObjectType, String fieldName, String fieldValue) {
        Map<String, Schema.SObjectField> sobjFieldMap = currentSObjectType.Fields.getMap();

        Boolean hasField = sobjFieldMap.containsKey(fieldName);
        Boolean isFieldValueNull = fieldValue == null;

        if (!hasField || isFieldValueNull) {
            return fieldValue;
        }

        Schema.SObjectField sobjField = sobjFieldMap.get(fieldName);

        Schema.DisplayType fieldType = sobjField.getDescribe().getType();

        Object returnFieldValue;

        if (fieldType == null) {
            returnFieldValue = fieldValue;
        } else if (fieldType == Schema.DisplayType.Integer) {
            returnFieldValue = Integer.valueOf(fieldValue);
        } else if (fieldType == Schema.DisplayType.Boolean) {
            returnFieldValue = Boolean.valueOf(fieldValue);
        } else if (fieldType == Schema.DisplayType.Currency || fieldType == Schema.DisplayType.Double || fieldType == Schema.DisplayType.Percent) {
            returnFieldValue = Double.valueOf(fieldValue);
        } else if (fieldType == Schema.DisplayType.Date) {
            returnFieldValue = Date.valueOf(String.valueOf(fieldValue));
        } else if (fieldType == Schema.DisplayType.DateTime) {
            returnFieldValue = DateTime.valueOf(String.valueOf(fieldValue));
        } else {
            returnFieldValue = String.valueOf(fieldValue);
        }

        return returnFieldValue;
    }

    class DcrWrapper implements Comparable {
        public Data_Change_Request_MVN__c dcr;

        public dcrWrapper(Data_Change_Request_MVN__c dcr) {
            this.dcr = dcr;
        }

        public Integer compareTo(Object compareTo) {
            // Cast argument to DcrWrapper
            DcrWrapper compareToDcr = (DcrWrapper) compareTo;

            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (dcr.MDM_Datetime_MVN__c > compareToDcr.dcr.MDM_Datetime_MVN__c) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (dcr.MDM_Datetime_MVN__c < compareToDcr.dcr.MDM_Datetime_MVN__c) {
                // Set return value to a negative value.
                returnValue = -1;
            }

            return returnValue;
        }
    }
}