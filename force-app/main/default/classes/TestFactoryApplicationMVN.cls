/**
* @author Mavens
* @date 07/10/2018 
* @description Class to provide factory methods to create test data for Application objects
*/
@isTest
public class TestFactoryApplicationMVN {

    private TestFactorySObjectMVN objectFactory;

    public TestFactoryApplicationMVN() {
        objectFactory = new TestFactorySObjectMVN('Application_MVN__c', new Map<String, Object>());
    }

    public Application_MVN__c construct(Map<String, Object> valuesByField){
        return (Application_MVN__c) objectFactory.constructSObject(valuesByField);
    }

    public Application_MVN__c create(Map<String, Object> valuesByField){
        return (Application_MVN__c) objectFactory.createSObject(valuesByField);
    }

    public List<Application_MVN__c> constructMany(Integer numOfRequests, Map<String,Object> valuesByField) {
        return (List<Application_MVN__c>) objectFactory.constructSObjects(numOfRequests, valuesByField);
    }

    public List<Application_MVN__c> createMany(Integer numOfRequests, Map<String,Object> valuesByField) {
        return (List<Application_MVN__c>) objectFactory.createSObjects(numOfRequests, valuesByField);
    }
}