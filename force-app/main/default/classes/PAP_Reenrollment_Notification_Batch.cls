global class PAP_Reenrollment_Notification_Batch implements Database.Batchable<sObject>, Database.allowsCallouts{ 
	 global Database.QueryLocator start(Database.BatchableContext bc){
          return Database.getQueryLocator([
              SELECT
                Id,
                Program_Member_MVN__c,
                Program_Member_MVN__r.Program_MVN__r.Program_ID_MVN__c,
                Application_Reminder_Date_MVN__c,
             	Program_Member_MVN__r.Name,
                Application_Reminder_Follow_Up_Date_MVN__c
            FROM
                Application_MVN__c
            WHERE
                (
                    Application_Reminder_Date_MVN__c <= TODAY
                    OR
                    Application_Reminder_Follow_Up_Date_MVN__c <= TODAY
                )
            AND
                Status_MVN__c = 'Fulfilled'
            AND
                Is_Application_Active_MVN__c = true
            
           ]);
         
 	}
 	 global void execute(Database.BatchableContext BC, List<Application_MVN__c> applications) {
       Boolean reenrollFlag = true;    
       Boolean buttonFlag = false; 
       String SendPatientResult = '';
       String SendHCPResult = '';  
   	 for (Application_MVN__c applicationObj : applications) {
        	                 
            SendHCPResult	  = PAP_SendNotification.sendNotification_HCP(applicationObj.Program_Member_MVN__c,reenrollFlag,buttonFlag);  
          system.debug('********* SendHCP RESULT  '+SendHCPResult); 	
         SendPatientResult = PAP_SendNotification.sendNotification_Patient(applicationObj.Program_Member_MVN__c,reenrollFlag,buttonFlag);  
        
         //call update
       try{  
           	if(SendHCPResult.contains('BatchSRFaxSuccess')){
                    Program_Member_MVN__c updatepgmMemberObj = [select Id, Is_Prescription_Generated_JKC__c,Document_Generated_Date_JKC__c  from Program_Member_MVN__c where Id= :applicationObj.Program_Member_MVN__c];
                    
                    //Update on 11/11/20 - added IF statement to trigger process builder
                    IF(updatepgmMemberObj.Is_Prescription_Generated_JKC__c = true)
                    {        
                        updatepgmMemberObj.Is_Prescription_Generated_JKC__c = false;
                        update updatepgmMemberObj;  
                    }      
                
                	updatepgmMemberObj.Is_Prescription_Generated_JKC__c = true;
                    updatepgmMemberObj.Document_Generated_Date_JKC__c =system.now();
                    update updatepgmMemberObj; 
                }
            //if((SendPatientResult=='Success') && (SendHCPResult=='Success')){
                Application_MVN__c updateAppObj = [select Id, Application_Reminder_Date_MVN__c,
													Application_Reminder_Follow_Up_Date_MVN__c,
 													Application_Reminder_Sent_MVN__c,
													Application_Reminder_Follow_Up_Sent_MVN__c
  													from Application_MVN__c where Id= :applicationObj.Id];
                if (updateAppObj.Application_Reminder_Date_MVN__c <= Date.today()){
                	updateAppObj.Application_Reminder_Sent_MVN__c = Datetime.now();
                	updateAppObj.Application_Reminder_Date_MVN__c = null;
                    update updateAppObj; 
                }
                if (updateAppObj.Application_Reminder_Follow_Up_Date_MVN__c <= Date.today()) {
                	updateAppObj.Application_Reminder_Follow_Up_Sent_MVN__c = Datetime.now();
               		updateAppObj.Application_Reminder_Follow_Up_Date_MVN__c = null;
                     update updateAppObj; 
                }
           // }
          	
        }catch(Exception ex){             
            catchException(ex, 'ReenrollmentNotification_Batch',400,applicationObj.Program_Member_MVN__r.Name );
        }   	
		}//end for
     }
    
   
/*
*   Method   : catchException
*   Desc     : Method to log details of Exception. 
*   @param   : Exception - The exception object 
*   @param   : String - Name of the method in which the exception occured.       
*   @return  : none
*/    
    private static void catchException(Exception ex , String methodName ,Integer responseCode, String ObjectId ){    
        PAP_ErrorLog.logErrorActivity(ex, methodName , responseCode,  ObjectId);       
    }   
    global void finish(Database.BatchableContext bc) {
        
    }
    
}