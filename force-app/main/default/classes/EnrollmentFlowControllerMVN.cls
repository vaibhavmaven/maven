/**
 *  EnrollmentFlowControllerMVN
 *  Created By:     Aggen
 *  Created On:     11/02/2015
 *  Description:    this class is the controller for the EnrollmentFlowMVN VF page
 *
 **/
public with sharing class EnrollmentFlowControllerMVN {
    public Id selectedAttachmentId { get; set; }
    public List<SelectOption> attachments { get; private set; }

    public Boolean isEnrolling {
        get {
            String param = ApexPages.currentPage().getParameters().get('isEnrollmentFlow');
            if(param != null && param == 'true')
                return true;
            return false;
        }
    }

    public Boolean hasMedicalHistoryTypes {
        get {
            return [SELECT count() FROM Medical_History_Type_MVN__c WHERE Program_MVN__c = :selectedProgramId] > 0;
        }
    }

    public List<SelectOption> programs { get; private set; }
    public Map<Id,Program_MVN__c> programMap;
    public Case thisCase { get; set; }

    public String caseQuery = 'SELECT AccountId, Account.Name, Address_MVN__c, Referred_By_MVN__c, Caller_MVN__c, Caller_MVN__r.Name,'
                                +'Referred_By_MVN__r.Name, Program_MVN__c, Program_Member_MVN__c, No_Physician_MVN__c, RecordType.DeveloperName,Program_Member_MVN__r.Patient_Status_MVN__c, Type, Program_Member_MVN__r.Active_Application_MVN__r.Expiry_Date_MVN__c, Program_MVN__r.Call_Greeting_JKC__c '
                                + 'FROM Case WHERE Id = \'' + String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('caseId')) + '\'';

    public Case viewCase {
        get {
            return Database.query(caseQuery);
        } set;
    }

    public String selectedProgramId {
        get {
            if(viewCase.Program_Member_MVN__c != thisCase.Program_Member_MVN__c) {
                selectedProgramId = viewCase.Program_MVN__c;
                thisCase.Program_Member_MVN__c = viewCase.Program_Member_MVN__c ;
            }
            return selectedProgramId;
        } set;
    }

    public EnrollmentFlowControllerMVN(ApexPages.StandardController stdController) {
        programMap = new Map<Id,Program_MVN__c>();
        thisCase = viewCase;
        selectedProgramId = viewCase.Program_MVN__c;

        // program select options
        programs = new List <SelectOption> {new SelectOption('', Label.Select_Program)};
        for(Program_MVN__c program : [SELECT Id, Name FROM Program_MVN__c WHERE Active_MVN__c = true order by Name ASC]) {
            programs.add(new SelectOption(program.Id, program.Name));
            programMap.put(program.Id, program);
        }

        // attachment select options for the right side panel
        attachments = new List <SelectOption> {new SelectOption('', Label.Select_Attachment)};
        
        //Appended Document name into the picklist.  Updated by Knipper Admin 2/14/19
        for(Document_MVN__c thisDocument : [SELECT Attachment_Id_MVN__c, Title_MVN__c, name FROM Document_MVN__c WHERE Case_MVN__c = :viewCase.Id]) {
            attachments.add(new SelectOption(thisDocument.Attachment_Id_MVN__c, thisDocument.name + ' | ' + thisDocument.Title_MVN__c));
            selectedAttachmentId = thisDocument.Attachment_Id_MVN__c;
        }
    }

    public void updateCase() {
        try {
            Case updateCase = viewCase.clone(true,true,true,true);
            updateCase.Program_MVN__c = (String.isBlank(selectedProgramId)) ? null : selectedProgramId;
            updateCase.No_Physician_MVN__c = thisCase.No_Physician_MVN__c;
            update updateCase;

            selectedProgramId = viewCase.Program_MVN__c;
        } catch(Exception e) {
            ApexPages.addMessages(e);
        }
    }
}