/**
* @author Mavens
* @group Network API
* @description test class for VeevaNetworkSettingsMVN
*/
@isTest
private class VeevaNetworkSettingsTestMVN {

    private static User testUser;

    static {
        testUser = TestDataFactoryMVN.generateCaseManager();
        insert testUser;
    }

    @isTest
    static void itShouldInitializeDefaultValuesCorrectly() {
        VeevaNetworkSettingsMVN vnSettings;

        System.runAs(testUser) {
            TestFactoryCustomMetadataMVN.setMocks();

            Test.startTest();
            vnSettings = new VeevaNetworkSettingsMVN(VeevaNetworkSettingsMVN.DEFAULT_NAME);
            System.assertNotEquals(null, vnSettings.setting);
            Test.stopTest();
        }

        System.assertEquals('v12.0', vnSettings.apiVersion);
        System.assertEquals(100, vnSettings.searchLimit);
        System.assertEquals('Patient Services', vnSettings.sourceName);
        System.assertEquals('PS', vnSettings.systemName);
        System.assertEquals('US', vnSettings.defaultSearchCountry);
        System.assertEquals('Account_MVN', vnSettings.dcrAccount);
        System.assertEquals('Address_MVN', vnSettings.dcrAddress);
        System.assertEquals('Affiliation_MVN', vnSettings.dcrParent);
        System.assertEquals('SUCCESS', vnSettings.successCalloutStatus);
        System.assertEquals('INACTIVE_USER', vnSettings.inactiveUserCalloutStatus);
        System.assertEquals('INVALID_SESSION_ID', vnSettings.invalidSessionIdCalloutStatus);
        System.assertEquals('USER_LOCKED_OUT', vnSettings.userLockedOutCalloutStatus);
        System.assertEquals('INSUFFICIENT_ACCESS', vnSettings.insufficientAccessCalloutStatus);
        System.assertEquals('A', vnSettings.activeCodeStatus);
        System.assertEquals('HCP', vnSettings.hcpType);
        System.assertEquals('HCO', vnSettings.hcoType);
        System.assertEquals('Address', vnSettings.addressType);
        System.assertEquals('entity', vnSettings.entityRecordName);
        System.assertEquals('parent_hcos__v', vnSettings.parentRecordName);
        System.assertEquals('supplementalResults', vnSettings.parentAttrRecordName);
        System.assertEquals('addresses__v', vnSettings.addressRecordName);
        System.assertEquals('vid__v', vnSettings.entityIdFieldName);
        System.assertEquals('parent_hco_vid__v', vnSettings.parentIdFieldName);
        System.assertEquals('address_status__v', vnSettings.addressStatusFieldName);
        System.assertEquals('address_ordinal__v', vnSettings.addressRankFieldName);
        System.assert(!vnSettings.addressRankFilter.isEmpty(), 'Address Rank Filter is not initialized correctly.');
        System.assertEquals(4, vnSettings.addressRankFilter.size());
        System.assertEquals('completed_date', vnSettings.completedDateFieldName);
        System.assertEquals('network_id_mvn__c', vnSettings.networkExternalIdFieldName);
    }
}