/**
 * @author Mavens
 * @date July 2018
 * @description handler class with helper methods to process files:
 *    - convert attachment to file
 *    - delete file
 */
public with sharing class FileHdlrMVN {

    /*************************************************************************************
     * @description Convert the attachment associated to a Document into a File
     * @return ContentVersion the new content version record created
     */ 
    public static ContentVersion convertAttachmentToFile(Document_MVN__c thisDocument) {
        Attachment thisAttachment = [
            SELECT Id, Name, ParentId, Body
            FROM Attachment
            WHERE Id = :thisDocument.Attachment_Id_MVN__c
            LIMIT 1
        ];

        ContentVersion newFileVersion = new ContentVersion(
            VersionData = thisAttachment.Body,
            Title = thisDocument.Title_MVN__c,
            Description = thisDocument.Type_MVN__c,
            PathOnClient = '/' + thisAttachment.Name,
            FirstPublishLocationId = thisAttachment.ParentId
        );
        insert newFileVersion;

        newFileVersion = [
            SELECT Id, ContentDocumentId, Title
            FROM ContentVersion 
            WHERE Id = :newFileVersion.Id
        ];
        return newFileVersion;
    }

}