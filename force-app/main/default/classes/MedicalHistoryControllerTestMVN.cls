/*
*   MedicalHistoryControllerMVN
*   Created By:     Paul Battisson
*   Created Date:   April 26, 2016
*   Modified By:     Florian Hoehn
*   Modified Date:   June 16, 2016
*   Description:    Test class for the MedicalHistoryControllerMVN class.
*/
@isTest private class MedicalHistoryControllerTestMVN {
    @testSetup static void setup() {
        TestDataFactoryMVN.createPSSettings();
        Account member = TestDataFactoryMVN.createMember();
        Program_MVN__c program = TestDataFactoryMVN.createProgram();

        Medical_History_Type_MVN__c type = new Medical_History_Type_MVN__c(
            Record_Type_Name_MVN__c = 'Allergies_MVN',
            Program_MVN__c = program.Id,
            Order_MVN__c = 1
        );
        insert type;

        Program_Member_MVN__c progMember = TestDataFactoryMVN.createProgramMember(program, member);
        RecordType recordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Medical_History_MVN__c' AND DeveloperName = 'Allergies_MVN'];

        Medical_History_MVN__c history = new Medical_History_MVN__c(
            RecordType = recordType,
            RecordTypeId = recordType.Id,
            Program_Member_MVN__c = progMember.Id
        );
        insert history;
    }

    @isTest private static void test_Constructor_InstantiateCorrectly() {
        Program_Member_MVN__c progMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Page.MedicalHistoryMVN'));
        System.currentPageReference().getParameters().put('pmId', progMember.Id);

        Test.startTest();
            MedicalHistoryControllerMVN controller = new MedicalHistoryControllerMVN();
            List<String> actualOrderedRecordTypeNames = controller.orderedRecordTypeNames;
            Map<String, RecordType> actualrecordTypeMap = controller.recordTypeMap;
            String actualselectedRecordTypeDeveloperName = controller.selectedRecordTypeDeveloperName;
        Test.stopTest();

        System.assertNotEquals(null, actualOrderedRecordTypeNames);
        System.assertNotEquals(null, actualrecordTypeMap);
        System.assertNotEquals(null, actualselectedRecordTypeDeveloperName);
    }

    @isTest private static void test_Constructor_InstantiateIncorrectly() {
        Test.setCurrentPageReference(new PageReference('Page.MedicalHistoryMVN'));

        Test.startTest();
            MedicalHistoryControllerMVN controller = new MedicalHistoryControllerMVN();
        Test.stopTest();

        System.assertEquals(true, ApexPages.hasMessages());
        System.assert(ApexPages.getMessages().get(0).getDetail().contains(String.format(Label.MedicalHistory_ContactAdmin, new String[]{''})));
    }

    @isTest private static void test_selectRecordType_Called() {
        Program_Member_MVN__c progMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Page.MedicalHistoryMVN'));
        System.currentPageReference().getParameters().put('pmId', progMember.Id);
        MedicalHistoryControllerMVN controller = new MedicalHistoryControllerMVN();

        Test.startTest();
            PageReference actualPageReference = controller.selectRecordType();
        Test.stopTest();

        System.assertEquals(null, actualPageReference);
    }

    @isTest private static void test_getMedicalHistory_NewItemCreated() {
        Program_Member_MVN__c progMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        RecordType recordType = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Medical_History_MVN__c' AND DeveloperName = 'Allergies_MVN'];
        Medical_History_MVN__c expectedMedicalHistory = new Medical_History_MVN__c(
            RecordType = recordType,
            RecordTypeId = recordType.Id,
            Program_Member_MVN__c = progMember.Id
        );
        Test.setCurrentPageReference(new PageReference('Page.MedicalHistoryMVN'));
        System.currentPageReference().getParameters().put('pmId', progMember.Id);
        MedicalHistoryControllerMVN controller = new MedicalHistoryControllerMVN();

        Test.startTest();
            MedicalHistoryItemMVN actualMedicalHistory = controller.medicalHistory;
        Test.stopTest();

        MedicalHistoryItemTestMVN.assertMedicalHistoryItemMVN(expectedMedicalHistory, actualMedicalHistory);
    }

    @isTest private static void test_getMedicalHistoryAfterSave_NewItemCreated() {
        Program_Member_MVN__c progMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        Medical_History_MVN__c expectedMedicalHistory = [SELECT Id, Program_Member_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Medical_History_MVN__c LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Page.MedicalHistoryMVN'));
        System.currentPageReference().getParameters().put('pmId', progMember.Id);
        MedicalHistoryControllerMVN controller = new MedicalHistoryControllerMVN();
        controller.medicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);

        Test.startTest();
            MedicalHistoryItemMVN actualMedicalHistory = controller.medicalHistory;
        Test.stopTest();

        System.assertEquals(expectedMedicalHistory.RecordTypeId, actualMedicalHistory.item.RecordTypeId);
        System.assertEquals(expectedMedicalHistory.RecordType.DeveloperName, actualMedicalHistory.item.RecordType.DeveloperName);
        System.assertEquals(expectedMedicalHistory.Program_Member_MVN__c, actualMedicalHistory.item.Program_Member_MVN__c);
    }

    @isTest private static void test_getMedicalHistoryList_GetExistingItems() {
        Program_Member_MVN__c progMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        Medical_History_MVN__c expectedMedicalHistory = [SELECT Id, Program_Member_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Medical_History_MVN__c LIMIT 1];
        Test.setCurrentPageReference(new PageReference('Page.MedicalHistoryMVN'));
        System.currentPageReference().getParameters().put('pmId', progMember.Id);
        MedicalHistoryControllerMVN controller = new MedicalHistoryControllerMVN();

        Test.startTest();
            List<MedicalHistoryItemMVN> actualMedicalHistoryList = controller.medicalHistoryList;
        Test.stopTest();

        System.assertEquals(expectedMedicalHistory.RecordTypeId, actualMedicalHistoryList.get(0).item.RecordTypeId);
        System.assertEquals(expectedMedicalHistory.RecordType.DeveloperName, actualMedicalHistoryList.get(0).item.RecordType.DeveloperName);
    }
}