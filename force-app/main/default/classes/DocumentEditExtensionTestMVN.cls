/*
*   DocumentEditExtensionTestMVN
*   Created By:     Florian Hoehn
*   Created Date:   May 16th, 2017
*   Description:    tests DocumentEditExtensionMVN and all its functionality
*/
@isTest private class DocumentEditExtensionTestMVN {
    /**
    * @description setups data for all unit tests
    */
    @testSetup private static void setupData() {
        TestDataFactoryMVN.createPSSettings();
        Program_MVN__c program = TestDataFactoryMVN.createProgram();
        TestDataFactoryMVN.createProgramStage(program);
        Account testAccount = TestDataFactoryMVN.createMember();
        Program_Member_MVN__c programMember = TestDataFactoryMVN.createProgramMember(program, testAccount);
        Case caseRecord = TestDataFactoryMVN.createTestCase();
        caseRecord.Program_Member_MVN__c = programMember.Id;
        update caseRecord;
    }

    /**
    * @description tests the constructor is setting the document and attachment records
    */
    @isTest private static void testConstructorIsInitiatedAndDocumentAndAttachmentAreSet() {
        Case expectedCase = [SELECT Id, Program_Member_MVN__c FROM Case LIMIT 1];
        Document_MVN__c document = new Document_MVN__c(
            Case_MVN__c = expectedCase.Id,
            Type_MVN__c = 'A'
        );
        ApexPages.StandardController stdController = new ApexPages.StandardController(document);

        Test.startTest();
            DocumentEditExtensionMVN actualExtension = new DocumentEditExtensionMVN(stdController);
        Test.stopTest();

        System.assertNotEquals(null, actualExtension.file);
        System.assertEquals(expectedCase.Id, actualExtension.document.Case_MVN__c);
        System.assertEquals(expectedCase.Program_Member_MVN__c, actualExtension.document.Program_Member_MVN__c);
    }

    /**
    * @description tests the constructor is getting the Program_Member_MVN__c field when document already exists
    */
    @isTest private static void testConstructorIsGettingProgramMemberLookupWhenDocumentExists() {
        Case expectedCase = [SELECT Id, Program_Member_MVN__c FROM Case LIMIT 1];
        Document_MVN__c expectedDocument = new Document_MVN__c(
            Program_Member_MVN__c = [SELECT Id FROM Program_Member_MVN__c LIMIT 1].Id,
            Case_MVN__c = expectedCase.Id,
            Title_MVN__c = 'Test_Title',
            Type_MVN__c = 'A'
        );
        insert expectedDocument;
        ApexPages.StandardController stdController = new ApexPages.StandardController(expectedDocument);

        Test.startTest();
            DocumentEditExtensionMVN actualExtension = new DocumentEditExtensionMVN(stdController);
        Test.stopTest();

        System.assertNotEquals(null, actualExtension.file);
        System.assertEquals(expectedDocument.Id, actualExtension.document.Id);
        System.assertEquals(expectedCase.Id, actualExtension.document.Case_MVN__c);
        System.assertEquals(expectedCase.Program_Member_MVN__c, actualExtension.document.Program_Member_MVN__c);
    }

    /**
    * @description tests saving document and attachment
    */
    @isTest private static void testSaveDocumentWithAttachment() {
        Case expectedCase = [SELECT Id, Program_Member_MVN__c FROM Case LIMIT 1];
        Document_MVN__c document = new Document_MVN__c(
            Case_MVN__c = expectedCase.Id,
            Type_MVN__c = 'A'
        );
        Attachment expectedFile = new Attachment(
            Name = 'Test_Name',
            Body = Blob.valueOf('Test_Body')
        );
        ApexPages.StandardController stdController = new ApexPages.StandardController(document);
        DocumentEditExtensionMVN actualExtension = new DocumentEditExtensionMVN(stdController);
        actualExtension.file = expectedFile;

        Test.startTest();
            PageReference savePageReference = actualExtension.save();
        Test.stopTest();

        System.assertEquals(null, savePageReference);
        System.assertEquals(true, actualExtension.isUploadedAndSaved);
        System.assertNotEquals(null, actualExtension.document.Id);
        System.assertEquals(expectedFile.Name, actualExtension.document.Title_MVN__c);
        System.assertEquals(actualExtension.file.Id, actualExtension.document.Attachment_Id_MVN__c);
        System.assertEquals(expectedFile.Name, actualExtension.file.Name);
        System.assertEquals(expectedFile.Body, actualExtension.file.Body);
    }

    /**
    * @description tests saving throws error when no attachment is set
    */
    @isTest private static void testSaveThrowsErrorWhenNoAttachmentIsSet() {
        Case expectedCase = [SELECT Id, Program_Member_MVN__c FROM Case LIMIT 1];
        Document_MVN__c document = new Document_MVN__c(
            Case_MVN__c = expectedCase.Id,
            Type_MVN__c = 'A'
        );
        ApexPages.StandardController stdController = new ApexPages.StandardController(document);
        DocumentEditExtensionMVN actualExtension = new DocumentEditExtensionMVN(stdController);

        Test.startTest();
            PageReference savePageReference = actualExtension.save();
        Test.stopTest();

        System.assertEquals(null, savePageReference);
        System.assertEquals(false, actualExtension.isUploadedAndSaved);
        System.assert(ApexPages.hasMessages());
        for(ApexPages.Message message :  ApexPages.getMessages()) {
            System.assertEquals(System.Label.Error_No_Attachment, message.getSummary());
            System.assertEquals(ApexPages.Severity.ERROR, message.getSeverity());
        }
    }

    /**
    * @description tests saving throws error and rolls DB back
    */
    @isTest private static void testSaveThrowsErrorAndDocumentNotInserted() {
        Case expectedCase = [SELECT Id, Program_Member_MVN__c FROM Case LIMIT 1];
        Document_MVN__c document = new Document_MVN__c(
            Case_MVN__c = expectedCase.Id,
            Type_MVN__c = 'A'
        );
        Attachment expectedFile = new Attachment(
            Name = 'Test_Name',
            Body = Blob.valueOf('Test_Body')
        );
        ApexPages.StandardController stdController = new ApexPages.StandardController(document);
        DocumentEditExtensionMVN actualExtension = new DocumentEditExtensionMVN(stdController);
        actualExtension.file = expectedFile;

        Test.startTest();
            actualExtension.document.Title_MVN__c = '123456789012345678901234567890123456789012345678901234567890' +
                                                    '123456789012345678901234567890123456789012345678901234567890' +
                                                    '123456789012345678901234567890123456789012345678901234567890' +
                                                    '123456789012345678901234567890123456789012345678901234567890' +
                                                    '123456789012345678901234567890123456789012345678901234567890';
            PageReference savePageReference = actualExtension.save();
        Test.stopTest();

        System.assertEquals(null, savePageReference);
        System.assertEquals(false, actualExtension.isUploadedAndSaved);
        System.assert(ApexPages.hasMessages());
        for(ApexPages.Message message :  ApexPages.getMessages()) {
            System.assert(message.getSummary().startsWith('Title: data value too large:'));
            System.assertEquals(ApexPages.Severity.ERROR, message.getSeverity());
        }
        System.assertEquals(new List<Document_MVN__c>(), [SELECT Id FROM Document_MVN__c]);
    }
}