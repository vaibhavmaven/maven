global class SRFaxRequestBatch_PAP implements Database.Batchable<sObject>, Database.allowsCallouts{   
      private List<SRFax_API_ConfigurationSettings__mdt> srfaxConfig = new List<SRFax_API_ConfigurationSettings__mdt>();
    public SRFaxRequestBatch_PAP() {
        
       this.srfaxConfig = getEndpointConfigurations();
        System.debug('Configuration1: '+srfaxConfig);
    }
   global Database.QueryLocator start(Database.BatchableContext bc){
       
       List<SRFax_API_ConfigurationSettings__mdt> config = [select Access_ID_JKC__c, 
                                                                 MasterLabel,
                                                                 Type_JKC__c 
                                                                 from  SRFax_API_ConfigurationSettings__mdt
                                                                 where MasterLabel = 'Status Batch'
                                                                 LIMIT 1];
       return Database.getQueryLocator([
            SELECT
                Id,
                Program_Member_MVN__c,
                Program_Member_MVN__r.Program_MVN__r.Program_ID_MVN__c,
                Program_Member_MVN__r.Physician_MVN__r.Fax,
            	Program_Member_MVN__r.Nintex_HCP_Fax_MVN__c,
                Application_Reminder_Date_MVN__c,
                Application_Reminder_Follow_Up_Date_MVN__c,
        		Name,
                Number_of_Medicare_B_C_records_MVN__c  //Defines # of medicare records.  If 0, no medicare. If > 0 has medicare.
            FROM
                Application_MVN__c
            WHERE
                (
                    Application_Reminder_Date_MVN__c <= TODAY
                    OR
                    Application_Reminder_Follow_Up_Date_MVN__c <= TODAY       
                )
            AND
                Status_MVN__c = 'Fulfilled'
            AND
                Is_Application_Active_MVN__c = true
           AND 
           	  	Number_of_Medicare_B_C_records_MVN__c =0
           AND 
           		Program_Member_MVN__r.Nintex_HCP_Fax_MVN__c <> null
           	
        ]);
              
      }
  global void execute(Database.BatchableContext bc, List<Application_MVN__c> applications){
        String queuedFaxIDs = '';
        String faxPayload;
        System.debug('COnfiguration: '+srfaxConfig);
        System.debug('COnfiguration: '+this.srfaxConfig);
        /*try{
            for(Case caseObj: listOfcases){
                queuedFaxIDs = queuedFaxIDs + caseObj.Queued_Fax_Result_KAMGEN__c + '|' ;             
            }
            queuedFaxIDs = queuedFaxIDs.substringBeforeLast('|');
           // faxPayload = populatePayload(srfaxConfig, queuedFaxIDs);
           // callSRFaxEndpoint(srfaxConfig, faxPayload);
        }catch(CustomException custExcp){
            // In case of failure, we do not update the status of any case 
            catchException(custExcp, 'checkQueuedStatus');           
        }catch(Exception excp){
            // In case of failure, we do not update the status of any case 
            catchException(excp, 'checkQueuedStatus');  

        } */ 
        system.debug('Finished processing checkQueuedStatus method of KAMGEN_SRFax_Scheduler');
        //system.debug('Final case check:' +listOfcases);
        
    }
    global void finish(Database.BatchableContext bc) {
        
    } 
    
      /*
    *   Method   : getEndpointConfigurations
    *   Desc     : Get SRFax configuration details from SRFax_API_ConfigurationSettings__mdt
    *   @param   : Does not except any parameters.
    *   @return  : returns configuration details
    */       
    private static List<SRFax_API_ConfigurationSettings__mdt> getEndpointConfigurations(){
        system.debug('Fetching SRFax configurations details for Get_MuultiFaxStatus API.');
        // get configuration details from SRFax_API_ConfigurationSettings__mdt
        List<SRFax_API_ConfigurationSettings__mdt> srfaxConfig = [select 
                                                               Access_ID_JKC__c, 
                                                               Access_Password_JKC__c, 
                                                               Endpoint_URL_JKC__c, 
                                                               Fax_Action_JKC__c, 
                                                               HTTP_Method_JKC__c
                                                               from 
                                                               SRFax_API_ConfigurationSettings__mdt
                                                               where 
                                                               Type_JKC__c = 'SRFax_Get_MultiFaxStatus'
                                                               LIMIT 1];  
        // validations
        if(srfaxConfig == null || srfaxConfig.size() == 0)
        { 
            throw new CustomException('SRFax GetMultiFax Status endoints not configured correctly in \"Kamgen_ConfigurationSettings\".'); 
        }
        if(String.isBlank(srfaxConfig[0].Access_ID_JKC__c) || String.isBlank(srfaxConfig[0].Access_Password_JKC__c) || 
           String.isBlank(srfaxConfig[0].Endpoint_URL_JKC__c) || String.isBlank(srfaxConfig[0].HTTP_Method_JKC__c) ||
           String.isBlank(srfaxConfig[0].Fax_Action_JKC__c) ){
               throw new CustomException('SRFax GetMultiFax Status configuration fields empty in in \"Kamgen_ConfigurationSettings\".');             
           }
        system.debug('SRFax GetMultiFax Status configuration details fetched for Get_MultiFaxStatus API : ' + srfaxConfig[0]);   
        return srfaxConfig;         
    }
}