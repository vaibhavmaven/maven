/**
 *  EnhancedAccountSearchResultMockMVN
 *  Created By:     Aggen
 *  Created On:     02/04/2016
 *  Description:    This is mock wrapper class used to store search results for the enhanced account search
 *                  
 **/
global with sharing class EnhancedAccountSearchResultMockMVN implements Comparable {    
    public Account resultAcct { get; set; }
    public String resultAcctId { get; set; }
    public String resultAcctExternalId { get; set; }
    public Boolean isExternalDataSource { get; set; }
    public AccountSearchRsltMVN asr { get; set; }

    // address contact info list
    public List<Contact_Information_MVN__c> resultContactInfoAddrList { get; set; }
    public List<SelectOption> contactInfoAddrSelectOptions { get; set; }
    public String selectedContactInfoAddrId { get; set; }

    // non-address contact info list (ie email and phone)
    public List<Contact_Information_MVN__c> resultAdditionalContactInfoList { get; set; }

    // program member information
    public List<SelectOption> programMemberSelectOptions { get; set; }
    public String selectedProgramMemberId { get; set; }
    public Map<String,String> programMemberships { get; set; }

    public static Map<String,String> contactInfoRecordTypeMap {
        get {
            if(contactInfoRecordTypeMap == null) {
                contactInfoRecordTypeMap = new Map<String,String>();
                for(RecordType rt : [select Id, DeveloperName, Name from RecordType where SobjectType = 'Contact_Information_MVN__c']) {
                    contactInfoRecordTypeMap.put(rt.DeveloperName,rt.Id);
                }
            }
            return contactInfoRecordTypeMap;
        } set;
    }    

    public EnhancedAccountSearchResultMockMVN(Account acct, List<Contact_Information_MVN__c> contactInfoList, List<Program_Member_MVN__c> programMemberList, Boolean isExternalDataSrc, AccountSearchRsltMVN asr) {
        this.asr = asr;
        resultAcct = acct;
        resultAcctId = acct.Id;
        resultAcctExternalId = acct.External_Id_vod__c;
        if(String.isBlank(resultAcctExternalId)) {
            resultAcctExternalId = acct.Network_Id_MVN__c;
        }
        if(String.isBlank(resultAcctId)) {
            resultAcctId = resultAcctExternalId;
        }
        isExternalDataSource = isExternalDataSrc;

        resultContactInfoAddrList = new List<Contact_Information_MVN__c>();
        contactInfoAddrSelectOptions = new List<SelectOption>();
        selectedContactInfoAddrId = '';

        resultAdditionalContactInfoList = new List<Contact_Information_MVN__c>();
        
        programMemberSelectOptions = new List<SelectOption>();
        selectedProgramMemberId = '';
        programMemberships = new Map<String,String>();

        // build the resultContactInfoAddrList, contactInfoAddrSelectOptions, and resultAdditionalContactInfoList
        if(contactInfoList != null) {
            Integer i = 0;
            for(Contact_Information_MVN__c ci : contactInfoList) {
                if(String.isBlank(ci.External_ID_MVN__c)) {
                    ci.External_ID_MVN__c = String.valueOf(i);
                }
                if(contactInfoRecordTypeMap.containsKey('Address_MVN') && ci.RecordTypeId == contactInfoRecordTypeMap.get('Address_MVN')) {
                    resultContactInfoAddrList.add(ci);
                    contactInfoAddrSelectOptions.add(new SelectOption(ci.External_ID_MVN__c, ''));

                    // default the primary address contact info
                    if(ci.Primary_MVN__c) {
                        selectedContactInfoAddrId = ci.External_ID_MVN__c;
                    }
                } else {
                    resultAdditionalContactInfoList.add(ci);
                }
                i++;
            }
            // if there is no primary default the first address contact info
            if(selectedContactInfoAddrId == '' && resultContactInfoAddrList.size() > 0) {
                selectedContactInfoAddrId = resultContactInfoAddrList[0].External_ID_MVN__c;
            }
        }

        // build the programMemberSelectOptions
        if(programMemberList != null && programMemberList.size() > 0) {
            programMemberSelectOptions.add(new SelectOption('',Label.Picklist_Select_Option));
            
            for(Program_Member_MVN__c pm : programMemberList) {
                String suffix = ' ('+pm.Name+')';
                if (String.isNotBlank(pm.Active_Application_MVN__r.Status_MVN__c)) {
                    //suffix = ' ('+pm.Active_Application_MVN__r.Status_MVN__c+')';
                    date expiryDate = pm.Active_Application_MVN__r.Expiry_Date_MVN__c;  
                     if (String.isNotBlank(String.valueOf(expiryDate))) {           
                         String formattedDate = expiryDate.format();
                         
                         Date todaysDate = date.today();
                         Integer dateDiff = todaysDate.daysBetween(expiryDate);
                         System.debug(dateDiff);
                         
                         Decimal appReminderDays = 0;
                         if(String.isNotBlank(String.valueof(pm.Program_MVN__r.Application_Reminder_Days_MVN__c)))
                         {                             
                             appReminderDays = pm.Program_MVN__r.Application_Reminder_Days_MVN__c;
                         }
                         
                         if (expiryDate < date.today())
                         {
							suffix = ' ('+pm.Active_Application_MVN__r.Status_MVN__c +') | Expired';    
                         }
                         else if (expiryDate >= date.today() && expiryDate <= date.today() + INTEGER.valueOf(appReminderDays))
                         {
                             suffix = ' ('+pm.Active_Application_MVN__r.Status_MVN__c +') | Expires in '+ dateDiff +' days on '+ formattedDate;                          
                         }
                         else
                         {
                             suffix = ' ('+pm.Active_Application_MVN__r.Status_MVN__c+')';
                         }
                    }
                    else
                    {
                        //if(pm.Patient_Status_MVN__c != 'Opt-In')
                       // {
                         //   suffix = ' ('+pm.Patient_Status_MVN__c+')'; 
                    	//}
                       // else
                       // {
                            suffix = ' ('+pm.Active_Application_MVN__r.Status_MVN__c+')';   
                       // }
                    }                          
                }              
                   
                programMemberSelectOptions.add(new SelectOption(pm.Id, pm.Program_MVN__r.Name + suffix));
                programMemberships.put(pm.Id, pm.Program_MVN__r.Name + suffix);
            }
            // if there is only one Program Member record, default it
            if(programMemberList.size() == 1) {
                selectedProgramMemberId = programMemberList[0].Id;
            }
        }
    }

    /**
     * Comparing objects for ordering
     * @param   Object      Object to compare
     * @return  Integer     Priority
     * @access  global
     */
    global Integer compareTo(Object obj) {
        EnhancedAccountSearchResultMockMVN toCompare = (EnhancedAccountSearchResultMockMVN) (obj);

        Account thisAcc = this.resultAcct;
        Account compAcc = toCompare.resultAcct;
        
        // External Data Source results are always placed after the results coming from Salesforce
        if(this.isExternalDataSource && !toCompare.isExternalDataSource) {
            return 1;
        } else if(!this.isExternalDataSource && toCompare.isExternalDataSource) {
            return -1;
        }

        if(thisAcc.LastName != null && compAcc.LastName != null) {
            if(thisAcc.LastName != compAcc.LastName) {
                if(thisAcc.LastName > compAcc.LastName) {
                    return 1;
                } else {
                    return -1;
                }
            } else if(thisAcc.FirstName != compAcc.FirstName) {
                if(thisAcc.FirstName > compAcc.FirstName) {
                    return 1;
                } else {
                    return -1;
                }
            }
        } else if(thisAcc.LastName == null && compAcc.LastName == null) {
            if(thisAcc.Name != compAcc.Name) {
                if(thisAcc.Name > compAcc.Name) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }

        return 0;
    }
}