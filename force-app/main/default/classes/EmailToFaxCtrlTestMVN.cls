/**
 * @author Mavens
 * @date 08/2018
 * @description Tests for EmailToFaxCtrlMVN
 */ 

@isTest
private class EmailToFaxCtrlTestMVN {

    static Case testCase;
    static User caseManager;
    static Program_MVN__c testProgram;
    static Program_Member_MVN__c testProgramMember;
    static Id testDocId;
    static List<Attachment> attachmentList;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        createProgramMember();
        createTestCase();
        createAttachment();
    }

    static void createProgramMember() {
        System.runAs(caseManager) {
            TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
            testProgram = programFactory.create();
            List<OrgWideEmailAddress> orgWideEmailAddressList = [SELECT Address FROM OrgWideEmailAddress];
            if (!orgWideEmailAddressList.isEmpty()) {
                testProgram.Send_Fax_From_Email_MVN__c = orgWideEmailAddressList.get(0).Address;
                update testProgram;
            }

            TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
            List<Account> patients = accountFactory.createPatients(1);
            Account hcp = accountFactory.createPrescriber(null);

            TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();
            testProgramMember = programMemberFactory.create(testProgram, hcp, patients)[0];
        }
    }

    static void createTestCase() {
        System.runAs(caseManager) {
            TestFactoryCaseMVN caseFactory = new TestFactoryCaseMVN();
            testCase = caseFactory.create(
                new Map<String, Object> {
                    'RecordTypeId' => SObjectType.Case.getRecordTypeInfosByName().get('Request').getRecordTypeId(),
                    'Status' => 'Test',
                    'Subject' => 'My Request',
                    'Program_Member_MVN__c' => testProgramMember.Id,
                    'Program_MVN__c' => testProgram.Id
                }
            );
        }
    }

    static void createAttachment() {
        System.runAs(caseManager) {
            attachmentList = new List<Attachment>();
            attachmentList.add(new Attachment(
                Name = 'Attachment Program Member 1',
                Body = Blob.valueOf('Test Program Member Attachment 1'),
                ParentId = testProgramMember.Id
            ));
            attachmentList.add(new Attachment(
                Name = 'Attachment Program Member 2',
                Body = Blob.valueOf('Test Program Member Attachment 2'),
                ParentId = testProgramMember.Id
            ));
            attachmentList.add(new Attachment(
                Name = 'Attachment Program Member 3',
                Body = Blob.valueOf('Test Program Member Attachment 3'),
                ParentId = testProgramMember.Id
            ));
            insert attachmentList;
        }
    }

    @isTest
    static void testInitializeAttachmentListWithProgramMember() {
        Test.setCurrentPage(Page.EmailToFaxMVN);
        System.CurrentPageReference().getParameters().put('caseId', testCase.Id);

        System.runAs(caseManager) {
            Test.startTest();
            EmailToFaxCtrlMVN controller = new EmailToFaxCtrlMVN();
            Test.stopTest();

            System.assertEquals(3, controller.attachmentOptions.size());
        }
    }

    @isTest
    static void testInitializeAttachmentListNoProgramMember() {

        System.runAs(caseManager) {
            TestFactoryCaseMVN caseFactory = new TestFactoryCaseMVN();
            Case testCase2 = caseFactory.create(
                new Map<String, Object> {
                    'RecordTypeId' => SObjectType.Case.getRecordTypeInfosByName().get('Request').getRecordTypeId(),
                    'Status' => 'Test',
                    'Subject' => 'My Request',
                    'Program_MVN__c' => testProgram.Id
                }
            );
            List<Attachment> attachmentList = new List<Attachment>();
            attachmentList.add(new Attachment(
                Name = 'Attachment Case 1',
                Body = Blob.valueOf('Test Case Attachment 1'),
                ParentId = testCase2.Id
            ));
            attachmentList.add(new Attachment(
                Name = 'Attachment Case 2',
                Body = Blob.valueOf('Test Case Attachment 2'),
                ParentId = testCase2.Id
            ));
            insert attachmentList;

            Test.setCurrentPage(Page.EmailToFaxMVN);
            System.CurrentPageReference().getParameters().put('caseId', testCase2.Id);

            Test.startTest();
            EmailToFaxCtrlMVN controller = new EmailToFaxCtrlMVN();
            Test.stopTest();

            System.assertEquals(2, controller.attachmentOptions.size());
        }
    }

    @isTest
    static void testSendFaxInvalidData() {
        Test.setCurrentPage(Page.EmailToFaxMVN);
        System.CurrentPageReference().getParameters().put('caseId', testCase.Id);

        System.runAs(caseManager) {

            EmailToFaxCtrlMVN controller = new EmailToFaxCtrlMVN();

            Test.startTest();
            controller.sendFax();
            Test.stopTest();

            List<ApexPages.Message> msgList = ApexPages.getMessages();
            System.assertEquals(ApexPages.Severity.ERROR, msgList.get(0).getSeverity());
            System.assertEquals(Label.Fax_Number_Missing_Field_Error_MVN, msgList.get(0).getDetail());
            System.assertEquals(ApexPages.Severity.ERROR, msgList.get(1).getSeverity());
            System.assertEquals(Label.Subject_Missing_Field_Error_MVN, msgList.get(1).getDetail());
            System.assertEquals(ApexPages.Severity.ERROR, msgList.get(2).getSeverity());
            System.assertEquals(Label.Attachment_Missing_Field_Error_MVN, msgList.get(2).getDetail());
        }
    }

    @isTest
    static void testSendFaxInvalidFaxNumberFormat() {
        Test.setCurrentPage(Page.EmailToFaxMVN);
        System.CurrentPageReference().getParameters().put('caseId', testCase.Id);

        System.runAs(caseManager) {

            EmailToFaxCtrlMVN controller = new EmailToFaxCtrlMVN();
            controller.subject = 'Test Subject';
            controller.selectedAttachments = new List<String>{attachmentList.get(0).Id};
            controller.faxNumber = '812-222-333';

            Test.startTest();
            controller.sendFax();
            Test.stopTest();

            List<ApexPages.Message> msgList = ApexPages.getMessages();
            System.assertEquals(ApexPages.Severity.ERROR, msgList.get(0).getSeverity());
            System.assertEquals(Label.Fax_Number_Invalid_Format_Error_MVN, msgList.get(0).getDetail());
        }
    }

    @isTest
    static void testSendFaxSuccess() {
        Test.setCurrentPage(Page.EmailToFaxMVN);
        System.CurrentPageReference().getParameters().put('caseId', testCase.Id);

        List<OrgWideEmailAddress> orgWideEmailAddressList = [SELECT Address FROM OrgWideEmailAddress];
        if (!orgWideEmailAddressList.isEmpty()) {
            System.runAs(caseManager) {

                EmailToFaxCtrlMVN controller = new EmailToFaxCtrlMVN();
                controller.subject = 'Test Subject';
                controller.selectedAttachments = new List<String>{attachmentList.get(0).Id};
                controller.faxNumber = '812222333';

                Test.startTest();
                controller.sendFax();
                Test.stopTest();

                System.assert([SELECT count() FROM EmailMessage WHERE relatedtoid = :testCase.Id] > 0);
                System.assert(controller.showMessage);
            }
        }
    }

    @isTest
    static void testSendFaxErrorEmailConfiguration() {
        Test.setCurrentPage(Page.EmailToFaxMVN);
        System.CurrentPageReference().getParameters().put('caseId', testCase.Id);

        testProgram.Send_Fax_From_Email_MVN__c = null;
        update testProgram;

        System.runAs(caseManager) {

            EmailToFaxCtrlMVN controller = new EmailToFaxCtrlMVN();
            controller.subject = 'Test Subject';
            controller.selectedAttachments = new List<String>{attachmentList.get(0).Id};
            controller.faxNumber = '812222333';

            Test.startTest();
            controller.sendFax();
            Test.stopTest();

            System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages().get(0).getSeverity());
            String label = String.format(System.Label.Send_Fax_Email_Missing_Error_MVN, 
                            new List<String>{testProgram.Name});
            System.assertEquals(label, ApexPages.getMessages().get(0).getDetail());
        }
    }
    
    @isTest
    static void testSendFaxErrorEmailConfiguration2() {
        Test.setCurrentPage(Page.EmailToFaxMVN);
        System.CurrentPageReference().getParameters().put('caseId', testCase.Id);

        testProgram.Send_Fax_From_Email_MVN__c = 'adifferentemail@email.com';
        update testProgram;

        System.runAs(caseManager) {

            EmailToFaxCtrlMVN controller = new EmailToFaxCtrlMVN();
            controller.subject = 'Test Subject';
            controller.selectedAttachments = new List<String>{attachmentList.get(0).Id};
            controller.faxNumber = '812222333';

            Test.startTest();
            controller.sendFax();
            Test.stopTest();

            System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages().get(0).getSeverity());
            System.assertEquals(Label.Email_Configuration_Error_MVN, ApexPages.getMessages().get(0).getDetail());
        }
    }
}