@IsTest
public class PAP_Get_MultiFaxStatusResponse_Test {
    
    // This test method should give 100% coverage
    static testMethod void testParse() {
        String json = '{'+
        '    \"Status\": \"Success\",'+
        '    \"Result\": ['+
        '        {'+
        '            \"FileName\": \"20190503002734_77|499443211\",'+
        '            \"FaxDetailsID\": \"499443211\",'+
        '            \"SentStatus\": \"Failed\",'+
        '            \"DateQueued\": \"May 03/19 03:27 AM\",'+
        '            \"DateSent\": \"May 03/19 03:40 AM\",'+
        '            \"EpochTime\": 1556869202,'+
        '            \"ToFaxNumber\": \"011917126691111\",'+
        '            \"Pages\": \"1\",'+
        '            \"Duration\": \"88\",'+
        '            \"RemoteID\": \"\",'+
        '            \"ErrorCode\": \"No Answer\",'+
        '            \"Size\": \"71558\",'+
        '            \"AccountCode\": \"\"'+
        '        }, '+
        '        {'+
        '            \"FileName\": \"20190503002734_77|499443211\",'+
        '            \"FaxDetailsID\": \"499443211\",'+
        '            \"SentStatus\": \"Failed\",'+
        '            \"DateQueued\": \"May 03/19 03:27 AM\",'+
        '            \"DateSent\": \"May 03/19 03:40 AM\",'+
        '            \"EpochTime\": 1556869202,'+
        '            \"ToFaxNumber\": \"011917126691111\",'+
        '            \"Pages\": \"1\",'+
        '            \"Duration\": \"88\",'+
        '            \"RemoteID\": \"\",'+
        '            \"ErrorCode\": \"No Answer\",'+
        '            \"Size\": \"71558\",'+
        '            \"AccountCode\": \"\"'+
        '        }      '+
        '    ]'+
        '}';
        PAP_Get_MultiFaxStatusResponse r = PAP_Get_MultiFaxStatusResponse.parse(json);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        PAP_Get_MultiFaxStatusResponse objPAP_Get_MultiFaxStatusResponse = new PAP_Get_MultiFaxStatusResponse(System.JSON.createParser(json));
        System.assert(objPAP_Get_MultiFaxStatusResponse != null);
        System.assert(objPAP_Get_MultiFaxStatusResponse.Status == null);
        System.assert(objPAP_Get_MultiFaxStatusResponse.Result == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        PAP_Get_MultiFaxStatusResponse.Result objResult = new PAP_Get_MultiFaxStatusResponse.Result(System.JSON.createParser(json));
        System.assert(objResult != null);
        System.assert(objResult.FileName == null);
        System.assert(objResult.FaxDetailsID == null);
        System.assert(objResult.SentStatus == null);
        System.assert(objResult.DateQueued == null);
        System.assert(objResult.DateSent == null);
        System.assert(objResult.EpochTime == null);
        System.assert(objResult.ToFaxNumber == null);
        System.assert(objResult.Pages == null);
        System.assert(objResult.Duration == null);
        System.assert(objResult.RemoteID == null);
        System.assert(objResult.ErrorCode == null);
        System.assert(objResult.Size == null);
        System.assert(objResult.AccountCode == null);
    }

}