/**
 * DCRFieldSettingsMVN
 * Created By: Rick Locke
 * Created Date: October 2017
 * Description : Accessor class for the Account Fields Settings custom metadata specifically for DCR usage.
 */
public with sharing class DCRFieldSettingsMVN {

    public List<DCR_Field_Setting_MVN__mdt> settings { get; private set; }

    private static List<DCR_Field_Setting_MVN__mdt> testMockList;

    public DCRFieldSettingsMVN() {
        settings = initSettings(null, null);
    }

    public DCRFieldSettingsMVN(String sobjectName) {
        settings = initSettings(sobjectName, null);
    }

    public DCRFieldSettingsMVN(String sobjectName, String recordTypeId) {
        settings = initSettings(sobjectName, recordTypeId);
    }

    public List<DCR_Field_Setting_MVN__mdt> getSettings() {
        return settings;
    }

    private static List<DCR_Field_Setting_MVN__mdt> initSettings(String sobjectName, String recordTypeId) {
        List<DCR_Field_Setting_MVN__mdt> dcrFieldSettings = new List<DCR_Field_Setting_MVN__mdt>();

        String recordTypeDevName = String.IsNotBlank(recordTypeId) ?
                [SELECT DeveloperName FROM RecordType WHERE Id = :recordTypeId].DeveloperName :
                null;

        Map<String, Schema.SObjectField> DCRSettingsFMap_MVN = DCR_Field_Setting_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
        List<String> dcrFields = new List<String>(DCRSettingsFMap_MVN.keySet());

        String queryFields = String.join(dcrFields, ',');
        String queryString = 'SELECT ' + queryFields + ' ' +
                'FROM DCR_Field_Setting_MVN__mdt ' +
                'WHERE Active_MVN__c = true ';

        queryString += String.IsNotBlank(sobjectName) ?
                'AND Object_MVN__c = \'' + sobjectName + '\' ' :
                '';

        queryString += String.IsNotBlank(recordTypeDevName) ?
                'AND (Account_Record_Type_MVN__c = \'' + recordTypeDevName + '\' OR Account_Record_Type_MVN__c = \'All\')' :
                '';

        try {
            dcrFieldSettings = Database.query(queryString);
        } catch(Exception ex) {
            System.debug('#### Account Field Settings are not configured correctly: ' + ex);
        }

        if (testMockList != null) {
            dcrFieldSettings = testMockList;
        }

        return dcrFieldSettings;
    }

    public List<DCR_Field_Setting_MVN__mdt> filterRecordType(String recordTypeId) {
        List<DCR_Field_Setting_MVN__mdt> dcrFiltered = new List<DCR_Field_Setting_MVN__mdt>();

        for (DCR_Field_Setting_MVN__mdt setting : settings) {
            if (setting.Account_Record_Type_MVN__c == recordTypeId) {
                dcrFiltered.add(setting);
            }
        }

        return dcrFiltered;
    }

    public List<DCR_Field_Setting_MVN__mdt> filterRequired() {
        List<DCR_Field_Setting_MVN__mdt> dcrFiltered = new List<DCR_Field_Setting_MVN__mdt>();

        for (DCR_Field_Setting_MVN__mdt setting : settings) {
            if (setting.Required_for_DCR_Transmission_MVN__c) {
                dcrFiltered.add(setting);
            }
        }

        return dcrFiltered;
    }

    @TestVisible
    private static void setMockList(List<DCR_Field_Setting_MVN__mdt> mockList){
        testMockList = mockList;
    }

}