/**
 * TestFactoryAddressMVN
 * @author: Pablo Roldan
 * @created Date: October 2017
 * @description: This class is used to construct Address objects
 */
@isTest
public class TestFactoryAddressMVN{

    private TestFactorySObjectMVN objectFactory;

    public TestFactoryAddressMVN() {
        Map<String, Object> defaultValues = new Map<String, Object>{'Country_vod__c' => 'US'};

        objectFactory= new TestFactorySObjectMVN('Address_vod__c', defaultValues);
    }

    public Address_vod__c createAddress(Account account, Map<String, Object> valuesByField){
        Map<String, Object> creationValues = new Map<String, Object> {
            'Account_vod__c' => account.Id
        };
        
        creationValues.putAll(valuesByField);
        return (Address_vod__c) objectFactory.createSObject(creationValues);
    }

    public Address_vod__c constructAddress(Account account, Map<String, Object> valuesByField){
        Map<String, Object> creationValues = new Map<String, Object> {
            'Account_vod__c' => account.Id
        };

        creationValues.putAll(valuesByField);
        return (Address_vod__c) objectFactory.constructSObject(creationValues);
    }
}