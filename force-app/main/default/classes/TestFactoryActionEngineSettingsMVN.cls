/**
 * @author Mavens
 * @date 08/2018
 * @description Class used to build fake data for custom metadata type for Action Engine Settings
 */
@isTest
public with sharing class TestFactoryActionEngineSettingsMVN {

    public static void setMocks() {
        List<Action_Engine_Setting_MVN__mdt> settings = new List<Action_Engine_Setting_MVN__mdt>();
        Map<String, Object> setting;

        setting = new Map<String, Object>{
            'Action_Name_MVN__c' => 'Test Action 1',
            'Active_MVN__c' => true,
            'Child_Stage_Name_To_Close_MVN__c' => 'Welcome Call',
            'Next_Stage_Name_MVN__c' => 'Benefits Investigation',
            'Program_Id_MVN__c' => 'Test_Program_MVN',
            'DeveloperName' => 'Close_And_Open_Next_Existing_One_MVN',
            'Action_Type_MVN__c' => 'Close_And_Open_Next_Existing_One_MVN'
        };

        settings.add(
            (Action_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Action_Engine_Setting_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Action_Name_MVN__c' => 'Test Action 2',
            'Active_MVN__c' => true,
            'Child_Stage_Name_To_Close_MVN__c' => 'Welcome Call',
            'Next_Stage_Name_MVN__c' => 'Education',
            'Program_Id_MVN__c' => 'Test_Program_MVN',
            'DeveloperName' => 'Close_And_Open_No_Next_Existing_One_MVN',
            'Action_Type_MVN__c' => 'Close_And_Open_No_Next_Existing_One_MVN'
        };

        settings.add(
            (Action_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Action_Engine_Setting_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Action_Name_MVN__c' => 'Test Action 3',
            'Active_MVN__c' => true,
            'Child_Stage_Name_To_Close_MVN__c' => 'Welcome Call',
            'Next_Stage_Name_MVN__c' => 'New Stage',
            'Program_Id_MVN__c' => 'Test_Program_MVN',
            'DeveloperName' => 'Close_And_Open_No_Existing_One_MVN',
            'Action_Type_MVN__c' => 'Close_And_Open_No_Existing_One_MVN'
        };

        settings.add(
            (Action_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Action_Engine_Setting_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Action_Name_MVN__c' => 'Test Action 5',
            'Active_MVN__c' => true,
            'Child_Stage_Name_To_Close_MVN__c' => 'Welcome Call',
            'Next_Stage_Name_MVN__c' => '',
            'Program_Id_MVN__c' => 'Test_Program_MVN',
            'DeveloperName' => 'Just_Close_MVN',
            'Action_Type_MVN__c' => 'Just_Close_MVN'
        };

        settings.add(
            (Action_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Action_Engine_Setting_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Action_Name_MVN__c' => 'Test Action 1',
            'Active_MVN__c' => true,
            'Child_Stage_Name_To_Close_MVN__c' => 'Welcome Call',
            'Next_Stage_Name_MVN__c' => 'Benefits Investigation',
            'Program_Id_MVN__c' => 'Test_Program_MVN',
            'DeveloperName' => ActionEngineMVN.BENEFITACTION,
            'Action_Type_MVN__c' => ActionEngineMVN.BENEFITACTION
        };

        settings.add(
            (Action_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Action_Engine_Setting_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Action_Name_MVN__c' => 'Test Action 1',
            'Active_MVN__c' => true,
            'Child_Stage_Name_To_Close_MVN__c' => 'Welcome Call',
            'Next_Stage_Name_MVN__c' => 'Benefits Investigation',
            'Program_Id_MVN__c' => 'Test_Program_MVN',
            'DeveloperName' => ActionEngineMVN.AUDITACTION,
            'Action_Type_MVN__c' => ActionEngineMVN.AUDITACTION
        };

        settings.add(
            (Action_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Action_Engine_Setting_MVN__mdt', setting)
        );

         setting = new Map<String, Object>{
            'Action_Name_MVN__c' => 'Appeal Action 1',
            'Active_MVN__c' => true,
            'Child_Stage_Name_To_Close_MVN__c' => 'Process Appeal',
            'Next_Stage_Name_MVN__c' => '',
            'Program_Id_MVN__c' => 'Test_Program_MVN',
            'DeveloperName' => ActionEngineMVN.APPEALACTION,
            'Action_Type_MVN__c' => ActionEngineMVN.APPEALACTION
        };

        settings.add(
            (Action_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Action_Engine_Setting_MVN__mdt', setting)
        );

        ActionEngineSettingsMVN.setMocks(settings);
    }

    public static void setMocks(List<Map<String, Object>> fieldValuesList) {
        List<Action_Engine_Setting_MVN__mdt> settings = new List<Action_Engine_Setting_MVN__mdt>();
        for (Map<String, Object> setting : fieldValuesList) {
            settings.add(
                (Action_Engine_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                    'Action_Engine_Setting_MVN__mdt', setting)
            );
        }
        ActionEngineSettingsMVN.setMocks(settings);
    }

}