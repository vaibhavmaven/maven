@isTest
public class PAP_PrimeRx_CreatePatientBatchTest {
    
    static Program_MVN__c program;
    static Program_Member_MVN__c programMember;
    static List<Account> patients;
    static Account hcp;
    static Product_MVN__c product;
    static Application_MVN__c application;
    static Prescription_MVN__c prescription;
    static List<Address_vod__c> addresses;
    static Order_Scheduler_MVN__c orderScheduler;
    static List<Order_MVN__c> orderList;
    static Patient_Service_Settings_MVN__c settings;
    static Attachment attach ;
    static List<Order_Line_Item_MVN__c> OrderLnItemObj;
    static Document_MVN__c document;

    static String initialStatus;

    @testSetup
    static void setup(){
        settings = TestDataFactoryMVN.buildPSSettings();
        insert settings;       
    

        initialStatus = (String) Order_MVN__c.sObjectType.newSObject(null, true).get('Status_MVN__c');
        	//patients = createPatients(20);
            program = createProgram();
            programMember = createProgramMember();
            product = createProductAndProductLineItems();
            application = createApplication();        	
            document = createDocument();
        	attach = createAttachment();
        	document.Attachment_Id_MVN__c = attach.Id;
        	update document;
            prescription = createPrescription();
            addresses = createAddresses();
            orderScheduler = createOrderScheduler();
            orderList = createOrders();
            createOrderLineItems(orderList);
           }
	 public static Map<String, RecordType> accountRecordTypesByName {
        get {
            if (accountRecordTypesByName == null) {
                accountRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
                    accountRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return accountRecordTypesByName;
        }
        private set;
    }
    
    public static List<Account> createPatients(Integer numOfPatients) {
        List<Account> patientsList = new List<Account>();
        // we are creating 20 patients .. so diving these 20 in grousp of M, F, Male, Female.
        List<String> genderList = new List<String>{'Female', 'Male', 'M', 'F'}; 
        Integer counter = 1; 
        for(String genderStr : genderList){
            system.debug('Creating patients for Gender : ' + genderStr); 
            for (Integer i=0; i<5; i++) {
                Account patient= new Account();
                patient.FirstName = 'Test';
                patient.MiddleName = 'testing';
                patient.LastName = 'Patient' + counter;
                patient.Gender_MVN__c = genderStr; 
                patient.Marital_Status_MVN__c = 'Single';                      
                patient.PersonBirthdate = System.today();
                System.debug(patient.LastName);
                patient.RecordTypeId = accountRecordTypesByName.get('Patient_MVN').id;
                counter++;          
                patientsList.add(patient);
            }            
        }
        
        insert patientsList;
        return patientsList;
   	}
    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        Program_MVN__c program = programFactory.create();
        program.Fulfilment_Method_MVN__c = 'Pharmacy';
        return program;
    }

    static Program_Member_MVN__c createProgramMember() {
        TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
        //patients = accountFactory.createPatients(1);  
        patients = createPatients(1);    
        hcp = accountFactory.createPrescriber(null);

        TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();
        Program_Member_MVN__c programMember = programMemberFactory.create(program, hcp, patients)[0];
        return programMember;
    }

    static Product_MVN__c createProductAndProductLineItems() {

        TestFactoryProductMVN productFactory = new TestFactoryProductMVN();
        Product_MVN__c product = productFactory.create(new Map<String, Object>{
            'Program_MVN__c' => program.Id,
            'Name' => 'Horizant',
            'Days_Supply_MVN__c' => 60,
            'Ship_To_MVN__c' => 'Patient',
            'Product_Status_MVN__c' => 'Active'        
        });

        TestFactoryProductLineItemMVN productLineItemFactory = new TestFactoryProductLineItemMVN();
        List<Product_Line_Item_MVN__c> productLineItemList = new List<Product_Line_Item_MVN__c>();
        productLineItemList.add(
            productLineItemFactory.construct(new Map<String, Object>{
                'Product_MVN__c' => product.Id,
                'Name' => 'Bottle of Horizant',
                'NDC_MVN__c' => 'NDC1234',
                'Is_Primary_MVN__c' => true,
                'Packaged_Quantity_MVN__c' => 100
            })
        );
        productLineItemList.add(
            productLineItemFactory.construct(new Map<String, Object>{
                'Product_MVN__c' => product.Id,
                'Name' => 'Syringe',
                'NDC_MVN__c' => 'NDC5678',
                'Is_Primary_MVN__c' => false,
                'Packaged_Quantity_MVN__c' => 1
            })
        );
        insert productLineItemList;
        return product;
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id,
            'Status_MVN__c' => 'Passed',
            'Enrollment_Date_MVN__c' => System.today(),
            'Expiry_Date_MVN__c' => System.today()+30,
            'Known_Allergies_MVN__c'=>'PENECILIN'
        });
        return testApplication;
    }

      static Attachment createAttachment(){
       
            Attachment attach = new Attachment();              
            attach.Name = 'TestAttachment.pdf' ; 
            attach.Body =  Blob.valueOf('Test PDF Contents'); 
        	attach.ParentId = document.Id;
          	insert attach;
        	return attach;
    }
     static Document_MVN__c createDocument()
    {	        
        Document_MVN__c document = new Document_MVN__c (
            Title_MVN__c = 'Test Document',
            Type_MVN__c = 'Prescription',
            Program_Member_MVN__c = programMember.Id          
            );  
        insert document;
        return document;      
    }
    
    static Prescription_MVN__c createPrescription() {
        TestFactoryPrescriptionMVN prescriptionFactory = new TestFactoryPrescriptionMVN();       
        Prescription_MVN__c testPrescription = prescriptionFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id,
            'Application_MVN__c' => application.Id,
            'Signal_Code_MVN__c' => 'QD - Once a day',
            'Signal_Code_Quantity_MVN__c' => 4,
            'Refills_MVN__c' => 1,
            'Timeframe_MVN__c' => '60',
            'Product_MVN__c' => product.Id,
            'Prescription_doc_JKC__c' => document.Id
        });
        return testPrescription;
    }
    
    static List<Address_vod__c> createAddresses() {
        TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();
        List<Address_vod__c> addressTestList = new List<Address_vod__c>();
        addressTestList.add(
            addressFactory.constructAddress(patients[0], new Map<String,Object>{
                'Name' => 'Patient Address Line 1',
                'Address_line_2_vod__c' => 'Patient Address Line 2',
                'Shipping_vod__c' => true,
                'Primary_vod__c' => true,
                'City_vod__c' => 'Patient City',
                'Zip_vod__c' => 'Patient 11111',
                'State_vod__c' => 'IL'
            })
        );
        addressTestList.add(
            addressFactory.constructAddress(hcp, new Map<String,Object>{
                'Name' => 'HCP Address Line 1',
                'Address_line_2_vod__c' => 'HCP Address Line 2',
                'Shipping_vod__c' => true,
                'City_vod__c' => 'HCP City',
                'Zip_vod__c' => 'HCP 11111',
                'State_vod__c' => 'TX'
            })
        );
        insert addressTestList;
        return addressTestList;
    }

    static Order_Scheduler_MVN__c createOrderScheduler() {

        TestFactoryOrderSchedulerMVN orderSchedulerFactory = new TestFactoryOrderSchedulerMVN();
        Order_Scheduler_MVN__c orderSchedulerTest = orderSchedulerFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id,
            'Application_MVN__c' => application.Id,
            'Shipping_Address_MVN__c' => addresses.get(0).Id,
            'Prescription_MVN__c' => prescription.Id,
            'Order_Start_Date_MVN__c' => Date.today()
        });
        return orderSchedulerTest;
    }

    static List<Order_MVN__c> createOrders(){
        TestFactoryOrderMVN orderFactory = new TestFactoryOrderMVN();
        List<Order_MVN__c> orderListTest = orderFactory.createMany(1, new Map<String, Object>{
            'Order_Scheduler_MVN__c' => orderScheduler.Id,
            'Application_MVN__c' => application.Id,
            'Program_Member_MVN__c' => orderScheduler.Program_Member_MVN__c,
            'Order_Date_MVN__c' => Date.today(),
            'Status_MVN__c' => initialStatus
        });     
        return orderListTest;
    }
    
     static void createOrderLineItems(List<Order_MVN__c> orderListTest){
        TestFactoryOrderLineItemMVN orderLineItemFactory = new TestFactoryOrderLineItemMVN();
        List<Order_Line_Item_MVN__c> orderLineItemListTest = new List<Order_Line_Item_MVN__c>();
        for (Order_MVN__c order : orderListTest) {
            orderLineItemListTest.add(
                orderLineItemFactory.construct(new Map<String, Object>{
                    'Order_MVN__c' => order.Id,
                    'Status_MVN__c' => initialStatus,
                    'Tracking_Number_MVN__c' => '1112223333',
                    'NDC_MVN__c' => 'NDC1234',
                    'Known_Allergies_MVN__c' =>'PENECILIN' 
                })
            );         
        }
        insert orderLineItemListTest;
    }
    
    @isTest
    public static void testBatchClassPatient(){
        Test.setMock(HttpCalloutMock.class, new PrimeRxHttpCalloutMock_CreateToken());
        Test.setMock(HttpCalloutMock.class, new PrimeRxHttpCalloutMock_CreatePatient());
        Test.setMock(HttpCalloutMock.class, new PrimeRxHttpCalloutMock_CreatePrescription());
        Test.setMock(HttpCalloutMock.class, new PrimeRxHttpCalloutMock_CreateInsurance());
        Test.setMock(HttpCalloutMock.class, new PrimeRxHttpCalloutMock_CreateAllergies());
        test.startTest();
        PAP_PrimeRx_CreatePatientBatch batch=new PAP_PrimeRx_CreatePatientBatch();
        Database.executeBatch(batch);
        test.stopTest();       
    }
   
     @isTest
    public static void testBatchClassPatientFailed(){
        Test.setMock(HttpCalloutMock.class, new MockClassPrimeRxFail());       
        test.startTest();
        PAP_PrimeRx_CreatePatientBatch batch=new PAP_PrimeRx_CreatePatientBatch();
        Database.executeBatch(batch);
        test.stopTest();       
    }
    private class PrimeRxHttpCalloutMock_CreateToken implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest httpReq)
        {
            HttpResponse httpRes = new HttpResponse(); 
            RestContext.response = new RestResponse();
            httpRes.setHeader('Content-Type', 'application/json'); 
            httpRes.setStatusCode(200);
            httpRes.setStatus('Success');
            Map<String, Object> mapmap=new Map<String, Object>();
            mapmap.put('errorDescription','Success-Token generated successfully.');
            mapmap.put('errorCode',0);           
            mapmap.put('errorMessage','Test error message');
            mapmap.put('token','tekjkjhdjhddsfdsf');
            httpRes.setBody(JSON.serialize(mapmap));
            return httpRes; 
        }
    }  
    
     private class PrimeRxHttpCalloutMock_CreatePatient implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest httpReq)
        {
            HttpResponse httpRes = new HttpResponse(); 
            RestContext.response = new RestResponse();
            httpRes.setHeader('Content-Type', 'application/json'); 
            httpRes.setStatusCode(200);
            httpRes.setStatus('Success');
            Map<String, Object> mapmap=new Map<String, Object>();
            Map<String, Object> patientMap =new Map<String, Object> ();                    
            patientMap.put('PatientNo','269476');
            patientMap.put('LastName','Test');
            patientMap.put('FirstName','Patient1');
            patientMap.put('Gender','F');
            mapmap.put('ClientId','AD46B134C687');
            mapmap.put('ClientSecret','3F284E4D-32F8-4AA0-9E33-755A10EFA8D2');
            mapmap.put('MemberId','KnipperRx');
            mapmap.put('errorDescription','Success-Token generated successfully.');
            mapmap.put('errorCode',0);
            mapmap.put('PatientNo','213456');
            mapmap.put('ErrorMessage','Test error message');
            mapmap.put('ErrorCode',0);
            mapmap.put('token','tekjkjhdjhddsfdsf');
            mapmap.put('Patient',patientMap);
            httpRes.setBody(JSON.serialize(mapmap));
            system.debug('patientmap'+mapmap);
           
            return httpRes; 
        }
    }  
    
    private class PrimeRxHttpCalloutMock_CreatePrescription implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest httpReq)
        {
            HttpResponse httpRes = new HttpResponse(); 
            RestContext.response = new RestResponse();
            httpRes.setHeader('Content-Type', 'application/json'); 
            httpRes.setStatusCode(200);
            httpRes.setStatus('Success');
            Map<String, Object> mapmap=new Map<String, Object>();
            mapmap.put('ClientId','AD46B134C687');
            mapmap.put('ClientSecret','3F284E4D-32F8-4AA0-9E33-755A10EFA8D2');
            mapmap.put('MemberId','KnipperRx');
            mapmap.put('IdentifierType','2');
            mapmap.put('Identifier','5164083999');
            mapmap.put('DocumentName','AARON_BOGLE_OLI-04385');
            mapmap.put('DocumentExtension','.pdf');
            mapmap.put('PatientNo','213456');
            mapmap.put('ExternalReferenceNo','01234');
            mapmap.put('ExternalReferenceType','2');
            mapmap.put('DateSent', Datetime.now());
            mapmap.put('Timestamp', Datetime.now());
           // mapmap.put('Base64EncodedDocument',[select id , body , name from attachment limit 1]);
            mapmap.put('errorDescription','Success-Token generated successfully.');
            mapmap.put('errorCode',0);
            mapmap.put('ErrorMessage','Test error message');
            mapmap.put('ErrorCode',0);
            mapmap.put('PatientNo','213456');
            mapmap.put('errorMessage','Test error message');
            mapmap.put('token','tekjkjhdjhddsfdsf');
            httpRes.setBody(JSON.serialize(mapmap));
            return httpRes; 
        }
    }
    
    private class PrimeRxHttpCalloutMock_CreateInsurance implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest httpReq)
        {
            HttpResponse httpRes = new HttpResponse(); 
            RestContext.response = new RestResponse();
            httpRes.setHeader('Content-Type', 'application/json'); 
            httpRes.setStatusCode(200);
            httpRes.setStatus('Success');
            Map<String, Object> mapmap=new Map<String, Object>();
            mapmap.put('ClientId','AD46B134C687');
            mapmap.put('ClientSecret','3F284E4D-32F8-4AA0-9E33-755A10EFA8D2');
            mapmap.put('MemberId','KnipperRx');
            mapmap.put('IdentifierType','2');
            mapmap.put('Identifier','5164083999');
            mapmap.put('DocumentName','AARON_BOGLE_OLI-04385');
            mapmap.put('DocumentExtension','.pdf');
            mapmap.put('PatientNo','213456');
            mapmap.put('GroupNo','01234');
            mapmap.put('ExpiryDate',Datetime.now()+8);
            mapmap.put('InsuranceBillingSequence','1' );
            mapmap.put('InsuranceName','Bausch PAP');
            mapmap.put('InsuranceBIN','999999');
            mapmap.put('InsurancePCN','9999');
            mapmap.put('CardEffectiveDate',Datetime.now()-10);
            mapmap.put('errorDescription','Success-Token generated successfully.');
            mapmap.put('errorCode',0);
            mapmap.put('PatientNo','213456');
            mapmap.put('errorMessage','Test error message');
            mapmap.put('token','tekjkjhdjhddsfdsf');
            httpRes.setBody(JSON.serialize(mapmap));
            return httpRes; 
        }
    }
    
    private class PrimeRxHttpCalloutMock_CreateAllergies implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest httpReq)
        {
            HttpResponse httpRes = new HttpResponse(); 
            RestContext.response = new RestResponse();
            httpRes.setHeader('Content-Type', 'application/json'); 
            httpRes.setStatusCode(200);
            httpRes.setStatus('Success');
            Map<String, Object> mapmap=new Map<String, Object>();           
            mapmap.put('ClientId','AD46B134C687');
            mapmap.put('ClientSecret','3F284E4D-32F8-4AA0-9E33-755A10EFA8D2');
            mapmap.put('MemberId','KnipperRx');
            mapmap.put('IdentifierType','2');
            mapmap.put('Identifier','5164083999');
            mapmap.put('ActionType','1');
            mapmap.put('Allergies','PENECILINE');
            mapmap.put('PatientNo',213456);
            mapmap.put('GroupNo','01234');
            mapmap.put('errorDescription','Success-Token generated successfully.');
            mapmap.put('errorCode',0);            
            mapmap.put('ErrorMessage','Test error message');
            mapmap.put('ErrorCode',0);
            mapmap.put('token','tekjkjhdjhddsfdsf');
            Map<String, Object> patientMap =new Map<String, Object> ();                    
            patientMap.put('PatientNo',213456);
            patientMap.put('LastName','Test');
            patientMap.put('FirstName','Patient1');
            patientMap.put('Gender','F');
            mapmap.put('Patient',patientMap);
            httpRes.setBody(JSON.serialize(mapmap));
            system.debug('patientmap'+mapmap);           
            return httpRes;  
        }
    }
    
    private class MockClassPrimeRxFail implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest httpReq)
        {
            HttpResponse httpRes = new HttpResponse(); 
            RestContext.response = new RestResponse();
            httpRes.setHeader('Content-Type', 'application/json'); 
            httpRes.setStatusCode(401);
            httpRes.setStatus('Failed');
            Map<String, Object> mapmap=new Map<String, Object>();
            mapmap.put('ClientId','AD46B134C687');
            mapmap.put('ClientSecret','3F284E4D-32F8-4AA0-9E33-755A10EFA8D2');
            mapmap.put('errorDescription','Authentication Failed');
            mapmap.put('errorCode',401);
            httpRes.setBody(JSON.serialize(mapmap));
            return httpRes; 
        }
    }
}