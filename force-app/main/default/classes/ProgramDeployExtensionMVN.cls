/**
*   ProgramDeployExtensionMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 21st, 2017
*   Description:    extension for ProgramDeployMVN page
*                   - upload JSON
*                   - parse and deploy program and related records
*/
public with sharing class ProgramDeployExtensionMVN {
    /**
    * @description program JSON upload body
    */
    public Blob programBlob { get; set; }

    /**
    * @description program JSON upload file name
    */
    public String programFileName { get; set; }

    /**
    * @description constructor
    * @param ApexPages.StandardSetController stdSetController
    */
    public ProgramDeployExtensionMVN(ApexPages.StandardSetController stdSetController) {}

    /**
    * @description parse and deploy the program with all related records
    * @return PageReference pageDetailProgram
    */
    public PageReference deploy() {
        try {
            ProgramDeployMVN programDeploy = new ProgramDeployMVN(new ProgramDeployWrapperMVN().parse(this.programBlob.toString()));
            programDeploy.deploy();
            return programDeploy.view();
        } catch(Exception ex) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage())
            );
            return null;
        }
    }
}