/**
 * @author Mavens
 * @date December 2018
 * @description Tests for OrderSetAppExpiryDateTriggerMVN
 */
@isTest
private class OrderSetAppExpiryDateTriggerTestMVN
{

    static User caseManager;
    static Program_MVN__c program;
    static Program_Member_MVN__c programMember;
    static List<Account> patients;
    static Account hcp;
    static Product_MVN__c product;
    static List<Application_MVN__c> applications;
    static List<Prescription_MVN__c> prescriptions;
    static List<Address_vod__c> addresses;
    static Patient_Service_Settings_MVN__c settings;

    static String initialStatus;
    static final Integer DATA_SIZE = 200;

    static {
        settings = TestDataFactoryMVN.buildPSSettings();
        insert settings;

        initialStatus = (String) Order_MVN__c.sObjectType.newSObject(null, true).get('Status_MVN__c');

        caseManager = TestDataFactoryMVN.generateCaseManager();
        System.runAs(caseManager) {
            program = createProgram();
            programMember = createProgramMember();
            product = createProductAndProductLineItems();
            applications = createApplications();
            prescriptions = createPrescriptions();
            addresses = createAddresses();
        }
    }

    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        Program_MVN__c program = programFactory.create(
            new Map<String, Object> {
                'Acute_Application_Expiry_MVN__c' => 3
            }
        );
        program.Fulfilment_Method_MVN__c = 'Pharmacy';
        return program;
    }

    static Program_Member_MVN__c createProgramMember() {
        TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
        patients = accountFactory.createPatients(1);
        hcp = accountFactory.createPrescriber(null);

        TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();
        Program_Member_MVN__c programMember = programMemberFactory.create(program, hcp, patients)[0];
        return programMember;
    }

    static Product_MVN__c createProductAndProductLineItems() {

        TestFactoryProductMVN productFactory = new TestFactoryProductMVN();
        Product_MVN__c product = productFactory.create(new Map<String, Object>{
            'Program_MVN__c' => program.Id,
            'Name' => 'Horizant',
            'Ship_To_MVN__c' => 'Patient',
            'Product_Status_MVN__c' => 'Active'
        });

        TestFactoryProductLineItemMVN productLineItemFactory = new TestFactoryProductLineItemMVN();
        List<Product_Line_Item_MVN__c> productLineItemList = new List<Product_Line_Item_MVN__c>();
        productLineItemList.add(
            productLineItemFactory.construct(new Map<String, Object>{
                'Product_MVN__c' => product.Id,
                'Name' => 'Bottle of Horizant',
                'NDC_MVN__c' => 'NDC1234',
                'Is_Primary_MVN__c' => true,
                'Packaged_Quantity_MVN__c' => 100
            })
        );
        productLineItemList.add(
            productLineItemFactory.construct(new Map<String, Object>{
                'Product_MVN__c' => product.Id,
                'Name' => 'Syringe',
                'NDC_MVN__c' => 'NDC5678',
                'Is_Primary_MVN__c' => false,
                'Packaged_Quantity_MVN__c' => 1
            })
        );
        insert productLineItemList;

        return product;
    }

    static List<Application_MVN__c> createApplications() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        List<Application_MVN__c> applications = new List<Application_MVN__c>();
        for (Integer i = 0; i < DATA_SIZE; i++) {
            applications.add(
                applicationFactory.construct(
                    new Map<String, Object>{
                        'Program_Member_MVN__c' => programMember.Id,
                        'Status_MVN__c' => 'Passed',
                        'Condition_MVN__c' => 'Acute'
                    }
                )
            );
        }
        insert applications;
        return applications;
    }

    static List<Prescription_MVN__c> createPrescriptions() {
        TestFactoryPrescriptionMVN prescriptionFactory = new TestFactoryPrescriptionMVN();
        List<Prescription_MVN__c> testPrescriptions = new List<Prescription_MVN__c>();
        for (Application_MVN__c application : applications) {
            testPrescriptions.add(
                prescriptionFactory.construct(new Map<String, Object>{
                    'Program_Member_MVN__c' => programMember.Id,
                    'Application_MVN__c' => application.Id,
                    'Signal_Code_MVN__c' => 'QD - Once a day',
                    'Signal_Code_Quantity_MVN__c' => 4,
                    'Refills_MVN__c' => 1,
                    'Timeframe_MVN__c' => '60',
                    'Product_MVN__c' => product.Id,
                    'Prescription_doc_JKC__c' => createDocument(programMember.Id)
                })
            );
        }
        insert testPrescriptions;
        return testPrescriptions;
    }
    
    static Id createDocument(Id programMemberId)
    {
        Document_MVN__c document = new Document_MVN__c (
            Title_MVN__c = 'Test Document',
            Type_MVN__c = 'Eligibility',
            Program_Member_MVN__c = programMemberId
            );
            insert document;
            return document.id;
    }

    static List<Address_vod__c> createAddresses() {
        TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();
        List<Address_vod__c> addressTestList = new List<Address_vod__c>();
        addressTestList.add(
            addressFactory.constructAddress(patients[0], new Map<String,Object>{
                'Name' => 'Patient Address Line 1',
                'Address_line_2_vod__c' => 'Patient Address Line 2',
                'Shipping_vod__c' => true,
                'Primary_vod__c' => true,
                'City_vod__c' => 'Patient City',
                'Zip_vod__c' => 'Patient 11111',
                'State_vod__c' => 'IL'
            })
        );
        addressTestList.add(
            addressFactory.constructAddress(hcp, new Map<String,Object>{
                'Name' => 'HCP Address Line 1',
                'Address_line_2_vod__c' => 'HCP Address Line 2',
                'Shipping_vod__c' => true,
                'City_vod__c' => 'HCP City',
                'Zip_vod__c' => 'HCP 11111',
                'State_vod__c' => 'TX'
            })
        );
        insert addressTestList;
        return addressTestList;
    }

    static List<Order_Scheduler_MVN__c> createOrderSchedulers() {

        TestFactoryOrderSchedulerMVN orderSchedulerFactory = new TestFactoryOrderSchedulerMVN();
        List<Order_Scheduler_MVN__c> orderSchedulerTests = new List<Order_Scheduler_MVN__c>();
        for (Integer i = 0; i < DATA_SIZE; i++) {
            orderSchedulerTests.add(
                orderSchedulerFactory.construct(
                    new Map<String, Object>{
                        'Program_Member_MVN__c' => programMember.Id,
                        'Application_MVN__c' => applications[i].Id,
                        'Shipping_Address_MVN__c' => addresses.get(0).Id,
                        'Prescription_MVN__c' => prescriptions[i].Id,
                        'Order_Start_Date_MVN__c' => Date.today()
                    }
                )
            );
        }
        insert orderSchedulerTests;
        return orderSchedulerTests;
    }

  /*  @isTest
    static void itShouldUpdateExpiryDateOnApplicationWhenOrderCreated()
    {
        System.runAs(caseManager) {
            System.assertEquals(200, [
                SELECT
                    Count()
                FROM
                    Application_MVN__c
                WHERE
                    Expiry_Date_MVN__c = null
            ]);

            Test.startTest();
            createOrderSchedulers();
            Test.stopTest();

            System.assertEquals(200, [
                SELECT
                    Count()
                FROM
                    Application_MVN__c
                WHERE
                    Expiry_Date_MVN__c = :Date.today().addMonths(3)
            ]);
        }
    }*/

    /*@isTest
    static void itShouldUpdateExpiryDateOnApplicationWhenOrderDeleted()
    {
        System.runAs(caseManager) {
            Test.startTest();
            createOrderSchedulers();

            System.assertEquals(200, [
                SELECT
                    Count()
                FROM
                    Application_MVN__c
                WHERE
                    Expiry_Date_MVN__c = :Date.today().addMonths(3)
            ]);

            
            delete [SELECT Id FROM Order_Scheduler_MVN__c];
            Test.stopTest();

            System.assertEquals(200, [
                SELECT
                    Count()
                FROM
                    Application_MVN__c
                WHERE
                    Expiry_Date_MVN__c = null
            ]);
        }
    }*/
}