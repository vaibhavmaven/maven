@isTest
public class ViewNDCDetailsTest {
    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
    static List<Program_Member_MVN__c> programMembers;
    static List<Account> patients;
    static Program_MVN__c program;
    static Prescription_MVN__c prescription;
    static Benefits_Coverage_MVN__c benefitCoverage;
    static Product_MVN__c productFamily;
    static Product_Line_Item_MVN__c product;
    
    public static Map<String, RecordType> accountRecordTypesByName {
        get {
            if (accountRecordTypesByName == null) {
                accountRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
                    accountRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return accountRecordTypesByName;
        }
        private set;
    }
    
    public static List<Account> createPatients(Integer numOfPatients) {
        List<Account> patientsList = new List<Account>();
        
        for (Integer i=0; i<numOfPatients; i++) {
            Account patient= new Account();
            patient.FirstName = 'Test';
            patient.LastName = 'Patient' + i;
            System.debug(patient.LastName);
            patient.RecordTypeId = accountRecordTypesByName.get('Patient_MVN').id;
            patientsList.add(patient);
        }
        
        insert patientsList;
        return patientsList;
   		 }
    
    @testSetup
    static void setup(){
              
        patients = createPatients(20);    
        program = new Program_MVN__c();
        insert program;
        Program_Member_MVN__c programMember = new Program_Member_MVN__c();
        programMember.Program_MVN__c = program.id;
        programMember.Member_MVN__c = patients[0].id;
        programMember.Do_Not_Initialize_MVN__c = false;
        insert programMember;
        
        programMembers = new List<Program_Member_MVN__c>();
        programMembers.add(programMember);
        
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        List<Application_MVN__c> apps = new List<Application_MVN__c>();
        
        for (Integer i=0; i < 10; i++) {
            apps.add(
                applicationFactory.construct(
                    new Map<String, Object>{
                        'Program_Member_MVN__c' => programMembers[0].Id
                            }
                )
            );
        }
        insert apps;
        
        benefitCoverage= new  Benefits_Coverage_MVN__c();
        benefitCoverage.Program_Member_MVN__c = programMembers[0].Id;
        benefitCoverage.Application_MVN__c = apps[0].Id;
        insert benefitCoverage;
        productFamily = new Product_MVN__c();
        productFamily.Program_MVN__c = program.Id;
        productFamily.Product_Status_MVN__c = 'Active';
        insert productFamily;
        product = new Product_Line_Item_MVN__c();
        product.Product_MVN__c = productFamily.Id;
        product.Packaged_Quantity_MVN__c = 5;
        product.NDC_MVN__c = '123';
        insert product;
        prescription = new Prescription_MVN__c();
        prescription.Application_MVN__c = apps[0].Id;
        prescription.Program_Member_MVN__c = programMember.Id;
        prescription.Product_MVN__c =productFamily.Id;
        insert prescription;
        
        RTBI_Response_Master__c rtbiResponseMaster = new RTBI_Response_Master__c(); 
        rtbiResponseMaster.NDC__c = product.NDC_MVN__c;
        rtbiResponseMaster.Benefits_Coverage__c = benefitCoverage.Id;
        insert rtbiResponseMaster; 

        Eligibility_Response_Master__c elgbRespMaster = new Eligibility_Response_Master__c();
        elgbRespMaster.Benefits_Coverage__c = benefitCoverage.Id;
        elgbRespMaster.NDC__c = product.NDC_MVN__c;
        elgbRespMaster.Status__c = 'test status';
        elgbRespMaster.Status_Message__c = 'test status message';
        insert elgbRespMaster;
        
        FAS_Response_Master__c master= new FAS_Response_Master__c();
        master.Benefits_Coverage__c = benefitCoverage.Id;
        insert master;                     
    }
	
    @isTest
    public static void viewNDCDetailTestMethod1(){       
        Product_Line_Item_MVN__c prodObj = [select NDC_MVN__c from Product_Line_Item_MVN__c LIMIT 1];
        ViewNDCDetails.testNDCID = prodObj.NDC_MVN__c;         
        Test.startTest();
        ViewNDCDetails obj = new ViewNDCDetails(new ApexPages.StandardController([select id from Benefits_Coverage_MVN__c LIMIT 1] ));       
        obj.initializePage(); 
        Test.stopTest();        	
    }
    
   
    @isTest
    public static void viewNDCDetailTestMethod2(){    
        ViewNDCDetails.testNDCID = null ; 
        Test.startTest();
        ViewNDCDetails obj = new ViewNDCDetails(new ApexPages.StandardController([select id from Benefits_Coverage_MVN__c LIMIT 1] ));       
        obj.initializePage(); 
        Test.stopTest();        	
    }     
    
    @isTest
    public static void viewNDCDetailTestMethod3(){    
        ViewNDCDetails.testNDCID = null ; 
        Test.startTest();
        ViewNDCDetails obj = new ViewNDCDetails(new ApexPages.StandardController([select id from Product_MVN__c LIMIT 1] ));       
        obj.initializePage(); 
        Test.stopTest();        	
    }     
     
}