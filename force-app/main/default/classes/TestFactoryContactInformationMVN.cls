/*
 * TestFactoryContactInformationMVN
 * Created By: Kyle Thornton
 * Created Date: 4/7/2017
 * Description: This class is used to construct Contact Information objects
 *
 */
@isTest
public class TestFactoryContactInformationMVN {

    private TestFactorySObjectMVN objectFactory;

    public static Map<String, RecordType> ciRecordTypesByName {
        get {
            if (ciRecordTypesByName == null) {
                ciRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Contact_Information_MVN__c']) {
                    ciRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return ciRecordTypesByName;
        }
        private set;
    }

    public TestFactoryContactInformationMVN() {
        objectFactory = new TestFactorySObjectMVN('Contact_Information_MVN__c', new Map<String, Object>());
    }

    public Contact_Information_MVN__c constructEmail(Account parentAccount) {
        Contact_Information_MVN__c email = new Contact_Information_MVN__c(
            Account_MVN__c = parentAccount.Id,
            Email_MVN__c = parentAccount.FirstName + 'email@' + parentAccount.LastName + '.com',
            RecordTypeId = ciRecordTypesByName.get('Email_MVN').Id
        );

        return email;
    }

    public Contact_Information_MVN__c constructPhone(Account parentAccount) {
        Contact_Information_MVN__c phone = new Contact_Information_MVN__c(
            Account_MVN__c = parentAccount.Id,
            Phone_MVN__c = '5555555555',
            RecordTypeId = ciRecordTypesByName.get('Phone_MVN').Id
        );

        return phone;
    }

    public Contact_Information_MVN__c constructFax(Account parentAccount) {
        Contact_Information_MVN__c fax = new Contact_Information_MVN__c(
            Account_MVN__c = parentAccount.Id,
            Phone_MVN__c = '5555555555',
            Label_MVN__c = 'Fax',
            RecordTypeId = ciRecordTypesByName.get('Phone_MVN').Id
        );

        return fax;
    }
    
}