/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description implements And rule
 */
public with sharing class EligibilityEngineAndHdlrMVN extends EligibilityEngineRuleHdlrMVN {
    /**
     * checks if rule is fired for Ands - ALL of the child rules need to passed - then the AND rule is
     * passed
     * @return  Boolean     flag if rule is fired
     */
    private Boolean isRuleFired() {
        EligibilityEngineMVN engine = new EligibilityEngineMVN(super.requestedData,
                                                               super.rule.Child_Rules_MVN__r);
        List<Eligibility_Engine_Result_MVN__c> results = engine.run();
        for(Eligibility_Engine_Result_MVN__c childResult : results) {
            if(EligibilityEngineMVN.RULESUCCESS == childResult.Result_MVN__c) {
                return false;
            }
        }
        return true;
    }
}