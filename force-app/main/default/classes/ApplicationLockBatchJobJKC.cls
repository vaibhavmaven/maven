/**
 * @author      Knipper Admin
 * @date        March 2019
 * @group       ApplicationLockBatchJobJKC
 * @description Batch job that identifies all applicactions ready to be locked
 *             
 */
global class ApplicationLockBatchJobJKC implements Database.Batchable<sObject>, Schedulable {

    /**
    * EXECUTE SCHEDULE
    * @description: Call batch when the schedule runs
    */
    global void execute(SchedulableContext sc) {
        ApplicationLockBatchJobJKC ApplicationLockBatchJob = new ApplicationLockBatchJobJKC();
        database.executebatch(ApplicationLockBatchJob);
    }
       
    /**
     * @description Query Applications that expire today and in a status of Fulfilled 
     */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Date today = Date.Today();
        return Database.getQueryLocator([
            SELECT
                Id,
                Status_MVN__c,
                Expiry_Date_MVN__c
            FROM
                Application_MVN__c
            WHERE
                (Expiry_Date_MVN__c = today
                AND  
                Status_MVN__c = 'Fulfilled')
                  
        ]);
    }
    
 
    /**
     * @description Lock all applications and thier related records
     */
    global void execute(Database.BatchableContext bc, List<Application_MVN__c> applicationList){

        for (Application_MVN__c application : applicationList) {
          
            Set<Id> recordsToLock = new Set<Id>();

            Application_MVN__c applicationRelatedRecords = getApplicationWithAllRelatedRecordsToBeLocked(application.Id);
            recordsToLock.add(applicationRelatedRecords.Id);
            
            for(Id recordToLock : recordsToLock) {
                submitForApproval(recordToLock);
            }
            
            for(Benefits_Coverage_MVN__c thisBenefitsAndCoverage : applicationRelatedRecords.Benefits_Coverage_MVN__r) {
                recordsToLock.add(thisBenefitsAndCoverage.Id);
            } 
            
            for(Income_MVN__c thisIncome : applicationRelatedRecords.Income_MVN__r) {
                recordsToLock.add(thisIncome.Id);
            }
            
            for(Prescription_MVN__c thisPrescription : applicationRelatedRecords.Prescriptions_MVN__r) {
                recordsToLock.add(thisPrescription.Id);
            }
            
            for(Id recordToLock : recordsToLock) {
                this.submitForApproval(recordToLock);
            }
           
             
            Savepoint sp = Database.setSavepoint();
            try {
            
            } catch (Exception exc) {
                System.debug('\n\n\n### ' + exc.getStackTraceString() + '\n\n\n');
                Database.rollback(sp);
            }
        
        }
    }
    

  
     private void submitForApproval(Id recordId) {
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Lock');
        approvalRequest.setObjectId(recordId);
        Approval.ProcessResult approvalResult = Approval.process(approvalRequest);
    }
    
    
      /**
     * @description get Application with all its related records to lock
     * @parameter applicationId
     */
    private Application_MVN__c getApplicationWithAllRelatedRecordsToBeLocked(Id applicationId) {
        return [
            SELECT
                Id,
                (
                    SELECT
                        Id
                    FROM
                        Authorization_Consent_MVN__r
                ),
                (
                    SELECT
                        Id
                    FROM
                        Benefits_Coverage_MVN__r
                ),
                (
                    SELECT
                        Id
                    FROM
                        Income_MVN__r
                ),
                (
                    SELECT
                        Id
                    FROM
                        Prescriptions_MVN__r
                )
            FROM
                Application_MVN__c
            WHERE
                Id = :applicationId
        ];
    }
    
    global void finish(Database.BatchableContext bc){}
}