/*
*   DocumentEditExtensionMVN
*   Created By:     Florian Hoehn
*   Created Date:   May 16th, 2017
*   Description:    Provides extension functionality for the Program Member Document page
*/
public class DocumentEditExtensionMVN {
    /**
    * @description standardController of Document_MVN__c to provide navigation and record details
    */
    private ApexPages.StandardController stdController;

    /**
    * @description document record that gets inserted/updated
    */
    @testVisible private Document_MVN__c document;

    /**
    * @description attachment record that gets inserted
    */
    public Attachment file { get; set; }

    /**
    * @description boolean to indicate if file and document record are inserted
    */
    public Boolean isUploadedAndSaved { get; set; }

    /**
    * @description constructor
    *              - sets document record with pre-sets from the standardController
    *              - initiates attachment record
    * @param ApexPages.StandardController stdController
    */
    public DocumentEditExtensionMVN(ApexPages.StandardController stdController) {
        this.isUploadedAndSaved = false;
        this.stdController = stdController;
        this.document = (Document_MVN__c) this.stdController.getRecord();
        if(this.document.Id != null) {
            this.document.Program_Member_MVN__c = [SELECT Program_Member_MVN__c
                                                     FROM Document_MVN__c
                                                    WHERE Id =: this.document.Id].Program_Member_MVN__c;
        }
        if(this.document.Program_Member_MVN__c == null) {
            this.setProgramMember();
        }
        this.file = new Attachment();
    }

    /**
    * @description inserts/updates document & attachment records into the DB with transactional security
    *              - checks if attachment is set
    *              - upserts document
    *              - inserts attachment
    *              - updates document with attachment id
    * @return PageReference [to where the user came from]
    */
    public PageReference save() {
        if(this.file.body == null && this.document.Id == null) {
            ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Error_No_Attachment)
            );
            return null;
        }
        System.Savepoint sp = Database.setSavePoint();
        try {
            if(this.document.Title_MVN__c == null) {
                this.document.Title_MVN__c = this.file.Name;
            }

            if(this.document.Id == null) {
                insert this.document;
                this.file.ParentId = this.document.Id;
                insert this.file;

                this.document.Attachment_Id_MVN__c = this.file.Id;
            }
            update this.document;
            this.isUploadedAndSaved = true;
            this.file.body = null; // reset body to null to avoid heap size limit
        } catch (System.DmlException ex) {
            Database.rollback(sp);
            ApexPages.addMessages(ex);
            return null;
        }
        return null;
    }

    /**
    * @description finds the Program_Member_MVN__c Id this new document is related to
    *              - checks in program member attachment settings for
    *                  - sObject API names
    *                  - lookup field API name on this sObject to Program_Member_MVN__c
    *                  - lookup field API name to this sObject on Document_MVN__c
    *              - sets Program_Member_MVN__c relationship on the document
    * ATTENTION: SOQL query in for loop: not an issue as we are limited by the amount of lookup fields on an sObject
    */
    private void setProgramMember() {
        for(Document_Setting_MVN__mdt setting : [SELECT Lookup_Field_API_Name_MVN__c,
                                                        SObject_API_Name_MVN__c,
                                                        Program_Member_Lookup_Field_API_Name_MVN__c
                                                   FROM Document_Setting_MVN__mdt]) {
            try {
                if(this.document.get(setting.Lookup_Field_API_Name_MVN__c) != null) {
                    SObject obj = Database.query(
                        'SELECT ' + String.escapeSingleQuotes(setting.Program_Member_Lookup_Field_API_Name_MVN__c) +
                        '  FROM ' + String.escapeSingleQuotes(setting.SObject_API_Name_MVN__c) +
                        ' WHERE Id = \'' + String.escapeSingleQuotes((String)this.document.get(setting.Lookup_Field_API_Name_MVN__c)) + '\' LIMIT 1');
                    this.document.Program_Member_MVN__c =
                        (Id)obj.get(setting.Program_Member_Lookup_Field_API_Name_MVN__c);
                }
            } catch(Exception e) {
                ApexPages.addMessage(
                    new ApexPages.Message(
                        ApexPages.Severity.INFO,
                        System.Label.Info_PMA_Settings_Incorrect
                    )
                );
            }
        }
    }
}