/**
 *  ContactInformationControllerTestMVN
 *  Created By:     Paul Battisson
 *  Created On:     03/23/2016
 *  Description:    This is the test class for the ContactInfoRedirectControllerMVN class.
 *                  
 **/

@isTest
private class ContactInfoRedirectControllerTestMVN {
	
    static {
        TestDataFactoryMVN.createPSSettings();
    }

	@isTest 
    private static void testRedirectNewPageAddress() {
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Address_MVN' AND SobjectType = 'Contact_Information_MVN__c'].Id;
        Schema.DescribeSObjectResult addrDSR = Address_vod__c.SObjectType.getDescribe();

		Test.setCurrentPageReference(new PageReference('Page.CreateContactInformationMVN'));
        System.currentPageReference().getParameters().put('RecordType', rtId);

        ApexPages.StandardController stdCon = new ApexPages.StandardController(new Contact_Information_MVN__c());
        ContactInfoRedirectControllerMVN con = new ContactInfoRedirectControllerMVN(stdCon);

        System.assertEquals(rtId, con.contInfoRecordTypeId, 'Incorrect record type retrieved from params');
        System.assert(con.redirectNewPage().getUrl().contains(addrDSR.getKeyPrefix()), 'Incorrect pagereference, should be redirecting to address create page');
	}

    @isTest 
    private static void testRedirectNewPageEmail() {
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Email_MVN' AND SobjectType = 'Contact_Information_MVN__c'].Id;
        Schema.DescribeSObjectResult ciDSR = Contact_Information_MVN__c.SObjectType.getDescribe();

        Test.setCurrentPageReference(new PageReference('Page.CreateContactInformationMVN'));
        System.currentPageReference().getParameters().put('RecordType', rtId);

        ApexPages.StandardController stdCon = new ApexPages.StandardController(new Contact_Information_MVN__c());
        ContactInfoRedirectControllerMVN con = new ContactInfoRedirectControllerMVN(stdCon);

        System.assertEquals(rtId, con.contInfoRecordTypeId, 'Incorrect record type retrieved from params');
        System.assert(con.redirectNewPage().getUrl().contains(ciDSR.getKeyPrefix()), 'Incorrect pagereference, should be redirecting to address create page');
    }

    @isTest
    private static void testRedirectEditPageAddress() {
        Account doc = TestDataFactoryMVN.createPhysician();
        Account pat = TestDataFactoryMVN.createMember();
        Program_MVN__c prog = TestDataFactoryMVN.createProgram();
        Program_Stage_MVN__c parent = TestDataFactoryMVN.createParentProgramStage(prog);
        Program_Stage_MVN__c child = TestDataFactoryMVN.createChildProgramStage(prog, parent);
        Program_Member_MVN__c member = TestDataFactoryMVN.createProgramMember(prog, pat);
        Address_vod__c add = TestDataFactoryMVN.createTestAddress(pat);

        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Address_MVN' AND SobjectType = 'Contact_Information_MVN__c'].Id;
        Contact_Information_MVN__c info = [SELECT Id, Address_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Contact_Information_MVN__c WHERE Address_MVN__c = :add.Id AND RecordTypeId = :rtId LIMIT 1];
        
        Test.setCurrentPageReference(new PageReference('Page.CreateContactInformationMVN'));
        System.currentPageReference().getParameters().put('RecordType', rtId);
        System.currentPageReference().getParameters().put('id', info.Id);

        Test.startTest();
        ApexPages.StandardController stdCon = new ApexPages.StandardController(info);
        ContactInfoRedirectControllerMVN con = new ContactInfoRedirectControllerMVN(stdCon);
        Test.stopTest();

        System.assertEquals(rtId, con.contInfoRecordTypeId, 'Incorrect record type retrieved from params');
        String addStr = String.valueOf(add.Id).subString(0,15);
        System.assert(con.redirectEditPage().getUrl().contains(addStr), 'Incorrect pagereference, should be redirecting to address edit page');
        System.assertEquals('/' + add.Id, con.redirectEditPage().getParameters().get('retURL'), 'It should redirect to Address view action.');
        con.paramMap.put('retURL', '/');
        System.assertEquals('/', con.redirectEditPage().getParameters().get('retURL'), 'It should override the return URL.');
    }

    @isTest
    private static void testRedirectEditPagePhone() {
        Account pat = TestDataFactoryMVN.createMember();
        
        Id rtId = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Contact_Information_MVN__c' AND DeveloperName = 'Phone_MVN' LIMIT 1].Id;

        Contact_Information_MVN__c info = new Contact_Information_MVN__c();
        info.Account_MVN__c = pat.Id;
        info.Phone_MVN__c = '0987654321';
        info.RecordTypeId = rtId;
        insert info;

        Test.setCurrentPageReference(new PageReference('Page.CreateContactInformationMVN'));
        System.currentPageReference().getParameters().put('RecordType', rtId);
        System.currentPageReference().getParameters().put('id', info.Id);

        ApexPages.StandardController stdCon = new ApexPages.StandardController(info);
        ContactInfoRedirectControllerMVN con = new ContactInfoRedirectControllerMVN(stdCon);

        System.assertEquals(rtId, con.contInfoRecordTypeId, 'Incorrect record type retrieved from params');
        String addStr = String.valueOf(info.Id).subString(0,15);
        System.assert(con.redirectEditPage().getUrl().contains(addStr), 'Incorrect pagereference, should be redirecting to contact info edit page');

    }
	
}