/**
 * @author Mavens
 * @description when a record is inserted and application is not filled out, fill it with the active
 *              application on the related program member
 * @group Application
 */
public with sharing class ApplicationRelationHdlrMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
     * @description handle
     */
    public void handle() {
        Map<Id, Id> activeApplicationIdsByProgramMember = this.getActiveApplicationsByProgramMember();
        List<SObject> relatedRecords = new List<SObject>();
        for(SObject thisSObject : trigger.new) {
            if(thisSObject.get('Application_MVN__c') == null) {
                thisSObject.put(
                    'Application_MVN__c',
                    activeApplicationIdsByProgramMember.get(
                        (Id)thisSObject.get('Program_Member_MVN__c')
                    )
                );
            }
        }
    }

    private Map<Id, Id> getActiveApplicationsByProgramMember() {
        Map<Id, Id> activeApplicationIdsByProgramMember = new Map<Id, Id>();
        for(SObject thisSObject : trigger.new) {
            if(thisSObject.get('Application_MVN__c') == null) {
                activeApplicationIdsByProgramMember.put(
                    (Id)thisSObject.get('Program_Member_MVN__c'),
                    null
                );
            }
        }
        for(Program_Member_MVN__c thisProgramMember : this.getProgramMembers(activeApplicationIdsByProgramMember.keyset())) {
            activeApplicationIdsByProgramMember.put(
                thisProgramMember.Id,
                thisProgramMember.Active_Application_MVN__c
            );
        }
        return activeApplicationIdsByProgramMember;
    }

    private List<Program_Member_MVN__c> getProgramMembers(Set<Id> programMemberIds) {
        return [
            SELECT
                Id,
                Active_Application_MVN__c
            FROM
                Program_Member_MVN__c
            WHERE
                Id IN :programMemberIds
        ];
    }
}