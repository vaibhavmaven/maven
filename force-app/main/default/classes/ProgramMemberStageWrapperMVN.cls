/*
 *  ProgramMemberStageWrapperMVN
 *  Created By:     Jean-Philippe Monette
 *  Created Date:   August 31st, 2016
 *  Description:    Wrapper for the Program Member Stage object. Representing information about the
 *                  Stage and related Activities.
 */
global with sharing class ProgramMemberStageWrapperMVN implements Comparable {

	// Representing the Stage Name
	public String name {
		get {
			if(pms != null) {
				return String.isNotBlank(pms.Program_Stage_Name_MVN__c) ? pms.Program_Stage_Name_MVN__c + ' (' + pms.Application_MVN__r.Name + ') ' : Label.Blank;
			} else if(caseRecord != null) {
 				return String.isNotBlank(caseRecord.Subject) ? caseRecord.Subject : Label.Blank;
  			} else {
				return Label.Adhoc_Activities;
			}
		}
	}

	// Representing the Stage Record ID
	public Id stageId {
		get {
			if(pms != null) {
				return pms.Id;
			} else {
				return null;
			}
		}
	}

	// Representing the Case Record ID
	public Id caseId {
		get {
			if(caseRecord != null) {
				return caseRecord.Id;
			} else {
				return null;
			}
		}
	}

	// Representing the Stage Status, accessible via Visualforce Components
	public Boolean isInactive	{ get { return isInactive(); } }
	public Boolean isStarted    { get { return isStarted(); } }
	public Boolean isCompleted 	{ get { return isCompleted(); } }

	public Case 						caseRecord  { get; private set; }
	public Program_Member_Stage_MVN__c	pms			{ get; private set; }

	public List<ProgramMemberStageWrapperMVN> children { get; private set; }
	public List<ProgramMemberStageWrapperMVN> activeChildren {get; private set;}
	public List<ProgramMemberStageWrapperMVN> inactiveChildren {get; private set;}
	public List<ProgramMemberStageWrapperMVN> completedChildren {get; private set;}

	/**
	 * Program Member Stage Wrapper Constructor
	 * @access	public
	 */
	public ProgramMemberStageWrapperMVN() {
		initializeLists();
	}

	/**
	 * Program Member Stage Wrapper Constructor, using Program Member Stage record
	 * @param	Program_Member_Stage_MVN__c
	 * @access	public
	 */
	public ProgramMemberStageWrapperMVN(Program_Member_Stage_MVN__c pms) {
		this.pms = pms;
		initializeLists();

		if(!pms.Cases__r.isEmpty()) {
			this.caseRecord = pms.Cases__r[0];
		}
	}

	/**
	 * Program Member Stage Wrapper Constructor, using Case record
	 * @param	Case
	 * @access	public
	 */
	public ProgramMemberStageWrapperMVN(Case caseRecord) {
		this.caseRecord = caseRecord;
		initializeLists();
	}

	public void initializeLists() {
		this.children = new List<ProgramMemberStageWrapperMVN>();
		this.activeChildren = new List<ProgramMemberStageWrapperMVN>();
		this.inactiveChildren = new List<ProgramMemberStageWrapperMVN>();
		this.completedChildren = new List<ProgramMemberStageWrapperMVN>();
	}

	/**
	 * Adding Child Stage
	 * @return	void
	 * @access	private
	 */
	public void addChild(ProgramMemberStageWrapperMVN pms) {
		if(pms.isInactive){
			inactiveChildren.add(pms);
		} else if(pms.isStarted){
			activeChildren.add(pms);
		} else if(pms.isCompleted){
			completedChildren.add(pms);
		}
		rebuildChildren();
	}

	public void rebuildChildren(){
		children = new List<ProgramMemberStageWrapperMVN>();
		children.addAll(inactiveChildren);
		children.addAll(activeChildren);
		children.addAll(getLimitedCompletedChildren());
		children.sort();
	}

	/**
	 * The list of stages displayed should be limited to the active and inactive stages or 10 total stages. This method
	 */
	public List<ProgramMemberStageWrapperMVN> getLimitedCompletedChildren() {
		Integer sizeRemaining = 10 - (activeChildren.size() + inactiveChildren.size());

		List<ProgramMemberStageWrapperMVN> stages = new List<ProgramMemberStageWrapperMVN>();

		for(Integer i = 0; i < sizeRemaining; i++) {
			if(completedChildren.size() == i) break;
			stages.add(completedChildren.get(i));
		}

		return stages;
	}

	public Boolean getIsIncompleteList() {
		return completedChildren.size() != getLimitedCompletedChildren().size();
	}

	/**
	 * Is Stage Inactive or not
	 * @return	Boolean
	 * @access	private
	 */
	private Boolean isInactive() {
		if(pms != null) {
			return pms.Status_MVN__c == 'Not Started';
		} else {
			return false;
		}
	}

	/**
	 * Is Stage Started or not
	 * @return	Boolean
	 * @access	private
	 */
	private Boolean isStarted() {
		if(pms != null) {
			return pms.Status_MVN__c == 'Started';
		} else if(caseRecord != null) {
			return !caseRecord.IsClosed;
		} else {
			return true;
		}
	}

	/**
	 * Is Stage Completed or not
	 * @return	Boolean
	 * @access	private
	 */
	private Boolean isCompleted() {
		if(pms != null) {
			return pms.IsClosed_MVN__c;
		} else if(caseRecord != null) {
			return caseRecord.IsClosed;
		} else {
			return false;
		}
	}

	/**
	 * If wrapper has Case
	 * @return	Boolean
	 * @access	private
	 */
	private Boolean hasCase() {
		return caseRecord != null;
	}

	/**
	 * If wrapper has Program Member Stage
	 * @return	Boolean
	 * @access	private
	 */
	private Boolean hasStage() {
		return pms != null;
	}

	/**
	 * If wrapper is Additional Activities
	 * @return	Boolean
	 * @access	private
	 */
	private Boolean isAdditionalActivities() {
		return !hasCase() && !hasStage();
	}

	/**
	 * Comparing records on sort
	 * @param	Object	Salesforce Object
	 * @return	Integer
	 * @access	global
	 */
	global Integer compareTo(Object obj) {
		ProgramMemberStageWrapperMVN stage = (ProgramMemberStageWrapperMVN)(obj);

		if(this.isAdditionalActivities() && !stage.isAdditionalActivities()) return 0;
		if(this.isAdditionalActivities() && stage.isCompleted()) return -1;
		if(stage.isAdditionalActivities() && this.isCompleted()) return 1;

		if(String.isNotBlank(this.pms.Parent_Program_Member_Stage_MVN__c) && String.isNotBlank(stage.pms.Parent_Program_Member_Stage_MVN__c)) {
			if(this.pms.Status_MVN__c != stage.pms.Status_MVN__c) {
				if(this.isCompleted())  return 1;
				if(stage.isCompleted()) return -1;

				if(this.isStarted())    return -1;
				if(stage.isStarted())   return 1;
			}
		}

		if(this.isCompleted() != stage.isCompleted()) {
			if(this.isCompleted())  return 1;
			if(stage.isCompleted()) return -1;
		}

		Decimal sourceNo = this.pms.Stage_Sequence_Number_MVN__c;
		Decimal compareNo = stage.pms.Stage_Sequence_Number_MVN__c;

		if(sourceNo > compareNo) 		return 1;
		else if(sourceNo < compareNo)	return -1;

		return 0;
	}
}