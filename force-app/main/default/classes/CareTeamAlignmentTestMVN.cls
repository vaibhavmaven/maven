/**
 * @author Mavens
 * @description Test CareTeamAlignmentMVN
 * @group ProgramMember
 **/

@isTest
private class CareTeamAlignmentTestMVN {
    static String zipCode = '60000';
    static String country = 'United States';
    static User caseManager;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        TestFactoryCustomMetadataMVN.setMocks();
        TestDataFactoryMVN.createPSSettings();

        System.runAs(caseManager) {
            Program_MVN__c program = TestDataFactoryMVN.createProgram();

            Zip_to_Team_MVN__c zipToTeam = new Zip_to_Team_MVN__c(
                Zip_Code_MVN__c = zipCode,
                Country_MVN__c = country,
                Program_MVN__c = program.Id
            );

            zipToTeam.put(new CareTeamAlignmentMVN().caseManager, UserInfo.getUserId());
            insert zipToTeam;

            Account testAccount = TestDataFactoryMVN.createMember();
        }
    }

    @isTest static void default_assignment() {
        // we update the zip and country to match the zipToTeam record.

        Account testAccount = [SELECT Id FROM Account];
        testAccount.BillingPostalCode = zipCode;
        testAccount.BillingCountry = country;
        update testAccount;

        System.runAs(caseManager) {
            Test.startTest();
                Program_Member_MVN__c programMember = TestDataFactoryMVN.createProgramMember(
                    [SELECT Id FROM Program_MVN__c],
                    [SELECT Id FROM Account]
                );
            Test.stopTest();
        }

        // assignment found
        Id zipToTeamId = [SELECT Id FROM Zip_to_Team_MVN__c].Id;
        Id programMemberZipToTeamId = [SELECT Zip_to_Team_MVN__c FROM Program_Member_MVN__c].Zip_to_Team_MVN__c;
        System.assertEquals(zipToTeamId, programMemberZipToTeamId);
    }


    @isTest static void assignment_not_found() {
        Program_Member_MVN__c programMember;

        System.runAs(caseManager) {
            Test.startTest();
                Program_MVN__c program = [SELECT Id FROM Program_MVN__c];
                Account account = [SELECT Id FROM Account];
                programMember = TestDataFactoryMVN.createProgramMember(
                    program,
                    account
                );
            Test.stopTest();
        }

        programMember = database.query('SELECT ' + new CareTeamAlignmentMVN().caseManager + ', Zip_to_Team_MVN__c FROM Program_Member_MVN__c');

        // assignment failed as expected
        System.assertEquals(null, programMember.Zip_to_Team_MVN__c);

        // the case manager is set to the active user by default
        System.assertEquals(caseManager.Id, programMember.get(new CareTeamAlignmentMVN().caseManager));

    }

    @isTest static void test_physician() {
        // we update the zip and country to match the zipToTeam record.
        Account testAccount = [SELECT Id FROM Account];
        testAccount.BillingPostalCode = zipCode;
        testAccount.BillingCountry = country;
        update testAccount;

        System.runAs(caseManager) {
            Test.startTest();
                Program_MVN__c program = [SELECT Id FROM Program_MVN__c];
                Account account = [SELECT Id FROM Account];
                Program_Member_MVN__c programMember = TestDataFactoryMVN.createProgramMember(
                    program,
                    account
                );
                Account professional = new TestFactoryAccountMVN().createPrescriber();
                programMember.Physician_MVN__c = professional.Id;
                update programMember;
            Test.stopTest();
        }

        // assignment found
        Id zipToTeamId = [SELECT Id FROM Zip_to_Team_MVN__c].Id;
        Id programMemberZipToTeamId = [SELECT Zip_to_Team_MVN__c FROM Program_Member_MVN__c].Zip_to_Team_MVN__c;
        System.assertEquals(zipToTeamId, programMemberZipToTeamId);
    }
}