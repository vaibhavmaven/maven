/*
 * CreateRecurringCaseTriggerMVN
 * Created By: Roman Lerman
 * Created Date: 5/4/2017
 * Description: When an address record is inserted or updated manage the related contact info records
 *
 */
public with sharing class CreateRecurringCaseTriggerMVN implements TriggerHandlerMVN.HandlerInterface {

    static final Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();

    public List<Schema.FieldSetMember> getCaseCloneFields() {
        return SObjectType.Case.FieldSets.Recurring_Case_Fields_MVN.getFields();
    }

    public void handle() {
        Map<Id, Case> oldCaseMap = (Map<Id, Case>) Trigger.oldMap;

        List<Case> caseList = new List<Case>();

        Map<Id, Case> parentStageMap = 	new Map<Id, Case>([select Program_Member_Stage_MVN__r.Program_Stage_MVN__c,
                                                            Program_Member_Stage_MVN__r.Program_Member_MVN__c,
                                                            Program_Member_Stage_MVN__r.Program_Stage_Name_MVN__c,
                                                            Program_Member_Stage_MVN__r.Parent_Program_Member_Stage_MVN__c 
                                                            from Case where Id in :Trigger.new]
                                                        );

        Map<Id, Program_Member_Stage_MVN__c> programMemberStageMap = new Map<Id, Program_Member_Stage_MVN__c>();
        for(Case cs : (List<Case>) Trigger.new) {
            // Setup the child program stages for the new activities we will have to create 
            if(cs.Is_Recurring_MVN__c == settings.Is_Recurring_Indicator_MVN__c 
                && cs.Status != oldCaseMap.get(cs.Id).Status 
                && cs.Status == settings.Case_Completed_Status_MVN__c) {

                if(parentStageMap.get(cs.Id) != null &&
                    parentStageMap.get(cs.Id).Program_Member_Stage_MVN__r.Program_Member_MVN__c != null) {
                    Case parentStageFields = parentStageMap.get(cs.Id);
                    Program_Member_Stage_MVN__c stage = new Program_Member_Stage_MVN__c();
                    stage.Program_Stage_MVN__c = parentStageFields.Program_Member_Stage_MVN__r.Program_Stage_MVN__c;
                    stage.Parent_Program_Member_Stage_MVN__c = parentStageFields.Program_Member_Stage_MVN__r.Parent_Program_Member_Stage_MVN__c;
                    stage.Program_Member_MVN__c = parentStageFields.Program_Member_Stage_MVN__r.Program_Member_MVN__c;
                    stage.Status_MVN__c = settings.Program_Member_Stage_Start_Status_MVN__c;
                    
                    programMemberStageMap.put(cs.Id, stage);
                }
            }
        }

        if(programMemberStageMap != null && programMemberStageMap.size() > 0) {
            insert programMemberStageMap.values();
        }

        Id openAuditRequestsGroup = [SELECT DeveloperName,Id FROM Group WHERE DeveloperName = 'Open_Audit_Requests_Knipper_PAP_MVN'].Id;
        for(Case cs : (List<Case>) Trigger.new) {
            // If the Case is Recurring and was just closed
            // let's go ahead and clone the case based on the fieldset
            // and set the due date of the case either to the next call date
            // or the case frequency days (i.e. number of days from today)
            if(cs.Is_Recurring_MVN__c == settings.Is_Recurring_Indicator_MVN__c
                && cs.Status != oldCaseMap.get(cs.Id).Status 
                && cs.Status == settings.Case_Completed_Status_MVN__c) {

                Case nextCase = new Case();
                for(Schema.FieldSetMember caseField : this.getCaseCloneFields()) {
                    nextCase.put(caseField.getFieldPath(), cs.get(caseField.getFieldPath()));
                }

                if(programMemberStageMap.get(cs.Id) != null) {
                    nextCase.Program_Member_Stage_MVN__c = programMemberStageMap.get(cs.Id).Id;
                }
                
                nextCase.ParentId = cs.Id;

                if(cs.Next_Call_Date_MVN__c != null) {
                    nextCase.Due_Date_MVN__c = cs.Next_Call_Date_MVN__c;
                } else {
                    nextCase.Due_Date_MVN__c = Date.today() + (Integer) cs.Case_Frequency_Days_MVN__c;
                }
                nextCase.OwnerId = openAuditRequestsGroup;
                caseList.add(nextCase);
            }
        }
        
        if(caseList != null && caseList.size() > 0) {
            insert caseList;
        }
    }
}