/**
* @author Mavens
* @date 07/10/2018 
* @description Class to provide factory methods to create test data for Order Scheduler objects
*/
@isTest
public class TestFactoryOrderSchedulerMVN {

    private TestFactorySObjectMVN objectFactory;

    public TestFactoryOrderSchedulerMVN() {
        objectFactory = new TestFactorySObjectMVN('Order_Scheduler_MVN__c', new Map<String, Object>());
    }

    public Order_Scheduler_MVN__c construct(Map<String, Object> valuesByField){
        return (Order_Scheduler_MVN__c) objectFactory.constructSObject(valuesByField);
    }

    public Order_Scheduler_MVN__c create(Map<String, Object> valuesByField){
        return (Order_Scheduler_MVN__c) objectFactory.createSObject(valuesByField);
    }

    public List<Order_Scheduler_MVN__c> constructMany(Integer numOfRequests, Map<String,Object> valuesByField) {
        return (List<Order_Scheduler_MVN__c>) objectFactory.constructSObjects(numOfRequests, valuesByField);
    }

    public List<Order_Scheduler_MVN__c> createMany(Integer numOfRequests, Map<String,Object> valuesByField) {
        return (List<Order_Scheduler_MVN__c>) objectFactory.createSObjects(numOfRequests, valuesByField);
    }
}