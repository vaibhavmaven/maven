/*
 * ContactInfoSetValueFieldTriggerMVN
 * Created By: Kyle Thornton
 * Created Date: 4/10/2017
 * Description: This Trigger handler ensures the value field on a contact info record displays the
 *              proper value (email, phone or address) for use in related lists
 *
 */

public with sharing class ContactInfoSetValueFieldTriggerMVN implements TriggerHandlerMVN.HandlerInterface {

    public void handle () {
        Map<Id, RecordType> ciRecordTypes = getCiRecordTypeMap();
        List<Contact_Information_MVN__c> setAddressList = new List<Contact_Information_MVN__c>();

        for (Contact_Information_MVN__c contactInfo : (List<Contact_Information_MVN__c>) Trigger.new) {
            String recTypeName = ciRecordTypes.get(contactInfo.RecordTypeId).DeveloperName;

            if (recTypeName == 'Address_MVN') {
                setAddressList.add(contactInfo);
            } else if (recTypeName == 'Email_MVN') {
                setEmail(contactInfo);
            } else if (recTypeName == 'Phone_MVN') {
                setPhone(contactInfo);
            }
        }

        Map<Id, Address_vod__c> addresses = getAddressesFor(setAddressList);

        for (Contact_Information_MVN__c ci : setAddressList) {
            if (addresses.containsKey(ci.Address_MVN__c)) {
                setAddress(ci, addresses.get(ci.Address_MVN__c));
            }
        }
    }

    private Map<Id, RecordType> getCiRecordTypeMap() {
        return new Map<Id, RecordType>(
            [SELECT Id,
                    DeveloperName
               FROM RecordType
              WHERE SObjectType='Contact_Information_MVN__c']
        );
    }

    private Map<Id, Address_vod__c> getAddressesFor(List<Contact_Information_MVN__c> addressCis) {
        Set<Id> addressIds = new Set<Id>();
        for (Contact_Information_MVN__c addressCi : addressCis){
            addressIds.add(addressCi.Address_MVN__c);
        }

        return new Map<Id, Address_vod__c>(
            [SELECT Id,
                    Name,
                    Address_Line_2_vod__c,
                    City_vod__c,
                    State_vod__c,
                    Zip_vod__c,
                    Country_vod__c
               FROM Address_vod__c
              WHERE Id IN :addressIds]
        );
    }

    public static void setAddress(Contact_Information_MVN__c contactInfo, Address_vod__c address) {
        String formattedAddress = address.Name;
        if (String.isNotBlank(address.Address_Line_2_vod__c)) {
            formattedAddress += '\n' + address.Address_Line_2_vod__c;
        }

        formattedAddress += '\n';

        if (String.isNotBlank(address.City_vod__c)) {
            formattedAddress += address.City_vod__c + ', ';
        }

        if (String.isNotBlank(address.State_vod__c)) {
            formattedAddress += address.State_vod__c + ' ';
        }

        if (String.isNotBlank(address.Zip_vod__c)) {
            formattedAddress += address.Zip_vod__c + ' ';
        }

        if (String.isNotBlank(address.Country_vod__c)) {
            formattedAddress += address.Country_vod__c;
        }

        contactInfo.Value_MVN__c = formattedAddress;
    }

    private static void setEmail(Contact_Information_MVN__c contactInfo) {
        contactInfo.Value_MVN__c = contactInfo.Email_MVN__c;
    }

    private static void setPhone(Contact_Information_MVN__c contactInfo) {
        contactInfo.Value_MVN__c = contactInfo.Phone_MVN__c;
    }
}