/**
 * @author Mavens
 * @description when an application gets inserted, add it as the active application on the related
 *              program member
 * @group Application
 */
public with sharing class ProgramMemberAddActiveAppHdlrMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
     * @description handle
     */
    public void handle() {
        Map<Id, Program_Member_MVN__c> relatedProgramMembers = new Map<Id, Program_Member_MVN__c>();
        for(Application_MVN__c thisApplication : (List<Application_MVN__c>)trigger.new) {
            if (relatedProgramMembers.containsKey(thisApplication.Program_Member_MVN__c)) {
                relatedProgramMembers.get(thisApplication.Program_Member_MVN__c).Active_Application_MVN__c = thisApplication.Id;
            } else {
                relatedProgramMembers.put(
                    thisApplication.Program_Member_MVN__c,
                    new Program_Member_MVN__c(
                        Id = thisApplication.Program_Member_MVN__c,
                        Active_Application_MVN__c = thisApplication.Id
                    )
                );
            }
        }
        update relatedProgramMembers.values();
    }
}