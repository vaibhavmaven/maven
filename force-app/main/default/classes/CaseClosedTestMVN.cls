/**
 * @author Mavens
 * @date 10/2018
 * @description Test for CaseClosedMVN
 * @group Case
 */ 
@isTest
private class CaseClosedTestMVN {

    static User caseManager;
    static Case testCase;
    static Case_Note_MVN__c note;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        System.runAs(caseManager) {
            testCase = TestDataFactoryMVN.createTestCase();
            createNote();
        }
    }

    static void createNote() {
        note = new Case_Note_MVN__c(
            Case_MVN__c = testCase.Id,
            Notes_MVN__c = 'My note',
            Locked_MVN__c = false
        );
        insert note;
    }

    static List<Case_Note_MVN__c> getLockedNotes() {
        return [
            SELECT 
                Id
            FROM
                Case_Note_MVN__c
            WHERE
                Case_MVN__c = :testCase.Id
            AND 
                Locked_MVN__c = true
        ];
    }

    @isTest
    static void testLockNote() {
        System.assert(getLockedNotes().isEmpty());
        System.runAs(caseManager) {
            Test.startTest();
            testCase.Status = 'Closed';
            testCase.Exclude_from_Billing_MVN__c = true;
            update testCase;
            Test.stopTest();
        }
        System.assert(!getLockedNotes().isEmpty());
    }

    @isTest
    static void testDoNotLockNote() {
        System.assert(getLockedNotes().isEmpty());
        System.runAs(caseManager) {
            Test.startTest();
            testCase.Exclude_from_Billing_MVN__c = true;
            update testCase;
            Test.stopTest();
        }
        System.assert(getLockedNotes().isEmpty());
    }

    @isTest
    static void testDoNotLockNoteEmptyNote() {
        System.assert(getLockedNotes().isEmpty());
        note.Notes_MVN__c = '';
        update note;
        System.runAs(caseManager) {
            Test.startTest();
            testCase.Status = 'Closed';
            testCase.Exclude_from_Billing_MVN__c = true;
            update testCase;
            Test.stopTest();
        }
        System.assert(getLockedNotes().isEmpty());
    }
}