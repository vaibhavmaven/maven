public class CoverageResponse{
    // this classs encapulates all the requried json responses for one NDC... 
    public Transient List<RTBI_Response_Master__c> rtbiMasterList {get; set;}
    public Transient List<FAS_Response_Master__c> fasResponseList {get; set;}
    public transient List<Eligibility_Response_Master__c> elgbResponseList {get; set;}
}