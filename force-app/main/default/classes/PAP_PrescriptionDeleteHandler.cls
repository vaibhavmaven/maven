/****************************************************************************************************************************** 
  Class Name: PAP_PrescriptionDeleteHandler
  Created By: PSL
  Created Date: 08-08-2019
  Description: This class is a handler for Trigger PrescriptionMVN. It is used to handle the changes to be done whenever any 
  Prescription is deleted
 ******************************************************************************************************************************/
public class PAP_PrescriptionDeleteHandler implements TriggerHandlerMVN.HandlerInterface{
    public void handle(){
        //List<Application_MVN__c> applicationList = new List<Application_MVN__c>();
        if(Trigger.isBefore){
            if(Trigger.isDelete){
                Set<Id> applicationIdSet = new Set<Id>();
                List<Prescription_MVN__c> prescList = (List<Prescription_MVN__c>)(Trigger.old);
                for(sObject Presc: Trigger.old)
                {
                    if(presc.get('Application_MVN__c') != null){
                        applicationIdSet.add((Id)presc.get('Application_MVN__c')); 
                    }     
                }//end of for loop
                Map<Id,Application_MVN__c> idAppMap = new Map<Id,Application_MVN__c>();
                idAppMap = new Map<Id,Application_MVN__c>([select id,Contains_First_Product_PAP__c,Contains_Second_Product_PAP__c, 
                                                           First_Product_Counter_PAP__c,Second_Product_Counter_PAP__c 
                                                           from Application_MVN__c where id IN :applicationIdSet]);
                
                for(Prescription_MVN__c presc: prescList){
                    if(presc.Application_MVN__c != null){
                        idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c = idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c == null ? 0 : idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c ; 
                        idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c = idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c == null ? 0 : idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c ; 
                        if(presc.Product_Type_PAP__c =='First Product')
                        {
                            if(idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c > 0){
                                idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c --;                                
                            }                            
                            if(idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c == 0)
                            {
                                idAppMap.get(presc.Application_MVN__c).Contains_First_Product_PAP__c = false;
                            }
                        }
                        else if(presc.Product_Type_PAP__c == 'Second Product')
                        {
                            if(idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c > 0)
                            {
                                idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c --;                                
                            }                            
                            if(idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c == 0)
                            {
                                idAppMap.get(presc.Application_MVN__c).Contains_Second_Product_PAP__c = false;
                            }
                        }
                    }//end of if   
                }
                update idAppMap.values();
            }//end of isDelete condition
        }//end of isBefore condition
    }
}