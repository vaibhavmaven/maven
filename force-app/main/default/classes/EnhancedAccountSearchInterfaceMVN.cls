/**
 *	EnhancedAccountSearchInterfaceMVN
 *	Created By:		Aggen
 *	Created On:		02/04/2016
 *  Description:	This is an interface class used to support the enhanced account search.
 *					
 **/
public with sharing class EnhancedAccountSearchInterfaceMVN {
	public interface searchInterface {
		// search the utility for Accounts and Contact Information for a given account record type
		List<EnhancedAccountSearchResultMockMVN> search(Account searchAcct, Address_vod__c searchAddr, Contact_Information_MVN__c searchContactInfo, String searchAcctRecordTypeId, Boolean isPersonAcctSearch, Boolean isEnrollmentPersonAcctSearch);
	}
}