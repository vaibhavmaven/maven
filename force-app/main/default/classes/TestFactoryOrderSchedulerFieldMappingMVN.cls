/**
 * @author Mavens
 * @date 07/14/2018
 * @description Class used to build fake data for custom metadata type for Order Scheduler Field Mapping
 */ 
@isTest
public with sharing class TestFactoryOrderSchedulerFieldMappingMVN {

    public static void setMocks() {
        List<Order_Scheduler_Field_Mapping_MVN__mdt> settings = new List<Order_Scheduler_Field_Mapping_MVN__mdt>();
        Map<String, Object> setting;

        setting = new Map<String, Object>{
            'Source_Field_MVN__c' => 'Shipping_Address_MVN__r.Name',
            'Source_Object_MVN__c' => 'Order_Scheduler_MVN__c',
            'Target_Field_MVN__c' => 'Shipping_Address_Line_1_MVN__c',
            'Target_Object_MVN__c' => 'Order_MVN__c'
        };

        settings.add(
            (Order_Scheduler_Field_Mapping_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Order_Scheduler_Field_Mapping_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Source_Field_MVN__c' => 'Prescription_MVN__r.Product_MVN__r.Name',
            'Source_Object_MVN__c' => 'Order_Scheduler_MVN__c',
            'Target_Field_MVN__c' => 'Product_Name_MVN__c',
            'Target_Object_MVN__c' => 'Order_Line_Item_MVN__c'
        };

        settings.add(
            (Order_Scheduler_Field_Mapping_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Order_Scheduler_Field_Mapping_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Source_Field_MVN__c' => 'NDC_MVN__c',
            'Source_Object_MVN__c' => 'Product_Line_Item_MVN__c',
            'Target_Field_MVN__c' => 'NDC_MVN__c',
            'Target_Object_MVN__c' => 'Order_Line_Item_MVN__c'
        };

        settings.add(
            (Order_Scheduler_Field_Mapping_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Order_Scheduler_Field_Mapping_MVN__mdt', setting)
        );

        OrderSchedulerFieldMappingMVN.setMocks(settings);
    }

    public static void setMocks(List<Map<String, Object>> fieldValuesList) {
        List<Order_Scheduler_Field_Mapping_MVN__mdt> settings = new List<Order_Scheduler_Field_Mapping_MVN__mdt>();
        for (Map<String, Object> setting : fieldValuesList) {
            settings.add(
                (Order_Scheduler_Field_Mapping_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                    'Order_Scheduler_Field_Mapping_MVN__mdt', setting)
            );
        }
        OrderSchedulerFieldMappingMVN.setMocks(settings);
    }

}