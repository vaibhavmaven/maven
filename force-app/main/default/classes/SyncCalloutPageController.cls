public with sharing class SyncCalloutPageController {


    // Unique label for the first request
    public String requestLabel1;
    // Unique label for the second request
    public String requestLabel2;
    // Result of first callout
    public String result1 {get;set;}
    public String result17 {get;set;}
    public String result3 {get;set;}
    public String result4 {get;set;}
    public String result5 {get;set;}
    public String result6 {get;set;}
    public String result7 {get;set;}
    public String result8 {get;set;}
    public String result9 {get;set;}
    public String result10 {get;set;}
    public String result12 {get;set;}
    public String result11 {get;set;}
    public String result13 {get;set;}
    public String result14 {get;set;}
    public String result15 {get;set;}
    public String result16 {get;set;}
    
   // Result of second callout
    public String result2 {get;set;}
    // Endpoints of long-running service
    private static final String LONG_RUNNING_SERVICE_URL1 = 
        'http://durationservice.herokuapp.com';
    private static final String LONG_RUNNING_SERVICE_URL2 = 
        'http://durationservice.herokuapp.com';
           
    // Action method
    public Object startRequestsInParallel() {
      // Create continuation with a timeout
      Continuation con = new Continuation(120);
      // Set callback method
      con.continuationMethod='processAllResponses';
      
      // Create first callout request
      HttpRequest req1 = new HttpRequest();
      req1.setMethod('GET');
      req1.setEndpoint(LONG_RUNNING_SERVICE_URL1);
      
      // Add first callout request to continuation
      this.requestLabel1 = con.addHttpRequest(req1);     
      
      // Create second callout request
      HttpRequest req2 = new HttpRequest();
      req2.setMethod('GET');
      req2.setEndpoint(LONG_RUNNING_SERVICE_URL2);
      
      // Add second callout request to continuation
      this.requestLabel2 = con.addHttpRequest(req2);     
      
      // Return the continuation
      return con;  
    }
    
    // Callback method.
    // Invoked only when responses of all callouts are returned.
    public Object processAllResponses() {   
      // Get the response of the first request
      HttpResponse response1 = Continuation.getResponse(this.requestLabel1);
      this.result1 = response1.getBody();
HttpResponse response3 = Continuation.getResponse(this.requestLabel1);
      this.result3 = response3.getBody();

        HttpResponse response4 = Continuation.getResponse(this.requestLabel1);
      this.result4 = response4.getBody();
        HttpResponse response5 = Continuation.getResponse(this.requestLabel1);
      this.result5 = response5.getBody();
        HttpResponse response6 = Continuation.getResponse(this.requestLabel1);
      this.result6 = response6.getBody();

        HttpResponse response7 = Continuation.getResponse(this.requestLabel1);
      this.result7 = response7.getBody();
        HttpResponse response8 = Continuation.getResponse(this.requestLabel1);
      this.result8 = response8.getBody();
        
                HttpResponse response9 = Continuation.getResponse(this.requestLabel1);
      this.result9 = response9.getBody();
        HttpResponse response10 = Continuation.getResponse(this.requestLabel1);
      this.result10 = response10.getBody();
        HttpResponse response11 = Continuation.getResponse(this.requestLabel1);
      this.result11 = response11.getBody();

        HttpResponse response12 = Continuation.getResponse(this.requestLabel1);
      this.result12 = response12.getBody();
        HttpResponse response13 = Continuation.getResponse(this.requestLabel1);
      this.result13 = response13.getBody();

                  HttpResponse response19 = Continuation.getResponse(this.requestLabel1);
      this.result14 = response19.getBody();
        HttpResponse response110 = Continuation.getResponse(this.requestLabel1);
      this.result15 = response110.getBody();
        HttpResponse response111 = Continuation.getResponse(this.requestLabel1);
      this.result16 = response111.getBody();

        HttpResponse response112 = Continuation.getResponse(this.requestLabel1);
      this.result1 = response112.getBody();
        HttpResponse response113 = Continuation.getResponse(this.requestLabel1);
      this.result1 = response113.getBody();


      // Get the response of the second request
      HttpResponse response2 = Continuation.getResponse(this.requestLabel2);
      this.result2 = response2.getBody();
                 
      // Return null to re-render the original Visualforce page
      return null;
    }
   
}