/**
 *	ProgramMemberStageManagerMVN
 *	Created By:		Aggen
 *	Created On:		6/5/2015
 *  Modified:       March 3, 2017 to bulkify code (Kyle Thornton)
 *  Description:	this class is responsible for traversing Program_Member_Stage_MVN__c records for a Program Member and starting the next stage.
 *					it is also responsible for closing the parent stage if all the activities have been completed.
 *
 **/
public class ProgramMemberStageManagerMVN {
    public static Boolean allowNextParentStart = true;
    private static final Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();

    private static List<Program_Member_Stage_MVN__c> programMemberStagesToUpdate;
    private static Map<Id, List<Program_Member_Stage_MVN__c>> parentStagesByProgramMember;
    private static Map<Id, Program_Member_Stage_MVN__c> programMemberStagesById;

    public static Boolean startAtSpecificStage = false;

    public static void initializeProgramMembers(Set<Id> programMemberIds) {
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>();
        for (Id programMemberId : programMemberIds) {
            programMembers.add(new Program_Member_MVN__c(Id = programMemberId));
        }
        initializeProgramMembers(programMembers);
    }

    public static void initializeProgramMembers(List<Program_Member_MVN__c> programMembers) {
        programMembers = [SELECT Id,
                                 Starting_Program_Stage_ID_MVN__c,
                                 Do_Not_Initialize_MVN__c
                            FROM Program_Member_MVN__c
                           WHERE Id IN :programMembers];

        List<Program_Member_MVN__c> programMembersStartingSpecificStage = new List<Program_Member_MVN__c>();
        List<Program_Member_MVN__c> programMembersToInitialize = new List<Program_Member_MVN__c>();

        for(Program_Member_MVN__c programMember : programMembers) {
            if (programMember.Do_Not_Initialize_MVN__c){
                continue;
            }

            if (String.isNotBlank(programMember.Starting_Program_Stage_ID_MVN__c)) {
                programMembersStartingSpecificStage.add(programMember);
            } else {
                programMembersToInitialize.add(programMember);
            }
        }

        if(programMembersStartingSpecificStage.size() > 0) {
            startSpecificStages(programMembersStartingSpecificStage);
        }

        if(programMembersToInitialize.size() > 0) {
            initializeNormally(programMembersToInitialize);
        }
    }

    private static void initializeNormally(List<Program_Member_MVN__c> programMembers) {
        programMemberStagesToUpdate = new List<Program_Member_Stage_MVN__c>();

        createDataMapsFor(programMembers);

        for (Program_Member_MVN__c programMember : programMembers) {
            if (!parentStagesByProgramMember.containsKey(programMember.id)) {
                continue; //no parent stages to process for this program member
            }

            List<Program_Member_Stage_MVN__c> parentStages = parentStagesByProgramMember.get(programMember.id);
            initializeNextParent(parentStages);
        }

        update programMemberStagesToUpdate;
    }

    public static void initializeParentStage(Id programMemberId, Id parentStageId) {
        Program_Member_MVN__c programMember = new Program_Member_MVN__c(Id=programMemberId);
        createDataMapsFor(new List<Program_Member_MVN__c>{programMember});
        initializeNextParent([SELECT Id,
                                     Status_MVN__c,
                                     Closed_Date_MVN__c,
                                     Initiated_Date_MVN__c,
                                     (SELECT Id FROM Program_Member_Stages__r)
                                FROM Program_Member_Stage_MVN__c
                               WHERE Id = :parentStageId]);
        update programMemberStagesToUpdate;
    }

    private static void initializeNextParent(List<Program_Member_Stage_MVN__c> parentStages) {
        Boolean needsStartedParentStage = allowNextParentStart;
        for (Program_Member_Stage_MVN__c parentStage : parentStages) {
            Set<ID> childStageIds = new Set<Id>();
            for (Program_Member_Stage_MVN__c childStage : parentStage.Program_Member_Stages__r) {
                childStageIds.add(childStage.id);
            }

            Boolean allChildrenClosed = initializeChildStages(childStageIds, needsStartedParentStage);
            if (allChildrenClosed) {
                if (parentStage.Status_MVN__c != settings.ProgramMS_Completed_Status_MVN__c) {
                    parentStage.Status_MVN__c = settings.ProgramMS_Completed_Status_MVN__c;
                    parentStage.Closed_Date_MVN__c = System.today();
                    programMemberStagesToUpdate.add(parentStage);
                }
            } else if (parentStage.Status_MVN__c == settings.ProgramMS_Completed_Status_MVN__c) {
                parentStage.Status_MVN__c = settings.Program_Member_Stage_Start_Status_MVN__c;
                parentStage.IsClosed_MVN__c = false;
                programMemberStagesToUpdate.add(parentStage);
            } else if (needsStartedParentStage ) {
                needsStartedParentStage = false;
                if (parentStage.Status_MVN__c != settings.Program_Member_Stage_Start_Status_MVN__c) {
                    parentStage.Status_MVN__c = settings.Program_Member_Stage_Start_Status_MVN__c;
                    parentStage.Initiated_Date_MVN__c = System.today();
                    programMemberStagesToUpdate.add(parentStage);
                }
            }
        }
    }

    /**
     * Iterate over the child stages and determine if any stages need to be set to started and if all
     * children are closed
     * @param  childStageIds the set of ids which need to be checked.
     * @return               Boolean indicating whether all children are closed
     */
    private static Boolean initializeChildStages(Set<Id> childStageIds, Boolean pmNeedsStartedParentStage) {
        Boolean allChildrenClosed = true;

        for(Id childStageId : childStageIds) {
            Program_Member_Stage_MVN__c stage = programMemberStagesById.get(childStageId);

            // is the current stage already started? All children are not closed
            if(stage.Status_MVN__c == settings.Program_Member_Stage_Start_Status_MVN__c) {
                allChildrenClosed = false;
                continue;
            }

            // is the current stage closed?
            if (!stage.IsClosed_MVN__c) {
                allChildrenClosed = false;
                // If the current program member has no open started stages (pmNeedsStarteedParentStage) or
                // the parent of this stage has already been started AND this stage is not dependent on an open child
                // stages, then this child stage can be started.
                String parentStatus = stage.Parent_Program_Member_Stage_MVN__r.Status_MVN__c;
                Boolean parentStarted =  parentStatus == settings.Program_Member_Stage_Start_Status_MVN__c;
                Boolean childCanBeStarted = pmNeedsStartedParentStage || parentStarted;

                if(stage.Program_Member_Stage_Dependencies__r.isEmpty() && childCanBeStarted) {
                    // set the stage status to 'Started' and initiated date to today
                    stage.Status_MVN__c = settings.Program_Member_Stage_Start_Status_MVN__c;
                    stage.Initiated_Date_MVN__c = System.today();
                    programMemberStagesToUpdate.add(stage);
                }
            }
        }

        return allChildrenClosed;
    }

    // Start all of the elements of the program member list at the stage specified on the program member records
    private static void startSpecificStages(List<Program_Member_MVN__c> programMembers) {
        programMemberStagesToUpdate = new List<Program_Member_Stage_MVN__c>();

        createDataMapsFor(programMembers);

        for (Program_Member_MVN__c programMember : programMembers) {
            if(!parentStagesByProgramMember.containsKey(programMember.id)) {
                continue; //no parent stages for this program member
            }

            List<Program_Member_Stage_MVN__c> parentStages = parentStagesByProgramMember.get(programMember.id);

            //work backwards through the parent stage list and if the start stage is found set it to started along
            //with all children. Any stages (and children) after it is found should be closed.
            Boolean foundStage = false;
            for (Integer index = parentStages.size() - 1; index >= 0; index--) {
                Program_Member_Stage_MVN__c parentStage = parentStages[index];

                if (foundStage) {
                    closeProgramMemberStage(parentStage.Id);
                    for (Program_Member_Stage_MVN__c programMemberStage : parentStage.Program_Member_Stages__r) {
                        closeProgramMemberStage(programMemberStage.Id);
                    }
                }

                if (parentStage.Program_Stage_MVN__c == programMember.Starting_Program_Stage_ID_MVN__c) {
                    foundStage = true;
                    startProgramMemberStage(parentStage.Id);

                    for (Program_Member_Stage_Dependency_MVN__c dependency : parentStage.Program_Member_Stages_MVN__r) {
                        startProgramMemberStage(dependency.Program_Member_Stage_MVN__c);
                    }
                }
            }

            programMember.Starting_Program_Stage_ID_MVN__c = null;
        }

        startAtSpecificStage = true;
        update programMemberStagesToUpdate;

        update programMembers;
    }

    private static void startProgramMemberStage(Id programMemberStageId) {
        if (programMemberStagesById.containsKey(programMemberStageId)) {
            Program_Member_Stage_MVN__c programMemberStage = programMemberStagesById.get(programMemberStageId);
            programMemberStage.Status_MVN__c = settings.Program_Member_Stage_Start_Status_MVN__c;
            programMemberStage.Closed_Date_MVN__c = null;
            programMemberStage.IsClosed_MVN__c = false;

            programMemberStagesToUpdate.add(programMemberStage);
        }
    }

    private static void closeProgramMemberStage(Id programMemberStageId) {
        if (programMemberStagesById.containsKey(programMemberStageId)) {
            Program_Member_Stage_MVN__c programMemberStage = programMemberStagesById.get(programMemberStageId);
            programMemberStage.Status_MVN__c = settings.ProgramMS_Completed_Status_MVN__c;
            programMemberStage.Closed_Date_MVN__c = Date.today();
            programMemberStage.IsClosed_MVN__c = true;

            programMemberStagesToUpdate.add(programMemberStage);
        }
    }

    /**
     * Create data mappings for
     *     - parent stages (with children) grouped by program member and
     *     - all stages by id
     * @param programMembers : The list of program members in the current context
     */
    private static void createDataMapsFor(List<Program_Member_MVN__c> programMembers) {
        List<Program_Member_Stage_MVN__c> programMemberParentStages;
        programMemberParentStages = [SELECT Id,
                                            Program_Member_MVN__c,
                                            Status_MVN__c,
                                            Initiated_Date_MVN__c,
                                            Program_Stage_MVN__c,
                                            (SELECT Id,
                                                    Program_Member_Stage_MVN__c
                                               FROM Program_Member_Stages_MVN__r),
                                            (SELECT Id FROM Program_Member_Stages__r)
                                       FROM Program_Member_Stage_MVN__c
                                      WHERE Program_Member_MVN__c IN :programMembers
                                        AND Parent_Program_Member_Stage_MVN__c = null
                                   ORDER BY Stage_Sequence_Number_MVN__c, CreatedDate];

        parentStagesByProgramMember = new Map<Id, List<Program_Member_Stage_MVN__c>>();
        for (Program_Member_Stage_MVN__c programMemberParentStage : programMemberParentStages) {
            Id programMemberId = programMemberParentStage.Program_Member_MVN__c;

            if (!parentStagesByProgramMember.containsKey(programMemberId)) {
                parentStagesByProgramMember.put(programMemberId, new List<Program_Member_Stage_MVN__c>());
            }

            parentStagesByProgramMember.get(programMemberId).add(programMemberParentStage);
        }

        List<Program_Member_Stage_MVN__c> allPMStages;
        allPMStages = [SELECT Id,
                              Status_MVN__c,
                              Initiated_Date_MVN__c,
                              IsClosed_MVN__c,
                              Parent_Program_Member_Stage_MVN__r.Status_MVN__c,
                              (SELECT Id
                                 FROM Program_Member_Stages__r),
                              (SELECT Program_Member_Stage_MVN__c
                                 FROM Program_Member_Stage_Dependencies__r
                                WHERE Program_Member_Stage_Dependency_MVN__r.Parent_Program_Member_Stage_MVN__c != null
                                  AND Program_Member_Stage_Dependency_MVN__r.IsClosed_MVN__c = false)
                          FROM Program_Member_Stage_MVN__c
                         WHERE Program_Member_MVN__c IN :programMembers];

        programMemberStagesById = new Map<Id, Program_Member_Stage_MVN__c>(allPMStages);
    }
}