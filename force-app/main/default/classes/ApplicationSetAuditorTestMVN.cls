/**
 * @author Mavens
 * @description tests ApplicationSetAuditorMVN
 * @group Application
 */
@isTest private class ApplicationSetAuditorTestMVN {
    private static User caseManager;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;

    /**
    * @description setups data for all unit tests
    */
    static {
        TestDataFactoryMVN.createPSSettings();

        caseManager = TestDataFactoryMVN.generateCaseManager();
        System.runAs(caseManager) {
            program = TestDataFactoryMVN.createProgram();
            createStages();
            Account testAccount = TestDataFactoryMVN.createMember();
            programMember = TestDataFactoryMVN.createProgramMember(program, testAccount);
        }
        application = new Application_MVN__c(
            Program_Member_MVN__c = programMember.Id
        );
        insert application;

        programMember.Active_Application_MVN__c = application.Id;
        programMember.Program_Id_MVN__c = 'Test_Program_MVN';
        update programMember;
    }

    static void createStages() {
        // Single default stage generated when a Program is first created
        Program_Stage_MVN__c programInitiation = [select Id from Program_Stage_MVN__c];
        programInitiation.Name = 'Program Initiation';
        update programInitiation;
        Program_Stage_MVN__c welcomeCall = TestDataFactoryMVN.createChildProgramStage(program, programInitiation, 'Welcome Call');

        Program_Stage_MVN__c benefitsInvestigation = TestDataFactoryMVN.createParentProgramStage(program, 'Benefits Investigation');
        Program_Stage_MVN__c priorAuthorization = TestDataFactoryMVN.createChildProgramStage(program, benefitsInvestigation, 'Prior Authorization');
        benefitsInvestigation.Stage_Sequence_Number_MVN__c = 2;
        update benefitsInvestigation;

        Program_Stage_MVN__c education = TestDataFactoryMVN.createParentProgramStage(program, 'Education');
        Program_Stage_MVN__c nurseVisit = TestDataFactoryMVN.createChildProgramStage(program, education, 'Nurse Visit');
        education.Stage_Sequence_Number_MVN__c = 3;
        update education;
    }

    static Program_Member_Stage_MVN__c getProgramMemberStageByName(String stageName) {
        return [
            SELECT 
                Status_MVN__c,
                IsClosed_MVN__c,
                Activity_ID_MVN__c
            FROM 
                Program_Member_Stage_MVN__c 
            WHERE 
                Program_Member_MVN__c = :programMember.Id
            AND 
                Program_Stage_Name_MVN__c = :stageName
        ];
    }

    @isTest private static void applicationHasChangedButNoAuditNoAuditorSet() {
        TestFactoryActionEngineSettingsMVN.setMocks();

        Program_Member_Stage_MVN__c programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);

        // Prepare the Case to be closed
        Case caseToClose = new Case (
            Id = programMemberTest.Activity_ID_MVN__c,
            Exclude_from_Billing_MVN__c = true
        );
        update caseToClose;

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals('Not Started', programMemberTest.Status_MVN__c);

        System.runAs(caseManager) {
            Test.startTest();
                application.Status_MVN__c = 'Pending';
                update application;
            Test.stopTest();
        }

        Application_MVN__c actualApplication = [
            SELECT
                Id,
                Auditor_MVN__c
            FROM
                Application_MVN__c
            WHERE
                Id = :application.Id
        ];

        System.assertEquals(
            null,
            actualApplication.Auditor_MVN__c
        );

        programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals('Not Started', programMemberTest.Status_MVN__c);
    }

    @isTest private static void applicationHasChangedAWithAuditAndAuditorSetDifferenttoPass() {
        TestFactoryActionEngineSettingsMVN.setMocks();

        Program_Member_Stage_MVN__c programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);

        // Prepare the Case to be closed
        Case caseToClose = new Case (
            Id = programMemberTest.Activity_ID_MVN__c,
            Exclude_from_Billing_MVN__c = true
        );
        update caseToClose;

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals('Not Started', programMemberTest.Status_MVN__c);
        System.runAs(caseManager) {
            Test.startTest();
                application.Audit_Result_MVN__c = 'Fail';
                application.Audit_Failed_Reason_MVN__c = 'INCOME';
                update application;
            Test.stopTest();
        }

        Application_MVN__c actualApplication = [
            SELECT
                Id,
                Auditor_MVN__c
            FROM
                Application_MVN__c
            WHERE
                Id = :application.Id
        ];

        System.assertEquals(
            caseManager.Id,
            actualApplication.Auditor_MVN__c
        );

        programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals('Not Started', programMemberTest.Status_MVN__c);
    }

    @isTest private static void applicationHasChangedAWithAuditAndAuditorSetToPass() {
        TestFactoryActionEngineSettingsMVN.setMocks();

        Program_Member_Stage_MVN__c programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);

        // Prepare the Case to be closed
        Case caseToClose = new Case (
            Id = programMemberTest.Activity_ID_MVN__c,
            Exclude_from_Billing_MVN__c = true
        );
        update caseToClose;

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals('Not Started', programMemberTest.Status_MVN__c);

        System.runAs(caseManager) {
            Test.startTest();
                application.Audit_Result_MVN__c = Patient_Service_Settings_MVN__c.getInstance().Application_Audit_Result_Pass_Value_MVN__c;
                application.Audit_Failed_Reason_MVN__c = 'INCOME';
                update application;
            Test.stopTest();
        }

        programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(true, programMemberTest.IsClosed_MVN__c);

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);
    }

    @isTest private static void applicationAuditResultHasChangedToSystemCancelled() {
        TestFactoryActionEngineSettingsMVN.setMocks();

        System.runAs(caseManager) {
            Test.startTest();
                application.Audit_Result_MVN__c = Label.System_Cancelled_MVN;
                update application;
            Test.stopTest();
        }

        Application_MVN__c actualApplication = [
            SELECT
                Id,
                Auditor_MVN__c
            FROM
                Application_MVN__c
            WHERE
                Id = :application.Id
        ];

        System.assertEquals(
            null,
            actualApplication.Auditor_MVN__c
        );
    }

}