/**
 *	EnhancedAccountSearchUtilityMVN
 *	Created By:		Brian Aggen
 *	Created On:		2/4/2016
 *  Description:	This utility class implements the Account and Contact Information search used as part of the EnhancedAccountSearchMVN page
 *					It allows searching for Physicians, Patients, and Caregivers using the following criteria:
 *						* Last Name, First Name, Phone, Phone, Email, City, State, Postal Code, and Country
 *					It also allows searching for Business Accounts using the following criteria:
 *						* Business Name, External Id, Phone, City, Postal Code, State, Country
 *					The search offers an intuitive search order for both Person and Business Accounts
 *						* Person Accounts => Phone, Email, then Name/Address Information
 *						* Business Accounts => Phone, External Id, then Name/Address Information
 **/
public with sharing class EnhancedAccountSearchUtilityMVN implements EnhancedAccountSearchInterfaceMVN.searchInterface {
    private Account searchAccount;
    private Address_vod__c searchAddress;

    private Contact_Information_MVN__c searchContactInformation;
    private String searchAccountRecordTypeId;
    private List<String> searchAccountRecordTypeIdList;

    private Boolean isPersonAccount;
    private Boolean isEnrollmentSearch;

    private Boolean isEmailOrPhoneQuery = false;
    private String contactInfoOrderBy = '';

    private Enhanced_Account_Search_Settings_MVN__c searchSettings = Enhanced_Account_Search_Settings_MVN__c.getInstance();

    // Program member subquery only used in person  account searches
    private String programMemberSubQuery = ', (SELECT Id, Name, Program_MVN__r.Name, Active_Application_MVN__r.Status_MVN__c,Active_Application_MVN__r.Expiry_Date_MVN__c,Program_MVN__r.Application_Reminder_Days_MVN__c,Patient_Status_MVN__c FROM Program_Enrollments__r ORDER BY CreatedDate DESC)';

    
    // Map of all account record types by DeveloperName
    public static Map<String,String> acctRecordTypeMap {
        get {
            if(acctRecordTypeMap == null) {
                acctRecordTypeMap = new Map<String,String>();
                for(RecordType rt : [select Id, DeveloperName, Name from RecordType where SObjectType = 'Account']) {
                    acctRecordTypeMap.put(rt.DeveloperName,rt.Id);
                }
            }

            return acctRecordTypeMap;
        } set;
    }

    public Boolean hasCountry() {
        if(searchAddress == null) {
            return false;
        }

        return !String.isBlank(searchAddress.Country_vod__c);
    }

    public Boolean hasAddress() {
        if(searchAddress == null) {
            return false;
        }

        return !String.isBlank(searchAddress.City_vod__c)
                    || !String.isBlank(searchAddress.State_vod__c)
                    || !String.isBlank(searchAddress.Zip_vod__c);
    }

    public Boolean hasExternalId() {
        if(searchAccount == null) {
            return false;
        }

        return String.isNotBlank(searchAccount.External_Id_vod__c) || String.isNotBlank(searchAccount.Network_Id_MVN__c);
    }

    public Boolean hasPhone() {
        if(searchContactInformation == null) {
            return false;
        }

        return !String.isBlank(searchContactInformation.Phone_MVN__c);
    }

    public Boolean hasEmail() {
        if(searchContactInformation == null) {
            return false;
        }

        return !String.isBlank(searchContactInformation.Email_MVN__c);
    }

    public Boolean hasName() {
        if(searchAccount == null) {
            return false;
        }

        return !String.isBlank(searchAccount.Name)
                || !String.isBlank(searchAccount.FirstName)
                || !String.isBlank(searchAccount.LastName);
    }

    public List<EnhancedAccountSearchResultMockMVN> search(Account searchAcct, Address_vod__c searchAddr, Contact_Information_MVN__c searchContactInfo, String searchAcctRecordTypeId, Boolean isPersonAcctSearch, Boolean isEnrollmentPersonSearchSearch) {
        Boolean skipSearch = false;

        List<EnhancedAccountSearchResultMockMVN> results = new List<EnhancedAccountSearchResultMockMVN>();
        List<String> acctIds = new List<String>();

        String query = '';
        String queryLimit = (searchSettings.Account_Search_Max_Results_MVN__c != null) ? ' LIMIT ' + (Integer)searchSettings.Account_Search_Max_Results_MVN__c : ' LIMIT 200';


        searchAccount = searchAcct;
        searchAddress = searchAddr;
        searchContactInformation = searchContactInfo;
        searchAccountRecordTypeId = searchAcctRecordTypeId;
        isPersonAccount = isPersonAcctSearch;
        isEnrollmentSearch = isEnrollmentPersonSearchSearch;

        String acctWhereClause = buildAccountWhereClause();
        String contactInfoSelectStatement = buildContactInfoSelectStatement() + ' From Correspondences__r' ;
        String addressWhereClause = buildAddressWhereClause();
        String contactInfoWhereClause = buildContactInfoWhereClause();

        query += buildAccountSelectStatement();
        query += ',(' + contactInfoSelectStatement + addressWhereClause + contactInfoOrderBy+')';

        Set<String> accountIdsFromContactAddressQuery = null;

        if(hasAddress()
            || hasPhone()
            || hasEmail()
            || (hasName() && hasCountry())){
            accountIdsFromContactAddressQuery = searchContactInformation(contactInfoWhereClause);
        }

        if(isPersonAcctSearch) query += programMemberSubQuery;

        query += ' FROM Account';
        query += acctWhereClause;

        if ((accountIdsFromContactAddressQuery == null || accountIdsFromContactAddressQuery.size() == 0)
                && (((hasPhone() || hasEmail()) && (!hasName() && !hasExternalId())) || hasAddress())){
            skipSearch = true;
        } else {
            if((hasName() || hasExternalId()) && !hasAddress() && hasCountry()) {
                if(accountIdsFromContactAddressQuery != null && accountIdsFromContactAddressQuery.size() > 0) {
                    query += ' AND (Id IN ' + convertIdSetToString(accountIdsFromContactAddressQuery);
                    query += ' OR Country_MVN__c = \'' + searchAddress.Country_vod__c + '\')';
                }
            } else if (hasAddress()) {
                query += ' AND Id IN ' + convertIdSetToString(accountIdsFromContactAddressQuery);
            } else {
                if((hasPhone() || hasEmail()) && !hasAddress() && !hasName()) {
                    if(accountIdsFromContactAddressQuery.size() > 0) {
                        query += ' AND Id IN ' + convertIdSetToString(accountIdsFromContactAddressQuery);
                    }
                }
            }
        }

        query += queryLimit;
        query = query.replaceAll('\\*', '%');

        System.debug('### Account Query:### '+query);

        try {
            if(!skipSearch) {
                for(Account acct : Database.query(query)) {
                    EnhancedAccountSearchResultMockMVN e = new EnhancedAccountSearchResultMockMVN(acct, acct.Correspondences__r, acct.Program_Enrollments__r, false, null);

                    Set<String> countrySet = new Set<String>();
                    for(Contact_Information_MVN__c contactInfo : e.resultContactInfoAddrList) {
                        if(!String.isBlank(contactInfo.Address_MVN__r.Country_vod__c)) {
                            countrySet.add(contactInfo.Address_MVN__r.Country_vod__c);
                        }
                    }

                    if(String.IsBlank(searchAddress.Country_vod__c)
                        || (!String.isBlank(acct.Country_MVN__c) && acct.Country_MVN__c == searchAddress.Country_vod__c)
                        || countrySet.contains(searchAddress.Country_vod__c)) {
                        results.add(e);
                    }
                }
            }
        } catch(Exception e) {
            System.debug('\n\n#### Exception occurred during the EnhancedAccountSearchUtilityMVN account query: '+e.getMessage()+' \n');
        }

        return results;
    }

    /**
     * Searching Contact Information records
     * @return	Set<String>		Contact Information query results for Account IDs
     * @access	private
     */
    private Set<String> searchContactInformation(String contactInfoWhereClause) {
        Set<String> result = new Set<String>();

        // Query to avoid build semijoin query if contactInfoWhereClause is not blank
        if(String.isNotBlank(contactInfoWhereClause) && (hasAddress() || hasCountry() || hasPhone() || hasEmail())) {
            try {
                String contactQuery = 'SELECT Account_MVN__c FROM Contact_Information_MVN__c' + contactInfoWhereClause;

                System.debug('\n\n#### search() method: Contact_Information_MVN__c (for Address data) query is: '+ contactQuery +' \n');

                for(Contact_Information_MVN__c contact : Database.query(contactQuery)) {
                    result.add(contact.Account_MVN__c);
                }

                System.debug('\n\n#### search() method: Ids found: ' + result + '\n');
            } catch(Exception e) {
                System.debug('\n\n#### An error occurred during the EnhancedAccountSearchUtilityMVN contact/address query: '+e.getMessage()+' \n');
            }
        }

        return result;
    }

    /**
     * Building Account SELECT statement
     * @return	String		SOQL SELECT statement
     * @access	public
     */
    public String buildAccountSelectStatement() {
        Set<String> fields = new Set<String>();
        fields.add('Name');
        fields.add('FirstName');
        fields.add('LastName');
        fields.add('IsPersonAccount');
        fields.add('RecordTypeId');
        fields.add('RecordType.DeveloperName');
        fields.add('External_Id_vod__c');
        fields.add('Network_Id_MVN__c');
        fields.add('Country_MVN__c');

        Set<Schema.FieldSet> fieldsets = new Set<Schema.FieldSet>();
        fieldsets.add(SObjectType.Account.FieldSets.Person_Account_Search_Result_Fields_MVN);
        fieldsets.add(SObjectType.Account.FieldSets.Business_Acct_Search_Result_Fields_MVN);

        return buildSelectStatement(fields, fieldsets);
    }

    /**
     * Building Contact Information SELECT statement
     * @return	String		SOQL SELECT statement
     * @access	public
     */
    public String buildContactInfoSelectStatement() {
        Set<String> fields = new Set<String>();
        fields.add('Name');
        fields.add('RecordType.DeveloperName');
        fields.add('RecordTypeId');
        fields.add('Account_MVN__c');
        fields.add('External_ID_MVN__c');
        fields.add('Address_MVN__r.Country_vod__c');
        fields.add('Address_MVN__r.External_Id_vod__c');
        fields.add('Address_MVN__r.Network_Id_MVN__c');
        fields.add('Primary_MVN__c');

        Set<Schema.FieldSet> fieldsets = new Set<Schema.FieldSet>();
        fieldsets.add(SObjectType.Contact_Information_MVN__c.FieldSets.Contact_Info_Person_Acct_Src_Results_MVN);
        fieldsets.add(SObjectType.Contact_Information_MVN__c.FieldSets.Contact_Info_Bus_Acct_Search_Result_MVN);

        return buildSelectStatement(fields, fieldsets);
    }

    /**
     * Building generic SELECT statement
     * @param	Set<String>				Set of field paths to use
     * @param	Set<Schema.FieldSet>	Set of fieldsets to use
     * @return	String					SOQL SELECT statement
     * @access	public
     */
    public String buildSelectStatement(Set<String> fields, Set<Schema.FieldSet> fieldsets) {
        for(Schema.FieldSet fieldset : fieldsets) {
            for(Schema.FieldSetMember field : fieldset.getFields()) {
                fields.add(field.getFieldPath());
            }
        }

        System.debug('\n\n#### buildSelectStatement() returns: ' + 'SELECT ' + String.join(new List<String>(fields), ', ') + '\n');
        return 'SELECT ' + String.join(new List<String>(fields), ', ');
    }

    public String buildAddressWhereClause() {
        String addressWhereClause = '';

        if(isPersonAccount) {
            if(String.isBlank(addressWhereClause)) {
                addressWhereClause += buildWhereClause(SObjectType.Address_vod__c.FieldSets.Address_Person_Account_Search_Fields_MVN, searchAddress, 'Address_MVN__r');
            }
        } else {
            if(String.isBlank(addressWhereClause)) {
                addressWhereClause += buildWhereClause(SObjectType.Address_vod__c.FieldSets.Addr_Business_Account_Search_Fields_MVN, searchAddress, 'Address_MVN__r');
            }
        }

        if(!String.isBlank(addressWhereClause)) {
            return ' WHERE (' + addressWhereClause.replaceAll('\\*', '%')
                                + ' and RecordType.DeveloperName = \'' + searchSettings.Contact_Info_Address_Record_Type_MVN__c
                                + '\') or RecordType.DeveloperName != \'' + searchSettings.Contact_Info_Address_Record_Type_MVN__c + '\'';
        }

        return '';
    }

    /**
     * Builds the account where clause.
     * @return	String				WHERE clause (or empty string) without the WHERE itself
     * @access	private
     */
    public String buildAccountWhereClause() {
        String acctWhereClause = ' WHERE ';

        acctWhereClause += buildPartialAccountWhereClause(false);

        System.debug('\n\n#### buildAccountWhereClause() returns: ' + acctWhereClause + '\n');
        return acctWhereClause;
    }

    /**
     * Builds part of the account where clause, can also be used to create where clauses that are using an Account lookup field.
     * @param	Boolean				Is this from the Account object or will it be using a lookup.
     * @return	String				WHERE clause (or empty string) without the WHERE itself
     * @access	private
     */
    private String buildPartialAccountWhereClause(Boolean lookup) {
        return buildPartialAccountWhereClause(false, null);
    }

    /**
     * Builds part of the account where clause, can also be used to create where clauses that are using an Account lookup field.
     * @param	Boolean				Is this from the Account object or will it be using a lookup.
     * @param	SObject				If it's using a lookup, then what's the name of the lookup field (example: Account_MVN__r)
     * @return	String				WHERE clause (or empty string) without the WHERE itself
     * @access	private
     */
    private String buildPartialAccountWhereClause(Boolean lookup, String lookupField) {
        String acctWhereClause = '';

        if (lookup) {
            acctWhereClause += lookupField + '.IsPersonAccount = '+ isPersonAccount;
        } else {
            acctWhereClause += 'IsPersonAccount = '+ isPersonAccount;
        }

        searchAccountRecordTypeIdList = new List<String>();

        // if no account record type is specified by the user (aka All), then use the record types indicated in the searchSettings
        if(searchAccountRecordTypeId == '') {
            if (lookup) {
                acctWhereClause += ' AND ' + lookupField + '.RecordTypeId IN (';
            } else {
                acctWhereClause += ' AND RecordTypeId IN (';
            }

            List<String> personAcctSearchRecordTypeList;
            personAcctSearchRecordTypeList = (searchSettings.Search_Person_Account_Record_Types_MVN__c != null) ? searchSettings.Search_Person_Account_Record_Types_MVN__c.split(',') : new List<String>();

            List<String> businessAcctSearchRecordTypeList = (searchSettings.Search_Business_Account_Record_Types_MVN__c != null) ? searchSettings.Search_Business_Account_Record_Types_MVN__c.split(',') : new List<String>();

            // Add the specified Person Account Record Types
            // If person account record types don't apply to this search, the IsPersonAccount will filter accordingly
            for(Integer x = 0; x < personAcctSearchRecordTypeList.size(); x++) {
                String rtDevName = personAcctSearchRecordTypeList[x];
                if(acctRecordTypeMap.containsKey(rtDevName)) {
                    acctWhereClause += '\''+acctRecordTypeMap.get(rtDevName)+'\'';
                    searchAccountRecordTypeIdList.add(acctRecordTypeMap.get(rtDevName));

                    if(x < personAcctSearchRecordTypeList.size() - 1) {
                        acctWhereClause += ',';
                    }
                }
            }
            // Add the business account record types
            // If business account record types don't apply to this search, the IsPersonAccount will filter accordingly
            Boolean hasPersonAcctRecordTypes = (personAcctSearchRecordTypeList.size() > 0) ? true : false;

            for(Integer x = 0; x < businessAcctSearchRecordTypeList.size(); x++) {
                String rtDevName = businessAcctSearchRecordTypeList[x];
                if(acctRecordTypeMap.containsKey(rtDevName)) {
                    if(hasPersonAcctRecordTypes) {
                        acctWhereClause += ',';
                        hasPersonAcctRecordTypes = false;
                    }
                    acctWhereClause += '\''+acctRecordTypeMap.get(rtDevName)+'\'';
                    searchAccountRecordTypeIdList.add(acctRecordTypeMap.get(rtDevName));

                    if(x < businessAcctSearchRecordTypeList.size() - 1) {
                        acctWhereClause += ',';
                    }
                }
            }
            acctWhereClause += ')';
        } else {
            if (lookup) {
                acctWhereClause += ' AND ' + lookupField + '.RecordTypeId = \''+searchAccountRecordTypeId+'\'';
            } else {
                acctWhereClause += ' AND RecordTypeId = \''+searchAccountRecordTypeId+'\'';
            }
        }

        // Add first name and last name if specified
        if(!String.isBlank(searchAccount.FirstName)) {
            if (lookup) {
                acctWhereClause += ' AND ' + lookupField + '.FirstName LIKE \''+String.escapeSingleQuotes(searchAccount.FirstName.trim())+'\'';
            } else {
                acctWhereClause += ' AND FirstName LIKE \''+String.escapeSingleQuotes(searchAccount.FirstName.trim())+'\'';
            }
        }
        if(!String.isBlank(searchAccount.LastName)) {
            if (lookup) {
                acctWhereClause += ' AND ' + lookupField + '.LastName LIKE \''+String.escapeSingleQuotes(searchAccount.LastName.trim())+'\'';
            } else {
                acctWhereClause += ' AND LastName LIKE \''+String.escapeSingleQuotes(searchAccount.LastName.trim())+'\'';
            }
        }
        if(!String.isBlank(searchAccount.Name)) {
            if (lookup) {
                acctWhereClause += ' AND ' + lookupField + '.Name LIKE \''+String.escapeSingleQuotes(searchAccount.Name.trim())+'\'';
            } else {
                acctWhereClause += ' AND Name LIKE \''+String.escapeSingleQuotes(searchAccount.Name.trim())+'\'';
            }
        }

        String whereClause = '';

        if(isPersonAccount) {
            // Add person account search fields if specified
            if (lookup) {
                whereClause = buildWhereClause(SObjectType.Account.FieldSets.Person_Account_Search_Fields_MVN, searchAccount, lookupField);
                acctWhereClause += (whereClause.length() > 0 ? ' AND ' : '') + whereClause;
            } else {
                whereClause = buildWhereClause(SObjectType.Account.FieldSets.Person_Account_Search_Fields_MVN, searchAccount);
                acctWhereClause += (whereClause.length() > 0 ? ' AND ' : '') + whereClause;
            }
        } else {
            // Add business account search fields if specified
            if (lookup) {
                acctWhereClause += buildWhereClause(SObjectType.Account.FieldSets.Business_Account_Search_Fields_MVN, searchAccount, lookupField);
            } else {
                acctWhereClause += buildWhereClause(SObjectType.Account.FieldSets.Business_Account_Search_Fields_MVN, searchAccount);
            }
        }

        return acctWhereClause;
    }

    /**
     * Building a generic WHERE SOQL clause
     * @param	Schema.FieldSet		Fieldset to use to generate WHERE clause
     * @param	SObject				SObject to get values from
     * @return	String				WHERE clause (or empty string)
     * @access	public
     */
    public String buildWhereClause(Schema.Fieldset fieldset, SObject data) {
        return buildWhereClause(fieldset, data, null);
    }

    /**
     * Building a generic WHERE SOQL clause
     * @param	Schema.FieldSet		Fieldset to use to generate WHERE clause
     * @param	SObject				SObject to get values from
     * @param 	String				Prefix to prepend fields with
     * @return	String				WHERE clause (or empty string)
     * @access	public
     */
    public String buildWhereClause(Schema.FieldSet fieldset, SObject data, String prefix) {
        List<Schema.FieldSetMember> fields = fieldset.getFields();
        List<String> clauses = new List<String>();

        prefix = String.isBlank(prefix) ? '' : prefix + '.';

        for (Schema.FieldSetMember field : fields) {
            Schema.DisplayType fieldType = field.getType();
            String fieldPath = String.escapeSingleQuotes(prefix + field.getFieldPath());

            try {
                String value = String.valueOf(data.get(field.getFieldPath()));
                if (!String.isBlank(value)) {
                    // If an External Id is part of the search criteria for the Business Account and
                    // the Name and Address are not specified then only the External Id is used to search
                    if(field.getFieldPath() == 'External_Id_vod__c') {
                        if(!hasName() && !hasAddress()) {
                            return (' AND External_Id_vod__c LIKE \'' + String.escapeSingleQuotes(searchAccount.External_Id_vod__c) + '\'').replaceAll('\\*', '%');
                        } else {
                            continue;
                        }
                    } else if(field.getFieldPath() == 'Network_Id_MVN__c') {
                        if(!hasName() && !hasAddress()) {
                            return (' AND Network_Id_MVN__c LIKE \'' + String.escapeSingleQuotes(searchAccount.Network_Id_MVN__c) + '\'').replaceAll('\\*', '%');
                        } else {
                            continue;
                        }
                    }

                    /*	Adding new filter logic
                        If there is phone and nothing else -> we add phone filter
                        If there is email and phone -> we add only  the email
                        If there is any other field -> no phone or email should be added  */
                    if (fieldType == Schema.DisplayType.PHONE
                         && !hasAddress()
                         && !hasName()
                         && hasPhone()
                         && !hasEmail()
                         && !hasExternalId()) {
                        value = value.replaceAll('[^0-9]', '');

                        String newPhoneValue = '%';
                        for (String s : value.split('')) {
                            newPhoneValue += s + '%';
                        }

                        clauses.add(fieldPath + ' LIKE \'' + String.escapeSingleQuotes(newPhoneValue) + '\'');
                    } else if (fieldType == Schema.DisplayType.DATE) {
                        clauses.add(fieldPath + ' = ' + String.escapeSingleQuotes(value.substring(0, 10)));
                    } else if (fieldType == Schema.DisplayType.PICKLIST) {
                        clauses.add(fieldPath + ' = \'' + String.escapeSingleQuotes(value.trim()) + '\'');
                    } else if(fieldType == Schema.DisplayType.EMAIL
                         && !hasAddress()
                         && !hasName()
                         && hasEmail()) {
                        clauses.add(fieldPath + ' LIKE \'' + String.escapeSingleQuotes(value.trim()) + '\'');
                    } else if (fieldType != Schema.DisplayType.EMAIL && fieldType != Schema.DisplayType.PHONE) {
                        clauses.add(fieldPath + ' LIKE \'' + String.escapeSingleQuotes(value.trim()) + '\'');
                    }
                }
            } catch (Exception e) {
                System.debug('\n\n#### An exception occurred while adding an Account filter: ' + e.getMessage() + '\n');
            }
        }

        System.debug('\n\n#### buildWhereClause() returns: ' + String.join(clauses, ' AND ').replaceAll('\\*', '%') + '\n');
        return String.join(clauses, ' AND ').replaceAll('\\*', '%');
    }

    public String buildContactInfoWhereClause() {
        String contactInfoWhereClause = '';

        if(isPersonAccount) {
            contactInfoWhereClause += buildWhereClause(SObjectType.Contact_Information_MVN__c.FieldSets.Contact_Info_Person_Acct_Src_Fields_MVN, searchContactInformation);

            if(String.isBlank(contactInfoWhereClause)) {
                contactInfoWhereClause += buildWhereClause(SObjectType.Address_vod__c.FieldSets.Address_Person_Account_Search_Fields_MVN, searchAddress, 'Address_MVN__r');
            }
        } else {
            contactInfoWhereClause += buildWhereClause(SObjectType.Contact_Information_MVN__c.FieldSets.Contact_Info_Bus_Acct_Search_Field_MVN, searchContactInformation);

            if(String.isBlank(contactInfoWhereClause)) {
                contactInfoWhereClause += buildWhereClause(SObjectType.Address_vod__c.FieldSets.Addr_Business_Account_Search_Fields_MVN, searchAddress, 'Address_MVN__r');
            }
        }

        // If any contact info search filters are part of the search, then add the WHERE statement to the front of the string
        if(String.isNotBlank(contactInfoWhereClause)) {
            contactInfoWhereClause = ' WHERE ' + contactInfoWhereClause;
            contactInfoWhereClause += ' AND ' + buildPartialAccountWhereClause(true, 'Account_MVN__r');
            contactInfoOrderBy = ' ORDER BY Primary_MVN__c DESC';
        }

        return contactInfoWhereClause.replaceAll('\\*', '%');
    }

    // Convert a Set<String> into a comma separated String literal for inclusion in a dynamic SOQL Query
    private String convertIdSetToString(Set<String> mapKeySet) {
        String newSetStr = '' ;
        for(String str : mapKeySet) {
            newSetStr += '\'' + str + '\',';
        }

        newSetStr = newSetStr.lastIndexOf(',') > 0 ? '(' + newSetStr.substring(0,newSetStr.lastIndexOf(',')) + ')' : newSetStr;

        return newSetStr;
    }
}