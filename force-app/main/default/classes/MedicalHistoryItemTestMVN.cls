/*
*   MedicalHistoryControllerMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 16, 2016
*   Description:    Test class for the MedicalHistoryItemMVN class.
*/
@isTest public class MedicalHistoryItemTestMVN {
    @testSetup static void setupData() {
        TestDataFactoryMVN.createPSSettings();
        Account member = TestDataFactoryMVN.createMember();
        Program_MVN__c program = TestDataFactoryMVN.createProgram();

        Medical_History_Type_MVN__c type = new Medical_History_Type_MVN__c(
            Record_Type_Name_MVN__c = 'Allergies_MVN',
            Program_MVN__c = program.Id,
            Order_MVN__c = 1
        );
        insert type;

        Program_Member_MVN__c progMember = TestDataFactoryMVN.createProgramMember(program, member);
        RecordType recordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Medical_History_MVN__c' AND DeveloperName = 'Allergies_MVN'];

        Medical_History_MVN__c history = new Medical_History_MVN__c(
            RecordType = recordType,
            RecordTypeId = recordType.Id,
            Program_Member_MVN__c = progMember.Id
        );
        insert history;
    }

    @isTest static void test_Constructor_IsInitiated() {
        Medical_History_MVN__c expectedMedicalHistory = [SELECT Id, Program_Member_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Medical_History_MVN__c LIMIT 1];

        Test.startTest();
            MedicalHistoryItemMVN actualMedicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);
        Test.stopTest();

        assertMedicalHistoryItemMVN(expectedMedicalHistory, actualMedicalHistory);
        System.assertEquals(false, actualMedicalHistory.isEditMode);
    }

    @isTest static void test_ChangeEditMode_IsChanged() {
        Medical_History_MVN__c expectedMedicalHistory = [SELECT Id, Program_Member_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Medical_History_MVN__c LIMIT 1];
        MedicalHistoryItemMVN actualMedicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);
        System.assertEquals(false, actualMedicalHistory.isEditMode);

        Test.startTest();
            actualMedicalHistory.changeEditMode();
        Test.stopTest();

        System.assertEquals(true, actualMedicalHistory.isEditMode);
    }

    public static void assertMedicalHistoryItemMVN(Medical_History_MVN__c expectedMedicalHistory, MedicalHistoryItemMVN actualMedicalHistory) {
        System.assertEquals(expectedMedicalHistory.Id, actualMedicalHistory.item.Id);
        System.assertEquals(expectedMedicalHistory.RecordTypeId, actualMedicalHistory.item.RecordTypeId);
        System.assertEquals(expectedMedicalHistory.RecordType.DeveloperName, actualMedicalHistory.item.RecordType.DeveloperName);
        System.assertEquals(expectedMedicalHistory.Program_Member_MVN__c, actualMedicalHistory.item.Program_Member_MVN__c);
    }
}