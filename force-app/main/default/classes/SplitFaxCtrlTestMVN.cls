/**
 * @author Mavens
 * @date 07/04/2018
 * @description Tests for SplitFaxCtrlMVN
 */ 
@isTest
private class SplitFaxCtrlTestMVN {

    static Case testCase;
    static User caseManager;
    static Id testDocId;

    static {
        createCaseManager();
        createTestCase();
        createAttachment();
    }

    static void createCaseManager() {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        insert caseManager;
        TestFactoryUserMVN userFactory = new TestFactoryUserMVN();
        userFactory.setUserPermissionSet(caseManager.Id, 'DDP_User');
        userFactory.setUserPermissionSet(caseManager.Id, 'DDP_User_All');
    }

    static void createTestCase() {
        System.runAs(caseManager) {
            TestFactoryCaseMVN caseFactory = new TestFactoryCaseMVN();
            testCase = caseFactory.create(
                new Map<String, Object> {
                    'RecordTypeId' => SObjectType.Case.getRecordTypeInfosByName().get('Request').getRecordTypeId(),
                    'Status' => 'Test',
                    'Subject' => 'My Request'
                }
            );
        }
    }

    static void createAttachment() {
        System.runAs(caseManager) {
            List<Attachment> attachmentList = new List<Attachment>();
            attachmentList.add(new Attachment(
                Name = 'TestName1',
                Body = Blob.valueOf('TestBody1'),
                ParentId = testCase.Id
            ));
            attachmentList.add(new Attachment(
                Name = 'TestName2',
                Body = Blob.valueOf('TestBody2'),
                ParentId = testCase.Id
            ));
            insert attachmentList;
        }
    }

    @isTest
    static void splitFaxErrorNoFilesTest() {

        System.runAs(caseManager) {
            TestFactoryCaseMVN caseFactory = new TestFactoryCaseMVN();
            Case testCaseNoFiles = caseFactory.create(
                new Map<String, Object> {
                    'RecordTypeId' => SObjectType.Case.getRecordTypeInfosByName().get('Request').getRecordTypeId(),
                    'Status' => 'Test',
                    'Subject' => 'My Request'
                }
            );

            Test.startTest();
            ApexPages.StandardController standardController = new ApexPages.StandardController(testCaseNoFiles);
            SplitFaxCtrlMVN controller = new SplitFaxCtrlMVN(standardController);
            Test.stopTest();
            
            ApexPages.Message msg = ApexPages.getMessages().get(0);
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
            System.assertEquals(Label.No_Files_Found_MVN, msg.getDetail());
            System.assert(controller.error);
        }
    }

    @isTest
    static void buildDocPicklistTest() {
        System.runAs(caseManager) {
            Test.startTest();
            ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
            SplitFaxCtrlMVN controller = new SplitFaxCtrlMVN(standardController);
            Test.stopTest();

            System.assertEquals(2, controller.documentOptions.size());            
            System.assertEquals('TestName1', controller.documentOptions.get(0).getLabel());            
            System.assertEquals('TestName2', controller.documentOptions.get(1).getLabel());            
        }
    }

    @isTest
    static void goToSplitScreenTest() {

        System.runAs(caseManager) {
            ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
            SplitFaxCtrlMVN controller = new SplitFaxCtrlMVN(standardController);
            controller.selectedDoc = controller.documentOptions.get(0).getValue();

            Test.startTest();
            controller.showSplitScreen();
            Test.stopTest();

            ContentVersion cv = [
                SELECT Id, ContentDocumentId, Title
                FROM ContentVersion
                WHERE FirstPublishLocationId = :controller.selectedDoc
            ];

            System.assert(cv != null);
            System.assertEquals(cv.Id, controller.documentId);
            System.assert(!controller.inProgress);
            System.assert(!controller.error);
            System.assertEquals(1, controller.splitDocumentList.size());
        }
    }

    @isTest
    static void splitFaxAddNewRowRemoveRowTest() {

        System.runAs(caseManager) {
            ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
            SplitFaxCtrlMVN controller = new SplitFaxCtrlMVN(standardController);
            controller.selectedDoc = controller.documentOptions.get(0).getValue();
            controller.showSplitScreen();

            Test.startTest();
            controller.addNewRow();
            controller.addNewRow();
            Test.stopTest();

            System.assertEquals(3, controller.splitDocumentList.size());
            System.assertEquals(1, controller.splitDocumentList.get(1).index);
            System.assertEquals(2, controller.splitDocumentList.get(2).index);

            controller.rowToRemove = 1;
            controller.removeRow();

            System.assertEquals(2, controller.splitDocumentList.size());
        }
    }

    @isTest
    static void splitFaxSplitDocumentEmptyRowErrorTest() {

        System.runAs(caseManager) {
            ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
            SplitFaxCtrlMVN controller = new SplitFaxCtrlMVN(standardController);
            controller.selectedDoc = controller.documentOptions.get(0).getValue();
            controller.showSplitScreen();

            Test.startTest();
            controller.generateDocuments();
            Test.stopTest();

            String errorMsg = Label.Split_Document_Fix_The_Errors_MVN + ':<br/>' +
                            '  * ' + Label.Split_Document_Blank_Field_Error_MVN + '<br/>';

            ApexPages.Message msg = ApexPages.getMessages().get(0);
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
            System.assertEquals(errorMsg, msg.getDetail());

            System.assertEquals(1, controller.splitDocumentList.size());
            System.assert(controller.splitDocumentList.get(0).startPageError);
            System.assert(controller.splitDocumentList.get(0).endPageError);
            System.assert(controller.splitDocumentList.get(0).docNameError);
        }
    }

    @isTest
    static void splitFaxSplitDocumentMultipleErrorsTest() {

        System.runAs(caseManager) {
            ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
            SplitFaxCtrlMVN controller = new SplitFaxCtrlMVN(standardController);
            controller.selectedDoc = controller.documentOptions.get(0).getValue();
            controller.showSplitScreen();
            controller.addNewRow();
            controller.addNewRow();
            controller.addNewRow();
            controller.addNewRow();

            System.assertEquals(5, controller.splitDocumentList.size());

            // no docWrapper0.docName => missing mandatory field error
            SplitFaxCtrlMVN.SplitDocumentWrapper docWrapper0 = controller.splitDocumentList.get(0);
            docWrapper0.startPage = 1;
            docWrapper0.endPage = 4;

            // Empty row => will be removed
            SplitFaxCtrlMVN.SplitDocumentWrapper docWrapper1 = controller.splitDocumentList.get(1);

            // Negative values => Wrong values error
            SplitFaxCtrlMVN.SplitDocumentWrapper docWrapper2 = controller.splitDocumentList.get(2);
            docWrapper2.startPage = -1;
            docWrapper2.endPage = -3;
            docWrapper2.docName = 'test';

            // endPage < startPage => page number error
            SplitFaxCtrlMVN.SplitDocumentWrapper docWrapper3 = controller.splitDocumentList.get(3);
            docWrapper3.startPage = 3;
            docWrapper3.endPage = 1;
            docWrapper3.docName = 'test';

            // no startPage, no endPage => missing mandatory field error
            SplitFaxCtrlMVN.SplitDocumentWrapper docWrapper4 = controller.splitDocumentList.get(4);
            docWrapper4.docName = 'test';

            Test.startTest();
            controller.generateDocuments();
            Test.stopTest();

            String errorMsg = Label.Split_Document_Fix_The_Errors_MVN + ':<br/>' +
                            '  * ' + Label.Split_Document_Blank_Field_Error_MVN + '<br/>' +
                            '  * ' + Label.Split_Document_Page_Number_Error_MVN + '<br/>' + 
                            '  * ' + Label.Split_Document_Wrong_Value_Error_MVN + '<br/>';

            ApexPages.Message msg = ApexPages.getMessages().get(0);
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
            System.assertEquals(errorMsg, msg.getDetail());

            System.assertEquals(4, controller.splitDocumentList.size());
            System.assert(controller.splitDocumentList.get(0).docNameError);
            System.assert(controller.splitDocumentList.get(1).startPageError);
            System.assert(controller.splitDocumentList.get(1).endPageError);
            System.assert(controller.splitDocumentList.get(2).startPageError);
            System.assert(controller.splitDocumentList.get(2).endPageError);
            System.assert(controller.splitDocumentList.get(3).startPageError);
            System.assert(controller.splitDocumentList.get(3).endPageError);

            // Reset errors
            controller.addNewRow();

            System.assertEquals(5, controller.splitDocumentList.size());
            System.assert(!controller.splitDocumentList.get(0).docNameError);
            System.assert(!controller.splitDocumentList.get(1).startPageError);
            System.assert(!controller.splitDocumentList.get(2).startPageError);
            System.assert(!controller.splitDocumentList.get(2).endPageError);

        }
    }

    @isTest
    static void splitFaxSplitDocumentSuccessTest() {

        System.runAs(caseManager) {
            ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
            SplitFaxCtrlMVN controller = new SplitFaxCtrlMVN(standardController);
            controller.selectedDoc = controller.documentOptions.get(0).getValue();
            controller.showSplitScreen();
            controller.addNewRow();

            SplitFaxCtrlMVN.SplitDocumentWrapper docWrapper0 = controller.splitDocumentList.get(0);
            docWrapper0.startPage = 1;
            docWrapper0.endPage = 4;
            docWrapper0.docName = 'test split doc 1';

            SplitFaxCtrlMVN.SplitDocumentWrapper docWrapper1 = controller.splitDocumentList.get(1);
            docWrapper1.startPage = 5;
            docWrapper1.endPage = 5;
            docWrapper1.docName = 'test split doc 2';

            Test.startTest();
            controller.generateDocuments();
            Test.stopTest();

            System.assert(controller.isChildCasesPanelVisible);
            System.assert(controller.isNintexPanelVisible);
            System.assert(controller.inProgress);

            List<Case> childCases = [
                SELECT RecordTypeId, Origin
                FROM Case
                WHERE ParentId = :testCase.Id
            ];

            System.assertEquals(2, childCases.size());
            System.assertEquals(testCase.RecordTypeId, childCases.get(0).RecordTypeId);
            System.assertEquals(testCase.RecordTypeId, childCases.get(1).RecordTypeId);
            System.assertEquals(testCase.Origin, childCases.get(0).Origin);
            System.assertEquals(testCase.Origin, childCases.get(1).Origin);
        }
    }

    @isTest
    static void splitFaxOnCompleteCallbackTest() {

        System.runAs(caseManager) {
            ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
            SplitFaxCtrlMVN controller = new SplitFaxCtrlMVN(standardController);
            controller.selectedDoc = controller.documentOptions.get(0).getValue();
            controller.showSplitScreen();

            SplitFaxCtrlMVN.SplitDocumentWrapper docWrapper0 = controller.splitDocumentList.get(0);
            docWrapper0.startPage = 1;
            docWrapper0.endPage = 4;
            docWrapper0.docName = 'test split doc';

            controller.generateDocuments();

            // Link file to child Case
            List<Case> childCases = [
                SELECT Due_Date_MVN__c, RecordTypeId, Subject, caseNumber
                FROM Case
                WHERE ParentId = :testCase.Id
            ];
            Case childCase = childCases.get(0);

            Attachment att = new Attachment(
                Name = 'TestChildName1',
                Body = Blob.valueOf('TestChildBody1'),
                ParentId = childCase.Id
            );
            insert att;

            Test.startTest();
            controller.returnUri = 'https://test.salesforce.com/' + childCase.Id;
            controller.fileName = 'test split doc';
            controller.onCompleteNintexCallbackApex();
            Test.stopTest();

            System.assertEquals(1, controller.childCases.size());
            SplitFaxCtrlMVN.ResultCaseWrapper caseWrapper = controller.childCases.get(0);
            System.assertEquals('https://test.salesforce.com/' + childCase.Id, caseWrapper.caseUrl);
            System.assertEquals('test split doc', caseWrapper.fileName);
            System.assertEquals(childCase.caseNumber, caseWrapper.caseNumber);

            Document_MVN__c doc = [
                SELECT Attachment_Id_MVN__c
                FROM Document_MVN__c
                WHERE Case_MVN__c = :childCase.Id
            ];
            System.assertEquals('/servlet/servlet.FileDownload?file=' + doc.Attachment_Id_MVN__c, caseWrapper.fileUrl);

            Case theCase = [SELECT Status FROM Case WHERE Id = :testCase.Id];
            System.assertEquals('Cancelled', theCase.Status);
        }
    }

    @isTest
    static void splitFaxOnErrorCallbackTest() {
        System.runAs(caseManager) {
            ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
            SplitFaxCtrlMVN controller = new SplitFaxCtrlMVN(standardController);

            Test.startTest();
            controller.nintexErrorMsg = 'test nintex error';
            controller.onErrorNintexCallbackApex();
            Test.stopTest();

            ApexPages.Message msg = ApexPages.getMessages().get(0);
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
            System.assertEquals('test nintex error', msg.getDetail());
        }
    }

    @isTest
    static void backButtonTest() {
        System.runAs(caseManager) {
            ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
            SplitFaxCtrlMVN controller = new SplitFaxCtrlMVN(standardController);
            controller.selectedDoc = controller.documentOptions.get(0).getValue();
            controller.showSplitScreen();

            System.assertEquals('split', controller.step);

            Test.startTest();
            controller.back();
            Test.stopTest();

            System.assertEquals('selection', controller.step);
        }
    }
}