/*
 * ContactInfoSetValueFieldTriggerTestMVN
 * Create By: Kyle Thornton
 * Created Date: 4/10/2017
 * Description: Unit tests for ContactInfoSetValueFieldTriggerTestMVN
 */
@isTest
private class ContactInfoSetValueFieldTriggerTestMVN
{
	private static final Integer DATA_SIZE = 200;
    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();

    static Account prescriber;
    static List<Address_vod__c> addresses;
    static List<Contact_Information_MVN__c> contactInformation;

    static {
        prescriber = accountFactory.createPrescriber(null);
    }

    @isTest
	static void itShouldSetValueToTheTheAddress() {
		givenAListOfAddresses();
        thenTheCIValueFieldIsSetToTheAddress();
	}

    private static void givenAListOfAddresses() {
        addresses = new List<Address_vod__c>();
        for (Integer i=0; i<DATA_SIZE; i++) {
            addresses.add(
                new Address_vod__c(
                    Account_vod__c        = prescriber.id,
                    Name                  = i + ' Main St.',
                    Address_Line_2_vod__c = 'Suite ' + i,
                    City_vod__c           = 'Boston',
                    State_vod__c          = 'MA',
                    Zip_vod__c            = '02108',
                    Country_vod__c        = 'US'
                )
            );
        }
        insert addresses;
    }

    private static Id getCiRecordTypeId(String developerName) {
        return [SELECT Id
                  FROM RecordType
                 WHERE SObjectType = 'Contact_Information_MVN__c'
                   AND DeveloperName = :developerName
                 LIMIT 1].Id;
    }

    private static void thenTheCIValueFieldIsSetToTheAddress() {
        contactInformation = [SELECT Id,
                                     Value_MVN__c,
                                     Address_MVN__r.Name,
                                     Address_MVN__r.Address_Line_2_vod__c,
                                     Address_MVN__r.City_vod__c,
                                     Address_MVN__r.State_vod__c,
                                     Address_MVN__r.Zip_vod__c,
                                     Address_MVN__r.Country_vod__c
                                FROM Contact_Information_MVN__c
                               WHERE Address_MVN__c IN :addresses];

        System.assertEquals(DATA_SIZE, contactInformation.size());
        for (Contact_Information_MVN__c ci : contactInformation) {
            String expectedAddress = ci.Address_MVN__r.Name + '\n' +
                                     ci.Address_MVN__r.Address_Line_2_vod__c + '\n' +
                                     ci.Address_MVN__r.City_vod__c + ', ' +
                                     ci.Address_MVN__r.State_vod__c + ' ' +
                                     ci.Address_MVN__r.Zip_vod__c + ' ' +
                                     ci.Address_MVN__r.Country_vod__c;
            System.assertEquals(expectedAddress, ci.Value_MVN__c);
        }
    }

    @isTest
    static void itShouldCopyEmailToTheValueField() {
        givenAListOfContactInfoWithEmailPopulated();
        whenContactInfoIsInserted();
        thenEmailIsCopiedToValueField();
    }

    private static void givenAListOfContactInfoWithEmailPopulated() {
        contactInformation = new List<Contact_Information_MVN__c>();
        Id emailRecTypeId = getCiRecordTypeId('Email_MVN');

        for (Integer i=0; i<DATA_SIZE; i++) {
            contactInformation.add(
                new Contact_Information_MVN__c(
                    RecordTypeId   = emailRecTypeId,
                    Account_MVN__c = prescriber.Id,
                    Email_MVN__c   = 'test' + i + '@email.com'
                )
            );
        }
    }

    private static void whenContactInfoIsInserted() {
        Test.startTest();
        insert contactInformation;
        Test.stopTest();
    }

    private static void thenEmailIsCopiedToValueField() {
        contactInformation = [SELECT Id,
                                     Email_MVN__c,
                                     Value_MVN__c
                                FROM Contact_Information_MVN__c
                               WHERE Id IN :contactInformation];
        System.assertEquals(DATA_SIZE, contactInformation.size());
        for (Contact_Information_MVN__c ci :contactInformation) {
            System.assertEquals(ci.Email_MVN__c, ci.Value_MVN__c);
        }
    }

     @isTest
    static void itShouldCopyPhoneToTheValueField() {
        givenAListOfContactInfoWithPhonePopulated();
        whenContactInfoIsInserted();
        thenPhoneIsCopiedToValueField();
    }

    private static void givenAListOfContactInfoWithPhonePopulated() {
        contactInformation = new List<Contact_Information_MVN__c>();
        Id phoneRecTypeId = getCiRecordTypeId('Phone_MVN');

        for (Integer i=0; i<DATA_SIZE; i++) {
            contactInformation.add(
                new Contact_Information_MVN__c(
                    RecordTypeId   = phoneRecTypeId,
                    Account_MVN__c = prescriber.Id,
                    Phone_MVN__c   = '5554446677'
                )
            );
        }
    }

    private static void thenPhoneIsCopiedToValueField() {
        contactInformation = [SELECT Id,
                                     Phone_MVN__c,
                                     Value_MVN__c
                                FROM Contact_Information_MVN__c
                               WHERE Id IN :contactInformation];
        System.assertEquals(DATA_SIZE, contactInformation.size());
        for (Contact_Information_MVN__c ci :contactInformation) {
            System.assertEquals(ci.Phone_MVN__c, ci.Value_MVN__c);
        }
    }
}