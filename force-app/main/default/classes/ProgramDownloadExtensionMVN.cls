/*
*   ProgramDownloadExtensionMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 21st, 2017
*   Description:    extension for ProgramDownloadMVN page
*                   - collect program and all related records
*                   - convert to JSON for download
*/
public with sharing class ProgramDownloadExtensionMVN {
    /**
    * @description external Id for the program
    */
    public String programExternalId { get; set; }

    /**
    * @description program and all related records to be downloaded
    */
    @testVisible private ProgramDeployWrapperMVN programToDownload;

    /**
    * @description
    */
    public String programJson {
        get {
            return programToDownload.toJson();
        }
    }

    /**
    * @description constructor
    *              - use stdController to collect program and all related records
    * @param ApexPages.StandardController stdController
    */
    public ProgramDownloadExtensionMVN(ApexPages.StandardController stdController) {
        this.programExternalId = [SELECT Program_ID_MVN__c
                                    FROM Program_MVN__c
                                   WHERE Id =: stdController.getRecord().Id].Program_ID_MVN__c;
        this.programToDownload = new ProgramDeployWrapperMVN(this.programExternalId);
    }
}