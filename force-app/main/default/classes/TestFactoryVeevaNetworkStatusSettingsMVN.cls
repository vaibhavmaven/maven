/**
* @author Mavens
* @group Network API
* @description This class is used to construct Veeva Network Settings custom metadatas
*/
@isTest
public with sharing class TestFactoryVeevaNetworkStatusSettingsMVN {

    private static Map<String, Object> myFieldValues;

    public static void setMock() {
        setMock(null);
    }

    public static void setMock(Map<String, Object> fieldValues) {
        myFieldValues = fieldValues;
        List<Veeva_Network_Status_Settings_MVN__mdt> settings = new List<Veeva_Network_Status_Settings_MVN__mdt>();

        settings.add(getAcceptedDCR(fieldValues));
        settings.add(getAcceptedField(fieldValues));
        settings.add(getAddedField(fieldValues));
        settings.add(getAlreadyAppliedField(fieldValues));
        settings.add(getCancelled(fieldValues));
        settings.add(getChangeNew(fieldValues));
        settings.add(getInqueue(fieldValues));
        settings.add(getModifiedField(fieldValues));
        settings.add(getPartial(fieldValues));
        settings.add(getPartiallyProcessed(fieldValues));
        settings.add(getPendingReview(fieldValues));
        settings.add(getNetworkProcessed(fieldValues));
        settings.add(getRejectedDCR(fieldValues));
        settings.add(getRejectedField(fieldValues));
        settings.add(getSalesforceCancelled(fieldValues));
        settings.add(getSalesforcePending(fieldValues));
        settings.add(getSalesforceProcessed(fieldValues));
        settings.add(getSalesforceSubmitted(fieldValues));
        settings.add(getSalesforceUpdated(fieldValues));

        VeevaNetworkStatusSettingsMVN.setMocks(settings);
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getAcceptedDCR(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Accepted DCR',
                'Network_Accepted_DCR_MVN',
                true,
                'Accepted',
                'DCR',
                'change_accepted'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getAcceptedField(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Accepted Field',
                'Network_Accepted_Field_MVN',
                true,
                null,
                'Field',
                'change_accepted'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getAddedField(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Added Field',
                'Network_Added_Field_MVN',
                true,
                null,
                'Field',
                'change_added'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getAlreadyAppliedField(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Already Applied Field',
                'Network_Already_Applied_Field_MVN',
                true,
                null,
                'Field',
                'change_alreadyapplied'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getCancelled(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Cancelled',
                'Network_Cancelled',
                true,
                'Cancelled',
                'DCR',
                'change_cancelled'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getChangeNew(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Already Change New',
                'Network_Change_New_MVN',
                false,
                null,
                'DCR',
                'change_new'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getInqueue(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Inqueue',
                'Network_Inqueue_MVN',
                false,
                null,
                'DCR',
                'change_inqueue'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getModifiedField(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Modified Field',
                'Network_Modified_Field_MVN',
                true,
                null,
                'Field',
                'change_modified'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getPartial(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Partial',
                'Network_Partial_MVN',
                true,
                'Partial',
                'DCR',
                'change_partial'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getPartiallyProcessed(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Partially Processed',
                'Network_Partially_Processed_MVN',
                true,
                null,
                'DCR',
                'change_partiallyprocessed'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getPendingReview(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Pending Review',
                'Network_Pending_Review_MVN',
                false,
                null,
                'DCR',
                'change_pendingreview'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getNetworkProcessed(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Processed',
                'Network_Processed',
                true,
                'Processed',
                'DCR',
                'change_processed'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getRejectedDCR(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Rejected DCR',
                'Network_Rejected_DCR_MVN',
                true,
                'Rejected',
                'DCR',
                'change_rejected'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getRejectedField(Map<String, Object> fieldValues) {
        return getStatus(
                'Network Rejected Field',
                'Network_Rejected_Field_MVN',
                false,
                null,
                'Field',
                'change_rejected'
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getSalesforceCancelled(Map<String, Object> fieldValues) {
        return getStatus(
                'Salesforce Cancelled',
                'Salesforce_Cancelled_MVN',
                false,
                'Cancelled',
                'DCR',
                null
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getSalesforcePending(Map<String, Object> fieldValues) {
        return getStatus(
                'Salesforce Pending',
                'Salesforce_Pending_MVN',
                false,
                'Pending',
                'DCR',
                null
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getSalesforceProcessed(Map<String, Object> fieldValues) {
        return getStatus(
                'Salesforce Processed',
                'Salesforce_Processed_MVN',
                false,
                'Processed',
                'DCR',
                null
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getSalesforceSubmitted(Map<String, Object> fieldValues) {
        return getStatus(
                'Salesforce Submitted',
                'Salesforce_Submitted_MVN',
                true,
                'Submitted',
                'DCR',
                null
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getSalesforceUpdated(Map<String, Object> fieldValues) {
        return getStatus(
                'Salesforce Updated',
                'Salesforce_Updated_MVN',
                true,
                'Updated',
                'DCR',
                null
        );
    }

    private static Veeva_Network_Status_Settings_MVN__mdt getStatus(String label, String name, Boolean isApplied, String sfdcStatus, String type, String networkStatus) {
        Map<String, Object> setting = new Map<String, Object> {
            'Label' => label,
            'Name' => name,
            'Apply_MVN__c' => isApplied,
            'Salesforce_Status_MVN__c' => sfdcStatus,
            'Type_MVN__c' => type,
            'Veeva_Network_Status_MVN__c' => networkStatus
        };

        if (myFieldValues != null) {
            setting.putAll(myFieldValues);
        }

        return (Veeva_Network_Status_Settings_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata('Veeva_Network_Status_Settings_MVN__mdt', setting);
    }
}