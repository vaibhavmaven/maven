/**
* @author Mavens
* @date 07/14/2018 
* @description Class to provide factory methods to create test data for Product objects
*/
@isTest
public class TestFactoryProductMVN {

    private TestFactorySObjectMVN objectFactory;

    public TestFactoryProductMVN() {
        objectFactory = new TestFactorySObjectMVN('Product_MVN__c', new Map<String, Object>());
    }

    public Product_MVN__c construct(Map<String, Object> valuesByField){
        return (Product_MVN__c) objectFactory.constructSObject(valuesByField);
    }

    public Product_MVN__c create(Map<String, Object> valuesByField){
        return (Product_MVN__c) objectFactory.createSObject(valuesByField);
    }

    public List<Product_MVN__c> constructMany(Integer numOfRequests, Map<String,Object> valuesByField) {
        return (List<Product_MVN__c>) objectFactory.constructSObjects(numOfRequests, valuesByField);
    }

    public List<Product_MVN__c> createMany(Integer numOfRequests, Map<String,Object> valuesByField) {
        return (List<Product_MVN__c>) objectFactory.createSObjects(numOfRequests, valuesByField);
    }
}