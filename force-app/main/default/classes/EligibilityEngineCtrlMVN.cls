/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description controller for the EligibilityEngineMVN page
 *              gets all eligibility engine results for the active application
 *              linked to this program memger, shows those and the
 *              overall result and last run date on the VF inlay page
 */
public class EligibilityEngineCtrlMVN {
    /**
     * latest eligibility engine run for the active application
     */
    public Eligibility_Engine_Run_MVN__c eligibilityEngineRun { get; set; }

    /**
     * result to show for the engine run
     */
    public ResultWrapper resultForProgramMember {
        get {
            return new ResultWrapper(this.eligibilityEngineRun);
        }
    }

    /**
     * Variable to manage what to display in the Visualforce page
     */
    public String mode { get; set; }

    /**
     * variable to store the message in case of Nintex error
     */
    public String nintexErrorMsg { get; set; }

    /**
     * parameters used by Nintex package to generate eligibility letters
     */
    public ParamInfo nintexParamInfo { get; set; }

    /**
    * current program member id
    */
    public Program_Member_MVN__c programMember { get; private set; }
    /**
    * program for current program member
    */
    private Id programId { get; set; }
    /**
    * active application for current program member
    */
    private Id applicationId { get; set; }

    public Boolean isFirstRun { get; set; }
    public Boolean doRefresh { get; set; }

    /**
     * constructor: collect program member and previous eligibility engine results
     * @param ApexPages.StandardController          stdController
     */
    public EligibilityEngineCtrlMVN(ApexPages.StandardController stdController) {
        Id programMemberId = stdController.getRecord().Id;
        this.programMember = [
            SELECT
                Id,
                Active_Application_MVN__c,
                Program_MVN__c,
                Program_Id_MVN__c,
                Patient_Status_MVN__c,
                Active_Application_MVN__r.IsMIgrated__c
            FROM
                Program_Member_MVN__c
            WHERE
                Id = :programMemberId
        ];
        
     
    
        this.getEligibilityEngineRunWithEligibilityEngineResults();
        this.mode = 'init';
        this.doRefresh = true;
      
    }

    /**
     * Run the Eligibility engine when the page is loaded if it has not been run previously
     */
    public PageReference runEngineAFirstTime() {
        if(eligibilityEngineRun == null || eligibilityEngineRun.Eligibility_Engine_Results_MVN__r == null) {
            if(!isMigrated()) {
                this.runEngine();
            }
            this.isFirstRun = true;
        }
        // If there has been an error disable the refreshment of the page
        if(eligibilityEngineRun == null || eligibilityEngineRun.Eligibility_Engine_Results_MVN__r == null) {
            this.isFirstRun = false;
        }
        return null;
    }

    /**
     * run Engine, upsert results, refresh program member and new eligibility engine results,
     * build and send letters with eligibility results to patient
     */
    public void runEngine() {
        ApexPages.getMessages().clear();

        try {
            // List<Eligibility_Engine_Rule_MVN__c> rules = EligibilityEngineMVN.getRulesForProgram(this.programId);
   
             EligibilityEngineMVN engine = new EligibilityEngineMVN(this.applicationId);
             List<Eligibility_Engine_Result_MVN__c> results = engine.run();
             EligibilityEngineMVN.processResults(results, this.applicationId);
             insert results;

             this.getEligibilityEngineRunWithEligibilityEngineResults();
             this.updateApplicationStatus();
             this.mode = 'letterGen';

             this.triggerActionEngine();
        } catch(Exception e) {
            if (e.getMessage().contains('ENTITY_IS_LOCKED')) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,
                Label.Eligibiligy_Engine_Lock_App_Error_Message_MVN));
            } else {
                System.debug(e.getStackTraceString());
                ApexPages.addMessages(e);
            }
            this.doRefresh = false;
        }
    }

    /**
     * Call the action engine to execute automated actions based on custom metadata definition
     */
    private void triggerActionEngine() {
        ActionEngineMVN actionEngine = new ActionEngineMVN(this.programMember);
        if(this.eligibilityEngineRun.Application_MVN__r.Status_MVN__c == EligibilityEngineMVN.RULESUCCESS) {
            actionEngine.run(ActionEngineMVN.PASSEDACTION);
        } else if(this.eligibilityEngineRun.Application_MVN__r.Status_MVN__c == EligibilityEngineMVN.RULEDENIED) {
            actionEngine.run(ActionEngineMVN.DENIEDACTION);
        } else if(this.eligibilityEngineRun.Application_MVN__r.Status_MVN__c == EligibilityEngineMVN.RULEMISSINGINFO) {
            actionEngine.run(ActionEngineMVN.MISSINGINFOACTION);
        }
    }

    /**
    * @description Nintex class used to generate doc letters
    */
    public class ParamInfo {
        public Loop.ProcessDdpParameters Parameters { get; set; }
        public ParamInfo(Loop.ProcessDdpParameters parameters) {
            this.Parameters = parameters;
        }
    }

    /**
     * Generate and send letters with eligibility results to patient using Nintex package
     */
    public void generateEligibilityLetters() {
 

        if (this.resultForProgramMember != null && this.resultForProgramMember.docGenPackageFilter != null) {
            String docGenPackageFilterWithProgramId = this.programMember.Program_Id_MVN__c + '-PM-' + this.resultForProgramMember.docGenPackageFilter;
            List<Loop__DDP__c> docGenPackageList = [
                SELECT
                    Id,
                    (SELECT Id, RecordType.Name, Loop__Attach_As__c FROM Loop__Custom_Integration_Options__r)
                FROM
                    Loop__DDP__c
                WHERE
                    Loop__Filter__c = :docGenPackageFilterWithProgramId
            ];

            if (!docGenPackageList.isEmpty()) {
                Id docGenPackageId = docGenPackageList.get(0).Id;
                Id deliveryOptionId;
                List<Loop__DDP_Integration_Option__c> deliveryOptionList = docGenPackageList.get(0).Loop__Custom_Integration_Options__r;

                for (Loop__DDP_Integration_Option__c deliveryOption : deliveryOptionList) {
                    if (deliveryOption.RecordType.Name != 'Attach' || deliveryOption.Loop__Attach_as__c != 'Attachment') {
                        deliveryOptionId = deliveryOption.Id;
                        break;
                    }
                }
                if (!deliveryOptionList.isEmpty()) {
                    if (deliveryOptionId == null) {
                        deliveryOptionId = deliveryOptionList.get(0).Id;
                    }
                    Loop.ProcessDdpParameters param = new Loop.ProcessDdpParameters(
                        docGenPackageId,
                        deliveryOptionId,
                        this.programMember.Id,
                        new List<Id>()
                    );
                    param.theme = Loop.ProcessDdpParameters.ThemeStyle.BOOTSTRAP_3_2;
                    param.onCompleteCallback = 'onCompleteCallback';
                    param.onErrorCallback = 'onErrorCallback';
                    this.nintexParamInfo = new ParamInfo(param);
                }
                this.mode = 'nintex';
            }
        }
    }

    /*************************************************************************************
     * @description This method is called when the Nintex package finishes the generation
     * of a document. It hides the generation and sending of letter section.
     */
    public void onCompleteNintexCallbackApex() {
        this.mode = 'init';
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,
            Label.Eligibility_Engine_Letter_Sending_Success_MVN));
    }

    /*************************************************************************************
     * @description This method is called when the Nintex package throws an error during
     * the generation of a document. It displays the error message thrown by Nintex to the
     * user.
     */
    public void onErrorNintexCallbackApex() {
        this.mode = 'init';
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, nintexErrorMsg));
    }

    /**
     * get eligibility engine run and new eligibility engine results
     */
    private void getEligibilityEngineRunWithEligibilityEngineResults() {
        List<Eligibility_Engine_Run_MVN__c> eligibilityEngineRunList = [
            SELECT
                Id,
                Application_MVN__c,
                Result_MVN__c,
                Result_Status_MVN__c,
                Run_Datetime_MVN__c,
                Application_MVN__r.Id,
                Application_MVN__r.Program_Member_MVN__r.Program_MVN__c,
                (
                    SELECT
                        Id,
                        Name,
                        Result_MVN__c,
                        Result_Message_MVN__c,
                        Cannot_be_overriden_MVN__c
                    FROM
                        Eligibility_Engine_Results_MVN__r
                    ORDER BY
                        Result_MVN__c DESC, Name
                )
            FROM
                Eligibility_Engine_Run_MVN__c
            WHERE
                Application_MVN__c = :this.programMember.Active_Application_MVN__c
            AND
                Application_MVN__r.Program_Member_MVN__c = :this.programMember.Id
            ORDER BY
                Run_Datetime_MVN__c DESC
        ];
        if (!eligibilityEngineRunList.isEmpty()) {
            this.eligibilityEngineRun = eligibilityEngineRunList.get(0);
            this.programId = this.eligibilityEngineRun.Application_MVN__r.Program_Member_MVN__r.Program_MVN__c;
            this.applicationId = this.eligibilityEngineRun.Application_MVN__c;
        } else {
            Program_Member_MVN__c programMember = [
                SELECT
                    Program_MVN__c, Active_Application_MVN__c
                FROM
                    Program_Member_MVN__c
                WHERE
                    Id = :programMember.Id
            ];
            this.programId = programMember.Program_MVN__c;
            this.applicationId = programMember.Active_Application_MVN__c;
        }
    }

    /**
     * Update the Application Status with the Eligibility Engine Run Result
     */
    private void updateApplicationStatus() {
        this.eligibilityEngineRun.Application_MVN__r.Status_MVN__c = this.eligibilityEngineRun.Result_MVN__c;
        update this.eligibilityEngineRun.Application_MVN__r;
    }

    /**
     * inner wrapper class to show overall results
     */
    public class ResultWrapper {
        /**
        * result string that is shown
        */
        public String result { get; set; }

        /**
        * icon for type of result that is shown
        */
        public String iconName { get; set; }

        /**
        * color for the result string that is shown
        */
        public String color { get; set; }

        /**
        * last run datetime
        */
        public String lastRun { get; set; }

        /**
         * Word to filter docGen Packages
         */
        public String docGenPackageFilter { get; set; }

        /**
        * constructor: initiate result wrapper record to show
        * @param Program_Member_MVN__c          programMember
        */
        public ResultWrapper(Eligibility_Engine_Run_MVN__c eligibilityEngineRun) {

            if (eligibilityEngineRun != null) {

                EligibilityEngineSettingMVN engineSettingsClass = new EligibilityEngineSettingMVN();
                List<Eligibility_Engine_Setting_MVN__mdt> engineSettings =
                    engineSettingsClass.getEligibilityEngineSettingByField(
                        'Engine_Result_Value_MVN__c', eligibilityEngineRun.Result_Status_MVN__c
                    );

                if (!engineSettings.isEmpty()) {
                    this.iconName = engineSettings.get(0).Result_Icon_Name_MVN__c;
                    this.color = engineSettings.get(0).Result_Color_MVN__c;
                    this.result = engineSettings.get(0).Result_Label_MVN__c;
                    this.docGenPackageFilter = engineSettings.get(0).DocGen_Package_filter_MVN__c;
                }
                if (eligibilityEngineRun.Run_Datetime_MVN__c != null) {
                    this.lastRun = eligibilityEngineRun.Run_Datetime_MVN__c.format();
                }
            }
        }
    }

    /**
     * Update mode variable to make results editable
     */
    public void edit() {
        this.mode = 'edit';
    }

    /**
     * Update mode variable to make results read-only
     */
    public void cancel() {
        this.mode = 'init';
    }

    /**
     * Update mode variable to hide the letter generation section
     */
    public void hideLetterSection() {
        this.mode = 'init';
    }

    /**
     * Save values entered by user for eligibility engine results
     */
    public void save() {
        List<Eligibility_Engine_Result_MVN__c> newResults =
            this.eligibilityEngineRun.Eligibility_Engine_Results_MVN__r.deepClone();
        EligibilityEngineMVN.processManualOverrideResults(newResults, this.applicationId);
        insert newResults;
        this.getEligibilityEngineRunWithEligibilityEngineResults();
        this.updateApplicationStatus();
        this.mode = 'letterGen';
        this.triggerActionEngine();
    }

    /**
     * Clear Page messages
     */
    public void clearMessages() {
        ApexPages.getMessages().clear();
    }
    
    /* 
    * Check if the current active application is migrated record or created using app on SF
    */
    public boolean isMigrated()
    {
       if(this.programMember!=null)
           if(this.programMember.Active_Application_MVN__r!=null)
               if(this.programMember.Active_Application_MVN__r.IsMIgrated__c!=null)
                  return this.programMember.Active_Application_MVN__r.IsMIgrated__c;
       return false; 
    }
 
}