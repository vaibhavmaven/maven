global class RTBI_Retrieve_Only {
    webservice static String getRTBI_Retrieve_Only(String bncID){
        try{ 
            // check whether RTBI Test claim reversal was hit.
            list<RTBI_Response_Master__c> rtbiResMasterlist=[select id from RTBI_Response_Master__c where Benefits_Coverage__c = :bncID 
                                                             ORDER BY Last_Query_Date__c DESC NULLS LAST LIMIT 1];
            if(rtbiResMasterlist.size()==0){
                System.debug('ERROR: RTBI Test claim reversal for this benefits and coverage is not hit not processing retrieve only api.');
                return 'false';
            }
            
            // fetching required data for RTBI.   
            Benefits_Coverage_MVN__c bncObj = EligibilityChecker.getBnCDetails(bncID); 
            Program_Member_MVN__c prgmMemObj = EligibilityChecker.getPatientAndPrescriberDetails(bncObj); 
            List<Prescription_MVN__c> prscpList = EligibilityChecker.getPrescriptionDetails(bncObj.Application_MVN__c); 
            Pharmacy_Details__mdt phDetailObj = EligibilityChecker.getPharmacyDetails(); 
            
            // fetching RTBI retrieve only endpoint details. 
            List<Eligibility_Endpoint_Configuration__mdt> elgbEndpointList = EligibilityChecker.getEligibilityEndpointDetails();
            
            map<String, Eligibility_Endpoint_Configuration__mdt> mapEligibilityEP = new map<String, Eligibility_Endpoint_Configuration__mdt>{}; 
                for(Eligibility_Endpoint_Configuration__mdt elgObj : elgbEndpointList){
                    mapEligibilityEP.put(elgObj.Endpoint_Type__c, elgObj); 
                } // RTBI view.
            system.debug('Populated Endpoint Configuration Data.:  '+ mapEligibilityEP); 
            
            // populating rtbi payload here.
            String strDaysSupply = EligibilityChecker.getDaysSupply();    // gets the mapped value from custom metadata.
            system.debug('Days Supply configured in custom metadata is : ' + strDaysSupply);
            String rtbipayload = EligibilityChecker.populateRTBIPayload(prgmMemObj, mapEligibilityEP.get('RTBI Retrieve Only'), phDetailObj, strDaysSupply); 
            
            String rtbiPayloadCopy= ''; 
            String responseString = ''; 
            String uniqueKey = '';
            Map<String, String> calloutResponseMap = new Map<String, String>{}; 
            AuthenticateAndCallout authnCall; 
                     
            
            // for each prescription, we will have to iterate through each drug mentioned in it. 
            for(Prescription_MVN__c prscp : prscpList){
                // re-initialize rtbiPayloadCopy to its original value.
                rtbiPayloadCopy = EligibilityChecker.populatePrescriptionDetails(prscp, rtbipayload, strDaysSupply); 
                // As per business logic, this query will be executed from within loop !!!
                List<Product_Line_Item_MVN__c> prdLineList = [select NDC_MVN__c from Product_Line_Item_MVN__c where 
                                                              Product_MVN__c = :prscp.Product_MVN__c];   
                // Processing each drug one at a time. 
                for(Product_Line_Item_MVN__c prdLineObj : prdLineList){                                                                  
                    // Confirm the Caching period for this drug from Eligibility Response Master,
                    authnCall = null; 
                    responseString = ''; 
                    uniqueKey = ''; 
                    system.debug('Eligibile record : ' + bncID);
                    
                    rtbiPayloadCopy = EligibilityChecker.populateNDCDetails(prdLineObj, rtbiPayloadCopy); // rtbi payload prepared completely.
                    authnCall = EligibilityChecker.makeCallout(mapEligibilityEP.get('RTBI Retrieve Only'), rtbiPayloadCopy);
                    responseString = authnCall.getResponse();
                    
                    if(200 != authnCall.getResponseCode()){
                        // some thing went wrong .. rasie excpetion. 
                        System.debug('RTBI retrieve only callout failed.'); 
                        throw new CustomException('RTBI retrieve only callout failed.'); 
                    }                    
                    // Adding RTBI response to map, we will process it later. 
                    uniqueKey = prscp.Name + '_' + prdLineObj.NDC_MVN__c + '_RTBI-RetrieveOnly' ; 
                    system.debug('RTBI retrieve only response string : ' + responseString);
                    calloutResponseMap.put(uniqueKey, responseString); 
                } // for each drug  
            }  // for each prescription
            
            // populate custom objects and insert them. 
            for(String responseKey : calloutResponseMap.keySet()){
                List<String> tokenList = responseKey.split('_'); 
                if(responseKey.contains('_RTBI-RetrieveOnly')){                    
                    EligibilityChecker.processRTBIJson(calloutResponseMap.get(responseKey), tokenList[1], bncID, true);                    
                }                
            } 
            return 'true';
        }catch (CustomException cust){
            handleException();               
            system.debug('Exception : ' + cust.getMessage()); 
            system.debug('Custom Exception Details : ' + cust.getStackTraceString()); 
        }catch(Exception excp){
            handleException();               
            system.debug('Exception : ' + excp.getMessage()); 
            system.debug('Exception Details : ' + excp.getStackTraceString()); 
        }
        return 'false'; 
    }
     public static void handleException(){        
        Database.setSavepoint();
        system.debug('Exception encountered, commiting changes to Database; setSavepoint called.');
    }
}