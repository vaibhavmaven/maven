/*
 * TaskTriggerHandlerMVN
 * Created By:      Jen Wyher
 * Created Date:    Dec 2015
 * Description:     Test class for RelationshipGraphCtrlMVN
 *					
 */

@isTest
private class RelationshipGraphCtrlTestMVN {

	static Account member;
	static Account physician;
	static Relationship_MVN__c relationship;
	static Program_MVN__c program;
	static Program_Member_MVN__c pMember;
	
	static {
		TestDataFactoryMVN.createPSSettings();

		program = TestDataFactoryMVN.createProgram();
		member = TestDataFactoryMVN.createMember();
		pMember = TestDataFactoryMVN.createProgramMember(program, member);
		physician = TestDataFactoryMVN.createPhysician();
		relationship = TestDataFactoryMVN.createRelationship(member, physician, pMember);
	}
	
	@isTest static void getRelationshipTree() {
		List<RecordType> rts = [SELECT Id, Name, SObjectType FROM RecordType WHERE SObjectType = 'Contact_Information_MVN__c'];

		List<Contact_Information_MVN__c> infos = new List<Contact_Information_MVN__c>();

		for(RecordType rt: rts) {
			if(rt.Name == 'Email') {
				Contact_Information_MVN__c contactEmail = new Contact_Information_MVN__c();
				contactEmail.Account_MVN__c = physician.Id;
				contactEmail.Email_MVN__c = 'test@mavens.com';
				contactEmail.RecordTypeId = rt.Id;
				contactEmail.Primary_MVN__c = true;
				infos.add(contactEmail);
			} else if(rt.Name == 'Phone') {
				Contact_Information_MVN__c contactPhone = new Contact_Information_MVN__c();
				contactPhone.Account_MVN__c = physician.Id;
				contactPhone.Phone_MVN__c = '(123) 123-4321';
				contactPhone.RecordTypeId = rt.Id;
				contactPhone.Primary_MVN__c = true;
				infos.add(contactPhone);
			}	
		}

		insert infos;

		PageReference pageRef = Page.RelationshipGraphMVN;
		Test.setCurrentPage(pageRef);
		RelationshipGraphCtrlMVN controller = new RelationshipGraphCtrlMVN(); 
		
		RelationshipGraphCtrlMVN.Node node = RelationshipGraphCtrlMVN.getTree(member.Id, member.Id);

		System.debug(node);
		System.debug(member);

		// initial node is the Account itself
		System.assertEquals(node.Id, member.Id);
		System.assertEquals(node.name, member.FirstName + ' ' + member.LastName);


		// all children are initially shown (not hidden)
		// the first children are the roles
		List<RelationshipGraphCtrlMVN.Node> roleNodes = node.children;
		System.assertEquals(roleNodes.size(), 1);
		System.assertEquals(node.hiddenChildren, null);
		RelationshipGraphCtrlMVN.Node roleNode = roleNodes[0];
		System.assertEquals(roleNode.name, relationship.To_Role_MVN__c);

		// the next node are the actual relationships, for that role
		List<RelationshipGraphCtrlMVN.Node> relNodes = roleNode.children;
		System.assertEquals(relNodes.size(), 1);
		RelationshipGraphCtrlMVN.Node relNode = relNodes[0];
		System.assertEquals(relNode.Id, relationship.To_Account_MVN__c+'-'+relationship.Id);
		System.assertEquals(relNode.acctId, relationship.To_Account_MVN__c);
		System.assertEquals(relNode.name, physician.FirstName + ' ' + physician.LastName);
		System.assertEquals(relNode.children, null); // it has no children
		System.assertEquals(relNode.getURL(), '/' + relationship.To_Account_MVN__c );
	}

	// Tests searching
	// Creating a relationship
	// Delete a relationship

	@isTest static void createUpdateAndDeleteRelationship() {
		// create an account to search for
		Account account = new Account();
		account.FirstName = 'account';
		account.LastName = 'Nancy';
		account.RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and Name = 'Caregiver'][0].Id;
		insert account;

		Test.startTest();
		// Search for the account to create a relationship against
		List<Account> results = RelationshipGraphCtrlMVN.getSearchResults('Nancy');
		System.assertEquals(1, results.size());

		// Get the program members for the member/patient
		PageReference pageRef = Page.RelationshipGraphMVN;
		Test.setCurrentPage(pageRef);
		
		RelationshipGraphCtrlMVN controller = new RelationshipGraphCtrlMVN(); 
		RelationshipGraphCtrlMVN.accId = member.Id;
		List<SelectOption> options = controller.getPrograms();
		System.assertEquals(1, options.size());
		System.assertEquals(program.Name, options[0].getLabel());
		System.assertEquals(pMember.Id, options[0].getValue());

		// get the potential roles
		List<SelectOption> toRoles = controller.getToRoles();
		List<SelectOption> fromRoles = controller.getFromRoles();
		controller.setToRole(toRoles[0].getLabel());
		controller.setFromRole(fromRoles[0].getLabel());

		// create the new relationship
		RelationshipGraphCtrlMVN.createRelationship(member.Id, account.Id, controller.getFromRole(), controller.getToRole(), pMember.Id, 'My comment');
		List<Relationship_MVN__c> rels = [select Id, From_Account_MVN__c, To_Account_MVN__c, From_Role_MVN__c, To_Role_MVN__c, Comments_MVN__c, Program_Member_MVN__c
												from Relationship_MVN__c
												where To_Account_MVN__c = :account.Id ];
		System.assertEquals(1, rels.size());


		RelationshipGraphCtrlMVN.updateRelationship(rels[0].Id, rels[0].Program_Member_MVN__c, 'Test update', fromRoles[0].getLabel(), toRoles[0].getLabel());
		rels = [select Id, From_Account_MVN__c, To_Account_MVN__c, From_Role_MVN__c, To_Role_MVN__c, Comments_MVN__c, Program_Member_MVN__c
												from Relationship_MVN__c
												where To_Account_MVN__c = :account.Id ];
		System.assertEquals('Test update', rels[0].Comments_MVN__c);

		// remove it now
		RelationshipGraphCtrlMVN.removeRelationship(rels[0].Id);
		rels = [select Id, From_Account_MVN__c, To_Account_MVN__c, From_Role_MVN__c, To_Role_MVN__c, Comments_MVN__c, Program_Member_MVN__c
												from Relationship_MVN__c
												where To_Account_MVN__c = :account.Id ];
		System.assertEquals(0, rels.size());
		Test.stopTest();
	}	
}