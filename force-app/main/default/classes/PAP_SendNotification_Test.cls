@isTest
public class PAP_SendNotification_Test {
    static Program_MVN__c program;
    static Program_Member_MVN__c programMember;
    static List<Account> patients;
    static Account hcp;
    Static Eligibility_Engine_Run_MVN__c eligibilityEngineRun;
    Static Eligibility_Engine_Result_MVN__c eligibilityEngineResult;
    static List<Contact_Information_MVN__c> contactInfo;
    static Product_MVN__c product;
    static Application_MVN__c application;
    static Prescription_MVN__c prescription;
    static List<Address_vod__c> addresses;
    static Order_Scheduler_MVN__c orderScheduler;
    static List<Order_MVN__c> orderList;
    static Attachment attach ;
    static List<Order_Line_Item_MVN__c> OrderLnItemObj;
    static Document_MVN__c document;
    private static Id nintexDeliveryId;
    static Loop__DDP__c loopDDPs;
    static Loop__DDP_Integration_Option__c loopDDPIntegrations;
    static List<Benefits_Coverage_MVN__c> benefitAndCoverage;
    static String initialStatus;
    
    @testSetup
    static void setup(){
        createNintexPackages();
        program = createProgram();
        programMember = createProgramMember();
        application = createApplication(); 
        eligibilityEngineRun = createEligibilityEngineRun(application);
        eligibilityEngineResult =  createEligibilityEngineResult(eligibilityEngineRun);
        addresses = createAddresses();
    }
    public static Map<String, RecordType> accountRecordTypesByName {
        get {
            if (accountRecordTypesByName == null) {
                accountRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
                    accountRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return accountRecordTypesByName;
        }
        private set;
    }
    
    public static List<Account> createPatients(Integer numOfPatients) {
        List<Account> patientsList = new List<Account>();
        List<String> genderList = new List<String>{'Female', 'M'}; 
            Integer counter = 1; 
        for(String genderStr : genderList){
            for (Integer i=0; i<2; i++) {
                Account patient= new Account();
                patient.FirstName = 'Test';
                patient.MiddleName = 'testing';
                patient.LastName = 'Patient' + counter;
                patient.Gender_MVN__c = genderStr; 
                patient.Marital_Status_MVN__c = 'Single';                      
                patient.PersonBirthdate = System.today();
                patient.phone='(432-12345)';
                patient.PersonMobilePhone ='(432-12345)';
                patient.Primary_Email_MVN__c = 'test@example.com';
                patient.Fax='(432-12345)';
                patient.BillingStreet='TEST ';
                patient.BillingCity='';
                patient.BillingStateCode='';
                patient.BillingPostalCode='';              
                patient.BillingCountry='';
                patient.BillingState='';
                patient.BillingCountryCode='';
                patient.RecordTypeId = accountRecordTypesByName.get('Patient_MVN').id;
                counter++;          
                patientsList.add(patient);
            }            
        }
        
        insert patientsList;
        Account accPayer = TestDataFactoryMVN.createPayer();
        accPayer.BillingCity = 'TestPayer';
        accPayer.BillingStreet = 'Street1';
        accPayer.BillingPostalCode='62626';
        accPayer.BillingStateCode='NJ';
        accPayer.BillingCountryCode='US';
        update accPayer;
        return patientsList;
    }
    
    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        Program_MVN__c program = programFactory.create();
        program.Fulfilment_Method_MVN__c = 'Pharmacy';
        program.SMS_JKC__c = true;
        program.AutoDialer_JKC__c= true;
        program.Email_JKC__c= true;
        program.Mail_JKC__c=true;
        program.Fax_JKC__c=true;
        update program;
        return program;
    }
    
    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        system.debug('######### programMember Id'+programMember.Id);
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id,
                'Status_MVN__c' => 'Fulfilled',
                'Enrollment_Date_MVN__c' => System.today(),
                'Application_Reminder_Date_MVN__c' => System.today()-2,
                'Expiry_Date_MVN__c' => System.today()+30,
                'Known_Allergies_MVN__c'=>'PENECILIN'
                
                });
        return testApplication;
    }
    
    
    static Program_Member_MVN__c createProgramMember() {
        TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
        patients = createPatients(1); 
        
        hcp = accountFactory.createPrescriber(null);
        hcp.Fax= '(432-12345890)';
        hcp.Primary_Email_MVN__c='test@example.com';
        update hcp;
        TestFactoryContactInformationMVN contactInfo = new TestFactoryContactInformationMVN();
        
        Contact_Information_MVN__c contactPhone = contactInfo.constructPhone(patients[0]);
        contactPhone.Label_MVN__c = 'Mobile';
        contactPhone.Primary_MVN__c = True;
        contactPhone.Messages_OK_MVN__c = 'Yes';
        Insert contactPhone;
        
        TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();
        Program_Member_MVN__c programMember = programMemberFactory.create(program, hcp, patients)[0];
        programMember.Patient_Status_MVN__c = 'Opt-In';
        programMember.Program_Id_MVN__c = 'Test';
        
        update programMember;
          Document_MVN__c document = new Document_MVN__c(
            Program_Member_MVN__c = programMember.Id,
            Type_MVN__c = 'A'
        );
        insert document;
        Attachment att = new Attachment();
        att.name = 'Test';
        att.Body = Blob.valueOf('Tester');
        att.ParentId = document.Id;
        insert att;
        return programMember;
    }
    static Eligibility_Engine_Run_MVN__c createEligibilityEngineRun(Application_MVN__c app) {
        system.debug('######### applicaion '+app);
        Eligibility_Engine_Run_MVN__c eligibilityEngineRun = new Eligibility_Engine_Run_MVN__c();
        eligibilityEngineRun.Application_MVN__c = app.Id;
        eligibilityEngineRun.Last_Run_MVN__c = true; 
        insert eligibilityEngineRun;
        return eligibilityEngineRun;
    }
    
    static Eligibility_Engine_Result_MVN__c createEligibilityEngineResult(Eligibility_Engine_Run_MVN__c engineRun) {
        system.debug('######### applicaion '+engineRun);
        Eligibility_Engine_Result_MVN__c eligibilityEngineRun = new Eligibility_Engine_Result_MVN__c();
        eligibilityEngineRun.Result_Message_MVN__c = 'Denied Reason1';
        eligibilityEngineRun.Result_MVN__c = 'Denied';
        eligibilityEngineRun.Eligibility_Engine_Run_MVN__c = engineRun.Id;
        insert eligibilityEngineRun;
        return eligibilityEngineRun;
    }
    
    
    static void createNintexPackages() {
        Loop__DDP__c nintexPackage = new Loop__DDP__c(
            Name = 'test package',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'Test-PM-Approval-Patient'
        );
        Insert nintexPackage;
        
        Loop__DDP__c nintexPackageSponsor = new Loop__DDP__c(
            Name = 'test package',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'Test-PM-MedicareMedD'
        );
        insert nintexPackageSponsor;
        
        Loop__DDP__c nintexPackage2 = new Loop__DDP__c(
            Name = 'test package',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'Test-PM-Approval-HCP'
        );
        insert nintexPackage2;
        
        Loop__DDP_Integration_Option__c ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Email').getRecordTypeId();
        ddpOption.Loop__Attach_As__c = 'Attachment';
        ddpOption.Name = 'Email';
        insert ddpOption;
        
        Loop__DDP_Integration_Option__c ddpOption1 = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage2.Id);
        ddpOption1.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Email').getRecordTypeId();
        ddpOption1.Loop__Attach_As__c = 'Attachment';
        ddpOption1.Name = 'Email';
        insert ddpOption1;
        
        Loop__DDP_Integration_Option__c ddpOption2 = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackageSponsor.Id);
        ddpOption2.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Email').getRecordTypeId();
        ddpOption2.Loop__Attach_As__c = 'Attachment';
        ddpOption2.Name = 'Mail';
        insert ddpOption2;
    }
    
    static List<Address_vod__c> createAddresses() {
        TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();
        List<Address_vod__c> addressTestList = new List<Address_vod__c>();
        addressTestList.add(
            addressFactory.constructAddress(patients[0], new Map<String,Object>{
                'Name' => 'Patient Address Line 1',
                    'Address_line_2_vod__c' => 'Patient Address Line 2',
                    'Shipping_vod__c' => true,
                    'Primary_vod__c' => true,
                    'City_vod__c' => 'Patient City',
                    'Zip_vod__c' => 'Patient 11111',
                    'State_vod__c' => 'IL'
                    })
        );
      
        insert addressTestList;
        return addressTestList;
    }
    
    @isTest static void getPatientTestSMS() {
        //   insert getBAndCs();
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>([select id from Program_Member_MVN__c]);
        PAP_SendNotification papSendNotification  = new PAP_SendNotification();
        PAP_SendNotification.sendNotification_Patient(programMembers[0].Id ,True , True);
        PAP_SendNotification.sendNotification_HCP(programMembers[0].Id ,True , True);
   
        system.assertNotEquals(programMembers.size(), 1);
    }
    
      @isTest static void getPatientTestSMSNegative() {
        //   insert getBAndCs();
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>([select id from Program_Member_MVN__c]);
        PAP_SendNotification papSendNotification  = new PAP_SendNotification();
        PAP_SendNotification.sendNotification_Patient(programMembers[0].Id ,false , true);
        PAP_SendNotification.sendNotification_HCP(programMembers[0].Id ,false , true);
   
        system.assertNotEquals(programMembers.size(), 1);
    }
   
     @isTest static void getPatientTestSMSButton() {
        //   insert getBAndCs();
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>([select id from Program_Member_MVN__c]);
        PAP_SendNotification papSendNotification  = new PAP_SendNotification();
        PAP_SendNotification.sendNotification_Patient(programMembers[0].Id ,false , false);
        PAP_SendNotification.sendNotification_HCP(programMembers[0].Id ,false , false);
   
        system.assertNotEquals(programMembers.size(), 1);
    }
    
    @isTest static void getPatientTestAutodial() {
        Program_MVN__c programMemberAutodial = [Select id,SMS_JKC__c from Program_MVN__c];
        programMemberAutodial.SMS_JKC__c = false;
        update programMemberAutodial;
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>([select id from Program_Member_MVN__c]);
        PAP_SendNotification papSendNotification  = new PAP_SendNotification();
        PAP_SendNotification.sendNotification_Patient(programMembers[0].Id ,True , True);
        system.assertNotEquals(programMembers.size(), 1);
    }
    @isTest static void getPatientTestEmail() {
        Program_MVN__c programMemberAutodial = [Select id,SMS_JKC__c from Program_MVN__c];
        programMemberAutodial.SMS_JKC__c = false;
        programMemberAutodial.AutoDialer_JKC__c = false;
        programMemberAutodial.Fax_JKC__c = false;
        update programMemberAutodial;
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>([select id from Program_Member_MVN__c]);
        test.startTest();
        PAP_SendNotification papSendNotification  = new PAP_SendNotification();
        PAP_SendNotification.sendNotification_Patient(programMembers[0].Id ,True , True);
        PAP_SendNotification.sendNotification_HCP(programMembers[0].Id ,True , True);
        
        test.stopTest();
        system.assertNotEquals(programMembers.size(), 1);
    }
    @isTest static void getPatientTestMail() {
        Program_MVN__c programMemberAutodial = [Select id,SMS_JKC__c from Program_MVN__c];
        programMemberAutodial.SMS_JKC__c = false;
        programMemberAutodial.AutoDialer_JKC__c = false;
        programMemberAutodial.Email_JKC__c = false;
        update programMemberAutodial;
        
        Account acc = [select id from Account where Fax= '(432-12345890)']; 
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>([select id from Program_Member_MVN__c]);
       	Test.startTest();
        PAP_SendNotification papSendNotification  = new PAP_SendNotification();
        PAP_SendNotification.sendNotification_Patient(programMembers[0].Id ,True , True);
        test.stopTest();
        system.assertNotEquals(programMembers.size(), 1);
    }
    @isTest static void getPatientTestInvalidAddress() {
        Program_MVN__c programMemberAutodial = [Select id,SMS_JKC__c from Program_MVN__c];
        programMemberAutodial.SMS_JKC__c = false;
        programMemberAutodial.AutoDialer_JKC__c = false;
        programMemberAutodial.Email_JKC__c = false;
        programMemberAutodial.Mail_JKC__c=true;
        programMemberAutodial.Fax_JKC__c = false;
        update programMemberAutodial;
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>([select id from Program_Member_MVN__c]);
        PAP_SendNotification papSendNotification  = new PAP_SendNotification();
        PAP_SendNotification.sendNotification_Patient(programMembers[0].Id ,True , True);
        PAP_SendNotification.sendNotification_HCP(programMembers[0].Id ,True , True);
        
        Account accountPayer = [Select id from Account where BillingCity = 'TestPayer' ];                 
        Application_MVN__c applications =[Select id from Application_MVN__c ];
        
        Benefits_Coverage_MVN__c b2=   new Benefits_Coverage_MVN__c( Program_Member_MVN__c = programMembers[0].Id,
                                                                    Application_MVN__c = applications.Id,Medicare_Part_D_MVN__c ='No',
                                                                    Medicare_Part_B_MVN__c = 'Yes', // one true
                                                                    Payer_Med_B_Plan_Sponsor_JKC__c = accountPayer.Id,
                                                                    Payer_Med_D_Plan_Sponsor_JKC__c = accountPayer.Id
                                                                   );
        insert b2;
        String deliveryType = 'Mail';
        Id pgmId = programMembers[0].Id;
        PAP_SendNotification.sendNotification_PlanSponsor(programMembers[0].Id ,True , True);
        
        system.assertNotEquals(programMembers.size(), 1);
        
    }
    @isTest static void getPatientTestPlanSponsor() {
        Program_MVN__c programMemberAutodial = [Select id,SMS_JKC__c from Program_MVN__c];
        programMemberAutodial.SMS_JKC__c = false;
        programMemberAutodial.AutoDialer_JKC__c = false;
        programMemberAutodial.Email_JKC__c = false;
        programMemberAutodial.Mail_JKC__c=true;
        programMemberAutodial.Fax_JKC__c = false;
        update programMemberAutodial;
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>([select id from Program_Member_MVN__c]);
        
        Account accountPayer = [Select id from Account where BillingCity = 'TestPayer' ];                 
        Application_MVN__c applications =[Select id from Application_MVN__c ];
        Benefits_Coverage_MVN__c b1=   new Benefits_Coverage_MVN__c( Program_Member_MVN__c = programMembers[0].Id,
                                                                    Application_MVN__c = applications.Id,Medicare_Part_D_MVN__c ='Yes',
                                                                    Medicare_Part_A_MVN__c = 'Yes', // one true
                                                                    Payer_Med_B_Plan_Sponsor_JKC__c = accountPayer.Id,
                                                                    Payer_Med_D_Plan_Sponsor_JKC__c = accountPayer.Id
                                                                   );
        insert b1;
        
        String deliveryType = 'Mail';
        Id pgmId = programMembers[0].Id;
        PAP_SendNotification.sendNotification_PlanSponsor(programMembers[0].Id ,True , True);
        system.assertNotEquals(programMembers.size(), 1);
        
    }
}