/**
 * DCRFieldSettingsTestMVN
 * Created By: Rick Locke
 * Created Date: October 2017
 * Description : Unit tests for DCRFieldSettingsMVN
 */
@isTest
public with sharing class DCRFieldSettingsTestMVN {

    @isTest
    private static void itShouldFindSettingsWhenNotMocked() {
        DCRFieldSettingsMVN fieldSettings = new DCRFieldSettingsMVN();
        System.assertNotEquals(null, fieldSettings.getSettings());
    }

    @isTest
    private static void itShouldAllowMocksToBeSet() {
        DCRFieldSettingsMVN.setMockList(new List<DCR_Field_Setting_MVN__mdt>{
            (DCR_Field_Setting_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata(
                'DCR_Field_Setting_MVN__mdt',
                new Map<String, Object>{'Field_MVN__c'=>'value'}
            )
        });
        DCRFieldSettingsMVN fieldSettings = new DCRFieldSettingsMVN();
        List<DCR_Field_Setting_MVN__mdt> wrappedSettings = fieldSettings.getSettings();
        System.assertEquals('value', wrappedSettings[0].Field_MVN__c);
    }

    @isTest
    private static void itShouldReturnConfiguredValues() {
        DCRUtilityMVN dcrUtility = new DCRUtilityMVN();
        DCRFieldSettingsMVN.setMockList(dcrUtility.buildMockFieldSettingsForTest());
        DCRFieldSettingsMVN fieldSettings = new DCRFieldSettingsMVN();
        List<DCR_Field_Setting_MVN__mdt> wrappedSettings = fieldSettings.getSettings();
        System.assertEquals(10, wrappedSettings.size());
    }
}