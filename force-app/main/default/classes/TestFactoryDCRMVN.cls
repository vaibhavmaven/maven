/**
 * TestFactoryDCRMVN
 * @author: Pablo Roldan
 * @created Date: October 2017
 * @description: This class is used to construct DCR objects
 */
@isTest
public class TestFactoryDCRMVN {

    private TestFactorySObjectMVN objectFactory;
    private Map<String, RecordType> recordTypes;

    public TestFactoryDCRMVN() {
        Map<String, Object> defaultValues = new Map<String, Object>{'Country_MVN__c' => 'ZZ',
																	'Status_MVN__c' => 'Pending'};

        objectFactory = new TestFactorySObjectMVN('Data_Change_Request_MVN__c', defaultValues);
		recordTypes = UtilitiesMVN.getRecordTypesForObject('Data_Change_Request_MVN__c');
		System.debug('****' +recordTypes );
    }

	public Data_Change_Request_MVN__c constructAccountDCR(Id accId, Map<String, Object> valuesByField) {
		String accRecordTypeId = recordTypes.get(VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount).Id;
        Map<String, Object> creationValues = new Map<String, Object> {
            'RecordTypeId' => accRecordTypeId,
            'Notes_MVN__c' => 'DCR-'+ accId,
            'Account_MVN__c' => accId
        };

        creationValues.putAll(valuesByField);
        return (Data_Change_Request_MVN__c) objectFactory.constructSObject(creationValues);
	}

    public Data_Change_Request_MVN__c createAccountDCR(Id accId, Map<String, Object> valuesByField){
        Data_Change_Request_MVN__c dcr = constructAccountDCR(accId, valuesByField);
		insert dcr;
		return dcr;
    }

	public Data_Change_Request_MVN__c constructParentDCR(Id accId, Map<String, Object> valuesByField) {
		String accRecordTypeId = recordTypes.get(VeevaNetworkConnectionUtilMVN.networkSettings.dcrParent).Id;
        Map<String, Object> creationValues = new Map<String, Object> {
            'RecordTypeId' => accRecordTypeId,
            'Notes_MVN__c' => 'DCR-'+ accId,
            'Parent_Account_MVN__c' => accId
        };

        creationValues.putAll(valuesByField);
        return (Data_Change_Request_MVN__c) objectFactory.constructSObject(creationValues);
	}

    public Data_Change_Request_MVN__c createParentDCR(Id accId, Map<String, Object> valuesByField){
        Data_Change_Request_MVN__c dcr = constructParentDCR(accId, valuesByField);
		insert dcr;
		return dcr;
    }

    public Data_Change_Request_MVN__c constructAddressDCR(Id addressId, Map<String, Object> valuesByField){
        String addressRecordTypeId = recordTypes.get(VeevaNetworkConnectionUtilMVN.networkSettings.dcrAddress).Id;

        Map<String, Object> creationValues = new Map<String, Object> {
            'RecordTypeId' => addressRecordTypeId,
            'Notes_MVN__c' => 'DCR-'+ addressId,
            'Address_MVN__c' => addressId
        };

        creationValues.putAll(valuesByField);
        return (Data_Change_Request_MVN__c) objectFactory.constructSObject(creationValues);
    }

    public Data_Change_Request_MVN__c createAddressDCR(Id addressId, Map<String, Object> valuesByField){
        Data_Change_Request_MVN__c dcr = constructAddressDCR(addressId, valuesByField);
		insert dcr;
		return dcr;
    }

    public Data_Change_Request_MVN__c construct(Map<String,Object> valuesByField) {
        return (Data_Change_Request_MVN__c) objectFactory.constructSObject(valuesByField);
    }

    public Data_Change_Request_MVN__c create(Map<String,Object> valuesByField) {
        return (Data_Change_Request_MVN__c) objectFactory.createSObject(valuesByField);
    }
}