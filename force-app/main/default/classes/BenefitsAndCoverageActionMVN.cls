/**
 * @author Mavens
 * @description When the Benefits Investigation Status is updated to 'Eligible' call the action engine
 *              to perfom the automated actions defined in custom metadata
 * @group ActionEngine
 */
public with sharing class BenefitsAndCoverageActionMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
     * @description handle
     */
    public void handle() {
        for (Id benefitsAndCoverageId : trigger.newMap.keySet()) {
            if (this.benefitsStatusHasChangedToEligible(benefitsAndCoverageId) || this.benefitsStatusHasChangedToNotEligible(benefitsAndCoverageId)) {
                (new ActionEngineMVN(((Benefits_Coverage_MVN__c)trigger.newMap.get(benefitsAndCoverageId)).Program_Member_MVN__c)).run(ActionEngineMVN.BENEFITACTION);
            }
        }
    }

    /**
     * @description returns true if the benefits investigation status has changed to 'Eligible'
     * @param Id this benefits and coverage id
     * @return Boolean true if the status has changed to Eligible
     */
    private Boolean benefitsStatusHasChangedToEligible(Id benefitsAndCoverageId) {
        if (trigger.isInsert) {
            return (((Benefits_Coverage_MVN__c)trigger.newMap.get(benefitsAndCoverageId)).Benefits_Investigation_Status_MVN__c ==
                Patient_Service_Settings_MVN__c.getInstance().Benefits_Coverage_Status_Eligible_MVN__c);

        } else {
            return ((((Benefits_Coverage_MVN__c)trigger.oldMap.get(benefitsAndCoverageId)).Benefits_Investigation_Status_MVN__c !=
                ((Benefits_Coverage_MVN__c)trigger.newMap.get(benefitsAndCoverageId)).Benefits_Investigation_Status_MVN__c) &&
                (((Benefits_Coverage_MVN__c)trigger.newMap.get(benefitsAndCoverageId)).Benefits_Investigation_Status_MVN__c ==
                Patient_Service_Settings_MVN__c.getInstance().Benefits_Coverage_Status_Eligible_MVN__c));
        }
    }

    /**
     * @description returns true if the benefits investigation status has changed to 'Not Eligible'
     * @param Id this benefits and coverage id
     * @return Boolean true if the status has changed to Eligible
     */
    private Boolean benefitsStatusHasChangedToNotEligible(Id benefitsAndCoverageId) {
        if (trigger.isInsert) {
            return (((Benefits_Coverage_MVN__c)trigger.newMap.get(benefitsAndCoverageId)).Benefits_Investigation_Status_MVN__c ==
                Patient_Service_Settings_MVN__c.getInstance().Benefits_Coverage_Status_NotEligible_MVN__c);

        } else {
            return ((((Benefits_Coverage_MVN__c)trigger.oldMap.get(benefitsAndCoverageId)).Benefits_Investigation_Status_MVN__c !=
                ((Benefits_Coverage_MVN__c)trigger.newMap.get(benefitsAndCoverageId)).Benefits_Investigation_Status_MVN__c) &&
                (((Benefits_Coverage_MVN__c)trigger.newMap.get(benefitsAndCoverageId)).Benefits_Investigation_Status_MVN__c ==
                Patient_Service_Settings_MVN__c.getInstance().Benefits_Coverage_Status_NotEligible_MVN__c));
        }
    }
}