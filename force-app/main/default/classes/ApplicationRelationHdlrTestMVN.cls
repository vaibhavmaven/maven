/**
 * @author Mavens
 * @description tests ApplicationRelationHdlrMVN
 * @group Application
 */
@isTest private class ApplicationRelationHdlrTestMVN {
    private static User caseManager;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;

    /**
    * @description setups data for all unit tests
    */
    static {
        TestFactoryCustomMetadataMVN.setMocks();
        TestDataFactoryMVN.createPSSettings();

        caseManager = TestDataFactoryMVN.generateCaseManager();
        System.runAs(caseManager) {
            Program_MVN__c program = TestDataFactoryMVN.createProgram();
            Account testAccount = TestDataFactoryMVN.createMember();
            programMember = TestDataFactoryMVN.createProgramMember(program, testAccount);
            application = new Application_MVN__c(
                Program_Member_MVN__c = programMember.Id
            );
            insert application;
        }
    }

    @isTest private static void onAuthAndConsentInsertAddActiveAppFromProgramMember() {
        Authorization_and_Consent_MVN__c expectedAuthAndConsent;

        System.runAs(caseManager) {
            Test.startTest();
                expectedAuthAndConsent = new Authorization_and_Consent_MVN__c(
                    Program_Member_MVN__c = programMember.Id
                );
                insert expectedAuthAndConsent;
            Test.stopTest();
        }

        Authorization_and_Consent_MVN__c actualAuthAndConsent = [
            SELECT
                Id,
                Application_MVN__c
            FROM
                Authorization_and_Consent_MVN__c
            WHERE
                Id = :expectedAuthAndConsent.Id
        ];

        System.assertEquals(
            application.Id,
            actualAuthAndConsent.Application_MVN__c
        );
    }

    @isTest private static void onBenefitsCoverageInsertAddActiveAppFromProgramMember() {
        Benefits_Coverage_MVN__c expectedBenefitsCoverage;

        System.runAs(caseManager) {
            Test.startTest();
                expectedBenefitsCoverage = new Benefits_Coverage_MVN__c(
                    Program_Member_MVN__c = programMember.Id
                );
                insert expectedBenefitsCoverage;
            Test.stopTest();
        }

        Benefits_Coverage_MVN__c actualBenefitsCoverage = [
            SELECT
                Id,
                Application_MVN__c
            FROM
                Benefits_Coverage_MVN__c
            WHERE
                Id = :expectedBenefitsCoverage.Id
        ];

        System.assertEquals(
            application.Id,
            actualBenefitsCoverage.Application_MVN__c
        );
    }

    @isTest private static void onCaseInsertAddActiveAppFromProgramMember() {
        Case expectedCase;

        System.runAs(caseManager) {
            Test.startTest();
                expectedCase = new Case(
                    Program_Member_MVN__c = programMember.Id
                );
                insert expectedCase;
            Test.stopTest();
        }

        Case actualCase = [
            SELECT
                Id,
                Application_MVN__c
            FROM
                Case
            WHERE
                Id = :expectedCase.Id
        ];

        System.assertEquals(
            application.Id,
            actualCase.Application_MVN__c
        );
    }

    @isTest private static void onIcomeInsertAddActiveAppFromProgramMember() {
        Income_MVN__c expectedIcome;

        System.runAs(caseManager) {
            Test.startTest();
                expectedIcome = new Income_MVN__c(
                    Program_Member_MVN__c = programMember.Id, 
                    Application_MVN__c = application.Id
                );
                insert expectedIcome;
            Test.stopTest();
        }

        Income_MVN__c actualIcome = [
            SELECT
                Id,
                Application_MVN__c
            FROM
                Income_MVN__c
            WHERE
                Id = :expectedIcome.Id
        ];

        System.assertEquals(
            application.Id,
            actualIcome.Application_MVN__c
        );
    }

    @isTest private static void onPrescriptionInsertAddActiveAppFromProgramMember() {
        Prescription_MVN__c expectedPrescription;

        System.runAs(caseManager) {
            Product_MVN__c product = new Product_MVN__c(
                Program_MVN__c = programMember.Program_MVN__c,
                Product_Status_MVN__c = 'Active');
            insert product;
            Test.startTest();
                expectedPrescription = new Prescription_MVN__c(
                    Program_Member_MVN__c = programMember.Id,
                    Product_MVN__c = product.Id
                );
                insert expectedPrescription;
            Test.stopTest();
        }

        Prescription_MVN__c actualPrescription = [
            SELECT
                Id,
                Application_MVN__c
            FROM
                Prescription_MVN__c
            WHERE
                Id = :expectedPrescription.Id
        ];

        System.assertEquals(
            application.Id,
            actualPrescription.Application_MVN__c
        );
    }
}