/**
 *	ContactInfoNewTriggerMVN
 *	Created By:		Aggen
 *	Created On:		01/26/2016
 *  Description:	This is a trigger class that creates new Communication Preference records to mirror the Contact Information for any 
 *					Program Members related to Contact Information account
 *					
 **/
public class ContactInfoNewTriggerMVN implements TriggerHandlerMVN.HandlerInterface {
	public void handle() {
		Map<Id,List<Contact_Information_MVN__c>> acctContactInfoMap = new Map<Id,List<Contact_Information_MVN__c>>();
		List<Communication_Preference_MVN__c> newCommunicationPrefs = new List<Communication_Preference_MVN__c>();

		for(Contact_Information_MVN__c newContactInfo : (List<Contact_Information_MVN__c>) Trigger.new) {
			if(acctContactInfoMap.containsKey(newContactInfo.Account_MVN__c)) {
				List<Contact_Information_MVN__c> contactInfos = acctContactInfoMap.get(newContactInfo.Account_MVN__c);
				contactInfos.add(newContactInfo);
				acctContactInfoMap.put(newContactInfo.Account_MVN__c,contactInfos);
			} else {
				List<Contact_Information_MVN__c> contactInfos = new List<Contact_Information_MVN__c>();
				contactInfos.add(newContactInfo);
				acctContactInfoMap.put(newContactInfo.Account_MVN__c,contactInfos);
			}
		}

		for(Program_Member_MVN__c pm : [select Member_MVN__c, Id from Program_Member_MVN__c where Member_MVN__c in :acctContactInfoMap.keySet()]) {
			for(Contact_Information_MVN__c ci : acctContactInfoMap.get(pm.Member_MVN__c)) {
				Communication_Preference_MVN__c cp = new Communication_Preference_MVN__c();
				cp.Contact_Information_MVN__c = ci.Id;
				cp.Program_Member_MVN__c = pm.Id;
				newCommunicationPrefs.add(cp);
			}
		}

		if(newCommunicationPrefs.size() > 0) {
			insert newCommunicationPrefs;
		}
	}
}