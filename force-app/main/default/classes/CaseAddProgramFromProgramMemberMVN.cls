/**
 * @author Mavens
 * @description Copy Case.Program_Member_MVN__r.Program_MVN__c into Case.Program_MVN__c
 * @group Case
 **/
 
public with sharing class CaseAddProgramFromProgramMemberMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
     * @description handle
     */
    public void handle() {
        List<Id> programMemberIds = new List<Id>();

        for(Case myCase : (List<Case>) trigger.new) {
            if (myCase.Program_Member_MVN__c != null) {
                programMemberIds.add(myCase.Program_Member_MVN__c);
            }
        }

        if (!programMemberIds.isEmpty()) {
            Map<Id, Program_Member_MVN__c> programMembersById = new Map<Id, Program_Member_MVN__c>(
                                                                [SELECT Program_MVN__c
                                                                FROM Program_Member_MVN__c
                                                                WHERE Id IN :programMemberIds]); //Added limit one by Knipper Admin 1/9/20

            for (Case myCase : (List<Case>) trigger.new) {
                if (programMembersById.containsKey(myCase.Program_Member_MVN__c)) {
                    myCase.Program_MVN__c = programMembersById.get(myCase.Program_Member_MVN__c).Program_MVN__c;
                }   
            }
        }
        
    }
}