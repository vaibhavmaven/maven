/*
 * DCRUtilityMVN
 * Created By: Rick Locke
 * Created Date: October 2017
 * Description: Methods that are used in multiple DCR classes.
 */
public class DCRUtilityMVN {

    public List<RecordType> dcrRecordTypes {
        get {
            if (dcrRecordTypes == null) {
                dcrRecordTypes = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Data_Change_Request_MVN__c'];
            }
            return dcrRecordTypes;
        }
        public set;
    }

    public String getRecordTypeDeveloperName(String recordTypeId, String SObjectTypeName) {
        if(SObjectTypeName == 'Account') {
            for(RecordType rt : accountRecordTypes) {
                if(rt.Id == recordTypeId) {
                    return rt.DeveloperName;
                }
            }
        } else if (SObjectTypeName == 'Address_vod__c') {
            for(RecordType rt : addressRecordTypes) {
                if(rt.Id == recordTypeId) {
                    return rt.DeveloperName;
                }
            }
        }
        return null;
    }

    public List<RecordType> accountRecordTypes {
        get {
            if (accountRecordTypes == null) {
                Object existingAccountRecordTypes = IntegrationTokensCacheMVN.get(IntegrationTokensCacheMVN.ACCOUNT_RECORD_TYPES);

                if(existingAccountRecordTypes == null) {
                    existingAccountRecordTypes = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Account' OR SObjectType = 'Address_vod__c'];
                    IntegrationTokensCacheMVN.put(IntegrationTokensCacheMVN.ACCOUNT_RECORD_TYPES, (List<RecordType>) existingAccountRecordTypes, 60000);
                }

                accountRecordTypes = (List<RecordType>) existingAccountRecordTypes;
            }
            return accountRecordTypes;
        }
        public set;
    }

    public List<RecordType> addressRecordTypes {
        get {
            if (addressRecordTypes == null) {
                addressRecordTypes = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Address_vod__c' OR SObjectType = 'Address_vod__c'];
            }
            return addressRecordTypes;
        }
        public set;
    }

    public List<DCR_Field_Setting_MVN__mdt> buildMockFieldSettingsForTest() {
        return new List<DCR_Field_Setting_MVN__mdt>{
            metadataBuilder('FirstName','Account','Professional_vod',null,true),
            metadataBuilder('LastName','Account','Professional_vod',null,true),
            metadataBuilder('Gender_MVN__c','Account','Professional_vod',null,false),
            metadataBuilder('Name','Account','Hospital_vod',null,true),
            metadataBuilder('Name','Address_vod__c','All',null,true),
            metadataBuilder('Address_Line_2_vod__c','Address_vod__c','All',null,false),
            metadataBuilder('City_vod__c','Address_vod__c','All',null,true),
            metadataBuilder('Country_vod__c','Address_vod__c','All',null,false),
            metadataBuilder('State_vod__c','Address_vod__c','All',null,false),
            metadataBuilder('Name','License_MVN__c','All',null,false)
        };
    }

    public DCR_Global_Setting_MVN__mdt buildMockDCRSettingsForTest() {
        return (DCR_Global_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
            'DCR_Global_Setting_MVN__mdt',
            new Map<String, Object>{'DCR_Affiliation_Processing_MVN__c' => true,
                                    'DCR_Approval_Required_for_Update_MVN__c' => true,
                                    'DCR_Create_Handler_Class_MVN__c' => 'DCRCreationHdlrMVN',
                                    'DCR_Excluded_Profile_Names_MVN__c' => 'Test Profile',
                                    'DCR_Globally_Active_MVN__c' => true}
        );
    }

    public DCR_Field_Setting_MVN__mdt metadataBuilder(String field, String objectType, String accountRecordType, String addressRecordType, Boolean requiredForSave) {
        return (DCR_Field_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
            'DCR_Field_Setting_MVN__mdt',
            new Map<String, Object>{'Field_MVN__c'=> field,
                                    'Object_MVN__c'=> objectType,
                                    'Account_Record_Type_MVN__c'=>accountRecordType,
                                    'Address_Record_Type_MVN__c'=>addressRecordType,
                                    'Required_for_DCR_Transmission_MVN__c'=>requiredForSave,
                                    'Active_MVN__c'=>true}
        );
    }
}