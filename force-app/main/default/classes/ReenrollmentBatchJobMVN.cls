/**
 * @author      Mavens
 * @date        December 2018
 * @group       ReenrollmentBatchJob
 * @description Batch job that identifies all app
 * 
 * licactions ready for re-enrollment and create/send
 *              the appropriate letters
 */
global class ReenrollmentBatchJobMVN implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable {

    private Map<String, Loop__DDP__c> ddpsByFilter;

    /**
     * @description Exception Class to be thrown in event of error
     */
    global class ReenrollmentBatchJobException extends Exception {}

    /**
     * implement the execute method of the scheudlable interface
     */
    global void execute(SchedulableContext sc) {
        ReenrollmentBatchJobMVN.run();
    }

    /**
     * @description Static helper method to kick off the batch execution with the proper batch size
     */
    global static void run() {
        Database.executeBatch( new ReenrollmentBatchJobMVN(), 100 );
    }

    /**
     * @description Return the query locator for all applications having an application reminder date
     *              or application reminder follow up date set to a date less then or equal to today
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT
                Id,
                Program_Member_MVN__c,
                Program_Member_MVN__r.Program_MVN__r.Program_ID_MVN__c,
                Application_Reminder_Date_MVN__c,
                Application_Reminder_Follow_Up_Date_MVN__c
            FROM
                Application_MVN__c
            WHERE
                (
                    Application_Reminder_Date_MVN__c <= TODAY
                    OR
                    Application_Reminder_Follow_Up_Date_MVN__c <= TODAY
                )
            AND
                Status_MVN__c = 'Fulfilled'
            AND
                Is_Application_Active_MVN__c = true

        ]);
    }

    /**
     * @description for all applications in scope find all relevant DDP records and create the
     *              correct loop message request for each application. clear out the application
     *              reminder date and/or application reminder followup date if less than or equal
     *              to today.
     */
    global void execute(Database.BatchableContext BC, List<Application_MVN__c> applications) {
        if (applications.size() > 100) {
            throw new ReenrollmentBatchJobException('Batch size must be 100 or less due to callouts');
        }

        Loop.loopMessage drawloopMessage = new Loop.loopMessage();
        drawloopMessage.sessionId = UserInfo.getSessionId();

        try {
            ddpsByFilter = getDDPs(applications);

            for (Application_MVN__c application : applications) {

                if (application.Application_Reminder_Date_MVN__c <= Date.today()) {
                    Loop.loopMessage.loopMessageRequest req = createRequest(application, false);
                    if (req != null) {
                        drawLoopMessage.requests.add( req );
                    }
                }

                if (application.Application_Reminder_Follow_Up_Date_MVN__c <= Date.today()) {
                    Loop.loopMessage.loopMessageRequest req = createRequest(application, true);
                    if (req != null) {
                        drawLoopMessage.requests.add( req );
                    }
                }
            }

            drawLoopMessage.sendAllRequests();

        } catch (Exception e) {
            // insert into the log but also throw so the batch doesn't show as successful
            insert new System_Log_MVN__c(Logged_Message_MVN__c = 'ReenrollmentBatchJobMVN error: ' + e.getMessage());
            throw e;
        }
    }

    /**
     * @description create drawloop message requests
     * @param application   The application being processed
     * @param Boolean       Indicates whether the filter is for followup or not
     * @return              The loopMessage message request to add to the loop message
     */
    private Loop.loopMessage.loopMessageRequest createRequest(Application_MVN__c application, Boolean followup) {
        String ddpFilter = getFitlerId(application, followup);

        if (ddpsByFilter.containsKey( ddpFilter )) {
            Loop__DDP__c ddp = ddpsByFilter.get( ddpFilter );
            return new Loop.loopMessage.loopMessageRequest(
                application.Program_Member_MVN__c,
                ddp.Id,
                new Map<string, string>{
                    'deploy' => ddp.Loop__Custom_Integration_Options__r[0].Id
                }
            );
        }
        return null;
    }

    /**
     * @description format filter id
     * @param application   The application being processed
     * @param Boolean       Indicates whether the filter is for followup or not
     * @return              The ddp filter id corresponding to the applicatin program
     */
    private String getFitlerId(Application_MVN__c application, Boolean followup) {
        String programId = application.Program_Member_MVN__r.Program_MVN__r.Program_ID_MVN__c;
        String filter = programId + '-PM-Re-Enrollment';
        if (followup) {
            filter += '-Followup';
        }
        return filter;
    }

    /**
     * @description return the DDPs that might be needed based on applications in scope
     * @param application   The application being processed
     * @param Boolean       Indicates whether the filter is for followup or not
     * @return              Map of DDPs by their filter names. Each DDP includes the SFTP delivery
     *                      option for that DDP
     */
    private Map<String, Loop__DDP__c> getDDPs(List<Application_MVN__c> applications) {
        List<String> filters = new List<String>();
        for (Application_MVN__c application : applications) {
            filters.add( getFitlerId(application, false) );
            filters.add( getFitlerId(application, true) );
        }

        List<Loop__DDP__c> docGenPackageList = [
            SELECT
                Id,
                Loop__Filter__c,
                (
                    SELECT
                        Id,
                        RecordType.Name,
                        Loop__Attach_As__c
                    FROM
                        Loop__Custom_Integration_Options__r
                    WHERE
                        Loop__Attach_As__c = 'SFTP'
                )
            FROM
                Loop__DDP__c
            WHERE
                Loop__Filter__c IN :filters
        ];

        Map<String, Loop__DDP__c> ddpsByFilter = new Map<String, Loop__DDP__c>();
        for (Loop__DDP__c docGenPackage : docGenPackageList) {
            ddpsByFilter.put(docGenPackage.Loop__Filter__c, docGenPackage);
        }

        return ddpsByFilter;
    }

    global void finish(Database.BatchableContext BC) {

    }
}