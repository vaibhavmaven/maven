/**
 * @author Mavens
 * @description This is the main controller of the Enrollment VF Page. Data is initialised and template logic is implemented.
 * @group MasterActivity
 */

 public with sharing class MasterActivityControllerMVN {

    public Id caseId;
    public Case caseRecord {get;set;}
    public Program_Member_MVN__c programMember {get;set;}

    public Map<Id, Program_Activity_MVN__mdt> activityMap {get;set;}
    public Map<String, List<Program_Activity_Page_MVN__mdt>> activityToPagesMap {get;set;}
    public Map<String, List<Program_Activity_Component_MVN__mdt>> pageToComponentsMap {get;set;}

    public List<SelectOption> activityOptions {get;set;}
    public Boolean activityNavigationDisabled {get;set;}
    public Id selectedActivity {get; set;}
    public Boolean activityFound {get;set;}

    public Integer numOfPages {get;set;}
    public Integer currentPage {get;set;}
    public Program_Activity_Page_MVN__mdt activePage {
        get {
            return activityToPagesMap.get(selectedActivity)[currentPage];
        }
        set;
    }

    private final String DUPLICATE_ERROR = Label.Master_Activity_Duplicate_Error_MVN;

    public Map<Id,ProgramActivityComponentControllerMVN> componentControllerMap {get;set;}
    public Map<Id,SObject> componentObjectMap {get;set;}
    public List<Program_Activity_Component_MVN__mdt> activeComponents {
        get {
            return pageToComponentsMap.get(activePage.Id);
        }
        set;
    }
    public Boolean componentsFound {
        get {
            if (numOfPages == -1) {
                return false; // -1 pages is an error
            } else {
                return true;
            }
        }
        set;
    }
     public Boolean isFinished { get; set; }

    /**
     * Called to pass the MasterActivityController to the Component
     * @return MasterActivityControllerMVN
     */
    public MasterActivityControllerMVN getThis(){
        return this;
    }

    /**
     * Getter/Setter for Boolean determining if Previous button be rendered on current page
     */
    public Boolean showPrevious {
        get {
            if(currentPage > 0 && selectedActivity != null && componentsFound) {
                return true;
            } else {
                return false;
            }
        }
        set;
    }

    /**
     * Getter/Setter for Boolean determining if Next button be rendered on current page
     */
    public Boolean showNext {
        get {
            if(selectedActivity != null && currentPage != numOfPages && componentsFound) {
                return true;
            } else {
                return false;
            }
        }
        set;
    }

    /**
     * Getter/Setter for Boolean determining if Finish button be rendered on current page
     */
    public Boolean showFinish {
        get {
            if(selectedActivity != null && currentPage == numOfPages && componentsFound) {
                return true;
            } else {
                return false;
            }
        }
        set;
    }

    /**
     * Getter/Setter for Boolean determining if Cancel button be rendered on current page
     */
    public Boolean showCancel {
        get {
            if(selectedActivity != null && currentPage == 0) {
                return true;
            } else {
                return false;
            }
        }
        set;
    }

    public Boolean allowPageNavigation;
    public Map<String, Schema.SObjectType> globalDescribe;

    private static final Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();

    /**
     * Constructor
     */
    public MasterActivityControllerMVN() {

        globalDescribe = Schema.getGlobalDescribe();
        this.caseId = ApexPages.currentPage().getParameters().get('caseId');

        componentControllerMap = new Map<Id,ProgramActivityComponentControllerMVN>();
        componentObjectMap =  new Map<Id,SObject>();

        activityMap  = new Map<Id, Program_Activity_MVN__mdt>();
        activityToPagesMap  = new Map<String, List<Program_Activity_Page_MVN__mdt>>();
        pageToComponentsMap  = new Map<String, List<Program_Activity_Component_MVN__mdt>>();
        initializeData();

        activityFound = false;
        activityOptions  = new List<SelectOption>();
        activityOptions.add(new SelectOption('',Label.Select_Activity_Default_Text_MVN));

        for(Program_Activity_MVN__mdt activity : [SELECT Title_MVN__c, QualifiedApiName, Program_External_Id_MVN__c, NamespacePrefix, MasterLabel, Language, Label, Id, DeveloperName, Activity_Sequence_Number_MVN__c
                                                    FROM Program_Activity_MVN__mdt
                                                   WHERE Program_External_Id_MVN__c = :programMember.Program_MVN__r.Program_Id_MVN__c
                                                     AND Active_MVN__c = true
                                                ORDER BY Activity_Sequence_Number_MVN__c]) {
            activityOptions.add(new SelectOption(activity.Id, activity.Title_MVN__c));
            activityMap.put(activity.Id, activity);
            activityFound = true;
        }

        for(Program_Activity_Page_MVN__mdt page : [SELECT Id, DeveloperName, MasterLabel, Language, NamespacePrefix, Label, Page_Sequence_Number_MVN__c, QualifiedApiName, Program_Activity_MVN__c
                                                     FROM Program_Activity_Page_MVN__mdt
                                                    WHERE Program_Activity_MVN__r.Program_External_Id_MVN__c = :programMember.Program_MVN__r.Program_Id_MVN__c
                                                      AND Active_MVN__c = true
                                                 ORDER BY Page_Sequence_Number_MVN__c]) {
            if(activityToPagesMap.containsKey(page.Program_Activity_MVN__c)) {
                List<Program_Activity_Page_MVN__mdt> pageList = activityToPagesMap.get(page.Program_Activity_MVN__c);
                pageList.add(page);
                activityToPagesMap.put(page.Program_Activity_MVN__c, pageList);
            } else {
                activityToPagesMap.put(page.Program_Activity_MVN__c, new List<Program_Activity_Page_MVN__mdt>{page});
            }
        }

        for(Program_Activity_Component_MVN__mdt component : [SELECT Id, DeveloperName, Edit_Modal_Title_MVN__c, MasterLabel, Language, NamespacePrefix, Label, QualifiedApiName, Program_Activity_Page_MVN__c, Program_Member_Id_Field_MVN__c, Record_Type_DeveloperName_MVN__c, Title_MVN__c, SObject_MVN__c, Field_Set_MVN__c, Mode_MVN__c, Component_Type_MVN__c, Component_Sequence_Number_MVN__c
                                                               FROM Program_Activity_Component_MVN__mdt
                                                              WHERE Program_Activity_Page_MVN__r.Program_Activity_MVN__r.Program_External_Id_MVN__c = :programMember.Program_MVN__r.Program_Id_MVN__c
                                                                AND Active_MVN__c = true
                                                           ORDER BY Component_Sequence_Number_MVN__c]) {
            if(pageToComponentsMap.containsKey(component.Program_Activity_Page_MVN__c)) {
                List<Program_Activity_Component_MVN__mdt> componentList = pageToComponentsMap.get(component.Program_Activity_Page_MVN__c);
                componentList.add(component);
                pageToComponentsMap.put(component.Program_Activity_Page_MVN__c, componentList);
            } else {
                pageToComponentsMap.put(component.Program_Activity_Page_MVN__c, new List<Program_Activity_Component_MVN__mdt>{component});
            }
        }
    }

    /**
     * Initializes the caseRecord
     */
    public void initializeData(){
        if(this.caseId != null){
            caseRecord = [SELECT Program_Member_MVN__c
                            FROM Case
                           WHERE Id = :caseId];

            if(caseRecord.Program_Member_MVN__c != null){
                programMember = [SELECT Name,
                                        Program_MVN__c,
                                        Program_MVN__r.Program_Id_MVN__c,
                                        Active_Application_MVN__c
                                   FROM Program_Member_MVN__c
                                  WHERE Id = :caseRecord.Program_Member_MVN__c];
            } else {
                programMember = new Program_Member_MVN__c();
            }
        }
    }


    /**
     * This is called when the activity changes
     */
    public void activityChanged() {
        componentControllerMap = new Map<Id,ProgramActivityComponentControllerMVN>();
        if (activityToPagesMap.containsKey(selectedActivity)) {
            numOfPages = activityToPagesMap.get(selectedActivity).size() - 1;
        } else {
            numOfPages = -1; // selectedActivity was not found to have pages.
        }
        currentPage = 0;
        activityNavigationDisabled = true;
    }

    /**
     * Cancel button
     */
    public void cancel() {
        activityNavigationDisabled = false;
        selectedActivity = null;
    }

    /**
     * Previous button
     */
    public void previous() {
        if(saveComponentRecords(true) || allowPageNavigation) {
            ApexPages.getMessages().clear();
            currentPage--;
        }
    }

    /**
     * Next button
     */
    public void next() {
        if(saveComponentRecords(true) || allowPageNavigation) {
            ApexPages.getMessages().clear(); // clears error messages in the event of ignoring
            currentPage++;
        }
    }


    /**
     * Finish button
     */
    public void finish() {
        if(saveComponentRecords(true) || allowPageNavigation) {
            activityNavigationDisabled = false;

            Boolean isNotViewMode = false;
            for(Program_Activity_Component_MVN__mdt componentRecord : activeComponents) {
                if(componentRecord.Mode_MVN__c != 'View') {
                    isNotViewMode = true;
                }
            }

            selectedActivity = null;

            setActiveApplication();

            if(isNotViewMode) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,Label.Activity_Save_Successful_MVN));
            }

            (new ActionEngineMVN(programMember.Id)).run(ActionEngineMVN.ENROLMENTACTION);

			this.caseRecord.Status = 'Closed';
            update this.caseRecord;

            this.isFinished = true;
        }
    }

    /**
     * Called when user hits return.
     */
    public void returnKeyPress() {
        if(showFinish) {
            finish();
        } else if (showNext) {
            next();
        }
    }

    /**
     * Saves the records assigned to all active components, returns boolean is true if there is an error.
     * @return Boolean
     */
    public Boolean saveComponentRecords() {
        // call overrided method
        return saveComponentRecords(false);
    }

    /**
     * Saves the records assigned to all active components, returns boolean is true if there is an error.
     * @param navigationRequest
     * @return Boolean
     */
    public Boolean saveComponentRecords(Boolean navigationRequest) {
        // reset boolean
        allowPageNavigation = false;

        // Generic SOBject Lists are not allowed to upsert, so the lists must be split up.
        List<Sobject> insertRecords = new List<Sobject>();
        List<Sobject> updateRecords = new List<Sobject>();
        List<SObjectValidationWrapper> validationWrapperList = new List<SObjectValidationWrapper>();

        if(activeComponents == null) {
            return true; // nothing to save, but it must return to allow navigation
        } else {
            for(Program_Activity_Component_MVN__mdt componentRecord : activeComponents) {
                // saves are only performed from master are only performed on Detail components, if another custom component is added the logic may change
                if(componentRecord.Component_Type_MVN__c == 'Detail') {
                    SObject record = componentObjectMap.get(componentRecord.Id);
                    if(record == null) {
                        continue;
                    }
                    // if it's 'Create' mode then it's an insert UNLESS the componentController was rendered after a previous button navigation, in which case it's an update as it was already inserted.
                    if(componentRecord.Mode_MVN__c == 'Create') {
                        validationWrapperList.add(new SObjectValidationWrapper(componentRecord, record));
                        // Default the Application Type to New/Re-Enrollment based on the activity selection
                        defaultApplicationType(record);
                        insertRecords.add(record);
                    // componentRecord is an Update if it's an 'Edit' component mode OR if it's a 'Create' component mode and the component was rendered via previous button meaning it was already inserted.
                    } else if(componentRecord.Mode_MVN__c == 'Edit') {
                        validationWrapperList.add(new SObjectValidationWrapper(componentRecord, record));
                        updateRecords.add(record);
                    }
                }
            }
        }

        Boolean formEmptyException = true; // can the form allow page navigation bc it's empty
        Boolean hasError = false; // do any components have error?

        for(SObjectValidationWrapper fieldValidationWrapper : validationWrapperList) {
            if(fieldValidationWrapper.hasError) {
                hasError = true;
            }
			if(!fieldValidationWrapper.hasNoFieldsFilledError) {
                formEmptyException = false;
            }
        }

        if(navigationRequest && formEmptyException) {
            allowPageNavigation = true; // no fields filled, allow navigation.
            return true;
        }

        if(hasError) {
            for(SObjectValidationWrapper fieldValidationWrapper : validationWrapperList) {
                addPageMessageErrors(fieldValidationWrapper);
            }
            if(!Test.isRunningTest()) {
                // not practical to fill non-required fields through unit tests with dynamic configuration
                return false;
            }
        }

        Savepoint sp = Database.setSavepoint();
        try {
            update updateRecords;
            insert insertRecords;
         } catch (Exception e) {
            Database.rollback(sp);
            if (e.getTypeName() == 'System.DmlException' && ((DmlException) e).getDmlType(0) == System.StatusCode.DUPLICATE_VALUE) {
                ApexPages.addMessage(
                    new ApexPages.Message(
                        ApexPages.Severity.Error,
                        DUPLICATE_ERROR
                    )
                );
            } else {
                ApexPages.addMessages(e);
            }
            return false; // fail
        }

        // remove the objects from the componentObjectMap, this will force the components to start from scratch if rerendered.
        for(Program_Activity_Component_MVN__mdt componentRecord : activeComponents) {
            componentObjectMap.remove(componentRecord.Id);
        }
        return true; // success
    }

    /**
     * Default the Application Type field on the Application to New/Re-Enrollment
     * based on the activity selection
     * @param  record SObject record being processed
     */
    private void defaultApplicationType(SObject record) {
        if (String.valueOf(record.getSObjectType()) == 'Application_MVN__c') {
            String activityTitle = activityMap.get(selectedActivity).Title_MVN__c;
            if (activityTitle == settings.Activity_Title_For_New_Enrollment_MVN__c) {
                record.put('Application_Type_MVN__c', settings.Application_type_value_for_New_MVN__c);
            } else if (activityTitle == settings.Activity_Title_For_Renewal_MVN__c) {
                record.put('Application_Type_MVN__c', settings.Application_type_value_for_Renewal_MVN__c);
            }
        }
    }

    /**
     * Saves the record assigned to an individual component usually called by the component, returns boolean is true if there is an error.
     * @param componentRecordId
     * @return Boolean
     */
    public Boolean saveComponentRecord(Id componentRecordId) {
        return saveComponentRecord(componentRecordId, false);
    }

    /**
     * Saves the record assigned to an individual component usually called by the component, returns boolean is true if there is an error.
     * @param componentRecordId
     * @param navigationRequest
     * @return Boolean
     */
    public Boolean saveComponentRecord(Id componentRecordId, Boolean navigationRequest) {
        // reset boolean
        allowPageNavigation = true;

        // Generic SOBject Lists are not allowed to upsert, so the lists must be split up.
        List<Sobject> insertRecords = new List<Sobject>();
        SObjectValidationWrapper fieldValidationWrapper;

        for(Program_Activity_Component_MVN__mdt componentRecord : activeComponents) {
            if(componentRecord.Component_Type_MVN__c == 'Detail' && componentRecord.Mode_MVN__c == 'Create' && componentRecordId == componentRecord.Id) {
                fieldValidationWrapper = new SObjectValidationWrapper(componentRecord, componentObjectMap.get(componentRecord.Id));
                SObject record = componentObjectMap.get(componentRecord.Id);
                // Default the Application Type to New/Re-Enrollment based on the activity selection
                defaultApplicationType(record);
                insertRecords.add(record);
            }
        }

        if(fieldValidationWrapper == null) {
            return true;
        } else if(fieldValidationWrapper.hasError) {
            addPageMessageErrors(fieldValidationWrapper);
            return false; // fail
        }

        Savepoint sp = Database.setSavepoint();
        try {
            insert insertRecords;
        } catch (Exception e) {
            Database.rollback(sp);
            if (e.getTypeName() == 'System.DmlException' && ((DmlException) e).getDmlType(0) == System.StatusCode.DUPLICATE_VALUE) {
                ApexPages.addMessage(
                    new ApexPages.Message(
                        ApexPages.Severity.Error,
                        DUPLICATE_ERROR
                    )
                );
            } else {
                ApexPages.addMessages(e);
            }
            return false; // fail
        }
        return true; // success
    }

    public void addPageMessageErrors(SObjectValidationWrapper fieldValidationWrapper) {
        if(fieldValidationWrapper.hasError) {
            if(fieldValidationWrapper.hasNoFieldsFilledError) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Activity_No_Fields_Filled));
            }
            for(String errorDescription : fieldValidationWrapper.errorDescriptions) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorDescription));
            }
        }
    }

    public class SObjectValidationWrapper {

        public Boolean hasError {get;set;}
        public Boolean hasNoFieldsFilledError {get;set;}
        public List<String> errorDescriptions {get;set;}

        /**
         * Constructor
         * @param componentRecord
         * @param record
         */
        public SObjectValidationWrapper (Program_Activity_Component_MVN__mdt componentRecord, SObject record) {
            Map<String,Object> fieldToValueMap = record.getPopulatedFieldsAsMap();

            hasError = false;
            hasNoFieldsFilledError = true;
            errorDescriptions = new List<String>();

            for(FieldSetMember field : (Schema.getGlobalDescribe().get(componentRecord.SObject_MVN__c).getDescribe().fieldSets.getMap().get(componentRecord.Field_Set_MVN__c)).getFields()) {
                if((field.getDBRequired() || field.getRequired()) && fieldToValueMap.get(field.getFieldPath()) == null) {
                    // add a message emulating Salesforce error.
                    errorDescriptions.add(field.getLabel() + Label.Activity_Required_Field_Error_MVN);
                    hasError = true;
                }
                // if field is filled and is not a system generated field then mark hasNoFieldsFilledError to false
                if((fieldToValueMap.get(field.getFieldPath()) != null
                    && !(field.getType() == Schema.DisplayType.Boolean && fieldToValueMap.get(field.getFieldPath()) == false)) // checkbox with false can be considered null
                    && field.getFieldPath() != componentRecord.Program_Member_Id_Field_MVN__c // if does not apply to this error
                    && field.getFieldPath() != 'RecordTypeId' // recordType does not apply to this error
                    && field.getFieldPath() != 'Id') { // id does not apply to this error

                    hasNoFieldsFilledError = false;
                }
            }

            // edit is exempt
            if(componentRecord.Mode_MVN__c == 'Edit') {
                hasNoFieldsFilledError = false;
            }

            if(hasNoFieldsFilledError) {
                hasError = true;
            }
        }
    }

    /**
     * Set Application as the Active Application defined in the Program Member.
     */
    private void setActiveApplication() {
        caseRecord.Application_MVN__c = [SELECT Active_Application_MVN__c
                                        FROM Program_Member_MVN__c
                                        WHERE Id = :programMember.Id].Active_Application_MVN__c;
        update caseRecord;
    }
}