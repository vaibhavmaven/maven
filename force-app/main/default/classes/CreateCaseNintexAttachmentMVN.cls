/**
 * @author Mavens
 * @date Oct 2018
 * @description When a Nintex doc is generated and sent via SFTP, a field on the case is updated.
 *              We use this field update to fire the attachment of that doc on the case.
 *              We need to do this due to a Nintex limitation.
 * @group Documents
 */
public with sharing class CreateCaseNintexAttachmentMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
     * @description handle
     */
    public void handle() {
        for (Id caseId : trigger.newMap.keySet()) {
            if (this.nintexDeliveryIdHasChanged(caseId)) {
                this.generateAttachment(caseId);
            }
        }
    }

    /**
     * @description returns true if the Nintex delivery Datetime has changed
     * @param Id of the case
     * @return Boolean true if the field has changed
     */
    private Boolean nintexDeliveryIdHasChanged(Id caseId) {
        return ((((Case)trigger.oldMap.get(caseId)).Nintex_Delivery_Datetime_MVN__c !=
            ((Case)trigger.newMap.get(caseId)).Nintex_Delivery_Datetime_MVN__c) && 
            (((Case)trigger.newMap.get(caseId)).Nintex_Delivery_Datetime_MVN__c != null) &&
            (((Case)trigger.newMap.get(caseId)).Nintex_Delivery_Id_MVN__c != null));
    }

    /**
     * Generate the Nintex document and attach it to the Case
     * @param  caseId Case Id
     */
    private void generateAttachment(Id caseId) {
        Id storedDeliveryId = getDeliveryIdFromCase(caseId);
        Id docGenPackageId = getPackageId(storedDeliveryId);
        Id deliveryOptionId = getDeliveryId(docGenPackageId);

        if (deliveryOptionId != null) {
            callNintex(caseId, docGenPackageId, deliveryOptionId, UserInfo.getSessionId());
        }
    }

    /**
     * Retrieve the SFTP delivery Id stored on the case
     * @param  caseId case Id
     * @return delivery Id for the SFTP method 
     */ 
    private Id getDeliveryIdFromCase(Id caseId) {
        return [
            SELECT 
                Nintex_Delivery_Id_MVN__c
            FROM
                Case 
            WHERE
                Id = :caseId
        ].Nintex_Delivery_Id_MVN__c;
    }

    /**
     * Retrieve the DocGen Package Id
     * @param  deliveryId Id of the delivery Id option linked to DocGen Package
     * @return DocGen Package Id
     */
    private Id getPackageId (Id deliveryId) {
        return [
            SELECT 
                Loop__DDP__c
            FROM 
                Loop__DDP_Integration_Option__c
            WHERE 
                Id = :deliveryId
            LIMIT 1
        ].Loop__DDP__c;
    }

    /**
    * Get delivery Id for the attachment delivery option
    * @param  packageId Package Id 
    * @return delivery option Id for Attachment
    */ 
    private Id getDeliveryId(Id packageId) {
        List<Loop__DDP_Integration_Option__c> deliveryOptions = [
            SELECT
                Id
            FROM
                Loop__DDP_Integration_Option__c
            WHERE 
                Loop__DDP__c = :packageId
            AND 
                RecordType.Name = 'Attach'
            AND 
                Loop__Attach_As__c = 'Attachment'
            LIMIT 1
        ];
        if (!deliveryOptions.isEmpty()) {
            return deliveryOptions.get(0).Id;
        }
        return null;
    }

    /**
     * Callout to Nintex server to generate the attachment doc on the case
     * @param  caseId  case Id
     * @param  docGenPackageId  DocGen Package Id to be exeucted
     * @param  deliveryOptionId delivery option id for the attachment method 
     */ 
    @future(callout=true)
    private static void callNintex(Id caseId, Id docGenPackageId, Id deliveryOptionId, String sessionId) {
        Loop.loopMessage thisLoopMessage = new Loop.loopMessage();
        thisLoopMessage.sessionId = sessionId;
        thisLoopMessage.requests.add(new Loop.loopMessage.loopMessageRequest(
            caseId, 
            docGenPackageId, 
            new Map<string, string>{
                'deploy' => deliveryOptionId 
            }
        ));
        thisLoopMessage.sendAllRequests();
    }
}