/**
 * @author Mavens
 * @date 08/2018
 * @description Class to manage retrieving the fields from the Eligibility Engine Setting custom metadata type
 */ 
public with sharing class EligibilityEngineSettingMVN {

    private static List<Eligibility_Engine_Setting_MVN__mdt> testMock;

    public static List<Eligibility_Engine_Setting_MVN__mdt> allSettings {
        get {
            if (allSettings == null) {
                Map<String, Schema.SObjectField> fieldMapping = 
                    Eligibility_Engine_Setting_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
                List<String> fields = new List<String>(fieldMapping.keySet());

                String queryString = 'SELECT ' + String.escapeSingleQuotes(String.join(fields, ','))
                                    + ' FROM Eligibility_Engine_Setting_MVN__mdt';
                allSettings = Database.query(queryString);
            }
            if(testMock != null){
                allSettings = testMock;
            }
            return allSettings;
        }
        private set;
    }

    public static Eligibility_Engine_Setting_MVN__mdt deniedSettings {
        get {
            if (deniedSettings == null) {
                for (Eligibility_Engine_Setting_MVN__mdt setting : allSettings) {
                    if (setting.DeveloperName == 'Denied_MVN') {
                        return setting;
                    }
                }
            } 
            return deniedSettings;
        }
        private set;
    }

    public static Eligibility_Engine_Setting_MVN__mdt passedSettings {
        get {
            if (passedSettings == null) {
                for (Eligibility_Engine_Setting_MVN__mdt setting : allSettings) {
                    if (setting.DeveloperName == 'Passed_MVN') {
                        return setting;
                    }
                }
            } 
            return passedSettings;
        }
        private set;
    }

    public static Eligibility_Engine_Setting_MVN__mdt missingInfoSettings {
        get {
            if (missingInfoSettings == null) {
                for (Eligibility_Engine_Setting_MVN__mdt setting : allSettings) {
                    if (setting.DeveloperName == 'Missing_Information_MVN') {
                        return setting;
                    }
                }
            } 
            return missingInfoSettings;
        }
        private set;
    }

    public static Eligibility_Engine_Setting_MVN__mdt invalidRuleSettings {
        get {
            if (invalidRuleSettings == null) {
                for (Eligibility_Engine_Setting_MVN__mdt setting : allSettings) {
                    if (setting.DeveloperName == 'Invalid_Rule_MVN') {
                        return setting;
                    }
                }
            } 
            return invalidRuleSettings;
        }
        private set;
    }

    public List<Eligibility_Engine_Setting_MVN__mdt> getEligibilityEngineSettingByField(String filterField, String filterValue) {
        List<Eligibility_Engine_Setting_MVN__mdt> filteredSetting = new List<Eligibility_Engine_Setting_MVN__mdt>();
        for (Eligibility_Engine_Setting_MVN__mdt setting : allSettings) {
            if (setting.get(filterField) == filterValue) {
                filteredSetting.add(setting);
            }
        }
        return filteredSetting;
    } 

    @TestVisible
    private static void setMocks(List<Eligibility_Engine_Setting_MVN__mdt> mocks){
        testMock = mocks;
    }
}