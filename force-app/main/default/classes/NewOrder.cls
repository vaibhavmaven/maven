global class NewOrder {
    webservice static string createOrder(String orderSchID){
        Savepoint sp = Database.setSavepoint();
        try{
            //Query Order Scheduler
            Order_MVN__c order;
            Order_Line_Item_MVN__c orderLineItem;
            for(Order_Scheduler_MVN__c orderSch : [Select Id, Application_MVN__c, Program_Member_MVN__c from Order_Scheduler_MVN__c where Id=:orderSchID]){
                order = new Order_MVN__c();
                order.Order_Scheduler_MVN__c = orderSch.Id;                
                order.Program_Member_MVN__c = orderSch.Program_Member_MVN__c;
                order.Status_MVN__c = 'Pending'; 
                order.Order_Date_MVN__c = Date.today().addDays(1); 	                
                order.Application_MVN__c = orderSch.Application_MVN__c;
                order.Allowable_Release_Date_MVN__c = Date.today();                
            }
            if(order != null){
                insert order;                
            }            
            orderLineItem = new Order_Line_Item_MVN__c();
            orderLineItem.Order_MVN__c = order.Id;
            insert orderLineItem;
            return order.Id;
        }catch(Exception e){
            System.debug('Exception in creating new order: Message: '+ e.getMessage());   
            System.debug('Exception stack trace: '+ e.getStackTraceString());                   
        }
        return null;
    }
}