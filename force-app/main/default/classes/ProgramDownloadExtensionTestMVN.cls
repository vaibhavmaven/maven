/**
*   ProgramDownloadExtensionTestMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 22nd, 2017
*   Description:    tests ProgramDeployMVN
*/
@isTest private class ProgramDownloadExtensionTestMVN {
	/**
    * @description setups data for all unit tests
    */
    @testSetup private static void setupData() {
        TestDataFactoryMVN.createPSSettings();
        Program_MVN__c program = TestDataFactoryMVN.createProgram();
        program.Program_Id_MVN__c = ProgramDeployWrapperTestMVN.EXTERNALID;
        update program;
        Program_Stage_MVN__c parentProgramStage = TestDataFactoryMVN.createParentProgramStage(program);
        Program_Stage_MVN__c programStageOne = TestDataFactoryMVN.createChildProgramStage(program,
                                                                                          parentProgramStage,
                                                                                          'Stage ONE');
        Program_Stage_MVN__c programStageTwo = TestDataFactoryMVN.createChildProgramStage(program,
                                                                                          parentProgramStage,
                                                                                          'Stage TWO');
        TestDataFactoryMVN.createStageDependency(programStageOne, programStageTwo);
        Medical_History_Type_MVN__c medHistType = new Medical_History_Type_MVN__c(
            Order_MVN__c = 1,
            Program_MVN__c = program.Id,
            Record_Type_Name_MVN__c = 'TEST'
        );
    }

    /**
    * @description tests the constructor is setting the program and all related records
    */
    @isTest private static void testConstructorGetsProgram() {
    	Program_MVN__c program = [SELECT Id FROM Program_MVN__c LIMIT 1];
    	ApexPages.StandardController stdController = new ApexPages.StandardController(program);

        Test.startTest();
            ProgramDownloadExtensionMVN actualProgramDownloadExtension = new ProgramDownloadExtensionMVN(stdController);
        Test.stopTest();

        System.assertNotEquals(null, actualProgramDownloadExtension);
        System.assertNotEquals(null, actualProgramDownloadExtension.programToDownload.program);
        System.assertEquals(ProgramDeployWrapperTestMVN.EXTERNALID, actualProgramDownloadExtension.programToDownload.program.Program_Id_MVN__c);
        System.assertEquals(ProgramDeployWrapperTestMVN.EXTERNALID, actualProgramDownloadExtension.programExternalId);
    }

    /**
    * @description tests the json is constructed correctly
    */
    @isTest private static void testGetJson() {
    	Program_MVN__c program = [SELECT Id FROM Program_MVN__c LIMIT 1];
    	ApexPages.StandardController stdController = new ApexPages.StandardController(program);
    	ProgramDownloadExtensionMVN actualProgramDownloadExtension = new ProgramDownloadExtensionMVN(stdController);

        Test.startTest();
            String actualJson = actualProgramDownloadExtension.programJson;
        Test.stopTest();

        System.assertNotEquals(null, actualJson);
        System.assert(actualJson.contains(ProgramDeployWrapperTestMVN.EXTERNALID));
    }
}