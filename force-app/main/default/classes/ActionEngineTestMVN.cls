/**
 * @author      Mavens
 * @date        Sept 2018
 * @description tests for ActionEngineMVN
 * @group       ActionEngine
 */
@isTest 
private class ActionEngineTestMVN {
    static User caseManager;
    static Program_MVN__c program;
    static Program_Member_MVN__c programMember;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        TestDataFactoryMVN.createPSSettings();
        program = TestDataFactoryMVN.createProgram();
        createStages();
        createProgramMember();
        createApplication();
    }

    static void createProgramMember() {
        Account member = TestDataFactoryMVN.createMember();
        programMember = TestDataFactoryMVN.createProgramMember(program, member);
        programMember.Program_Id_MVN__c = 'Test_Program_MVN';
        update programMember;
    }

    static void createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        programMember.Active_Application_MVN__c = testApplication.Id;
    }

    static void createStages() {
        // Single default stage generated when a Program is first created
        Program_Stage_MVN__c programInitiation = [select Id from Program_Stage_MVN__c];
        programInitiation.Name = 'Program Initiation';
        update programInitiation;
        Program_Stage_MVN__c welcomeCall = TestDataFactoryMVN.createChildProgramStage(program, programInitiation, 'Welcome Call');

        Program_Stage_MVN__c benefitsInvestigation = TestDataFactoryMVN.createParentProgramStage(program, 'Benefits Investigation');
        Program_Stage_MVN__c priorAuthorization = TestDataFactoryMVN.createChildProgramStage(program, benefitsInvestigation, 'Prior Authorization');
        benefitsInvestigation.Stage_Sequence_Number_MVN__c = 2;
        update benefitsInvestigation;

        Program_Stage_MVN__c education = TestDataFactoryMVN.createParentProgramStage(program, 'Education');
        Program_Stage_MVN__c nurseVisit = TestDataFactoryMVN.createChildProgramStage(program, education, 'Nurse Visit');
        education.Stage_Sequence_Number_MVN__c = 3;
        update education;

        Program_Stage_Dependency_MVN__c dependency1 = TestDataFactoryMVN.createStageDependency(programInitiation, welcomeCall);
        Program_Stage_Dependency_MVN__c dependency2 = TestDataFactoryMVN.createStageDependency(benefitsInvestigation, priorAuthorization);
        Program_Stage_Dependency_MVN__c dependency3 = TestDataFactoryMVN.createStageDependency(education, nurseVisit);
    }

    static Program_Member_Stage_MVN__c getProgramMemberStageByName(String stageName) {
        return [
            SELECT 
                Status_MVN__c,
                IsClosed_MVN__c,
                Activity_ID_MVN__c
            FROM 
                Program_Member_Stage_MVN__c 
            WHERE 
                Program_Member_MVN__c = :programMember.Id
            AND 
                Program_Stage_Name_MVN__c = :stageName
        ];
    }

    @isTest
    static void testCloseAndOpenNextOne() {
        TestFactoryActionEngineSettingsMVN.setMocks();

        Program_Member_Stage_MVN__c programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);

        // Prepare the Case to be closed
        Case caseToClose = new Case (
            Id = programMemberTest.Activity_ID_MVN__c,
            Exclude_from_Billing_MVN__c = true
        );
        update caseToClose;

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals('Not Started', programMemberTest.Status_MVN__c);


        Test.startTest();
        ActionEngineMVN actionEngine = new ActionEngineMVN(programMember.Id);
        actionEngine.run('Close_And_Open_Next_Existing_One_MVN');
        Test.stopTest();

        programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(true, programMemberTest.IsClosed_MVN__c);

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);
    }

    @isTest
    static void testCloseAndOpenNoNextOne() {
        TestFactoryActionEngineSettingsMVN.setMocks();

        Program_Member_Stage_MVN__c programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);

        // Prepare the Case to be closed
        Case caseToClose = new Case (
            Id = programMemberTest.Activity_ID_MVN__c,
            Exclude_from_Billing_MVN__c = true
        );
        update caseToClose;

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals('Not Started', programMemberTest.Status_MVN__c);

        programMemberTest = getProgramMemberStageByName('Education');
        System.assertEquals('Not Started', programMemberTest.Status_MVN__c);

        Test.startTest();
        ActionEngineMVN actionEngine = new ActionEngineMVN(programMember.Id);
        actionEngine.run('Close_And_Open_No_Next_Existing_One_MVN');
        Test.stopTest();

        programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(true, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Completed', programMemberTest.Status_MVN__c);

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals('Not Started', programMemberTest.Status_MVN__c);

        programMemberTest = getProgramMemberStageByName('Education');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);
    }

    @isTest
    static void testCloseAndOpenNoExistingOne() {
        TestFactoryActionEngineSettingsMVN.setMocks();

        Program_Stage_MVN__c newStage = TestDataFactoryMVN.createParentProgramStage(program, 'New Stage');
        Program_Stage_MVN__c newChildStage = TestDataFactoryMVN.createChildProgramStage(program, newStage, 'New Child Stage');

        Program_Member_Stage_MVN__c programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);

        // Prepare the Case to be closed
        Case caseToClose = new Case (
            Id = programMemberTest.Activity_ID_MVN__c,
            Exclude_from_Billing_MVN__c = true
        );
        update caseToClose;

        System.assertEquals(true, [SELECT Id FROM Program_Member_Stage_MVN__c 
                                    WHERE Program_Member_MVN__c = :programMember.Id
                                    AND Program_Stage_Name_MVN__c = 'New Stage'].isEmpty());

        Test.startTest();
        ActionEngineMVN actionEngine = new ActionEngineMVN(programMember.Id);
        actionEngine.run('Close_And_Open_No_Existing_One_MVN');
        Test.stopTest();

        programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(true, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Completed', programMemberTest.Status_MVN__c);

        programMemberTest = getProgramMemberStageByName('New Stage');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);
    }

    @isTest
    static void testCloseAndOpenNextOnePassingProgram() {
        TestFactoryActionEngineSettingsMVN.setMocks();

        Program_Member_Stage_MVN__c programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);

        // Prepare the Case to be closed
        Case caseToClose = new Case (
            Id = programMemberTest.Activity_ID_MVN__c,
            Exclude_from_Billing_MVN__c = true
        );
        update caseToClose;

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals('Not Started', programMemberTest.Status_MVN__c);


        Program_Member_MVN__c programMember = [
            SELECT
                Id,
                Active_Application_MVN__c,
                Program_Id_MVN__c,
                Program_MVN__c
            FROM
                Program_Member_MVN__c
            WHERE
                Id = :programMember.Id
        ];
        Test.startTest();
        ActionEngineMVN actionEngine = new ActionEngineMVN(programMember);
        actionEngine.run('Close_And_Open_Next_Existing_One_MVN');
        Test.stopTest();

        programMemberTest = getProgramMemberStageByName('Welcome Call');
        System.assertEquals(true, programMemberTest.IsClosed_MVN__c);

        programMemberTest = getProgramMemberStageByName('Benefits Investigation');
        System.assertEquals(false, programMemberTest.IsClosed_MVN__c);
        System.assertEquals('Started', programMemberTest.Status_MVN__c);
    }
}