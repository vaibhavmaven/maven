public class PAP_Get_MultiFaxStatusResponse {
    
    public String Status {get;set;} 
    public List<Result> Result {get;set;} 

    public PAP_Get_MultiFaxStatusResponse(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'Status') {
                        Status = parser.getText();
                    } else if (text == 'Result') {
                        Result = arrayOfResult(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'PAP_Get_MultiFaxStatusResponse consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class Result {
        public String FileName {get;set;} 
        public String FaxDetailsID {get;set;} 
        public String SentStatus {get;set;} 
        public String DateQueued {get;set;} 
        public String DateSent {get;set;} 
        public Integer EpochTime {get;set;} 
        public String ToFaxNumber {get;set;} 
        public String Pages {get;set;} 
        public String Duration {get;set;} 
        public String RemoteID {get;set;} 
        public String ErrorCode {get;set;} 
        public String Size {get;set;} 
        public String AccountCode {get;set;} 

        public Result(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'FileName') {
                            FileName = parser.getText();
                        } else if (text == 'FaxDetailsID') {
                            FaxDetailsID = parser.getText();
                        } else if (text == 'SentStatus') {
                            SentStatus = parser.getText();
                        } else if (text == 'DateQueued') {
                            DateQueued = '';  // parser.getText();
                        } else if (text == 'DateSent') {
                            DateSent = '' ;   // parser.getText();
                        } else if (text == 'EpochTime') {
                            EpochTime = parser.getIntegerValue();
                        } else if (text == 'ToFaxNumber') {
                            ToFaxNumber = parser.getText();
                        } else if (text == 'Pages') {
                            Pages = parser.getText();
                        } else if (text == 'Duration') {
                            Duration = parser.getText();
                        } else if (text == 'RemoteID') {
                            RemoteID = parser.getText();
                        } else if (text == 'ErrorCode') {
                            ErrorCode = parser.getText();
                        } else if (text == 'Size') {
                            Size = parser.getText();
                        } else if (text == 'AccountCode') {
                            AccountCode = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Result consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static PAP_Get_MultiFaxStatusResponse parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new PAP_Get_MultiFaxStatusResponse(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    
    private static List<Result> arrayOfResult(System.JSONParser p) {
        List<Result> res = new List<Result>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Result(p));
        }
        return res;
    }
}