/*
 * AccountSearchIntfMVN
 * Created by: Jennifer Wyher
 * Created Date: May 29, 2014
 * Description: This is the generic interface to support Account search from various sources
 *              (eg: Veeva Account and Addresses vs a Customer Master web service)
 */

public interface AccountSearchIntfMVN {

	// Search the utility for Accounts and Addresses for the identified recordType
	List<AccountSearchRsltMVN> search(AccountSearchRqstMVN searchRequest);

	// Insert the identified result record into Salesforce
	AccountSearchRsltMVN insertAccountDetails(AccountSearchRsltMVN asr);

    List<AccountSearchRsltMVN> getAccountsWithDetails(AccountSearchRqstMVN searchRequest);
}