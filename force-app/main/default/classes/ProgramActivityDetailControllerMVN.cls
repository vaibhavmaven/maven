/**
 *  ProgramActivityDetailControllerMVN
 *  Created By: Rick Locke
 *  Created On: August 2017
 *  Description: Controller for the Program Activity Detail component.
 */

public with sharing class ProgramActivityDetailControllerMVN extends ProgramActivityComponentControllerMVN {

    public SObject record {get;set;}

    public Boolean viewMode {
        get {
            return (componentRecord.Mode_MVN__c == 'View');
        }
        set;
    }

    public Boolean showSave {
        get {
            if (componentRecord.Mode_MVN__c == 'Create') {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Constructor
     */
    public ProgramActivityDetailControllerMVN() {}

    /**
     * Generates and sets the record, checking first for the record in the componentObjectMap in the Master Controller 
     */
    public override void setObjectRecords() {
        if(masterController.componentObjectMap.containsKey(componentRecord.Id)) {
            record = masterController.componentObjectMap.get(componentRecord.Id);
        } else {
            generateObjectRecord();
        }
        masterController.componentObjectMap.put(componentRecord.Id, record);
    }

    /**
     * Generates a new SObject and sets the record -or- retrieves the record via query if in View/Edit mode.
     * This is separated from the setObjectRecords because this can be called if the save button is used to reinitialize the object. 
     */
    public void generateObjectRecord() {
        if (componentRecord.Mode_MVN__c == 'Create') {
            Schema.SobjectType objectType = globalDescribe.get(componentRecord.SObject_MVN__c);
            if(hasRecordType) {
                record = objectType.newSObject(RecordTypeId, true);
            } else {
                record = objectType.newSObject(null, false);    
            }
            record.put(componentRecord.Program_Member_Id_Field_MVN__c, masterController.programMember.Id);
        } else {
            record = Database.query(getFieldSetSOQL())[0];
        }
    }

    /**
     * This is called when the "Save" button is clicked in the component.
     */
    public void saveRecord() {
        if(masterController.saveComponentRecord(componentRecord.Id)) {
            generateObjectRecord();
            masterController.componentObjectMap.put(componentRecord.Id, record);
        }
    }

    /**
     * Calls master controller returnKeyPress.
     */
    public void returnKeyPress() {
        masterController.returnKeyPress();
    }
}