/**
* @author Mavens
* @group Network API
* @description Wrapper class to manage retrieving the Veeva Network Status Settings custom metadata type
*/
public with sharing class VeevaNetworkStatusSettingsMVN {

    /**
    * @description Custom metadata wrapper for VeevaNetworkStatusSettings custom metadata
    */
    public List<Veeva_Network_Status_Settings_MVN__mdt> settings { get; private set; }

    /**
    * @description List of test mock Custom metadata wrappers for VeevaNetworkStatusSettings custom metadata
    */
    private static List<Veeva_Network_Status_Settings_MVN__mdt> testMocks;

    /**
    * @description Map of Veeva Network Statuses applied by Veeva Network Status name
    */
    public Map<String, Boolean> networkDCRStatusApplied;

    /**
    * @description Map of Salesforce Status names by Veeva Network Status name
    */
    public Map<String, String> networkToSalesforceStatus;

    /**
    * @description Map of Veeva Network Field Statuses applied by Veeva Network Field Status name
    */
    public Map<String, Boolean> networkFieldStatusApplied;

    /**
    * @description Constructor which gets VeevaNetworkStatusSettings custom metadata according Name passed.
    * @example
    * * VeevaNetworkStatusSettingsMVN setting = new VeevaNetworkStatusSettingsMVN();
    */
    public VeevaNetworkStatusSettingsMVN() {
        settings = new List<Veeva_Network_Status_Settings_MVN__mdt>();

        if(testMocks != null) {
            settings = testMocks;
        }

        initalizeProperties();
    }

    /**
    * @description Constructor which gets VeevaNetworkStatusSettings custom metadata according Name passed.
    * @param    statusSettings  list of custom metadata wrappers for VeevaNetworkStatusSettings as Object
    * @example
    * * VeevaNetworkStatusSettingsMVN setting = new VeevaNetworkStatusSettingsMVN(statusSettings);
    */
    public VeevaNetworkStatusSettingsMVN(List<Veeva_Network_Status_Settings_MVN__mdt> statusSettings) {
        settings = statusSettings;

        if(testMocks != null) {
            settings = testMocks;
        }

        initalizeProperties();
    }

    /**
    * @description Sets list of test mock custom metadata wrappers for VeevaNetworkStatusSettings
    * @param    mocks   List of test custom metadata wrapper for VeevaNetworkStatusSettings
    * @example
    * * setMocks(mocks);
    */
    @TestVisible
    private static void setMocks(List<Veeva_Network_Status_Settings_MVN__mdt> mocks){
        testMocks = mocks;
    }

    /**
    * @description Initializes all properties in this class
    * @example
    * * initalizeProperties();
    */
    private void initalizeProperties() {
        networkDCRStatusApplied = new Map<String, Boolean>();
        networkToSalesforceStatus = new Map<String, String>();
        networkFieldStatusApplied = new Map<String, Boolean>();

        for (Veeva_Network_Status_Settings_MVN__mdt setting : settings) {
            String networkStatusName = setting.Veeva_Network_Status_MVN__c;
            String type = setting.Type_MVN__c;
            String salesforceStatusName = setting.Salesforce_Status_MVN__c;
            Boolean apply = Boolean.valueOf(setting.Apply_MVN__c);

            if(type == 'DCR' && String.IsNotBlank(networkStatusName)) {
                networkDCRStatusApplied.put(networkStatusName, apply);

                if(String.IsNotBlank(salesforceStatusName)) {
                    networkToSalesforceStatus.put(networkStatusName, salesforceStatusName);
                }
            } else if(type == 'Field' && String.IsNotBlank(networkStatusName)) {
                networkFieldStatusApplied.put(networkStatusName, apply);
            }
        }
    }
}