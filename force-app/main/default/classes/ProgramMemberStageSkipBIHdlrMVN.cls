/**
 * @author      Mavens
 * @group       ProgramMemberStageSkipBIHdlrMVN
 * @description Trigger handler to skip BI when program is configured and BI records configured correctly
 */
public with sharing class ProgramMemberStageSkipBIHdlrMVN implements TriggerHandlerMVN.HandlerInterface {

    public void handle() {
        Set<Id> programMembersIdsToProcess = getProgramMemberIds();
        if (programMembersIdsToProcess.isEmpty()) {
            return;
        }

        Set<Id> applicationIdsToProcess = new Set<Id>();
        for (Program_Member_MVN__c pm : getProgramMembers(programMembersIdsToProcess)) {
            applicationIdsToProcess.add(pm.Active_Application_MVN__c);
        }

        List<Benefits_Coverage_MVN__c> bcRecordsToSetEligible = getBCRecords(applicationIdsToProcess);

        Map<ID, Benefits_Coverage_MVN__c> bcMap = new Map<ID, Benefits_Coverage_MVN__c>(bcRecordsToSetEligible);

        if (System.isBatch() || System.isFuture()) {
            updateBCRecords(bcMap.keySet(), new Set<Id>());
        } else {
            List<Program_Member_MVN__c> programMembersUnderEval = new List<Program_Member_MVN__c>();
            for (Id progrmprogramMembersIdToProcess : programMembersIdsToProcess) {
                programMembersUnderEval.add(
                    new Program_Member_MVN__c (
                        Id = progrmprogramMembersIdToProcess,
                        BI_Evaluation_In_Process_MVN__c = true
                    )
                );
            }
            update programMembersUnderEval;
            updateBCRecordsFuture(bcMap.keySet(), programMembersIdsToProcess);
        }
    }

    /**
     * Run update in the future
     * @params Set<Id> benefits and coverage ids
     */
    @future
    public static void updateBCRecordsFuture (Set<Id> bcIds, Set<Id> programMembersIdsToProcess) {
        updateBCRecords(bcIds, programMembersIdsToProcess);
    }

    /**
     * Update benefits and coverage records and set them to eligible
     * @params Set<Id> benefits and coverage ids
     */
    public static void updateBCRecords (Set<Id> bcIds, Set<Id> programMembersIdsToProcess) {
        List<Benefits_Coverage_MVN__c> bcsToUpdate = new List<Benefits_Coverage_MVN__c>();

        for (Id bcId : bcIds) {
            bcsToUpdate.add(
                new Benefits_Coverage_MVN__c (
                    Id = bcId,
                    Benefits_Investigation_Status_MVN__c = 'Eligible'
                )
            );
        }
        update bcsToUpdate;

        List<Program_Member_MVN__c> programMembersUnderEval = new List<Program_Member_MVN__c>();
        for (Id progrmprogramMembersIdToProcess : programMembersIdsToProcess) {
            programMembersUnderEval.add(
                new Program_Member_MVN__c (
                    Id = progrmprogramMembersIdToProcess,
                    BI_Evaluation_In_Process_MVN__c = false
                )
            );
        }
        update programMembersUnderEval;
    }

    /*
    * Return all program member ids related to program member stages in the current trigger
    * that just change status to Started and are configured to skip BI. This is a formula field
    * that reads the configuration from the Parent Program
    */
    private Set<Id> getProgramMemberIds() {
        Set<Id> pmIds = new Set<Id>();
        for (Program_Member_Stage_MVN__c pmStage : (List<Program_Member_Stage_MVN__c>) Trigger.new) {
            if (statusChangedToStarted(pmStage) && pmStage.BI_Skippable_Stage_MVN__c) {
                pmIds.add(pmStage.Program_Member_MVN__c);
            }
        }
        return pmIds;
    }

    private Boolean statusChangedToStarted(Program_Member_Stage_MVN__c pmStage) {
        Program_Member_Stage_MVN__c oldStage = (Program_Member_Stage_MVN__c) Trigger.oldMap.get(pmStage.Id);
        return pmStage.Status_MVN__c == 'Started' && oldStage.Status_MVN__c != 'Started';
    }

    /*
    * Return all programs for the ids passed in. Retrieve the active application id
    */
    private List<Program_Member_MVN__c> getProgramMembers(Set<Id> programMemberIds) {
        return [
            SELECT
                Active_Application_MVN__c
            FROM
                Program_Member_MVN__c
            WHERE
                Id IN :programMemberIds
        ];
    }

    /*
    * Return all of the B&C records related to the application ids where Health_Insurance_MVN__c is
    * indicated as No, retrieving the Benefits_Investigation_Status_MVN__c field
    */
    private List<Benefits_Coverage_MVN__c> getBCRecords(Set<Id> applicationIds) {
        return [
            SELECT
                Id,
                Benefits_Investigation_Status_MVN__c
            FROM
                Benefits_Coverage_MVN__c
            WHERE
                Application_MVN__c IN :applicationIds
            AND
                Health_Insurance_MVN__c = 'No'
        ];
    }
}