/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description tests EligibilityEngineNumberCompHdlrMVN
 */
@isTest private class EligibilityEngineNumberCompHdlrTestMVN {
    private static User caseManager;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        TestDataFactoryMVN.createPSSettings();

        System.runAs(caseManager) {
            program = TestDataFactoryMVN.createProgram();
            Account member = TestDataFactoryMVN.createMember();
            member.NumberOfEmployees = 10;
            update member;
            programMember = TestDataFactoryMVN.createProgramMember(program, member);
            application = createApplication();
            programMember.Active_Application_MVN__c = application.Id;
            update programMember;
        }
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        return testApplication;
    }

    @isTest private static void runNumberComparisonUnsuccessfullyNoComparison() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Wrong Test Rule One',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Value_MVN__c = '9',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void runNumberComparisonUnsuccessfullyNoValue() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Wrong Test Rule One',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Comparison_MVN__c = 'Less_Than_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Value_MVN__c = null,
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void fireLessThanRule() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Max Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Less_Than_MVN',
            Value_MVN__c = '11',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void fireGreaterThanRule() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Min Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Greater_Than_MVN',
            Value_MVN__c = '9',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void fireLessThanOrEqualToRule1() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Max Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Less_Than_Or_Equal_To_MVN',
            Value_MVN__c = '11',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void fireLessThanOrEqualToRule2() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Max Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Less_Than_Or_Equal_To_MVN',
            Value_MVN__c = '10',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void fireGreaterThanOrEqualToRule1() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Max Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Greater_Than_Or_Equal_To_MVN',
            Value_MVN__c = '9',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void fireGreaterThanOrEqualToRule2() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Max Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Greater_Than_Or_Equal_To_MVN',
            Value_MVN__c = '10',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void fireEqualToRule() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Max Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Equal_To_MVN',
            Value_MVN__c = '10',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void fireNotEqualToRule() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Max Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Not_Equal_To_MVN',
            Value_MVN__c = '11',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }
}