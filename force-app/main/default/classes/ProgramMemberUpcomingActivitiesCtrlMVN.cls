/*
 *	ProgramMemberUpcomingActivitiesCtrlMVN
 *	Created By:     Jen Wyher
 *	Created Date:   Dec 2015
 *	Description:    Controller for the ProgramMemberUpcomingActivities page. Display the upcoming activities for a specific Program Member.
 *
 *	Modified:		February 10, 2016 by Vincent Reeder
 *	Modified:		March 22, 2016 - Thomas Hajcak (thomas@mavensconsulting.com)
 *					Refactored queries.
 */

global with sharing class ProgramMemberUpcomingActivitiesCtrlMVN {
    public final String programMemberId {get; private set;}
    public String selectedFilterOptions {get;set;}
    private final Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();


    /**
     * Program Member Upcoming Activities - Controller Constructor
     * @param	ApexPages.StandardController	Standard Program Member Controller
     * @access	public
     */
    public ProgramMemberUpcomingActivitiesCtrlMVN(ApexPages.StandardController stdController) {
        this.programMemberId = stdController.getId();
        this.selectedFilterOptions = '[]';
    }

    /**
     * Getting Program Member Stages with Cases
     * @return	List<ProgramMemberStageWrapperMVN>
     * @access	public
     */
    public List<ProgramMemberStageWrapperMVN> getStages() {
        Map<Id, Program_Member_Stage_MVN__c> stages = new Map<Id, Program_Member_Stage_MVN__c>(getProgramMemberStages());

        Map<Id, ProgramMemberStageWrapperMVN> parentStagesByIDs = new Map<Id, ProgramMemberStageWrapperMVN>();

        for(Program_Member_Stage_MVN__c stage : stages.values()) {
            if(stage.Parent_Program_Member_Stage_MVN__c == null) {
                parentStagesByIDs.put(stage.Id, new ProgramMemberStageWrapperMVN(stage));
            } else if(parentStagesByIDs.get(getParentStage(stages, stage.Id)) != null
                   && (!stage.Cases__r.isEmpty()
                   || !stage.Program_Member_Stage_Dependencies__r.isEmpty()
                   || stage.Program_Stage_Name_MVN__c != null)) {
                parentStagesByIDs.get(getParentStage(stages, stage.Id)).addChild(new ProgramMemberStageWrapperMVN(stage));
            }
        }

        // Creating Additional Activities section
        parentStagesByIDs.put(null, new ProgramMemberStageWrapperMVN());

        for(Case caseRecord : getAdditionalActivities()) {
            parentStagesByIDs.get(null).addChild(new ProgramMemberStageWrapperMVN(caseRecord));
        }

        List<ProgramMemberStageWrapperMVN> stageList = parentStagesByIDs.values();
        stageList.sort();
        return stageList;
    }

    /**
     * Getting Parent Stage recursively
     * @param	Map<Id, Program_Member_Stage_MVN__c>	Map of Program Member Stages
     * @param	Id										Record Id
     * @return	Id
     * @access	private
     */
    private Id getParentStage(Map<Id, Program_Member_Stage_MVN__c> stages, Id recordId) {
        if(recordId != null && stages.get(recordId)	!= null) {
            if(stages.get(recordId).Parent_Program_Member_Stage_MVN__c == null) {
                return recordId;
            } else {
                return getParentStage(stages, stages.get(recordId).Parent_Program_Member_Stage_MVN__c);
            }
        }

        return recordId;
    }

    /**
     * Getting Upcoming Activity Fields
     * @return	String		Field names
     * @access	private
     */
    private String getCaseFields() {
        List<Schema.FieldSetMember> fields = SObjectType.Case.FieldSets.Upcoming_Activity_Fields_MVN.getFields();

        List<String> fieldNames = new List<String>();

        for (Schema.FieldSetMember f : fields) {
            fieldNames.add(f.getFieldPath());
        }

        return String.join(fieldNames, ', ');
    }

    /**
     * Getting related Program Member Stages
     * @return	List<Program_Member_Stage_MVN__c>	List of Program Member Stages
     * @access	private
     */
    private List<Program_Member_Stage_MVN__c> getProgramMemberStages() {
        String query = 'SELECT Application_MVN__r.Name, Program_Stage_Name_MVN__c, Status_MVN__c, IsClosed_MVN__c, ' +
            'Stage_Sequence_Number_MVN__c, Parent_Program_Member_Stage_MVN__c, ' +
            '(SELECT ' + String.escapeSingleQuotes(getCaseFields()) + ' FROM Cases__r LIMIT 1), ' +
            '(SELECT Program_Member_Stage_Dependency_MVN__c, Program_Member_Stage_MVN__c, ' +
            'Program_Member_Stage_Dependency_MVN__r.Program_Stage_Name_MVN__c, ' +
            'Program_Member_Stage_Dependency_MVN__r.Status_MVN__c ' +
            'FROM Program_Member_Stage_Dependencies__r) ' +
            'FROM Program_Member_Stage_MVN__c ' +
            'WHERE Program_Member_MVN__c = :programMemberId ' +
            'AND Is_Hidden_MVN__c = false ' +
            'ORDER BY Parent_Program_Member_Stage_MVN__c ASC NULLS FIRST, Application_MVN__r.Name DESC';

        return Database.query(query);
    }

    /**
     * Getting related Additional Activities
     * @return	List<Case>	List of Cases
     * @access	private
     */
    private List<Case> getAdditionalActivities() {
        String query = 'SELECT IsClosed, {0} ' +
            'FROM Case ' +
            'WHERE Program_Member_MVN__c = :programMemberId ' +
            'AND Program_Member_Stage_MVN__c = null';

        query = String.format(query, new List<String>{ getCaseFields() });

        return Database.query(query);
    }

    @RemoteAction
    global static Id createActivity(Id parentStageId, Id programMemberId){
        Program_Member_Stage_MVN__c stage = new Program_Member_Stage_MVN__c();
        stage.Parent_Program_Member_Stage_MVN__c = parentStageId;
        stage.Program_Member_MVN__c = programMemberId;
        stage.Status_MVN__c = 'Started';

        insert stage;

        Id activityRecordTypeId = [select Id from RecordType where SObjectType = 'Case' and DeveloperName = 'Activity_PS_MVN' limit 1].Id;
        Case newCase = new Case();
        newCase.RecordTypeId = activityRecordTypeId;
        newCase.Program_Member_Stage_MVN__c = stage.Id;
        newCase.Program_Member_MVN__c = programMemberId;

        if(!String.isBlank(programMemberId)) {
            newCase.AccountId = [select Member_MVN__c from Program_Member_MVN__c where Id = :programMemberId].Member_MVN__c;
        }

        insert newCase;

        stage.Activity_ID_MVN__c = newCase.Id;

        update stage;

        return newCase.Id;
    }

    @RemoteAction
    global static Id createAdditionalActivity(Id programMemberId){
        Id activityRecordTypeId = [select Id from RecordType where SObjectType = 'Case' and DeveloperName = 'Activity_PS_MVN' limit 1].Id;
        Case newCase = new Case();
        newCase.RecordTypeId = activityRecordTypeId;
        newCase.Program_Member_MVN__c = programMemberId;

        if(!String.isBlank(programMemberId)) {
            newCase.AccountId = [select Member_MVN__c from Program_Member_MVN__c where Id = :programMemberId].Member_MVN__c;
        }

        insert newCase;

        return newCase.Id;
    }

    public List<String> getCaseNoteFilters(){
        Schema.DescribeSObjectResult caseObject = Case.getSObjectType().getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> fieldsMap = caseObject.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> picklistValues = fieldsMap.get(settings.Case_Filter_Field_MVN__c).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject

        List<String> options = new List<String>();

        for(Schema.PicklistEntry value : picklistValues){
            options.add(value.getLabel());
        }

        return options;
    }

    public String getCaseNoteFilterFieldLabel(){
        Schema.DescribeSObjectResult caseObject = Case.getSObjectType().getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> fieldsMap = caseObject.fields.getMap(); //get a map of fields for the passed sobject
        return fieldsMap.get(settings.Case_Filter_Field_MVN__c).getDescribe().getLabel();
    }

    public List<CaseNotes> caseNotes {
        get {
            caseNotes = new List<CaseNotes>();
            String queryString = 'select Id, Subject, CaseNumber, Application_MVN__r.Name,  (select Id, Notes_MVN__c, Locked_MVN__c, CreatedById, CreatedBy.Name, LastModifiedDate from Case_Notes__r where Locked_MVN__c = true order by LastModifiedDate DESC) from Case where Program_Member_MVN__c = :programMemberId {0} order by LastModifiedDate DESC';

            List<String> filter = new List<String>();
            System.debug(selectedFilterOptions);
            filter = (List<String>) JSON.deserialize(selectedFilterOptions, List<String>.class);

            if(!filter.isEmpty()) {
                queryString = String.format(queryString, new List<String>{' AND ' + settings.Case_Filter_Field_MVN__c + ' in :filter'});
            } else {
                queryString = String.format(queryString, new List<String>{''});
            }

            for(Case cs : (List<Case>) Database.query(queryString)) {
                if(cs.Case_Notes__r != null && !cs.Case_Notes__r.isEmpty()) {
                    caseNotes.add(new CaseNotes(cs));
                }
            }
            caseNotes.sort();
            return caseNotes;
        } set;
    }

    public class CaseNotes implements Comparable{
        public List<Case_Note_MVN__c> notes {get;set;}
        public Case parentCase {get;set;}

        public CaseNotes (Case parentCase){
            this.parentCase = parentCase;
            this.notes = parentCase.Case_Notes__r;
        }

        public Integer compareTo(Object obj) {
            CaseNotes compareToNote = (CaseNotes)(obj);

            if (this.notes[0].LastModifiedDate < compareToNote.notes[0].LastModifiedDate) {
                return 1;
            }

            if (this.notes[0].LastModifiedDate == compareToNote.notes[0].LastModifiedDate) {
                return 0;
            }

            return -1;
        }
    }

}