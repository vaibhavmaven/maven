/**
 * @author Mavens
 * @description Test InitialStageForProgramTriggerMVN
 * @group Program
 */

@isTest
private class InitialStageForProgramTriggerTestMVN {

    @isTest
    static void default() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        Program_MVN__c program = programFactory.create();
        System.assertEquals(1, [select Id from Program_MVN__c].size());
        System.assertEquals(1, [select Id from Program_Stage_MVN__c].size());
    }
}