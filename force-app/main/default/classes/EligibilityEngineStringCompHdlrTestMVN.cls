/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description tests EligibilityEngineStringCompHdlrMVN
 */
@isTest private class EligibilityEngineStringCompHdlrTestMVN {
    private static User caseManager;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static Account member;
    private static Application_MVN__c application;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        TestDataFactoryMVN.createPSSettings();


        System.runAs(caseManager) {
            program = TestDataFactoryMVN.createProgram();
            member = TestDataFactoryMVN.createMember();
            programMember = TestDataFactoryMVN.createProgramMember(program, member);
            application = createApplication();
            programMember.Active_Application_MVN__c = application.Id;
            update programMember;
        }
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        return testApplication;
    }

    @isTest private static void runStringCompUnsuccessfully() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Wrong Test Rule One',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Id',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void runStringCompEqualToSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Program_MVN__c',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Not_Equal_To_MVN',
            Value_MVN__c = program.Id,
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void runStringCompNotEqualToSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Program_MVN__c',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Equal_To_MVN',
            Value_MVN__c = null,
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void runStringCompContainsSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Program_MVN__c',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Contains_MVN',
            Value_MVN__c = 'test',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void runStringCompDoesNotContainNoSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Program_MVN__c',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Does_Not_Contain_MVN',
            Value_MVN__c = 'test',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void runStringCompStartsWithSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Program_MVN__c',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Starts_With_MVN',
            Value_MVN__c = 'test',
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void runStringCompWithRelationshipSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.Id',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Not_Equal_To_MVN',
            Value_MVN__c = member.Id,
            Active_MVN__c = true
        );
        insert expectedRule;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }
}