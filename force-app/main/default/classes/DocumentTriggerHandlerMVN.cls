/**
 *  AttachmentInsertUpdateTriggerMVN
 *  Created By:     Florian Hoehn
 *  Created On:     June 12th 2017
 *  Description:    Trigger Handler which ensures that documents that are connected to an
 **/
public class DocumentTriggerHandlerMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
    * @description handle
    *              - checks if the document's attachment ParentId is a document
    *              - and if it isn't reparents (clones & deletes) the attachment to the document
    *                that is already linked up and updates that document with the reparented Id
    */
    public void handle() {
        if(Trigger.isInsert && Trigger.IsAfter) {
            Map<Id, Id> documentIdsByAttachment = new Map<Id, Id>();
            for(Document_MVN__c thisDocument : (List<Document_MVN__c>)Trigger.new) {
                documentIdsByAttachment.put(thisDocument.Attachment_ID_MVN__c, thisDocument.Id);
            }

            List<Attachment> attachmentsToDelete = new List<Attachment>();
            List<Attachment> attachmentsToReParent = new List<Attachment>();
            for(Attachment thisAttachment : [SELECT Id, ParentId, Name, Body
                                               FROM Attachment
                                              WHERE Id IN: documentIdsByAttachment.keySet()]) {
                if(thisAttachment.ParentId.getSObjectType() != (new Document_MVN__c()).getSObjectType()) {
                    attachmentsToReParent.add(
                        new Attachment(
                            Name = thisAttachment.Name,
                            Body = thisAttachment.Body,
                            ParentId = documentIdsByAttachment.get(thisAttachment.Id),
                            Description = thisAttachment.Id
                        )
                    );
                    attachmentsToDelete.add(thisAttachment);
                }
            }
            insert attachmentsToReParent;
            delete attachmentsToDelete;

            List<Document_MVN__c> documentsToUpdate = new List<Document_MVN__c>();
            for(Attachment thisAttachment : attachmentsToReParent) {
                documentsToUpdate.add(
                    new Document_MVN__c(
                        Id = documentIdsByAttachment.get((Id)thisAttachment.Description),
                        Attachment_Id_MVN__c = thisAttachment.Id
                    )
                );
            }
            update documentsToUpdate;
        }
    }
}