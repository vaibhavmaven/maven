/**
 *  @group AttachmentInsertTriggerMVN
 *  @author Mavens
 *  @description    This is a trigger class that creates a document record for each attachment
 *                  (usually) uploaded against an sObject which has a record inside the document setting
 **/
public class AttachmentInsertTriggerMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
    * @description ordered documentSettings in a map to be able to retrieve the setting via sObject API name
    */
    private Map<String, Document_Setting_MVN__mdt> documentSettings {
        get {
            Map<String, Document_Setting_MVN__mdt> documentSettings = new Map<String, Document_Setting_MVN__mdt>();
                for(Document_Setting_MVN__mdt setting : [SELECT SObject_API_Name_MVN__c,
                                                                Lookup_Field_API_Name_MVN__c,
                                                                Program_Member_Lookup_Field_API_Name_MVN__c
                                                           FROM Document_Setting_MVN__mdt]) {
                    documentSettings.put(setting.SObject_API_Name_MVN__c, setting);
            }
            return documentSettings;
        }
    }

    /**
    * @description parentIds (attachment) ordered by type (sObject API name)
    */
    private Map<String, Set<Id>> parentIdsByType;

    /**
    * @description attachments ordered by parentId in case several are uploaded against one parent
    */
    private Map<Id, List<Attachment>> attachmentsByParentId;

    /**
    * @description constructor
    *              - initiates parent maps
    */
    public AttachmentInsertTriggerMVN() {
        this.parentIdsByType = new Map<String, Set<Id>>();
        this.attachmentsByParentId = new Map<Id, List<Attachment>>();
    }

    /**
    * @description handle
    *              - creates a document record for each attachment linked to the parent records
    *                and the related Program Member
    */
    public void handle() {
        if(Trigger.isBefore) {
            copyAttachmentsToCase();
        } else if(Trigger.isInsert && Trigger.isAfter) {
            this.goThroughAttachmentsAndSetParentIdAndTypeMaps((List<Attachment>)Trigger.new);
            List<Document_MVN__c> newDocuments = this.createDocumentForEachAttachment();
            upsert newDocuments Attachment_Id_MVN__c;
        }
    }

    /**
    * @description copyAttachmentsToCase, copy attachments from Email to Case
    */
    private void copyAttachmentsToCase() {
        List<Id> emailsIds = new List<Id>();

        // select only attachment attached to an email
        for(Attachment att : (List<Attachment>)trigger.new) {
            if(Id.valueOf(att.ParentId).getSobjectType() == Schema.EmailMessage.SObjectType) {
                emailsIds.add(att.ParentId);
            }
        }

        if(!emailsIds.isEmpty()) {
            Map<Id, Id> emailIdToCaseId = new Map<Id, Id>();
            for(EmailMessage email : [SELECT RelatedToId, ToAddress FROM EmailMessage WHERE Id IN :emailsIds]) {
                if(!email.ToAddress.contains('@srfax.com')) {
                	emailIdToCaseId.put(email.Id, email.RelatedToId);   
                }
            }

            List<Attachment> caseAttachments = new List<Attachment>();

            for(Attachment att : (List<Attachment>)trigger.new) {
                if(emailIdToCaseId.containsKey(att.ParentId) && Id.valueOf(emailIdToCaseId.get(att.ParentId)).getSobjectType() == Schema.Case.SObjectType) {
                    // only if the parent of the email is a case.
                    Attachment caseAttach = att.clone();
                    caseAttach.ParentId = emailIdToCaseId.get(att.ParentId);
                    caseAttachments.add(caseAttach);
                }
            }

            if (!caseAttachments.isEmpty()) {
                insert caseAttachments;
            }
        }
    }

    /**
    * @description goes through the attachments and populates the parent maps for later use
    * @param List<Attachment> attachments
    */
    private void goThroughAttachmentsAndSetParentIdAndTypeMaps(List<Attachment> attachments) {
        for(Attachment thisAttachment : attachments) {
            if(thisAttachment.ParentId.getSObjectType() != (new Document_MVN__c()).getSObjectType()) {
                List<Attachment> attachmentList = new List<Attachment>();
                if(this.attachmentsByParentId.containsKey(thisAttachment.ParentId)) {
                    attachmentList = this.attachmentsByParentId.get(thisAttachment.ParentId);
                }
                attachmentList.add(thisAttachment);
                this.attachmentsByParentId.put(thisAttachment.ParentId, attachmentList);

                String parentIdType = thisAttachment.ParentId.getSObjectType().getDescribe().getName();
                Set<Id> parentIds = new Set<Id>();
                if(this.parentIdsByType.containsKey(parentIdType)) {
                    parentIds = this.parentIdsByType.get(parentIdType);
                }
                if(this.documentSettings.containsKey(parentIdType)) {
                    parentIds.add(thisAttachment.ParentId);
                    this.parentIdsByType.put(parentIdType, parentIds);
                }
            }
        }
    }

    /**
    * @description create documents by pulling the program member lookup fields from the setting
    *              and using a dynamic query to get the content
    * ATTENTION:
    *   SOQL inside a for loop: as long as we do not have too many sObjects inside the Document setting (>100)
    *   this should not represent an issue. We need this to create the dynamic query for each needed parent
    * @param List<Attachment> attachments
    */
    private List<Document_MVN__c> createDocumentForEachAttachment() {
        Map<Id, Document_MVN__c> newDocuments = new Map<Id, Document_MVN__c>();
        for(String parentIdType : this.parentIdsByType.keySet()) {
            Set<Id> parentIdsToSelect = this.parentIdsByType.get(parentIdType);
            Document_Setting_MVN__mdt thisDocumentSetting = this.documentSettings.get(parentIdType);
            if(thisDocumentSetting != null) {
                String query = 'SELECT ' + thisDocumentSetting.Program_Member_Lookup_Field_API_Name_MVN__c +
                                ' FROM ' + parentIdType +
                               ' WHERE Id IN: parentIdsToSelect';
                for(SObject thisObject : Database.query(query)) {
                    Id parentId = (Id)thisObject.get('Id');
                    for(Attachment thisAttachment : this.attachmentsByParentId.get(parentId)) {
                        Document_MVN__c newDocument = new Document_MVN__c(
                            Attachment_Id_MVN__c = thisAttachment.Id,
                            Title_MVN__c = thisAttachment.Name,
                            Program_Member_MVN__c =
                                (Id)thisObject.get(thisDocumentSetting.Program_Member_Lookup_Field_API_Name_MVN__c)
                        );
                        newDocument.put(thisDocumentSetting.Lookup_Field_API_Name_MVN__c, parentId);
                        newDocuments.put(newDocument.Attachment_Id_MVN__c, newDocument);
                    }
                }
            }
        }
        return newDocuments.values();
    }
}