/**
 * @author Mavens
 * @date 09/2018
 * @description Class to manage retrieving the fields from the Action Engine Settings custom metadata type
 */ 
public with sharing class ActionEngineSettingsMVN {

    private static List<Action_Engine_Setting_MVN__mdt> testMock;

    /**
     * Retrieve all custom metadata records for the Action Engine
     */ 
    public static List<Action_Engine_Setting_MVN__mdt> allSettings {
        get {
            if (allSettings == null) {
                Map<String, Schema.SObjectField> fieldMapping = 
                    Action_Engine_Setting_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
                List<String> fields = new List<String>(fieldMapping.keySet());

                String queryString = 'SELECT ' + String.escapeSingleQuotes(String.join(fields, ','))
                                    + ' FROM Action_Engine_Setting_MVN__mdt' 
                                    + ' WHERE Active_MVN__c = true';
                allSettings = Database.query(queryString);
            }
            if(testMock != null){
                allSettings = testMock;
            }
            return allSettings;
        }
        private set;
    }

    /**
     * Get the setting for an specific program id by developer name
     * @param  programId     program id
     * @param  actionDevName action developer name
     * @return custom metadata setting
     */ 
    public static Action_Engine_Setting_MVN__mdt getSettingByProgramIdAndActionDevName(String programId, String actionDevName) {
        for (Action_Engine_Setting_MVN__mdt setting : allSettings) {
            if (setting.Program_Id_MVN__c == programId && setting.Action_Type_MVN__c == actionDevName) {
                return setting;
            }
        }
        return null;
    } 

    /**
     * Set settings for tests
     * @param  mocks action engine settings
     */
    @TestVisible
    private static void setMocks(List<Action_Engine_Setting_MVN__mdt> mocks){
        testMock = mocks;
    }
}