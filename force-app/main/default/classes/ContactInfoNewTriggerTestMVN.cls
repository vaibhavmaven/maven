/**
 *  ContactInfoNewTriggerTestMVN
 *  Created By:     Paul Battisson
 *  Created On:     03/23/2016
 *  Description:    This is the test class for the ContactInfoNewTriggerMVN class.
 *                  
 **/

@isTest
private class ContactInfoNewTriggerTestMVN {
	
    static Account pat;
    static Program_Member_MVN__c member;

    static {
        TestDataFactoryMVN.createPSSettings();

        Account doc = TestDataFactoryMVN.createPhysician();
        pat = TestDataFactoryMVN.createMember();
        Program_MVN__c prog = TestDataFactoryMVN.createProgram();
        Program_Stage_MVN__c parent = TestDataFactoryMVN.createParentProgramStage(prog);
        Program_Stage_MVN__c child = TestDataFactoryMVN.createChildProgramStage(prog, parent);
        member = TestDataFactoryMVN.createProgramMember(prog, pat);
    }

	@isTest 
    private static void testInsertSingleContactInfo() {
		Contact_Information_MVN__c info = new Contact_Information_MVN__c();
        info.Account_MVN__c = pat.Id;
        info.Email_MVN__c = 'test@patsupp.com';
        info.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Contact_Information_MVN__c' AND DeveloperName = 'Email_MVN' LIMIT 1].Id;
        insert info;
        
        List<Communication_Preference_MVN__c> prefs = [SELECT Id, Contact_Information_MVN__c, Program_Member_MVN__c FROM Communication_Preference_MVN__c WHERE Program_Member_MVN__c = :member.Id];

        System.assertEquals(1, prefs.size(), 'Incorrect number of communication preferences retrieved');
        System.assertEquals(info.Id, prefs[0].Contact_Information_MVN__c, 'Incorrect contact information record');
	}

    @isTest 
    private static void testInsertMultipleContactInfo() {
        Contact_Information_MVN__c info = new Contact_Information_MVN__c();
        info.Account_MVN__c = pat.Id;
        info.Email_MVN__c = 'test@patsupp.com';
        info.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Contact_Information_MVN__c' AND DeveloperName = 'Email_MVN' LIMIT 1].Id;

        Contact_Information_MVN__c info2 = new Contact_Information_MVN__c();
        info2.Account_MVN__c = pat.Id;
        info2.Email_MVN__c = 'test2@patsupp.com';
        info2.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Contact_Information_MVN__c' AND DeveloperName = 'Email_MVN' LIMIT 1].Id;
        
        insert new List<Contact_Information_MVN__c>{info, info2};
        
        List<Communication_Preference_MVN__c> prefs = [SELECT Id, Contact_Information_MVN__c, Program_Member_MVN__c FROM Communication_Preference_MVN__c WHERE Program_Member_MVN__c = :member.Id];

        System.assertEquals(2, prefs.size(), 'Incorrect number of communication preferences retrieved');
    }
	
}