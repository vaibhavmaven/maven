/**
 * @author Mavens
 * @date November 2018
 * @description Class used to build fake data for custom metadata type for Assure Settings
 */ 
@isTest
public with sharing class TestFactoryAssureSettingMVN {

    public static void setMocks() {
        List<Assure_Setting_MVN__mdt> settings = new List<Assure_Setting_MVN__mdt>();
        Map<String, Object> setting;

        setting = new Map<String, Object>{
            'Assure_Result_Code_MVN__c' => '9',
            'Engine_Result_Message_MVN__c' => 'Denied 9',
            'Engine_Result_Value_MVN__c' => 'Denied',
            'DeveloperName' => '9_MVN'
        };

        settings.add(
            (Assure_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Assure_Setting_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Assure_Result_Code_MVN__c' => 'DEA-C',
            'Engine_Result_Message_MVN__c' => 'Missing Information DEA-C',
            'Engine_Result_Value_MVN__c' => 'Missing Information',
            'DeveloperName' => 'DEA_C_MVN'
        };

        settings.add(
            (Assure_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Assure_Setting_MVN__mdt', setting)
        );

        setting = new Map<String, Object>{
            'Assure_Result_Code_MVN__c' => 'E',
            'Engine_Result_Message_MVN__c' => 'Passed E',
            'Engine_Result_Value_MVN__c' => 'Passed',
            'DeveloperName' => 'E_MVN'
        };

        settings.add(
            (Assure_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                'Assure_Setting_MVN__mdt', setting)
        );

        AssureSettingMVN.setMocks(settings);
    }

    public static void setMocks(List<Map<String, Object>> fieldValuesList) {
        List<Assure_Setting_MVN__mdt> settings = new List<Assure_Setting_MVN__mdt>();
        for (Map<String, Object> setting : fieldValuesList) {
            settings.add(
                (Assure_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata(
                                                    'Assure_Setting_MVN__mdt', setting)
            );
        }
        AssureSettingMVN.setMocks(settings);
    }

}