/*
 *  RelationshipKeyTestMVN
 *  Created By:     Florian Hoehn
 *  Created Date:   June 13th 2017
 *  Description:    tests RelationshipKeyMVN
 */
@isTest private class RelationshipKeyTestMVN {
    /**
    * @description setups data for all unit tests
    */
    @testSetup private static void setupData() {
        TestDataFactoryMVN.createPSSettings();
        Program_MVN__c program = TestDataFactoryMVN.createProgram();
        TestDataFactoryMVN.createProgramStage(program);
        Account testAccount = TestDataFactoryMVN.createMember();
        Program_Member_MVN__c programMember = TestDataFactoryMVN.createProgramMember(program, testAccount);
        Account professional = new TestFactoryAccountMVN().createPrescriber();
    }

    /**
    * @description tests that the key is correctly generated
    */
    @isTest private static void testRelationshipKeyGeneration() {
        Program_Member_MVN__c expectedProgramMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        Account expectedMember = [SELECT Id FROM Account WHERE LastName = 'Member' LIMIT 1];
        Account expectedPhysician = [SELECT Id FROM Account WHERE Credentials_vod__c = 'MD'];
        Relationship_MVN__c relationship = new Relationship_MVN__c(
            From_Account_MVN__c = expectedMember.Id,
            To_Account_MVN__c = expectedPhysician.Id,
            From_Role_MVN__c = 'From_Role_Test',
            To_Role_MVN__c = 'To_Role_Test',
            Program_Member_MVN__c = expectedProgramMember.Id
        );

        Test.startTest();
            String actualRelationshipKey = RelationshipKeyMVN.generate(relationship);
        Test.stopTest();

        String expectedRelationshipKey = expectedMember.Id +
                                         ':' + expectedPhysician.Id +
                                         ':' + expectedProgramMember.Id;
        System.assertEquals(expectedRelationshipKey, actualRelationshipKey);
    }

    /**
    * @description tests that the key is correctly generated
    */
    @isTest private static void testRelationshipKeyGenerationWhenFromAccountIsNotSet() {
        Program_Member_MVN__c expectedProgramMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        Account expectedMember = [SELECT Id FROM Account WHERE LastName = 'Member' LIMIT 1];
        Account expectedPhysician = [SELECT Id FROM Account WHERE Credentials_vod__c = 'MD'];
        Relationship_MVN__c relationship = new Relationship_MVN__c(
            From_Account_MVN__c = null,
            To_Account_MVN__c = expectedPhysician.Id,
            From_Role_MVN__c = 'From_Role_Test',
            To_Role_MVN__c = 'To_Role_Test',
            Program_Member_MVN__c = expectedProgramMember.Id
        );

        Test.startTest();
            try {
                String actualRelationshipKey = RelationshipKeyMVN.generate(relationship);
            } catch(RelationshipKeyMVN.RelationshipKeyException relEx) {
                System.assertEquals(System.Label.Relationship_Key_Values_Blank_MVN, relEx.getMessage());
            }
        Test.stopTest();
    }

    /**
    * @description tests that the key is correctly generated
    */
    @isTest private static void testRelationshipKeyGenerationWhenToAccountIsNotSet() {
        Program_Member_MVN__c expectedProgramMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        Account expectedMember = [SELECT Id FROM Account WHERE LastName = 'Member' LIMIT 1];
        Account expectedPhysician = [SELECT Id FROM Account WHERE Credentials_vod__c = 'MD'];
        Relationship_MVN__c relationship = new Relationship_MVN__c(
            From_Account_MVN__c = expectedMember.Id,
            To_Account_MVN__c = null,
            From_Role_MVN__c = 'From_Role_Test',
            To_Role_MVN__c = 'To_Role_Test',
            Program_Member_MVN__c = expectedProgramMember.Id
        );

        Test.startTest();
            try {
                String actualRelationshipKey = RelationshipKeyMVN.generate(relationship);
            } catch(RelationshipKeyMVN.RelationshipKeyException relEx) {
                System.assertEquals(System.Label.Relationship_Key_Values_Blank_MVN, relEx.getMessage());
            }
        Test.stopTest();
    }

    /**
    * @description tests that the key is correctly generated
    */
    @isTest private static void testRelationshipKeyGenerationWhenProgramMemberIsNotSet() {
        Program_Member_MVN__c expectedProgramMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        Account expectedMember = [SELECT Id FROM Account WHERE LastName = 'Member' LIMIT 1];
        Account expectedPhysician = [SELECT Id FROM Account WHERE Credentials_vod__c = 'MD'];
        Relationship_MVN__c relationship = new Relationship_MVN__c(
            From_Account_MVN__c = expectedMember.Id,
            To_Account_MVN__c = expectedPhysician.Id,
            From_Role_MVN__c = 'From_Role_Test',
            To_Role_MVN__c = 'To_Role_Test',
            Program_Member_MVN__c = null
        );

        Test.startTest();
            try {
                String actualRelationshipKey = RelationshipKeyMVN.generate(relationship);
            } catch(RelationshipKeyMVN.RelationshipKeyException relEx) {
                System.assertEquals(System.Label.Relationship_Key_Values_Blank_MVN, relEx.getMessage());
            }
        Test.stopTest();
    }
}