/**
 *	ProgramMemberStageTriggerHandlerTestMVN
 *	Created By:		Vincent Reeder
 *	Created On:		3/1/2016
 *  Description:	This is the test class for the ProgramMemberStageTriggerHandlerMVN class
 **/

@isTest
private class ProgramMemberStageTriggerHandlerTestMVN {
	static Program_MVN__c program;
	static Program_Stage_MVN__c childStage1;
	static Program_Stage_MVN__c childStage2;
	static Program_Stage_MVN__c childStage3;
	static Program_Member_MVN__c programMember;
	static Patient_Service_Settings_MVN__c settings;

	static{
		init();
	}

	@isTest static void testCreateCaseForProgramMemberStage(){

		createProgramStageCaseFieldMaps();
		createProgramMember();

		List<Program_Member_Stage_MVN__c> programMemberStages = [SELECT Id, Status_MVN__c, Exclude_Activity_MVN__c, Program_Member_MVN__c, Program_Stage_MVN__c, Parent_Program_Member_Stage_MVN__c FROM Program_Member_Stage_MVN__c WHERE Status_MVN__c = 'Started' AND Parent_Program_Member_Stage_MVN__c != null];

		Program_Member_Stage_MVN__c programMemberStage = programMemberStages[0];

		programMemberStage.Status_MVN__c = settings.ProgramMS_Completed_Status_MVN__c;

		Test.startTest();
		update programMemberStage;
		Test.stopTest();

		programMemberStage = [SELECT Id, Activity_ID_MVN__c FROM Program_Member_Stage_MVN__c WHERE Id = :programMemberStage.Id LIMIT 1];
		System.assertEquals('Request_PS_MVN', [SELECT RecordType.DeveloperName FROM Case WHERE Id = :programMemberStage.Activity_ID_MVN__c].RecordType.DeveloperName);
		System.assertNotEquals(null, programMemberStage.Activity_ID_MVN__c);

	}


	@isTest static void testCreateCaseForProgramMemberStage_noFieldMaps(){

		createProgramMember();

		List<Program_Member_Stage_MVN__c> programMemberStages = [SELECT Id, Status_MVN__c, Exclude_Activity_MVN__c, Program_Member_MVN__c, Program_Stage_MVN__c, Parent_Program_Member_Stage_MVN__c FROM Program_Member_Stage_MVN__c WHERE Status_MVN__c = 'Started' AND Parent_Program_Member_Stage_MVN__c != null];

		System.debug('programmemberstages: \n' + programMemberStages);

		Program_Member_Stage_MVN__c programMemberStage = programMemberStages[0];
		programMemberStage.Status_MVN__c = settings.ProgramMS_Completed_Status_MVN__c;

		Test.startTest();
		update programMemberStage;
		Test.stopTest();

		programMemberStage = [SELECT Id, Activity_ID_MVN__c FROM Program_Member_Stage_MVN__c WHERE Id = :programMemberStage.Id LIMIT 1];
		System.assertNotEquals(null, programMemberStage.Activity_ID_MVN__c);

	}

	@isTest static void testCreateActivityForProgramMemberStage(){

		childStage1.Activity_Type_MVN__c = 'Task';
		update childStage1;

		createProgramStageTaskFieldMaps();
		createProgramMember();

		List<Program_Member_Stage_MVN__c> programMemberStages = [SELECT Id, Status_MVN__c, Exclude_Activity_MVN__c, Program_Member_MVN__c, Program_Stage_MVN__c, Parent_Program_Member_Stage_MVN__c FROM Program_Member_Stage_MVN__c WHERE Status_MVN__c = 'Started' AND Parent_Program_Member_Stage_MVN__c != null];

		Program_Member_Stage_MVN__c programMemberStage = programMemberStages[0];

		programMemberStage.Status_MVN__c = settings.ProgramMS_Completed_Status_MVN__c;

		Test.startTest();
		update programMemberStage;
		Test.stopTest();

		programMemberStage = [SELECT Id, Activity_ID_MVN__c FROM Program_Member_Stage_MVN__c WHERE Id = :programMemberStage.Id LIMIT 1];
		System.assertNotEquals(null, programMemberStage.Activity_ID_MVN__c);
	}


	@isTest static void testCreateActivityForProgramMemberStage_noFieldMaps(){

		childStage1.Activity_Type_MVN__c = 'Task';
		update childStage1;
		createProgramMember();

		List<Program_Member_Stage_MVN__c> programMemberStages = [SELECT Id, Status_MVN__c, Exclude_Activity_MVN__c, Program_Member_MVN__c, Program_Stage_MVN__c, Parent_Program_Member_Stage_MVN__c FROM Program_Member_Stage_MVN__c WHERE Status_MVN__c = 'Started' AND Parent_Program_Member_Stage_MVN__c != null];

		System.debug('programmemberstages: \n' + programMemberStages);

		Program_Member_Stage_MVN__c programMemberStage = programMemberStages[0];

		programMemberStage.Status_MVN__c = settings.ProgramMS_Completed_Status_MVN__c;

		Test.startTest();
		update programMemberStage;
		Test.stopTest();

		programMemberStage = [SELECT Id, Activity_ID_MVN__c FROM Program_Member_Stage_MVN__c WHERE Id = :programMemberStage.Id LIMIT 1];
		System.assertNotEquals(null, programMemberStage.Activity_ID_MVN__c);

	}

	static void init(){
		settings = TestDataFactoryMVN.buildPSSettings();
		insert settings;

		program = TestDataFactoryMVN.createProgram();

		Program_Stage_MVN__c parentStage = TestDataFactoryMVN.createParentProgramStage(program);

		childStage1 = TestDataFactoryMVN.createChildProgramStage(program, parentStage);
		childStage2 = TestDataFactoryMVN.createChildProgramStage(program, parentStage);
		childStage3 = TestDataFactoryMVN.createChildProgramStage(program, parentStage);

		Program_Stage_Dependency_MVN__c dependency1 = TestDataFactoryMVN.createStageDependency(parentStage, childStage1);
		Program_Stage_Dependency_MVN__c dependency2 = TestDataFactoryMVN.createStageDependency(childStage1, childStage2);
		Program_Stage_Dependency_MVN__c dependency3 = TestDataFactoryMVN.createStageDependency(childStage2, childStage3);

	}

	static void createProgramStageTaskFieldMaps() {
		List<Program_Stage_Field_Map_MVN__c> psfms = new List<Program_Stage_Field_Map_MVN__c>();
		psfms.add(new Program_Stage_Field_Map_MVN__c(
			Program_Stage_MVN__c = childStage1.Id,
			Field_MVN__c = 'Status',
			Object_MVN__c = 'Task',
			Value_MVN__c = 'New'
		));
		psfms.add(new Program_Stage_Field_Map_MVN__c(
			Program_Stage_MVN__c = childStage1.Id,
			Field_MVN__c = 'Priority',
			Object_MVN__c = 'Task',
			Value_MVN__c = 'Normal'
		));
		insert psfms;
	}

	static void createProgramStageCaseFieldMaps() {
		List<Program_Stage_Field_Map_MVN__c> psfms = new List<Program_Stage_Field_Map_MVN__c>();

		Program_Stage_Field_Map_MVN__c programStageFieldMap1 = new Program_Stage_Field_Map_MVN__c();
		programStageFieldMap1.Program_Stage_MVN__c = childStage1.Id;
		programStageFieldMap1.Field_MVN__c = '$RecordType';
		programStageFieldMap1.Object_MVN__c = 'Case';
		programStageFieldMap1.Value_MVN__c = 'Request_PS_MVN';
		psfms.add(programStageFieldMap1);

		Program_Stage_Field_Map_MVN__c programStageFieldMap2 = new Program_Stage_Field_Map_MVN__c();
		programStageFieldMap2.Program_Stage_MVN__c = childStage1.Id;
		programStageFieldMap2.Field_MVN__c = 'Custom_Field_MVN__c';
		programStageFieldMap2.Object_MVN__c = 'Case';
		programStageFieldMap2.Value_MVN__c = 'A value';
		psfms.add(programStageFieldMap2);

		Program_Stage_Field_Map_MVN__c programStageFieldMap3 = new Program_Stage_Field_Map_MVN__c();
		programStageFieldMap3.Program_Stage_MVN__c = childStage1.Id;
		programStageFieldMap3.Field_MVN__c = 'Status';
		programStageFieldMap3.Object_MVN__c = 'Case';
		programStageFieldMap3.Value_MVN__c = '$PM.Status_MVN__c';
		psfms.add(programStageFieldMap3);

		Program_Stage_Field_Map_MVN__c programStageFieldMap6 = new Program_Stage_Field_Map_MVN__c();
		programStageFieldMap6.Program_Stage_MVN__c = childStage1.Id;
		programStageFieldMap6.Field_MVN__c = 'Last_Call_Attempt_Date_MVN__c';
		programStageFieldMap6.Object_MVN__c = 'Case';
		programStageFieldMap6.Value_MVN__c = '$PM.Enrollment_Date_MVN__c';
		//psfms.add(programStageFieldMap6);

		Program_Stage_Field_Map_MVN__c programStageFieldMap8 = new Program_Stage_Field_Map_MVN__c();
		programStageFieldMap8.Program_Stage_MVN__c = childStage1.Id;
		programStageFieldMap8.Field_MVN__c = 'Last_Call_Attempt_Date_MVN__c';
		programStageFieldMap8.Object_MVN__c = 'Case';
		programStageFieldMap8.Value_MVN__c = '$PM.Enrollment_Date_MVN__c+7';
		//psfms.add(programStageFieldMap8);

		Program_Stage_Field_Map_MVN__c programStageFieldMap10 = new Program_Stage_Field_Map_MVN__c();
		programStageFieldMap10.Program_Stage_MVN__c = childStage1.Id;
		programStageFieldMap10.Field_MVN__c = 'Last_Call_Attempt_Date_MVN__c';
		programStageFieldMap10.Object_MVN__c = 'Case';
		programStageFieldMap10.Value_MVN__c = '$Today()';
		psfms.add(programStageFieldMap10);

		Program_Stage_Field_Map_MVN__c programStageFieldMap12 = new Program_Stage_Field_Map_MVN__c();
		programStageFieldMap12.Program_Stage_MVN__c = childStage1.Id;
		programStageFieldMap12.Field_MVN__c = 'Last_Call_Attempt_Date_MVN__c';
		programStageFieldMap12.Object_MVN__c = 'Case';
		programStageFieldMap12.Value_MVN__c = '$Today()+7';
		psfms.add(programStageFieldMap12);

		insert psfms;
	}

	static void createProgramMember() {
		Account memberAccount = TestDataFactoryMVN.createMember();
		programMember = TestDataFactoryMVN.createProgramMember(program, memberAccount);
	}
}