/*
 * TestFactoryProgramMVN
 * Created By: Kyle Thornton
 * Created Date: 4/7/2017
 * Description: This class contains unit tests for the AddressToContactInfoTriggerMVN class
 */
@isTest
public class TestFactoryProgramMVN {

    private TestFactorySObjectMVN objectFactory;

    public TestFactoryProgramMVN() {
        Map<String, Object> defaultValues = new Map<String, Object>{
            'Name'=>'Test Program',
            'Description_MVN__c'=>'For all Test Program Patients',
            'Country_MVN__c'=>'US',
            'Active_MVN__c'=>true
        };

        objectFactory = new TestFactorySObjectMVN('Program_MVN__c', defaultValues);
    }

    public Program_MVN__c construct() {
        return construct(null);
    }

    public Program_MVN__c construct(Map<String,Object> valuesByField) {
        return (Program_MVN__c) objectFactory.constructSObject(valuesByField);
    }

    public Program_MVN__c create() {
        return create(null);
    }

    public Program_MVN__c create(Map<String,Object> valuesByField) {
        return (Program_MVN__c) objectFactory.createSObject(valuesByField);
    }
}