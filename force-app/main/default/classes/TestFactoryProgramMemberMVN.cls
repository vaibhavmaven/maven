/*
 * TestFactoryProgramMemberMVN
 * Created By: Kyle Thornton
 * Created Date: 4/7/2017
 * Description: This class is used to construct Program Member objects
 */
@isTest
public class TestFactoryProgramMemberMVN {

    private TestFactorySObjectMVN objectFactory;

    public TestFactoryProgramMemberMVN() {
        objectFactory = new TestFactorySObjectMVN('Program_Member_MVN__c', new Map<String,Object>());
    }

    public Program_Member_MVN__c construct() {
        return construct(null);
    }

    public Program_Member_MVN__c construct(Map<String,Object> valuesByField) {
        return (Program_Member_MVN__c) objectFactory.constructSObject(valuesByField);
    }

    public List<Program_Member_MVN__c> constructMany(Integer numOfProgramMembers, Map<String,Object> valuesByField) {
        return (List<Program_Member_MVN__c>) objectFactory.constructSObjects(numOfProgramMembers, valuesByField);
    }

    public Program_Member_MVN__c create() {
        return create(null);
    }

    public Program_Member_MVN__c create(Map<String,Object> valuesByField) {
        return (Program_Member_MVN__c) objectFactory.createSObject(valuesByField);
    }

    public List<Program_Member_MVN__c> create(Program_MVN__c program,
                                              Account prescriber,
                                              List<Account> patients)
    {
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>();

        for (Account patient : patients) {
            Id prescriberId = prescriber == null ? null : prescriber.Id;
            Map<String,Object> fieldValues = new Map<String,Object> {
                'Program_MVN__c' => program.Id,
                'Physician_MVN__c' => prescriberId,
                'Member_MVN__c' => patient.Id,
                'Do_Not_Initialize_MVN__c' => false
            };
            programMembers.add(construct(fieldValues));
        }

        insert programMembers;
        return programMembers;
    }
}