@isTest
public class RTBI_Retrieve_OnlyTest {
    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
    static List<Program_Member_MVN__c> programMembers;
    static List<Account> patients;
    static Program_MVN__c program;
    static Prescription_MVN__c prescription;
    static Benefits_Coverage_MVN__c benefitCoverage;
    static Product_MVN__c productFamily;
    static Product_Line_Item_MVN__c product;
    
    public static Map<String, RecordType> accountRecordTypesByName {
        get {
            if (accountRecordTypesByName == null) {
                accountRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
                    accountRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return accountRecordTypesByName;
        }
        private set;
    }
    
    public static List<Account> createPatients(Integer numOfPatients) {
        List<Account> patientsList = new List<Account>();
        // we are creating 20 patients .. so diving these 20 in grousp of M, F, Male, Female.
        List<String> genderList = new List<String>{'Female', 'Male', 'M', 'F'}; 
            Integer counter = 1; 
        for(String genderStr : genderList){
            system.debug('Creating patients for Gender : ' + genderStr); 
            for (Integer i=0; i<5; i++) {
                Account patient= new Account();
                patient.FirstName = 'Test';
                patient.MiddleName = 'testing';
                patient.LastName = 'Patient' + counter;
                patient.Gender_MVN__c = genderStr; 
                System.debug(patient.LastName);
                patient.RecordTypeId = accountRecordTypesByName.get('Patient_MVN').id;
                counter++;          
                patientsList.add(patient);
            }            
        }
        insert patientsList;
        return patientsList;
    }
    
    static void initializeData(){
        patients = createPatients(20);    
        program = new Program_MVN__c();
        insert program;
        Program_Member_MVN__c programMember = new Program_Member_MVN__c();
        programMember.Program_MVN__c = program.id;
        programMember.Member_MVN__c = patients[0].id;
        programMember.Do_Not_Initialize_MVN__c = false;
        insert programMember;
        
        programMembers = new List<Program_Member_MVN__c>();
        programMembers.add(programMember);
        
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        List<Application_MVN__c> apps = new List<Application_MVN__c>();
        
        for (Integer i=0; i < 10; i++) {
            apps.add(
                applicationFactory.construct(
                    new Map<String, Object>{
                        'Program_Member_MVN__c' => programMembers[0].Id
                            }
                )
            );
        }
        insert apps;
        
        benefitCoverage= new  Benefits_Coverage_MVN__c();
        benefitCoverage.Program_Member_MVN__c = programMembers[0].Id;
        benefitCoverage.Application_MVN__c = apps[0].Id;
        insert benefitCoverage;
        
        productFamily = new Product_MVN__c();
        productFamily.Program_MVN__c = program.Id;
        productFamily.Product_Status_MVN__c = 'Active';
        insert productFamily;
        
        product = new Product_Line_Item_MVN__c();
        product.Product_MVN__c = productFamily.Id;
        product.Packaged_Quantity_MVN__c = 5;
        product.NDC_MVN__c = '123';
        insert product;
        
        prescription = new Prescription_MVN__c();
        prescription.Application_MVN__c = apps[0].Id;
        prescription.Program_Member_MVN__c = programMember.Id;
        prescription.Product_MVN__c =productFamily.Id;
        prescription.Timeframe_MVN__c = '30';
        prescription.Refills_MVN__c = 10;
        prescription.Quantity_MVN__c = 10;
        prescription.Signal_Code_MVN__c = 'QD - Once a day';   
        insert prescription;
        
        RTBI_Response_Master__c rtbiRespMaster1=new RTBI_Response_Master__c();
        rtbiRespMaster1.NDC__c = '1234567887';
        rtbiRespMaster1.Last_Query_Date__c = Datetime.now();
        rtbiRespMaster1.Benefits_Coverage__c = benefitCoverage.Id; 
        rtbiRespMaster1.RTBI_View_Only_Flag__c=true;
        insert rtbiRespMaster1;
    }
    
    @isTest
    public static void testcheckEligibilityRTBISuccess(){   
        initializeData();
        Benefits_Coverage_MVN__c benefitCoverage = [select id, Program_Member_MVN__c, Application_MVN__c from Benefits_Coverage_MVN__c];
        Test.setMock(HttpCalloutMock.class, new EligibilityCheckerHttpCalloutMockRTBI());
        Test.startTest();
        RTBI_Retrieve_Only.getRTBI_Retrieve_Only(benefitCoverage.Id);
        Test.stopTest();
    }
    
    @isTest
    public static void testcheckEligibilityRTBIFailure(){   
        Test.setMock(HttpCalloutMock.class, new EligibilityCheckerHttpCalloutMockRTBI());
        Test.startTest();
        RTBI_Retrieve_Only.getRTBI_Retrieve_Only('');
        Test.stopTest();
    }
    
    private class EligibilityCheckerHttpCalloutMockRTBI implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest httpReq)
        {
            HttpResponse httpRes = new HttpResponse(); 
            httpRes.setHeader('Content-Type', 'application/json'); 
            httpRes.setStatusCode(200);
            Map<String, Object> mapmap=new Map<String, Object>();
            Map<String, Object> elgbResp =new Map<String, Object> ();
            
            String jason =  '['+
                ' {'+
                ' \"Pharmacy\": {'+
                ' \"Type\": \"Mail\",'+
                ' \"NCPDPID\": \"2323239\",'+
                ' \"NPI\": \"1538460530\",'+
                ' \"PharmacyName\": \"Mail Order Pharmacy 10.6MU\",'+
                ' \"AddressLine1\": \"9292 Langley Rd\",'+
                ' \"AddressLine2\": \"Suite 100\",'+
                ' \"City\": \"Phoenix\",'+
                ' \"State\": \"AZ\",'+
                ' \"Zip\": \"85001\"'+
                ' },'+
                ' \"PricingCoverage\": {'+
                '\"DrugStatus\": \"CoveredWithRestrictions\",'+
                '\"Message\": \"\",'+
                '\"QuantityPriced\": \"120\",'+
                '\"QuantityUnitDescription\": \"Tablet\",'+
                '\"DaysSupplyPriced\": \"30\",'+
                '\"PlanPayAmount\": \"193\",'+
                '\"EstimatedPatientPayAmount\": \"20\",'+
                '\"PriorAuthorizationRequired\": \"\",'+
                '\"OOPAppliedAmount\": \"\",'+
                '\"OOPRemainingAmount\": \"\",'+
                '\"DeductibleAppliedAmount\": \"0\",'+
                '\"DeductibleRemainingAmount\": \"0\",'+
                '\"FormularyStatus\": \"\"'+
                ' },'+
                ' \"CoverageAlerts\": [{'+
                '\"ReferenceCode\": \"QuantityLimits - 30 per 30 Days\",'+
                '\"TextMessage\": \"TextMessage - THIS DRUG REQUIRES A NEBULIZER\",'+
                '\"ReferenceText\": \"ResourceLink - www.xyz.com\"'+ 
                ' }]'+ 
                '}'+
                ' ]';
            
            System.JSONParser parser = System.JSON.createParser(jason);
            RTBIResponse.PatientSpecificPricingCoverages  resp= new RTBIResponse.PatientSpecificPricingCoverages(parser);
            elgbResp.put('PatientSpecificPricingCoverages',new List<RTBIResponse.PatientSpecificPricingCoverages>{resp});
            elgbResp.put('Status','True');
            mapmap.put('EligibilityResponse',elgbResp);
            httpRes.setBody(JSON.serialize(mapmap));
            return httpRes; 
        }
    }
}