/**
* @author      : Persistent Systems.
* @date        : September 12 2019
* @description : Trigger handler class for "Product_MVN__C" for "After update" event. 
                 1. This logic will execute only when you turn check the checkbox "Update Existing Product Type	" 
                    in the custom metadata settings called "Product_Update_Setting_PAP__mdt". 
                 2. In this handler we just update the value of "Decrement_Counter_PAP__c" field on Prescription 
                    object, thus firing the update trigger for Prescription object, which will take care of 
                    updating the count of "First product counter" and "Second Product Counter" and mark the check boxes 
                    accordingly.                 
*/

public class PAP_ProductUpdateHandler implements TriggerHandlerMVN.HandlerInterface {
    @TestVisible private static boolean updateSettings = false;
    
    public void handle(){
        system.debug('Inside PAP_ProductUpdateHandler'); 
        if(Trigger.isUpdate && Trigger.isAfter){         
            // check for the cusotm metadata setting here.           
            Boolean metadataSettings ; 
            metadataSettings = getUpdateSettings(); 
            if(Test.isRunningTest()){
                metadataSettings = updateSettings; 
            }
            if(!metadataSettings){
                // either custom metadata not set or is set to false. not executing the update handler any further. 
                return; 
            }            
           
            // for each updated Product_MVN__c object in this trigger. 
            system.debug('Processing Product Update Trigger.'); 
            List<Prescription_MVN__C> updatedList = new List<Prescription_MVN__C>(); 
            for(ID prodID : Trigger.newMap.keySet()){
                // will fire only when Product type has chagned. 
                if(((Product_MVN__C)Trigger.oldMap.get(prodID)).Product_Type_PAP__c != ((Product_MVN__C)Trigger.newMap.get(prodID)).Product_type_PAP__c){
                    //for all Prescriptions linked to Acitve applications only which are attached to this Product_MVN__c 
                    for(List<Prescription_MVN__c> prscpList: [select Decrement_Counter_PAP__c 
                                                              from Prescription_MVN__c 
                                                              where Product_MVN__c = :prodID 
                                                              and Application_MVN__r.Is_Application_Active_MVN__c = true]){
                             if(prscpList == null && prscpList.size() == 0){
                                 continue; // process for the next product.                                                                      
                             }
                             for(Prescription_MVN__c prscp: prscpList){
                                 // decrement the counter only when the old product type exists, else we do not decrement. 
                                 prscp.Decrement_Counter_PAP__c = String.isEmpty(((Product_MVN__C)Trigger.oldMap.get(prodID)).Product_Type_PAP__c) ? false : true ; 
                                 updatedList.add(prscp);    
                                 system.debug('Changed value of Prescripton : ' + prscp.Id); 
                             }// populate the updated prescription list.                        
                    }// for()
                }// if product type has changed. 
            }// for iterate over Trigger elements. 
            if(updatedList.size() > 0){
                try{
                    Database.update(updatedList, false);
                }catch(Exception e){
                    system.debug('Exception encountered while saving updated list. Details: ' + e.getMessage()); 
                    system.debug(e.getStackTraceString());   
                    return; 
                }// try-catch.                
                system.debug('Successfully updatd number of Prescriptions : '+ updatedList.size());
                system.debug('Updated Prescriptions are : ' + updatedList); 
            }// update modified Prescriptions. 
        }// If the Trigger is after Update. 
    }// handle()
    
    private Boolean getUpdateSettings(){
        List<Product_Update_Setting_PAP__mdt> updateSettings ; 
        updateSettings = [select Update_Existing_Product_Type_PAP__c 
                          from Product_Update_Setting_PAP__mdt 
                          where MasterLabel = 'Update Products' 
                          limit 1]; 
        if(updateSettings == null || updateSettings.size() == 0){
            system.debug('ERROR: Custom metadata Product_Update_Setting_PAP__mdt is not set.'); 
            return false;             
        }
        // if the checkbox is not checked in custom metadata we will return from here. 
        if(updateSettings[0].Update_Existing_Product_Type_PAP__c == false){
            system.debug('Update_Existing_Product_Type_PAP__c is set to false. Not updating Product Type and Prescriptions.'); 
            return false; 
        }// if  
        return true;         
    } //getUpdateSettings()
}// classs