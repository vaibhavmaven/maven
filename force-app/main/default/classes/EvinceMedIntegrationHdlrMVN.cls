public class EvinceMedIntegrationHdlrMVN {
    
    //@future (callout=true)
    public  void CallEvinceMedIntegration(){
        
        try{
             String returnValue = '';
            JSONGenerator gen = JSON.createGenerator(true); 
            Account acc= [select FirstName,MiddleName,LastName,PersonBirthdate,Gender_MVN__c,ShippingCountry,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode from Account where id = '0010m00000HAHOqAAP'];
            gen.writeStartObject();   
            gen.writeStringField('Token', 'sdsds');
            gen.writeFieldName('Patient');
            gen.writeStartObject();
            gen.writeObjectField('FirstName ', acc.FirstName);
            gen.writeObjectField('LastName', acc.LastName);
            gen.writeDateField('DateOfBirth',  acc.PersonBirthdate);
            gen.writeStringField('Gender', acc.Gender_MVN__c);
            gen.writeFieldName('Address');
            gen.writeStartObject();
            gen.writeObjectField('FirstLine', acc.ShippingStreet+', '+acc.ShippingCity+', '+acc.ShippingState+', '+acc.ShippingCountry);
            gen.writeStringField('SecondLine', '' );
            gen.writeObjectField('ZipCode', acc.ShippingPostalCode);
            gen.writeEndObject();
            gen.writeEndObject();
            gen.writeEndObject();   
            String jsonS = gen.getAsString();
            System.debug('Request'+jsonS);
            //calling rest web service
           EvinceMedIntegrationWebServiceMVN reqst = new EvinceMedIntegrationWebServiceMVN();
            reqst.EvinceMedIntegrationWebService(jsonS);
        }
        catch(exception e){
            system.debug(e);
        }
        
    }
    // public HttpResponse CallEvinceMedIntegrationWebService(Http http, HttpRequest req) {
    //Invoke Web Service
    //  HttpResponse res = http.send(req); 
    //  return res;        
    // }
    
    
}