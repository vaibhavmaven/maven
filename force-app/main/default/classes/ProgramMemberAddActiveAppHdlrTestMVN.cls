/**
 * @author Mavens
 * @description tests ProgramMemberAddActiveAppHdlrMVN
 * @group ProgramMember, Application
 */
@isTest private class ProgramMemberAddActiveAppHdlrTestMVN {
    static User caseManager;
    static Program_Member_MVN__c programMember;

    /**
    * @description setups data for all unit tests
    */
    static {
        TestFactoryCustomMetadataMVN.setMocks();
        TestDataFactoryMVN.createPSSettings();

        caseManager = TestDataFactoryMVN.generateCaseManager();
        System.runAs(caseManager) {
            Program_MVN__c program = TestDataFactoryMVN.createProgram();
            Account testAccount = TestDataFactoryMVN.createMember();
            programMember = TestDataFactoryMVN.createProgramMember(program, testAccount);
        }
    }

    @isTest private static void onApplicationInsertAddActiveAppToProgramMember() {
        Application_MVN__c expectedApplication;

        System.runAs(caseManager) {
            Test.startTest();
                expectedApplication = new Application_MVN__c(
                    Program_Member_MVN__c = programMember.Id
                );
                insert expectedApplication;
            Test.stopTest();
        }

        Program_Member_MVN__c actualProgramMember = [
            SELECT
                Id,
                Active_Application_MVN__c
            FROM
                Program_Member_MVN__c
            WHERE
                Id = :programMember.Id
        ];

        System.assertNotEquals(null, actualProgramMember.Active_Application_MVN__c);
        System.assertEquals(
            expectedApplication.Id,
            actualProgramMember.Active_Application_MVN__c
        );
    }
}