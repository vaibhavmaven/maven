/*
*   DocumentEditExtensionTestMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 13th, 2017
*   Description:    tests DocumentTriggerHandlerMVN and all its functioanlity
*                   BULK tests are covered inside AttachmentInsertTriggerTestMVN
*/
@isTest private class DocumentTriggerHandlerTestMVN {
    /**
    * @description setups data for all unit tests
    */
    @testSetup private static void setupData() {
        TestDataFactoryMVN.createPSSettings();
        Program_MVN__c program = TestDataFactoryMVN.createProgram();
        TestDataFactoryMVN.createProgramStage(program);
        Account testAccount = TestDataFactoryMVN.createMember();
        Program_Member_MVN__c programMember = TestDataFactoryMVN.createProgramMember(program, testAccount);
        Case caseRecord = TestDataFactoryMVN.createTestCase();
        caseRecord.Program_Member_MVN__c = programMember.Id;
        update caseRecord;
    }

    /**
    * @description tests that documents are created when attachment is uploaded
    */
    @isTest private static void testAttachmentUplaodOnCaseReParentsAttachment() {
        Case expectedCase = [SELECT Id, Program_Member_MVN__c FROM Case LIMIT 1];
        Attachment expectedAttachment = new Attachment(
            Name = 'TestName',
            Body = Blob.valueOf('TestBody'),
            ParentId = expectedCase.Id
        );

        Test.startTest();
            insert expectedAttachment;
        Test.stopTest();

        Document_MVN__c actualDocument = [SELECT Id, Program_Member_MVN__c, Case_MVN__c, Title_MVN__c,
                                                 (SELECT Id, Name FROM Attachments)
                                            FROM Document_MVN__c
                                           LIMIT 1];
        System.assertNotEquals(null, actualDocument);
        System.assertEquals(expectedCase.Id, actualDocument.Case_MVN__c);
        System.assertEquals(expectedCase.Program_Member_MVN__c, actualDocument.Program_Member_MVN__c);
        System.assertEquals(expectedAttachment.Name, actualDocument.Title_MVN__c);
    }
}