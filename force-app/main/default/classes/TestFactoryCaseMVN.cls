/*
 * TestFactoryCaseMVN
 * Created By: Roman Lerman
 * Created Date: 5/4/2017
 * Description: This class is used to construct Case objects
 */
@isTest
public class TestFactoryCaseMVN {

    private TestFactorySObjectMVN objectFactory;

    public static Map<String, RecordType> caseRecordTypesByName {
        get {
            if (caseRecordTypesByName == null) {
                caseRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT DeveloperName FROM RecordType WHERE SObjectType = 'Case']) {
                    caseRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return caseRecordTypesByName;
        }
        private set;
    }

    public TestFactoryCaseMVN() {
        objectFactory = new TestFactorySObjectMVN('Case', new Map<String, Object>());
    }

    public Case construct() {
        return construct(null);
    }

    public List<Case> constructMany(Integer numOfCases, Map<String,Object> valuesByField) {
        return (List<Case>) objectFactory.constructSObjects(numOfCases, valuesByField);
    }

    public Case construct(Map<String,Object> valuesByField) {
        return (Case) objectFactory.constructSObject(valuesByField);
    }

    public Case create() {
        return create(null);
    }

    public List<Case> createMany(Integer numOfCases, Map<String,Object> valuesByField) {

        return (List<Case>) objectFactory.createSObjects(numOfCases, valuesByField);
    }

    public Case create(Map<String,Object> valuesByField) {
        return (Case) objectFactory.createSObject(valuesByField);
    }

}