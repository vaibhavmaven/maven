@isTest
public class PAP_SRFAX_HTTPMOCK_SUCCESS implements HttpCalloutMock {
    
    // Implement respond method to implement the HttpCalloutMock interface
    public HttpResponse respond(HttpRequest request){
          String json = '{'+
        '    \"Status\": \"Success\",'+
        '    \"Result\": ['+
        '        {'+
        '            \"FileName\": \"20190503002734_77|499443211\",'+
        '            \"FaxDetailsID\": \"499443211\",'+
        '            \"SentStatus\": \"Failed\",'+
        '            \"DateQueued\": \"May 03/19 03:27 AM\",'+
        '            \"DateSent\": \"May 03/19 03:40 AM\",'+
        '            \"EpochTime\": 1556869202,'+
        '            \"ToFaxNumber\": \"011917126691111\",'+
        '            \"Pages\": \"1\",'+
        '            \"Duration\": \"88\",'+
        '            \"RemoteID\": \"\",'+
        '            \"ErrorCode\": \"No Answer\",'+
        '            \"Size\": \"71558\",'+
        '            \"AccountCode\": \"\"'+
        '        }, '+
        '        {'+
        '            \"FileName\": \"20190503002734_77|499443211\",'+
        '            \"FaxDetailsID\": \"499443211\",'+
        '            \"SentStatus\": \"Sent\",'+
        '            \"DateQueued\": \"May 03/19 03:27 AM\",'+
        '            \"DateSent\": \"May 03/19 03:40 AM\",'+
        '            \"EpochTime\": 1556869202,'+
        '            \"ToFaxNumber\": \"011917126691111\",'+
        '            \"Pages\": \"1\",'+
        '            \"Duration\": \"88\",'+
        '            \"RemoteID\": \"\",'+
        '            \"ErrorCode\": \"No Answer\",'+
        '            \"Size\": \"71558\",'+
        '            \"AccountCode\": \"\"'+
        '        }      '+
        '    ]'+
        '}';
      
        HttpResponse response = new HttpResponse(); 
        response.setHeader('Content-Type', 'application/json');
        response.setBody(json); 
        response.setStatus('Success'); 
        response.setStatusCode(200); 
        return response; 
    }// respond()
}//class