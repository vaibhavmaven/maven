/*
*   MedicalHistoryControllerMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 16, 2016
*   Description:    Test class for the MedicalHistoryItemControllerMVN class.
*   ASSUMPTIONS:    There needs to be an 'Allergies_MVN' RecordType and FieldSet in the org for tests to run properly
*                   There needs to be an 'ReadOnly' FieldSet in the org for tests to run properly
*/
@isTest private class MedicalHistoryItemControllerTestMVN {
    @testSetup 
    static void setupData() {
        TestDataFactoryMVN.createPSSettings();
        Account member = TestDataFactoryMVN.createMember();
        Program_MVN__c program = TestDataFactoryMVN.createProgram();

        Medical_History_Type_MVN__c type = new Medical_History_Type_MVN__c(
            Record_Type_Name_MVN__c = 'Allergies_MVN',
            Program_MVN__c = program.Id,
            Order_MVN__c = 1
        );
        insert type;

        Program_Member_MVN__c progMember = TestDataFactoryMVN.createProgramMember(program, member);
        RecordType recordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Medical_History_MVN__c' AND DeveloperName = 'Allergies_MVN'];

        Medical_History_MVN__c history = new Medical_History_MVN__c(
            RecordType = recordType,
            RecordTypeId = recordType.Id,
            Program_Member_MVN__c = progMember.Id
        );
        insert history;
    }

    @isTest static void testConstructorIsInitiated() {
        Medical_History_MVN__c expectedMedicalHistory = [SELECT Id, Program_Member_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Medical_History_MVN__c LIMIT 1];
        MedicalHistoryItemMVN medicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);

        Test.startTest();
            MedicalHistoryItemControllerMVN actualMedicalHistoryItemController = new MedicalHistoryItemControllerMVN();
            actualMedicalHistoryItemController.medicalHistory = medicalHistory;
        Test.stopTest();

        MedicalHistoryItemTestMVN.assertMedicalHistoryItemMVN(expectedMedicalHistory, actualMedicalHistoryItemController.medicalHistory);
    }

    @isTest static void testFieldSetIsReturned() {
        Medical_History_MVN__c expectedMedicalHistory = [SELECT Id, Program_Member_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Medical_History_MVN__c LIMIT 1];
        MedicalHistoryItemMVN medicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);
        MedicalHistoryItemControllerMVN actualMedicalHistoryItemController = new MedicalHistoryItemControllerMVN();
        actualMedicalHistoryItemController.medicalHistory = medicalHistory;
        List<Schema.FieldSetMember> expectedFieldSet = Schema.SObjectType.Medical_History_MVN__c.fieldSets.getMap().get('Allergies_MVN').getFields();

        Test.startTest();
            List<Schema.FieldSetMember> actualFieldSet = actualMedicalHistoryItemController.fieldSet;
        Test.stopTest();

        for(Integer index = 0; index < expectedFieldSet.size(); index++) {
            System.assertEquals(expectedFieldSet.get(index).getFieldPath(), actualFieldSet.get(index).getFieldPath());
        }
    }

    @isTest static void testFieldSetErrorIsThrown() {
        Medical_History_MVN__c expectedMedicalHistory = [SELECT Id, Program_Member_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Medical_History_MVN__c LIMIT 1];
        MedicalHistoryItemMVN medicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);
        medicalHistory.item.RecordType.DeveloperName = 'TEST_DOESNOTEXIST';
        MedicalHistoryItemControllerMVN actualMedicalHistoryItemController = new MedicalHistoryItemControllerMVN();
        actualMedicalHistoryItemController.medicalHistory = medicalHistory;

        Test.startTest();
            List<Schema.FieldSetMember> actualFieldSet = actualMedicalHistoryItemController.fieldSet;
        Test.stopTest();

        System.assertEquals(null, actualFieldSet);
        System.assertEquals(true, ApexPages.hasMessages());
        System.assert(ApexPages.getMessages().get(0).getDetail().contains('TEST_DOESNOTEXIST'));
    }

    @isTest static void testisNumberOfFieldsOddReturnedCorrectly() {
        Medical_History_MVN__c expectedMedicalHistory = [SELECT Id, Program_Member_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Medical_History_MVN__c LIMIT 1];
        MedicalHistoryItemMVN medicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);
        MedicalHistoryItemControllerMVN actualMedicalHistoryItemController = new MedicalHistoryItemControllerMVN();
        actualMedicalHistoryItemController.medicalHistory = medicalHistory;
        List<Schema.FieldSetMember> expectedFieldSet = Schema.SObjectType.Medical_History_MVN__c.fieldSets.getMap().get('Allergies_MVN').getFields();
        Boolean expectedIsNumberOfFieldsOdd = (System.Math.mod(expectedFieldSet.size(), 2) == 1 ? true : false);

        Test.startTest();
            List<Schema.FieldSetMember> actualFieldSet = actualMedicalHistoryItemController.fieldSet;
            Boolean actualIsNumberOfFieldsOdd = actualMedicalHistoryItemController.isNumberOfFieldsOdd;
        Test.stopTest();

        for(Integer index = 0; index < expectedFieldSet.size(); index++) {
            System.assertEquals(expectedFieldSet.get(index).getFieldPath(), actualFieldSet.get(index).getFieldPath());
        }
        System.assertEquals(expectedIsNumberOfFieldsOdd, actualIsNumberOfFieldsOdd);
    }

    @isTest static void testSaveIsSuccess() {
        Program_Member_MVN__c progMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        RecordType recordType = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Medical_History_MVN__c' AND DeveloperName = 'Allergies_MVN'];
        Medical_History_MVN__c expectedMedicalHistory = new Medical_History_MVN__c(
            RecordType = recordType,
            RecordTypeId = recordType.Id,
            Program_Member_MVN__c = progMember.Id
        );
        MedicalHistoryItemMVN medicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);
        MedicalHistoryItemControllerMVN actualMedicalHistoryItemController = new MedicalHistoryItemControllerMVN();
        actualMedicalHistoryItemController.medicalHistory = medicalHistory;
        System.assertEquals(null, actualMedicalHistoryItemController.medicalHistory.item.Id);

        Test.startTest();
            actualMedicalHistoryItemController.save();
        Test.stopTest();

        System.assertNotEquals(null, actualMedicalHistoryItemController.medicalHistory.item.Id);
        MedicalHistoryItemTestMVN.assertMedicalHistoryItemMVN(expectedMedicalHistory, actualMedicalHistoryItemController.medicalHistory);
    }

    @isTest static void testEditEditModeIsChanged() {
        Medical_History_MVN__c expectedMedicalHistory = [SELECT Id, Program_Member_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Medical_History_MVN__c LIMIT 1];
        MedicalHistoryItemMVN medicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);
        MedicalHistoryItemControllerMVN actualMedicalHistoryItemController = new MedicalHistoryItemControllerMVN();
        actualMedicalHistoryItemController.medicalHistory = medicalHistory;
        System.assertEquals(false, actualMedicalHistoryItemController.medicalHistory.isEditMode);

        Test.startTest();
            actualMedicalHistoryItemController.edit();
        Test.stopTest();

        System.assertEquals(true, actualMedicalHistoryItemController.medicalHistory.isEditMode);
    }

    @isTest static void testCancelChangesUndoneAndEditModeIsChanged() {
        Medical_History_MVN__c expectedMedicalHistory = [SELECT Id, Program_Member_MVN__c, RecordTypeId, RecordType.DeveloperName FROM Medical_History_MVN__c LIMIT 1];
        MedicalHistoryItemMVN medicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);
        MedicalHistoryItemControllerMVN actualMedicalHistoryItemController = new MedicalHistoryItemControllerMVN();
        actualMedicalHistoryItemController.medicalHistory = medicalHistory;
        Id expectedRecordTypeId = actualMedicalHistoryItemController.medicalHistory.item.RecordTypeId;
        actualMedicalHistoryItemController.medicalHistory.item.RecordTypeId = actualMedicalHistoryItemController.medicalHistory.item.Id;
        actualMedicalHistoryItemController.medicalHistory.changeEditMode();
        System.assertEquals(true, actualMedicalHistoryItemController.medicalHistory.isEditMode);
        System.assertEquals(actualMedicalHistoryItemController.medicalHistory.item.Id, actualMedicalHistoryItemController.medicalHistory.item.RecordTypeId);

        Test.startTest();
            actualMedicalHistoryItemController.cancel();
        Test.stopTest();

        System.assertEquals(false, actualMedicalHistoryItemController.medicalHistory.isEditMode);
        System.assertEquals(expectedRecordTypeId, actualMedicalHistoryItemController.medicalHistory.item.RecordTypeId);
    }

    @isTest static void testFailureToSave() {
        Program_Member_MVN__c progMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        RecordType recordType = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'Medical_History_MVN__c' AND DeveloperName = 'Allergies_MVN'];
        Medical_History_MVN__c expectedMedicalHistory = new Medical_History_MVN__c(
            RecordType = recordType,
            RecordTypeId = recordType.Id,
            Program_Member_MVN__c = progMember.Id
        );
        MedicalHistoryItemMVN medicalHistory = new MedicalHistoryItemMVN(expectedMedicalHistory);
        MedicalHistoryItemControllerMVN actualMedicalHistoryItemController = new MedicalHistoryItemControllerMVN();
        actualMedicalHistoryItemController.medicalHistory = medicalHistory;
        System.assertEquals(null, actualMedicalHistoryItemController.medicalHistory.item.Id);

        Test.setReadOnlyApplicationMode(true);

        Test.startTest();
            actualMedicalHistoryItemController.save();
        Test.stopTest();

        Test.setReadOnlyApplicationMode(false);

        System.assertEquals(1, ApexPages.getMessages().size());
    }
}