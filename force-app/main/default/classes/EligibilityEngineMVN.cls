/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description This is the eligibility engine. It can run all rules given for one application
 */
public with sharing class EligibilityEngineMVN {
    /**
     * Get values for the result field of the engine run and engine results records.
     * Retrieved from custom metadata setting.
     * For RULESUCCESS use the default value of the Result_MVN__c field (PASSED)
     */
    public static String RULEDENIED = EligibilityEngineSettingMVN.deniedSettings != null ?
                    EligibilityEngineSettingMVN.deniedSettings.Engine_Result_Value_MVN__c : null;
    public static String RULEMISSINGINFO = EligibilityEngineSettingMVN.missingInfoSettings != null ?
                    EligibilityEngineSettingMVN.missingInfoSettings.Engine_Result_Value_MVN__c : null;
    public static String RULEINVALID = EligibilityEngineSettingMVN.invalidRuleSettings != null ?
                    EligibilityEngineSettingMVN.invalidRuleSettings.Engine_Result_Value_MVN__c : null;
    public static String RULESUCCESS = (String) Eligibility_Engine_Result_MVN__c.sObjectType
                                                                            .newSObject(null, true)
                                                                            .get('Result_MVN__c');
    /**
     * Data to pass to rules
     */
    private Application_MVN__c application;
    private Map<String, Object> requestedData;

    /**
     * all eligibility engine rules for this program member's program
     */
    private Map<Id, Eligibility_Engine_Rule_MVN__c> allRules;

    /**
     * the eligibility engine rules for this run of the Eligibility Engine
     */
    private List<Eligibility_Engine_Rule_MVN__c> rules;

    /**
     * construct Engine for Application running all top level (parent) rules
     * @param  Id   application Id
     */
    public EligibilityEngineMVN(Id applicationId) {
        Id applicationProgramId = [
            SELECT
                Program_Member_MVN__r.Program_MVN__c
            FROM
                Application_MVN__c
            WHERE
                Id = :applicationId
        ].Program_Member_MVN__r.Program_MVN__c;

        this.allRules = getRulesForProgram( applicationProgramId );

        this.rules = new List<Eligibility_Engine_Rule_MVN__c>();
        for (Eligibility_Engine_Rule_MVN__c rule : allRules.values()) {
            if (rule.Parent_Rule_MVN__c == null) {
                rules.add(rule);
            }
        }

        // get full application details
        this.application = this.getApplication(applicationId);
        this.requestedData = getAdditionalData(application.Id);
        this.requestedData.put('application', application);
    }

    /**
     * construct Engine for Application
     * @param  Map<String, Object> requestedData        data requested by all rules for application
     * @param  List<Eligibility_Engine_Rule_MVN__c>     rules to run
     */
    public EligibilityEngineMVN(Map<String, Object> requestedData,
                                List<Eligibility_Engine_Rule_MVN__c> rules) {
        this.requestedData = requestedData;
        this.application = (Application_MVN__c) requestedData.get('application');

        // get full rull details from the rule cache
        this.rules = new List<Eligibility_Engine_Rule_MVN__c>();
        for (Eligibility_Engine_Rule_MVN__c rule : rules) {
            this.rules.add(
                getRulesForProgram(this.application.Program_Member_MVN__r.Program_MVN__c)
                    .get(rule.Id)
            );
        }
    }

    /**
     * run all given rules for the application
     */
    public List<Eligibility_Engine_Result_MVN__c> run() {
        List<Eligibility_Engine_Result_MVN__c> results = new List<Eligibility_Engine_Result_MVN__c>();
        for(Eligibility_Engine_Rule_MVN__c thisRule : this.rules) {
            Type classType = Type.forName(ruleHandlerByType.get(thisRule.Type_MVN__c));
            if(classType != null) {
                EligibilityEngineRuleIntfMVN ruleHandler
                    = (EligibilityEngineRuleIntfMVN) classType.newInstance();

                List<Eligibility_Engine_Result_MVN__c> thisResultList = ruleHandler.run(
                    thisRule,
                    this.requestedData
                );
                results.addAll(thisResultList);
            }
        }
        return results;
    }

    /**
     * Create Eligibility Engine Run record and link the results to it
     * @param  results eligibility engine results
     * @param  appId   active application id for this program member
     * @param  manualOverride
     */
    private static void processAllResults(List<Eligibility_Engine_Result_MVN__c> results, Id appId, Boolean manualOverride) {

        deactivateLastRunFlagForPreviousResults(appId);

        Eligibility_Engine_Run_MVN__c engineRun = new Eligibility_Engine_Run_MVN__c(
            Application_MVN__c = appId,
            Run_Datetime_MVN__c = Datetime.now()
        );
        for (Eligibility_Engine_Result_MVN__c result : results) {
            if (result.Result_MVN__c == RULEDENIED) {
                engineRun.Result_MVN__c = result.Result_MVN__c;
                break;
            } else if (result.Result_MVN__c == RULEMISSINGINFO) {
                engineRun.Result_MVN__c = result.Result_MVN__c;
            } else if (result.Result_MVN__c == null && result.Result_MVN__c == RULESUCCESS) {
                engineRun.Result_MVN__c = result.Result_MVN__c;
            }
        }
        engineRun.Last_Run_MVN__c = true;
        engineRun.Manual_Override_MVN__c  = manualOverride;
        insert engineRun;
        for (Eligibility_Engine_Result_MVN__c result : results) {
            result.Eligibility_Engine_Run_MVN__c = engineRun.Id;
            result.Last_Run_MVN__c = true;
        }
    }

    /**
     * Create Eligibility Engine Run record and link the results to it
     * @param  results eligibility engine results
     * @param  appId   active application id for this program member
     */
    public static void processResults(List<Eligibility_Engine_Result_MVN__c> results, Id appId) {
        processAllResults(results, appId, false);
    }

    /**
     * Create Eligibility Engine Run record and link the results to it
     * @param  results eligibility engine results
     * @param  appId   active application id for this program member
     */
    public static void processManualOverrideResults(List<Eligibility_Engine_Result_MVN__c> results, Id appId) {
        processAllResults(results, appId, true);
    }

    /**
     * Deactivate previous last run flag when a new run and result is created
     * @param  appId Application Id for the engine run
     */
    private static void deactivateLastRunFlagForPreviousResults(Id appId) {
        Map<Id, Eligibility_Engine_Run_MVN__c> pastRuns = new Map<Id, Eligibility_Engine_Run_MVN__c>([
            SELECT
                Id, Last_Run_MVN__c
            FROM
                Eligibility_Engine_Run_MVN__c
            WHERE
                Application_MVN__c = :appId
            AND
                Last_Run_MVN__c = true
        ]);
        List<Eligibility_Engine_Result_MVN__c> pastResults = [
            SELECT
                Id, Last_Run_MVN__c
            FROM
                Eligibility_Engine_Result_MVN__c
            WHERE
                Eligibility_Engine_Run_MVN__c IN :pastRuns.keySet()
            AND
                Last_Run_MVN__c = true
        ];
        for (Eligibility_Engine_Result_MVN__c res: pastResults) {
            res.Last_Run_MVN__c = false;
        }
        for (Eligibility_Engine_Run_MVN__c res: pastRuns.values()) {
            res.Last_Run_MVN__c = false;
        }
        update pastResults;
        update pastRuns.values();
    }

    /**
     * Get application with all fields defined in given rules
     * @param   Id                       application Id
     * @return  Application_MVN__c       application record with program Id
     */
    private Application_MVN__c getApplication(Id applicationId) {
        Map<String, Set<String>> fieldsToSelectMap = this.getFields();
        String query =
            'SELECT Id ';

        if (!fieldsToSelectMap.isEmpty()) {
            for (String objectName : fieldsToSelectMap.keySet()) {
                if (objectName == 'Application_MVN__c') {
                    query += ', ' + String.join(new List<String>(fieldsToSelectMap.get(objectName)), ',');
                } else {
                    query +=
                        ', (SELECT ' +
                            + String.join(new List<String>(fieldsToSelectMap.get(objectName)), ',') +
                        ' FROM ' +
                            objectName + ')';
                }
            }
        }
        query +=
            ' FROM ' +
                ' Application_MVN__c ' +
            ' WHERE ' +
                ' Id = :applicationId ';

        return Database.query(query);
    }

    /**
     * Get fields to query from rules
     * @return Map<String, Set<String>>  Map of fields by object to query against the Application record
     */
    private Map<String, Set<String>> getFields() {
        Map<String, Set<String>> fieldsToSelectMap = new Map<String, Set<String>>{
            'Application_MVN__c' => new Set<String>{
                'Program_Member_MVN__c',
                'Program_Member_MVN__r.Program_MVN__c'}
        };
        for(Eligibility_Engine_Rule_MVN__c thisRule : this.allRules.values()) {

            if(thisRule.Source_Field_API_Name_MVN__c != null) {
                if (!fieldsToSelectMap.containsKey(thisRule.Source_Object_API_Name_MVN__c)) {
                    fieldsToSelectMap.put(thisRule.Source_Object_API_Name_MVN__c, new Set<String>());
                }
                fieldsToSelectMap.get(thisRule.Source_Object_API_Name_MVN__c).add(thisRule.Source_Field_API_Name_MVN__c);
            }
            // Get extra fields to query
            Type classType = Type.forName(ruleHandlerByType.get(thisRule.Type_MVN__c));
            if(classType != null) {
                EligibilityEngineRuleIntfMVN ruleHandler = (EligibilityEngineRuleIntfMVN) classType.newInstance();
                Map<String, Set<String>> extraFields = ruleHandler.getExtraFieldsToQuery();
                for (String key : extraFields.keySet()) {
                    if (!fieldsToSelectMap.containsKey(key)) {
                        fieldsToSelectMap.put(key, new Set<String>());
                    }
                    fieldsToSelectMap.get(key).addAll(extraFields.get(key));
                }
            }
        }
        return fieldsToSelectMap;
    }

    /**
     * map rule handler class by rule type - defines which class is executed for which rule setup
     * static and lazy loaded for caching
     */
    private static Map<String, String> ruleHandlerByType {
        get {
            if (ruleHandlerByType == null) {
                ruleHandlerByType = new Map<String, String>();

                List<Eligibility_Engine_Rule_Type_MVN__mdt> eeRuleTypes = [
                    SELECT
                        Rule_Type_MVN__c,
                        Rule_Handler_MVN__c
                    FROM
                        Eligibility_Engine_Rule_Type_MVN__mdt
                ];

                for(Eligibility_Engine_Rule_Type_MVN__mdt eligibilityEngineRuleType : eeRuleTypes) {
                    ruleHandlerByType.put(
                        eligibilityEngineRuleType.Rule_Type_MVN__c,
                        eligibilityEngineRuleType.Rule_Handler_MVN__c
                    );
                }
            }
            return ruleHandlerByType;
        }
        private set;
    }

    /**
     * for any rule that is defined using an EligibilityEngineQueryConfigMVN class retrieve
     * the necessary data and return it to the caller as a map of data "key" to data
     */
    private Map<String, Object> getAdditionalData(Id applicationId) {
        Map<String, EligibilityEngineQueryConfigMVN> configurationsByKey
            = new Map<String, EligibilityEngineQueryConfigMVN>();

        // combine configurations with similar keys
        for(Eligibility_Engine_Rule_MVN__c thisRule : this.rules) {
            Type classType = Type.forName(ruleHandlerByType.get(thisRule.Type_MVN__c));
            if(classType != null) {
                EligibilityEngineRuleIntfMVN ruleHandler
                    = (EligibilityEngineRuleIntfMVN) classType.newInstance();

                EligibilityEngineQueryConfigMVN config = ruleHandler.getAdditionalDataConfig();

                if (config.isDefined) {
                    if (!configurationsByKey.containsKey(config.getKey())) {
                        configurationsByKey.put(config.getKey(), config);
                    } else {
                        configurationsByKey.get(config.getKey()).addChildConfiguration(
                            config.childRelationshipToFields
                        );
                    }
                }
            }
        }

        Map<String, Object> additionalData = new Map<String, Object>();

        if (!configurationsByKey.isEmpty()) {
            for (String key : configurationsByKey.keySet()) {
                additionalData.put(key, configurationsByKey.get(key).getData(applicationId));
            }
        }

        return additionalData;
    }

    /**
     * get all eligibility engine rules for a program - used outside of the engine to ensure all
     * fields are being queried correclty
     * @param   Id                                      program Id
     * @return  List<Eligibility_Engine_Rule_MVN__c>    eligibility engine rules
     */
    public static Map<Id, Eligibility_Engine_Rule_MVN__c> getRulesForProgram(Id programId) {
        if (rulesByProgram.containsKey(programId)) {
            return rulesByProgram.get(programId);
        }

        Map<Id, Eligibility_Engine_Rule_MVN__c> rulesForProgram = new Map<Id, Eligibility_Engine_Rule_MVN__c>([
            SELECT
                Id,
                Program_MVN__c,
                Type_MVN__c,
                Result_MVN__c,
                Result_Message_MVN__c,
                Source_Field_API_Name_MVN__c,
                Source_Object_API_Name_MVN__c,
                Name,
                Comparison_MVN__c,
                Value_MVN__c,
                Cannot_be_overriden_MVN__c,
                Child_Evaluation_Type_MVN__c,
                Return_Multiple_Results_MVN__c,
                Parent_Rule_MVN__c,
                (SELECT Id FROM Child_Rules_MVN__r WHERE Active_MVN__c = true),
                (
                    SELECT
                        Id,
                        Product_Family_MVN__c,
                        Poverty_Guideline_Increment_MVN__c
                    FROM
                        Product_Eligibility_Rules_MVN__r
                )
            FROM
                Eligibility_Engine_Rule_MVN__c
            WHERE
                Program_MVN__c = :programId
            AND
                Active_MVN__c = true
        ]);

        rulesByProgram.put(programId, rulesForProgram);
        return rulesForProgram;
    }

    /*
    * cache of rules by program id
    */
    private static Map<Id, Map<Id, Eligibility_Engine_Rule_MVN__c>> rulesByProgram {
        get {
            if (rulesByProgram == null) {
                rulesByProgram = new Map<Id, Map<Id, Eligibility_Engine_Rule_MVN__c>>();
            }
            return rulesByProgram;
        }
        set;
    }
}