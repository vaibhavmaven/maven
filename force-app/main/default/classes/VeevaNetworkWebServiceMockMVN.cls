/*
 * VeevaNetworkWebServiceMockMVN
 * @created by: Pablo Roldan
 * @created on: Sept 27, 2017
 * @edited by: Pablo Roldan
 * @edited date: November 2017
 *
 * @description: This class tests the Veeva Network Connection Utility. Creates the Mock web responses.
 *
 * @edit description: Adapted to PS.
 */

@isTest
public class VeevaNetworkWebServiceMockMVN {

    /******* Vault Utility Mocks *******/
    public class NetworkUtilityMockInactiveService implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody = '{"responseStatus":"FAILURE","errors":[{"type":"INACTIVE_USER","message":"User is inactive or not found."}],"errorType" : "AUTHENTICATION_FAILED"}';
            return buildResponse(responseBody);
        }
    }

    public class NetworkUtilityMockUserLockedOut implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody = '{"responseStatus":"FAILURE","errors":[{"type":"USER_LOCKED_OUT","message":"User is inactive or not found."}],"errorType" : "AUTHENTICATION_FAILED"}';
            return buildResponse(responseBody);
        }
    }

    public class NetworkUtilityMockInvalidSession implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody = '{"responseStatus":"FAILURE","errors":[{"type":"INVALID_SESSION_ID","message":"User is inactive or not found."}],"errorType" : "AUTHENTICATION_FAILED"}';
            return buildResponse(responseBody);
        }
    }

    public class NetworkUtilityMockInsufficientAccess implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody = '{"responseStatus":"FAILURE","errors":[{"type":"INSUFFICIENT_ACCESS","message":"User is inactive or not found."}],"errorType" : "AUTHENTICATION_FAILED"}';
            return buildResponse(responseBody);
        }
    }

    public class NetworkUtilityMockOther implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody = '{"responseStatus":"FAILURE","errors":[{"type":"Other","message":"User is inactive or not found."}],"errorType" : "AUTHENTICATION_FAILED"}';
            return buildResponse(responseBody);
        }
    }

    /******* Vault Document Token Mocks *******/

    public class NetworkTokenUtilityMockSuccess implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody = '';
            if(req.getEndpoint().contains('/auth')) {
                responseBody =
                '{"responseStatus":"SUCCESS","sessionId":"newSession12345"}';
            } else {
                Map<String, Object> reqDeserialize = (Map<String, Object>) JSON.deserializeUntyped(req.getBody());
                String vidKey = (String) reqDeserialize.get('vid_key');
                vidKey = vidKey.substringAfterLast(':');

                Map<String, Object> entity = (Map<String, Object>) reqDeserialize.get('entity');
                if(entity.containsKey('addresses__v')) {
                    List<Object> addresses = (List<Object>) entity.get('addresses__v');

                    if(!addresses.isEmpty()) {
                        vidKey += 'Address';
                    }
                }

                responseBody =
                '{\n' +
                    ' "responseStatus": "SUCCESS",\n' +
                    ' "change_request_id": "newChangeRequest'+ vidKey +'"' +
                '}';
            }

            return buildResponse(responseBody);
        }
    }

    public class NetworkTokenUtilityMockCheckSuccess implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody = '';
            if(req.getEndpoint().contains('/auth')) {
                responseBody =
                '{"responseStatus":"SUCCESS","sessionId":"newSession12345"}';
            } else {
                String changeRequestId = req.getEndpoint().substringAfterLast('/');

                responseBody =
                '{\n' +
                    '"responseStatus": "SUCCESS",\n' +
                    '"change_requests": [' +
                        '{' +
                            '"metadata": {' +
                                '"creator": "creator@network.mavens",' +
                                '"note": "Mavens Network Test.",' +
                                '"source": "Medical Information Cloud",' +
                                '"system": "MIC"' +
                            '},\n' +
                            '"entity": {' +
                                '"hcp_type__v": {' +
                                    '"change_requested": "P",' +
                                    '"final_value": "P",' +
                                    '"result": "CHANGE_ACCEPTED"' +
                                '},\n' +
                                '"last_name__v": {' +
                                    '"change_requested": "Baggings",' +
                                    '"final_value": "Baggings",' +
                                    '"result": "CHANGE_ACCEPTED"' +
                                '},\n' +
                                    '"hcp_status__v": {' +
                                    '"change_requested": "A",' +
                                    '"final_value": "A",' +
                                    '"result": "CHANGE_ACCEPTED"' +
                                '},\n' +
                                    '"first_name__v": {' +
                                    '"change_requested": "FN",' +
                                    '"final_value": "FN",' +
                                    '"result": "CHANGE_ACCEPTED"' +
                                '},\n' +
                                    '"corporate_name__v": {' +
                                    '"change_requested": "CorpName",' +
                                    '"final_value": "CorpName",' +
                                    '"result": "CHANGE_ACCEPTED"' +
                                '},\n' +
                                    '"hco_status__v": {' +
                                    '"change_requested": "A",' +
                                    '"final_value": "A",' +
                                    '"result": "CHANGE_ACCEPTED"' +
                                '},\n' +
                                '"addresses__v": [' +
                                    '{' +
                                        '"vid_key": null,' +
                                        '"vid__v": "a'+ changeRequestId +'",' +
                                        '"object_id": "a'+ changeRequestId +'",' +
                                        '"administrative_area__v": {' +
                                            '"change_requested": "US-NY",' +
                                            '"final_value": "US-NY",' +
                                            '"result": "CHANGE_ACCEPTED"' +
                                        '},\n' +
                                        '"postal_code__v": {' +
                                            '"change_requested": "11235",' +
                                            '"final_value": "11235",' +
                                            '"result": "CHANGE_ACCEPTED"' +
                                        '},\n' +
                                        '"address_line_1__v": {' +
                                            '"change_requested": "3049 Brighton 1st St",' +
                                            '"final_value": "3049 Brighton 1st St",' +
                                            '"result": "CHANGE_ACCEPTED"' +
                                        '},\n' +
                                            '"address_status__v": {' +
                                            '"change_requested": "A",' +
                                            '"final_value": "A",' +
                                            '"result": "CHANGE_ACCEPTED"' +
                                        '},\n' +
                                        '"country__v": {' +
                                            '"change_requested": "US",' +
                                            '"final_value": "US",' +
                                            '"result": "CHANGE_ACCEPTED"' +
                                        '}' +
                                    '}' +
                                ']' +
                            '},\n' +
                            '"task_type": "ADD_REQUEST",' +
                            '"original_task_type": "ADD_REQUEST",' +
                            '"taskCountry": "US",' +
                            '"change_request_id": "' + changeRequestId + '",' +
                            '"status__v": "CHANGE_PROCESSED",' +
                            '"result__v": "CHANGE_ACCEPTED",' +
                            '"entity_type": "HCP",' +
                            '"vid_key": null,' +
                            '"vid__v": "' + changeRequestId + '",' +
                            '"created_date": "2014-12-17T15:57:50.000-06:00",' +
                            '"completed_date": "2014-12-17T16:03:26.000-06:00"' +
                        '}' +
                    ']' +
                '}';
            }

            return buildResponse(responseBody);
        }
    }

    public class NetworkTokenUtilityAuthenticationSuccess implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody =
                '{"responseStatus":"SUCCESS","sessionId":"newSession12345"}';

            return buildResponse(responseBody);
        }
    }

    public class NetworkTokenUtilityInvalidSession implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody =
                '{"responseStatus":"FAILURE","errors":' +
                    '[{' +
                        '"type":"INVALID_SESSION_ID",' +
                        '"message":"Authentication failed for session id: session12345"' +
                    '}]' +
                '}';

            return buildResponse(responseBody);
        }
    }

    public class NetworkTokenUtilityMockSearchExternalIdSuccess implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody = '';
            if(req.getEndpoint().contains('/auth')) {
                responseBody =
                '{"responseStatus":"SUCCESS","sessionId":"newSession12345"}';
            } else {
                Map<String, Object> reqDeserialize = (Map<String, Object>) JSON.deserializeUntyped(req.getBody());
                List<Object> entities = (List<Object>) reqDeserialize.get('entities');

                Set<String> vidKeys = new Set<String>();
                for(Object entityObj : entities) {
                    vidKeys.add((String) ((Map<String, Object>) entityObj).get('vid_key'));
                }

                responseBody =
                '{\n' +
                    '"responseStatus": "SUCCESS",\n' +
                    '"entities": [\n';

                String entitiesBody = '';
                String supplementalResultsBody = '';

                for(String vidKey : vidKeys) {
                    entitiesBody += getSearchEntity(vidKey, null, null);
                    supplementalResultsBody += getSupplementalResults(vidKey);
                }

                if(String.IsNotBlank(entitiesBody)) {
                    responseBody += entitiesBody.substringBeforeLast(',');
                }

                supplementalResultsBody = String.IsNotBlank(supplementalResultsBody) ?
                        supplementalResultsBody.substringBeforeLast(',') :
                        '';

                responseBody += '],\n' +
                    '"totalCount": 10011,\n' +
                    '"offset": 0,\n' +
                    '"limit": 2,\n' +
                    '"supplementalResults": [\n' +
                    supplementalResultsBody +
                    + '\n]\n' +
                '}';
            }

            return buildResponse(responseBody);
        }
    }

    public class NetworkTokenUtilityMockSearchSuccess implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String responseBody = '';
            if(req.getEndpoint().contains('/auth')) {
                responseBody =
                '{"responseStatus":"SUCCESS","sessionId":"newSession12345"}';
            } else {
                String fieldQueriesString = req.getEndpoint().substringAfterLast('&types=HCP');
                List<String> fieldQueries = fieldQueriesString.split('&filters=');
                responseBody =
                '{\n' +
                    '"responseStatus": "SUCCESS",\n' +
                    '"entities": [\n';
                for(String fieldQuery : fieldQueries) {
                    if(String.IsNotBlank(fieldQuery)) {
                        List<String> fieldQueryNameAndValue = fieldQuery.split(':');

                        if(fieldQueryNameAndValue != null && fieldQueryNameAndValue.size() == 2) {
                            String entitiesBody = '';
                            if(fieldQueryNameAndValue[0] == 'last_name__v') {
                                entitiesBody += getSearchEntity('0', fieldQueryNameAndValue[1], null);
                            } else if(fieldQueryNameAndValue[0] == 'address.phone_1__v') {
                                for(Integer phoneIndex = 1; phoneIndex <= 10; phoneIndex++) {
                                    entitiesBody += getSearchEntity(String.valueOf(phoneIndex), 'LastName' + phoneIndex, fieldQueryNameAndValue[1]);
                                }
                            }

                            if(String.IsNotBlank(entitiesBody)) {
                                responseBody += entitiesBody.substringBeforeLast(',');
                            }
                        }
                    }
                }
                responseBody += '],\n' +
                    '"totalCount": 10011,\n' +
                    '"offset": 0,\n' +
                    '"limit": 2\n' +
                '}';
            }

            return buildResponse(responseBody);
        }
    }

    private static String getSearchEntity(String entityIndex, String lastName, String phone) {
        String entityId = '24305683487575347' + entityIndex;

        if(String.IsBlank(lastName) && String.IsBlank(phone)) {
            entityId = entityIndex;
            lastName = 'LN' + entityIndex;
        }

        return '{\n' +
                    '"entityId": "'+ entityId +'",\n' +
                    '"entityType": "HCP",\n' +
                    '"metaData": {\n' +
                        '"highlightTerms": [],\n' +
                        '"resultIsFromMaster": true,\n' +
                        '"vid__v": "'+ entityId +'",\n' +
                        '"relevance": 121.30947,\n' +
                        '"highlights": {}\n' +
                    '},\n' +
                    '"entity": {\n' +
                        '"record_state__v": "VALID",\n' +
                        '"is_externally_mastered__v": false,\n' +
                        '"hcp_type__v": "N",\n' +
                        '"formatted_name__v": "SMITHA JOHN",\n' +
                        '"last_name__v": "'+ lastName +'",\n' +
                        '"hcp_status__v": "A",\n' +
                        '"medical_degree_1__v": "RN",\n' +
                        '"specialty_2__v": "PM",\n' +
                        '"addresses__v": [\n' +
                        '{\n' +
                            '"vid__v": "add'+ entityId +'",\n' +
                            '"record_state__v": "VALID",\n' +
                            '"delivery_address_1__v": "1300 S Old Orchard Ln",\n' +
                            '"postal_code__v": "75067-5067",\n' +
                            '"administrative_area__v": "US-TX",\n' +
                            '"delivery_address__v": "1300 S Old Orchard Ln",\n' +
                            '"address_type__v": "P",\n' +
                            '"is_veeva_master__v": true,\n' +
                            '"entity_vid__v": "243056834875753472",\n' +
                            '"status_update_time__v": "2013-10-28T03:57:01.000-07:00",\n' +
                            '"address_line_2__v": "Lewisville TX 75067-5067",\n' +
                            '"created_date__v": "2013-10-28T03:57:01.000-07:00",\n' +
                            '"vid__v": "243293877115552801",\n' +
                            '"address_verification_status__v": "V",\n' +
                            '"postal_code_primary__v": "75067",\n' +
                            '"entity_type__v": "HCP",\n' +
                            '"phone_1__v": '+ phone +',\n' +
                            '"postal_code_secondary__v": "5067",\n' +
                            '"address_line_1__v": "1300 S Old Orchard Ln",\n' +
                            '"country__v": "US",\n' +
                            '"modified_date__v": "2013-10-28T03:57:01.000-07:00",\n' +
                            '"address_ordinal__v": 2,\n' +
                            '"locality__v": "Lewisville",\n' +
                            '"address_status__v": "A"\n' +
                        '}\n' +
                        '],\n' +
                        '"is_veeva_master__v": true,\n' +
                        '"parent_hcos__v": [\n' +
                            '{\n' +
                                '"parent_hco_vid__v": "'+ entityId +'1",\n' +
                                '"record_owner_type__v": "VOD",\n' +
                                '"relationship_type__v": "10",\n' +
                                '"status_update_time__v": "2015-05-26T14:24:12.000-07:00",\n' +
                                '"record_delta_id__v": "660356224834965504",\n' +
                                '"entity_type__v": "HCP",\n' +
                                '"hierarchy_type__v": "HCP_HCO",\n' +
                                '"record_owner_name__v": "OpenData",\n' +
                                '"modified_date__v": "2015-05-26T14:25:12.000-07:00",\n' +
                                '"parent_hco_corp_name__v": "Allergy And Asthma Medical Group",\n' +
                                '"record_state__v": "VALID",\n' +
                                '"parent_hco_status__v": "A",\n' +
                                '"is_primary_relationship__v": "N",\n' +
                                '"vid__v": "'+ entityId +'1",\n' +
                                '"entity_vid__v": "'+ entityId +'",\n' +
                                '"created_date__v": "2015-05-26T14:24:12.000-07:00",\n' +
                                '"is_veeva_master__v": true,\n' +
                                '"custom_keys__v": [\n' +
                                    '{\n' +
                                        '"custom_key_value__v": "'+ entityId +'1",\n' +
                                        '"custom_key_vid_key__v": "MASTER__v:PARENTHCO:'+ entityId +'1",\n' +
                                        '"custom_key_source_type__v": "MASTER__v",\n' +
                                        '"custom_key_entity_type__v": "RELATION",\n' +
                                        '"custom_key_status__v": "A",\n' +
                                        '"status_update_time__v": "2015-05-26T14:24:12.000-07:00",\n' +
                                        '"custom_key_entity_id__v": "'+ entityId +'",\n' +
                                        '"custom_key_item_type__v": "PARENTHCO",\n' +
                                        '"modified_date__v": "2015-05-26T14:24:12.000-07:00",\n' +
                                        '"vid__v": "'+ entityId +'1",\n' +
                                        '"created_date__v": "2015-05-26T14:24:12.000-07:00"\n' +
                                    '}\n' +
                                ']\n' +
                            '}\n' +
                        '],\n' +
                        '"medical_degree_2__v": "PVN",\n' +
                        '"specialty_1__v": "Internal Medicine",\n' +
                        '"pdrp_optout__v": "N",\n' +
                        '"status_update_time__v": "2013-10-28T03:57:01.000-07:00",\n' +
                        '"specialty_1_rank__v": 1,\n' +
                        '"licenses__v": [],\n' +
                        '"phone_1__v": '+ phone +',\n' +
                        '"primary_country__v": "US",\n' +
                        '"created_date__v": "2013-10-28T03:57:01.000-07:00",\n' +
                        '"fellow__v": "N",\n' +
                        '"middle_name__v": "Fred",\n' +
                        '"vid__v": "'+ entityId +'",\n' +
                        '"specialty_2_rank__v": 2,\n' +
                        '"modified_date__v": "2013-10-28T03:57:01.000-07:00",\n' +
                        '"gender__v": "M",\n' +
                        '"master_vid__v": "'+ entityId +'",\n' +
                        '"grad_training__v": "N",\n' +
                        '"first_name__v": "Jeffrey",\n' +
                        '"ama_do_not_contact__v": "N"\n' +
                    '}\n' +
                '},\n';
    }

    private static String getSupplementalResults(String entityIndex) {
        String entityId = entityIndex;

        return '{\n' +
                    '"hco_type__v": "4:5",\n' +
                    '"340B_eligible__v": "N",\n' +
                    '"master_vid__v": "' + entityIndex + '1",\n' +
                    '"record_owner_type__v": "VOD",\n' +
                    '"lab_services__v": "U",\n' +
                    '"hco_status__v": "A",\n' +
                    '"accept_medicare__v": "U",\n' +
                    '"record_owner_name__v": "OpenData",\n' +
                    '"kaiser__v": "N",\n' +
                    '"primary_country__v": "US",\n' +
                    '"created_date__v": "2015-05-26T14:22:42.000-07:00",\n' +
                    '"training_facility__v": "N",\n' +
                    '"is_veeva_master__v": true,\n' +
                    '"major_class_of_trade__v": "4",\n' +
                    '"ama_do_not_contact__v": "N",\n' +
                    '"corporate_name__v": "My Corp Test ' + entityIndex + '1",\n' +
                    '"status_update_time__v": "2015-05-26T14:22:42.000-07:00",\n' +
                    '"ams_id__v": "' + entityIndex + '111",\n' +
                    '"modified_date__v": "2015-05-26T14:22:42.000-07:00",\n' +
                    '"accept_medicaid__v": "U",\n' +
                    '"record_state__v": "VALID",\n' +
                    '"record_version__v": 0,\n' +
                    '"vid__v": "' + entityIndex + '1",\n' +
                    '"is_externally_mastered__v": false,\n' +
                    '"xray_services__v": "U",\n' +
                    '"custom_keys__v": [\n' +
                        '{\n' +
                            '"custom_key_entity_id__v": "' + entityIndex + '1",\n' +
                            '"custom_key_item_type__v": "HCO",\n' +
                            '"custom_key_value__v": "' + entityIndex + '1",\n' +
                            '"modified_date__v": "2015-05-26T14:22:42.000-07:00",\n' +
                            '"vid__v": "' + entityIndex + '123",\n' +
                            '"custom_key_vid_key__v": "MASTER__v:HCO:' + entityIndex + '1",\n' +
                            '"custom_key_source_type__v": "MASTER__v",\n' +
                            '"created_date__v": "2015-05-26T14:22:42.000-07:00",\n' +
                            '"custom_key_entity_type__v": "HCO",\n' +
                            '"custom_key_status__v": "A",\n' +
                            '"status_update_time__v": "2015-05-26T14:22:42.000-07:00"\n' +
                        '}\n' +
                    '],\n' +
                    '"licenses__v": [],\n' +
                    '"addresses__v": [\n' +
                        '{\n' +
                            '"dpv_confirmed_indicator__v": "X",\n' +
                            '"address_line_3__v": "Test Street IL 11111-1111",\n' +
                            '"postal_code_primary__v": "11111",\n' +
                            '"address_line_1__v": "Test Street",\n' +
                            '"record_owner_type__v": "VOD",\n' +
                            '"premise__v": "130",\n' +
                            '"cbsa__v": "11111",\n' +
                            '"record_owner_name__v": "OpenData",\n' +
                            '"thoroughfare_trailing_type__v": "Via",\n' +
                            '"locality__v": "Chicago",\n' +
                            '"delivery_address__v": "Test Street",\n' +
                            '"country__v": "US",\n' +
                            '"sub_building_number__v": "1",\n' +
                            '"created_date__v": "2015-05-26T14:22:42.000-07:00",\n' +
                            '"premise_number__v": "130",\n' +
                            '"phone_1__v": "1111111111",\n' +
                            '"is_veeva_master__v": true,\n' +
                            '"sub_building_type__v": "Ste",\n' +
                            '"ISO_3166_3__v": "USA",\n' +
                            '"ISO_3166_n__v": "111",\n' +
                            '"sub_building__v": "Str 1",\n' +
                            '"thoroughfare__v": "Corp Test",\n' +
                            '"status_update_time__v": "2015-05-26T14:22:42.000-07:00",\n' +
                            '"address_type__v": "P",\n' +
                            '"delivery_address_1__v": "Test Street",\n' +
                            '"sub_administrative_area__v": "Test Area",\n' +
                            '"entity_type__v": "HCO",\n' +
                            '"address_verification_status__v": "V",\n' +
                            '"address_status__v": "A",\n' +
                            '"modified_date__v": "2015-05-26T14:22:42.000-07:00",\n' +
                            '"record_state__v": "VALID",\n' +
                            '"vid__v": "' + entityIndex + '2",\n' +
                            '"entity_vid__v": "' + entityIndex + '1",\n' +
                            '"postal_code__v": "11111-1111",\n' +
                            '"is_externally_mastered__v": false,\n' +
                            '"administrative_area__v": "US-IL",\n' +
                            '"formatted_address__v": "Test Street Chicago IL 11111-1111",\n' +
                            '"address_ordinal__v": 1,\n' +
                            '"thoroughfare_name__v": "Corp Test",\n' +
                            '"postal_code_secondary__v": "1111",\n' +
                            '"custom_keys__v": [\n' +
                                '{\n' +
                                    '"custom_key_entity_id__v": "' + entityIndex + '2",\n' +
                                    '"custom_key_item_type__v": "ADDRESS",\n' +
                                    '"custom_key_value__v": "' + entityIndex + '2",\n' +
                                    '"modified_date__v": "2015-05-26T14:22:42.000-07:00",\n' +
                                    '"vid__v": "' + entityIndex + '22",\n' +
                                    '"custom_key_vid_key__v": "MASTER__v:ADDRESS:' + entityIndex + '2",\n' +
                                    '"custom_key_source_type__v": "MASTER__v",\n' +
                                    '"created_date__v": "2015-05-26T14:22:42.000-07:00",\n' +
                                    '"custom_key_entity_type__v": "ADDRESS",\n' +
                                    '"custom_key_status__v": "A",\n' +
                                    '"status_update_time__v": "2015-05-26T14:22:42.000-07:00"\n' +
                                '}\n' +
                            ']\n' +
                        '}\n' +
                    '],\n' +
                    '"parent_hcos__v": []\n' +
                '},\n';
    }

    /******* Helper Methods *******/

    /**
     * Obtain a successful JSON response.
     * @param  jsonBody Response content to include with the response.
     * @return HTTP 200 response including the JSON body content provided.
     */
    private static HTTPResponse buildResponse(String jsonBody) {
        HTTPResponse response = new HTTPResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setStatusCode(200);
        response.setBody(jsonBody);

        return response;
    }
}