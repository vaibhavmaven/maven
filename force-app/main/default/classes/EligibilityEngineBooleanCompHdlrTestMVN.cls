/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description tests EligibilityEngineBooleanCompHdlrMVN
 */
@isTest private class EligibilityEngineBooleanCompHdlrTestMVN {
    private static User caseManager;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        TestDataFactoryMVN.createPSSettings();

        System.runAs(caseManager) {
            program = TestDataFactoryMVN.createProgram();
            Account member = TestDataFactoryMVN.createMember();
            member.NumberOfEmployees = 10;
            update member;
            programMember = TestDataFactoryMVN.createProgramMember(program, member);
            application = createApplication();
            programMember.Active_Application_MVN__c = application.Id;
            update programMember;
        }
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        return testApplication;
    }

    @isTest private static void runBooleanCompRuleToSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Wrong Test Rule One',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Boolean_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Do_Not_Initialize_MVN__c',
            Value_MVN__c = 'true',
            Active_MVN__c = true
        );
        insert expectedRule;
        List<Eligibility_Engine_Rule_MVN__c> rules = new List<Eligibility_Engine_Rule_MVN__c> {
            expectedRule
        };

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    private static Eligibility_Engine_Rule_MVN__c testRule() {
        return  new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Test Rule for Children',
            Result_MVN__c = EligibilityEngineMVN.RULEDENIED,
            Type_MVN__c = 'Boolean_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Object_API_Name_MVN__c = 'Benefits_Coverage_MVN__r',
            Source_Field_API_Name_MVN__c = 'Medicare_flag_MVN__c',
            Value_MVN__c = 'true',
            Active_MVN__c = true
        );
    }

    @isTest
    static void childRuleShouldTriggerWhenNoChildRecords() {
        Eligibility_Engine_Rule_MVN__c expectedRule = testRule();
        insert expectedRule;

        List<Eligibility_Engine_Rule_MVN__c> rules = new List<Eligibility_Engine_Rule_MVN__c> {
            expectedRule
        };

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
        }
    }

    public static List<Benefits_Coverage_MVN__c> getBAndCs() {
        return new List<Benefits_Coverage_MVN__c> {
            new Benefits_Coverage_MVN__c(
                Program_Member_MVN__c = programMember.Id,
                Application_MVN__c = application.Id,
                Medicare_Part_A_MVN__c = 'Yes' // one true
            ),

            new Benefits_Coverage_MVN__c(
                Program_Member_MVN__c = programMember.Id,
                Application_MVN__c = application.Id,
                Medicare_Part_A_MVN__c = 'No' // one false
            )
        };
    }

    @isTest
    static void ORchildRuleShouldTriggerWhenAnyRecordTriggersRule() {
        Eligibility_Engine_Rule_MVN__c expectedRule = testRule();
        expectedRule.Child_Evaluation_Type_MVN__c = 'OR';
        insert expectedRule;

        insert getBAndCs(); // two b and c records, one with medicare one without

        List<Eligibility_Engine_Rule_MVN__c> rules = new List<Eligibility_Engine_Rule_MVN__c> {
            expectedRule
        };

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    static void ORchildRuleShouldNOTTriggerWhenNoRecordTriggersRule() {
        Eligibility_Engine_Rule_MVN__c expectedRule = testRule();
        expectedRule.Child_Evaluation_Type_MVN__c = 'OR';
        insert expectedRule;

        List<Benefits_Coverage_MVN__c> bAndCs = getBAndCs();
        bAndCs[0].Medicare_Part_A_MVN__c = 'No'; // now both records are false
        insert bAndCs;

        List<Eligibility_Engine_Rule_MVN__c> rules = new List<Eligibility_Engine_Rule_MVN__c> {
            expectedRule
        };

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    static void ANDchildRuleShouldNOTTriggerWhenAnyRecordDoesNotTrigger() {
        Eligibility_Engine_Rule_MVN__c expectedRule = testRule();
        expectedRule.Child_Evaluation_Type_MVN__c = 'AND';
        insert expectedRule;

        insert getBAndCs();

        List<Eligibility_Engine_Rule_MVN__c> rules = new List<Eligibility_Engine_Rule_MVN__c> {
            expectedRule
        };

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    static void ANDchildRuleShouldTriggerWhenAllRecordsTrigger() {
        Eligibility_Engine_Rule_MVN__c expectedRule = testRule();
        expectedRule.Child_Evaluation_Type_MVN__c = 'AND';
        insert expectedRule;

        List<Benefits_Coverage_MVN__c> bAndCs = getBAndCs();
        bAndCs[1].Medicare_Part_A_MVN__c = 'Yes'; // now both records are true
        insert bAndCs;

        List<Eligibility_Engine_Rule_MVN__c> rules = new List<Eligibility_Engine_Rule_MVN__c> {
            expectedRule
        };

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
        }
    }
}