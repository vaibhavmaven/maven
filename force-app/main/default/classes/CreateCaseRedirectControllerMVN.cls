/*
* CreateCaseRedirectControllerMVN
* Created By: Roman Lerman
* Created Date: Jan 1, 2013
* Edited By: Paul Battisson
* Last Edited: May 30, 2014
* Description: This class creates a new interaction and a new request whenever the "New" button is clicked on the Case list view,
*           Update (Jen, Dec 2015): Or from the ProgramMemberUpcomingActivities in the side panel in Program Member. Added the parameters of recordType (rt) and program member (pm)
*/

public with sharing class CreateCaseRedirectControllerMVN {

    public Case newCase { get; set; }

    public String accountId { get; set; }
    public String recordType {get; set;}
    public String programMemberId {get; set;}

    public String physicianId {get; set;}
    public String programId {get; set;}
    public String phone {get; set;}
    public String skillId { get; set; }
    public String programExternalId { get; set; }

    private static final String activityCaseRT = 'activity';
    private static final String interactionCaseRT = 'interaction';

    public CreateCaseRedirectControllerMVN (ApexPages.StandardController controller) {
        Case c = (Case)controller.getRecord();

        accountId = ApexPages.currentPage().getParameters().get('def_account_id');

        recordType = ApexPages.currentPage().getParameters().get('recordType');

        programMemberId = ApexPages.currentPage().getParameters().get('programMember');

        physicianId = ApexPages.currentPage().getParameters().get('physician');

        programId = ApexPages.currentPage().getParameters().get('program');

        phone = ApexPages.currentPage().getParameters().get('ani');

        programExternalId = ApexPages.currentPage().getParameters().get('programid');
    }

    public void getRedirect () {
        newCase = (Case)Case.sObjectType.newSObject(null, true);

        if (recordType == activityCaseRT) {
            newCase.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = :Patient_Service_Settings_MVN__c.getInstance().Case_Activity_Record_Type_MVN__c].Id;
        } else if (recordType == interactionCaseRT) {
            newCase.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = :Patient_Service_Settings_MVN__c.getInstance().Case_Interaction_Record_Type_MVN__c].Id;
        } else {
            newCase.RecordTypeId = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = :Patient_Service_Settings_MVN__c.getInstance().Case_Request_Record_Type_MVN__c].Id;
        }

        if(!String.isBlank(accountId)){
            newCase.AccountId = accountId;
            newCase.ContactId = [select PersonContactId from Account where Id = :accountId].PersonContactId;
        }

        newCase.Program_Member_MVN__c = !String.isBlank(programMemberId) ? programMemberId : null;

        newCase.Referred_By_MVN__c = !String.isBlank(physicianId) ? physicianId : null;

        if(!String.isBlank(programId)) {
            newCase.Program_MVN__c = programId;
        } else if(!String.isBlank(programExternalId)) {
            newCase.Program_MVN__r = new Program_MVN__c(Program_Id_MVN__c = programExternalId);
        }

        try{
            insert newCase;

            newCase = [select Id, ParentId from Case where Id =: newCase.Id];
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
      }
}