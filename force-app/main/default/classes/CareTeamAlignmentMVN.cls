/**
 * @author Mavens
 * @description When a patient is enrolled on their program, they need to be assigned their care team automatically
 *              based on an alignment file.
 * @group ProgramMember
 **/

public with sharing class CareTeamAlignmentMVN implements TriggerHandlerMVN.HandlerInterface {
    
    /**
     * @description caseManager
     */
    @TestVisible
    private String caseManager = Patient_Service_Settings_MVN__c.getInstance().Care_Team_Align_ZTT_Field_for_Owner_MVN__c;
    
    /**
     * @description handle
     */
    public void handle() {
        Map<Id, Account> accountsToProcess = getAccountIdsToProcess();

        // If no accounts have been found, then we can't make any matches - or we are in an update that
        // does not change anything
        if(!accountsToProcess.isEmpty()) {
            accountsToProcess = getAccounts(accountsToProcess);
            Set<String> externalIds = getExternalIds(accountsToProcess);
            Map<String, Zip_to_Team_MVN__c> zipToTeamByExternalId = getZipToTeamByExternalId(externalIds);
            assignProgramMemberToCareTeam(zipToTeamByExternalId, accountsToProcess);
        }

        copyCaseManagerToPMOwner();
    }

    /**
     * @description copyCaseManagerToPMOwner, align owner to the CM
     */
    private void copyCaseManagerToPMOwner() {
        List<Id> caseManagerIds = new List<Id>();
        if(caseManager != null) {
            for(Program_Member_MVN__c programMember : (List<Program_Member_MVN__c>)trigger.new) {
                if(trigger.isInsert ||
                   (trigger.isUpdate && trigger.oldMap.get(programMember.Id).get(caseManager) != programMember.get(caseManager))) {

                    caseManagerIds.add((Id)programMember.get(caseManager));
                }
            }
        }

        if(caseManagerIds.isEmpty()) {
            return;
        }

        Map<Id, User> activeCaseManagersById = new Map<Id, User>([SELECT Id FROM User WHERE IsActive = true AND Id IN :caseManagerIds]);
        for(Program_Member_MVN__c programMember : (List<Program_Member_MVN__c>)trigger.new) {
            // Always align the Owner to the Case Manager
            if(caseManager != null && activeCaseManagersById.containsKey((Id)programMember.get(caseManager))) {
                programMember.OwnerId = (Id)programMember.get(caseManager);
            }
        }
    }

    /**
     * @description build External Id
     */
    private String buildExternalId(Account account, Program_Member_MVN__c programMember) {
        return account.BillingPostalCode + '_' + account.BillingCountry + '_' + programMember.Program_MVN__c;
    }

    /**
     * @description assign ProgramMember to Care Team
     */
    private void assignProgramMemberToCareTeam(Map<String, Zip_to_Team_MVN__c> zipToTeamByExternalId, Map<Id, Account> accountsToProcess) {
        for(Program_Member_MVN__c programMember : (List<Program_Member_MVN__c>)trigger.new) {
            String externalId;

            // set by default. Will be overwritten with a proper manager, when a Zip is found.
            if(trigger.isInsert && caseManager != null) {
                programMember.put(caseManager, UserInfo.getUserId());
            }

            if(programMember.Physician_MVN__c != null) {
                Account account = accountsToProcess.get(programMember.Physician_MVN__c);
                if(account != null) {
                    externalId = buildExternalId(account, programMember);
                }
                if(!copyValuesFromZipToTeamToProgramMember(zipToTeamByExternalId, externalId, programMember)) {
                    // we reset externalId, so that we try to make a matching on Member
                    externalId = null;
                }
            }

            if(programMember.Member_MVN__c != null && externalId == null) {
                Account account = accountsToProcess.get(programMember.Member_MVN__c);
                if(account != null) {
                    externalId = buildExternalId(account, programMember);
                }
                copyValuesFromZipToTeamToProgramMember(zipToTeamByExternalId, externalId, programMember);
            }
        }
    }

    /**
     * @description Zip_to_Team_MVN__c by Unique_Key_MVN__c
     */
    private Map<String, Zip_to_Team_MVN__c> getZipToTeamByExternalId(Set<String> externalIds) {
        List<String> lookupFields = getLookupFields();
        Map<String, Zip_to_Team_MVN__c> zipToTeamByExternalId = new Map<String, Zip_to_Team_MVN__c>();

        for(Zip_to_Team_MVN__c zipToTeam : (List<Zip_to_Team_MVN__c>)database.query(
            'SELECT Unique_Key_MVN__c ' +
            (lookupFields.isEmpty()?'':',') +
            String.join(lookupFields, ',') +
             ' FROM Zip_to_Team_MVN__c' +
            ' WHERE Unique_Key_MVN__c IN :externalIds'
        )) {
            zipToTeamByExternalId.put(zipToTeam.Unique_Key_MVN__c, zipToTeam);
        }

        return zipToTeamByExternalId;
    }

    /**
     * @description get Account External Ids
     */
    private Set<String> getExternalIds(Map<Id, Account> accountsToProcess) {
        Set<String> externalIds = new Set<String>();

        for(Program_Member_MVN__c programMember : (List<Program_Member_MVN__c>)trigger.new) {
            if(programMember.Physician_MVN__c != null) {
                Account account = accountsToProcess.get(programMember.Physician_MVN__c);
                if(account != null) {
                    externalIds.add(buildExternalId(account, programMember));
                }
            }

            if(programMember.Member_MVN__c != null) {
                Account account = accountsToProcess.get(programMember.Member_MVN__c);
                if(account != null) {
                    externalIds.add(buildExternalId(account, programMember));
                }
            }
        }

        return externalIds;
    }

    /**
     * @description get Accounts
     */
    private Map<Id, Account> getAccounts(Map<Id, Account> accountsToProcess) {
        accountsToProcess = new Map<Id, Account>([
            SELECT BillingPostalCode, BillingCountry
            FROM Account
            WHERE Id IN :accountsToProcess.keySet()
        ]);

        return accountsToProcess;
    }

    /**
     * @description get AccountIds to be processed
     */
    private Map<Id, Account> getAccountIdsToProcess() {
        Map<Id, Account> accountsToProcess = new Map<Id, Account>();

        for(Program_Member_MVN__c programMember : (List<Program_Member_MVN__c>) trigger.new) {
            if(trigger.isUpdate) {
                Boolean isPhysicianUnchanged = ((Program_Member_MVN__c) trigger.oldMap.get(programMember.Id)).Physician_MVN__c == programMember.Physician_MVN__c;
                Boolean isProgramUnchanged = ((Program_Member_MVN__c) trigger.oldMap.get(programMember.Id)).Program_MVN__c == programMember.Program_MVN__c;

                if(isPhysicianUnchanged && isProgramUnchanged) {
                    // because neither the physician nor the program have been changed, 
                    // there is nothing to do
                    continue;
                }
            }

            if(programMember.Physician_MVN__c != null){
                accountsToProcess.put(programMember.Physician_MVN__c, null);
            }

            if(programMember.Member_MVN__c != null) {
                accountsToProcess.put(programMember.Member_MVN__c, null);
            }
        }

        return accountsToProcess;
    }

    /**
     * @description get Lookup Fields of Zip_to_Team_MVN__c
     */
    private List<String> getLookupFields() {
        List<String> lookupFields = new List<String>();
        Set<String> fieldsToIgnore = new Set<String>{'program_mvn__c'};

        if(trigger.isInsert) {
            // only setting the various lookups on insert
            Map<String, Schema.SObjectField> fieldsMap = Schema.SObjectType.Zip_to_Team_MVN__c.fields.getMap();
            for(String field : fieldsMap.keySet()) {
                Schema.DescribeFieldResult obj = fieldsMap.get(field).getDescribe();
                if(obj.getType() == DisplayType.Reference &&
                   !fieldsToIgnore.contains(field.toLowerCase()) &&
                   field.endsWith('__c')) {

                    lookupFields.add(field);
                }
            }
        }

        return lookupFields;
    }

    /**
     * @description copyValuesFromZipToTeamToProgramMember dynamically. Return true if a match could be found, false otherwise
     */
    private Boolean copyValuesFromZipToTeamToProgramMember(Map<String, Zip_to_Team_MVN__c> zipToTeamByExternalId, String externalId, Program_Member_MVN__c programMember) {
        if(zipToTeamByExternalId.containsKey(externalId)) {
            Zip_to_Team_MVN__c zipToTeam = zipToTeamByExternalId.get(externalId);
            programMember.Zip_to_Team_MVN__c = zipToTeam.Id;
                
            for(String field : getLookupFields()) {
                if(!Schema.SObjectType.Program_Member_MVN__c.fields.getMap().containsKey(field)) {
                    programMember.addError(Label.Invalid_Configuration_For_Field_MVN + ' ' + field);
                    continue;
                }
                programMember.put(field, zipToTeam.get(field));
            }
            return true;
        } else {
            return false;
        }
    }
}