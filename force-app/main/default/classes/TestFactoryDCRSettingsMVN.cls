/**
 * @author Mavens
 * @group DCR Creation
 * @description Test Factory for DCR Settings - custom metadata
 */
public with sharing class TestFactoryDCRSettingsMVN {

    public static void setMocks() {
        List<Map<String, Object>> settings = new List<Map<String, Object>> {
            ACCOUNT_MOCK,
            ADDRESS_MOCK,
            AFFILIATION_MOCK,
            LICENSE_MOCK
        };

        List<DCR_Setting_MVN__mdt> dcrSettings = new List<DCR_Setting_MVN__mdt>();

        for (Map<String, Object> setting : settings) {
            dcrSettings.add(initMock(setting));
        }

        DCRSettingsMVN.setMocks(dcrSettings);
    }

    private static DCR_Setting_MVN__mdt initMock(Map<String, Object> fieldValues) {
        Map<String, Object> setting = new Map<String, Object>();

        if (fieldValues != null) {
            setting.putAll(fieldValues);
        }

        return (DCR_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata('DCR_Setting_MVN__mdt', setting);
    }

    public static Map<String, Object> ACCOUNT_MOCK = new Map<String, Object> {
        'Label' => 'Account',
        'SObject_Name_MVN__c' => 'Account',
        'DCR_Record_Type_Name_MVN__c' => 'Account_MVN',
        'SObject_Field_Id_MVN__c' => 'Account_MVN__c',
        'SObject_Record_Types_Allowed_MVN__c' => 'Professional_vod, Hospital_vod'
    };

    public static Map<String, Object> ADDRESS_MOCK = new Map<String, Object> {
        'Label' => 'Address',
        'SObject_Name_MVN__c' => 'Address_vod__c',
        'DCR_Record_Type_Name_MVN__c' => 'Address_MVN',
        'SObject_Field_Id_MVN__c' => 'Address_MVN__c',
        'Parent_Account_Field_Name_MVN__c' => 'Account_vod__c',
        'SObject_Record_Types_Allowed_MVN__c' => 'All'
    };

    public static Map<String, Object> AFFILIATION_MOCK = new Map<String, Object> {
        'Label' => 'Affiliation',
        'SObject_Name_MVN__c' => 'Affiliation_MVN__c',
        'Processed_Field_Name_MVN__c' => 'DCR_Affiliation_Processing_MVN__c',
        'DCR_Record_Type_Name_MVN__c' => 'Affiliation_MVN',
        'SObject_Field_Id_MVN__c' => 'Parent_Account_MVN__c',
        'SObject_Record_Types_Allowed_MVN__c' => 'All',
        'Parent_Account_Field_Name_MVN__c' => 'Child_Account_MVN__c',
        'Relationship_Field_Name_MVN__c' => 'Parent_Account_MVN__c',
        'Relationship_SObject_Name_MVN__c' => 'Account',
        'Relationship_SObject_Id_Field_MVN__c' => 'Affiliation_MVN__c',
        'Relationship_Record_Types_Allowed_MVN__c' => 'Hospital_vod'
    };

    public static Map<String, Object> LICENSE_MOCK = new Map<String, Object> {
        'Label' => 'License',
        'SObject_Name_MVN__c' => 'License_MVN__c',
        'DCR_Record_Type_Name_MVN__c' => 'License_MVN',
        'SObject_Field_Id_MVN__c' => 'License_MVN__c',
        'Parent_Account_Field_Name_MVN__c' => 'Account_MVN__c',
        'SObject_Record_Types_Allowed_MVN__c' => 'All'
    };
}