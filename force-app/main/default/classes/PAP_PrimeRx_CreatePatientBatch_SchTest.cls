@isTest
public class PAP_PrimeRx_CreatePatientBatch_SchTest {
    public static String CRON_EXP = '0 0 23 * * ?';
    @isTest
    public static void testScheduler(){
        Test.startTest();   
        PAP_PrimeRx_CreatePatientBatch_Scheduler sh1 = new PAP_PrimeRx_CreatePatientBatch_Scheduler();
        String jobId = system.schedule('Create Patient Batch', CRON_EXP, sh1);
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP,ct.CronExpression);
        Test.StopTest();
    }
}