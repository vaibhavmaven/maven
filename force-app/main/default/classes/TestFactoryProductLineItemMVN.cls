/**
* @author Mavens
* @date 07/14/2018 
* @description Class to provide factory methods to create test data for Product Line Item objects
*/
@isTest
public class TestFactoryProductLineItemMVN {

    private TestFactorySObjectMVN objectFactory;

    public TestFactoryProductLineItemMVN() {
        objectFactory = new TestFactorySObjectMVN('Product_Line_Item_MVN__c', new Map<String, Object>());
    }

    public Product_Line_Item_MVN__c construct(Map<String, Object> valuesByField){
        return (Product_Line_Item_MVN__c) objectFactory.constructSObject(valuesByField);
    }

    public Product_Line_Item_MVN__c create(Map<String, Object> valuesByField){
        return (Product_Line_Item_MVN__c) objectFactory.createSObject(valuesByField);
    }

    public List<Product_Line_Item_MVN__c> constructMany(Integer numOfRequests, Map<String,Object> valuesByField) {
        return (List<Product_Line_Item_MVN__c>) objectFactory.constructSObjects(numOfRequests, valuesByField);
    }

    public List<Product_Line_Item_MVN__c> createMany(Integer numOfRequests, Map<String,Object> valuesByField) {
        return (List<Product_Line_Item_MVN__c>) objectFactory.createSObjects(numOfRequests, valuesByField);
    }
}