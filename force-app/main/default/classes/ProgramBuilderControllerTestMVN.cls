/*
*   ProgramBuilderControllerTestMVN
*   Created By:     Brian Aggen
*   Created Date:   November 30, 2015
*	Description:	Provides unit test coverage for ProgramBuilderControllerMVN
*/

@isTest
private class ProgramBuilderControllerTestMVN {

	static Program_MVN__c program;
	static Program_Stage_MVN__c parent;
	static Program_Stage_MVN__c child1;
	static Program_Stage_MVN__c child2;
	static {
		program = TestDataFactoryMVN.createProgram();
		parent = TestDataFactoryMVN.createProgramStage(program);
		child1 = TestDataFactoryMVN.createChildProgramStage(program, parent);
		child2 = TestDataFactoryMVN.createChildProgramStage(program, parent);
	}

	@isTest
	static void test_getRecordTypeId() {
		Test.startTest();
		Id recTypeId = ProgramBuilderControllerMVN.getRecordTypeId('Program_Stage_MVN__c', 'Child_Stage_MVN');
		Test.stopTest();
		System.assertNotEquals(null, recTypeId);
		System.assertEquals([SELECT ID FROM RecordType WHERE SobjectType = 'Program_Stage_MVN__c' AND DeveloperName = 'Child_Stage_MVN'].Id, recTypeId);
	}

	@isTest
	static void test_getGraphNodesForParentStageId() {
		Test.startTest();
		List<ProgramBuilderControllerMVN.Node> nodes = ProgramBuilderControllerMVN.getGraphNodesForParentStageId(parent.Id);
		Test.stopTest();

		System.assertEquals(2, nodes.size());
		System.assertEquals(child1.Id, nodes[0].recordId);
		System.assertEquals('Child Stage', nodes[0].title);
		System.assertEquals(101, nodes[0].x);
		System.assertEquals(202, nodes[0].y);
		System.assertEquals(child2.Id, nodes[1].recordId);
		System.assertEquals('Child Stage', nodes[1].title);
		System.assertEquals(101, nodes[1].x);
		System.assertEquals(202, nodes[1].y);
	}

	@isTest
	static void test_getFieldSetsForSobject() {
		Test.startTest();
		Map<String, String> fieldSets = ProgramBuilderControllerMVN.getFieldSetsForSobject('Program_Stage_MVN__c');
		Test.stopTest();

		System.assertEquals(2, fieldSets.size());
		System.assert(fieldSets.keySet().contains('Program Stage Builder Parent Stage'));
		System.assertEquals('Program_Stage_Builder_Parent_Stage_MVN', fieldSets.get('Program Stage Builder Parent Stage'));
		System.assert(fieldSets.keySet().contains('Program Stage Builder Child Stage'));
		System.assertEquals('Program_Stage_Builder_Child_Stage_MVN', fieldSets.get('Program Stage Builder Child Stage'));
	}

	@isTest
	static void test_getPicklistValues() {
		Test.startTest();
		Map<String, String> picklistVals = ProgramBuilderControllerMVN.getPicklistValues('Program_Stage_MVN__c', 'Activity_Type_MVN__c');
		Test.stopTest();

		System.assertEquals(2, picklistVals.size());
		System.assert(picklistVals.keySet().contains('Case'));
		System.assertEquals('Case', picklistVals.get('Case'));
		System.assert(picklistVals.keySet().contains('Task'));
		System.assertEquals('Task', picklistVals.get('Task'));
	}

	@isTest
	static void test_getGraphEdgesForParentStageId() {
		Program_Stage_Dependency_MVN__c dependency1 = TestDataFactoryMVN.createStageDependency(parent, child1);
		Program_Stage_Dependency_MVN__c dependency2 = TestDataFactoryMVN.createStageDependency(child1, child2);

		Test.startTest();
		List<ProgramBuilderControllerMVN.Edge> edges = ProgramBuilderControllerMVN.getGraphEdgesForParentStageId(parent.Id);
		Test.stopTest();

		System.assertEquals(2, edges.size());
		System.assertEquals(parent.id, edges[0].source.recordId);
		System.assertEquals(child1.id, edges[0].target.recordId);
		System.assertEquals(child1.id, edges[1].source.recordId);
		System.assertEquals(child2.id, edges[1].target.recordId);
	}

	@isTest
	static void test_insertGraphNode() {
		Test.startTest();
		String stageId = insertGraphNode();
		Test.stopTest();

		System.assert(String.isNotBlank(stageId));
		Program_Stage_MVN__c stage = [SELECT Name,
		                                     Program_MVN__c,
		                                     Parent_Stage_MVN__c,
		                                     Graph_X_Coordinate_MVN__c,
											 Graph_Y_Coordinate_MVN__c
									    FROM Program_Stage_MVN__c
									   WHERE Id = :stageId];
		System.assertNotEquals(null, stage);
		System.assertEquals('New Activty', stage.Name);
		System.assertEquals(program.Id, stage.Program_MVN__c);
		System.assertEquals(parent.Id, stage.Parent_Stage_MVN__c);
		System.assertEquals(123, stage.Graph_X_Coordinate_MVN__c);
		System.assertEquals(456, stage.Graph_Y_Coordinate_MVN__c);
	}

	@isTest
	static void test_updateGraphNode() {
		String nodeId = insertGraphNode();
		Test.startTest();
		Boolean updated = ProgramBuilderControllerMVN.updateGraphNode(nodeId, 'Updated Activity', 321, 654);
		Test.stopTest();

		System.assert(updated);
		Program_Stage_MVN__c ps = [SELECT Name,
		                                  Graph_X_Coordinate_MVN__c,
		                                  Graph_Y_Coordinate_MVN__c
		                             FROM Program_Stage_MVN__c
		                            WHERE ID = :nodeId];
		System.assertEquals('Updated Activity', ps.Name);
		System.assertEquals(321, ps.Graph_X_Coordinate_MVN__c);
		System.assertEquals(654, ps.Graph_Y_Coordinate_MVN__c);
	}

	@isTest
	static void test_deleteGraphNode() {
		String nodeId = insertGraphNode();
		Test.startTest();
		Boolean deleted = ProgramBuilderControllerMVN.deleteGraphNode(nodeId);
		Test.stopTest();

		System.assert(deleted);
		System.assertEquals(0, [SELECT COUNT() FROM Program_Stage_MVN__c WHERE Id = :nodeId]);
	}

	@isTest
	static void test_insertGraphEdge() {
		Test.startTest();
		String edgeId = ProgramBuilderControllerMVN.insertGraphEdge(parent.id, child1.id);
		Test.stopTest();

		System.assert(String.isNotBlank(edgeId));
		Program_Stage_Dependency_MVN__c edge = [SELECT Program_Stage_MVN__c,
		                                               Program_Stage_Dependency_MVN__c
		                                          FROM Program_Stage_Dependency_MVN__c
		                                         WHERE Id = :edgeId];
		System.assertEquals(parent.Id, edge.Program_Stage_Dependency_MVN__c);
		System.assertEquals(child1.Id, edge.Program_Stage_MVN__c);
	}

	@isTest
	static void test_updateGraphEdge() {
		Test.startTest();
		System.assert( ProgramBuilderControllerMVN.updateGraphEdge('') );
		Test.stopTest();
	}

	@isTest
	static void test_deleteGraphEdge() {
		String edgeId = ProgramBuilderControllerMVN.insertGraphEdge(parent.id, child1.id);

		Test.startTest();
		Boolean deleted = ProgramBuilderControllerMVN.deleteGraphEdge(edgeId);
		Test.stopTest();

		System.assert(deleted);
		System.assertEquals(0, [SELECT COUNT() FROM Program_Stage_Dependency_MVN__c WHERE Id = :edgeId]);
	}

	@isTest
	static void test_exercise_other_methods() {
		ProgramBuilderControllerMVN pb = new ProgramBuilderControllerMVN();
		pb.ajaxEndpoint();
		new ProgramBuilderControllerMVN(new ApexPages.StandardController(program));
	}

	@isTest
	static void test_cloneProgram() {
		ProgramBuilderControllerMVN.insertGraphEdge(parent.id, child1.id);
		ProgramBuilderControllerMVN.insertGraphEdge(child1.id, child2.id);

		Test.startTest();
		String programId = ProgramBuilderControllerMVN.cloneProgram(program.Id, 'Cloned Program');
		Test.stopTest();

		Program_MVN__c newProgram = [SELECT Id, Name FROM Program_MVN__c WHERE Id= :programId];
		System.assertNotEquals(null, newProgram);
		System.assertEquals('Cloned Program', newProgram.Name);

		System.assertEquals(2, [SELECT COUNT() FROM Program_MVN__c]);

		//coming back as 5 since there were 4 to start and it creates a "new stage" when the program is created.
		System.assertEquals(4, [SELECT COUNT() FROM Program_Stage_MVN__c WHERE Program_MVN__c = :programId]);

		System.assertEquals(4, [SELECT COUNT() FROM Program_Stage_Dependency_MVN__c]);
	}

	static String insertGraphNode() {
		return ProgramBuilderControllerMVN.insertGraphNode(program.id, parent.id, 'New Activty', 123, 456);
	}
}