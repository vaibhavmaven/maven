/**
* @author Mavens
* @group Network API
* @description This class is used to construct Veeva Network Settings custom metadatas
*/
@isTest
public with sharing class TestFactoryVeevaNetworkSettingsMVN {

    public static void setMock() {
        setMock(null);
    }

    public static void setMock(Map<String, Object> fieldValues) {
        Map<String, Object> setting = new Map<String, Object> {
            'DeveloperName' => 'Default',
            'Active_Code_Status_MVN__c' => 'A',
            'Address_Record_Name_MVN__c' => 'addresses__v',
            'Address_Status_Field_Name_MVN__c' => 'address_status__v',
            'Address_Rank_Field_Name_MVN__c' => 'address_ordinal__v',
            'Address_Rank_Filter_MVN__c' => '1-2, 4-5',
            'Address_Type_MVN__c' => 'Address',
            'API_Version_MVN__c' => 12.0,
            'Completed_Date_Field_Name_MVN__c' => 'completed_date',
            'DCR_Account_MVN__c' => 'Account_MVN',
            'DCR_Address_MVN__c' => 'Address_MVN',
            'DCR_Parent_MVN__c' => 'Affiliation_MVN',
            'Default_Search_Country_MVN__c' => 'US',
            'Entity_Id_Field_Name_MVN__c' => 'vid__v',
            'Entity_Record_Name_MVN__c' => 'entity',
            'External_Id_Field_Name_MVN__c' => 'Network_Id_MVN__c',
            'HCO_Type_MVN__c' => 'HCO',
            'HCP_Type_MVN__c' => 'HCP',
            'Inactive_User_Callout_Status_MVN__c' => 'INACTIVE_USER',
            'Insufficient_Access_Callout_Status_MVN__c' => 'INSUFFICIENT_ACCESS',
            'Invalid_Session_Id_Callout_Status_MVN__c' => 'INVALID_SESSION_ID',
            'Parent_HCO_Attributes_Record_Name_MVN__c' => 'supplementalResults',
            'Parent_HCO_Id_Field_Name_MVN__c' => 'parent_hco_vid__v',
            'Parent_HCO_Record_Name_MVN__c' => 'parent_hcos__v',
            'Search_Limit_MVN__c' => 100,
            'Source_MVN__c' => 'Patient Services',
            'Success_Callout_Status_MVN__c' => 'SUCCESS',
            'System_MVN__c' => 'PS',
            'User_Locked_Out_Callout_Status_MVN__c' => 'USER_LOCKED_OUT'
        };

        if (fieldValues != null) {
            setting.putAll(fieldValues);
        }

        VeevaNetworkSettingsMVN.setMock((Veeva_Network_Settings_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata('Veeva_Network_Settings_MVN__mdt', setting));
    }
}