/**
 * Record Types
 * @company Mavens <https://mavens.com/>
 * @author  Jean-Philippe Monette <jpmonette@mavens.com>
 * @since   2016-05-24
 */
public with sharing class RecordTypesMVN {

  private static Set<SObjectType>      cachedTypes = new Set<SObjectType>();
  public static Map<Id, RecordType> recordTypeMap = new Map<Id, RecordType>();

  /**
   * Caching all Record Types for a SObject
   * @param		SObjectType		SObject Type to cache
   * @return	void
   * @access	public static
   */
  public static void cache(SObjectType objectType) {
    cache(new Set<SObjectType>{objectType});
  }

  /**
   * Caching all Record Types for some SObject Types
   * @param   SObjectType[]   SObject Types to cache
   * @return  void
   * @access  public static
   */
  public static void cache(Set<SObjectType> objectTypes) {
    objectTypes.removeAll(cachedTypes);

    if(objectTypes.isEmpty()) return;
    cachedTypes.addAll(objectTypes);

    String[] typeNames = new String[]{};

    for(SObjectType objectType : cachedTypes) {
      typeNames.add(objectType.getDescribe().getName());
    }

    recordTypeMap.putAll([
      SELECT DeveloperName, SObjectType
        FROM RecordType
       WHERE SObjectType IN :typeNames
    ]);
  }

  /**
   * Getting a Record Type by Record ID
   * @param		Id				Record ID
   * @return	RecordType
   * @access	public static
   */
  public static RecordType get(Id rtId) {
    if(!recordTypeMap.containsKey(rtId))
      recordTypeMap.putAll([
        SELECT DeveloperName, SObjectType
          FROM RecordType
         WHERE Id = :rtId
      ]);

    return recordTypeMap.get(rtId);
  }

  /**
   * Getting a Record Type ID by Developer Name
   * @param		SObjectType		SObject Type
   * @param		String			Developer name
   * @return	Id
   * @access	public static
   */
  public static Id getId(SObjectType objectType, String developerName) {
    String typeName = objectType.getDescribe().getName();

    for(RecordType rt : recordTypeMap.values()) {
      if(rt.SObjectType == typeName && rt.DeveloperName == developerName) {
        return rt.Id;
      }
    }

    RecordType[] rts = [
      SELECT DeveloperName, SObjectType
        FROM RecordType
       WHERE SObjectType = :typeName
         AND DeveloperName = :developerName
    ];
    recordTypeMap.putAll(rts);

    return rts[0].Id;
  }

  /**
   * Getting a Record Type ID by Name
   * @param		SObjectType		SObject Type
   * @param		String			Record Type Name
   * @return	Id
   * @access	public static
   */
  public static Id getIdByName(SObjectType objectType, String name) {
    String typeName = objectType.getDescribe().getName();

    RecordType[] rts = [
      SELECT DeveloperName, SObjectType, Id
        FROM RecordType
       WHERE SObjectType = :typeName
         AND Name = :name
    ];
    recordTypeMap.putAll(rts);

    return rts[0].Id;
  }

}