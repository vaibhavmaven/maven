/**
 * AccountSearchRsltTestMVN.cls
 * @created By: Kyle Thornton
 * @created Date: Jun 2016
 * @edited By: Pablo Roldan
 * @edited Date: November 2017
 * @description: Test class for AccountSearchRsltMVN
 * @edit description: Adapted to Patient Support
 *
 */
@isTest
private class AccountSearchRsltTestMVN {
    static Account testAccount;
    static Address_vod__c testAddress;
    static AccountSearchRqstMVN request;
    static AccountSearchRsltMVN result;
    static User testUser;

    static AccountFieldSettingsMVN searchSettings;
    static AccountFieldSettingsMVN resultSettings;

    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
    static TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();
    static TestFactoryUserMVN userFactory = new TestFactoryUserMVN();

    static {
        TestFactoryCustomMetadataMVN.setMocks();

        System.runAs(new User(Id=UserInfo.getUserId())) {
            testUser = userFactory.create();
        }

        testAccount = accountFactory.createPrescriber(new Map<String,Object>{});
        testAddress = addressFactory.createAddress(testAccount, new Map<String,Object>{
            'Name' => 'Address Line 1',
            'Address_line_2_vod__c' => 'Address Line 2',
            'Primary_vod__c' => true,
            'City_vod__c' => 'City',
            'Zip_vod__c' => '11111',
            'State_vod__c' => 'IL',
            'Phone_vod__c' => '5555555555'
        });

        searchSettings = new AccountFieldSettingsMVN('US', 'HCP', 'Search_Field_Order_MVN__c', false);
        resultSettings = new AccountFieldSettingsMVN('US', 'HCP', 'Search_Results_Order_MVN__c', false);
        request = new AccountSearchRqstMVN(searchSettings, resultSettings, 'US');

        TestDataFactoryMVN.createSearchSettings();
    }

	@isTest
	static void itShouldInstantiateAResultWithJustAnAccount() {
        System.runAs(testUser) {
            Test.startTest();
            result = new AccountSearchRsltMVN(testAccount, 'aSearchUtilityClass', request);
            Test.stopTest();
            System.assertEquals(testAccount, result.accountDetail);
        }
	}

    @isTest
    static void itShouldInstantiateAResultWithAnAccountAndAddressRecord() {
        System.runAs(testUser) {
            Test.startTest();
            result = new AccountSearchRsltMVN(testAccount, testAddress, 'aSearchUtilityClass', request);
            Test.stopTest();
            System.assertEquals(testAccount, result.accountDetail);
            // Id phoneRecTypeId = UtilitiesMVN.getRecordTypesForObject('Address_vod__c').get('Phone_MVN').Id;
            System.assertEquals(testAddress, result.getAddressForRecordType(null)[0]);
        }
    }

    @isTest
    static void itShouldInstantiateAResultWithJustAAddressRecord() {
        System.runAs(testUser) {
            Test.startTest();
            result = new AccountSearchRsltMVN(testAddress, 'aSearchUtilityClass', request);
            Test.stopTest();
            System.assertEquals(testAccount.ID, result.accountDetail.Id);
            // Id phoneRecTypeId = UtilitiesMVN.getRecordTypesForObject('Address_vod__c').get('Phone_MVN').Id;
            System.assertEquals(testAddress, result.getAddressForRecordType(null)[0]);
        }
    }

    @isTest
    static void itShouldAllowAccountTypeSettingsToBeSet() {
        TestFactoryAccountTypeSettingsMVN.setMock();
        System.runAs(testUser) {
            result = new AccountSearchRsltMVN(testAccount, 'aSearchUtilityClass', request);
            Test.startTest();
            AccountTypeSettingsMVN acctTypeSettings = new AccountTypeSettingsMVN('US');
            result.setAccountTypeSettings(acctTypeSettings);
            Test.stopTest();
            System.assertEquals(acctTypeSettings, result.acctTypeSettings);
        }
    }

    @isTest
    static void itShouldIndicateIfTheResultCanBeAReferrer() {
        TestFactoryAccountTypeSettingsMVN.setMock();
        System.runAs(testUser) {
            Test.startTest();
            result = new AccountSearchRsltMVN(testAccount, 'aSearchUtilityClass', request);
            System.assertEquals(false, result.getCanBeReferrer());

            AccountTypeSettingsMVN acctTypeSettings = new AccountTypeSettingsMVN('US');
            result.setAccountTypeSettings(acctTypeSettings);
            System.assertEquals(false, result.getCanBeReferrer());

            Id empRecTypeId = UtilitiesMVN.getRecordTypesForObject('Account').get('Staff_MVN').Id;
            result.accountDetail.RecordTypeId = empRecTypeId;
            System.assertEquals(true, result.getCanBeReferrer());

            Test.stopTest();
        }
    }

    @isTest
    static void itShouldReturnTheProperAccountIdentifier() {
        System.runAs(testUser) {
            result = new AccountSearchRsltMVN(testAccount, 'aSearchUtilityClass', request);
            Test.startTest();
            System.assertEquals(testAccount.Id, result.getAccountIdentifier());

            result.accountDetail.Network_Id_MVN__c = 'anExternalId';
            System.assertEquals('anExternalId', result.getAccountIdentifier());
            Test.stopTest();
        }
    }

    @isTest
    static void itShouldBeAbleToAddAddressRecords() {
        System.runAs(testUser) {
            result = new AccountSearchRsltMVN(testAccount, 'aSearchUtilityClass', request);
            Test.startTest();

            Address_vod__c testAddress1 = testAddress.clone(false, false, false);
            testAddress1.Network_Id_MVN__c = 'newId1';
            result.addAddressRecord(testAddress1);

            Address_vod__c testAddress2 = testAddress.clone(false, false, false);
            testAddress2.Network_Id_MVN__c = 'newId2';
            result.addAddressRecords(new List<Address_vod__c>{testAddress2});

            Test.stopTest();
        }
    }

    @isTest
    static void itShouldBeAbleToAddParentAccounts() {
        TestFactoryAccountTypeSettingsMVN.setMock();
        System.runAs(testUser) {
            Test.startTest();
            result = new AccountSearchRsltMVN(testAccount, 'aSearchUtilityClass', request);
            Account parentAccount = testAccount.clone(false, false, false);
            AccountSearchRsltMVN parentResult = new AccountSearchRsltMVN(parentAccount, 'aSearchUtilityClass', request);
            result.addParentAccount(parentResult);
            Test.stopTest();
            System.assertEquals(2, result.getAccountAndParentAccounts().size());
        }
    }

    @isTest
    static void itShouldBeAbleToAddSObjectsRepresentingAffiliations() {
        System.runAs(testUser) {
            Test.startTest();
            result = new AccountSearchRsltMVN(testAccount, 'aSearchUtilityClass', request);
            result.addAffiliations(new List<SObject>{new Affiliation_MVN__c()});
            System.assertEquals(1,result.getAffiliations().size());
            Test.stopTest();
        }
    }

    private static Affiliation_MVN__c createAffiliation() {
        //set up affiliations
        Account parentAccount = testAccount.clone(false, false, false);
        insert parentAccount;

        return new Affiliation_MVN__c(Parent_Account_MVN__c=parentAccount.Id,
                                      Child_Account_MVN__c=testAccount.Id,
                                      Role_MVN__c='test affliation');
    }

    @isTest
    static void itShouldReturnAllAddressRecordsInAList() {
        TestFactoryAccountFieldSettingsMVN.setAddressPhoneAndAffiliationMock();
        //request needs new result settings based on above mock.
        resultSettings = new AccountFieldSettingsMVN('US', 'HCP', 'Search_Results_Order_MVN__c', false);
        request = new AccountSearchRqstMVN(searchSettings, resultSettings, 'US');
        System.runAs(testUser) {
            Test.startTest();
            result = new AccountSearchRsltMVN(testAccount, testAddress, 'aSearchUtilityClass', request);
            result.addAddressRecord(testAddress);
            List<Address_vod__c> ciRecords = result.getAllAddressRecords();
            System.assertEquals(1, ciRecords.size());

            //add some affiliations to cover the parent contact info section
            Affiliation_MVN__c affiliation = createAffiliation();
            result.addAffiliations(new List<SObject>{affiliation, affiliation});

            ciRecords = result.getAccountAndParentsAddressRecords();
            System.assertEquals(1, ciRecords.size());
            Test.stopTest();
        }
    }

    @isTest
    static void itShouldInsertAccountDetailsWhenUtilityIsValid() {
        System.runAs(testUser) {
            Test.startTest();
            result = new AccountSearchRsltMVN(testAccount, testAddress, 'NetworkAccountSearchHdlrMVN', request);
            system.assertEquals(result, result.insertAccountDetails());

            result = new AccountSearchRsltMVN(testAccount, testAddress, null, request);
            Boolean exceptionCaught = false;
            try {
                result.insertAccountDetails();
            } catch (UtilitiesMVN.utilException ex) {
                System.assertEquals('This instance of AccountSearchRsltMVN was constructed with a null searchUtilityClass.',
                                    ex.getMessage());
                exceptionCaught = true;
            }
            System.assert(exceptionCaught);
            Test.stopTest();
        }
    }
}