/*
 *	ProgramMemberStageControllerTestMVN
 *	Created By:		Thomas Hajcak
 *	Created Date:   March 28, 2016
 *	Description:	Unit tests for ProgramMemberStageControllerMVN
 */

@isTest
private class ProgramMemberStageControllerTestMVN  {

	static ApexPages.StandardController stdController;
	static ProgramMemberStageControllerMVN controller;
	static Program_Member_MVN__c programMember;
	static Program_Stage_MVN__c stage1;
	static Program_Stage_MVN__c stage2;
	static Program_Stage_MVN__c stage3;
	static Program_Stage_MVN__c stage4;
	static Program_Member_Stage_MVN__c pms1;
	static Program_Member_Stage_MVN__c pms2;
	static Program_Member_Stage_MVN__c pms3;
	static Program_Member_Stage_MVN__c pms4;

	static{
		TestDataFactoryMVN.createPSSettings();
		Program_MVN__c program = TestDataFactoryMVN.createProgram();
		Account member = TestDataFactoryMVN.createMember();
		programMember = TestDataFactoryMVN.createProgramMember(program, member);
		stage1 = TestDataFactoryMVN.createProgramStage(program);
		stage2 = TestDataFactoryMVN.createChildProgramStage(program,stage1);
		stage3 = TestDataFactoryMVN.createChildProgramStage(program,stage1);
		stage4 = TestDataFactoryMVN.createProgramStage(program);

		Program_Stage_Dependency_MVN__c dependency = TestDataFactoryMVN.createStageDependency(stage1, stage2);
		TestDataFactoryMVN.createStageDependency(stage2, stage3);
		TestDataFactoryMVN.createStageDependency(stage3, stage4);

		pms1 = TestDataFactoryMVN.createProgramMemberStage(programMember, stage1);
		pms2 = TestDataFactoryMVN.createProgramMemberStage(programMember, stage2);
		pms3 = TestDataFactoryMVN.createProgramMemberStage(programMember, stage3);
		pms4 = TestDataFactoryMVN.createProgramMemberStage(programMember, stage4);

		Program_Member_Stage_Dependency_MVN__c dependencyMember = new Program_Member_Stage_Dependency_MVN__c(
			Program_Member_Stage_MVN__c = pms2.Id,
			Program_Member_Stage_Dependency_MVN__c = pms1.Id
			);
		insert dependencyMember;

		pms1.Stage_Sequence_Number_MVN__c = 1;
		pms2.Stage_Sequence_Number_MVN__c = 2;
		pms3.Stage_Sequence_Number_MVN__c = 3;
		pms4.Stage_Sequence_Number_MVN__c = 4;

		pms2.Parent_Program_Member_Stage_MVN__c = pms1.id;
		Task pms2Task = TestDataFactoryMVN.createStageTask(pms2);
		pms2.Activity_ID_MVN__c = pms2Task.Id;

		pms3.Parent_Program_Member_Stage_MVN__c = pms1.id;
		Case pms3Case = TestDataFactoryMVN.createStageCase(pms3);
		pms3.Activity_ID_MVN__c = pms3Case.Id;

		update new List<Program_Member_Stage_MVN__c>{pms1, pms2, pms3, pms4};

		stdController = new ApexPages.StandardController(programMember);
		controller = new ProgramMemberStageControllerMVN(stdController);
	}

	@isTest
	static void testUpdateProgramMemberStatus() {
		Test.startTest();
		//Create an Event_MVN__c
		controller.programMemberStatus = 'Pending';
		controller.updateProgramMemberStatus();
		Test.stopTest();

		List<Event_MVN__c> events = [SELECT Id FROM Event_MVN__c WHERE Program_Member_MVN__c = :programMember.Id];
		System.assert(events.size() > 0);
	}

	@isTest
	static void testGetProgramParentStages() {
		Test.startTest();
		List<Program_Stage_MVN__c> programParentStages = controller.programParentStages;
		List<Program_Member_Stage_MVN__c> programMemberStages = controller.parentStages;
		Test.stopTest();

		System.assertEquals(3, programParentStages.size());
		System.assertEquals(3, programMemberStages.size());
	}

	@isTest
	static void testAddAndStartStages() {
		Test.startTest();
		controller.addStageId = stage1.Id;
		controller.addStage();

		controller.startStageId = pms1.Id;
		controller.startStage();
		Test.stopTest();

		Date today = System.today();

		System.assertEquals(1, [SELECT COUNT() FROM Program_Member_Stage_MVN__c
												WHERE Id = :pms1.Id
												AND Status_MVN__c = 'Started'
												AND Initiated_Date_MVN__c = :today] );
	}

	@isTest
	static void testSkipStage() {
		controller.addStageId = stage1.Id;
		controller.addStage();

		controller.startStageId = stage1.Id;
		controller.startStage();

		controller.startStageId = pms1.Id;
		controller.startStage();

		Test.startTest();
		controller.skipStageId = pms1.Id;
		controller.skipStage();
		Test.stopTest();

		Date today = System.today();

		System.assertEquals(1, [SELECT COUNT() FROM Program_Member_Stage_MVN__c
									WHERE Id = :pms1.Id
									AND IsClosed_MVN__c = true
                                    AND Closed_Date_MVN__c = :today], 'The stage should have been closed');

		System.assertEquals(2, [SELECT COUNT() FROM Program_Member_Stage_MVN__c
                                    WHERE Parent_Program_Member_Stage_MVN__c = :pms1.Id
                                    AND IsClosed_MVN__c = true
                                    AND Closed_Date_MVN__c = :today], 'The two child stages should also have been closed');
	}

	@isTest
	static void testRemoveStage() {
		Test.startTest();
		controller.removeStageId = pms1.Id;
		controller.removeStage();
		Test.stopTest();

		System.assertEquals(0, [SELECT COUNT() FROM Program_Member_Stage_MVN__c WHERE ID = :pms1.Id OR Id = :pms2.Id], 'The stage and its children should have been deleted');
	}

	// TEST EXCEPTION HANDLING
	@isTest
	static void testSkipStageWithException() {
		Test.setReadOnlyApplicationMode(true);
		Test.startTest();

		//Skip stage
		controller.skipStageId = pms1.Id;
		try {
			controller.skipStage();
		}catch(Exception e){}

		Test.stopTest();
        System.assert(!ApexPages.getMessages().isEmpty(), 'The page should display an error message');
        System.assertEquals(true, controller.hasError);
        System.assert(controller.errorMessage.contains('Update failed'));
	}

	@isTest
	static void testRemoveStageWithException() {
		Test.setReadOnlyApplicationMode(true);

		Test.startTest();

		controller.removeStageId = pms1.Id;
		try {
			controller.removeStage();
		}catch(Exception e){}

		Test.stopTest();

        System.assert(!ApexPages.getMessages().isEmpty(), 'The page should display an error message');
        System.assertEquals(true, controller.hasError);
        System.assert(controller.errorMessage.contains('Delete failed'));
	}

	@isTest
	static void testStartStageWithException() {
		Test.setReadOnlyApplicationMode(true);

		Test.startTest();

		controller.startStageId = pms1.Id;

		try {
			controller.startStage();
		}catch(Exception e){}

		Test.stopTest();

        System.assert(!ApexPages.getMessages().isEmpty(), 'The page should display an error message');
        System.assertEquals(true, controller.hasError);
        System.assert(controller.errorMessage.contains('Update failed'));
	}

	@isTest
	static void testUpdateStageWithException() {
		Test.setReadOnlyApplicationMode(true);

		Test.startTest();

		controller.programMemberStatus = 'New';

		try {
			controller.updateProgramMemberStatus();
		}catch(Exception e){}

		Test.stopTest();

        System.assert(!ApexPages.getMessages().isEmpty(), 'The page should display an error message');
        System.assertEquals(true, controller.hasError);
        System.assert(controller.errorMessage.contains('Insert failed'));
	}

    @isTest
    static void testUpdatePMPatientStatus() {
        assertPatientStatusIs('Opt-In');
        Test.startTest();
        controller.pm.Patient_Status_MVN__c = 'Opt-Out';
        controller.updatePatientStatus();
        Test.stopTest();
        assertPatientStatusIs('Opt-Out');
    }

    @isTest
    static void testUpdatePMPatientStatusWithException() {
        Test.setReadOnlyApplicationMode(true);
        Test.startTest();
        controller.pm.Patient_Status_MVN__c = 'Opt-Out';

        try {
			controller.updatePatientStatus();
		}catch(Exception e){}
        Test.stopTest();

        System.assert(!ApexPages.getMessages().isEmpty(), 'The page should display an error message');
        System.assertEquals(true, controller.hasError);
        System.assert(controller.errorMessage.contains('Update failed'), controller.errorMessage);
    }

    private static void assertPatientStatusIs(String status) {
        System.assertEquals(status, [
            SELECT Patient_Status_MVN__c
            FROM Program_Member_MVN__c
            WHERE Id = :programMember.Id].Patient_Status_MVN__c
        );
    }
}