/**
 * @author      Knipper Admin
 * @date        April 2019
 * @group       Nintex
 * @description Reset Nintex counter field
 *             
 */
global class NintexResetCounterJKC implements  Schedulable {

    /**
    * EXECUTE SCHEDULE
    * @description: Call class when the schedule runs
    */
    global void execute(SchedulableContext sc) {
        resetCounter();
    }
       
    public void resetCounter()
    {
            List<Program_Member_MVN__c> programMembers= new List<Program_Member_MVN__c>();
            programMembers= [SELECT ID,Nintex_Counter_JKC__c FROM Program_Member_MVN__c];

            for(Program_Member_MVN__c pm : programMembers)
            {
                pm.Nintex_Counter_JKC__c = 0;
            }

            update programMembers;
    
    }


}