/**
 * @author Mavens
 * @group Network API
 * @description Trigger handler for DCRs.
 */
public with sharing class DCRTriggerHandlerMVN implements TriggerHandlerMVN.HandlerInterface {
    private static String dcrType;
    private static Map<Id, SObject> oldRecordsMap;

    public void handle() {
        if (Trigger.IsInsert) {
            dcrType = 'New';
        } else if(Trigger.IsUpdate) {
            dcrType = 'Edit';
            oldRecordsMap = Trigger.oldMap;
        }

        execute();
    }

    private void execute() {
        Boolean isAllowed = String.IsNotBlank(dcrType);

        if (isAllowed) {
            List<Type> dcrCreationUtilityTypes = getDCRCreationUtilityTypes();

            for (Type dcrCreationUtilityType : dcrCreationUtilityTypes) {
                DCRCreationIntfMVN dcrCreationUtility = (DCRCreationIntfMVN) dcrCreationUtilityType.newInstance();
                Boolean isDCRCreated = dcrCreationUtility.createDCRs(Trigger.new, dcrType, oldRecordsMap);
            }
        }
    }

    private List<Type> getDCRCreationUtilityTypes() {
        List<Type> dcrCreationUtilityTypes = new List<Type>();

        List<String> dcrCreationHdlrNames = getDCRCreationHdlrNames();

        for (String dcrCreationHdlrName : dcrCreationHdlrNames) {
            dcrCreationUtilityTypes.add(Type.forName(dcrCreationHdlrName));
        }

        return dcrCreationUtilityTypes;
    }

    private static List<String> getDCRCreationHdlrNames() {
        List<String> dcrCreationHdlrNames = new List<String>();

        DCRGlobalSettingMVN dcrGlobalSetting = UtilitiesMVN.dcrGlobalSetting;

        Boolean isDCRCreateHandlerConfigured = dcrGlobalSetting != null
                && String.IsNotBlank(dcrGlobalSetting.setting.DCR_Create_Handler_Class_MVN__c);

        if (isDCRCreateHandlerConfigured) {
            List<String> dcrCreateHandlerClasses = dcrGlobalSetting.setting.DCR_Create_Handler_Class_MVN__c.split(',');

            for (String dcrCreateHandlerClass : dcrCreateHandlerClasses) {
                dcrCreationHdlrNames.add(dcrCreateHandlerClass.normalizeSpace());
            }
        }

        return dcrCreationHdlrNames;
    }
}