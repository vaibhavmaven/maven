public class PAP_SendNotification_Batch_Scheduler implements Schedulable{
   
    public void execute(SchedulableContext sc){
         // Execute a batch job 
        String test = 'Test';
        Database.executeBatch(new PAP_SendNotification_Batch(),1);
         system.assertNotEquals(test.Length(), 1);
    }
}