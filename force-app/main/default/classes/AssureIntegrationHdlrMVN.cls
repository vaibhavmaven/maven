/**
 * @author Mavens
 * @date August 2018
 * @group AssureIntegration
 * @description Handler for the callout to the Assure server to check the license 
 */
public with sharing class AssureIntegrationHdlrMVN {

    private String username;
    private String password;
    private String validationType;

    private final Map<String, Assure_Setting_MVN__mdt> ASSURE_CODES_MAP {
        get {
            return AssureSettingMVN.allSettingsByCode;
        }
    }

    /**
     * constructor: collect username and password for the connection and the type of validation
     * @param  username       username to be passed in the callout to the Assure server
     * @param  password       password to be passed in the callout to the Assure server
     * @param  validationType indicates the type of license to be checked
     */ 
    public AssureIntegrationHdlrMVN(String username, String password, String validationType) {
        this.username = username;
        this.password = password;
        this.validationType = validationType;
    }

    /**
     * Execute the callout to the Assure Web Service
     * @param programMember Program Member with the physician information needed to call Assure
     * @param primaryAddress Primary Address for the physician
     * @return   AssureWebServiceValidateMVN.PractValidationType Assure Web Service response
     */ 
    public AssureResponseWrapper callAssureWebService(
        Program_Member_MVN__c programMember, 
        Address_vod__c primaryAddress,
        String DEASchedule
    ) {

        AssureWebServiceMVN.soap ws = new AssureWebServiceMVN.soap();
        ws.timeout_x = 60 * 1000;
        AssureWebServiceValidateMVN.PractValidationType res = ws.GetData(
            this.username,
            this.password,
            this.validationType,
            programMember.Program_MVN__r.Job_No_MVN__c, 
            programMember.Physician_MVN__r.LastName,
            programMember.Physician_MVN__r.FirstName,
            programMember.Physician_MVN__r.MiddleName,
            programMember.Physician_MVN__r.Suffix,
            programMember.Physician_MVN__r.Credentials_vod__c, // 'MD'
            programMember.Physician_MVN__r.DEA_MVN__c,
            primaryAddress.Name,
            primaryAddress.Address_line_2_vod__c,
            '',         // Address line 3
            primaryAddress.City_vod__c,
            primaryAddress.State_vod__c,
            primaryAddress.Zip_vod__c,
            programMember.Physician_MVN__r.SLN_MVN__c,
            '',         // String productCode
            null,       // Integer DEAOffSet
            DEASchedule,
            '', ''      // String ssanno,String ssanexemption
        );
        return this.processResponse(res);
    }

    /**
     * Process the result returned by Assure Web Service, translating the code into a valid engine
     * result.
     * @param  res Assure Web Service response
     * @return     wrapper with the engine result and Assure returned message
     */
    private AssureResponseWrapper processResponse(AssureWebServiceValidateMVN.PractValidationType res) {
        AssureResponseWrapper resultWrapper = new AssureResponseWrapper();
        String resultMessage = '';
        Boolean denied = false;
        Boolean missingInformation = false;
        Boolean passed = false;

        List<String> returnCodes = res.Returncode.split(',');
        for (String code : returnCodes) {
            if (ASSURE_CODES_MAP.get(code) != null) {
                if (ASSURE_CODES_MAP.get(code).Engine_Result_Value_MVN__c == EligibilityEngineMVN.RULEDENIED) {
                    denied = true;
                } else if (ASSURE_CODES_MAP.get(code).Engine_Result_Value_MVN__c == EligibilityEngineMVN.RULEMISSINGINFO) {
                    missingInformation = true;
                } else {
                    passed = true;
                }
                String message = ASSURE_CODES_MAP.get(code).Engine_Result_Message_MVN__c;
                if (message != null) {
                    resultMessage += message + ';';
                }
            }
        }
        if (!denied && !missingInformation && !passed) {
            resultWrapper.result = EligibilityEngineMVN.RULEDENIED;
            resultMessage = Label.No_mapping_found_for_Assure_codes_MVN + returnCodes;
        } else if (denied) {
            resultWrapper.result = EligibilityEngineMVN.RULEDENIED;
        } else if (missingInformation) {
            resultWrapper.result = EligibilityEngineMVN.RULEMISSINGINFO;
        } else {
            resultWrapper.result = EligibilityEngineMVN.RULESUCCESS;
        }
        resultWrapper.resultDescription = resultMessage.removeEnd(';');
        return resultWrapper;
    }

    /**
     * Inner wrapper class to manage Assure Web Service response
     */ 
    public class AssureResponseWrapper {
        public String result { get; set; }
        public String resultDescription { get; set; }
    }

    /**
     * Check if all mandatory data is filled before calling the web service
     * @param  programMember Program Member with the physician information needed to call Assure 
     * @param  addressList   List of primary addresses for the physician
     * @param  DEASchedule   DEA Schedule for product
     * @return               list of missing fields
     */
    public List<String> getMissingFields(Program_Member_MVN__c programMember, 
                                            List<Address_vod__c> addressList,
                                            String DEASchedule) {

        List<String> missingFields = new List<String>();
        Map<String, Schema.SObjectField> accountFieldsMap = 
            Account.getSObjectType().getDescribe().fields.getMap();
        Map<String, Schema.SObjectField> addressFieldsMap = 
            Address_vod__c.getSObjectType().getDescribe().fields.getMap();
        Map<String, Schema.SObjectField> programFieldsMap = 
            Program_MVN__c.getSObjectType().getDescribe().fields.getMap();

        if (String.isBlank(programMember.Program_MVN__r.Job_No_MVN__c)) {
           missingFields.add(programFieldsMap.get('Job_No_MVN__c').getDescribe().getLabel());
        }
        if (String.isBlank(programMember.Physician_MVN__r.FirstName)){
            //missingFields.add(accountFieldsMap.get('FirstName').getDescribe().getLabel());
            missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_First_Name_Field_JKC);
        }
        if (String.isBlank(programMember.Physician_MVN__r.LastName)){
          // missingFields.add(accountFieldsMap.get('LastName').getDescribe().getLabel());
             missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_Last_Name_Field_JKC);
        }
        if (String.isBlank(programMember.Physician_MVN__r.Credentials_vod__c)) {
           // missingFields.add(accountFieldsMap.get('Credentials_vod__c').getDescribe().getLabel());
             missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_Credentials_Field_JKC);
        }
        if (addressList.isEmpty()) {
            //missingFields.add(SObjectType.Address_vod__c.getLabel());
            missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_Address_Field_JKC);
        }
        if (!addressList.isEmpty() && String.isBlank(addressList.get(0).Name)) {
           //missingFields.add(addressFieldsMap.get('Name').getDescribe().getLabel());
            missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_Address_Field_JKC);
        }
        if (!addressList.isEmpty() && String.isBlank(addressList.get(0).City_vod__c)) {
           //missingFields.add(addressFieldsMap.get('City_vod__c').getDescribe().getLabel());
            missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_City_Field_JKC);
        }
        if (!addressList.isEmpty() && String.isBlank(addressList.get(0).State_vod__c)) {
           // missingFields.add(addressFieldsMap.get('State_vod__c').getDescribe().getLabel());
            missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_State_Field_JKC);
        }
        if (!addressList.isEmpty() && String.isBlank(addressList.get(0).Zip_vod__c)) {
            //missingFields.add(addressFieldsMap.get('Zip_vod__c').getDescribe().getLabel());
            missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_Postal_Code_Field_JKC);
        }
        if ((this.validationType == 'SLN Order' || this.validationType == 'SLN + DEA Order') && 
            String.isBlank(programMember.Physician_MVN__r.SLN_MVN__c)) { 
           // missingFields.add(accountFieldsMap.get('SLN_MVN__c').getDescribe().getLabel());
            missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_SLN_Field_JKC);
        }
        if ((this.validationType == 'DEA Order' || this.validationType == 'SLN + DEA Order') && 
                String.isBlank(programMember.Physician_MVN__r.DEA_MVN__c)) {
           // missingFields.add(accountFieldsMap.get('DEA_MVN__c').getDescribe().getLabel());
            missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_DEA_Field_JKC);
        }
        if ((this.validationType == 'DEA Order' || this.validationType == 'SLN + DEA Order') && 
                String.isBlank(DEASchedule)) {
          //missingFields.add('DEA Schedule') ;
          missingFields.add(' ' + System.Label.Missing_Assure_Mandatory_DEA_Field_JKC);
            
        }
        return missingFields;
    }
}