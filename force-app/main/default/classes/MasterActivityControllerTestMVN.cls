/**
 * @author Mavens
 * @description Serves as the test class for MasterActivityControllerTestMVN, ProgramActivityComponentControllerMVN, ProgramActivityDetailControllerMVN and ProgramActivityRelatedRecController.
 * @group MasterActivity
 */

@isTest
private class MasterActivityControllerTestMVN {
    static MasterActivityControllerMVN masterController;
    static Program_Activity_MVN__mdt activity;
    static List<Program_Activity_Component_MVN__mdt> components;

    static Map<String, Schema.SObjectType> globalDescribe;

    static TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
    static TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();
    static TestFactoryCaseMVN caseFactory = new TestFactoryCaseMVN();

    static Case testCase;
    static Id recordTypeId;
    static Product_MVN__c product;

    static {
        activity = [SELECT Program_External_Id_MVN__c FROM Program_Activity_MVN__mdt WHERE DeveloperName != 'Test' LIMIT 1];
        System.debug('### Activity: ' + activity);
        components = [SELECT Id, DeveloperName, Edit_Modal_Title_MVN__c, MasterLabel, Language, NamespacePrefix, Label, QualifiedApiName, Program_Activity_Page_MVN__c, Program_Member_Id_Field_MVN__c, Record_Type_DeveloperName_MVN__c, Title_MVN__c, SObject_MVN__c, Field_Set_MVN__c, Mode_MVN__c, Component_Type_MVN__c, Component_Sequence_Number_MVN__c FROM Program_Activity_Component_MVN__mdt WHERE Program_Activity_Page_MVN__r.Program_Activity_MVN__c = :activity.Id];

        List<Program_MVN__c> programs = [SELECT Id FROM Program_MVN__c WHERE Program_ID_MVN__c = :activity.Program_External_Id_MVN__c];
        Program_MVN__c program;

        if(programs.isEmpty()) {
            program = programFactory.create(new Map<String, Object>{
                'Active_MVN__c' => true,
                'Cloned_MVN__c' => true,
                'Country_MVN__c' => 'US',
                'Description_MVN__c' => 'Cholecap Care',
                'Name' => 'Cholecap Care',
                'Program_ID_MVN__c' => activity.Program_External_Id_MVN__c
                });
        } else {
            program = programs.get(0);
        }
        product = new Product_MVN__c(
            Program_MVN__c = program.Id,
            Product_Status_MVN__c = 'Active');
        insert product;

        Account prescriber = accountFactory.createPrescriber();
        List<Account> patient = accountFactory.createPatients(1);
        Program_Member_MVN__c pm = programMemberFactory.create(program,prescriber,patient)[0];
        Application_MVN__c activeApplication = new Application_MVN__c(
            Program_Member_MVN__c = pm.Id
        );
        insert activeApplication;
        testCase = caseFactory.create(new Map<String, Object>{
            'AccountId' => patient[0].Id,
            'Description' => 'test',
            'Program_MVN__c' => program.Id,
            'Program_Member_MVN__c' => pm.Id
        });

        globalDescribe = Schema.getGlobalDescribe();

        List<SObject> recordsToInsert = new List<SObject>();
        List<SObject> recordsToUpdate = new List<SObject>();
        Boolean incomeCreatedAlready = false;
        Product_MVN__c product = new Product_MVN__c(
            Program_MVN__c = program.Id,
            Product_Status_MVN__c = 'Active');
        insert product;
        for(Program_Activity_Component_MVN__mdt component : components) {
            Schema.SobjectType objectType = globalDescribe.get(component.SObject_MVN__c);
            if (objectType == null) {
                continue;
            }
            if (component.SObject_MVN__c == 'Income_MVN__c') {
                if (incomeCreatedAlready) {
                    continue;
                } else {
                    incomeCreatedAlready = true;
                }
            }
            SObject record = objectType.newSObject();
            if (component.SObject_MVN__c == 'Prescription_MVN__c') {
                record.put('Product_MVN__c', product.Id);
                record.put('Application_MVN__c', activeApplication.Id); 
            }
            record = buildObjectFields(record,objectType,component.Field_Set_MVN__c);
            record.put(component.Program_Member_Id_Field_MVN__c, pm.Id);

            if (component.Record_Type_DeveloperName_MVN__c != null) {
                recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :component.Record_Type_DeveloperName_MVN__c LIMIT 1].Id;
                record.put('RecordTypeId',RecordTypeId);
            }

            if(component.Program_Member_Id_Field_MVN__c.toLowerCase() == 'id') {
                recordsToUpdate.add(record);
            } else {
                recordsToInsert.add(record);
            }
        }
        if(!recordsToInsert.isEmpty()) {
            System.debug('### Records to insert: ' + recordsToInsert);
            insert recordsToInsert;
        }
        if(!recordsToUpdate.isEmpty()) {
            update recordsToUpdate;
        }

        Test.setCurrentPage(Page.MasterActivityMVN);
        System.CurrentPageReference().getParameters().put('caseId', testCase.Id);
        masterController = new MasterActivityControllerMVN();

    }

    @isTest
    static void cancelShouldClearActivity() {
        selectTestActivity();

        ProgramActivityDetailControllerMVN detailController;
        ProgramActivityRelatedRecControllerMVN relatedRecordController;
        Schema.SobjectType objectType;

        // simulate instantiating controllers add any customer components that are built here
        for(Program_Activity_Component_MVN__mdt component : components) {
            if(component.Component_Type_MVN__c == 'Detail') {
                detailController = callDetailComponent(component);
                objectType = globalDescribe.get(component.SObject_MVN__c);
            } else if (component.Component_Type_MVN__c == 'Related_Råecord') {
                relatedRecordController = callRelatedRecordComponent(component);
            }
        }

        testPageButtonBooleans();

        if(masterController.showCancel) {
            masterController.cancel();
        }

        System.assertEquals(null,masterController.selectedActivity);
        System.assert(!masterController.activityNavigationDisabled);
    }
/*
    @isTest
    static void detailControllerShouldInsert() {
        selectTestActivity();

        ProgramActivityDetailControllerMVN detailController;
        ProgramActivityRelatedRecControllerMVN relatedRecordController;
        Schema.SobjectType objectType;
        Integer numberOfInserts = 0;

        // simulate instantiating controllers add any customer components that are built here
        for(Program_Activity_Component_MVN__mdt component : components) {
            if(component.Component_Type_MVN__c == 'Detail') {
                detailController = callDetailComponent(component);
                objectType = globalDescribe.get(component.SObject_MVN__c);
            } else if (component.Component_Type_MVN__c == 'Related_Record') {
                relatedRecordController = callRelatedRecordComponent(component);
            }
        }

        List<SObject> componentViewRecords = Database.query(detailController.getFieldSetSOQL());

        if(!detailController.viewMode) {
            detailController.record = buildObjectFields(detailController.record,objectType,detailController.componentRecord.Field_Set_MVN__c);

            // do an individual save if allowed
            if(detailController.showSave) {
                detailController.saveRecord();
                detailController.record = buildObjectFields(detailController.record,objectType,detailController.componentRecord.Field_Set_MVN__c);
                numberOfInserts++;
            }

            testPageButtonBooleans();

            // finish or go to the next page
            if(masterController.showFinish) {
                masterController.finish();
                numberOfInserts++;
                System.assertEquals(null,masterController.selectedActivity);
                System.assert(!masterController.activityNavigationDisabled);
                testPageButtonBooleans();
            } else if(masterController.showNext) {
                masterController.next();
                masterController.previous();
                masterController.next();
                numberOfInserts++;
                System.assert(masterController.activityNavigationDisabled);
                testPageButtonBooleans();
                masterController.saveComponentRecords();
                masterController.returnKeyPress();
                masterController.finish();
            }
        }

        List<SObject> postSaveComponentRecords = Database.query(detailController.getFieldSetSOQL());

        // make sure record was saved
        System.assert(componentViewRecords.size() + numberOfInserts >= postSaveComponentRecords.size());
    }
*/
    @isTest
    static void relatedRecordControllerShouldUpdate() {
        selectTestActivity();

        ProgramActivityDetailControllerMVN detailController;
        ProgramActivityRelatedRecControllerMVN relatedRecordController;
        Schema.SobjectType objectType;

        // simulate instantiating controllers add any customer components that are built here
        for(Program_Activity_Component_MVN__mdt component : components) {
            if(component.Component_Type_MVN__c == 'Detail') {
                detailController = callDetailComponent(component);
            } else if (component.Component_Type_MVN__c == 'Related_Record') {
                relatedRecordController = callRelatedRecordComponent(component);
                objectType = globalDescribe.get(component.SObject_MVN__c);
            }
        }

        if (relatedRecordController != null) {
            List<SObject> componentRelatedRecords = Database.query(relatedRecordController.getFieldSetSOQL());
            System.assertEquals(componentRelatedRecords.size(), relatedRecordController.records.size());

            System.assertNotEquals(relatedRecordController.relatedRecord,null);
            relatedRecordController.relatedRecordId = relatedRecordController.records[0].Id;
            relatedRecordController.findRelatedRecord();
            relatedRecordController.relatedRecord = buildObjectFields(relatedRecordController.relatedRecord,objectType,relatedRecordController.componentRecord.Field_Set_MVN__c);
            relatedRecordController.saveRelatedRecord();

            testPageButtonBooleans();

            System.assert(!relatedRecordController.hasErrors);
        }
    }

    @isTest
    static void relatedRecordControllerShouldFail() {
        selectTestActivity();

        ProgramActivityDetailControllerMVN detailController;
        ProgramActivityRelatedRecControllerMVN relatedRecordController;
        Schema.SobjectType objectType;

        // simulate instantiating controllers add any customer components that are built here
        for(Program_Activity_Component_MVN__mdt component : components) {
            if(component.Component_Type_MVN__c == 'Detail') {
                detailController = callDetailComponent(component);
            } else if (component.Component_Type_MVN__c == 'Related_Record') {
                relatedRecordController = callRelatedRecordComponent(component);
                objectType = globalDescribe.get(component.SObject_MVN__c);
            }
        }

        if (relatedRecordController != null) {
            List<SObject> componentRelatedRecords = Database.query(relatedRecordController.getFieldSetSOQL());
            System.assertEquals(componentRelatedRecords.size(), relatedRecordController.records.size());

            System.assertNotEquals(relatedRecordController.relatedRecord,null);
            relatedRecordController.relatedRecordId = relatedRecordController.records[0].Id;
            relatedRecordController.saveRelatedRecord();

            testPageButtonBooleans();

            System.assert(relatedRecordController.hasErrors);
        }
    }

    @isTest
    static void testAddPageMessageErrors() {
        MasterActivityControllerMVN.SObjectValidationWrapper fieldValidationWrapper = new MasterActivityControllerMVN.SObjectValidationWrapper(components.get(0), new Account());
        masterController.addPageMessageErrors(fieldValidationWrapper);
    }

    static String firstPicklistValue (Schema.SObjectType objectType, String field) {
        // broken down step by step for sanity
        Schema.DescribeSObjectResult describeSObject = objectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = describeSObject.fields.getMap();
        Schema.DescribeFieldResult fieldDescribe = fieldMap.get(field).getDescribe();
        String result = fieldDescribe.getPicklistValues()[0].getValue();
        return result;
    }

    static ProgramActivityDetailControllerMVN callDetailComponent(Program_Activity_Component_MVN__mdt componentRecord) {
        ProgramActivityDetailControllerMVN result = new ProgramActivityDetailControllerMVN();
        result.componentRecord = componentRecord; // this is set first in the component
        result.masterController = masterController.getThis();
        return result;
    }

    static ProgramActivityRelatedRecControllerMVN callRelatedRecordComponent(Program_Activity_Component_MVN__mdt componentRecord) {
        ProgramActivityRelatedRecControllerMVN result = new ProgramActivityRelatedRecControllerMVN();
        result.componentRecord = componentRecord; // this is set first in the component
        result.masterController = masterController.getThis();
        return result;
    }

    static void selectTestActivity() {
        masterController.selectedActivity = activity.Id;
        masterController.activityChanged();
        System.assertEquals(activity.Id, masterController.selectedActivity);
    }

    static String getTestSOQL(Program_Activity_Component_MVN__mdt componentRecord) {
        String[] fields = new String[0];
        FieldSet componentFieldSet = globalDescribe.get(componentRecord.SObject_MVN__c).getDescribe().fieldSets.getMap().get(componentRecord.Field_Set_MVN__c);

        for(FieldSetMember field: componentFieldSet.getFields()) {
            fields.add(field.getFieldPath());
        }

        String result = 'SELECT LastModifiedDate ' +
                             ', ' + String.join(fields,', ') +
                         ' FROM ' + String.valueOf(componentFieldSet.getSObjectType()) +
                        ' WHERE ' + componentRecord.Program_Member_Id_Field_MVN__c + ' = \'' +  String.escapeSingleQuotes(masterController.programMember.Id) + '\'';
        if (componentRecord.Record_Type_DeveloperName_MVN__c != null) {
            recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :componentRecord.Record_Type_DeveloperName_MVN__c LIMIT 1].Id;
            result += 'AND RecordTypeId = \'' + String.escapeSingleQuotes(recordTypeId) + '\'';
        }
        result += ' ORDER BY LastModifiedDate DESC';

        return result;
    }

    static SObject buildObjectFields(SObject record, Schema.SobjectType objectType, String fieldSet) {
        FieldSet objectFS = objectType.getDescribe().fieldSets.getMap().get(fieldSet);
        if (objectFS == null ) {
            return record;
        }

        for(FieldSetMember field : objectFS.getFields()) {
            if(!objectType.getDescribe().fields.getMap().get(field.getFieldPath()).getDescribe().isCreateable()) {
                continue;
            }
            if(field.getType() == Schema.DisplayType.Boolean) {
                record.put(field.getFieldPath(),true);
            } else if(field.getType() == Schema.DisplayType.Date) {
                record.put(field.getFieldPath(),Date.today());
            } else if(field.getType() == Schema.DisplayType.DateTime) {
                record.put(field.getFieldPath(),DateTime.now());
            } else if(field.getType() == Schema.DisplayType.Double || field.getType() == Schema.DisplayType.Integer || field.getType() == Schema.DisplayType.Percent) {
                record.put(field.getFieldPath(),1);
            } else if(field.getType() == Schema.DisplayType.String || field.getType() == Schema.DisplayType.TextArea) {
                record.put(field.getFieldPath(),'Te');
            } else if(field.getType() == Schema.DisplayType.Picklist) {
                record.put(field.getFieldPath(),firstPicklistValue(objectType, field.getFieldPath()));
            }
        }

        return record;
    }

    static void testPageButtonBooleans() {
        if(masterController.currentPage > 0 && masterController.selectedActivity != null) {
            System.assertEquals(true,masterController.showPrevious);
        } else {
            System.assertEquals(false,masterController.showPrevious);
        }

        if(masterController.currentPage != masterController.numOfPages) {
            System.assertEquals(true,masterController.showNext);
        } else {
            System.assertEquals(false,masterController.showNext);
        }

        if(masterController.selectedActivity != null && masterController.currentPage == masterController.numOfPages) {
            System.assertEquals(true, masterController.showFinish);
        } else {
            System.assertEquals(false, masterController.showFinish);
        }

        if(masterController.selectedActivity != null && masterController.currentPage == 0) {
            System.assertEquals(true,masterController.showCancel);
        } else {
            System.assertEquals(false,masterController.showCancel);
        }
    }
}