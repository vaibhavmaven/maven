@IsTest
public class RTBIResponseTest {
	
	// This test method should give 100% coverage
	static testMethod void testParse() {
		String json = '{'+
		'        \"PatientSpecificPricingCoverages\": ['+
		'            {'+
		'                \"Pharmacy\": {'+
		'                    \"Type\": \"Mail\",'+
		'                    \"NCPDPID\": \"2323239\",'+
		'                    \"NPI\": \"1538460530\",'+
		'                    \"PharmacyName\": \"Mail Order Pharmacy 10.6MU\",'+
		'                    \"AddressLine1\": \"9292 Langley Rd\",'+
		'                    \"AddressLine2\": \"Suite 100\",'+
		'                    \"City\": \"Phoenix\",'+
		'                    \"State\": \"AZ\",'+
		'                    \"Zip\": \"85001\"'+
		'                },'+
		'                \"PricingCoverage\": {'+
		'                    \"DrugStatus\": \"CoveredWithRestrictions\",'+
		'                    \"Message\": \"\",'+
		'                    \"QuantityPriced\": \"120\",'+
		'                    \"QuantityUnitDescription\": \"Tablet\",'+
		'                    \"DaysSupplyPriced\": \"30\",'+
		'                    \"PlanPayAmount\": \"193\",'+
		'                    \"EstimatedPatientPayAmount\": \"20\",'+
		'                    \"PriorAuthorizationRequired\": \"\",'+
		'                    \"OOPAppliedAmount\": \"\",'+
		'                    \"OOPRemainingAmount\": \"\",'+
		'                    \"DeductibleAppliedAmount\": \"0\",'+
		'                    \"DeductibleRemainingAmount\": \"0\",'+
		'                    \"FormularyStatus\": \"\"'+
		'                }, '+
		'				\"CoverageAlerts\": [{'+
		'					\"ReferenceCode\": \"QuantityLimits - 30 per 30 Days\",'+
		'					\"TextMessage\": \"TextMessage - THIS DRUG REQUIRES A NEBULIZER\",'+
		'					\"ReferenceText\": \"ResourceLink - www.xyz.com\"'+
		'			    }]'+
		'            }'+
		'        ]'+
		'}';
		RTBIResponse r = RTBIResponse.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		RTBIResponse.PricingCoverage objPricingCoverage = new RTBIResponse.PricingCoverage(System.JSON.createParser(json));
		System.assert(objPricingCoverage != null);
		System.assert(objPricingCoverage.DrugStatus == '');
		System.assert(objPricingCoverage.Message == '');
		System.assert(objPricingCoverage.QuantityPriced == '');
		System.assert(objPricingCoverage.QuantityUnitDescription == '');
		System.assert(objPricingCoverage.DaysSupplyPriced == '');
		System.assert(objPricingCoverage.PlanPayAmount == '');
		System.assert(objPricingCoverage.EstimatedPatientPayAmount == '');
		System.assert(objPricingCoverage.PriorAuthorizationRequired == '');
		System.assert(objPricingCoverage.OOPAppliedAmount == '');
		System.assert(objPricingCoverage.OOPRemainingAmount == '');
		System.assert(objPricingCoverage.DeductibleAppliedAmount == '');
		System.assert(objPricingCoverage.DeductibleRemainingAmount == '');
		System.assert(objPricingCoverage.FormularyStatus == '');

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		RTBIResponse.PatientSpecificPricingCoverages objPatientSpecificPricingCoverages = new RTBIResponse.PatientSpecificPricingCoverages(System.JSON.createParser(json));
		System.assert(objPatientSpecificPricingCoverages != null);
		System.assert(objPatientSpecificPricingCoverages.Pharmacy == null);
		System.assert(objPatientSpecificPricingCoverages.PricingCoverage == null);
		//System.assert(objPatientSpecificPricingCoverages.CoverageAlerts == null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		RTBIResponse.Pharmacy objPharmacy = new RTBIResponse.Pharmacy(System.JSON.createParser(json));
		System.assert(objPharmacy != null);
		System.assert(objPharmacy.Type_Z == '');
		System.assert(objPharmacy.NCPDPID == '');
		System.assert(objPharmacy.NPI == '');
		System.assert(objPharmacy.PharmacyName == '');
		System.assert(objPharmacy.AddressLine1 == '');
		System.assert(objPharmacy.AddressLine2 == '');
		System.assert(objPharmacy.City == '');
		System.assert(objPharmacy.State == '');
		System.assert(objPharmacy.Zip == '');

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		RTBIResponse.CoverageAlerts objCoverageAlerts = new RTBIResponse.CoverageAlerts(System.JSON.createParser(json));
		System.assert(objCoverageAlerts != null);
		System.assert(objCoverageAlerts.ReferenceCode == '');
		System.assert(objCoverageAlerts.TextMessage == '');
		System.assert(objCoverageAlerts.ReferenceText == '');

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		RTBIResponse objRTBIResponse = new RTBIResponse(System.JSON.createParser(json));
		System.assert(objRTBIResponse != null);
		System.assert(objRTBIResponse.PatientSpecificPricingCoverages == null);
	}
}