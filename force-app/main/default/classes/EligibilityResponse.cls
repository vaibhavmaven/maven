public class EligibilityResponse {
    public String PlanName {get; set;}
	public String Formulary {get;set;} 
	public String RetailPharmacy {get;set;} 
	public String MailOrderPrescriptionDrug {get;set;} 
	public String SpecialtyPharmacy {get;set;} 
	public String StepTherapy {get;set;} 
	public String PriorAuthorization {get;set;} 
	public String StepMedication {get;set;} 
	public String BIN {get;set;} 
	public String PCN {get;set;} 
	public String GROUP_Z {get;set;} // in json: GROUP
	public String RTBI {get;set;} 
	public String PayerName {get;set;} 
	public String GROUPID {get;set;} 
	public String CardholderID {get;set;} 
	public String PBMMemberID {get;set;} 

	public EligibilityResponse(JSONParser parser) {
        initializeEligibilityResponse();
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'Formulary') {
						Formulary = parser.getText();
					} else if (text == 'RetailPharmacy') {
						RetailPharmacy = parser.getText();
					} else if (text == 'MailOrderPrescriptionDrug') {
						MailOrderPrescriptionDrug = parser.getText();
					} else if (text == 'SpecialtyPharmacy') {
						SpecialtyPharmacy = parser.getText();
					} else if (text == 'StepTherapy') {
						StepTherapy = parser.getText();
					} else if (text == 'PriorAuthorization') {
						PriorAuthorization = parser.getText();
					} else if (text == 'StepMedication') {
						StepMedication = parser.getText();
					} else if (text == 'BIN') {
						BIN = parser.getText();
					} else if (text == 'PCN') {
						PCN = parser.getText();
					} else if (text == 'GROUP') {
						GROUP_Z = parser.getText();
					} else if (text == 'RTBI') {
						RTBI = parser.getText();
					} else if (text == 'PayerName') {
						PayerName = parser.getText();
					} else if (text == 'GROUPID') {
						GROUPID = parser.getText();
					} else if (text == 'CardholderID') {
						CardholderID = parser.getText();
					} else if (text == 'PBMMemberID') {
						PBMMemberID = parser.getText();
					} else {
						System.debug(LoggingLevel.WARN, 'EligibilityResponse consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
    
    private void initializeEligibilityResponse(){
        PlanName = '';
        Formulary = '';
        RetailPharmacy = '';
        MailOrderPrescriptionDrug = '';
        SpecialtyPharmacy = '';
        StepTherapy = '';
        PriorAuthorization = '';
        StepMedication = '';
        BIN = '';
        PCN = '';
        GROUP_Z = '';
        RTBI = '';
        PayerName = '';
        GROUPID = '';
        CardholderID = '';
        PBMMemberID = '';
    }
	
	
	public static EligibilityResponse parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new EligibilityResponse(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
}