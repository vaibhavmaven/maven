/**
 * AccountTypeSettingsMVN
 * @author Kyle Thornton
 * @createdDate May, 2016
 * @description returns account type settings based on country. if no record exists for rec type for country
 *              then the default record type is returned.
 */

public with sharing class AccountTypeSettingsMVN {

    public List<Account_Type_Setting_MVN__mdt> allSettings;

    public Set<Id> allowReferredByIds {
        get{
            if(allowReferredByIds == null){
                allowReferredByIds = new Set<Id>();

                for(Account_Type_Setting_MVN__mdt setting : allSettings){
                    if(Boolean.valueOf(setting.Allow_Referred_By_MVN__c)){
                        allowReferredByIds.add(UtilitiesMVN.getRecordTypesForObject('Account').get(setting.Account_Record_Type_MVN__c).Id);
                    }
                }
            }
            return allowReferredByIds;
        }
        private set;
    }

    private static List<Account_Type_Setting_MVN__mdt> testMock;

    public AccountTypeSettingsMVN(String country) {
        List<Account_Type_Setting_MVN__mdt> globalAndCountrySettings = getOrderedSettingsFor(country);

        allSettings = extractSingleSettings(globalAndCountrySettings);

        System.debug('### all Settings: ' + allSettings);
    }

    @TestVisible
    private static void setMock(List<Account_Type_Setting_MVN__mdt> mock){
        testMock = mock;
    }

    private List<Account_Type_Setting_MVN__mdt> getOrderedSettingsFor(String country) {
        Set<Id> metadataSegmentIds = SegmentsMVN.filterSegments(country);
        String queryString = getQueryString();

        List<Account_Type_Setting_MVN__mdt> settings = Database.query(queryString);

        if (testMock != null) {
            settings = testMock;
        }

        //can't order custom metadata via a relationship field and can't group by either so create groups of settings
        //by account search order then order each of those groups by the segment information
        List<List<Account_Type_Setting_MVN__mdt>> settingsByAccountSearchOrder = new List<List<Account_Type_Setting_MVN__mdt>>();
        List<Account_Type_Setting_MVN__mdt> currentSettingGroup = new List<Account_Type_Setting_MVN__mdt>();
        Decimal priorSortOrder = null;

        for (Account_Type_Setting_MVN__mdt setting : settings) {
            Decimal sortOrder = setting.Account_Search_Order_MVN__c;
            if (sortOrder == priorSortOrder){
                currentSettingGroup.add(setting);
            } else {
                settingsByAccountSearchOrder.add(currentSettingGroup);
                currentSettingGroup = new List<Account_Type_Setting_MVN__mdt>{setting};
            }
            priorSortOrder = sortOrder;
        }
        //add the last group
        settingsByAccountSearchOrder.add(currentSettingGroup);

        List<Account_Type_Setting_MVN__mdt> finalSettings = new List<Account_Type_Setting_MVN__mdt>();
        for (List<Account_Type_Setting_MVN__mdt> settingList : settingsByAccountSearchOrder) {
            finalSettings.addAll((List<Account_Type_Setting_MVN__mdt>)SegmentsMVN.sortMetadataBySegment(settingList) );
        }
        return finalSettings;
    }

    private String getQueryString() {
        Map<String, Schema.SObjectField> acctTypeSettingsFMap;
        acctTypeSettingsFMap = Account_Type_Setting_MVN__mdt.sObjectType
                                                            .getDescribe()
                                                            .Fields.getMap();

        List<String> acctSettingFields = new List<String>(acctTypeSettingsFMap.keySet());

        String queryFields = String.join(acctSettingFields, ',');
        String queryFormat = 'SELECT {0} FROM Account_Type_Setting_MVN__mdt WHERE Segment_MVN__c IN :metadataSegmentIds ORDER BY Account_Search_Order_MVN__c ASC';

        return String.format(queryFormat, new List<String>{queryFields});
    }

    private List<Account_Type_Setting_MVN__mdt> extractSingleSettings(List<Account_Type_Setting_MVN__mdt> settings) {
        Map<String, Account_Type_Setting_MVN__mdt> singleSettings = new Map<String, Account_Type_Setting_MVN__mdt>();

        for (Account_Type_Setting_MVN__mdt setting : settings) {
            String recordType = setting.Account_Record_Type_MVN__c;
            if(recordType.toLowerCase().equals(UtilitiesMVN.ALL_PICKLIST_VALUE.toLowerCase())){
            	singleSettings.put(recordType + setting.Is_Person_Record_Type_MVN__c, setting);
        	} else {
            	singleSettings.put(recordType, setting);
            }
        }

        return singleSettings.values();
    }

    public Map<Id, Account_Type_Setting_MVN__mdt> settingsByRecTypeId {
        get {
            Map<String, RecordType> accountRecTypesByDevName = UtilitiesMVN.getRecordTypesForObject('Account');

            settingsByRecTypeId = new Map<Id, Account_Type_Setting_MVN__mdt>();
            for (Account_Type_Setting_MVN__mdt setting: allSettings) {
                if (setting.Account_Record_Type_MVN__c.toLowerCase() == UtilitiesMVN.ALL_PICKLIST_VALUE.toLowerCase()) {
                    continue;
                }

                try {
                    Id recTypeId = accountRecTypesByDevName.get(setting.Account_Record_Type_MVN__c).Id;
                    settingsByRecTypeId.put(recTypeId, setting);
                } catch(System.NullPointerException ex) {
                    System.debug('### Account record type not available: \n' + ex + '\n' + ex.getStackTraceString());
                }
            }

            return settingsByRecTypeId;
        }
        private set;
    }

    public List<SelectOption> getSelectOptions(Boolean isPersonSearch, Boolean searchable, Boolean createable) {
        Map<String, String> translations = getTranslations(isPersonSearch, searchable, createable);

        List<SelectOption> selectOptions = new List<SelectOption>();
        for (String recTypeId : translations.keySet()) {
            selectOptions.add(new SelectOption(recTypeId, translations.get(recTypeId)));
        }

        return selectOptions;
    }

    public Map<String, String> getTranslations(Boolean isPersonSearch, Boolean searchable, Boolean createable) {
        Map<String, Account_Type_Setting_MVN__mdt> settings = filterSettings(isPersonSearch);

        if (searchable != null && searchable) {
            settings = getSearchableSettings(settings);
        }

        if (createable != null && createable) {
            settings = getCreateableSettings(settings);
        }

        return translate(settings);
    }

    public Account_Type_Setting_MVN__mdt getDefaultForSearch() {
        for (Account_Type_Setting_MVN__mdt setting : allSettings) {
            if (setting.Default_for_Search_MVN__c) {
                return setting;
            }
        }
        return null;
    }

    private Map<String, Account_Type_Setting_MVN__mdt> filterSettings(Boolean returnPersonTypes) {
        Map<String, Account_Type_Setting_MVN__mdt> settings = new Map<String, Account_Type_Setting_MVN__mdt>();

        Map<String, RecordType> accountRecTypesByDevName = UtilitiesMVN.getRecordTypesForObject('Account');
        for (Account_Type_Setting_MVN__mdt setting : allSettings) {
            if (setting.Is_Person_Record_Type_MVN__c == returnPersonTypes) {
                RecordType recType = accountRecTypesByDevName.get(setting.Account_Record_Type_MVN__c);

                if (recType == null && setting.Account_Record_Type_MVN__c.toLowerCase() != UtilitiesMVN.ALL_PICKLIST_VALUE.toLowerCase()) {
                    throw new UtilitiesMVN.utilException('Bad Configuration: ' + setting.Account_Record_Type_MVN__c + ' is an invalid record type name for Account');
                }

                if (setting.Account_Record_Type_MVN__c.toLowerCase() == UtilitiesMVN.ALL_PICKLIST_VALUE.toLowerCase()) {
                    settings.put(UtilitiesMVN.ALL_PICKLIST_VALUE, setting);
                } else {
                    settings.put(recType.Id, setting);
                }
            }
        }

        return settings;
    }

    private Map<String, Account_Type_Setting_MVN__mdt> getSearchableSettings(Map<String, Account_Type_Setting_MVN__mdt> settings) {
        for (String key : settings.keySet()) {
            if (!settings.get(key).Include_in_Search_MVN__c) {
                settings.remove(key);
            }
        }
        return settings;
    }

    private Map<String, Account_Type_Setting_MVN__mdt> getCreateableSettings(Map<String, Account_Type_Setting_MVN__mdt> settings) {
        for (String key : settings.keySet()) {
            if (!settings.get(key).Include_in_Create_MVN__c) {
                settings.remove(key);
            }
        }
        return settings;
    }

    private Map<String, String> translate(Map<String, Account_Type_Setting_MVN__mdt> settings) {
        Map<String, String> recTypeTranslationById = new Map<String, String>();

        Map<Id, RecordTypeLocalization> rtlByParentId = UtilitiesMVN.recordTypeLocalizations;

        for (String settingId : settings.keySet()) {
            String value;
            if (settingId == UtilitiesMVN.ALL_PICKLIST_VALUE) {
                value = UtilitiesMVN.ALL_PICKLIST_VALUE;
            } else if (rtlByParentId.containsKey(settingId)) {
                value = rtlByParentId.get(settingId).Value;
            } else {
                value = UtilitiesMVN.getRecordTypesByIdForObject('Account').get(settingId).Name;
            }

            recTypeTranslationById.put(settingId, value);
        }
        return recTypeTranslationById;
    }
}