/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description implements Or rule
 */
public with sharing class EligibilityEngineOrHdlrMVN extends EligibilityEngineRuleHdlrMVN {
    /**
     * checks if rule is fired for Ors - if ONE of the child rules is passed - the OR rule is
     * passed
     * @return  Boolean     flag if rule is fired
     */
    private Boolean isRuleFired() {
        EligibilityEngineMVN engine = new EligibilityEngineMVN(super.requestedData,
                                                               super.rule.Child_Rules_MVN__r);
        List<Eligibility_Engine_Result_MVN__c> results = engine.run();
        for(Eligibility_Engine_Result_MVN__c childResult : results) {
            if(EligibilityEngineMVN.RULESUCCESS != childResult.Result_MVN__c) {
                return true;
            }
        }
        return false;
    }
}