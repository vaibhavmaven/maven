/**
 * DCRGlobalSettingMVN
 * Created By: Rick Locke
 * Created Date: November 2017
 * Description : Accessor class for the DCR Global Settings custom metadata specifically for DCR usage.
 */
public with sharing class DCRGlobalSettingMVN {

    public DCR_Global_Setting_MVN__mdt setting {get; private set;}

    private static DCR_Global_Setting_MVN__mdt testMock;

    public DCRGlobalSettingMVN() {
        if(testMock != null){
            setting = testMock;
        } else {
            Map<String, Schema.SObjectField> dcrSettingsFMap = DCR_Global_Setting_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
            List<String> dcrSettingFields = new List<String>(dcrSettingsFMap.keySet());

            String queryFields = String.join(dcrSettingFields, ',');
            String queryString = 'SELECT ' + String.escapeSingleQuotes(queryFields) + ' FROM DCR_Global_Setting_MVN__mdt LIMIT 1';
            setting = (DCR_Global_Setting_MVN__mdt) Database.query(queryString);
        }
    }

    @TestVisible
    private static void setMock(DCR_Global_Setting_MVN__mdt mock){
        testMock = mock;
    }

    public Set<String> excludedProfileNames {
        get{
            Set<String> response = new Set<String>();

            String excludedProfileNamesString = setting.DCR_Excluded_Profile_Names_MVN__c;// DCR Excluded Profile Names

            if(String.IsNotBlank(excludedProfileNamesString)) {
                List<String> excludedProfileNamesList = excludedProfileNamesString.split(',');

                if(excludedProfileNamesList != null && !excludedProfileNamesList.isEmpty()) {
                    for (String excludedProfileName : excludedProfileNamesList) {
                        response.add(excludedProfileName.normalizeSpace());
                    }
                }
            }

            return response;
        }
    }
}