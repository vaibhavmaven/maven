/**
* @author Mavens
* @date 07/10/2018 
* @description Class to provide factory methods to create test data for Prescription objects
*/
@isTest
public class TestFactoryPrescriptionMVN {

    private TestFactorySObjectMVN objectFactory;

    public TestFactoryPrescriptionMVN() {
        objectFactory = new TestFactorySObjectMVN('Prescription_MVN__c', new Map<String, Object>());
    }

    public Prescription_MVN__c construct(Map<String, Object> valuesByField){
        return (Prescription_MVN__c) objectFactory.constructSObject(valuesByField);
    }

    public Prescription_MVN__c create(Map<String, Object> valuesByField){
        return (Prescription_MVN__c) objectFactory.createSObject(valuesByField);
    }

    public List<Prescription_MVN__c> constructMany(Integer numOfRequests, Map<String,Object> valuesByField) {
        return (List<Prescription_MVN__c>) objectFactory.constructSObjects(numOfRequests, valuesByField);
    }

    public List<Prescription_MVN__c> createMany(Integer numOfRequests, Map<String,Object> valuesByField) {
        return (List<Prescription_MVN__c>) objectFactory.createSObjects(numOfRequests, valuesByField);
    }
}