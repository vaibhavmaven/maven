/*
 * MDMFieldMappingMVN
 * @created by: Pablo Roldan
 * @created Date: Oct 20, 2017
 * @description: Wrapper class to manage retrieving the MDM Fields custom metadata type
 */
public with sharing class MDMFieldMappingMVN {

    public List<MDM_Field_Mapping_MVN__mdt> settings { get; private set;}

    private static List<MDM_Field_Mapping_MVN__mdt> testMock;

    public MDMFieldMappingMVN(String mdmConnectionId) {
        Map<String, Schema.SObjectField> MDMFieldMappingMVN = MDM_Field_Mapping_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
        List<String> mdmFields = new List<String>(MDMFieldMappingMVN.keySet());

        String queryString = 'SELECT ' + String.escapeSingleQuotes(String.join(mdmFields, ','))
                            + ' FROM MDM_Field_Mapping_MVN__mdt'
                            + ' WHERE MDM_Connection_MVN__c = :mdmConnectionId';

        settings = Database.query(queryString);

        if(testMock != null){
            settings = testMock;
        }
    }

    @TestVisible
    private static void setMocks(List<MDM_Field_Mapping_MVN__mdt> mocks){
        testMock = mocks;
    }
}