/*
 * MDMFieldMappingTestMVN
 * @created by: Pablo Roldan
 * @created Date: Oct 20, 2017
 * @description: Test class for MDMFieldMappingMVN
 */
@isTest
private class MDMFieldMappingTestMVN {
    
    @isTest
    static void itShouldGetMDMFieldMappingConfigured() {
        TestFactoryCustomMetadataMVN.setMocks();

        List<MDM_Field_Mapping_MVN__mdt> mdmFieldMappings;
        Test.startTest();
        MDMConnectionsMVN mdmConnectionsCtrl = new MDMConnectionsMVN('ZZ');
        MDMFieldMappingMVN mdmFieldMappingCtrl = new MDMFieldMappingMVN((String) mdmConnectionsCtrl.setting.get('Id'));
        mdmFieldMappings = mdmFieldMappingCtrl.settings;
        Test.stopTest();

        System.assertNotEquals(null, mdmFieldMappings);
        System.assert(!mdmFieldMappings.isEmpty());

        for(MDM_Field_Mapping_MVN__mdt mdmFieldMapping : mdmFieldMappings) {
            String sfFieldName = (String) mdmFieldMapping.get('SF_Field_Name_MVN__c');
            String mdmFieldName = (String) mdmFieldMapping.get('MDM_Field_Name_MVN__c');

            System.assert(String.IsNotBlank(sfFieldName), mdmFieldMappings);
            System.assert(String.IsNotBlank(mdmFieldName), mdmFieldMappings);
        }
    }
}