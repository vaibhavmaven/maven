/**
 *  ProgramActivityComponentControllerMVN
 *  Created By:  Rick Locke
 *  Created On:  August 2017
 *  Description: This is a virtual classes used within the Program Activity process as an interface betweeen the Component Controllers and the MasterActivityControllerMVN.
 */
public virtual class ProgramActivityComponentControllerMVN {
    public MasterActivityControllerMVN masterController {
    	get;
        set {
            if (value != null){
                masterController = value;
	            if (componentRecord.Record_Type_DeveloperName_MVN__c != null) {
	                recordTypeId = [SELECT Id 
                                      FROM RecordType 
                                     WHERE DeveloperName = :componentRecord.Record_Type_DeveloperName_MVN__c 
                                     LIMIT 1].Id;
	                hasRecordType = true;
	            } else {
	            	hasRecordType = false;
	            }
                setObjectRecords();
                masterController.componentControllerMap.put(componentRecord.Id, this);
            }
        }
    }


	public Program_Activity_Component_MVN__mdt componentRecord {
        get;
        set {
            componentRecord = value;
        }
    }
	
    public Id recordTypeId {get;set;}
    public Boolean hasRecordType {
        get {
            if (hasRecordType != null) {
                return hasRecordType;
            } else if (hasRecordType == null && recordTypeId != null) {
                return true;
            } else {
                return false;
            }
            
        }
        set;
    }

	public FieldSet componentFieldSet {
		get {
			return globalDescribe.get(componentRecord.SObject_MVN__c).getDescribe().fieldSets.getMap().get(componentRecord.Field_Set_MVN__c);
		}
		set;
	}

    public Map<String, Schema.SObjectType> globalDescribe;

    /**
     * Constructor
     */
    public ProgramActivityComponentControllerMVN() {
    	globalDescribe = Schema.getGlobalDescribe();
    }

    public virtual void setObjectRecords(){}

    /**
     * Build the SOQL string to find the related record(s).
     * @return String
     */
    public String getFieldSetSOQL() {
        String[] fields = new String[0];
        for(FieldSetMember field: componentFieldSet.getFields()) {
            if(field.getFieldPath() != componentRecord.Program_Member_Id_Field_MVN__c) {
                fields.add(field.getFieldPath());
            }
        }

        String result = 'SELECT ' + componentRecord.Program_Member_Id_Field_MVN__c + 
                             ', ' + String.join(fields,', ') +
                         ' FROM ' + String.valueOf(componentFieldSet.getSObjectType()) + 
                        ' WHERE ' + componentRecord.Program_Member_Id_Field_MVN__c + ' = \'' +  String.escapeSingleQuotes(masterController.programMember.Id) + '\'';
        if (hasRecordType) {
            result += ' AND RecordTypeId = \'' + String.escapeSingleQuotes(recordTypeId) + '\'';
        }

        return result;
    }
}