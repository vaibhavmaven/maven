/**
 *  EnhancedAccountSearchResultMockTestMVN
 *  Created By:     Aggen
 *  Created On:     03/31/2016
 *  Description:    This is a test class for the EnhancedAccountSearchResultMockMVN
 *                  
 **/
@isTest
private class EnhancedAccountSearchResultMockTestMVN {
    static EnhancedAccountSearchControllerMVN stdController;    
    static Account patient;
    static Account payer;
    static Case cs;

    static {        
        TestDataFactoryMVN.createSearchSettings();
        TestDataFactoryMVN.createPSSettings();

        patient = TestDataFactoryMVN.createMember();
        payer = TestDataFactoryMVN.createPayer();
        cs = TestDataFactoryMVN.createTestCase();

        ApexPages.StandardController csController = new ApexPages.StandardController(cs);
        stdController = new EnhancedAccountSearchControllerMVN(csController);
    }

    @isTest static void testPersonAccountSearchWithResults() {      
        Program_MVN__c program = TestDataFactoryMVN.createProgram();
        Program_Member_MVN__c pm = TestDataFactoryMVN.createProgramMember(program, patient);

        Test.startTest();
            stdController.searchPersonAccount.LastName = 'Member';
            stdController.personAccountSearch();
        Test.stopTest();

        System.assert(stdController.getPersonAccountResultList().size() > 0, 'No person accounts found');
    }

    @isTest static void testBusinessAccountSearchWithResults() {        
        Test.startTest();
            stdController.searchBusinessAccount.Name = 'Blue Cross Blue Shield';
            stdController.businessAccountSearch();
        Test.stopTest();

        System.assert(stdController.getBusinessAccountResultList().size() == 1, 'No business accounts found');
    }

    @isTest
    static void testSortPersonAccounts() {
        Id patientRecordTypeId = RecordTypesMVN.getId(Account.SObjectType, 'Patient_MVN');

        List<Account> personAccounts = new List<Account>{
            new Account(FirstName = 'John', LastName = 'Doe', RecordTypeId = patientRecordTypeId),
            new Account(FirstName = 'Jane', LastName = 'Doe', RecordTypeId = patientRecordTypeId),
            new Account(FirstName = 'Ross', LastName = 'Doe', RecordTypeId = patientRecordTypeId),
            new Account(FirstName = 'Céline', LastName = 'Dion', RecordTypeId = patientRecordTypeId)
        };
        insert personAccounts;

        personAccounts = [SELECT FirstName, LastName, Name, IsPersonAccount, External_Id_vod__c, Network_Id_MVN__c FROM Account WHERE Id IN :personAccounts];

        List<EnhancedAccountSearchResultMockMVN> results = new List<EnhancedAccountSearchResultMockMVN>{
            new EnhancedAccountSearchResultMockMVN(personAccounts[0], null, null, false, null),
            new EnhancedAccountSearchResultMockMVN(personAccounts[1], null, null, false, null),
            new EnhancedAccountSearchResultMockMVN(personAccounts[2], null, null, false, null),
            new EnhancedAccountSearchResultMockMVN(personAccounts[3], null, null, false, null)
        };

        Test.startTest();
        results.sort();
        Test.stopTest();

        System.assertEquals('Céline', results[0].resultAcct.FirstName);
        System.assertEquals('Jane', results[1].resultAcct.FirstName);
        System.assertEquals('John', results[2].resultAcct.FirstName);
        System.assertEquals('Ross', results[3].resultAcct.FirstName);
    }

    @isTest
    static void testSortBusinessAccounts() {
        Id hospitalRecordTypeId = RecordTypesMVN.getId(Account.SObjectType, 'Hospital_vod');

        List<Account> businessAccounts = new List<Account>{
            new Account(Name = 'Hospital #2', RecordTypeId = hospitalRecordTypeId),
            new Account(Name = 'Hospital #4', RecordTypeId = hospitalRecordTypeId),
            new Account(Name = 'Hospital #1', RecordTypeId = hospitalRecordTypeId),
            new Account(Name = 'Hospital #3', RecordTypeId = hospitalRecordTypeId)
        };
        insert businessAccounts;

        businessAccounts = [SELECT FirstName, LastName, Name, IsPersonAccount, External_Id_vod__c, Network_Id_MVN__c FROM Account WHERE Id IN :businessAccounts];

        List<EnhancedAccountSearchResultMockMVN> results = new List<EnhancedAccountSearchResultMockMVN>{
            new EnhancedAccountSearchResultMockMVN(businessAccounts[0], null, null, false, null),
            new EnhancedAccountSearchResultMockMVN(businessAccounts[1], null, null, false, null),
            new EnhancedAccountSearchResultMockMVN(businessAccounts[2], null, null, false, null),
            new EnhancedAccountSearchResultMockMVN(businessAccounts[3], null, null, false, null)
        };

        Test.startTest();
        results.sort();
        Test.stopTest();

        System.assertEquals('Hospital #1', results[0].resultAcct.Name);
        System.assertEquals('Hospital #2', results[1].resultAcct.Name);
        System.assertEquals('Hospital #3', results[2].resultAcct.Name);
        System.assertEquals('Hospital #4', results[3].resultAcct.Name);
    }

    @isTest
    static void testSearchIncludingBirthDateCorrectClause() {

        Id patientRecordTypeId = RecordTypesMVN.getId(Account.SObjectType, 'Patient_MVN');

        List<Account> personAccounts = new List<Account>{
            new Account(FirstName = 'John', LastName = 'Doe', RecordTypeId = patientRecordTypeId, PersonBirthdate = Date.today())
        };
        insert personAccounts;

        Test.startTest();
            stdController.searchPersonAccount.PersonBirthdate = Date.today();
            stdController.searchPersonAccount.LastName = 'Doe';
            stdController.personAccountSearch();
        Test.stopTest();

        System.assert(stdController.getPersonAccountResultList().size() > 0, 'No person accounts found');
    }

}