/*
*   MedicalHistoryItemMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 14, 2016
*   Description:    Provides a wrapper for the medical history items to enable a 'state'
*/
public with sharing class MedicalHistoryItemMVN {
    public Medical_History_MVN__c item { get; set; }
    public Boolean isEditMode { get; set; }

    public MedicalHistoryItemMVN(Medical_History_MVN__c item) {
        this.item = item;
        this.isEditMode = false;

        if(this.item.Id == null) {
            setDefaultPicklistValues();
        }
    }

    public void changeEditMode() {
        this.isEditMode = !this.isEditMode;
    }

    private void setDefaultPicklistValues() {

        List<Schema.FieldSetMember> fieldSet = Schema.SObjectType.Medical_History_MVN__c.fieldSets.getMap().get(this.item.RecordType.DeveloperName).getFields();

        Map<String, Schema.SObjectField> fieldInfo = Schema.SObjectType.Medical_History_MVN__c.fields.getMap();

        for(Schema.FieldSetMember member : fieldSet) {
            if(member.getType() == Schema.DisplayType.Picklist) {
                String fieldPath = member.getFieldPath();
                for(Schema.PicklistEntry ple : fieldInfo.get(fieldPath).getDescribe().getPicklistValues()) {
                    if(ple.isDefaultValue()) {
                        this.item.put(fieldPath, ple.getValue());
                    }
                }
            }
        }
    }
}