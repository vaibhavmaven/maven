/*
 * NetworkAccountSearchHdlrTestMVN
 * @created by: Pablo Roldan
 * @created Date: Oct 9th, 2017
 * @description: This class tests NetworkAccountSearchHdlrMVN
 */
@isTest
private class NetworkAccountSearchHdlrTestMVN {
    static List<Account> accounts;
    static NetworkAccountSearchHdlrMVN handler;
    static AccountSearchRqstMVN request;
    static List<AccountSearchRsltMVN> results;
    static AccountSearchRsltMVN returnedResult;
    static User testUser;

    static TestFactoryUserMVN userFactory = new TestFactoryUserMVN();

    static {
        TestFactoryCustomMetadataMVN.setMocks();
        //override the default field settings that get returned
        TestFactoryAccountFieldSettingsMVN.setPersonSearchMock();

        testUser = userFactory.create();
        handler = new NetworkAccountSearchHdlrMVN();
    }

    @isTest
    static void itShouldHandleANameSearch() {
        System.runAs(testUser) {
            givenASerchRequestIsCreated();
            Test.startTest();
            whenALastNameSearchIsPerformed();
            Test.stopTest();
            thenTheCorrectNumberOfResultsAreReturned(1);
        }
    }

    @isTest
    static void itShouldHandleAAddressSearch() {
        System.runAs(testUser) {
            givenASerchRequestIsCreated();
            Test.startTest();
            whenAPhoneSearchIsPerformed();
            Test.stopTest();
            thenTheCorrectNumberOfResultsAreReturned(10);
        }
    }

    @isTest
    static void itShouldReturnTheAccountSearchResultWhenInsertAccountDetailsInterfaceMethodCalled() {
        System.runAs(testUser) {
            givenASerchRequestIsCreated();
            Test.startTest();
            whenALastNameSearchIsPerformed();
            whenInsertAccountDetailsIsCalled();
            Test.stopTest();
            thenReturnedResultIsSameAsPassedInResult();
        }
    }

    @isTest
    static void itShouldReturnAccountsByExternalId() {
        System.runAs(testUser) {
            givenASerchRequestIsCreated();
            givenAExternalIdsAreSet();
            Test.startTest();
            whenGetAccounsWithDetailsCalled();
            Test.stopTest();
            thenTheCorrectNumberOfResultsAreReturned(5);
        }
    }

    static void givenASerchRequestIsCreated() {
        AccountFieldSettingsMVN searchSettings = new AccountFieldSettingsMVN('US', 'HCP', 'Search_Field_Order_MVN__c', false);
        AccountFieldSettingsMVN resultSettings = new AccountFieldSettingsMVN('US', 'HCP', 'Search_Results_Order_MVN__c', false);
        request = new AccountSearchRqstMVN(searchSettings, resultSettings, 'US');
        request.acctTypeSettings = new AccountTypeSettingsMVN('US');
        request.isPersonSearch = true;
        request.recordType = 'All';
    }

    static void givenAExternalIdsAreSet() {
        request.accountExternalIds = new List<String>{'extId1','extId2','extId3','extId4','extId5'};
    }

    static void whenALastNameSearchIsPerformed() {
        request.account.LastName = 'Berry';
		Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockSearchSuccess());
		System.assert(!IntegrationTokensCacheMVN.toggleEnabled());
        results = handler.search(request);
    }

    static void whenAPhoneSearchIsPerformed() {
        request.address.Phone_vod__c = '5555555555';
		Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockSearchSuccess());
		System.assert(!IntegrationTokensCacheMVN.toggleEnabled());
        results = handler.search(request);
    }

    static void whenInsertAccountDetailsIsCalled() {
        returnedResult = handler.insertAccountDetails(results[0]);
    }

    static void whenGetAccounsWithDetailsCalled() {
		Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockSearchExternalIdSuccess());
		System.assert(!IntegrationTokensCacheMVN.toggleEnabled());
        results = handler.getAccountsWithDetails(request);
    }

    static void thenTheCorrectNumberOfResultsAreReturned(Integer correctNumber) {
        System.assertEquals(correctNumber, results.size());
    }

    static void thenReturnedResultIsSameAsPassedInResult() {
        System.assertEquals(results[0], returnedResult);
    }
}