/**
 * @author Knipper CC Admin 2/12/19
 * @Class that defines if App Status = Passed or Denied then call the Action Engine MISSINGINFOPASSEDACTION
 * @group Application
 */
public with sharing class MissingInfoPassedActionJKC implements TriggerHandlerMVN.HandlerInterface {
  
    
      public void handle() 
      {
        for (Id thisApplicationId : trigger.newMap.keySet()) 
        {
            if (this.missingInfoPassedChanged(thisApplicationId)) 
            {
                Application_MVN__c thisApplication = (Application_MVN__c) trigger.newMap.get(thisApplicationId);    
                (new ActionEngineMVN(thisApplication.Program_Member_MVN__c)).run(ActionEngineMVN.MISSINGINFOPASSEDACTION);   
                
            }
        }
    }


    private Boolean missingInfoPassedChanged(Id thisApplicationId) {
        Application_MVN__c oldApplication = (Application_MVN__c) trigger.oldMap.get(thisApplicationId);
        Application_MVN__c newApplication = (Application_MVN__c) trigger.newMap.get(thisApplicationId);
        return ((oldApplication.Status_MVN__c != newApplication.Status_MVN__c) && 
            ((newApplication.Status_MVN__c == 'Passed') || (newApplication.Status_MVN__c == 'Denied')));
    }
    
    
}