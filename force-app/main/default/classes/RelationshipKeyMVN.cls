/*
 *  CreateRelationshipsForProgramMembersMVN
 *  Created By:     Florian Hoehn
 *  Created Date:   June 13th 2017
 *  Description:    generates key for Relationship
 */
public class RelationshipKeyMVN {
    public class RelationshipKeyException extends Exception {}
    /**
    * @description generates key from relationship
    * @param Relationship_MVN__c relationship
    * @return String relationshipKey
    */
    public static String generate(Relationship_MVN__c relationship) {
        if(relationship.From_Account_MVN__c != null
                && relationship.To_Account_MVN__c != null) {
                //&& relationship.Program_Member_MVN__c != null) {
            return relationship.From_Account_MVN__c +
                   ':' + relationship.To_Account_MVN__c +
                   ':' + relationship.Program_Member_MVN__c;
        } else {
            throw new RelationshipKeyException(System.Label.Relationship_Key_Values_Blank_MVN);
        }
    }
}