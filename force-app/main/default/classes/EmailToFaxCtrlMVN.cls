/**
 * @author Mavens
 * @date 08/2018
 * @description Controller for the EmailToFaxMVN.page.
 *  Prompt the user to enter Fax Number, Subject and Attachment. 
 *  Send email to fax_number@srfax.com which processes it as a fax.
 */ 
public with sharing class EmailToFaxCtrlMVN {

    public String subject { get; set;}
    public String faxNumber { get; set;}
    public Boolean showMessage { get; set;}
    public List<SelectOption> attachmentOptions { get; set; }
    public List<String> selectedAttachments { get; set; }

    private Id caseId;
    private Case thisCase;

    /**
     * Constructor that initializes dropdown fields
     * @param  stdController standard controller for case
     */ 
    public EmailToFaxCtrlMVN() { 
        caseId = ApexPages.currentPage().getParameters().get('caseId');
        showMessage = false;
        thisCase = getCase();
        if (thisCase.Program_MVN__c == null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                Label.Send_Fax_Program_Missing_Error_MVN));
            showMessage = true;
        } else {
            initializeAttachmentList();
        }
    }

    /**
     * Retreive Case fields.
     * @return  Case record
     */ 
    private Case getCase() {
        return [
            SELECT 
                Id,
                Program_Member_MVN__c,
                Program_MVN__c,
                Program_MVN__r.Name,
                Program_MVN__r.Send_Fax_From_Email_MVN__c
            FROM
                Case
            WHERE 
                Id = :caseId
        ];
    }

    /**
     * Initialize dropdown list with available attachments to select
     */ 
    private void initializeAttachmentList() {
        List<Document_MVN__c> docList = getAttachments();
        buildAttachmentPicklistField(docList);
    }


    /**
     * Retrieve list of documents from:
     *    a) the program member if the Case has a program member linked to it
     *    b) the case if the case does NOT have a program member linked to it
     * @return list of documents
     */ 
    private List<Document_MVN__c> getAttachments() {
        List<Document_MVN__c> docList; 
        if (thisCase.Program_Member_MVN__c != null) {
            docList = [
                SELECT 
                    Attachment_Id_MVN__c,
                    Title_MVN__c,
                    Program_Member_MVN__r.Program_MVN__r.Send_Fax_From_Email_MVN__c
                FROM
                    Document_MVN__c
                WHERE 
                    Program_Member_MVN__c = :thisCase.Program_Member_MVN__c
            ];
        } else {
            docList = [
                SELECT 
                    Attachment_Id_MVN__c,
                    Title_MVN__c
                FROM
                    Document_MVN__c
                WHERE 
                    Case_MVN__c = :caseId
            ];
        }
        return docList;
    }

    /**
     * Build Picklist options for the attachment field
     * @param  docList list of documents to build the attachment picklist
     */
    private void buildAttachmentPicklistField(List<Document_MVN__c> docList) {
        attachmentOptions = new List<SelectOption>();
        for (Document_MVN__c doc : docList) {
            attachmentOptions.add(new SelectOption(doc.Attachment_Id_MVN__c, doc.Title_MVN__c));
        }
        if (docList.size() == 1) {
            selectedAttachments = new List<String>{docList.get(0).Attachment_Id_MVN__c};
        }
    }

    /**
     * send fax as an email to the fax number introduced by the user
     * @return success or error message
     */ 
    public PageReference sendFax() {

        if (!isEntryDataValid()) {
            return null;
        }

        try {

            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            if (String.isBlank(thisCase.Program_MVN__r.Send_Fax_From_Email_MVN__c)) {
                String label = String.format(System.Label.Send_Fax_Email_Missing_Error_MVN, 
                                new List<String>{thisCase.Program_MVN__r.Name});
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                    label));
                showMessage = true;
                return null;
            }

            OrgWideEmailAddress[] fromEmail = [
                SELECT 
                    Id 
                FROM 
                    OrgWideEmailAddress 
                WHERE 
                    Address = :thisCase.Program_MVN__r.Send_Fax_From_Email_MVN__c
            ];
            if ( fromEmail.size() > 0 ) {
                mail.setOrgWideEmailAddressId(fromEmail.get(0).Id);
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
                                    Label.Email_Configuration_Error_MVN));
                showMessage = true;
                return null;
            }

            mail.setSubject(subject);
            mail.setPlainTextBody('');
            mail.setBccSender(false);
            mail.setUseSignature(false);

            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            for (Attachment a : [select Name, Body, BodyLength from Attachment where Id IN :selectedAttachments]) {
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(a.Name);
                efa.setBody(a.Body);
                fileAttachments.add(efa);
            }
            mail.setFileAttachments(fileAttachments);

            // We are inserting this fake email and then deleting them because to use the setSaveAsActivity(true)
            // option we need to use setTargetObjectId.
            Contact c = new Contact(LastName='fake Contact', email=faxNumber + '@srfax.com');
            insert c;
            mail.setTargetObjectId(c.Id);
            mail.setWhatId(caseId);
            mail.setSaveAsActivity(true);

            Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            delete c;

            if (results.get(0).isSuccess()) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 
                    Label.Send_Fax_Success_Message_MVN));
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
                    results.get(0).getErrors().get(0).getMessage()));
            }
        } catch (Exception ex) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
                    ex.getMessage()));
        }
        showMessage = true;
        return null;
    }

    /**
     * Validate data entered by user
     * @return  boolean true if valid data, valse if not valid
     */
    private Boolean isEntryDataValid() {
        if (String.isBlank(faxNumber)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
            Label.Fax_Number_Missing_Field_Error_MVN));
        } else {
            String regex = '^[0-9]+$';
            Pattern regexPattern = Pattern.compile(regex);
            Matcher regexMatcher = regexPattern.matcher(faxNumber);
            if (!regexMatcher.matches()) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
                Label.Fax_Number_Invalid_Format_Error_MVN));
            }
        }
        if (String.isBlank(subject)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
            Label.Subject_Missing_Field_Error_MVN));
        }
        if (selectedAttachments == null || selectedAttachments.isEmpty()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 
            Label.Attachment_Missing_Field_Error_MVN));
        }
        if (ApexPages.hasMessages()) {
            return false;
        }
        return true;
    }

}