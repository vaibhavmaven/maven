/**
 * @author Mavens
 * @date Oct 2018
 * @description Tests for the trigger handler CreateCaseNintexAttachmentMVN
 */
@isTest
private class CreateCaseNintexAttachmentTestMVN {

    private static User caseManager;
    private static Case testCase;
    private static Id nintexDeliveryId;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        createNintexPackages();

        System.runAs(caseManager) {
            testCase = TestDataFactoryMVN.createTestCase();
        }
    }

    static void createNintexPackages() {
        Loop__DDP__c nintexPackage = new Loop__DDP__c(
            Name = 'test package',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'EligibilityLetters-Approval'
        );

        insert nintexPackage;

        Loop__DDP_Integration_Option__c ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Attach').getRecordTypeId();
        ddpOption.Loop__Attach_As__c = 'SFTP';
        insert ddpOption;
        nintexDeliveryId = ddpOption.Id;

        ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Attach').getRecordTypeId();
        ddpOption.Loop__Attach_As__c = 'Attachment';
        insert ddpOption;
    }

    @isTest
    private static void testGenerateAttachment() {
        testCase.Nintex_Delivery_Id_MVN__c = nintexDeliveryId;
        testCase.Nintex_Delivery_Datetime_MVN__c = Datetime.now();
        update testCase;
    }
}