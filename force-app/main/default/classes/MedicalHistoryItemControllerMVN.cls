/*
*   MedicalHistoryItemControllerMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 16, 2016
*   Description:    Provides controller functionality for the MedicalHistoryItemMVN component.
*/
public with sharing class MedicalHistoryItemControllerMVN {
    public transient Boolean error {get; set;}
    public MedicalHistoryItemMVN medicalHistory { get; set; }
    public List<Schema.FieldSetMember> fieldSet {
        get {
            try{
                return Schema.SObjectType.Medical_History_MVN__c.fieldSets.getMap().get(this.medicalHistory.item.RecordType.DeveloperName).getFields();
            } catch(NullPointerException nullEx) {
                if(this.medicalHistory != null && this.medicalHistory.item != null && this.medicalHistory.item.RecordType != null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.format(Label.MedicalHistory_NoFieldSetErrorMessage, new String[]{this.medicalHistory.item.RecordType.DeveloperName})));
                    return null; // show page messages in component
                }
                return new List<Schema.FieldSetMember>();
            }
        }
    }
    public List<Schema.FieldSetMember> readOnlyFieldSet {
        get {
            try{
                return Schema.SObjectType.Medical_History_MVN__c.fieldSets.getMap().get('readOnly').getFields();
            } catch(NullPointerException nullEx) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, String.format(Label.MedicalHistory_NoFieldSetErrorMessage, new String[]{'readOnly'})));
                return null; // show page messages in component
            }
        }
    }
    public Boolean isNumberOfFieldsOdd {
        get {
            return (System.Math.mod(this.fieldSet.size(), 2) == 1 ? true : false);
        }
    }

    public MedicalHistoryItemControllerMVN() {
        error = false;
    }

    public PageReference save() {
        error = false;

        try {
            upsert this.medicalHistory.item;
            this.medicalHistory.changeEditMode();

            UtilitiesMVN.medicalHistorySaveSuccess = true;
        } catch(Exception e) {
            error = true;
            ApexPages.addMessages(e);
        } 

        return null;
    }

    public PageReference cancel() {
        this.medicalHistory.changeEditMode();
        Set<String> fields = new Set<String>();
        for(Schema.FieldSetMember fieldSetMember : fieldSet) {
            fields.add(fieldSetMember.getFieldPath());
        }
        for(Schema.FieldSetMember fieldSetMember : readOnlyFieldSet) {
            fields.add(fieldSetMember.getFieldPath());
        }
        Id medicalHistoryItemId = medicalHistory.item.Id;
        this.medicalHistory.item = Database.query('SELECT RecordType.DeveloperName, ' + String.join(new List<String>(fields), ', ') +
                                                  ' FROM Medical_History_MVN__c' +
                                                  ' WHERE Id = :medicalHistoryItemId' +
                                                  ' LIMIT 1');
        return null;
    }

    public PageReference edit() {
        this.medicalHistory.changeEditMode();
        return null;
    }
}