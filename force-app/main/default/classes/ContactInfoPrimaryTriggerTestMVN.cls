/**
 *  ContactInfoPrimaryTriggerMVN
 *  Created By:     Paul Battisson 
 *  Created On:     2nd May 2017
 *  Description:    This test class tests the functionality of the ContactInfoPrimaryTriggerMVN               
 **/
@isTest
private class ContactInfoPrimaryTriggerTestMVN {

    private static Account patient;

    static {
        patient = TestDataFactoryMVN.createMember();
    }

    //Tests to verify the primary is set when we insert a new contact information record and none exist
    
    @isTest 
    private static void testInsertNewPrimarySetAccountValues_Email() {
        TestDataFactoryMVN.createEmailAddressPrimary(patient, 'test@example.com');

        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals('test@example.com', patient.Primary_Email_MVN__c);
    }
    
    @isTest
    private static void testInsertNewPrimarySetAccountValues_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Primary_vod__c = true;
        insert address;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);
    }

    @isTest
    private static void testInsertNewPrimarySetAccountValues_Phone() {
        TestDataFactoryMVN.createPhoneNumberPrimary(patient, '123-456-789');

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Phone);
    }

    @isTest
    private static void testInsertNewPrimarySetAccountValues_Fax() {
        TestDataFactoryMVN.createFaxNumberPrimary(patient, '123-456-789');

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Fax);
    }

    //Tests to verify the primary is updated when we insert a new primary and one previously existed. Also that old CI has the 
    //primary flag removed.

    @isTest 
    private static void testInsertNewPrimarySetAccountValuesAndRemoveOld_Email() {
        TestDataFactoryMVN.createEmailAddressPrimary(patient, 'test@example.com');

        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals('test@example.com', patient.Primary_Email_MVN__c);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        TestDataFactoryMVN.createEmailAddressPrimary(patient, 'test2@example.com');

        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals('test2@example.com', patient.Primary_Email_MVN__c);

        Contact_Information_MVN__c oldCI = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Email_MVN__c = 'test@example.com'];

        System.assertEquals(false, oldCI.Primary_MVN__c);
    }
    
    @isTest
    private static void testInsertNewPrimarySetAccountValuesAndRemoveOld_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Primary_vod__c = true;
        insert address;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Address_vod__c address2 = new Address_vod__c(Name = '321 Faux Blvd.');
        address2.City_vod__c = 'Chicago';
        address2.Zip_vod__c = '60606';
        address2.State_vod__c = 'IL';
        address2.Country_vod__c = 'US';
        address2.Account_vod__c = patient.Id;
        address2.Primary_vod__c = true;
        insert address2;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address2.Name, patient.BillingStreet);
        System.assertEquals(address2.City_vod__c, patient.BillingCity);
        System.assertEquals(address2.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address2.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address2.Zip_vod__c, patient.BillingPostalCode);

        address = [SELECT Primary_vod__c FROM Address_vod__c WHERE Id = :address.Id];

        System.assertEquals(false, address.Primary_vod__c);
    }

    @isTest
    private static void testInsertNewPrimarySetAccountValuesAndRemoveOld_Phone() {
        TestDataFactoryMVN.createPhoneNumberPrimary(patient, '123-456-789');

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Phone);
        
        ContactInfoPrimaryTriggerMVN.hasRun = false;

        TestDataFactoryMVN.createPhoneNumberPrimary(patient, '987-654-321');

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals('987-654-321', patient.Phone);

        Contact_Information_MVN__c oldCI = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '123-456-789'];

        System.assertEquals(false, oldCI.Primary_MVN__c);
    }

    @isTest
    private static void testInsertNewPrimarySetAccountValuesAndRemoveOld_Fax() {
        TestDataFactoryMVN.createFaxNumberPrimary(patient, '123-456-789');

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Fax);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        TestDataFactoryMVN.createFaxNumberPrimary(patient, '987-654-321');

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals('987-654-321', patient.Fax);

        Contact_Information_MVN__c oldCI = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '123-456-789'];

        System.assertEquals(false, oldCI.Primary_MVN__c);
    }

    //Tests to verify that if existing primary is unchecked then the details are removed from the account
    
    @isTest 
    private static void testUpdateExistingPrimaryToUncheckedRemoveDetails_Email() {
        TestDataFactoryMVN.createEmailAddressPrimary(patient, 'test@example.com');

        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals('test@example.com', patient.Primary_Email_MVN__c);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c oldCI = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Email_MVN__c = 'test@example.com'];
        oldCI.Primary_MVN__c = false;
        update oldCI;
    
        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals(null, patient.Primary_Email_MVN__c);
    }

    @isTest
    private static void testUpdateExistingPrimaryToUncheckedRemoveDetails_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Primary_vod__c = true;
        insert address;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        address = [SELECT Primary_vod__c FROM Address_vod__c WHERE Id = :address.Id];

        address.Primary_vod__c = false;
        update address;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(null, patient.BillingStreet);
        System.assertEquals(null, patient.BillingCity);
        System.assertEquals(null, patient.BillingStateCode);
        System.assertEquals(null, patient.BillingCountryCode);
        System.assertEquals(null, patient.BillingPostalCode);
    }

    @isTest
    private static void testUpdateExistingPrimaryToUncheckedRemoveDetails_Phone() {
        TestDataFactoryMVN.createPhoneNumberPrimary(patient, '123-456-789');

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Phone);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c oldCI = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '123-456-789'];
        oldCI.Primary_MVN__c = false;
        update oldCI;

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals(null, patient.Phone);
    }

    @isTest
    private static void testUpdateExistingPrimaryToUncheckedRemoveDetails_Fax() {
        TestDataFactoryMVN.createFaxNumberPrimary(patient, '123-456-789');

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Fax);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c oldCI = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '123-456-789'];
        oldCI.Primary_MVN__c = false;
        update oldCI;

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals(null, patient.Fax);
    }

    //Tests to verify that if existing non-primary is checked then the details are updated on the account and the existing primary is unchecked
    
    @isTest 
    private static void testUpdateExistingUncheckedToPrimaryDetails_Email() {
        TestDataFactoryMVN.createEmailAddressPrimary(patient, 'test@example.com');

        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals('test@example.com', patient.Primary_Email_MVN__c);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        TestDataFactoryMVN.createEmailAddress(patient, 'test2@example.com');

        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals('test@example.com', patient.Primary_Email_MVN__c);

        ContactInfoPrimaryTriggerMVN.hasRun = false;        

        Contact_Information_MVN__c ci = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Email_MVN__c = 'test2@example.com'];
        ci.Primary_MVN__c = true;
        update ci;
    
        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals('test2@example.com', patient.Primary_Email_MVN__c);
    }

    @isTest
    private static void testUpdateExistingUncheckedToPrimaryDetails_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Primary_vod__c = true;
        insert address;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Address_vod__c address2 = new Address_vod__c(Name = '321 Faux Blvd.');
        address2.City_vod__c = 'Chicago';
        address2.Zip_vod__c = '60606';
        address2.State_vod__c = 'IL';
        address2.Country_vod__c = 'US';
        address2.Account_vod__c = patient.Id;
        address2.Primary_vod__c = false;
        insert address2;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        address2 = [SELECT Name, City_vod__c, Zip_vod__c, State_vod__c, Country_vod__c, Primary_vod__c FROM Address_vod__c WHERE Id = :address2.Id];

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        address2.Primary_vod__c = true;
        update address2;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address2.Name, patient.BillingStreet);
        System.assertEquals(address2.City_vod__c, patient.BillingCity);
        System.assertEquals(address2.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address2.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address2.Zip_vod__c, patient.BillingPostalCode);
    }

    @isTest
    private static void testUpdateExistingUncheckedToPrimaryDetails_Phone() {
        TestDataFactoryMVN.createPhoneNumberPrimary(patient, '123-456-789');

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Phone);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        TestDataFactoryMVN.createPhoneNumber(patient, '987-654-321');

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Phone);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c ci = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '987-654-321'];
        ci.Primary_MVN__c = true;
        update ci;

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals('987-654-321', patient.Phone);
    }

    @isTest
    private static void testUpdateExistingUncheckedToPrimaryDetails_Fax() {

        TestDataFactoryMVN.createFaxNumberPrimary(patient, '123-456-789');

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Fax);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        TestDataFactoryMVN.createFaxNumber(patient, '987-654-321');

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Fax);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c ci = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '987-654-321'];
        ci.Primary_MVN__c = true;
        update ci;

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals('987-654-321', patient.Fax);
    }

    //Tests to verify that if existing primary is deleted then the details are removed from the account
    
    @isTest 
    private static void testDeleteExistingPrimaryRemoveDetails_Email() {
        TestDataFactoryMVN.createEmailAddressPrimary(patient, 'test@example.com');

        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals('test@example.com', patient.Primary_Email_MVN__c);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c oldCI = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Email_MVN__c = 'test@example.com'];

        delete oldCI;
    
        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals(null, patient.Primary_Email_MVN__c);
    }

    @isTest
    private static void testDeleteExistingPrimaryRemoveDetails_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Primary_vod__c = true;
        insert address;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        address = [SELECT Primary_vod__c FROM Address_vod__c WHERE Id = :address.Id];
        delete address;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(null, patient.BillingStreet);
        System.assertEquals(null, patient.BillingCity);
        System.assertEquals(null, patient.BillingStateCode);
        System.assertEquals(null, patient.BillingCountryCode);
        System.assertEquals(null, patient.BillingPostalCode);
    }

    @isTest
    private static void testDeleteExistingPrimaryRemoveDetails_Phone() {
        TestDataFactoryMVN.createPhoneNumberPrimary(patient, '123-456-789');

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Phone);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c oldCI = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '123-456-789'];
        
        delete oldCI;

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals(null, patient.Phone);
    }

    @isTest
    private static void testDeleteExistingPrimaryRemoveDetails_Fax() {
        TestDataFactoryMVN.createFaxNumberPrimary(patient, '123-456-789');

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Fax);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c oldCI = [SELECT Primary_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '123-456-789'];
        
        delete oldCI;

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals(null, patient.Fax);
    }

    //Tests to cover updating the value held on a contact information record e.g. updating the email address

    @isTest 
    private static void testUpdateExistingPrimaryValueDetails_Email() {
        TestDataFactoryMVN.createEmailAddressPrimary(patient, 'test@example.com');

        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals('test@example.com', patient.Primary_Email_MVN__c);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c oldCI = [SELECT Email_MVN__c FROM Contact_Information_MVN__c WHERE Email_MVN__c = 'test@example.com'];
        oldCI.Email_MVN__c = 'test2@example.com';
        update oldCI;
    
        patient = [SELECT Primary_Email_MVN__c FROM Account WHERE Id = :patient.Id];

        System.assertEquals('test2@example.com', patient.Primary_Email_MVN__c);
    }

    @isTest
    private static void testUpdateExistingPrimaryValueDetails_Address() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Primary_vod__c = true;
        insert address;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        address = [SELECT Primary_vod__c FROM Address_vod__c WHERE Id = :address.Id];

        address.Name = '321 Faux Blvd.';
        address.City_vod__c = 'Chicago';
        address.Zip_vod__c = '60606';
        address.State_vod__c = 'IL';
        address.Country_vod__c = 'US';
        update address;

        patient = [SELECT BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);
    }

    @isTest
    private static void testUpdateExistingPrimaryValueDetails_Phone() {
        TestDataFactoryMVN.createPhoneNumberPrimary(patient, '123-456-789');

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Phone);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c oldCI = [SELECT Phone_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '123-456-789'];
        oldCI.Phone_MVN__c = '987-654-321';
        update oldCI;

        patient = [SELECT Phone FROM Account WHERE Id = :patient.Id];

        System.assertEquals('987-654-321', patient.Phone);
    }

    @isTest
    private static void testUpdateExistingPrimaryValueDetails_Fax() {
        TestDataFactoryMVN.createFaxNumberPrimary(patient, '123-456-789');

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals('123-456-789', patient.Fax);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c oldCI = [SELECT Phone_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '123-456-789'];
        oldCI.Phone_MVN__c = '987-654-321';
        update oldCI;

        patient = [SELECT Fax FROM Account WHERE Id = :patient.Id];

        System.assertEquals('987-654-321', patient.Fax);
    }

    //Test edge case that was clearing an address when a primary contact info is updated

    @isTest
    private static void testUpdateExistingUncheckedPhoneToPrimaryDoesNotClearAddress() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Primary_vod__c = true;
        insert address;

        patient = [SELECT Fax, BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        TestDataFactoryMVN.createFaxNumber(patient, '987-654-321');

        patient = [SELECT Fax, BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);
        System.assertEquals(null, patient.Fax);

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        Contact_Information_MVN__c fax = [SELECT Phone_MVN__c, Primary_MVN__c FROM Contact_Information_MVN__c WHERE Phone_MVN__c = '987-654-321'];
        fax.Primary_MVN__c = true;
        update fax;

        patient = [SELECT Fax, BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);
        System.assertEquals('987-654-321', patient.Fax);
    }

    // Test setting phone/fax on Address does not propagate to the parent level

    @isTest
    private static void testAddressPhoneFaxPopulateAccount() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Primary_vod__c = true;
        address.Phone_vod__c = '123-456-789';
        address.Fax_vod__c = '987-654-321';
        insert address;

        patient = [SELECT Phone, Fax, BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);
        System.assertNotEquals('123-456-789', patient.Phone, 'Inserting a primary address should not update the patient phone number');
        System.assertNotEquals('987-654-321', patient.Fax, 'Inserting a primary address should not update the patient fax number number');
    }

    @isTest
    private static void testUpdateAddressPhoneFaxPopulateAccount() {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = 'US';
        address.Account_vod__c = patient.Id;
        address.Primary_vod__c = true;
        address.Phone_vod__c = '123-456-789';
        address.Fax_vod__c = '987-654-321';
        insert address;

        patient = [SELECT Phone, Fax, BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);
        System.assertNotEquals('123-456-789', patient.Phone, 'Inserting a primary address should not update the patient phone number');
        System.assertNotEquals('987-654-321', patient.Fax, 'Inserting a primary address should not update the patient fax number number');        

        ContactInfoPrimaryTriggerMVN.hasRun = false;

        address.Phone_vod__c = '987-654-321';
        address.Fax_vod__c = '123-456-789';
        update address;

        patient = [SELECT Phone, Fax, BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode FROM Account WHERE Id = :patient.Id];

        System.assertEquals(address.Name, patient.BillingStreet);
        System.assertEquals(address.City_vod__c, patient.BillingCity);
        System.assertEquals(address.State_vod__c, patient.BillingStateCode);
        System.assertEquals(address.Country_vod__c, patient.BillingCountryCode);
        System.assertEquals(address.Zip_vod__c, patient.BillingPostalCode);
        System.assertNotEquals('987-654-321', patient.Phone, 'Updating a primary address should not update the patient phone number');
        System.assertNotEquals('123-456-789', patient.Fax, 'Updating a primary address should not update the patient fax number number');        

        List<Contact_Information_MVN__c> cis = [SELECT Id, Phone_MVN__c, Label_MVN__c, Primary_MVN__c FROM Contact_Information_MVN__c WHERE Account_MVN__c = :patient.Id and Primary_MVN__c = true];
        System.assertEquals(1, cis.size(), 'The phone and fax CI records should not be marked as primary');
        
    }

}