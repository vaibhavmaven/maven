@IsTest
public class EligibilityResponseParserTest {
	
	// This test method should give 100% coverage
	static testMethod void testParse() {
		String json = '{'+
		'		        \"Formulary\":\"3\",'+
		'				\"RetailPharmacy\":\"Not Covered\",'+
		'				\"MailOrderPrescriptionDrug\":\"Covered\",'+
		'				\"SpecialtyPharmacy\":null,'+
		'				\"StepTherapy\":\"false\",'+
		'				\"PriorAuthorization\":\"false\",'+
		'				\"StepMedication\":\"false\",'+
		'				\"BIN\":\"003858\",'+
		'				\"PCN\":\"A5\",'+
		'				\"GROUP\":null,'+
		'				\"RTBI\":\"true\",'+
		'				\"PayerName\":\"PBMA\",'+
		'				\"GROUPID\":\"AA5V\",'+
		'				\"CardholderID\":\"123456789\",'+
		'				\"PBMMemberID\":\"AA512345678900\"'+
		'				}';
		EligibilityResponse r = EligibilityResponse.parse(json);
		System.assert(r != null);

		json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
		EligibilityResponse objEligibilityResponse = new EligibilityResponse(System.JSON.createParser(json));
		System.assert(objEligibilityResponse != null);
		System.assert(objEligibilityResponse.Formulary == '');
		System.assert(objEligibilityResponse.RetailPharmacy == '');
		System.assert(objEligibilityResponse.MailOrderPrescriptionDrug == '');
		System.assert(objEligibilityResponse.SpecialtyPharmacy == '');
		System.assert(objEligibilityResponse.StepTherapy == '');
		System.assert(objEligibilityResponse.PriorAuthorization == '');
		System.assert(objEligibilityResponse.StepMedication == '');
		System.assert(objEligibilityResponse.BIN == '');
		System.assert(objEligibilityResponse.PCN == '');
		System.assert(objEligibilityResponse.GROUP_Z == '');
		System.assert(objEligibilityResponse.RTBI == '');
		System.assert(objEligibilityResponse.PayerName == '');
		System.assert(objEligibilityResponse.GROUPID == '');
		System.assert(objEligibilityResponse.CardholderID == '');
		System.assert(objEligibilityResponse.PBMMemberID == '');
	}
}