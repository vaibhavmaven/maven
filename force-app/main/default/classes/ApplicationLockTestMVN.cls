/**
 * @author Mavens
 * @description tests ApplicationLockMVN
 * @group Application
 */
@isTest private class ApplicationLockTestMVN {
    private static User caseManager;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;
    private static Authorization_and_Consent_MVN__c authorizationAndConsent;
    private static Benefits_Coverage_MVN__c benefitsAndCoverage;
    private static Income_MVN__c income;
    private static Prescription_MVN__c prescription;

    /**
    * @description setups data for all unit tests
    */
    static {
        TestFactoryCustomMetadataMVN.setMocks();
        TestDataFactoryMVN.createPSSettings();

        caseManager = TestDataFactoryMVN.generateCaseManager();
        System.runAs(caseManager) {
            Program_MVN__c program = TestDataFactoryMVN.createProgram();
            Account testAccount = TestDataFactoryMVN.createMember();
            programMember = TestDataFactoryMVN.createProgramMember(program, testAccount);
            application = new Application_MVN__c(
                Program_Member_MVN__c = programMember.Id
            );
            insert application;
            authorizationAndConsent = (Authorization_and_Consent_MVN__c) createRecord(new Authorization_and_Consent_MVN__c());
            benefitsAndCoverage = (Benefits_Coverage_MVN__c) createRecord(new Benefits_Coverage_MVN__c());
            income = (Income_MVN__c) createRecord(new Income_MVN__c());
            Product_MVN__c product = new Product_MVN__c(
                Program_MVN__c = program.Id,
                Product_Status_MVN__c = 'Active');
            insert product;
            prescription = new Prescription_MVN__c(
                Program_Member_MVN__c = programMember.Id,
                Application_MVN__c = application.Id,
                Product_MVN__c = product.Id
            );
            insert prescription;
        }
    }

    private static SObject createRecord(SObject thisSObject) {
        thisSObject.put('Program_Member_MVN__c', programMember.Id);
        thisSObject.put('Application_MVN__c', application.Id);
        insert thisSObject;
        return thisSObject;
    }

    @isTest private static void applicationGetsLocked() {
        System.runAs(caseManager) {
            Test.startTest();
                ApplicationLockMVN appLock = new ApplicationLockMVN(application.Id);
                appLock.lock();
            Test.stopTest();
        }

        Set<Id> actualLockedRecords = new Set<Id> {
            application.Id,
            authorizationAndConsent.Id,
            benefitsAndCoverage.Id,
            income.Id,
            prescription.Id
        };
        for(ProcessInstance actualPrecessInstance : getProcessInstances(actualLockedRecords)) {
            System.assertEquals('Approved', actualPrecessInstance.Status);
        }
    }

    private static List<ProcessInstance> getProcessInstances(Set<Id> actualLockedRecords) {
        return [
            SELECT
                Id,
                Status
            FROM
                ProcessInstance
            WHERE
                TargetObjectId IN :actualLockedRecords
        ];
    }
}