public class PAP_SRFaxStatus_Batch_Scheduler implements Schedulable {
    
    // Override this method to implement schedulable 
    public void execute(SchedulableContext sc){
        system.debug('In execute method of PAP_SRFax_Scheduler'); 
        checkQueuedStatus(); 
        system.debug('Successfuly called the future method callout for Get_MultiFaxStatus from PAP_SRFax_Scheduler'); 
    }
    
    
    /*
*   Method   : checkQueuedStatus
*   Desc     : Checks the status of Queued fax.
*   @param   : Does not except any parameters.
*   @return  : none
*/     
    // we cannot implement a synchronous api callout in a scheduled apex class, hence we are making this asynchronous.     
    @future(callout=true)
    public static void checkQueuedStatus(){
        system.debug('In checkQueuedStatus method of PAP_SRFax_Scheduler'); 
        // variable declarations.
        String queuedFaxIDList; 
        String faxPayload;                   
        List<SRFax_API_ConfigurationSettings__mdt> srfaxConfig;
        List<Program_Member_MVN__c> queuedProgramIDList	= new List<Program_Member_MVN__c>();      
        try{
            queuedProgramIDList = [select Id,Name, 
                                   Queued_Fax_Result_JKC__c,
                                   Sub_Status_JKC__c
                                   from 
                                   Program_Member_MVN__c 
                                   where 
                                   Queued_Fax_Status_JKC__c  = 'Success' and
                                   Sub_Status_JKC__c        = 'Fax Queued' 
                                  ];  
            // 1. fetch all the "Queued Fax ID's" from Program Member object which are successfully queued in SRFax, separated by |             
            if(queuedProgramIDList.size()>0){
                
                
                queuedFaxIDList = getQueuedFaxIDs(queuedProgramIDList);    
                // 2. get SRFax endpoint details.
                srfaxConfig = getEndpointConfigurations();       
                // 3. Populate the payload for POST method.
                faxPayload = populatePayload(srfaxConfig, queuedFaxIDList);            
                // 4. Fire an asynchronous apex web callout to get the status of all these Queued Fax ID's 
                callSRFaxEndpoint(srfaxConfig, faxPayload);  
            }
        }catch(CustomException custExcp){
            // In Program Member of failure, we do not update the status of any Program Member 
            catchException(custExcp, 'checkQueuedStatus',400 ,queuedProgramIDList[0].Name);       
        }catch(Exception excp){
            // In Program Member of failure, we do not update the status of any Program Member 
            catchException(excp, 'checkQueuedStatus',400 ,queuedProgramIDList[0].Name);            
        }  
        system.debug('Finished processing checkQueuedStatus method of PAP_SRFax_Scheduler');         
    }
    
    
    /*
*   Method   : getQueuedFaxIDs
*   Desc     : Gets ID's of successfully queued fax.
*   @param   : Does not except any parameters.
*   @return  : queued fax id's in String format, separated by '|'
*/     
    private static String getQueuedFaxIDs(List<Program_Member_MVN__c> queuedFaxIDList){
        system.debug('Fetching all the Queued Fax IDs now.'); 
        String queuedFaxIDs = '';
        
        system.debug('All Queued Fax ID\'s are : ' + queuedFaxIDList); 
        //validations
        if(queuedFaxIDList == null || queuedFaxIDList.size() == 0)
        {
            system.debug('No Faxe(s) to processed for this invocation of scheduled task.');
            //throw new CustomException('No Faxe(s) to processed for this invocation of scheduled task.'); 
        }
        //populate queued fax ids 
        for(Program_Member_MVN__c programMemberObj: queuedFaxIDList){
            queuedFaxIDs = queuedFaxIDs + programMemberObj.Queued_Fax_Result_JKC__c + '|' ;             
        }
        queuedFaxIDs = queuedFaxIDs.substringBeforeLast('|');
        system.debug('Queued Fax ID\'s sepearated by \'|\' are : ' + queuedFaxIDs); 
        return queuedFaxIDs; 
    }
    
    
    /*
*   Method   : getEndpointConfigurations
*   Desc     : Get SRFax configuration details from SRFax_API_ConfigurationSettings__mdt
*   @param   : Does not except any parameters.
*   @return  : returns configuration details
*/       
    private static List<SRFax_API_ConfigurationSettings__mdt> getEndpointConfigurations(){
        system.debug('Fetching SRFax configurations details for Get_MuultiFaxStatus API.');
        // get configuration details from SRFax_API_ConfigurationSettings__mdt
        List<SRFax_API_ConfigurationSettings__mdt> srfaxConfig = [select 
                                                                  Access_ID_JKC__c, 
                                                                  Access_Password_JKC__c, 
                                                                  Endpoint_URL_JKC__c, 
                                                                  Fax_Action_JKC__c, 
                                                                  HTTP_Method_JKC__c
                                                                  from 
                                                                  SRFax_API_ConfigurationSettings__mdt
                                                                  where 
                                                                  Type_JKC__c = 'SRFax_Get_MultiFaxStatus'
                                                                  AND Program__c ='Bausch PAP'
                                                                  LIMIT 1];  
        // validations
        if(srfaxConfig == null || srfaxConfig.size() == 0)
        {
            throw new CustomException('SRFax GetMultiFax Status endoints not configured correctly in \"PAP_ConfigurationSettings\".'); 
        }
        if(String.isBlank(srfaxConfig[0].Access_ID_JKC__c) || String.isBlank(srfaxConfig[0].Access_Password_JKC__c) || 
           String.isBlank(srfaxConfig[0].Endpoint_URL_JKC__c) || String.isBlank(srfaxConfig[0].HTTP_Method_JKC__c) ||
           String.isBlank(srfaxConfig[0].Fax_Action_JKC__c) ){
               throw new CustomException('SRFax GetMultiFax Status configuration fields empty in in \"PAP_ConfigurationSettings\".');             
           }
        system.debug('SRFax GetMultiFax Status configuration details fetched for Get_MultiFaxStatus API : ' + srfaxConfig[0]);   
        return srfaxConfig;         
    }    
    
    
    /*
*   Method   : populatePayload
*   Desc     : Populate the payload, which is to be sent the the SRFax Get_MultiFaxStatus api.  
*   @param   : Does not except any parameters.
*   @return  : none.
*/      
    private static String populatePayload(List<SRFax_API_ConfigurationSettings__mdt> srfaxConfig,
                                          String queuedFaxIDList){    
                                              system.debug('Populating the JSON payload for GetMultiFax Status API.'); 
                                              String faxPayload ; 
                                              JSONGenerator generator = JSON.createGenerator(false);                              //instantiation of the generator
                                              generator.writeStartObject();                                                       // Initial '{'
                                              generator.writeStringField('action', srfaxConfig[0].Fax_Action_JKC__c);          // action                                               
                                              generator.writeStringField('access_id', srfaxConfig[0].Access_ID_JKC__c);        // access_id 
                                              generator.writeStringField('access_pwd', srfaxConfig[0].Access_Password_JKC__c); // access_pwd 
                                              generator.writeStringField('sFaxDetailsID', queuedFaxIDList);                       // sFaxDetailsID 
                                              generator.writeEndObject();                                                         // final '}'
                                              faxPayload = generator.getAsString(); 
                                              system.debug('JSON Payload for GetMultiFax Status API is : ' + faxPayload);  
                                              return faxPayload;                                             
                                          }    
    
    
    /*
*   Method   : callSRFaxEndpoint
*   Desc     : Call SRFax Get_MultiFaxStatus api to get the status of all successfuly queued fax.
*   @param   : srfaxConfig - srfax configuration settings. 
*            : faxPayload -  payload to be sent in JSON format.  
*   @return  : none
*/      
    private static void callSRFaxEndpoint(List<SRFax_API_ConfigurationSettings__mdt> srfaxConfig, 
                                          String faxPayload){     
                                              system.debug('Making the callout to Get_MultiFaxstatus Endpoint');
                                              // create new authncall obj 
                                              AuthenticateAndCallout authnCall = new AuthenticateAndCallout();
                                              // we do not set the authentication headers. Setting the endpoints details. 
                                              authnCall.setEndpointDetails(srfaxConfig[0].HTTP_Method_JKC__c, 
                                                                           srfaxConfig[0].Endpoint_URL_JKC__c, 
                                                                           faxPayload);
                                              // make the callout
                                              authnCall.makeCallout(); 
                                              system.debug('Get_MultiFaxstatus response object is :' + authnCall);
                                              // validate the response and finally update the status and substatus in Program Member object.
                                              validateResponseAndUpdateProgramMember(authnCall);
                                          }    
    
    
    /*
*   Method   : validateResponseAndUpdateProgramMember
*   Desc     : Validate the response and update the status of these queried fax id's back to Program Member object.
*   @param   : authnCall - callout response object.     
*   @return  : none
*/    
    private static void validateResponseAndUpdateProgramMember(AuthenticateAndCallout authnCall){
        system.debug('Validating the response of Get_MultiFaxStatus.'); 
        // validate the callout responsecode 
        Integer responseCode   = authnCall.getResponseCode(); 
        String  responseString = authnCall.getResponse();
        System.debug('*******responseString '+responseString);
        if(200 != responseCode){
            system.debug('Failed to get status of fax using \"Get_MultiFaxStatus\", response code is : ' + responseCode); 
            throw new CustomException('Call to \"Get_MultipleFaxStatus\" failed. Response code is not 200.');             
        }
        if(String.isBlank(responseString)){
            throw new CustomException('Response String for Get_MultiFaxStatus API is null or empty!');           
        }
        system.debug('******responseString'+responseString);
        PAP_Get_MultiFaxStatusResponse response ; 
        try{
            response = (PAP_Get_MultiFaxStatusResponse) JSON.deserialize(responseString, PAP_Get_MultiFaxStatusResponse.class);     
        }catch(Exception ex){
            system.debug('Failed to parse Get_MultiFaxStatus Response : ' + responseString);  
            system.debug('Exception Details : ' + ex.getMessage());
            system.debug('stack trace ' + ex.getStackTraceString()); 
            throw new CustomException('Failed to parse Get_MultiFaxStatus Response.'); 
        }        
        if((String.isBlank(response.Status)) || (response.Status != 'Success')){
            throw new CustomException('\"Status\" in json response of Get_MutliFaxStatus is not \"Success\".');             
        }
        // for each fax we need these attributes FaxDetailsID, SentStatus, ErrorCode
        Map<String, object> faxInfo = new Map<String, object>{}; 
            system.debug('Parsing Result section of received json to populate faxInfo Map.'); 
        if( response.Result == null || response.Result.size() == 0){
            throw new CustomException('\"Result\" in json response of Get_MutliFaxStatus is empty or null.');            
        }
        for(PAP_Get_MultiFaxStatusResponse.Result obj: response.Result){      
            //processing only Sent and Failed fax messages. 
            if(obj.SentStatus.equalsIgnoreCase('Sent') || obj.SentStatus.equalsIgnoreCase('Failed')){
                Map<String, String> faxDetail = new Map<String, String> {'SentStatus' => '', 'ErrorCode' => ''};                
                    faxDetail.put('SentStatus', obj.SentStatus);                      
                faxDetail.put('ErrorCode',  obj.ErrorCode);             
                faxInfo.put(obj.FaxDetailsID, faxDetail);                                       
            }               
        }
        system.debug('populated faxInfo map is : ' + faxInfo); 
        List<Program_Member_MVN__c> programMemberList = [Select  
                                                         //Status, 
                                                         Sub_Status_JKC__c, 
                                                         Queued_Fax_Result_JKC__c,
                                                         Fax_Delivery_JKC__c, 
                                                         Queued_Fax_Status_JKC__c
                                                         From 
                                                         Program_Member_MVN__c 
                                                         Where 
                                                         Queued_Fax_Status_JKC__c = 'Success' and
                                                         Sub_Status_JKC__c        = 'Fax Queued' and 
                                                         // Status                      = 'New Replacement Order Created' and 
                                                         Is_Prescription_Generated_JKC__c = true 
                                                        ]; 
        if(programMemberList == null || programMemberList.size() == 0){
            throw new CustomException('Queued Fax list to update the status is empty!');             
        }
        system.debug('Successfully queued fax details are : ' + programMemberList); 
        Set<String> faxInfoKeys = faxInfo.keySet(); 
        system.debug('Queued Fax Id\'s of our interest are : ' + faxInfoKeys);
        List<Program_Member_MVN__c> prograMemberToUpdate = new List<Program_Member_MVN__c>{}; 
            system.debug('Iterating the Program Members, and will update only above mentioned Queued Fax ID\'s. '); 
        for(Program_Member_MVN__c programMemberObj : programMemberList){
            String keyStr = programMemberObj.Queued_Fax_Result_JKC__c;  
            system.debug('Processing this Queued Fax ID :' + keyStr); 
            if(faxInfoKeys.contains(keyStr)){    
                system.debug('This queued fax id is in our json response. ' + keyStr); 
                String sentStatus = ((Map<String, String>)faxInfo.get(keyStr)).get('SentStatus'); 
                String errorCode  = ((Map<String, String>)faxInfo.get(keyStr)).get('ErrorCode');
                if(sentStatus == 'Sent'){
                    //    programMemberObj.Status = 'Faxed'; 
                    programMemberObj.Sub_Status_JKC__c = 'Fax Sent'; 
                    programMemberObj.Fax_Delivery_JKC__c = 'Delivered'; 
                    programMemberObj.Fax_Sent_Time_JKC__c = Datetime.now();
                    programMemberObj.Queued_Fax_Status_JKC__c = 'Fax Sent'; 
                }
                if(sentStatus == 'Failed'){
                    //     programMemberObj.Status = 'Fax Sent'; 
                    programMemberObj.Sub_Status_JKC__c = 'Fax Failed';                                        
                    programMemberObj.Fax_Delivery_JKC__c = 'Failed'; 
                    programMemberObj.Queued_Fax_Status_JKC__c = errorCode; 
                }                 
                prograMemberToUpdate.add(programMemberObj); 
            }// if condition            
        }// for loop 
        system.debug('finally Program Members to update are : ' + prograMemberToUpdate); 
        update prograMemberToUpdate;        
    }    
    
    
    /*
*   Method   : catchException
*   Desc     : Method to log details of Exception. 
*   @param   : Exception , The exception object 
*   @param   : String ,    Name of the method in which the exception occured.       
*   @return  : none
*/    
    private static void catchException(Exception ex , String methodName ,Integer responseCode, String ObjectId ){    
        PAP_ErrorLog.logErrorActivity(ex, methodName , responseCode,  ObjectId);       
    } 
}