/**
 *  ContactInfoPrimaryTriggerMVN
 *  Created By:     Paul Battisson 
 *  Created On:     2nd May 2017
 *  Description:    This trigger handler manages contact information records to ensure we always have at maximum 1 primary record. 
 *                  Possible paths handled are:
 *                  - Insert new CI record marked as primary
 *                       -> If there is an existing primary remove primary designation. 
 *                       -> Update Account primary information regardless
 *                  - Update CI to be marked as primary
 *                       -> If there is an existing primary remove primary designation
 *                       -> Update Account primary information regardless
 *                  - Update CI and remove primary designation
 *                       -> Remove details from the Account
 *                  - Delete CI which is marked as Primary
 *                      -> Remove details from the Account
 *                  
 **/
public with sharing class ContactInfoPrimaryTriggerMVN implements TriggerHandlerMVN.HandlerInterface {

    private Map<String, Id> recordTypeMap = new Map<String, Id>();

    public static Boolean hasRun = false;

    private Map<Id, Account> accountsToUpdate = new Map<Id, Account>();

    public void handle() {

        if(hasRun) {
            return;
        }

        hasRun = true;

        Map<Id, Contact_Information_MVN__c> acctIdToNewPrimaryMap_Email = new Map<Id, Contact_Information_MVN__c>();
        Map<Id, Contact_Information_MVN__c> acctIdToNewPrimaryMap_Phone = new Map<Id, Contact_Information_MVN__c>();
        Map<Id, Contact_Information_MVN__c> acctIdToNewPrimaryMap_Fax = new Map<Id, Contact_Information_MVN__c>();
        Map<Id, Contact_Information_MVN__c> acctIdToNewPrimaryMap_Address = new Map<Id, Contact_Information_MVN__c>();
        Set<Id> addressIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();

        if(Trigger.isInsert || Trigger.isUpdate) {
            for(Contact_Information_MVN__c newCI : (List<Contact_Information_MVN__c>)Trigger.new) {
                Id rtId = newCI.RecordTypeId;

                if(newCI.Primary_MVN__c) {
                    accountIds.add(newCI.Account_MVN__c);
                    if(isAddressRecordType(rtId)) {
                        acctIdToNewPrimaryMap_Address.put(newCI.Account_MVN__c, newCI);
                        addressIds.add(newCI.Address_MVN__c);
                    } else if(isEmailRecordType(rtId)){
                        acctIdToNewPrimaryMap_Email.put(newCI.Account_MVN__c, newCI);
                    } else if(isPhoneRecordType(rtId)){
                        if(newCI.Label_MVN__c == 'Fax') {
                            acctIdToNewPrimaryMap_Fax.put(newCI.Account_MVN__c, newCI);
                        } else {
                            acctIdToNewPrimaryMap_Phone.put(newCI.Account_MVN__c, newCI);
                        }
                    }
                } else if(Trigger.isUpdate && ((Contact_Information_MVN__c)Trigger.oldMap.get(newCI.Id)).Primary_MVN__c == true) {
                    Contact_Information_MVN__c dummyCI = new Contact_Information_MVN__c();
                    dummyCI.RecordTypeId = newCI.RecordTypeId;
                    dummyCI.Label_MVN__c = newCI.Label_MVN__c;
                    dummyCI.Primary_MVN__c = false;

                    accountIds.add(newCI.Account_MVN__c);

                    if(isAddressRecordType(rtId)) {
                        acctIdToNewPrimaryMap_Address.put(newCI.Account_MVN__c, dummyCI);
                        addressIds.add(newCI.Address_MVN__c);
                    } else if(isEmailRecordType(rtId)){
                        acctIdToNewPrimaryMap_Email.put(newCI.Account_MVN__c, dummyCI);
                    } else if(isPhoneRecordType(rtId)){
                        if(newCI.Label_MVN__c == 'Fax') {
                            acctIdToNewPrimaryMap_Fax.put(newCI.Account_MVN__c, dummyCI);
                        }
                                            
                        else {
                            acctIdToNewPrimaryMap_Phone.put(newCI.Account_MVN__c, dummyCI);
                        }
                    }
                }
            }

            retrieveAccounts(accountIds);
            getUpdatedAccounts(acctIdToNewPrimaryMap_Address, addressIds);
            getUpdatedAccounts(acctIdToNewPrimaryMap_Email, addressIds);
            getUpdatedAccounts(acctIdToNewPrimaryMap_Phone, addressIds);
            getUpdatedAccounts(acctIdToNewPrimaryMap_Fax, addressIds);
            update accountsToUpdate.values();

            List<Contact_Information_MVN__c> contactInfoToUpdate = new List<Contact_Information_MVN__c>();
            contactInfoToUpdate.addAll(getUpdatedContactInfoRecords(acctIdToNewPrimaryMap_Address));
            contactInfoToUpdate.addAll(getUpdatedContactInfoRecords(acctIdToNewPrimaryMap_Email));
            contactInfoToUpdate.addAll(getUpdatedContactInfoRecords(acctIdToNewPrimaryMap_Phone));
            contactInfoToUpdate.addAll(getUpdatedContactInfoRecords(acctIdToNewPrimaryMap_Fax));
            update contactInfoToUpdate;

            update getUpdatedAddressRecords(acctIdToNewPrimaryMap_Address, addressIds);

        } else if(Trigger.isDelete) {
            Map<Id, Contact_Information_MVN__c> acctIdToNewPrimaryMap = new Map<Id, Contact_Information_MVN__c>();
            for(Contact_Information_MVN__c deletedCI : (List<Contact_Information_MVN__c>)Trigger.old) {
                if(deletedCI.Primary_MVN__c) {
                    Contact_Information_MVN__c dummyCI = new Contact_Information_MVN__c();
                    dummyCI.RecordTypeId = deletedCI.RecordTypeId;
                    dummyCI.Label_MVN__c = deletedCI.Label_MVN__c;
                    acctIdToNewPrimaryMap.put(deletedCI.Account_MVN__c, dummyCI);
                    accountIds.add(deletedCI.Account_MVN__c);
                }
            }

            retrieveAccounts(accountIds);
            getUpdatedAccounts(acctIdToNewPrimaryMap, addressIds);
            update accountsToUpdate.values();
        }
    }

    private void retrieveAccounts(Set<Id> accountIds) {
        accountsToUpdate = new Map<Id, Account>([SELECT Primary_Email_MVN__c,  BillingStreet, BillingCity, BillingState, 
                                    BillingCountry, BillingPostalCode, Phone, Fax FROM Account WHERE Id in :accountIds]);
    }

    private void getUpdatedAccounts(Map<Id, Contact_Information_MVN__c> acctIdToNewPrimaryMap, Set<Id> addressIds) {
        

        Map<Id, Address_vod__c> addresses = new Map<Id,Address_vod__c>([SELECT Id, Name, Address_Line_2_vod__c, City_vod__c, State_vod__c, Country_vod__c, Zip_vod__c
                                                                            FROM Address_vod__c WHERE Id in :addressIds]);

        for(Account acct : accountsToUpdate.values()) {
            if(acctIdToNewPrimaryMap.containsKey(acct.Id)) {
                Contact_Information_MVN__c newCI = acctIdToNewPrimaryMap.get(acct.Id);

                if(isEmailRecordType(newCI.RecordTypeId)) {
                    acct.Primary_Email_MVN__c = newCI.Email_MVN__c;
                } else if(isAddressRecordType(newCI.RecordTypeId)) {
                    Address_vod__c address = addresses.get(newCI.Address_MVN__c);

                    if(Trigger.isDelete || (Trigger.isUpdate && newCI.Primary_MVN__c == false)) {
                        address = new Address_vod__c();
                    }

                    if(address != null){
                        acct.BillingStreet = String.isBlank(address.Address_line_2_vod__c) ? address.Name 
                                                                                           : address.Name + ', ' + address.Address_line_2_vod__c;
                        acct.BillingCity = address.City_vod__c;
                        acct.BillingStateCode = address.State_vod__c;
                        acct.BillingCountryCode = address.Country_vod__c;
                        acct.BillingPostalCode = address.Zip_vod__c;
                    }
                } else if(isPhoneRecordType(newCI.RecordTypeId)) {
                    if(newCI.Label_MVN__c == 'Fax') {
                        acct.Fax = newCI.Phone_MVN__c;
                    } else {
                        acct.Phone = newCI.Phone_MVN__c;
                        if(newCI.Label_MVN__c == 'Mobile' && newCI.Messages_OK_MVN__c == 'Yes'){
                            acct.Primary_SMS_Number_JKC__c = newCI.Phone_MVN__c;
                        }
                        else {
                            acct.Primary_SMS_Number_JKC__c = newCI.Phone_MVN__c;
                        }
                    }
                }    
            }
        }
    }

    private List<Contact_Information_MVN__c> getUpdatedContactInfoRecords(Map<Id, Contact_Information_MVN__c> acctIdToNewPrimaryMap) {
        List<Contact_Information_MVN__c> oldPrimaries = [SELECT Id, Phone_MVN__c, Email_MVN__c, Primary_MVN__c, Label_MVN__c, Address_MVN__c, Account_MVN__c, RecordTypeId 
                                                            FROM Contact_Information_MVN__c WHERE Account_MVN__c in :acctIdToNewPrimaryMap.keySet()
                                                                                            AND Primary_MVN__c = true
                                                                                            AND Id NOT in :Trigger.newMap.keySet()];

        List<Contact_Information_MVN__c> cisForUpdate = new List<Contact_Information_MVN__c>();
        for(Contact_Information_MVN__c oldCI : oldPrimaries) {
            Contact_Information_MVN__c newCI = acctIdToNewPrimaryMap.get(oldCI.Account_MVN__c);
            Boolean removePrimary = false;
            //We need to remove the primary flag if they have the same record type and in the case of phone the same label
            if(newCI.RecordTypeId == oldCI.RecordTypeId) {
                if(isEmailRecordType(oldCI.RecordTypeId)) {
                    removePrimary = true;
                } else if(isPhoneRecordType(oldCI.RecordTypeId) && ((newCI.Label_MVN__c == 'Fax' && oldCI.Label_MVN__c == 'Fax') || (newCI.Label_MVN__c != 'Fax' && oldCI.Label_MVN__c != 'Fax'))) {
                    removePrimary = true;
                }
            }

            if(removePrimary) {
                oldCI.Primary_MVN__c = false;
                cisForUpdate.add(oldCI);
            }
        }

        return cisForUpdate;
    }

    private List<Address_vod__c> getUpdatedAddressRecords(Map<Id, Contact_Information_MVN__c> acctIdToNewPrimaryMap, Set<Id> addressIds) {
        
        if(addressIds.size() == 0) {
            return new List<Address_vod__c>();
        }

        List<Address_vod__c> oldAddresses = [SELECT Id, Primary_vod__c, Account_vod__c FROM Address_vod__c WHERE Primary_vod__c = true
                                                                                            AND Account_vod__c in :acctIdToNewPrimaryMap.keySet()
                                                                                            AND Id NOT in :addressIds];

        for(Address_vod__c oldAddress : oldAddresses) {
            if(isAddressRecordType(acctIdToNewPrimaryMap.get(oldAddress.Account_vod__c).RecordTypeId)) {
                oldAddress.Primary_vod__c = false;    
            }
        }

        return oldAddresses;
    }

    private Boolean isEmailRecordType(Id rtId) {
        return rtId == getRTId('Email_MVN');
    } 

    private Boolean isAddressRecordType(Id rtId) {
        return rtId == getRTId('Address_MVN');
    } 

    private Boolean isPhoneRecordType(Id rtId) {
        return rtId == getRTId('Phone_MVN');
    } 

    private String getRTId(String rtId) {
        if(recordTypeMap.keySet().size() == 0) {
            populateRTMap();
        }
        return recordTypeMap.get(rtId);
    }

    private void populateRTMap() {
        recordTypeMap = new Map<String, Id>();
        for (RecordType rt : [SELECT Id, DeveloperName
                                    FROM RecordType
                                   WHERE SObjectType = 'Contact_Information_MVN__c']) {
                recordTypeMap.put(rt.DeveloperName, rt.Id);
            }
        }
}