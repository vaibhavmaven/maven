/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description tests EligibilityEngineOrHdlrMVN
 */
@isTest private class EligibilityEngineOrHdlrTestMVN {
    private static User caseManager;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        TestDataFactoryMVN.createPSSettings();

        System.runAs(caseManager) {
            program = TestDataFactoryMVN.createProgram();
            Account member = TestDataFactoryMVN.createMember();
            member.ShippingStreet = 'Test Address Line One';
            update member;
            programMember = TestDataFactoryMVN.createProgramMember(program, member);
            application = createApplication();
            programMember.Active_Application_MVN__c = application.Id;
            update programMember;
        }
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        return testApplication;
    }

    @isTest private static void runOrToSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Or Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'OR_MVN',
            Program_MVN__c = program.Id,
            Active_MVN__c = true
        );
        insert expectedRule;
        Eligibility_Engine_Rule_MVN__c expectedChildRuleOne = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '2',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Program_MVN__c',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Not_Equal_To_MVN',
            Value_MVN__c = program.Id,
            Parent_Rule_MVN__c = expectedRule.Id,
            Active_MVN__c = true
        );
        Eligibility_Engine_Rule_MVN__c expectedChildRuleTwo = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '3',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.BillingStreet',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Not_Equal_To_MVN',
            Value_MVN__c = null,
            Parent_Rule_MVN__c = expectedRule.Id,
            Active_MVN__c = true
        );
        List<Eligibility_Engine_Rule_MVN__c> childRules = new List<Eligibility_Engine_Rule_MVN__c> {
            expectedChildRuleOne,
            expectedChildRuleTwo
        };
        insert childRules;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest private static void runOrToNoSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Or Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'OR_MVN',
            Program_MVN__c = program.Id,
            Active_MVN__c = true
        );
        insert expectedRule;
        Eligibility_Engine_Rule_MVN__c expectedChildRuleOne = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '2',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.BillingStreet',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Equal_To_MVN',
            Value_MVN__c = null,
            Parent_Rule_MVN__c = expectedRule.Id,
            Active_MVN__c = true
        );
        Eligibility_Engine_Rule_MVN__c expectedChildRuleTwo = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '3',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Not_Equal_To_MVN',
            Value_MVN__c = null,
            Parent_Rule_MVN__c = expectedRule.Id,
            Active_MVN__c = true
        );
        List<Eligibility_Engine_Rule_MVN__c> childRules = new List<Eligibility_Engine_Rule_MVN__c> {
            expectedChildRuleOne,
            expectedChildRuleTwo
        };
        insert childRules;

        System.runAs(caseManager) {
            Test.startTest();
                List<Eligibility_Engine_Result_MVN__c> actualResults =
                    (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(expectedRule.Name, actualResults[0].Name);
            System.assertEquals(expectedRule.Result_MVN__c, actualResults[0].Result_MVN__c);
        }
    }
}