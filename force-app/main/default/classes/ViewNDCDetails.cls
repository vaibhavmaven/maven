public class ViewNDCDetails {
    private String bncid {get; set; }
    private Map<String, String> ndcMap {get; set;}
    
    //below variables are not transient any more as we fire refresh in VF page.
    public Map<String, CoverageResponse> mapCovgResp {get; set;} 
    public Boolean renderEvinceMedResponse {get; set;}
    public Boolean insured {get; set;}   
    public String bncNumber;
    
    @TestVisible private static String testNDCID; 
    
    // Constructor of class, we fetch the Bnc ID here. 
    public ViewNDCDetails(ApexPages.StandardController controller) {           
        // Initialize required variables.
        system.debug('Inside ViewNDCDetails Constructor.'); 
        bncid = controller.getId(); 
        ndcMap = new Map<String, String>{};
        mapCovgResp = new Map<String, CoverageResponse>{};    
        renderEvinceMedResponse = false ; 
        insured = false ; 
        bncNumber = '';
        system.debug('Leaving ViewNDCDetails Constructor.');        
    }
    
    // This function will be called when doInit() is called from JS code in the VF page from apex:actionFunction page.
    public void initializePage(){
        // Populate data in mapCovgResp variable to display on VF page.        
        getavailableNDC(); 
        retrieveAllNDCDetails() ;
    }
    
    // Get a map of <NDCID , NDC> for the current Benefit and Coverage page. 
    private void getavailableNDC(){           
        system.debug('Inside getavailableNDC with bncID : ' + bncid); 
        try{           
            // code to get the list of NDC's related to this BnC.
            List<Benefits_Coverage_MVN__c> bncObjList = [select Health_Insurance_MVN__c, Application_MVN__c,Name from 
                                                         Benefits_Coverage_MVN__c where ID =:bncid  LIMIT 1] ;           
            bncNumber = bncObjList[0].Name;            
            system.debug('bncNumber : ' + bncNumber); 
            if(bncObjList.size() == 0){
                system.debug('Failed to get Application ID using BnC ID.'); 
                throw new CustomException('Failed to get Application ID using BnC ID.');         
            }
            //list of prescriptions 
            List<Prescription_MVN__c> listpresp = [select Product_MVN__c from Prescription_MVN__c where 
                                                   Application_MVN__c = :bncObjList[0].Application_MVN__c]; 
            system.debug('Prescription list : ' + listpresp); 
            // set value for Insured
            insured = (null == bncObjList[0].Health_Insurance_MVN__c || bncObjList[0].Health_Insurance_MVN__c.equalsIgnoreCase('yes')) ? true : false  ;
            //List<String> listNDC = new List<String>{};      
            if(listpresp != null && listpresp.size() > 0 ){
                for (Prescription_MVN__c prscp : listpresp){
                    // As per business logic, since each prescription will have multiple drugs, we need this soql in for loop. 
                    List<Product_Line_Item_MVN__c> listProducts = [select ID, NDC_MVN__c from Product_Line_Item_MVN__c 
                                                                   where Product_MVN__c = :prscp.Product_MVN__c];
                    for(Product_Line_Item_MVN__c product : listProducts) {
                        //listNDC.add(product.NDC_MVN__c);       
                        ndcMap.put(product.id, product.NDC_MVN__c);  
                    }
                }
            }
            else{
                system.debug('No prescriptions found.'); 
            }
            system.debug('Leaving getavailableNDC() with list of ndcmaps ' + ndcMap); 
        }catch(CustomException cust){
            renderEvinceMedResponse = false ; 
            system.debug('Caught custom exception : ' + cust.getMessage());  
            handleException(cust.getMessage(), 'getavailableNDC',bncNumber);            
        }catch(Exception ex){
            renderEvinceMedResponse = false ; 
            system.debug('Caught exception : ' + ex.getMessage());  
            handleException(ex.getMessage(), 'getavailableNDC',bncNumber);            
        }// catch   
    }// function
    
    // Function to query and populate RTBI, Eligibility and SCC details. 
    private void retrieveAllNDCDetails(){  
        system.debug('Inside retrieveAllNDCDetails() with ndcMap : ' + ndcMap); 
        try{
            // if ndcMap is empty ,do no process and display any thing.. 
            // we retrieve FAS information only once, hence doing it outside the loop. 
            List<FAS_Response_Master__c> fasResponseList = retrieveFASDetails();
            for(String ndcID  : ndcMap.keySet()){   
                CoverageResponse covgResp = new CoverageResponse(); 
                // populate RTBI results
                processRTBIJson(ndcMap.get(ndcID), covgResp);                          
                // populate ELGB results
                processELGBJson(ndcMap.get(ndcID), covgResp);    
                // populate FAS results
                covgResp.fasResponseList = fasResponseList; 
                // populate the coverage response map
                mapCovgResp.put(ndcMap.get(ndcID), covgResp);
            } // for loop     
            system.debug('Data to be dumped on vf page.: ' + mapCovgResp); 
        }catch(CustomException cust){
            renderEvinceMedResponse = false ; 
            system.debug('Custom Exception details are : ' + cust.getMessage());
            system.debug('Custom Exception stack trace is : ' + cust.getStackTraceString());            
            handleException(cust.getMessage(), 'retrieveAllNDCDetails',bncNumber);                    
        }catch(Exception ex){
            renderEvinceMedResponse = false ; 
            system.debug('Exception details are : ' + ex.getMessage());
            system.debug('Exception stack trace is : ' + ex.getStackTraceString());
            handleException(ex.getMessage(), 'retrieveAllNDCDetails',bncNumber); 
        }
    }// function retrieveNDCDetail  
    
    private void processELGBJson(String ndcID, CoverageResponse covgResp){
        system.debug('Inside processELGBJson with ndcID : ' + ndcID); 
        if(Test.isRunningTest()){
            ndcID = testNDCID; 
        }        
        covgResp.elgbResponseList = [ select NDC__c, Patient__c, Prescriber__c, Program_Name__c, Status__c, Status_Message__c, 
                                      (select BIN__c, Cardholder_ID__c, Eligibility_Response_Master__c, Formulary__c, 
                                      GROUP__c, GROUP_ID__c, Mail_Order_Prescription_Drug__c, Payer_Name__c, PBM_Member_ID__c, 
                                      PCN__c, Plan_Name__c, Prior_Authorization__c, Retail_Pharmacy__c, RTBI__c, 
                                      Specialty_Pharmacy__c, Step_Medication__c, Step_Therapy__c from Health_Plan__r) 
                                      from Eligibility_Response_Master__c where Benefits_Coverage__c = :bncid and 
                                      NDC__c = :ndcID ORDER BY Last_Query_Date__C DESC NULLS LAST LIMIT 1 ]; 
        if(covgResp.elgbResponseList == null || covgResp.elgbResponseList.size() == 0){ // so that VF page does not crash.
            covgResp.elgbResponseList = new List<Eligibility_Response_Master__c>{};  
            system.debug('No eligiblity response master data stored for this NDC. ' + ndcID); 
            //throw new CustomException('No eligiblity response master data stored for this NDC. ' + ndcID); 
            return; 
        }
        // merging the status and statusMessage only if both have valid strings in them. 
        if(!string.isEmpty((covgResp.elgbResponseList[0].Status__c)) && (!string.isEmpty(covgResp.elgbResponseList[0].Status_Message__c)))
        covgResp.elgbResponseList[0].Status__c = covgResp.elgbResponseList[0].Status__c + '(' + 
                                                 covgResp.elgbResponseList[0].Status_Message__c + ')'; 
        renderEvinceMedResponse = true; 
        system.debug('Leaving processELGBJson with ELGB response : ' + covgResp.elgbResponseList);
    }
    
    private void processRTBIJson(String ndcID, CoverageResponse covgResp){
        system.debug('Inside processRTBIJson');
        covgResp.rtbiMasterList = [select NDC__c,(select Address_Line_1__c, Address_Line_2__c, BIN__c, City__c, NCPDPID__c, NPI__c, 
                          PharmacyName__c, State__c, Type__c, Zip__c from RTBI_Pharmacy_Details__r), (select BIN__c, 
                          Days_Supply_Priced__c, Deductible_Applied_Amount__c, Deductible_Remaining_Amount__c, Drug_Status__c, 
                          Estimated_Patient_Pay_Amount__c, Formulary_Status__c, Message__c, OOP_Applied_Amount__c, 
                          OOP_Remaining_Amount__c, Plan_Pay_Amount__c, Prior_Authorization_Required__c, Quantity_Priced__c, 
                          Quantity_Unit_Description__c from RTBI_Pricing_Coverages__r), (select BIN__c, Reference_Code__c, 
                          Reference_Text__c, Text_Message__c from RTBI_Coverage_Alerts__r) from RTBI_Response_Master__c where 
                          Benefits_Coverage__c = :bncid and NDC__c = :ndcID ORDER BY Last_Query_Date__C DESC NULLS LAST LIMIT 1]; 
        
        if(covgResp.rtbiMasterList == null || covgResp.rtbiMasterList.size() == 0){ // so that the VF page does not crash.
            covgResp.rtbiMasterList = new List<RTBI_Response_Master__c>{}; 
            system.debug('No rtbi response master data stored for this NDC. ' + ndcID);
            //throw new CustomException('No rtbi response master data stored for this NDC. ' + ndcID); 
            return; 
        }    
        renderEvinceMedResponse = true; 
        system.debug('Leaving processRTBIJson with RTBI response : ' + covgResp.rtbiMasterList);
    }    
    
    private List<FAS_Response_Master__c> retrieveFASDetails(){
        List<FAS_Response_Master__c> fasResponseList = [select Address__c, City__C, Date_Of_Birth__c, First_Name__c, 
                                                        FPL_Percentage__c, House_Hold_Income_Estimate__c, House_Hold_Size_Estimate__c, 
                                                        Last_Name__c, Middle_Name__c, State__c, Zip__c from FAS_Response_Master__c 
                                                        where Benefits_Coverage__c = :bncid ORDER BY Last_Query_Date__C DESC 
                                                        NULLS LAST LIMIT 1]; 
        if(fasResponseList == null || fasResponseList.size() == 0){
            fasResponseList = new List<FAS_Response_Master__c>{};   
            system.debug('No FAS data stroed for this patient.');
            return fasResponseList;
        } 
        renderEvinceMedResponse = true; 
        return fasResponseList; 
    }
    
    private void handleException(String errorMessage, String methodName, String ObjectId){        
        //Handling the exception and writing to errorlog object
        Error_Log_PAP__c errLog  = new Error_Log_PAP__c (); 
        errLog.Operation__c      = methodName;           
        errLog.Error_Message__c  = errorMessage; 
        errLog.Object_Id__c      = ObjectId;
        errLog.Program_Type__c   = 'Bausch PAP';
        errLog.Error_Type__c     = 'Error';
        errLog.Error_Code__c     = 0;        
        Insert errLog;         
    }    
}