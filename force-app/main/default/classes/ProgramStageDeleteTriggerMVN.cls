/**
 *	ProgramStageDeleteTriggerMVN
 *	Created By:		Aggen
 *	Created On:		04/25/2016
 *  Description:	This is a trigger class that removes related child stages when a parent stage is deleted
 *					
 **/
 public class ProgramStageDeleteTriggerMVN implements TriggerHandlerMVN.HandlerInterface {

	public void handle() {
		if(Trigger.isDelete) {
			List<Id> deletedParentStageIDs = new List<Id>();
			List<Program_Stage_MVN__c> removeChildrenStages = new List<Program_Stage_MVN__c>();

			for(Program_Stage_MVN__c deletedParentStage : (List<Program_Stage_MVN__c>) Trigger.old) {
				if(deletedParentStage.Is_Parent_Stage_MVN__c) {
					deletedParentStageIDs.add(deletedParentStage.Id);
				}
			}

			for(Program_Stage_MVN__c childStage : [select Id from Program_Stage_MVN__c where Parent_Stage_MVN__c in :deletedParentStageIDs]) {
				removeChildrenStages.add(childStage);
			}

			if(removeChildrenStages.size() > 0) delete removeChildrenStages;
		}
	}
}