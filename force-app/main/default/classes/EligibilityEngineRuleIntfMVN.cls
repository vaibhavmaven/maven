/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description This is the generic interface to structure an Eligibility Engine Rule
 *              it enables a controlled triggering of the rule implementations
 */
public interface EligibilityEngineRuleIntfMVN {
    /**
     * run rule on application
     * @param   Eligibility_Engine_Rule_MVN__c  rule to run
     * @param   Application_MVN__c              application to run the rule against
     */
    List<Eligibility_Engine_Result_MVN__c> run(
        Eligibility_Engine_Rule_MVN__c rule,
        Map<String, Object> requestedData
    );

    /**
     * Return a map of object api names to set of fields to be queried
     * @return  Map<String, Set<String>> map of object api names to set of fields needed
     */
    Map<String, Set<String>> getExtraFieldsToQuery();

    /**
     * Return a EligibilityEngineQueryConfigMVN object or null defining how to
     * query for objects not directly related to the applciation
     * @return EligibilityEngineQueryConfigMVN   definition
     */
    EligibilityEngineQueryConfigMVN getAdditionalDataConfig();
}