/**
 * @author      Mavens
 * @group       ProgramMemberStageSkipBIHdlrMVN
 * @description Unit tests for ProgramMemberStageSkipBIHdlrMVN
 */
@isTest
private class ProgramMemberStageSkipBIHdlrTestMVN
{
    static Program_MVN__c program;
    static Program_Stage_MVN__c stage;
    static List<Account> patients;
    static Account prescriber;
    static List<Program_Member_MVN__c> programMembers;
    static List<Application_MVN__c> applications;
    static List<Program_Member_Stage_MVN__c> memberStages;
    static List<Benefits_Coverage_MVN__c> coverages;

    static final Integer DATA_SIZE = 1;

    static void createProgram(Boolean skippable) {
        TestFactoryProgramMVN factory = new TestFactoryProgramMVN();
        program = (new TestFactoryProgramMVN()).create(
            new Map<String, Object> {
                'Allow_Benefits_Investigation_Skip_MVN__c' => skippable ? 'Yes' : 'No'
            }
        );
    }

    static void createBIStage() {
        stage = new Program_Stage_MVN__c (
            Name = 'Benefits Investigation',
            Program_MVN__c = program.Id
        );
        insert stage;
    }

    static void createPatients() {
        patients = (new TestFactoryAccountMVN()).createPatients(DATA_SIZE);
    }

    static void createPrescriber() {
        prescriber =  (new TestFactoryAccountMVN()).createPrescriber();
    }

    static void createProgramMembers() {
        programMembers = (new TestFactoryProgramMemberMVN()).create(
            program,
            prescriber,
            patients
        );
    }

    static void createApplications() {
        applications = new List<Application_MVN__c>();
        for (Program_Member_MVN__c member : programMembers) {
            Application_MVN__c application = new Application_MVN__c (Program_Member_MVN__c = member.Id);
            applications.add(application);
        }
        insert applications;
    }

    static void createMemberStages() {
        memberStages = new List<Program_Member_Stage_MVN__c>();
        List<Program_Member_MVN__c> membersWithActiveApplications = [
            SELECT
                Id,
                Active_Application_MVN__c
            FROM
                Program_Member_MVN__c
            WHERE
                Id IN :programMembers
        ];

        for (Program_Member_MVN__c member : membersWithActiveApplications) {
            memberStages.add(
                new Program_Member_Stage_MVN__c (
                    Program_Stage_MVN__c = stage.Id,
                    Program_Member_MVN__c = member.Id,
                    Application_MVN__c = member.Active_Application_MVN__c,
                    Status_MVN__c = 'Not Started'
                )
            );
        }

        insert memberStages;
    }


    static void setup(Boolean skippable) {
        createProgram(skippable);
        createBIStage();
        createPatients();
        createPrescriber();
        createProgramMembers();
        createApplications();
        createMemberStages();
    }

    static void createBCRecords(Boolean hasHealthInsurance) {
        coverages = new List<Benefits_Coverage_MVN__c>();
        for (Application_MVN__c application : applications) {
            coverages.add(
                new Benefits_Coverage_MVN__c(
                    Program_Member_MVN__c = application.Program_Member_MVN__c,
                    Application_MVN__c = application.Id,
                    Health_Insurance_MVN__c = hasHealthInsurance ? 'Yes' : 'No'
                )
            );
        }
        insert coverages;
    }

    static void startBIStages() {
        for (Program_Member_Stage_MVN__c memberStage : memberStages) {
            memberStage.Status_MVN__c = 'Started';
        }
        update memberStages;
    }

    @isTest
	static void itShouldSkipBenefitsInvestigation() {
        setup(true);
		System.assertEquals(DATA_SIZE, [select count() from Program_Member_Stage_MVN__c where Status_MVN__c = 'Not Started']);

        createBCRecords(false);
        Test.startTest();
        startBIStages();
        System.assertEquals(DATA_SIZE, [select count() from Program_Member_MVN__c where BI_Evaluation_In_Process_MVN__c = true]);
        Test.stopTest();
        System.assertEquals(DATA_SIZE, [
            SELECT
                Count()
            FROM
                Benefits_Coverage_MVN__c
            WHERE
                Benefits_Investigation_Status_MVN__c = 'Eligible'
        ]);
        System.assertEquals(DATA_SIZE, [select count() from Program_Member_MVN__c where BI_Evaluation_In_Process_MVN__c = false]);
	}

    @isTest
	static void itShouldNotSkipBenefitsInvestigationIfHasHealthInsurance() {
        setup(true);
		System.assertEquals(DATA_SIZE, [select count() from Program_Member_Stage_MVN__c where Status_MVN__c = 'Not Started']);

        createBCRecords(true);
        Test.startTest();
        startBIStages();
        Test.stopTest();
        System.assertEquals(DATA_SIZE, [
            SELECT
                Count()
            FROM
                Benefits_Coverage_MVN__c
            WHERE
                Benefits_Investigation_Status_MVN__c = 'Pending'
        ]);
	}

    @isTest
	static void itShouldNotSkipBenefitsInvestigationIfProgramNotSkippable() {
        setup(false);

        createBCRecords(true);
        Test.startTest();
        startBIStages();
        Test.stopTest();
        System.assertEquals(DATA_SIZE, [
            SELECT
                Count()
            FROM
                Benefits_Coverage_MVN__c
            WHERE
                Benefits_Investigation_Status_MVN__c = 'Pending'
        ]);
    }
}