/*
 * MDMValueMappingMVN
 * @created by: Pablo Roldan
 * @created Date: Oct 20, 2017
 * @description: Wrapper class to manage retrieving the MDM Field Values custom metadata type
 */
public with sharing class MDMValueMappingMVN {
    public List<MDM_Value_Mapping_MVN__mdt> settings { get; private set;}

    private static List<MDM_Value_Mapping_MVN__mdt> testMock;

    public MDMValueMappingMVN() {
        Map<String, Schema.SObjectField> MDMValueMappingMVN = MDM_Value_Mapping_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
        List<String> mdmValuesMappingFields = new List<String>(MDMValueMappingMVN.keySet());

        String queryString = 'SELECT ' + String.escapeSingleQuotes(String.join(mdmValuesMappingFields, ','))
                                + ' FROM MDM_Value_Mapping_MVN__mdt';

        settings = Database.query(queryString);

        if(testMock != null){
            settings = testMock;
        }
    }

    @TestVisible
    private static void setMocks(List<MDM_Value_Mapping_MVN__mdt> mocks){
        testMock = mocks;
    }

    public List<MDM_Value_Mapping_MVN__mdt> filterByFieldMapping(Id mdmFieldMappingId) {
        List<MDM_Value_Mapping_MVN__mdt> mdmValuesByFieldMappings = new List<MDM_Value_Mapping_MVN__mdt>();

        for(MDM_Value_Mapping_MVN__mdt setting : settings) {
            String mdmValueFieldMappingId = setting.MDM_Field_Mapping_MVN__c;

            if(mdmValueFieldMappingId == mdmFieldMappingId) {
                mdmValuesByFieldMappings.add(setting);
            }
        }

        return mdmValuesByFieldMappings;
    }
}