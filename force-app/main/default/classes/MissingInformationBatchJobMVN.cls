/**
 * @author      Mavens
 * @date        November 2018
 * @group       MissingInformationBatchJob
 * @description Batch job that retreives all Applications under Missing Information status that
 *              have exceeded the time that can stay under that status.
 *              For those applications execute the following actions:
 *              - Update Application Status to "Denied"
 *              - Update the Case Status for all open cases associated to that application to "Cancelled"
 *              - Override all Eligibility Results with a Result of "Missing Information" to "Denied"
 */
global class MissingInformationBatchJobMVN implements Database.Batchable<sObject>, Schedulable {

    /**
    * EXECUTE SCHEDULE
    * @description: Call batch when the schedule runs
    */
    global void execute(SchedulableContext sc) {
        MissingInformationBatchJobMVN missingInformationBatchJob = new MissingInformationBatchJobMVN();
        database.executebatch(missingInformationBatchJob);
    }

    /**
     * @description Query Applications under "Missing Information" status that have a Missing Information
     *              Close Date lower or equal to today. Get all open cases associated to it and also
     *              the last eligibility engine run record for that application.
     */
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Date today = Date.Today();
        return Database.getQueryLocator([
            SELECT
                Id,
                Status_MVN__c,
                (SELECT
                    Id,
                    Last_Run_MVN__c
                FROM
                    App_Eligibility_Engine_Results_MVN__r
                WHERE
                    Last_Run_MVN__c = true
                ORDER BY
                    CreatedDate DESC
                LIMIT 1),
                (SELECT
                    Id,
                    Status
                FROM
                    Cases__r
                WHERE
                    Status = 'Open'
                )
            FROM
                Application_MVN__c
            WHERE
                Status_MVN__c = :Label.Missing_Information_MVN
            AND
                Missing_Information_Close_Date_MVN__c != null
            AND
                Missing_Information_Close_Date_MVN__c <= :today
        ]);
    }

    /**
     * @description Process the application list retreived in the start method and do:
     *  - update the last run flag of the most recent eligibility engine run to false
     *  - create new eligibility engine run records as "Denied"
     *  - create new eligibility engine result records, moving the ones that were as "Missing Information"
     *    to "Denied"
     *  - update application status to "Denied"
     *  - update case status from "Open" to "Cancelled"
     */
    global void execute(Database.BatchableContext bc, List<Application_MVN__c> applicationList){

        // need to stop the next parent from triggering as we are stopping the program member from continuing
        ProgramMemberStageManagerMVN.allowNextParentStart = false;

        List<Eligibility_Engine_Run_MVN__c> existingEngineRunToUpdateLastRun = new List<Eligibility_Engine_Run_MVN__c>();
        List<Eligibility_Engine_Run_MVN__c> newEngineRunsToCreate = new List<Eligibility_Engine_Run_MVN__c>();
        Map<Id,Id> mapExistingEngineRunIdByApplicationId = new Map<Id,Id>();
        Set<Id> existingEngineRunIds = new Set<Id>();
        List<Case> caseList = new List<Case>();

        this.processApplicationList(applicationList,
                                    newEngineRunsToCreate,
                                    mapExistingEngineRunIdByApplicationId,
                                    existingEngineRunIds,
                                    existingEngineRunToUpdateLastRun,
                                    caseList);


        Savepoint sp = Database.setSavepoint();
        try {
            update existingEngineRunToUpdateLastRun;
            insert newEngineRunsToCreate;
            update applicationList;
            if (!newEngineRunsToCreate.isEmpty()) {
                this.createEngineResults(existingEngineRunIds, newEngineRunsToCreate, mapExistingEngineRunIdByApplicationId);
            }
            update caseList;
        } catch (Exception exc) {
            System.debug('\n\n\n### ' + exc.getStackTraceString() + '\n\n\n');
            Database.rollback(sp);
        }
    }

    /**
     * @description Go through the list of apolications and build the variables neded to execute
     * the actions:
     *      - Set application status to "Denied"
     *      - Build list with new eligibility engine run records to be inserted
     *      - Set case status to "Cancelled"
     *      - Build helper variables
     */
    private void processApplicationList(List<Application_MVN__c> applicationList,
                                        List<Eligibility_Engine_Run_MVN__c> newEngineRunsToCreate,
                                        Map<Id,Id> mapExistingEngineRunIdByApplicationId,
                                        Set<Id> existingEngineRunIds,
                                        List<Eligibility_Engine_Run_MVN__c> existingEngineRunToUpdateLastRun,
                                        List<Case> caseList) {

        for (Application_MVN__c application : applicationList) {
            application.Status_MVN__c = 'Denied';

            if (!application.App_Eligibility_Engine_Results_MVN__r.isEmpty()) {
                newEngineRunsToCreate.add(
                    new Eligibility_Engine_Run_MVN__c(
                        Application_MVN__c = application.Id,
                        Run_Datetime_MVN__c = Datetime.now(),
                        Result_MVN__c = EligibilityEngineMVN.RULEDENIED,
                        Last_Run_MVN__c = true,
                        Missing_Information_Timeout_MVN__c  = true
                    )
                );
                Eligibility_Engine_Run_MVN__c runEngine = application.App_Eligibility_Engine_Results_MVN__r.get(0);
                mapExistingEngineRunIdByApplicationId.put(application.Id, runEngine.Id);
                existingEngineRunIds.add(runEngine.Id);
                runEngine.Last_Run_MVN__c = false;
                existingEngineRunToUpdateLastRun.add(runEngine);
            }
            for (Case myCase : application.Cases__r) {
                myCase.Status = 'Cancelled';
            }
            caseList.addAll(application.Cases__r);
        }
    }

    /**
     * @description Insert new Eligibility Engine Result records copying over the results from
     *              previous run and updating the missing information ones to denied.
     * @param existingEngineRunIds set of ids for the most recent eligibility engine run record
     *                              associated to each application
     * @param newEngineRuns list of eligibility engine run that were newly created
     * @param mapExistingEngineRunIdByApplicationId map of eligibility engine run ids by application id
     */
    private void createEngineResults(Set<Id> existingEngineRunIds,
                                    List<Eligibility_Engine_Run_MVN__c> newEngineRuns,
                                    Map<Id, Id> mapExistingEngineRunIdByApplicationId) {

        List<Eligibility_Engine_Result_MVN__c> existingEngineResults = getResultsAndUpdateLastRun(existingEngineRunIds);
        Map<Id,Id> mapNewEngineRunIdByOldEngineRunId = buildMapNewEngineRunIdByOldEngineRunId(newEngineRuns,
                                                            mapExistingEngineRunIdByApplicationId);

        List<Eligibility_Engine_Result_MVN__c> newEngineResults = new List<Eligibility_Engine_Result_MVN__c>();
        Eligibility_Engine_Result_MVN__c newEngineResult;
        for (Eligibility_Engine_Result_MVN__c engineResult : existingEngineResults) {
            newEngineResult = new Eligibility_Engine_Result_MVN__c(
                Name = engineResult.Name,
                Cannot_be_overriden_MVN__c = engineResult.Cannot_be_overriden_MVN__c,
                Result_Message_MVN__c = engineResult.Result_Message_MVN__c,
                Result_MVN__c = engineResult.Result_MVN__c,
                Last_Run_MVN__c = true,
                Eligibility_Engine_Run_MVN__c = mapNewEngineRunIdByOldEngineRunId.get(engineResult.Eligibility_Engine_Run_MVN__c)
            );
            if (engineResult.Result_MVN__c == EligibilityEngineMVN.RULEMISSINGINFO) {
                newEngineResult.Result_MVN__c = EligibilityEngineMVN.RULEDENIED;
            }
            newEngineResults.add(newEngineResult);
        }
        insert newEngineResults;
    }

    /**
     * @description get the eligibility engine results associated to the last run and update
     *              the last run flag to false
     * @param existingEngineRunIds set of eligibility engine ids
     * @return list of eligibility engine results
     */
    private List<Eligibility_Engine_Result_MVN__c> getResultsAndUpdateLastRun(Set<Id> existingEngineRunIds) {
        List<Eligibility_Engine_Result_MVN__c> existingEngineResults = [
            SELECT
                Name,
                Cannot_be_overriden_MVN__c,
                Result_MVN__c,
                Result_Message_MVN__c,
                Eligibility_Engine_Run_MVN__c
            FROM
                Eligibility_Engine_Result_MVN__c
            WHERE
                Eligibility_Engine_Run_MVN__c IN :existingEngineRunIds
        ];

        for (Eligibility_Engine_Result_MVN__c engineResult : existingEngineResults) {
            engineResult.Last_Run_MVN__c = false;
        }
        update existingEngineResults;
        return existingEngineResults;
    }

    /**
     * @description Build map linking the existing eligibility engine run id and the new one we just
     *              created
     * @return   map of new eligibility engine run id by existing eligibility engine run id
     */
    private Map<Id,Id> buildMapNewEngineRunIdByOldEngineRunId(List<Eligibility_Engine_Run_MVN__c> newEngineRuns,
                                                Map<Id,Id> mapExistingEngineRunIdByApplicationId) {
        Map<Id,Id> mapNewEngineRunIdByOldEngineRunId = new Map<Id,Id>();
        for (Eligibility_Engine_Run_MVN__c engineRun : newEngineRuns) {
            Id oldEngineRunId = mapExistingEngineRunIdByApplicationId.get(engineRun.Application_MVN__c);
            mapNewEngineRunIdByOldEngineRunId.put(oldEngineRunId, engineRun.Id);
        }
        return mapNewEngineRunIdByOldEngineRunId;
    }

    global void finish(Database.BatchableContext bc){}
}