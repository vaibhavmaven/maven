/****************************************************************************************************************************** 
  Class Name: PAP_PrescriptionUpdateHandler
  Created By: PSL
  Created Date: 08-08-2019
  Description: This class is a handler for Trigger PrescriptionMVN. It will be used to update fields on Application related to 
  Prescription when a new prescription is created or it is updated.
 ******************************************************************************************************************************/
public class PAP_PrescriptionUpdateHandler implements TriggerHandlerMVN.HandlerInterface{
    public void handle(){
        List<Application_MVN__c> applicationList = new List<Application_MVN__c>(); 
        if(Trigger.isInsert)
        {
            Set<Id> applicationIdSet = new Set<Id>();
            List<Prescription_MVN__c> prescList = (List<Prescription_MVN__c>)(Trigger.New);
            System.debug('Inside before insert trigger line 8');
            for(SObject presc: Trigger.new)
            {
                if(presc.get('Application_MVN__c') != null){
                    applicationIdSet.add((Id)presc.get('Application_MVN__c')); 
                }
            }
            Map<Id,Application_MVN__c> idAppMap = new Map<Id,Application_MVN__c>();
            idAppMap = new Map<Id,Application_MVN__c>([select id,Contains_First_Product_PAP__c,Contains_Second_Product_PAP__c, 
                       First_Product_Counter_PAP__c,Second_Product_Counter_PAP__c 
                       from Application_MVN__c where id IN :applicationIdSet]);
            
            for(Prescription_MVN__c presc: prescList){
                if(presc.Application_MVN__c != null){
                    if(presc.Product_Type_PAP__c =='First Product')
                    {  if( idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c ==null){
                        idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c=0;
                        
                      }
                        idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c ++;
                        idAppMap.get(presc.Application_MVN__c).Contains_First_Product_PAP__c = true; 
                    }
                    else if(presc.Product_Type_PAP__c == 'Second Product')
                    {   if(idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c==null){
                    
                            idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c=0;
                         }
                        idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c ++;
                        idAppMap.get(presc.Application_MVN__c).Contains_Second_Product_PAP__c = true;
                    }
                }//end of if   
            }//end of for
            update idAppMap.values();
        }//end of isInsert
        else if(Trigger.isUpdate)
        {
            List<Prescription_MVN__c> prescList = new List<Prescription_MVN__c>();
            Set<Id> appIdSet = new Set<Id>();
            Boolean productUpdateSettings = getProductUpdateSetting(); 
            for(Id prescId: Trigger.newMap.keySet()){
                if(//((Prescription_MVN__c)Trigger.oldMap.get(prescId)).Product_Type_PAP__c != null &&
                   (((Prescription_MVN__c)Trigger.newMap.get(prescId)).Product_Type_PAP__c != null &&
                   ((Prescription_MVN__c)Trigger.oldMap.get(prescId)).Product_Type_PAP__c != ((Prescription_MVN__c)Trigger.newMap.get(prescId)).Product_Type_PAP__c) ||
                   (//((Prescription_MVN__c)Trigger.oldMap.get(prescId)).Attached_Product_Updated_PAP__c != ((Prescription_MVN__c)Trigger.newMap.get(prescId)).Attached_Product_Updated_PAP__c &&
                    productUpdateSettings == true)){
                       System.debug('Executing update logic');
                       appIdSet.add(((Prescription_MVN__c)Trigger.newMap.get(prescId)).Application_MVN__c);
                       prescList.add((Prescription_MVN__c)Trigger.newMap.get(prescId));
                   }
            }
            Map<Id,Application_MVN__c> idAppMap = new Map<Id,Application_MVN__c>();
            idAppMap = new Map<Id,Application_MVN__c>([select id,Contains_First_Product_PAP__c,Contains_Second_Product_PAP__c, 
                       First_Product_Counter_PAP__c,Second_Product_Counter_PAP__c 
                       from Application_MVN__c where id IN :appIdSet]);
            // for each prescription in this trigger.
            for(Prescription_MVN__c presc: prescList){
                if(presc.Application_MVN__c != null){
                    idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c = idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c == null ? 0 : idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c;
                    idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c = idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c == null ? 0 : idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c; 
                    if(presc.Product_Type_PAP__c =='First Product')
                    {
                        idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c ++;
                        idAppMap.get(presc.Application_MVN__c).Contains_First_Product_PAP__c = true; 
                        if((idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c > 0 && productUpdateSettings != true) || // only when the prescription is updated.
                           (productUpdateSettings == true && ((Prescription_MVN__c)Trigger.newMap.get(presc.Id)).Decrement_Counter_PAP__c == true ))  
                        {
                        	idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c --;                            
                        }
                        if(idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c < 1)
                        {   // if no second products uncheck the related check box. 
                            idAppMap.get(presc.Application_MVN__c).Contains_Second_Product_PAP__c = false;
                        }
                    } // if first product. 
                    else if(presc.Product_Type_PAP__c == 'Second Product')
                    {
                        idAppMap.get(presc.Application_MVN__c).Second_Product_Counter_PAP__c ++;
                        idAppMap.get(presc.Application_MVN__c).Contains_Second_Product_PAP__c = true;
                        if((idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c > 0 && productUpdateSettings != true) ||  // only when the prescription is updated.
                           (productUpdateSettings == true && ((Prescription_MVN__c)Trigger.newMap.get(presc.Id)).Decrement_Counter_PAP__c == true ))    
                        {
                       		idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c --;
                        }
                        if(idAppMap.get(presc.Application_MVN__c).First_Product_Counter_PAP__c < 1)
                        {   // if no first product, uncheck the related checkbox
                            idAppMap.get(presc.Application_MVN__c).Contains_First_Product_PAP__c = false;
                        }
                    } // if second product 
                } // if valid application.   
            }//end of prescription for loop
            update idAppMap.values();
        }//end of is update
        
    }//end of handle method
    
    private boolean getProductUpdateSetting(){
        List<Product_Update_Setting_PAP__mdt> updateSettings = [select Update_Existing_Product_Type_PAP__c 
                                                                from Product_Update_Setting_PAP__mdt 
                                                                where MasterLabel = 'Update Products' 
                                                                limit 1]; 
        if(updateSettings == null || updateSettings.size() == 0){
            system.debug('ERROR: Custom metadata Product_Update_Setting_PAP__mdt is not set.'); 
            return false;             
        }
        // if the checkbox is not checked in custom metadata we will return from here. 
        if(updateSettings[0].Update_Existing_Product_Type_PAP__c == false){
            system.debug('Update_Existing_Product_Type_PAP__c is set to false. Not updating Product Type and Prescriptions.'); 
            return false; 
        }// if         
        return true; 
    }
    
}