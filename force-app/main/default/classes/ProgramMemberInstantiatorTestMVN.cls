/**
 *  ProgramMemberInstantiatorTestMVN
 *  Created By:     Aggen
 *  Created On:     10/25/2015
 *  Description:    This class is responsible for testing the ProgramMemberInstantiatorMVN class
 *
 **/

 /*
  * Program Stages
  *     Program Initiation --> Welcome Call --> Secondary Greeting
  *     Benefits Investigation --> Prior Authorization --> Verify Patient Coverage
  *     Education --> Nurse Visit --> Second Nurse Visit
  */

@isTest
private class ProgramMemberInstantiatorTestMVN {
    static Program_MVN__c program;
    static Program_Member_MVN__c programMember;
    static Program_Member_MVN__c programMember2;
    static Account memberAccount;
    static Account memberAccount2;

    static Program_Stage_MVN__c programInitiation;
    static Program_Stage_MVN__c benefitsInvestigation;
    static Program_Stage_MVN__c education;

    static {
        init();
    }
    
    @isTest
    static void testAgentCreatesProgramMemberStages() {
        Test.startTest();
        programMember = TestDataFactoryMVN.createProgramMember(program, memberAccount);
        Test.stopTest();

        programMemberStagesWereCreated();
    }

    @isTest
    static void testJourneyIsNotStarted() {
        programMember = new Program_Member_MVN__c(Program_MVN__c = program.Id, Member_MVN__c = memberAccount.Id, Do_Not_Initialize_MVN__c = true);

        Test.startTest();
        insert programMember;
        Test.stopTest();

        caseWasNotCreated();
    }

    @isTest
    static void testJourneyIsStarted() {
        programMember = new Program_Member_MVN__c(Program_MVN__c = program.Id, Member_MVN__c = memberAccount.Id, Do_Not_Initialize_MVN__c = false);

        Test.startTest();
        insert programMember;
        Test.stopTest();

        caseWasCreated();
    }

    @isTest
    static void testStartAtTheBeginning() {
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>();

        Test.startTest();
        programMembers = TestDataFactoryMVN.createProgramMembers(program, null);
        Test.stopTest();

        List<Program_Member_Stage_MVN__c> programMemberStage1List = [select Status_MVN__c, Closed_Date_MVN__c, IsClosed_MVN__c, Program_Stage_MVN__c
                                                                        from Program_Member_Stage_MVN__c
                                                                        where Program_Member_MVN__c in :programMembers
                                                                        and (Program_Stage_MVN__c = :programInitiation.Id)];

        System.assertEquals(200, programMemberStage1List.size());
        for(Program_Member_Stage_MVN__c programMemberStage : programMemberStage1List) {
            System.assertEquals('Started', programMemberStage.Status_MVN__c);
            System.assertEquals(null, programMemberStage.Closed_Date_MVN__c);
            System.assertEquals(false, programMemberStage.IsClosed_MVN__c);
        }

        List<Program_Member_Stage_MVN__c> programMemberStages = [select Parent_Program_Member_Stage_MVN__r.Program_Stage_MVN__c,
                                                                    Status_MVN__c, Closed_Date_MVN__c, IsClosed_MVN__c from Program_Member_Stage_MVN__c where Program_Stage_MVN__r.Name = 'Welcome Call'];

        System.assertEquals(200, programMemberStages.size());

        for(Program_Member_Stage_MVN__c programMemberStage : programMemberStages) {
            System.assertEquals('Started', programMemberStage.Status_MVN__c);
            System.assertEquals(null, programMemberStage.Closed_Date_MVN__c);
            System.assertEquals(false, programMemberStage.IsClosed_MVN__c);
        }

        List<Case> cases = [select Id, Status from Case where Program_Member_Stage_MVN__r.Parent_Program_Member_Stage_MVN__c in :programMemberStage1List];

        System.assertEquals(200, cases.size());
    }

    @isTest
    static void testStartAtThirdStage() {
        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>();

        Test.startTest();
        programMembers = TestDataFactoryMVN.createProgramMembers(program, education.Id, 120);
        Test.stopTest();

        List<Program_Member_Stage_MVN__c> programMemberStage1And2List = [select Status_MVN__c, Closed_Date_MVN__c, IsClosed_MVN__c, Program_Stage_MVN__c
                                                                        from Program_Member_Stage_MVN__c
                                                                        where Program_Member_MVN__c in :programMembers
                                                                        and (Program_Stage_MVN__c = :programInitiation.Id or Program_Stage_MVN__c = :benefitsInvestigation.Id)];
        List<Program_Member_Stage_MVN__c> programMemberStage3List = [select Status_MVN__c, Closed_Date_MVN__c, IsClosed_MVN__c
                                                                        from Program_Member_Stage_MVN__c
                                                                        where Program_Member_MVN__c in :programMembers
                                                                        and Program_Stage_MVN__c = :education.Id];

        System.assertEquals(240, programMemberStage1And2List.size());
        for(Program_Member_Stage_MVN__c programMemberStage : programMemberStage1And2List) {
            System.assertEquals('Completed', programMemberStage.Status_MVN__c);
            System.assertEquals(Date.today(), programMemberStage.Closed_Date_MVN__c);
            System.assertEquals(true, programMemberStage.IsClosed_MVN__c);
        }

        List<Program_Member_Stage_MVN__c> programMemberStages = [select Parent_Program_Member_Stage_MVN__r.Program_Stage_MVN__c,
                                                                    Status_MVN__c, Closed_Date_MVN__c, IsClosed_MVN__c from Program_Member_Stage_MVN__c where Parent_Program_Member_Stage_MVN__c in :programMemberStage1And2List];

        System.assertEquals(360, programMemberStages.size());

        for(Program_Member_Stage_MVN__c programMemberStage : programMemberStages) {
            System.assertEquals('Completed', programMemberStage.Status_MVN__c);
            System.assertEquals(Date.today(), programMemberStage.Closed_Date_MVN__c);
            System.assertEquals(true, programMemberStage.IsClosed_MVN__c);
        }

        List<Case> cases = [select Id, Status from Case where Program_Member_Stage_MVN__r.Parent_Program_Member_Stage_MVN__c in :programMemberStage3List];

        System.assertEquals(120, cases.size());
    }

    @isTest
    static void testCompleteThirdStage() {
        programMember = new Program_Member_MVN__c(Program_MVN__c = program.Id, Member_MVN__c = memberAccount.Id, Do_Not_Initialize_MVN__c = false, Starting_Program_Stage_ID_MVN__c = education.Id);

        insert programMember;

        System.assertEquals(null, [select Starting_Program_Stage_ID_MVN__c from Program_Member_MVN__c where Id = :programMember.Id].Starting_Program_Stage_ID_MVN__c);

        Case activity = [select Id, Status from Case where Subject = 'Second Nurse Visit'];
        activity.Status = 'Closed';
        activity.Type = Case.Type.getDescribe().getPicklistValues().get(0).getValue();

        ProgramMemberStageManagerMVN.startAtSpecificStage = false;

        Test.startTest();
        update activity;
        Test.stopTest();

        System.assertEquals('Completed', [select Status_MVN__c from Program_Member_Stage_MVN__c where Program_Stage_MVN__r.Name = 'Second Nurse Visit'].Status_MVN__c);
        //System.assertEquals('Started', [select Status_MVN__c from Program_Member_Stage_MVN__c where Program_Stage_MVN__r.Name = 'Second Nurse Visit'].Status_MVN__c);
        /*
                activity = [select Id, Status from Case where Subject = 'Second Nurse Visit'];
                activity.Status = 'Closed';
                update activity;
        */

        for(Program_Member_Stage_MVN__c pmStage : [select Status_MVN__c from Program_Member_Stage_MVN__c]) {
            // Verify that all of the program member stages are completed
            System.assertEquals('Completed', pmStage.Status_MVN__c);
        }
    }

    static void programMemberStagesWereCreated() {
        List<Program_Member_Stage_MVN__c> parentStages = [SELECT Id, Program_Member_MVN__c, Program_Member_MVN__r.Program_MVN__c FROM Program_Member_Stage_MVN__c where Parent_Program_Member_Stage_MVN__c = null];
        List<Program_Member_Stage_MVN__c> childStages = [SELECT Id, Program_Member_MVN__c, Program_Member_MVN__r.Program_MVN__c FROM Program_Member_Stage_MVN__c where Parent_Program_Member_Stage_MVN__c != null];

        System.assertEquals(3, parentStages.size());
        System.assertEquals(4, childStages.size());

        System.assertEquals(programMember.Id, parentStages[0].Program_Member_MVN__c);
        System.assertEquals(programMember.Id, childStages[0].Program_Member_MVN__c);

        System.assertEquals(program.Id, parentStages[0].Program_Member_MVN__r.Program_MVN__c);
        System.assertEquals(program.Id, childStages[0].Program_Member_MVN__r.Program_MVN__c);

        System.assertEquals(4, [select count() from Program_Member_Stage_Dependency_MVN__c]);
    }

    static void caseWasNotCreated() {
        List<Case> cases = [SELECT Id FROM Case];
        System.assertEquals(0, cases.size());
    }

    static void caseWasCreated() {
        List<Case> cases = [SELECT Id FROM Case];
        System.assertEquals(1, cases.size());
    }

    private static void init() {
        TestDataFactoryMVN.createPSSettings();

        memberAccount = TestDataFactoryMVN.createMember();
        memberAccount2 = TestDataFactoryMVN.createMember();

        program = TestDataFactoryMVN.createProgram();

        // Single default stage generated when a Program is first created
        programInitiation = [select Id from Program_Stage_MVN__c];

        programInitiation.Name = 'Program Initiation';
        update programInitiation;

        Program_Stage_MVN__c welcomeCall = TestDataFactoryMVN.createChildProgramStage(program, programInitiation, 'Welcome Call');
        Program_Stage_MVN__c secondaryGreeting = TestDataFactoryMVN.createChildProgramStage(program, programInitiation, 'Secondary Greeting');

        benefitsInvestigation = TestDataFactoryMVN.createParentProgramStage(program, 'Benefits Investigation');
        Program_Stage_MVN__c priorAuthorization = TestDataFactoryMVN.createChildProgramStage(program, benefitsInvestigation, 'Prior Authorization');
        //Program_Stage_MVN__c verifyPatientCoverage = TestDataFactoryMVN.createChildProgramStage(program, benefitsInvestigation, 'Verify Patient Coverage');

        benefitsInvestigation.Stage_Sequence_Number_MVN__c = 2;
        update benefitsInvestigation;

        education = TestDataFactoryMVN.createParentProgramStage(program, 'Education');
        //Program_Stage_MVN__c nurseVisit = TestDataFactoryMVN.createChildProgramStage(program, education, 'Nurse Visit');
        Program_Stage_MVN__c secondNurseVisit = TestDataFactoryMVN.createChildProgramStage(program, education, 'Second Nurse Visit');

        education.Stage_Sequence_Number_MVN__c = 3;
        update education;

        Program_Stage_Dependency_MVN__c dependency1 = TestDataFactoryMVN.createStageDependency(programInitiation, welcomeCall);
        Program_Stage_Dependency_MVN__c dependency2 = TestDataFactoryMVN.createStageDependency(welcomeCall, secondaryGreeting);

        Program_Stage_Dependency_MVN__c dependency3 = TestDataFactoryMVN.createStageDependency(benefitsInvestigation, priorAuthorization);
        //Program_Stage_Dependency_MVN__c dependency4 = TestDataFactoryMVN.createStageDependency(priorAuthorization, verifyPatientCoverage);

        //Program_Stage_Dependency_MVN__c dependency5 = TestDataFactoryMVN.createStageDependency(education, nurseVisit);
        Program_Stage_Dependency_MVN__c dependency6 = TestDataFactoryMVN.createStageDependency(education, secondNurseVisit);
    }
}