/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description tests EligibilityEngineCtrlMVN
 */
@isTest private class EligibilityEngineCtrlTestMVN {
    private static User caseManager;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static Account member;
    private static Application_MVN__c application;

    static {
        System.runAs(new User(Id=UserInfo.getUserId())) {
            createCaseManager();
        }

        System.runAs(caseManager) {
            program = TestDataFactoryMVN.createProgram();
            program.Program_Id_MVN__c = 'Test_Program_MVN';
            update program;
            member = TestDataFactoryMVN.createMember();
            member.ShippingStreet = 'Test Address Line One';
            member.NumberOfEmployees = 10;
            update member;
            programMember = TestDataFactoryMVN.createProgramMember(program, member);
            application = createApplication();
            programMember.Active_Application_MVN__c = application.Id;
            programMember.Program_Id_MVN__c = 'Test_Program_MVN';
            update programMember;
        }
    }

    static void createCaseManager() {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        insert caseManager;
        TestFactoryUserMVN userFactory = new TestFactoryUserMVN();
        userFactory.setUserPermissionSet(caseManager.Id, 'DDP_User');
        userFactory.setUserPermissionSet(caseManager.Id, 'DDP_User_All');
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        return testApplication;
    }

    @isTest private static void runRulesToSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        System.runAs(caseManager) {
            createRules();
            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);

            Test.startTest();
                EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);
                actualController.runEngine();
                EligibilityEngineCtrlMVN.ResultWrapper actualResultWrapper = actualController.resultForProgramMember;
            Test.stopTest();

            Eligibility_Engine_Run_MVN__c actualRun = [
                SELECT
                    Id,
                    Run_Datetime_MVN__c,
                    Result_MVN__c
                FROM
                    Eligibility_Engine_Run_MVN__c
                WHERE
                    Application_MVN__c = :application.Id
            ];
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualRun.Result_MVN__c);
            System.assertEquals(Date.today(), actualRun.Run_Datetime_MVN__c.date());
            List<Eligibility_Engine_Result_MVN__c> actualResults = [
                SELECT
                    Result_MVN__c,
                    Name
                FROM
                    Eligibility_Engine_Result_MVN__c
                WHERE
                    Eligibility_Engine_Run_MVN__c = :actualRun.Id
            ];
            System.assertEquals(3, actualResults.size());
            for(Integer index = 0; index < actualResults.size(); index++) {
                Eligibility_Engine_Result_MVN__c actualResult = actualResults.get(index);
                System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResult.Result_MVN__c);
            }
            Eligibility_Engine_Setting_MVN__mdt setting = EligibilityEngineSettingMVN.passedSettings;
            System.assertEquals(setting.Result_Icon_Name_MVN__c, actualResultWrapper.iconName);
            System.assertEquals(setting.Result_Color_MVN__c, actualResultWrapper.color);
            System.assertEquals(setting.Result_Label_MVN__c, actualResultWrapper.result);
        }
    }

    @isTest private static void runRulesToNoSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        System.runAs(caseManager) {
            createRules();
            member.ShippingStreet = null;
            member.NumberOfEmployees = 8;
            update member;
            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);

            Test.startTest();
                EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);
                actualController.runEngine();
                EligibilityEngineCtrlMVN.ResultWrapper actualResultWrapper = actualController.resultForProgramMember;
            Test.stopTest();

            Eligibility_Engine_Run_MVN__c actualRun = [
                SELECT
                    Id,
                    Run_Datetime_MVN__c,
                    Result_MVN__c
                FROM
                    Eligibility_Engine_Run_MVN__c
                WHERE
                    Application_MVN__c = :application.Id
            ];
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualRun.Result_MVN__c);
            System.assertEquals(Date.today(), actualRun.Run_Datetime_MVN__c.date());
            List<Eligibility_Engine_Result_MVN__c> actualResults = [
                SELECT
                    Result_MVN__c,
                    Name
                FROM
                    Eligibility_Engine_Result_MVN__c
                WHERE
                    Eligibility_Engine_Run_MVN__c = :actualRun.Id
            ];
            System.assertEquals(3, actualResults.size());
            for(Integer index = 0; index < actualResults.size(); index++) {
                Eligibility_Engine_Result_MVN__c actualResult = actualResults.get(index);
                System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResult.Result_MVN__c);
            }
            Eligibility_Engine_Setting_MVN__mdt setting = EligibilityEngineSettingMVN.deniedSettings;
            System.assertEquals(setting.Result_Icon_Name_MVN__c, actualResultWrapper.iconName);
            System.assertEquals(setting.Result_Color_MVN__c, actualResultWrapper.color);
            System.assertEquals(setting.Result_Label_MVN__c, actualResultWrapper.result);
        }
    }

    @isTest private static void runRulesToNoSuccessMissingInfo() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        System.runAs(caseManager) {
            createRuleMissingIfo();
            Map<Id, Eligibility_Engine_Rule_MVN__c> rules = EligibilityEngineMVN.getRulesForProgram(program.Id);
            member.NumberOfEmployees = 8;
            update member;
            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);

            Test.startTest();
                EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);
                actualController.runEngine();
                EligibilityEngineCtrlMVN.ResultWrapper actualResultWrapper = actualController.resultForProgramMember;
            Test.stopTest();

            Eligibility_Engine_Run_MVN__c actualRun = [
                SELECT
                    Id,
                    Run_Datetime_MVN__c,
                    Result_MVN__c
                FROM
                    Eligibility_Engine_Run_MVN__c
                WHERE
                    Application_MVN__c = :application.Id
            ];
            System.assertEquals(EligibilityEngineMVN.RULEMISSINGINFO, actualRun.Result_MVN__c);
            System.assertEquals(Date.today(), actualRun.Run_Datetime_MVN__c.date());
            List<Eligibility_Engine_Result_MVN__c> actualResults = [
                SELECT
                    Result_MVN__c,
                    Name
                FROM
                    Eligibility_Engine_Result_MVN__c
                WHERE
                    Eligibility_Engine_Run_MVN__c = :actualRun.Id
            ];
            System.assertEquals(1, actualResults.size());
            System.assertEquals(rules.values().get(0).Name, actualResults.get(0).Name);
            System.assertEquals(rules.values().get(0).Result_MVN__c, actualResults.get(0).Result_MVN__c);

            Eligibility_Engine_Setting_MVN__mdt setting = EligibilityEngineSettingMVN.missingInfoSettings;
            System.assertEquals(setting.Result_Icon_Name_MVN__c, actualResultWrapper.iconName);
            System.assertEquals(setting.Result_Color_MVN__c, actualResultWrapper.color);
            System.assertEquals(setting.Result_Label_MVN__c, actualResultWrapper.result);
        }
    }

    @isTest private static void testEdit() {
        System.runAs(caseManager) {

            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);
            EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);
            System.assertEquals('init', actualController.mode);

            Test.startTest();
                actualController.edit();
            Test.stopTest();

            System.assertEquals('edit', actualController.mode);
        }

    }

    @isTest private static void testCancel() {
        System.runAs(caseManager) {

            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);
            EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);
            actualController.mode = 'edit';

            Test.startTest();
                actualController.cancel();
            Test.stopTest();

            System.assertEquals('init', actualController.mode);
        }

    }

    @isTest private static void testHideLetterSection() {
        System.runAs(caseManager) {

            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);
            EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);
            actualController.mode = 'letterGen';

            Test.startTest();
                actualController.hideLetterSection();
            Test.stopTest();

            System.assertEquals('init', actualController.mode);
        }

    }

    @isTest private static void testSave() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        Eligibility_Engine_Run_MVN__c resultRun = new Eligibility_Engine_Run_MVN__c(
            Result_MVN__c = EligibilityEngineMVN.RULESUCCESS,
            Application_MVN__c = application.Id,
            Run_Datetime_MVN__c = Datetime.now().addDays(-1)
        );
        insert resultRun;

        List<Eligibility_Engine_Result_MVN__c> results = new List<Eligibility_Engine_Result_MVN__c>();
        for (Integer i=0; i<2; i++) {
            results.add(
                new Eligibility_Engine_Result_MVN__c(
                    Result_MVN__c = EligibilityEngineMVN.RULESUCCESS,
                    Eligibility_Engine_Run_MVN__c = resultRun.Id
                )
            );
        }
        insert results;

        System.runAs(caseManager) {
            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);
            EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);
            for (Eligibility_Engine_Result_MVN__c result : actualController.eligibilityEngineRun.Eligibility_Engine_Results_MVN__r) {
                result.Result_MVN__c = EligibilityEngineMVN.RULEDENIED;
            }

            Test.startTest();
                actualController.save();
            Test.stopTest();

            Eligibility_Engine_Run_MVN__c actualRun = [
                SELECT
                    Id,
                    Run_Datetime_MVN__c,
                    Result_MVN__c
                FROM
                    Eligibility_Engine_Run_MVN__c
                WHERE
                    Application_MVN__c = :application.Id
                ORDER BY
                    Run_Datetime_MVN__c DESC
                LIMIT 1
            ];
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualRun.Result_MVN__c);
            System.assertEquals(Date.today(), actualRun.Run_Datetime_MVN__c.date());
            List<Eligibility_Engine_Result_MVN__c> actualResults = [
                SELECT
                    Result_MVN__c,
                    Name
                FROM
                    Eligibility_Engine_Result_MVN__c
                WHERE
                    Eligibility_Engine_Run_MVN__c = :actualRun.Id
            ];
            System.assertEquals(2, actualResults.size());
            for (Eligibility_Engine_Result_MVN__c result : actualResults) {
                System.assertEquals(EligibilityEngineMVN.RULEDENIED, result.Result_MVN__c);
            }

            Application_MVN__c app = [
                SELECT
                    Status_MVN__c
                FROM
                    Application_MVN__c
                WHERE
                    Id = :application.Id
            ];
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, app.Status_MVN__c);
        }
    }

    @isTest private static void testGenerateEligibilityLettersNoDocGenPackage() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        System.runAs(caseManager) {
            createRules();
            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);
            EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);

            Test.startTest();
                actualController.generateEligibilityLetters();
            Test.stopTest();

            System.assertEquals(null,actualController.nintexParamInfo);
        }
    }

    @isTest private static void testGenerateEligibilityLettersSuccess() {
        TestFactoryEligibilityEngineSettingMVN.setMocks();

        createNintexPackages();

        Eligibility_Engine_Run_MVN__c resultRun = new Eligibility_Engine_Run_MVN__c(
            Result_MVN__c = EligibilityEngineMVN.RULESUCCESS,
            Application_MVN__c = application.Id,
            Run_Datetime_MVN__c = Datetime.now()
        );
        insert resultRun;

        System.runAs(caseManager) {
            createRules();
            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);
            EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);

            Test.startTest();
                actualController.generateEligibilityLetters();
            Test.stopTest();

            System.assertNotEquals(null,actualController.nintexParamInfo);
            System.assertEquals('nintex', actualController.mode);
        }
    }

    @isTest static void testOnErrorCallback() {
        System.runAs(caseManager) {
            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);
            EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);

            Test.startTest();
            actualController.nintexErrorMsg = 'test nintex error';
            actualController.onErrorNintexCallbackApex();
            Test.stopTest();

            ApexPages.Message msg = ApexPages.getMessages().get(0);
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
            System.assertEquals('test nintex error', msg.getDetail());
            System.assertEquals('init', actualController.mode);
        }
    }

    @isTest static void testOnCompleteCallback() {
        System.runAs(caseManager) {
            ApexPages.StandardController stdController = new ApexPages.StandardController(programMember);
            EligibilityEngineCtrlMVN actualController = new EligibilityEngineCtrlMVN(stdController);

            Test.startTest();
            actualController.onCompleteNintexCallbackApex();
            Test.stopTest();

            ApexPages.Message msg = ApexPages.getMessages().get(0);
            System.assertEquals(ApexPages.Severity.INFO, msg.getSeverity());
            System.assertEquals(Label.Eligibility_Engine_Letter_Sending_Success_MVN, msg.getDetail());
            System.assertEquals('init', actualController.mode);
        }
    }

    private static void createNintexPackages() {
        Loop__DDP__c nintexPackage = new Loop__DDP__c(
            Name = 'test package',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'Test_Program_MVN-PM-EligibilityLetters-Approval'
        );

        insert nintexPackage;

        Loop__DDP_Integration_Option__c ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Attach').getRecordTypeId();
        insert ddpOption;
    }

    private static void createRules() {
        Eligibility_Engine_Rule_MVN__c thresholdRuleMax = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Max Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEDENIED,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Less_Than_MVN',
            Value_MVN__c = '9',
            Active_MVN__c = true
        );
        Eligibility_Engine_Rule_MVN__c stringCompRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '2',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEDENIED,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.ShippingStreet',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Equal_To_MVN',
            Value_MVN__c = null,
            Active_MVN__c = true
        );
        Eligibility_Engine_Rule_MVN__c orRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '3',
            Name = 'Or Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEDENIED,
            Type_MVN__c = 'OR_MVN',
            Program_MVN__c = program.Id,
            Active_MVN__c = true
        );
        Eligibility_Engine_Rule_MVN__c thresholdRuleMin = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '4',
            Name = 'Max Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Greater_Than_MVN',
            Value_MVN__c = '3',
            Active_MVN__c = false,
            Parent_Rule_MVN__c = null
        );
        List<Eligibility_Engine_Rule_MVN__c> rules = new List<Eligibility_Engine_Rule_MVN__c> {
            orRule,
            thresholdRuleMax,
            thresholdRuleMin,
            stringCompRule
        };
        insert rules;

        Eligibility_Engine_Rule_MVN__c expectedChildRuleOne = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '5',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.ShippingStreet',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Equal_To_MVN',
            Value_MVN__c = null,
            Parent_Rule_MVN__c = orRule.Id,
            Active_MVN__c = true
        );
        Eligibility_Engine_Rule_MVN__c expectedChildRuleTwo = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '6',
            Name = 'Equal To Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'String_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.ShippingStreet',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Equal_To_MVN',
            Value_MVN__c = null,
            Parent_Rule_MVN__c = orRule.Id,
            Active_MVN__c = true
        );
        List<Eligibility_Engine_Rule_MVN__c> childRules = new List<Eligibility_Engine_Rule_MVN__c> {
            expectedChildRuleOne,
            expectedChildRuleTwo
        };
        insert childRules;
    }

    private static void createRuleMissingIfo() {
        Eligibility_Engine_Rule_MVN__c thresholdRuleMax = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Max Threshold Test Rule',
            Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
            Type_MVN__c = 'Number_Comparison_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Program_Member_MVN__r.Member_MVN__r.NumberOfEmployees',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Comparison_MVN__c = 'Less_Than_MVN',
            Value_MVN__c = '9',
            Active_MVN__c = true
        );
        insert thresholdRuleMax;
    }
}