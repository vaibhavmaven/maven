/**
 *  AddressDeleteTriggerTestMVN
 *  Created By:     Paul Battisson
 *  Created On:     3/23/2016
 *  Description:    This is the test class for the AddressDeleteTriggerMVN class.
 *
 **/

@isTest
private class AddressDeleteTriggerTestMVN {

    static User caseManager;

    static {
        TestFactoryUserMVN userFactory = new TestFactoryUserMVN();

        caseManager = userFactory.create(new Map<String,Object>{
            'ProfileId' => [SELECT Id FROM Profile WHERE Name = 'Patient Case Manager - MVN' LIMIT 1].id,
            'LastName' => 'Manager',
            'FirstName' => 'Case',
            'Alias' => 'cm',
            'Email' => 'mscasemanager@mavens.com',
            'Username' => 'mscasemanager@mavens.com',
            'TimeZoneSidKey' => 'GMT',
            'LanguageLocaleKey' => 'en_US',
            'EmailEncodingKey' => 'UTF-8',
            'LocaleSidKey' => 'en_US'
        });
    }

    @isTest
    private static void testDeleteAddress() {
        System.runAs(caseManager) {
            TestDataFactoryMVN.createPSSettings();

            Account doc = TestDataFactoryMVN.createPhysician();
            Account pat = TestDataFactoryMVN.createMember();
            Program_MVN__c prog = TestDataFactoryMVN.createProgram();
            Program_Stage_MVN__c parent = TestDataFactoryMVN.createParentProgramStage(prog);
            Program_Stage_MVN__c child = TestDataFactoryMVN.createChildProgramStage(prog, parent);
            Program_Member_MVN__c member = TestDataFactoryMVN.createProgramMember(prog, pat);
            Address_vod__c add = TestDataFactoryMVN.createTestAddress(pat);

            List<Contact_Information_MVN__c> info = [SELECT Id FROM Contact_Information_MVN__c WHERE Address_MVN__c = :add.Id];

            if(info.size() == 0) {
                insert new Contact_Information_MVN__c(Address_MVN__c = add.Id, Account_MVN__c = pat.Id, Label_MVN__c = 'Home');
                info = [SELECT Id FROM Contact_Information_MVN__c WHERE Address_MVN__c = :add.Id];
            }

            System.assertEquals(2, info.size(), 'Incorrect number of contact information records retrieved.');

            delete add;

            info = [SELECT Id FROM Contact_Information_MVN__c WHERE Address_MVN__c = :add.Id];
            List<Address_vod__c> adds = [SELECT Id FROM Address_vod__c WHERE Id = :add.Id];

            System.assertEquals(0, info.size(), 'Should have returned no contact information records');
            System.assertEquals(0, adds.size(), 'Should have returned no address records');
        }
    }
}