@isTest
public class PAP_SendNotification_Batch_Test {
    
    static List<Account> patients;
    static Account hcp;
    static Program_MVN__c program;
    static Program_Member_MVN__c programMember;
    Static Application_MVN__c application;
     Static Eligibility_Engine_Run_MVN__c eligibilityEngineRun;
    Static Eligibility_Engine_Result_MVN__c eligibilityEngineResult;
    
    @testSetup
    static void setup(){
        program = createProgram();
        programMember = createProgramMember(); 
        
        application = createApplication();
         eligibilityEngineRun = createEligibilityEngineRun(application);
        eligibilityEngineResult =  createEligibilityEngineResult(eligibilityEngineRun);
       createNintexPackages();
    }
    
    public static Map<String, RecordType> accountRecordTypesByName {
        get {
            if (accountRecordTypesByName == null) {
                accountRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
                    accountRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return accountRecordTypesByName;
        }
        private set;
    }
    
    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        Program_MVN__c program = programFactory.create();
        program.Fulfilment_Method_MVN__c = 'Pharmacy';
        program.SMS_JKC__c = true;
        program.AutoDialer_JKC__c= true;
        program.Email_JKC__c= true;
        program.Mail_JKC__c=true;
        program.Fax_JKC__c=true;
        program.Name = 'Bausch PAP';
        update program;
        return program;
    }
    
    public static List<Account> createPatients(Integer numOfPatients) {
        List<Account> patientsList = new List<Account>();
        List<String> genderList = new List<String>{'Female', 'M'}; 
            Integer counter = 1; 
        for(String genderStr : genderList){
            for (Integer i=0; i<numOfPatients; i++) {
                Account patient= new Account();
                patient.FirstName = 'Test';
                patient.MiddleName = 'testing';
                patient.LastName = 'Patient' + counter;
                patient.Gender_MVN__c = genderStr; 
                patient.Marital_Status_MVN__c = 'Single';                      
                patient.PersonBirthdate = System.today();
                patient.phone='(432-12345)';
                patient.PersonMobilePhone ='(432-12345)';
                patient.Primary_Email_MVN__c = 'test@example.com';
                patient.Fax='(432-12345)';
                patient.BillingStreet='TEST ';
                patient.BillingCity='';
                patient.BillingStateCode='';
                patient.BillingPostalCode='';              
                patient.BillingCountry='';
                patient.BillingState='';
                patient.BillingCountryCode='';
                patient.RecordTypeId = accountRecordTypesByName.get('Patient_MVN').id;
                counter++;          
                patientsList.add(patient);
            }            
        }
        
        insert patientsList;
        Account accPayer = TestDataFactoryMVN.createPayer();
        accPayer.BillingCity = 'TestPayer';
        accPayer.BillingStreet = 'Street1';
        accPayer.BillingPostalCode='62626';
        accPayer.BillingStateCode='NJ';
        accPayer.BillingCountryCode='US';
        update accPayer;
        return patientsList;
    }
    
    static Program_Member_MVN__c createProgramMember() {
        TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
        patients = createPatients(1); 
        
        hcp = accountFactory.createPrescriber(null);
        hcp.Fax= '(432-12345890)';
        hcp.Primary_Email_MVN__c='test@example.com';
        update hcp;
        TestFactoryContactInformationMVN contactInfo = new TestFactoryContactInformationMVN();
        
        Contact_Information_MVN__c contactPhone = contactInfo.constructPhone(patients[0]);
        contactPhone.Label_MVN__c = 'Mobile';
        contactPhone.Primary_MVN__c = True;
        contactPhone.Messages_OK_MVN__c = 'Yes';
        Insert contactPhone;
        
        TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();
        Program_Member_MVN__c programMember = programMemberFactory.create(program, hcp, patients)[0];
        programMember.Patient_Status_MVN__c = 'Opt-In';
        programMember.Program_Id_MVN__c = 'Test';
        programMember.Patient_Status_Date_JKC__c = system.today();
        programMember.Notified_HCP_JKC__c = false;
        programMember.Notified_Patient_JKC__c = false;
        programMember.Notified_Sponsor_JKC__c = false;
        update programMember;
        Document_MVN__c document = new Document_MVN__c(
            Program_Member_MVN__c = programMember.Id,
            Type_MVN__c = 'A'
        );
        insert document;
        Attachment att = new Attachment();
        att.name = 'Test';
        att.Body = Blob.valueOf('Tester');
        att.ParentId = document.Id;
        insert att;
        return programMember;
    }
    
    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id,
                'Status_MVN__c' => 'Fulfilled',
                'Enrollment_Date_MVN__c' => System.today(),
                'Application_Reminder_Date_MVN__c' => System.today()-2,
                'Expiry_Date_MVN__c' => System.today()+30,
                'Known_Allergies_MVN__c'=>'PENECILIN'               
                
                });
       testApplication.Application_Status_Date_JKC__c = system.now();
        update testApplication;
        return testApplication;
    }
    
      static Eligibility_Engine_Run_MVN__c createEligibilityEngineRun(Application_MVN__c app) {
        system.debug('######### applicaion '+app);
        Eligibility_Engine_Run_MVN__c eligibilityEngineRun = new Eligibility_Engine_Run_MVN__c();
        eligibilityEngineRun.Application_MVN__c = app.Id;
        insert eligibilityEngineRun;
        return eligibilityEngineRun;
    }
    
    static Eligibility_Engine_Result_MVN__c createEligibilityEngineResult(Eligibility_Engine_Run_MVN__c engineRun) {
        Eligibility_Engine_Result_MVN__c eligibilityEngineRun = new Eligibility_Engine_Result_MVN__c();
        eligibilityEngineRun.Result_Message_MVN__c = 'Denied Reason1';
        eligibilityEngineRun.Result_MVN__c = 'Denied';
        eligibilityEngineRun.Eligibility_Engine_Run_MVN__c = engineRun.Id;
        insert eligibilityEngineRun;
        return eligibilityEngineRun;
    }
    
   
    static void createNintexPackages() {
        Loop__DDP__c nintexPackage = new Loop__DDP__c(
            Name = 'test package',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'Test-PM-Approval-Patient'
        );
        Insert nintexPackage;
        
        Loop__DDP__c nintexPackageSponsor = new Loop__DDP__c(
            Name = 'test package',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'Test-PM-MedicareMedD'
        );
        insert nintexPackageSponsor;
        
         Loop__DDP__c nintexPackageSponsor1 = new Loop__DDP__c(
            Name = 'test package',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'Test-PM-MedicareMedB'
        );
        insert nintexPackageSponsor1;
        
        Loop__DDP__c nintexPackage2 = new Loop__DDP__c(
            Name = 'test package',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'Test-PM-Approval-HCP'
        );
        insert nintexPackage2;
        
        Loop__DDP_Integration_Option__c ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Email').getRecordTypeId();
        ddpOption.Loop__Attach_As__c = 'Attachment';
        ddpOption.Name = 'Email';
        insert ddpOption;
        
        Loop__DDP_Integration_Option__c ddpOption1 = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage2.Id);
        ddpOption1.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Email').getRecordTypeId();
        ddpOption1.Loop__Attach_As__c = 'Attachment';
        ddpOption1.Name = 'Fax';
        insert ddpOption1;
        
        Loop__DDP_Integration_Option__c ddpOption2 = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackageSponsor.Id);
        ddpOption2.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Email').getRecordTypeId();
        ddpOption2.Loop__Attach_As__c = 'Attachment';
        ddpOption2.Name = 'Mail';
        insert ddpOption2;
        Loop__DDP_Integration_Option__c ddpOption3 = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackageSponsor1.Id);
        ddpOption3.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Email').getRecordTypeId();
        ddpOption3.Loop__Attach_As__c = 'Attachment';
        ddpOption3.Name = 'Mail';
        insert ddpOption3;
    }
    
    @isTest static void getPatientTestSMS() {
        Application_MVN__c app = [select id,Name,Application_Status_Date_JKC__c,Program_Member_MVN__r.Patient_Status_Date_JKC__c,Is_Application_Active_MVN__c from Application_MVN__c];
        
        Program_Member_MVN__c programMembers = [select id from Program_Member_MVN__c limit 1];
        programMembers.Active_Application_MVN__c = app.Id;
        update programMembers;
        
        List<Application_MVN__c> appList = new List<Application_MVN__c>([Select Id,Name,
                Program_Member_MVN__c,
                Program_Member_MVN__r.Program_MVN__r.Program_ID_MVN__c, 
				Status_MVN__c,              
                Application_Status_Date_JKC__c,
                Program_Member_MVN__r.Name,
                Program_Member_MVN__r.Patient_Status_MVN__c,
				Program_Member_MVN__r.Patient_Status_Date_JKC__c,
				Program_Member_MVN__r.Notified_HCP_JKC__c,
				Program_Member_MVN__r.Notified_Patient_JKC__c,
				Program_Member_MVN__r.Notified_Sponsor_JKC__c from Application_MVN__c] );
         Account accountPayer = [Select id from Account where BillingCity = 'TestPayer' ];                 
       
         Benefits_Coverage_MVN__c b2=   new Benefits_Coverage_MVN__c( Program_Member_MVN__c = programMembers.Id,
                                                                    Application_MVN__c = app.Id,Medicare_Part_D_MVN__c ='No',
                                                                    Medicare_Part_B_MVN__c = 'Yes', // one true
                                                                    Payer_Med_B_Plan_Sponsor_JKC__c = accountPayer.Id,
                                                                  Payer_Med_D_Plan_Sponsor_JKC__c = accountPayer.Id
                                                                   );
        insert b2;
         
      system.Test.startTest();
    
        PAP_SendNotification_Batch bc = new PAP_SendNotification_Batch();
        Test.setMock(HttpCalloutMock.class, new PAP_SendNotificationBatchMock('Test'));
      	database.executeBatch(bc,10);
        system.Test.stopTest();
        system.assertEquals(appList.size(), 1);
    }
    
}