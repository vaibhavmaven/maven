/*
 * SldsInputFieldControllerMVN
 * Created by: Kai Amundsen
 * Created Date: July 18, 2016
 * LastModified: July 27, 2016, Kyle Thornton
                 Updated to meet mavens standards and remove repetative code
 * Description: Controller for the SldsInputField component.  Allows replicating apex:inputField in Visualforce using
                the Lightning Design System.
    https://justinyue.wordpress.com/2016/01/23/migrating-visualforce-component-into-lightning-experience-world/
    https://github.com/Justin1108/sfdc-lexforce
 */
public with sharing class SldsInputFieldControllerMVN {
    public SObject inputObject { get; set; }
    public String fieldName { get; set; }
    public String defaultFieldValue { get; set; }
    public Boolean readOnlyOverride { get;set;}

    public void setDefaultValue() {
        if(defaultFieldValue != null && inputObject.get(fieldName) == null){
            if(fieldDisplayType == Schema.DisplayType.String ||
               fieldDisplayType == Schema.DisplayType.Email ||
               fieldDisplayType == Schema.DisplayType.TextArea ||
               fieldDisplayType == Schema.DisplayType.Picklist ||
               fieldDisplayType == Schema.DisplayType.reference && fieldName != 'OwnerId')
            {
                inputObject.put(fieldName, defaultFieldValue);
            } else if(fieldDisplayType == Schema.DisplayType.Double || fieldDisplayType == Schema.DisplayType.Percent){
                inputObject.put(fieldName, Double.valueOf(defaultFieldValue));
            } else if(fieldDisplayType == Schema.DisplayType.Integer){
                inputObject.put(fieldName, Integer.valueOf(defaultFieldValue));
            } else if(fieldDisplayType == Schema.DisplayType.Currency){
                inputObject.put(fieldName, Decimal.valueOf(defaultFieldValue));
            } else if(fieldDisplayType == Schema.DisplayType.Boolean){
                inputObject.put(fieldName, Boolean.valueOf(defaultFieldValue));
            } else if(fieldDisplayType == Schema.DisplayType.Date){
                inputObject.put(fieldName, Date.valueOf(defaultFieldValue));
            } else if(fieldDisplayType == Schema.DisplayType.DateTime){
                inputObject.put(fieldName, DateTime.valueOf(defaultFieldValue));
            }
        }
    }

    public Boolean isReadOnly {
        get {
            if (readOnlyOverride != null && readOnlyOverride) {
                return true;
            }

            return !fieldDescribe.isUpdateable();
        }
    }

    public Boolean isInput {
        get {
            if (fieldDisplayType == Schema.DisplayType.String ||
                fieldDisplayType == Schema.DisplayType.Currency ||
                fieldDisplayType == Schema.DisplayType.Email ||
                fieldDisplayType == Schema.DisplayType.Double ||
                fieldDisplayType == Schema.DisplayType.Integer ||
                fieldDisplayType == Schema.DisplayType.Percent ||
                fieldDisplayType == Schema.DisplayType.URL ||
                fieldDisplayType == Schema.DisplayType.Phone){
                setDefaultValue();
                return true;
            }
            else
                return false;
        }
    }

    public Boolean isTextarea {
        get {
            if (fieldDisplayType == Schema.DisplayType.TextArea) {
                setDefaultValue();
                return true;
            } else {
                return false;
            }
        }
    }

    public Boolean isPicklist {
        get {
            if (fieldDisplayType == Schema.DisplayType.Picklist) {
                setDefaultValue();
                return true;
            } else {
                return false;
            }
        }
    }


    public Boolean isMultiPicklist {
        get {
            if (fieldDisplayType == Schema.DisplayType.MultiPicklist) {
                setDefaultValue();
                return true;
            } else {
                return false;
            }
        }
    }

    public Boolean isCheckbox {
        get {
            if (fieldDisplayType == Schema.DisplayType.Boolean) {
                setDefaultValue();
                return true;
            } else {
                return false;
            }
        }
    }

    public Boolean isDatetime {
        get {
            if (fieldDisplayType == Schema.DisplayType.Date || fieldDisplayType == Schema.DisplayType.Datetime) {
                setDefaultValue();
                return true;
            } else {
                return false;
            }
        }
    }

    public Boolean isLookup {
        get {
            if (fieldDisplayType == Schema.DisplayType.reference && fieldName != 'OwnerId') {
                setDefaultValue();
                return true;
            } else {
                return false;
            }
        }
    }

    public Boolean isOwner {
        get {
            return fieldName == 'OwnerId';
        }
    }


    public String objectName {
        get {
            if (inputObject != null) {
                return inputObject.getSObjectType().getDescribe().getName();
            } else {
                return '';
            }
        }
    }

    @TestVisible private SObjectField sField {
        get {
            if (inputObject != null) {
                return inputObject.getSObjectType().getDescribe().fields.getMap().get(fieldName);
            } else {
                return null;
            }
        }
    }

    @TestVisible private DescribeFieldResult fieldDescribe {
        get {
            SObjectField sf = sField;
            if (sf != null) {
                return sf.getDescribe();
            } else {
                return null;
            }
        }
    }

    private DisplayType fieldDisplayType {
        get {
            return fieldDescribe.getType();
        }
    }
}