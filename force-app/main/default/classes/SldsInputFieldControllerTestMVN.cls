/*
 * SldsInputFieldControllerMVN
 * Created by: Kai Amundsen
 * Created Date: July 18, 2016
 * LastModified
 * Description: Test class for SldsInputFieldCtrl component controller
 */
@isTest
private class SldsInputFieldControllerTestMVN {

    static SldsInputFieldControllerMVN controller = new SldsInputFieldControllerMVN();

    @isTest static void test_field_default_value() {
        controller.inputObject = new Contact();

        controller.fieldName = 'LeadSource';
        controller.defaultFieldValue = 'Web';
        controller.setDefaultValue();
        System.assertEquals('Web', controller.inputObject.get('LeadSource'));

        controller.fieldName = 'DoNotCall';
        controller.defaultFieldValue = 'true';
        controller.setDefaultValue();
        System.assertEquals(true, controller.inputObject.get('DoNotCall'));

        controller.fieldName = 'Birthdate';
        controller.defaultFieldValue = '1900-01-01';
        controller.setDefaultValue();
        System.assertEquals(Date.newInstance(1900, 1, 1), controller.inputObject.get('Birthdate'));

        controller.inputObject = new Account();

        controller.fieldName = 'AnnualRevenue';
        controller.defaultFieldValue = '100';
        controller.setDefaultValue();
        System.assertEquals(100, controller.inputObject.get('AnnualRevenue'));

        controller.fieldName = 'NumberOfEmployees';
        controller.defaultFieldValue = '100';
        controller.setDefaultValue();
        System.assertEquals(100, controller.inputObject.get('NumberOfEmployees'));
    }

    @isTest static void test_is_read_only_override() {
        controller.readOnlyOverride = true;
        System.assertEquals(true, controller.isReadOnly);
    }

    @isTest static void test_object_name_is_returned() {
        controller.inputObject = new Account();
        System.assertEquals(Account.getSObjectType().getDescribe().getName(), controller.objectName);
    }

    @isTest static void test_null_object_name_is_empty_string() {
        controller.inputObject = null;
        System.assert(String.isBlank(controller.objectName));
    }

    @isTest static void test_read_only_fields_are_rendered_as_read_only() {
        controller.inputObject = new Account();
        controller.fieldName = 'LastModifiedDate';

        System.assert(controller.isReadOnly, 'Case.LastModifiedDate will not be rendered as read only.');
    }

    @isTest static void test_input_fields_are_rendered_as_input() {
        controller.inputObject = new Account();
        testFieldIsInput('AccountNumber', 'String');
        testFieldIsInput('AnnualRevenue', 'Currency');
        testFieldIsInput('NumberOfEmployees', 'Integer');
        testFieldIsInput('Website', 'URL');
        testFieldIsInput('Phone', 'Phone');

        controller.inputObject = new Campaign();
        testFieldIsInput('ExpectedResponse', 'Percent');

        controller.inputObject = new Contact();
        testFieldIsInput('Email', 'Email');

        controller.fieldName = 'DoNotCall';
        System.assert(!controller.isInput, 'DoNotCall would be rendered as inputField');

        controller.inputObject = new Opportunity();
        testFieldIsInput('TotalOpportunityQuantity', 'Decimal');
    }

    private static void testFieldIsInput(String fieldName, String type) {
        controller.fieldName = fieldName;
        List<String> formatFields = new List<String>{fieldName, type};
        System.assert(controller.isInput, String.format('{0} ({1}) will not be rendered as an inputField', formatFields));
    }

    @isTest static void test_textArea_rendered_as_textArea() {
        controller.inputObject = new Account();
        controller.fieldName = 'Description';
        System.assert(controller.isTextarea, 'Description would not be rendered as TextArea');

        controller.fieldName = 'Name';
        System.assert(!controller.isTextarea, 'Account Name would be rendered as TextArea');
    }

    @isTest static void test_picklist_rendered_as_picklist() {
        controller.inputObject = new Account();
        controller.fieldName = 'Salutation';
        System.assert(controller.isPicklist, 'Salutation would not be rendered as a Picklist');

        controller.fieldName = 'Name';
        System.assert(!controller.isPicklist, 'Account Name would be rendered as a Picklist');
    }

    @isTest static void test_MultiPicklist_rendered_as_MultiPicklist() {
        controller.inputObject = new Account();
        //There are no standard multiSelect pick lists to use so we can't test that

        controller.fieldName = 'Name';
        System.assert(!controller.isMultiPicklist, 'Account Name would be rendered as a Picklist');
    }

    @isTest static void test_boolean_rendered_as_boolean() {
        controller.inputObject = new Contact();
        controller.fieldName = 'DoNotCall';
        System.assert(controller.isCheckbox, 'DoNotCall would not be rendered as a checkbox');

        controller.fieldName = 'FirstName';
        System.assert(!controller.isCheckbox, 'FirstName would be rednered as a checkbox');
    }

    @isTest static void test_dateTime_rendered_as_dateTime() {
        controller.inputObject = new Contact();
        controller.fieldName = 'Birthdate';
        System.assert(controller.isDateTime, 'Birthdate would not be rendered as a DateTime');
        controller.fieldName = 'LastModifiedDate';
        System.assert(controller.isDateTime, 'LastModifiedDate would not be rendered as a DateTime');

        controller.fieldName = 'FirstName';
        System.assert(!controller.isDateTime, 'FirstName would be rednered as a DateTime');
    }

    @isTest static void test_lookup_rendered_as_lookup() {
        controller.inputObject = new Contact();
        controller.fieldName = 'AccountId';
        System.assert(controller.isLookup, 'AccountId would not be rendered as a Lookup');

        controller.fieldName = 'FirstName';
        System.assert(!controller.isLookup, 'FirstName would be rednered as a Lookup');
    }

    @isTest static void test_owner_rendered_as_owner() {
        controller.inputObject = new Contact();
        controller.fieldName = 'OwnerId';
        System.assert(controller.isOwner, 'OwnerId would not be rendered as a Owner');

        controller.fieldName = 'FirstName';
        System.assert(!controller.isOwner, 'FirstName would be rednered as a Owner');
    }

    @isTest static void test_sField_returns_null_with_no_object() {
        controller.inputObject = null;
        System.assertEquals(null, controller.sField);
    }

    @isTest static void test_fieldDescribe_returns_null_with_no_object() {
        controller.inputObject = null;
        System.assertEquals(null, controller.fieldDescribe);
    }
}