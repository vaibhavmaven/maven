global class PAP_ReprocessNotificationBatch implements Database.Batchable<sObject> , Database.allowsCallouts {

    global  List<Program_Member_MVN__c> start(Database.BatchableContext bc){
        
        List<Program_Member_MVN__c> pgmFaxFailedLists =  [SELECT Id,
                                                          Fax_Delivery_JKC__c,
                                                          Fax_Retry_Identifier_JKC__c,
                                                          Reprocessed_HCP_Type_JKC__c,
                                                          Fax_Sent_Time_JKC__c 
                                                          FROM Program_Member_MVN__c
                                                          where Fax_Delivery_JKC__c = 'Failed'];
        
        List<Program_Member_MVN__c> pgmPhysicianContactList =  [SELECT Id,Name,
                                                                Reprocessed_HCP_Type_JKC__c
                                                                FROM Program_Member_MVN__c
                                                                where  Program_Member_MVN__c.Physician_MVN__c
                                                                In (SELECT AccountId FROM Contact  
                                                                    where IsEmailBounced  = true )
                                                                and 
                                                                Active_Application_MVN__c
                                                                In (SELECT Id FROM Application_MVN__c
                                                                    WHERE
                                                                    (Application_Status_Date_JKC__c = TODAY) AND
                                                                    Is_Application_Active_MVN__c = true)
                                                                AND
                                                                Notified_HCP_JKC__c = True ];
        
        
        List<Program_Member_MVN__c> pgmOmniChannelList =  [SELECT Id,Name,
                                                           Reprocessed_Patient_Type_JKC__c 
                                                           FROM Program_Member_MVN__c
                                                           where  Id In (Select Program_Member_JKC__c from Omnichannel_JKC__c
                                                                         where (Status_JKC__c Like '%Error%' or
                                                                                SMS_Status_JKC__c  Like '%Error') AND SMS_JKC__c = True )  ];
        
        List<Program_Member_MVN__c> pgmMemberContactList =  [SELECT Id,Name,
                                                             Reprocessed_Patient_Type_JKC__c 
                                                             FROM Program_Member_MVN__c
                                                             where  Program_Member_MVN__c.Member_MVN__c
                                                             In (SELECT AccountId FROM Contact  
                                                                 where IsEmailBounced  = true ) 
                                                             and 
                                                             Active_Application_MVN__c
                                                             In (SELECT Id FROM Application_MVN__c
                                                                 WHERE
                                                                 (Application_Status_Date_JKC__c = TODAY) AND
                                                                 Is_Application_Active_MVN__c = true)
                                                             AND Notified_Patient_JKC__c  = True ];
        
        
        List<Program_Member_MVN__c> pgmListFinal = new List<Program_Member_MVN__c>(); 
        pgmListFinal.addAll(pgmFaxFailedLists);
        pgmListFinal.addAll(pgmPhysicianContactList);
        pgmListFinal.addAll(pgmOmniChannelList);
        pgmListFinal.addAll(pgmMemberContactList);
        return pgmListFinal;
        
    }
    global void execute(Database.BatchableContext BC, List<Program_Member_MVN__c> pgmMember) {
        
        Map<String, List<Id>> sendNotificationResult = new Map<String, List<Id>>();
        List<Id> pgmUpdateId = new List<Id>();
        List<Id> pgmPatientUpdateId = new List<Id>();
        List<Id> pgmCounterCheckId  = new List<Id>();
        List<Id> pgmListsId = new List<Id>();
       
        for(Program_Member_MVN__c pgm: [Select id,Fax_Attempt_Counter_JKC__c,Physician_MVN__r.BillingStreet,Is_Prescription_Generated_JKC__c,Document_Generated_Date_JKC__c,
                                        Physician_MVN__r.BillingCity,Physician_MVN__r.BillingCountryCode,Physician_MVN__r.BillingStateCode,
                                        Physician_MVN__r.BillingPostalCode,Physician_MVN__r.Primary_Email_MVN__c,Reprocessed_HCP_Type_JKC__c
                                        from Program_Member_MVN__c where Fax_Delivery_JKC__c = 'Failed' AND Id IN: pgmMember]){
                                            pgmListsId.add(pgm.Id);
                                            if(pgm.Fax_Attempt_Counter_JKC__c <3){
                                                system.debug('indise if of counter  ');
                                                //check flag condtionss 
                                                pgmCounterCheckId.add(pgm.Id);
                                            } 
                                            else{
                                                if (String.isNotEmpty(pgm.Physician_MVN__r.Primary_Email_MVN__c)){
                                                    String HCPNotification = sendNotification_HCP(pgm.Id,'Email');
                                                    system.debug('********* inside else of counter check '+HCPNotification);
                                                    if(HCPNotification == 'Success'){
                                                        pgmUpdateId.add(pgm.Id);
                                                        sendNotificationResult.put('HCP-Email',pgmUpdateId); 
                                                    }
                                                }
                                                else{
                                                    if(String.isNotEmpty(pgm.Physician_MVN__r.BillingStreet)
                                                       && String.isNotEmpty(pgm.Physician_MVN__r.BillingCity) 
                                                       && String.isNotEmpty(pgm.Physician_MVN__r.BillingPostalCode) 
                                                       && String.isNotEmpty(pgm.Physician_MVN__r.BillingStateCode)
                                                       && String.isNotEmpty(pgm.Physician_MVN__r.BillingCountryCode)){
                                                           String HCPNotification = sendNotification_HCP(pgm.Id,'Mail');
                                                           system.debug('********* inside else of counter check  Mail'+HCPNotification);
                                                           if(HCPNotification == 'Success'){
                                                               pgmUpdateId.add(pgm.Id);
                                                               sendNotificationResult.put('HCP-Mail',pgmUpdateId); 
                                                           }
                                                       }
                                                }
                                                
                                            }
                                        }
        //    sendNotificationResult.put('HCP',pgmUpdateId);
    for(Program_Member_MVN__c pgm: [SELECT Id,Name, Reprocessed_HCP_Type_JKC__c FROM Program_Member_MVN__c where  Program_Member_MVN__c.Physician_MVN__c
                                        In (SELECT AccountId FROM Contact  
                                            where IsEmailBounced  = true ) and 
                                        Active_Application_MVN__c
                                        In (SELECT Id FROM Application_MVN__c
                                            WHERE
                                            (Application_Status_Date_JKC__c = TODAY) AND
                                            Is_Application_Active_MVN__c = true)
                                        AND
                                        Notified_HCP_JKC__c = True  and ID in: pgmMember]){
                                            String HCPNotification =  sendNotification_HCP(pgm.Id,'Mail');
                                            system.debug('******************** HCPNotification '+HCPNotification);
                                            
                                            if(HCPNotification == 'Success'){
                                                
                                                pgmUpdateId.add(pgm.Id);
                                                sendNotificationResult.put('HCP-Mail',pgmUpdateId); 
                                            }
                                        }
       
        for(Program_Member_MVN__c pgm: [SELECT Id,Name,Member_MVN__r.Primary_Email_MVN__c,Member_MVN__r.BillingStreet,Member_MVN__r.BillingCity,
                                        Reprocessed_Patient_Type_JKC__c,Member_MVN__r.BillingPostalCode ,Member_MVN__r.BillingStateCode,Member_MVN__r.BillingCountryCode
                                        FROM Program_Member_MVN__c
                                        where  Id In (Select Program_Member_JKC__c from Omnichannel_JKC__c
                                                      where (Status_JKC__c Like '%Error%' or
                                                             SMS_Status_JKC__c  Like '%Error%') AND SMS_JKC__c = True ) ]){
                                                                 
                                                                
                                                                 if (String.isNotEmpty(pgm.Member_MVN__r.Primary_Email_MVN__c)){
                                                                     system.debug('******** pgm.Member_MVN__r.Primary_Email_MVN__c '+pgm.Member_MVN__r.Primary_Email_MVN__c);
                                                                     system.debug('****** inside Email for patient');
                                                                     String SendNotificationPatient=	sendNotification_Patient(pgm.Id,'Email');
                                                                     // set flag true for re-renrollment if mwssage equals  re-enrollment  and pass true to send notification method and send  nintex update 
                                                                     if(SendNotificationPatient == 'Success'){
                                                                         pgmPatientUpdateId.add(pgm.Id);
                                                                         sendNotificationResult.put('Patient-Email',pgmPatientUpdateId);
                                                                     }
                                                                 }
                                                                 else{
                                                                     
                                                                     if(String.isNotEmpty(pgm.Member_MVN__r.BillingStreet) 
                                                                        && String.isNotEmpty(pgm.Member_MVN__r.BillingCity) 
                                                                        && String.isNotEmpty(pgm.Member_MVN__r.BillingPostalCode) 
                                                                        && String.isNotEmpty(pgm.Member_MVN__r.BillingStateCode) 
                                                                        && String.isNotEmpty(pgm.Member_MVN__r.BillingCountryCode)
                                                                       ){
                                                                           system.debug('****** inside mail for patient');
                                                                           
                                                                           String SendNotificationPatient=	sendNotification_Patient(pgm.Id,'Mail');
                                                                           // set flag true for re-renrollment if mwssage equals  re-enrollment  and pass true to send notification method and send  nintex update 
                                                                           if(SendNotificationPatient == 'Success'){
                                                                               pgmPatientUpdateId.add(pgm.Id);
                                                                               sendNotificationResult.put('Patient-Mail',pgmPatientUpdateId);
                                                                               
                                                                           }
                                                                       }
                                                                 }
                                                             }
        
        // sendNotificationResult.put('Patient',pgmPatientUpdateId);
       
        
        for(Program_Member_MVN__c pgm:  [SELECT Id,Name
                                         FROM Program_Member_MVN__c
                                         where  Program_Member_MVN__c.Member_MVN__c
                                         In (SELECT AccountId FROM Contact  
                                             where IsEmailBounced  = true )
                                         and 
                                         Active_Application_MVN__c
                                         In (SELECT Id FROM Application_MVN__c
                                             WHERE
                                             (Application_Status_Date_JKC__c = TODAY) AND
                                             Is_Application_Active_MVN__c = true)
                                         AND Notified_Patient_JKC__c  = True ]){
                                             String SendNotificationPatient=	sendNotification_Patient(pgm.Id,'Mail');
                                             system.debug('******************** SendNotificationPatient '+SendNotificationPatient);
                                             
                                             if(SendNotificationPatient == 'Success'){
                                                 pgmPatientUpdateId.add(pgm.Id);
                                                 sendNotificationResult.put('Patient-Mail',pgmPatientUpdateId);
                                             }
                                             
                                         }
       
        FOR(String st : sendNotificationResult.keySet() ){
            for(Id pgmid : sendNotificationResult.get(st)){
                system.debug('****** Map key values '+st);
                if( st.contains('HCP')){
                    updateProgramMember(pgmid,'Reprocess',st);
                }
                if(st.contains('Patient')){
                    updateProgramMember(pgmid,'Reprocess',st);
                }
            }
        }
        system.debug('********* pgmCounterCheck size()'+pgmCounterCheckId.size());
    
        for(Id pgm : pgmCounterCheckId){
            SYSTEM.debug('************* check '+pgm);
            updateProgramMember(pgm,'FaxRetryCounter','Pgm');
        }
    }    
    global void finish(Database.BatchableContext bc) {
        
    }  
    
    webservice static String sendNotification_Patient(String pgmMemberID, String Delivery){   
        
        Program_Member_MVN__c pgmMemberObj = getPgmMemberDetails(pgmMemberID);
        Boolean IsSMS ;
        Boolean IsAutoDial ;  
        String deliveryType ='';
        Boolean terminationFlag = false;
        Contact_Information_MVN__c conInfo;
        String sendPatientResult ='';
        String invokeMethod ='';
        system.debug('****** Reprocessed_Patient_JKC__c '+pgmMemberObj.Reprocessed_Patient_JKC__c);
        system.debug('****** Reprocessed_Patient_Type_JKC__c '+pgmMemberObj.Reprocessed_Patient_Type_JKC__c);
          system.debug('****** pgmMemberObj '+pgmMemberObj.Id);
        system.debug('*********** delivery '+Delivery);
        
            
      
        try{ 
            if ((String.isNotEmpty(pgmMemberObj.Member_MVN__r.Primary_Email_MVN__c)) 
                && (pgmMemberObj.Program_MVN__r.Email_JKC__c == true)
                && Delivery == 'Email'
                && pgmMemberObj.Reprocessed_Patient_JKC__c == false 
                &&  pgmMemberObj.Reprocessed_Patient_Type_JKC__c == null)
            {
                
                SYSTEM.debug('**********************Email: '); 
                deliveryType ='Email';
                PAP_SendNotification.sendToNintex_Patient(pgmMemberObj,deliveryType,false);                
                sendPatientResult = 'Success';
                
            }
            SYSTEM.debug('********** pgmMemberObj.Member_MVN__r.BillingStreet '+pgmMemberObj.Member_MVN__r.BillingStreet);
           
            if (String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingStreet) 
                && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingCity) 
                && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingPostalCode) 
                && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingStateCode) 
                && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingCountryCode)
                && (pgmMemberObj.Program_MVN__r.Mail_JKC__c == true )
                &&  Delivery == 'Mail'
                && (pgmMemberObj.Reprocessed_Patient_JKC__c == false
                || (pgmMemberObj.Reprocessed_Patient_Type_JKC__c =='Email' && pgmMemberObj.Reprocessed_Patient_JKC__c == true)))
                
            {
                system.debug('****************** inside mail   patient');
                
                deliveryType ='Mail';
                PAP_SendNotification.sendToNintex_Patient(pgmMemberObj,deliveryType,false);                        
                sendPatientResult = 'Success';
            } 
            
            system.debug('*********** sendPatientResult '+sendPatientResult);
            Return sendPatientResult;  
        }
        catch(Exception ex){ 
            
            System.debug('ErrorMessage' +ex.getMessage());
            
        }
        Return sendPatientResult; 
    }
    
    
    webservice static String sendNotification_HCP(String pgmMemberID,String DeliveryTypes){       
        Program_Member_MVN__c pgmMemberObj = getPgmMemberDetails(pgmMemberID);
        Boolean IsFax ;        
        String deliveryType ='';
        String sendHCPResult ='';
        String invokeMethod ='';
        String responseString;
        IsFax = pgmMemberObj.Program_MVN__r.Fax_JKC__c;
        List<Id> programMemberId = new List<Id>();
        programMemberId.add(PgmMemberObj.Id);
        system.debug('#########Fax '+pgmMemberObj.Physician_MVN__r.Fax); 
        system.debug('###### IsFax '+IsFax);
        system.debug('**** batch class DeliveryTypes '+DeliveryTypes);
        try{
            SYSTEM.debug('**** pgmMemberObj.Reprocessed_HCP_JKC__c '+pgmMemberObj.Reprocessed_HCP_JKC__c);
            SYSTEM.debug('**** pgmMemberObj.Reprocessed_HCP_Type_JKC__c '+pgmMemberObj.Reprocessed_HCP_Type_JKC__c);
            SYSTEM.debug('**** pgmMemberObj.pgmMemberObj.Fax_Attempt_Counter_JKC__c '+pgmMemberObj.Fax_Attempt_Counter_JKC__c);
            
            if ((String.isNotEmpty(pgmMemberObj.Physician_MVN__r.Primary_Email_MVN__c)) 
                && DeliveryTypes == 'Email'
                && pgmMemberObj.Fax_Attempt_Counter_JKC__c == 3
                && pgmMemberObj.Reprocessed_HCP_JKC__c == false
                && pgmMemberObj.Reprocessed_HCP_Type_JKC__c == null
                && (pgmMemberObj.Program_MVN__r.Email_JKC__c == true )){
                    
                    system.debug('**** inside if of SEND NOTOGIFCATION HCP  '+ pgmMemberObj.Id);
                    deliveryType ='Email';
                    PAP_SendNotification.sendToNintex_HCP(pgmMemberObj,deliveryType,false);
                    sendHCPResult ='Success';
                    
                }
            
            if (String.isNotEmpty(pgmMemberObj.Physician_MVN__r.BillingStreet) 
                && String.isNotEmpty(pgmMemberObj.Physician_MVN__r.BillingCity) 
                && String.isNotEmpty(pgmMemberObj.Physician_MVN__r.BillingPostalCode) 
                && String.isNotEmpty(pgmMemberObj.Physician_MVN__r.BillingStateCode)
                && String.isNotEmpty(pgmMemberObj.Physician_MVN__r.BillingCountryCode)
                && DeliveryTypes == 'Mail'
                && pgmMemberObj.Program_MVN__r.Mail_JKC__c == true 
                && (pgmMemberObj.Reprocessed_HCP_JKC__c == false
                    || ((pgmMemberObj.Reprocessed_HCP_Type_JKC__c =='Email') && pgmMemberObj.Reprocessed_HCP_JKC__c == true))
               ){
                   system.debug('************* inside mail   hcp');
                   deliveryType ='Mail';
                   PAP_SendNotification.sendToNintex_HCP(pgmMemberObj,deliveryType,false);
                   sendHCPResult ='Success';
               } 
            
            
            system.debug('****** hcp result '+sendHCPResult);
            return (sendHCPResult);
        }
        catch(Exception ex){ 
            
            System.debug('*** message '+ex.getMessage());
        }
        return (sendHCPResult);
    } 
    
    public static Program_Member_MVN__c getPgmMemberDetails(String pgmMemberID){
        return [SELECT Id,
                Name,
                Program_Name_MVN__c,
                Program_Id_MVN__c,
                Fax_Attempt_Counter_JKC__c,
                Program_MVN__r.SMS_JKC__c,
                Program_MVN__r.AutoDialer_JKC__c,
                Program_MVN__r.Email_JKC__c,
                Program_MVN__r.Mail_JKC__c,
                Program_MVN__r.Fax_JKC__c,
                Patient_Status_MVN__c,                
                Patient_Status_Date_JKC__c,
                Reprocessed_HCP_JKC__c,
                Reprocessed_Patient_JKC__c,
                Reprocessed_HCP_Type_JKC__c,
                Reprocessed_Patient_Type_JKC__c,
                Active_Application_MVN__c,
                Active_Application_Status_MVN__c,
                Active_Application_Expiry_Date_JKC__c,
                Member_MVN__r.FirstName,
                Member_MVN__r.LastName,
                Is_Prescription_Generated_JKC__c,Document_Generated_Date_JKC__c,
                Member_MVN__r.phone,
                Member_MVN__r.PersonMobilePhone,
                Member_MVN__r.Primary_Email_MVN__c,
                Member_MVN__r.Fax,
                Member_MVN__r.BillingStreet,
                Member_MVN__r.BillingCity,
                Member_MVN__r.BillingStateCode,
                Member_MVN__r.BillingPostalCode,                
                Member_MVN__r.BillingCountry,
                Member_MVN__r.BillingState,
                Member_MVN__r.BillingCountryCode,
                Physician_MVN__r.Id,
                Physician_MVN__r.FirstName,
                Physician_MVN__r.LastName,
                Physician_MVN__r.Fax,
                Physician_MVN__r.Primary_Email_MVN__c,
                Physician_MVN__r.BillingStreet,
                Physician_MVN__r.BillingCity,
                Physician_MVN__r.BillingStateCode,
                Physician_MVN__r.BillingPostalCode,
                Physician_MVN__r.BillingCountryCode
                from Program_Member_MVN__c where Id=:pgmMemberID] ;      
    }
    
    public static void updateProgramMember(String PgmMemberId, String updateCase,String Type){
        
        system.debug('***** inside update program '+PgmMemberId);
        List<Program_Member_MVN__c> PgmMemberList = [select id,Reprocessed_HCP_JKC__c,
                                                     Queued_Fax_Status_JKC__c, 
                                                     Queued_Fax_Result_JKC__c, 
                                                     Fax_Delivery_JKC__c,
                                                     Fax_Attempt_Counter_JKC__c,
                                                     Is_Prescription_Generated_JKC__c,Document_Generated_Date_JKC__c,
                                                     Sub_Status_JKC__c,Reprocessed_Patient_Type_JKC__c,
                                                     Physician_MVN__r.Primary_Email_MVN__c,Member_MVN__r.Primary_Email_MVN__c,
                                                     Reprocessed_Patient_JKC__c,
                                                     Reprocessed_HCP_Type_JKC__c,
                                                     Fax_Retry_Identifier_JKC__c,Fax_Sent_Time_JKC__c 
                                                     from 
                                                     Program_Member_MVN__c 
                                                     where 
                                                     ID =:PgmMemberId
                                                     LIMIT 1];
        system.debug('******** Type '+Type);
        if(Type.contains('HCP')){
            if(Type.contains('Email')){
                PgmMemberList[0].Reprocessed_HCP_Type_JKC__c = 'Email';
                PgmMemberList[0].Reprocessed_HCP_JKC__c = true;
                update PgmMemberList[0];
            }
            else{
                system.debug('* * * * * * * * * * * * * * * * * * * * * * * * * * * * ** * * * * *');
                PgmMemberList[0].Reprocessed_HCP_Type_JKC__c = 'Mail';
                PgmMemberList[0].Reprocessed_HCP_JKC__c = true;
                
                update PgmMemberList[0];
            }
        }
        
        
        
        if(Type.contains('Patient')){
            if(Type.contains('Email')){
                PgmMemberList[0].Reprocessed_Patient_Type_JKC__c = 'Email';
                PgmMemberList[0].Reprocessed_Patient_JKC__c = true;
                update PgmMemberList[0];
            }
            if(Type.contains('Mail')){
                PgmMemberList[0].Reprocessed_Patient_Type_JKC__c = 'Mail';
                PgmMemberList[0].Reprocessed_Patient_JKC__c = true;
                update PgmMemberList[0];
            }
            
        }
        
        if(updateCase =='FaxRetryCounter'){
            if(  PgmMemberList[0].Is_Prescription_Generated_JKC__c ==true){
                PgmMemberList[0].Is_Prescription_Generated_JKC__c =false;
                update PgmMemberList[0];
               
            }
            PgmMemberList[0].Is_Prescription_Generated_JKC__c =true;
            PgmMemberList[0].Document_Generated_Date_JKC__c =system.now();
            update PgmMemberList[0];
        }
        
    }
    
}