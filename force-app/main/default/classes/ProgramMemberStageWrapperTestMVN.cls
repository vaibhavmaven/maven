/*
 *  ProgramMemberStageWrapperTestMVN
 *  Created By:     Jean-Philippe Monette
 *  Created Date:   September 8, 2016
 *  Description:    Test for ProgramMemberStageWrapperMVN Apex class
 */
@isTest
private class ProgramMemberStageWrapperTestMVN {
	private static User caseManager;
    
    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        TestDataFactoryMVN.createPSSettings();

        System.runAs(caseManager) {
            Program_MVN__c program = TestDataFactoryMVN.createProgram();
            TestDataFactoryMVN.createProgramStage(program);
            Account member = TestDataFactoryMVN.createMember();
            Program_Member_MVN__c programMember = TestDataFactoryMVN.createProgramMember(program, member);
            Case caseRecord = TestDataFactoryMVN.createTestCase();
        }
    }

    @isTest
    static void testCreateWrapperFromAProgramMemberStage() {
        System.runAs(caseManager) {
            List<Program_Member_Stage_MVN__c> programStages = [
                    SELECT Parent_Program_Member_Stage_MVN__c,
                           Status_MVN__c,
                           IsClosed_MVN__c,
                           Program_Stage_Name_MVN__c,
                           Stage_Sequence_Number_MVN__c,
                           Application_MVN__r.Name
                      FROM Program_Member_Stage_MVN__c
                  ORDER BY Stage_Sequence_Number_MVN__c ASC
            ];

            ProgramMemberStageWrapperMVN stage = new ProgramMemberStageWrapperMVN(programStages[0]);
            stage.addChild(new ProgramMemberStageWrapperMVN(programStages[1]));

            System.assertEquals(programStages[0].Id, stage.stageId);
            System.assertEquals(programStages[1].Id, stage.children[0].stageId);

            System.assertEquals(false, stage.isInactive);
            System.assertEquals(false, stage.isStarted);
            System.assertEquals(true, stage.isCompleted);

            System.assertEquals('Initial Contact (null) ', stage.name);
            System.assertEquals('New Stage (null) ', stage.children[0].name);

            System.assertEquals(null, stage.caseId);
        }
    }

    @isTest
    static void testCreateWrapperFromACase() {
        System.runAs(caseManager) {
            Case caseRecord = [
                SELECT IsClosed,
                       Subject
                  FROM Case
                 LIMIT 1
            ];

            ProgramMemberStageWrapperMVN stage = new ProgramMemberStageWrapperMVN(caseRecord);

            System.assertEquals(null, stage.stageId);
            System.assertEquals(caseRecord.Id, stage.caseId);

            System.assertEquals(false, stage.isInactive);
            System.assertEquals(true, stage.isStarted);
            System.assertEquals(false, stage.isCompleted);

            System.assertEquals(Label.Blank, stage.name);
        }
    }

    @isTest
    static void testCreateWrapperFromAdditionalActivities() {
        System.runAs(caseManager) {
            ProgramMemberStageWrapperMVN stage = new ProgramMemberStageWrapperMVN();

            System.assertEquals(null, stage.stageId);
            System.assertEquals(null, stage.caseId);

            System.assertEquals(false, stage.isInactive);
            System.assertEquals(true, stage.isStarted);
            System.assertEquals(false, stage.isCompleted);

            System.assertEquals(Label.Adhoc_Activities, stage.name);
        }
    }

    @isTest
    static void testSortProgramMemberStageWrapper() {
        System.runAs(caseManager) {
            List<Program_Member_Stage_MVN__c> programStages = [
                SELECT Parent_Program_Member_Stage_MVN__c,
                       Status_MVN__c,
                       IsClosed_MVN__c,
                       Program_Stage_Name_MVN__c,
                       Stage_Sequence_Number_MVN__c,
                        Application_MVN__r.Name
                  FROM Program_Member_Stage_MVN__c
              ORDER BY Stage_Sequence_Number_MVN__c ASC
            ];

            Case caseRecord = [SELECT Subject, IsClosed FROM Case LIMIT 1];


            List<ProgramMemberStageWrapperMVN> stages = new List<ProgramMemberStageWrapperMVN>{
                new ProgramMemberStageWrapperMVN(programStages[0]),
                new ProgramMemberStageWrapperMVN(programStages[1]),
                new ProgramMemberStageWrapperMVN(caseRecord),
                new ProgramMemberStageWrapperMVN()
            };

            Test.startTest();
            stages.sort();
            Test.stopTest();

            System.assertEquals(Label.Blank, stages[0].name);
            System.assertEquals(Label.Adhoc_Activities, stages[1].name);
            System.assertEquals('Initial Contact (null) ', stages[2].name);
            System.assertEquals('New Stage (null) ', stages[3].name);
        }
    }
}