/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description Implements the validation of the physician license calling Assure Web Service
 */
public with sharing class EligibilityEngineAssureLicenseHdlrMVN extends EligibilityEngineRuleHdlrMVN {

    private static final Set<String> extraFieldsToQuery = new Set<String> {
        'Program_Member_MVN__r.Physician_MVN__r.FirstName',
        'Program_Member_MVN__r.Physician_MVN__r.LastName',
        'Program_Member_MVN__r.Physician_MVN__r.MiddleName',
        'Program_Member_MVN__r.Physician_MVN__r.Suffix',
        'Program_Member_MVN__r.Program_MVN__r.Job_No_MVN__c',
        'Program_Member_MVN__r.Physician_MVN__c',
        'Program_Member_MVN__r.Physician_MVN__r.Credentials_vod__c',
        'Program_Member_MVN__r.Physician_MVN__r.DEA_MVN__c',
        'Program_Member_MVN__r.Physician_MVN__r.SLN_MVN__c'
    };

    private static final Set<String> prescriptionFieldsToQuery = new Set<String> {
        'Product_MVN__c',
        'Product_MVN__r.Name',
        'Product_MVN__r.DEA_Schedule_MVN__c'
    };

    /**
     * Return a map of object api names to set of fields to be queried
     * @return  Map<String, Set<String>> map of object api names to set of fields needed
     */
    public override Map<String, Set<String>> getExtraFieldsToQuery() {
        return new Map<String, Set<String>>{'Application_MVN__c' => extraFieldsToQuery,
                                            'Prescriptions_MVN__r' => prescriptionFieldsToQuery};
    }

    private Assure_Integration_Settings_MVN__c settings = Assure_Integration_Settings_MVN__c.getInstance();

    private List<Address_vod__c> primaryAddressList;
    
    private final String SLN_VALIDATION = 'SLN Order';
    private final String SLN_DEA_VALIDATION = 'SLN + DEA Order';

    /**
     * Checks license calling out Assure Web Service.
     * This is an exceptional rule, since it will return multiple results, for that reason
     * it will always return false since all the processing happens in this class, and not 
     * in the parent rule as it happens in the rest of rules.
     * @return  Boolean     always returns false
     */
    private Boolean isRuleFired() {
        primaryAddressList = this.getPrimaryAddress(
            super.application.Program_Member_MVN__r.Physician_MVN__c
        );
        super.resultList = new List<Eligibility_Engine_Result_MVN__c>();

        return this.checkLicense();
    }

    /**
     * Go through list of prescriptions associated to the application, check if the product
     * is a controlled substance or not and make the corresponding Assure callout to validate license
     * @return                false always since we don't want the parent class to process the result
     */
    private Boolean checkLicense() {
        Boolean SLNCallMade = false;
        Eligibility_Engine_Result_MVN__c result;
        String validationType;
        String ruleName;
        Set<String> DEAScheduleSet = new Set<String>();

        for (Prescription_MVN__c prescription : super.application.Prescriptions_MVN__r) {
            result = new Eligibility_Engine_Result_MVN__c();

            // Non controlled substance product call.
            // Only 1 SLN call for all non controlled substance product call
            String DEASchedule = prescription.Product_MVN__r.DEA_Schedule_MVN__c;
            if (DEASchedule == null && !SLNCallMade) {
                SLNCallMade = true;
                ruleName = String.format(Label.Assure_Validation_Rule_Name_MVN, new List<String>{'SLN'});
                validationType = this.SLN_VALIDATION;
            } else if (DEASchedule != null && !DEAScheduleSet.contains(DEASchedule)) {
                DEAScheduleSet.add(DEASchedule);
                ruleName = String.format(Label.Assure_Validation_DEA_Schedule_Rule_Name_MVN, new List<String>{DEASchedule});
                validationType = this.SLN_DEA_VALIDATION;
            } else {
                continue;
            }

            result.Name = ruleName;
            if (!isDataValid(validationType, result, prescription.Product_MVN__r.DEA_Schedule_MVN__c)) {
                super.resultList.add(result);
                continue;
            }
            callAssure(validationType, result, DEASchedule);
            super.resultList.add(result);
        }
        return false;
    }

    /**
     * Check if the data is valid before making the Assure call
     * @param  validationType Type of validation to execute
     * @param  result         Eligibility Engine Result to fill with the Assure response
     * @param  DEASchedule    DEA Schedule for product
     * @return                true is the data is valid
     */
    private Boolean isDataValid(String validationType, 
                                Eligibility_Engine_Result_MVN__c result, 
                                String DEASchedule) {

        if (String.isBlank(settings.Assure_Username_MVN__c) || String.isBlank(settings.Assure_Password_MVN__c)) {
            result.Result_MVN__c = EligibilityEngineMVN.RULEDENIED;
            result.Result_Message_MVN__c = Label.Assure_Missing_Credentials_Error_Message_MVN;
            return false;
        }

        AssureIntegrationHdlrMVN assureHdlr = new AssureIntegrationHdlrMVN(
            settings.Assure_Username_MVN__c, settings.Assure_Password_MVN__c, validationType
        );

        List<String> missingFields = assureHdlr.getMissingFields(super.application.Program_Member_MVN__r, 
                                                                    primaryAddressList,
                                                                    DEASchedule);
        if (!missingFields.isEmpty()) {
            result.Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO;
            result.Result_Message_MVN__c = Label.Missing_Assure_Mandatory_Fields_MVN + String.join(missingFields,',');
            return false;
        }
        return true;

    }

    /**
     * Call Assure Web Service to check if the license is valid or not
     * @param  validationType   Type of validation to execute
     * @param  result           Eligibility Engine Result to fill with the Assure response
     * @param  DEASchedule      Product DEA Schedule to be passed to the Assure call
     * @return                  true is the rule is fired (hence the license is not valid)
     */
    private Boolean callAssure(String validationType, Eligibility_Engine_Result_MVN__c result, String DEASchedule) {
        AssureIntegrationHdlrMVN assureHdlr = new AssureIntegrationHdlrMVN(
            settings.Assure_Username_MVN__c, settings.Assure_Password_MVN__c, validationType
        );
        try {
            AssureIntegrationHdlrMVN.AssureResponseWrapper resp =
                assureHdlr.callAssureWebService(super.application.Program_Member_MVN__r, primaryAddressList.get(0), DEASchedule);
            result.Result_MVN__c = resp.result;
            result.Result_Message_MVN__c = resp.resultDescription;
            if (resp.result == EligibilityEngineMVN.RULESUCCESS) {
                return false;
            }
            return true;
        } catch(Exception e) {
            ApexPages.addMessages(e);
            result.Result_MVN__c = e.getMessage();
            return true;
        }

    }

    /**
     * Retrieve primary address for the physician
     * @param accId account Id for the Physician
     * @return   List<Addres_vod__c> address record list
     */
    private List<Address_vod__c> getPrimaryAddress(Id accId) {
        return [
            SELECT Name, Address_line_2_vod__c, City_vod__c, State_vod__c, Zip_vod__c
            FROM Address_vod__c
            WHERE Account_vod__c = :accId
            AND Shipping_vod__c = true
        ];
    }

}