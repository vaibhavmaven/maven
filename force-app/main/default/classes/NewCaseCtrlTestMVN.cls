/**
 * @author Mavens
 * @date 10/2018
 * @description Test for NewCaseCtrlMVN
 * @group Case
 */ 
@isTest
private class NewCaseCtrlTestMVN {

    static User caseManager;
    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        Patient_Service_Settings_MVN__c settings = TestDataFactoryMVN.buildPSSettings();
        insert settings;
    }

    @isTest
    private static void testController() {
        System.runAs(caseManager) {
            Test.startTest();
            NewCaseCtrlMVN controller = new NewCaseCtrlMVN(new ApexPages.StandardController(new Case()));
            Test.stopTest();
            System.assertEquals('Request', controller.recordTypeSelection);
            System.assertEquals(2, controller.recordTypeOptions.size());

            controller.setParam();
        }

    }
}