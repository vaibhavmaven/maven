/**
 *  ContactInfoShippingTriggerMVN
 *  Created By:     Mavens
 *  Created On:     September 2018
 *  Description:    This trigger handler manages contact information records to ensure we always have at maximum 1 Shipping record.
 *                  Possible paths handled are:
 *                  - Insert new CI record marked as Shipping
 *                       -> If there is an existing Shipping remove Shipping designation.
 *                       -> Update Account Shipping information regardless
 *                  - Update CI to be marked as Shipping
 *                       -> If there is an existing Shipping remove Shipping designation
 *                       -> Update Account Shipping information regardless
 *                  - Update CI and remove Shipping designation
 *                       -> Remove details from the Account
 *                  - Delete CI which is marked as Shipping
 *                      -> Remove details from the Account
 *
 **/
public with sharing class ContactInfoShippingTriggerMVN implements TriggerHandlerMVN.HandlerInterface {

    private Map<String, Id> recordTypeMap = new Map<String, Id>();

    public static Boolean hasRun = false;

    private Map<Id, Account> accountsToUpdate = new Map<Id, Account>();

    public void handle() {

        if(hasRun) {
            return;
        }

        hasRun = true;

        Map<Id, Contact_Information_MVN__c> acctIdToNewShippingMap_Address = new Map<Id, Contact_Information_MVN__c>();
        Set<Id> addressIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();

        if(Trigger.isInsert || Trigger.isUpdate) {
            for(Contact_Information_MVN__c newCI : (List<Contact_Information_MVN__c>)Trigger.new) {
                Id rtId = newCI.RecordTypeId;

                if(newCI.Shipping_MVN__c) {
                    accountIds.add(newCI.Account_MVN__c);
                    if(isAddressRecordType(rtId)) {
                        acctIdToNewShippingMap_Address.put(newCI.Account_MVN__c, newCI);
                        addressIds.add(newCI.Address_MVN__c);
                    }
                } else if(Trigger.isUpdate && ((Contact_Information_MVN__c)Trigger.oldMap.get(newCI.Id)).Shipping_MVN__c == true) {
                    Contact_Information_MVN__c dummyCI = new Contact_Information_MVN__c();
                    dummyCI.RecordTypeId = newCI.RecordTypeId;
                    dummyCI.Label_MVN__c = newCI.Label_MVN__c;
                    dummyCI.Shipping_MVN__c = false;

                    accountIds.add(newCI.Account_MVN__c);

                    if(isAddressRecordType(rtId)) {
                        acctIdToNewShippingMap_Address.put(newCI.Account_MVN__c, dummyCI);
                        addressIds.add(newCI.Address_MVN__c);
                    }
                }
            }

            retrieveAccounts(accountIds);
            getUpdatedAccounts(acctIdToNewShippingMap_Address, addressIds);
            update accountsToUpdate.values();

            List<Contact_Information_MVN__c> contactInfoToUpdate = new List<Contact_Information_MVN__c>();
            contactInfoToUpdate.addAll(getUpdatedContactInfoRecords(acctIdToNewShippingMap_Address));
            update contactInfoToUpdate;

            update getUpdatedAddressRecords(acctIdToNewShippingMap_Address, addressIds);

        } else if(Trigger.isDelete) {
            Map<Id, Contact_Information_MVN__c> acctIdToNewShippingMap = new Map<Id, Contact_Information_MVN__c>();
            for(Contact_Information_MVN__c deletedCI : (List<Contact_Information_MVN__c>)Trigger.old) {
                if(deletedCI.Shipping_MVN__c) {
                    Contact_Information_MVN__c dummyCI = new Contact_Information_MVN__c();
                    dummyCI.RecordTypeId = deletedCI.RecordTypeId;
                    dummyCI.Label_MVN__c = deletedCI.Label_MVN__c;
                    acctIdToNewShippingMap.put(deletedCI.Account_MVN__c, dummyCI);
                    accountIds.add(deletedCI.Account_MVN__c);
                }
            }

            retrieveAccounts(accountIds);
            getUpdatedAccounts(acctIdToNewShippingMap, addressIds);
            update accountsToUpdate.values();
        }
    }

    private void retrieveAccounts(Set<Id> accountIds) {
        accountsToUpdate = new Map<Id, Account>([SELECT ShippingStreet, ShippingCity, ShippingState,
                                    ShippingCountry, ShippingPostalCode, Phone, Fax FROM Account WHERE Id in :accountIds]);
    }

    private void getUpdatedAccounts(Map<Id, Contact_Information_MVN__c> acctIdToNewShippingMap, Set<Id> addressIds) {


        Map<Id, Address_vod__c> addresses = new Map<Id,Address_vod__c>([SELECT Id, Name, Address_Line_2_vod__c, City_vod__c, State_vod__c, Country_vod__c, Zip_vod__c
                                                                            FROM Address_vod__c WHERE Id in :addressIds]);

        for(Account acct : accountsToUpdate.values()) {
            if(acctIdToNewShippingMap.containsKey(acct.Id)) {
                Contact_Information_MVN__c newCI = acctIdToNewShippingMap.get(acct.Id);

                if(isAddressRecordType(newCI.RecordTypeId)) {
                    Address_vod__c address = addresses.get(newCI.Address_MVN__c);

                    if(Trigger.isDelete || (Trigger.isUpdate && newCI.Shipping_MVN__c == false)) {
                        address = new Address_vod__c();
                    }

                    if(address != null){
                        acct.ShippingStreet = String.isBlank(address.Address_line_2_vod__c) ? address.Name
                                                                                           : address.Name + ', ' + address.Address_line_2_vod__c;
                        acct.ShippingCity = address.City_vod__c;
                        acct.ShippingStateCode = address.State_vod__c;
                        acct.ShippingCountryCode = address.Country_vod__c;
                        acct.ShippingPostalCode = address.Zip_vod__c;
                    }
                }
            }
        }
    }

    private List<Contact_Information_MVN__c> getUpdatedContactInfoRecords(Map<Id, Contact_Information_MVN__c> acctIdToNewShippingMap) {
        List<Contact_Information_MVN__c> oldPrimaries = [SELECT Id, Shipping_MVN__c, Label_MVN__c, Address_MVN__c, Account_MVN__c, RecordTypeId
                                                            FROM Contact_Information_MVN__c WHERE Account_MVN__c in :acctIdToNewShippingMap.keySet()
                                                                                            AND Shipping_MVN__c = true
                                                                                            AND Id NOT in :Trigger.newMap.keySet()];

        List<Contact_Information_MVN__c> cisForUpdate = new List<Contact_Information_MVN__c>();
        for(Contact_Information_MVN__c oldCI : oldPrimaries) {
            Contact_Information_MVN__c newCI = acctIdToNewShippingMap.get(oldCI.Account_MVN__c);
            Boolean removeShipping = false;
            //We need to remove the Shipping flag if they have the same record type and in the case of phone the same label
            if(newCI.RecordTypeId == oldCI.RecordTypeId) {
                removeShipping = true;
            }

            if(removeShipping) {
                oldCI.Shipping_MVN__c = false;
                cisForUpdate.add(oldCI);
            }
        }

        return cisForUpdate;
    }

    private List<Address_vod__c> getUpdatedAddressRecords(Map<Id, Contact_Information_MVN__c> acctIdToNewShippingMap, Set<Id> addressIds) {

        if(addressIds.size() == 0) {
            return new List<Address_vod__c>();
        }

        List<Address_vod__c> oldAddresses = [SELECT Id, Shipping_vod__c, Account_vod__c FROM Address_vod__c WHERE Shipping_vod__c = true
                                                                                            AND Account_vod__c in :acctIdToNewShippingMap.keySet()
                                                                                            AND Id NOT in :addressIds];

        for(Address_vod__c oldAddress : oldAddresses) {
            if(isAddressRecordType(acctIdToNewShippingMap.get(oldAddress.Account_vod__c).RecordTypeId)) {
                oldAddress.Shipping_vod__c = false;
            }
        }

        return oldAddresses;
    }

    private Boolean isAddressRecordType(Id rtId) {
        return rtId == getRTId('Address_MVN');
    }

    private String getRTId(String rtId) {
        if(recordTypeMap.keySet().size() == 0) {
            populateRTMap();
        }
        return recordTypeMap.get(rtId);
    }

    private void populateRTMap() {
        recordTypeMap = new Map<String, Id>();
        for (RecordType rt : [SELECT Id, DeveloperName
                                    FROM RecordType
                                   WHERE SObjectType = 'Contact_Information_MVN__c']) {
                recordTypeMap.put(rt.DeveloperName, rt.Id);
            }
        }
}