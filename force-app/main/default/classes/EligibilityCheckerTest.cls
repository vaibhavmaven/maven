@isTest
public class EligibilityCheckerTest {
    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
    static List<Program_Member_MVN__c> programMembers;
    static List<Account> patients;
    static Program_MVN__c program;
    static Prescription_MVN__c prescription;
    static Benefits_Coverage_MVN__c benefitCoverage;
    static Product_MVN__c productFamily;
    static Product_Line_Item_MVN__c product;
    
    
    public static Map<String, RecordType> accountRecordTypesByName {
        get {
            if (accountRecordTypesByName == null) {
                accountRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
                    accountRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return accountRecordTypesByName;
        }
        private set;
    }
    
    public static List<Account> createPatients(Integer numOfPatients) {
        List<Account> patientsList = new List<Account>();
        // we are creating 20 patients .. so diving these 20 in grousp of M, F, Male, Female.
        List<String> genderList = new List<String>{'Female', 'Male', 'M', 'F'}; 
        Integer counter = 1; 
        for(String genderStr : genderList){
            system.debug('Creating patients for Gender : ' + genderStr); 
            for (Integer i=0; i<5; i++) {
                Account patient= new Account();
                patient.FirstName = 'Test';
                patient.MiddleName = 'testing';
                patient.LastName = 'Patient' + counter;
                patient.Gender_MVN__c = genderStr; 
                System.debug(patient.LastName);
                patient.RecordTypeId = accountRecordTypesByName.get('Patient_MVN').id;
                counter++;          
                patientsList.add(patient);
            }            
        }
        
        insert patientsList;
        return patientsList;
   	}
 
    @testSetup
    static void setup(){
              
        patients = createPatients(20);    
        program = new Program_MVN__c();
        insert program;
        Program_Member_MVN__c programMember = new Program_Member_MVN__c();
        programMember.Program_MVN__c = program.id;
        programMember.Member_MVN__c = patients[0].id;
        programMember.Do_Not_Initialize_MVN__c = false;
        insert programMember;
        
        programMembers = new List<Program_Member_MVN__c>();
        programMembers.add(programMember);
        
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        List<Application_MVN__c> apps = new List<Application_MVN__c>();
        
        for (Integer i=0; i < 10; i++) {
            apps.add(
                applicationFactory.construct(
                    new Map<String, Object>{
                        'Program_Member_MVN__c' => programMembers[0].Id
                            }
                )
            );
        }
        insert apps;
        
        benefitCoverage= new  Benefits_Coverage_MVN__c();
        benefitCoverage.Program_Member_MVN__c = programMembers[0].Id;
        benefitCoverage.Application_MVN__c = apps[0].Id;
        benefitCoverage.Health_Insurance_MVN__c = 'Yes';
        insert benefitCoverage;
        productFamily = new Product_MVN__c();
        productFamily.Program_MVN__c = program.Id;
        productFamily.Product_Status_MVN__c = 'Active';
        insert productFamily;
        product = new Product_Line_Item_MVN__c();
        product.Product_MVN__c = productFamily.Id;
        product.Packaged_Quantity_MVN__c = 5;
        product.NDC_MVN__c = '123';
        insert product;
        prescription = new Prescription_MVN__c();
        prescription.Application_MVN__c = apps[0].Id;
        prescription.Program_Member_MVN__c = programMember.Id;
        prescription.Product_MVN__c =productFamily.Id;
        prescription.Timeframe_MVN__c = '30';
        prescription.Refills_MVN__c = 10;
        prescription.Quantity_MVN__c = 10;
        prescription.Signal_Code_MVN__c = 'QD - Once a day';   
        insert prescription;
        Eligibility_Response_Master__c elgbResponseMaster = new Eligibility_Response_Master__c(); 
        elgbResponseMaster.Benefits_Coverage__c = benefitCoverage.Id;
        elgbResponseMaster.NDC__c = product.NDC_MVN__c;
        elgbResponseMaster.Last_Query_Date__c = Date.today();
        insert elgbResponseMaster;
        FAS_Response_Master__c FASResponseMasterc = new FAS_Response_Master__c();
        FASResponseMasterc.Benefits_Coverage__c = benefitCoverage.Id;
        FASResponseMasterc.Last_Query_Date__c = Date.today();
        insert FASResponseMasterc;
    }
    
    @isTest
    public static void testcheckEligibilityRTBI(){       
        Benefits_Coverage_MVN__c benefitCoverage = [select id, Health_Insurance_MVN__c, Program_Member_MVN__c, Application_MVN__c,Name,
               Payer_Med_B_Plan_Sponsor_JKC__r.Name,Payer_Med_D_Plan_Sponsor_JKC__r.Name from Benefits_Coverage_MVN__c];
        Test.setMock(HttpCalloutMock.class, new EligibilityCheckerHttpCalloutMockRTBI());
        Test.startTest();
        EligibilityChecker.checkEligibility(benefitCoverage.Id);
        Test.stopTest();
    }
    
    @isTest
    public static void testcheckEligibilityELGB(){       
        Benefits_Coverage_MVN__c benefitCoverage = [select id, Health_Insurance_MVN__c, Program_Member_MVN__c, Application_MVN__c,Name,
               Payer_Med_B_Plan_Sponsor_JKC__r.Name,Payer_Med_D_Plan_Sponsor_JKC__r.Name from Benefits_Coverage_MVN__c];
        Test.setMock(HttpCalloutMock.class, new EligibilityCheckerHttpCalloutMockELGB());
        Test.startTest();
        EligibilityChecker.checkEligibility(benefitCoverage.Id);
        Test.stopTest();
    }
    
    @isTest
    public static void testcheckEligibilityFAS(){               
        Benefits_Coverage_MVN__c benefitCoverage = [select id, Health_Insurance_MVN__c, Program_Member_MVN__c, Application_MVN__c,Name,
               Payer_Med_B_Plan_Sponsor_JKC__r.Name,Payer_Med_D_Plan_Sponsor_JKC__r.Name from Benefits_Coverage_MVN__c];
        Test.setMock(HttpCalloutMock.class, new EligibilityCheckerHttpCalloutMockFAS());
        Test.startTest();
        EligibilityChecker.checkEligibility(benefitCoverage.Id);
        Test.stopTest();
    }
    
   

	private class EligibilityCheckerHttpCalloutMockELGB implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest httpReq)
        {
            HttpResponse httpRes = new HttpResponse(); 
            httpRes.setHeader('Content-Type', 'application/json'); 
            httpRes.setStatusCode(200);
            Map<String, Object> mapmap=new Map<String, Object>();
            mapmap.put('Patient','Test');
            mapmap.put('Prescriber','Test');
            mapmap.put('ProgramName','Test');
            mapmap.put('StatusMessage','Test');
            Map<String, Object> elgbResp =new Map<String, Object> ();
            String jason = '{'+
        '            \"Formulary\":\"3\",'+
        '        \"RetailPharmacy\":\"Not Covered\",'+
        '        \"MailOrderPrescriptionDrug\":\"Covered\",'+
        '        \"SpecialtyPharmacy\":null,'+
        '        \"StepTherapy\":\"false\",'+
        '        \"PriorAuthorization\":\"false\",'+
        '        \"StepMedication\":\"false\",'+
        '        \"BIN\":\"003858\",'+
        '        \"PCN\":\"A5\",'+
        '        \"GROUP\":null,'+
        '        \"RTBI\":\"true\",'+
        '        \"PayerName\":\"PBMA\",'+
        '        \"GROUPID\":\"AA5V\",'+
        '        \"CardholderID\":\"123456789\",'+
        '        \"PBMMemberID\":\"AA512345678900\"'+
        '        }';
            EligibilityResponse r = EligibilityResponse.parse(jason);
            elgbResp.put('KeyKey',r);
            mapmap.put('Status','True');
            mapmap.put('EligibilityResponse',elgbResp);
            httpRes.setBody(JSON.serialize(mapmap));
            return httpRes; 
        }
    }
	private class EligibilityCheckerHttpCalloutMockFAS implements HttpCalloutMock {
       
        public HttpResponse respond(HttpRequest httpReq)
        {
            HttpResponse httpRes = new HttpResponse(); 
            httpRes.setHeader('Content-Type', 'application/json'); 
            httpRes.setStatusCode(200);
            Map<String, Object> mapmap=new Map<String, Object>();
            Map<String, Object> fasMap =new Map<String, Object> ();
            mapmap.put('Status','13');
            Map<String, Object> detailMap =new Map<String, Object> ();
            Map<String, Object> fasDetailsMap =  new Map<String, Object>();
            fasDetailsMap.put('addr','Test');
            fasDetailsMap.put('city','Test');
            fasDetailsMap.put('dob','Test');
            fasDetailsMap.put('fname','Test');
            fasDetailsMap.put('fplPercentage','Test');
            fasDetailsMap.put('householdIncomeEstimate','Test');
            fasDetailsMap.put('householdSizeEstimate','Test');
            fasDetailsMap.put('lname','Test');
            fasDetailsMap.put('mname','Test');
            fasDetailsMap.put('ssn','Test');
            fasDetailsMap.put('state','Test');
            fasDetailsMap.put('zip','Test');
            List<Map<String, Object>> resultList = new List<Map<String, Object>>{fasDetailsMap};
            detailMap.put('result',resultList);
            fasMap.put('detail',detailMap);
            mapmap.put('Response',fasMap);
            httpRes.setBody(JSON.serialize(mapmap));
            return httpRes; 
        }
    }
	private class EligibilityCheckerHttpCalloutMockRTBI implements HttpCalloutMock {
       
        public HttpResponse respond(HttpRequest httpReq)
        {
            HttpResponse httpRes = new HttpResponse(); 
            httpRes.setHeader('Content-Type', 'application/json'); 
            httpRes.setStatusCode(200);
            Map<String, Object> mapmap=new Map<String, Object>();
            Map<String, Object> elgbResp =new Map<String, Object> ();
//            Map<String, Object> rtbimap=new Map<String, Object>();
            
            String jason =  '['+
                    ' {'+
                        ' \"Pharmacy\": {'+
                           ' \"Type\": \"Mail\",'+
                           ' \"NCPDPID\": \"2323239\",'+
                           ' \"NPI\": \"1538460530\",'+
                           ' \"PharmacyName\": \"Mail Order Pharmacy 10.6MU\",'+
                           ' \"AddressLine1\": \"9292 Langley Rd\",'+
                           ' \"AddressLine2\": \"Suite 100\",'+
                           ' \"City\": \"Phoenix\",'+
                           ' \"State\": \"AZ\",'+
                           ' \"Zip\": \"85001\"'+
                       ' },'+
                       ' \"PricingCoverage\": {'+
                            '\"DrugStatus\": \"CoveredWithRestrictions\",'+
                            '\"Message\": \"\",'+
                            '\"QuantityPriced\": \"120\",'+
                            '\"QuantityUnitDescription\": \"Tablet\",'+
                            '\"DaysSupplyPriced\": \"30\",'+
                            '\"PlanPayAmount\": \"193\",'+
                            '\"EstimatedPatientPayAmount\": \"20\",'+
                            '\"PriorAuthorizationRequired\": \"\",'+
                            '\"OOPAppliedAmount\": \"\",'+
                            '\"OOPRemainingAmount\": \"\",'+
                            '\"DeductibleAppliedAmount\": \"0\",'+
                            '\"DeductibleRemainingAmount\": \"0\",'+
                            '\"FormularyStatus\": \"\",'+
                            ' \"CoverageAlerts\": [{'+
                                '\"ReferenceCode\": \"QuantityLimits ‐ 30 per 30 Days\",'+
                                '\"TextMessage\": \"TextMessage ‐ THIS DRUG REQUIRES A NEBULIZER\",'+
                                '\"ReferenceText\": \"ResourceLink ‐ www.xyz.com\"'+ 
                            ' }]'+ 
                        ' }'+
                    '}'+
               ' ]';
            
            System.JSONParser parser = System.JSON.createParser(jason);
            RTBIResponse.PatientSpecificPricingCoverages  resp= new RTBIResponse.PatientSpecificPricingCoverages(parser);
            elgbResp.put('PatientSpecificPricingCoverages',new List<RTBIResponse.PatientSpecificPricingCoverages>{resp});
            elgbResp.put('Status','True');
//            elgbResp.put('Din',rtbimap);
            mapmap.put('EligibilityResponse',elgbResp);
            httpRes.setBody(JSON.serialize(mapmap));
            return httpRes; 
        }
    }
}