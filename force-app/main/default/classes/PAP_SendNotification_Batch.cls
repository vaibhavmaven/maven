global class PAP_SendNotification_Batch implements Database.Batchable<sObject>, Database.allowsCallouts{ 
	 global Database.QueryLocator start(Database.BatchableContext bc){
          return Database.getQueryLocator([
              SELECT Id,Name,
                Program_Member_MVN__c,
                Program_Member_MVN__r.Program_MVN__r.Program_ID_MVN__c, 
				Status_MVN__c,              
                Application_Status_Date_JKC__c,
              	Program_Member_MVN__r.Name,
                Program_Member_MVN__r.Patient_Status_MVN__c,
				Program_Member_MVN__r.Patient_Status_Date_JKC__c,
				Program_Member_MVN__r.Notified_HCP_JKC__c,
				Program_Member_MVN__r.Notified_Patient_JKC__c,
				Program_Member_MVN__r.Notified_Sponsor_JKC__c
            FROM
                Application_MVN__c
            WHERE
                (Application_Status_Date_JKC__c = TODAY OR Program_Member_MVN__r.Patient_Status_Date_JKC__c = TODAY)
            AND
              (Program_Member_MVN__r.Notified_Patient_JKC__c  = false 
               OR Program_Member_MVN__r.Notified_HCP_JKC__c = false 
               OR Program_Member_MVN__r.Notified_Sponsor_JKC__c =false )
            AND
                 Is_Application_Active_MVN__c = true
           ]);
         
 	}
 	 global void execute(Database.BatchableContext BC, List<Application_MVN__c> applications) {
       Boolean reenrollFlag = false; 
       Boolean buttonFlag = false; 
       String SendPatientResult = '';
       String SendHCPResult = ''; 
       String SendPlanSponsorResult = '';
       String appStatus='';
    for (Application_MVN__c applicationObj : applications) {
        
        appStatus = applicationObj.Status_MVN__c;
       
       
            if (applicationObj.Program_Member_MVN__r.Patient_Status_MVN__c == System.Label.PM_Patient_Status_Opt_In_MVN)
            {
                if((appStatus == 'Fulfilled') && (applicationObj.Program_Member_MVN__r.Notified_Sponsor_JKC__c == false)){
                        SendPlanSponsorResult = PAP_SendNotification.sendNotification_PlanSponsor(applicationObj.Program_Member_MVN__c,reenrollFlag,buttonFlag );
                 
                }
            }
             if((appStatus == 'Fulfilled') ||(appStatus == 'Denied') ||(appStatus == 'Missing Information') ||
               (applicationObj.Program_Member_MVN__r.Patient_Status_MVN__c != System.Label.PM_Patient_Status_Opt_In_MVN))
            {            
                if(applicationObj.Program_Member_MVN__r.Notified_HCP_JKC__c == false){
                    SendHCPResult = PAP_SendNotification.sendNotification_HCP(applicationObj.Program_Member_MVN__c,reenrollFlag,buttonFlag );
                }
                if(applicationObj.Program_Member_MVN__r.Notified_Patient_JKC__c == false){
                    SendPatientResult = PAP_SendNotification.sendNotification_Patient(applicationObj.Program_Member_MVN__c,reenrollFlag,buttonFlag );
                }
            }
         try{
            //call update
                if(SendPatientResult=='Success'){
                    Program_Member_MVN__c updatepgmMemberObj = [select Id, Notified_Patient_JKC__c  from Program_Member_MVN__c where Id= :applicationObj.Program_Member_MVN__c];
                                   updatepgmMemberObj.Notified_Patient_JKC__c = true;
                                   update updatepgmMemberObj; 
                }
                if(SendPlanSponsorResult=='Success'){
                    Program_Member_MVN__c updatepgmMemberObj = [select Id, Notified_Sponsor_JKC__c  from Program_Member_MVN__c where Id= :applicationObj.Program_Member_MVN__c];
                                  updatepgmMemberObj.Notified_Sponsor_JKC__c = true;
                                  update updatepgmMemberObj; 
                }
              system.debug('SendHCPResult is : ' + SendHCPResult);
                if(SendHCPResult=='Success'){
                    Program_Member_MVN__c updatepgmMemberObj = [select Id, Notified_HCP_JKC__c,Is_Prescription_Generated_JKC__c,Document_Generated_Date_JKC__c  from Program_Member_MVN__c where Id= :applicationObj.Program_Member_MVN__c];
                                updatepgmMemberObj.Notified_HCP_JKC__c = true;                    			
                                update updatepgmMemberObj; 
                }
              else if(SendHCPResult.contains('BatchSRFaxSuccess')){
                  Program_Member_MVN__c updatepgmMemberObj = [select Id, Notified_HCP_JKC__c,Is_Prescription_Generated_JKC__c,Document_Generated_Date_JKC__c  from Program_Member_MVN__c where Id= :applicationObj.Program_Member_MVN__c];
                                //Update on 11/11/20 - added IF statement to trigger process builder
                                IF(updatepgmMemberObj.Is_Prescription_Generated_JKC__c = true)
                                {        
                                    updatepgmMemberObj.Is_Prescription_Generated_JKC__c = false;
                                    update updatepgmMemberObj;  
                                }              

                  				updatepgmMemberObj.Notified_HCP_JKC__c = true;
                    			updatepgmMemberObj.Is_Prescription_Generated_JKC__c = true;
                    			updatepgmMemberObj.Document_Generated_Date_JKC__c =system.now();
                                update updatepgmMemberObj; 
              }
              
        }catch(Exception ex){ 
            
            catchException(ex, 'sendNotification_Batch',400,applicationObj.Program_Member_MVN__r.Name );
        }   
	}//end for
}
    

/*
*   Method   : catchException
*   Desc     : Method to log details of Exception. 
*   @param   : Exception - The exception object 
*   @param   : String - Name of the method in which the exception occured.       
*   @return  : none
*/    
    private static void catchException(Exception ex , String methodName ,Integer responseCode, String ObjectId ){    
        PAP_ErrorLog.logErrorActivity(ex, methodName , responseCode,  ObjectId);       
    }  
 
    global void finish(Database.BatchableContext bc) {
        
    }
      
 
}