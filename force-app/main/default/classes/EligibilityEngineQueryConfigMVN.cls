/**
* @author Mavens
* @group EligibilityEngineMVN
* @description Class defining a query for an object related to an application in the Eligibility
*              engine. The query allows for retrieval of children of the related object, or stated
*              in a different way, this class allows access to sibling records of the application
* @example
EligibilityEngineQueryConfigMVN config = new EligibilityEngineQueryConfigMVN (
    'Program_Member_MVN__c', // the pm lookup field on the application
    'Program_Member_MVN__c' // the object to query
);
*/
public with sharing class EligibilityEngineQueryConfigMVN {
    /*
    * @description the lookup field on the application used to indicate the related object to query for
    */
    public String applicationField;

    /*
    * @description the object to query for
    */
    public String sObjectName;

    /*
    * @description a map of child relationship names to the fields that need to be queried on those
    * child objects
    */
    public Map<String, Set<String>> childRelationshipToFields;

    private Boolean defined;
    /*
    * @description indicates whether the class is defined and can be used to build a query
    */
    public Boolean isDefined {
        get {
            return defined;
        }
    }

    /*
    * @description simple constructor, no parameters. Creates an "undefined" config.
    */
    public EligibilityEngineQueryConfigMVN() {
        this.defined = false;
    }

    /*
    * @description constructor - creates a "defined" config
    * @param field the application referencing the object to query
    * @param objName the api name the object to query
    */
    public EligibilityEngineQueryConfigMVN(string field, string objName) {
        this.defined = true;
        this.applicationField = field;
        this.sObjectName = objName;
        this.childRelationshipToFields = new Map<String, Set<String>>();
    }

    /*
    * @description returns a key for an instance of a class. Can be used to combine two query configs
    *              for the same object/field combo into a single query
    * @return String representing the key for the instance of the class
    */
    public String getKey() {
        return sObjectName + applicationField;
    }

    /*
    * @description provides the ability to add child queries to the generated query
    * @param config a Map of Child object API names to a set of fields to query for that object
    * @return none
    * @example
        config.addChildConfiguration(
            new Map<String, Set<String>>{
                'Order_Schedulers__r' => new Set<String> {
                    'Prescription_MVN__r.Product_MVN__c',
                    'Grandfathered_Cutoff_Date_MVN__c'
                }
            }
        );
    */
    public void addChildConfiguration(Map<String, Set<String>> config) {
        for (String key : config.keySet()) {
            if (!this.childRelationshipToFields.containsKey(key)) {
                this.childRelationshipToFields.put(key, new Set<String>());
            }
            this.childRelationshipToFields.get(key).addAll(config.get(key));
        }
    }

    /*
    * @description provides the ability to add child queries to the generated query
    * @param config a Map of Child object API names to a set of fields to query for that object
    * @return List<sObject> list of data retrieved based on the configured query
    */
    public List<SObject> getData(Id applicationId) {
        if (childRelationshipToFields.isEmpty()) {
            return new List<SObject>();
        }

        Id referenceId = (Id) Database.query(
            'SELECT ' + applicationField + ' FROM Application_MVN__c WHERE Id = :applicationId'
        )[0].get(applicationField);

        String query = 'SELECT Id, {0} FROM {1} WHERE Id = :referenceId';
        List<String> subQueries = new List<String>();

        for (String relationship : childRelationshipToFields.keySet()) {
            String subQuery = '(SELECT {0} FROM ' + relationship + ')';
            String subQueryFields = String.join(
                new List<String>(childRelationshipToFields.get(relationship)),
                ', '
            );

            subQueries.add(
                String.format(subQuery, new List<String>{subQueryFields})
            );
        }
        List<String> arguments = new List<String>();
        arguments.add(String.join(subQueries, ','));
        arguments.add(sObjectName);

        String formattedQuery = String.format( query, arguments);
        return Database.query(formattedQuery);
    }
}