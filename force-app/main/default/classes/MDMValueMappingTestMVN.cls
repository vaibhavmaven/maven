/*
 * MDMValueMappingTestMVN
 * @created by: Pablo Roldan
 * @created Date: Oct 20, 2017
 * @description: Test class for MDMValueMappingMVN
 */
@isTest
private class MDMValueMappingTestMVN {
    @isTest
    static void itShouldGetMDMFieldMappingConfigured() {
        TestFactoryCustomMetadataMVN.setMocks();

        List<MDM_Value_Mapping_MVN__mdt> mdmValueMappings = new List<MDM_Value_Mapping_MVN__mdt>();
        Test.startTest();
        MDMConnectionsMVN mdmConnectionsCtrl = new MDMConnectionsMVN('ZZ');
        MDMFieldMappingMVN mdmFieldMappingCtrl = new MDMFieldMappingMVN((String) mdmConnectionsCtrl.setting.Id);

        System.assertNotEquals(null, mdmFieldMappingCtrl.settings);
        System.assert(!mdmFieldMappingCtrl.settings.isEmpty());

        Boolean mdmMappingChecked = false;

        for(MDM_Field_Mapping_MVN__mdt mdmFieldMapping : mdmFieldMappingCtrl.settings) {
            MDMValueMappingMVN mdmValueMappingsCtrl = new MDMValueMappingMVN();
            mdmValueMappings = mdmValueMappingsCtrl.filterByFieldMapping((String) mdmFieldMapping.Id);
            
            if(mdmValueMappings != null && !mdmValueMappings.isEmpty()) {
                for(MDM_Value_Mapping_MVN__mdt mdmValueMapping : mdmValueMappings) {
                    System.assertEquals((String) mdmFieldMapping.Id, (String) mdmValueMapping.MDM_Field_Mapping_MVN__c);
                }

                mdmMappingChecked = true;
            }
        }
        
        Test.stopTest();
        
        System.assert(mdmMappingChecked);
    }
}