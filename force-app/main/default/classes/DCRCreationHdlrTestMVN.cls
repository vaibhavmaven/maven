@isTest
private class DCRCreationHdlrTestMVN {

    /**
     * Bulk number
     */
    static final Integer BULK_TEST = 200;

    /**
     * Admin user used for tests
     */
    static User adminUser;

    /**
     * Test user used for tests
     */
    static User testUser;

    /**
     * Account Test Factory
     */
    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();

    /**
     * Address Test Factory
     */
    static TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();

    /**
     * Static block to initializes default tests data
     */
    static {
        createUsers();

        DCRGlobalSettingMVN.setMock(
            (DCR_Global_Setting_MVN__mdt) TestFactoryCustomMetadataMVN.createMetadata (
                'DCR_Global_Setting_MVN__mdt',
                new Map<String, Object>{'DCR_Affiliation_Processing_MVN__c' => false,
                                        'DCR_Approval_Required_for_Update_MVN__c' => false,
                                        'DCR_Create_Handler_Class_MVN__c' => null,
                                        'DCR_Excluded_Profile_Names_MVN__c' => null,
                                        'DCR_Globally_Active_MVN__c' => false}
            )
        );

        List<Account> hcos = createHCOs();
        List<Account> hcps = createHCPs();

        createAffiliations(hcps, hcos);

        createAddresses(hcos, hcps);

        initializeDCRSettings();
    }

    static void createUsers() {
        TestFactoryUserMVN testUserFactory = new TestFactoryUserMVN();
        adminUser = testUserFactory.createAdminUser();
        testUser = testUserFactory.create();
    }

    static List<Account> createHCOs() {
        List<Account> hcos = new List<Account>();

        for (Integer index = 0; index < BULK_TEST; index++) {
            Map<String, Object> fieldValues = new Map<String, Object> {
                'Name' => 'HCO' + index,
                'DCR_Override_MVN__c' => true,
                'RecordTypeId' => TestFactoryAccountMVN.accountRecordTypesByName.get('Hospital_vod').id
            };

            hcos.add(accountFactory.construct(fieldValues));
        }

        insert hcos;

        return hcos;
    }

    static List<Account> createHCPs() {
        List<Account> hcps = new List<Account>();

        for (Integer index = 0; index < BULK_TEST; index++) {
            Map<String, Object> fieldValues = new Map<String, Object> {
                'FirstName' => 'DR',
                'LastName' => 'HCP' + index,
                'DCR_Override_MVN__c' => true,
                'RecordTypeId' => TestFactoryAccountMVN.accountRecordTypesByName.get('Professional_vod').id
            };

            hcps.add(accountFactory.construct(fieldValues));
        }

        insert hcps;

        return hcps;
    }

    static void createAffiliations(List<Account> hcps, List<Account> hcos) {
        List<Affiliation_MVN__c> affiliations = new List<Affiliation_MVN__c>();

        for (Integer index = 0; index < hcos.size(); index++) {
            affiliations.add(new Affiliation_MVN__c(
                Parent_Account_MVN__c = hcos[index].Id,
                Child_Account_MVN__c = hcps[index].Id,
                DCR_Override_MVN__c = true
            ));
        }

        insert affiliations;
    }

    static void createAddresses(List<Account> hcps, List<Account> hcos) {
        List<Address_vod__c> addresses = new List<Address_vod__c>();

        for (Account hcp : hcps) {
            addresses.add(new Address_vod__c(
                Account_vod__c = hcp.Id,
                Name = 'Address ' + hcp.Id,
                DCR_Override_MVN__c = true
            ));
        }

        for (Account hco : hcos) {
            addresses.add(new Address_vod__c(
                Account_vod__c = hco.Id,
                Name = 'Address ' + hco.Id,
                DCR_Override_MVN__c = true
            ));
        }

        insert addresses;
    }

    static void initializeDCRSettings() {
        DCRUtilityMVN dcrUtility = new DCRUtilityMVN();
        DCRFieldSettingsMVN.setMockList(dcrUtility.buildMockFieldSettingsForTest());
        DCRGlobalSettingMVN.setMock(dcrUtility.buildMockDCRSettingsForTest());
        TestFactoryDCRSettingsMVN.setMocks();
    }

    @isTest static void testAccountDCRInsert() {
        Boolean areDCRsCreated = false;
        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ACCOUNT_MOCK);

        System.runAs(testUser) {
            records = getRecords(dcrSetting);

            Test.startTest();
            areDCRsCreated = DCRCreationHdlrMVN.createDCRs(records, 'New');
            Test.stopTest();
        }

        System.assert(areDCRsCreated, 'There is an issue creation Account DCRs.');

        checkDCRCreated(dcrSetting, records);
    }

    @isTest static void testAccountDCRUpdateNoChange() {
        Boolean areDCRsCreated = false;
        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ACCOUNT_MOCK);

        System.runAs(testUser) {
            records = getRecords(dcrSetting);
            Map<Id, SObject> oldRecordMap = new Map<Id, SObject>(records);

            Test.startTest();
            areDCRsCreated = DCRCreationHdlrMVN.createDCRs(records, 'Edit', oldRecordMap);
            Test.stopTest();
        }

        System.assert(!areDCRsCreated, 'There is an issue creation Account DCRs.');

        checkDCRNotCreated(dcrSetting, records);
    }

    @isTest static void testAccountDCRUpdateChange() {
        Boolean areDCRsCreated = false;
        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ACCOUNT_MOCK);

        Map<Id, SObject> oldRecordMap;

        System.runAs(testUser) {
            records = getRecords(dcrSetting);
            oldRecordMap = getOldMapChangingValues(records, dcrSetting);

            Test.startTest();
            areDCRsCreated = DCRCreationHdlrMVN.createDCRs(records, 'Edit', oldRecordMap);
            Test.stopTest();
        }

        System.assert(areDCRsCreated, 'There is an issue creation Account DCRs.');

        checkDCRCreated(dcrSetting, records);
    }

    @isTest static void testAddressDCRInsert() {
        Boolean areDCRsCreated = false;
        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ADDRESS_MOCK);

        System.runAs(testUser) {
            records = getRecords(dcrSetting);

            Test.startTest();
            areDCRsCreated = DCRCreationHdlrMVN.createDCRs(records, 'New');
            Test.stopTest();
        }

        System.assert(areDCRsCreated, 'There is an issue creating Address DCRs.');

        List<Data_Change_Request_MVN__c> dcrs = checkDCRCreated(dcrSetting, records);

        checkParentDCRs(dcrs);
    }

    @isTest static void testAddressDCRUpdateNoChange() {
        Boolean areDCRsCreated = false;
        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ADDRESS_MOCK);

        System.runAs(testUser) {
            records = getRecords(dcrSetting);
            Map<Id, SObject> oldRecordMap = new Map<Id, SObject>(records);

            Test.startTest();
            areDCRsCreated = DCRCreationHdlrMVN.createDCRs(records, 'Edit', oldRecordMap);
            Test.stopTest();
        }

        System.assert(!areDCRsCreated, 'There is an issue creation Address DCRs.');

        checkDCRNotCreated(dcrSetting, records);
    }

    @isTest static void testAddressDCRUpdateChange() {
        Boolean areDCRsCreated = false;
        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ADDRESS_MOCK);

        System.runAs(testUser) {
            records = getRecords(dcrSetting);
            Map<Id, SObject> oldRecordMap = getOldMapChangingValues(records, dcrSetting);

            Test.startTest();
            areDCRsCreated = DCRCreationHdlrMVN.createDCRs(records, 'Edit', oldRecordMap);
            Test.stopTest();
        }

        System.assert(areDCRsCreated, 'There is an issue creation Address DCRs.');

        checkDCRCreated(dcrSetting, records);
    }

    @isTest static void testAffiliationDCRInsert() {
        Boolean areDCRsCreated = false;
        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.AFFILIATION_MOCK);

        System.runAs(testUser) {
            records = getRecords(dcrSetting);

            Test.startTest();
            areDCRsCreated = DCRCreationHdlrMVN.createDCRs(records, 'New');
            Test.stopTest();
        }

        System.assert(areDCRsCreated, 'There is an issue creating Affiliations DCRs.');

        List<Data_Change_Request_MVN__c> dcrs = checkDCRCreated(dcrSetting, records);

        checkParentDCRs(dcrs);
    }

    static DCR_Setting_MVN__mdt getDCRSetting(Map<String, Object> dcrSettingsMap) {
        DCRSettingsMVN dcrSetting = new DCRSettingsMVN(String.valueOf(dcrSettingsMap.get('SObject_Name_MVN__c')));

        System.assertNotEquals(null, dcrSetting.setting, 'DCR Setting is not configured.');

        return dcrSetting.setting;
    }

    static List<SObject> getRecords(DCR_Setting_MVN__mdt dcrSetting) {
        Map<String, Schema.SObjectField> sobjectFields = Schema.getGlobalDescribe().get(dcrSetting.SObject_Name_MVN__c).getDescribe().fields.getMap();
        List<String> fields = new List<String>(sobjectFields.keySet());

        String queryFields = String.join(fields, ',');

        String query = 'SELECT ' + queryFields + ' FROM ' + dcrSetting.SObject_Name_MVN__c;

        List<SObject> records = Database.query(query);

        System.assert(!records.isEmpty(), 'No records configured.');

        for (SObject record : records) {
            record.put('DCR_Override_MVN__c', false);
        }

        return records;
    }

    static Map<Id, SObject> getOldMapChangingValues(List<SObject> records, DCR_Setting_MVN__mdt dcrSetting) {
        if (records == null || records.isEmpty()) {
            return null;
        }

        Map<Id, SObject> oldRecordMap = new Map<Id, SObject>();

        List<DCR_Field_Setting_MVN__mdt> dcrFieldSettings = getValidDCRFieldSettings(records[0].getSobjectType().getDescribe().getName(), dcrSetting);

        for (SObject record : records) {
            oldRecordMap.put(record.Id, getOldRecord(record, dcrFieldSettings));
        }

        return oldRecordMap;
    }

    static SObject getOldRecord(SObject record, List<DCR_Field_Setting_MVN__mdt> dcrFieldSettings) {
        SObject recordCloned = record.clone();

        for (DCR_Field_Setting_MVN__mdt dcrFieldSetting : dcrFieldSettings) {
            Object newValue = record.get(dcrFieldSetting.Field_MVN__c) != null ?
                    record.get(dcrFieldSetting.Field_MVN__c) + 'TestUpdate' :
                    'TestUpdate';

            recordCloned.put(dcrFieldSetting.Field_MVN__c, newValue);
        }

        return recordCloned;
    }

    static List<DCR_Field_Setting_MVN__mdt> getValidDCRFieldSettings(String sobjectName, DCR_Setting_MVN__mdt dcrSetting) {
        List<DCR_Field_Setting_MVN__mdt> dcrFieldSettings = new List<DCR_Field_Setting_MVN__mdt>();

        String actualSObjectName = String.IsNotBlank(dcrSetting.Relationship_SObject_Name_MVN__c) ?
                dcrSetting.Relationship_SObject_Name_MVN__c :
                sobjectName;

        DCRFieldSettingsMVN dcrFieldSettingsMVN = new DCRFieldSettingsMVN();

        for (DCR_Field_Setting_MVN__mdt dcrFieldSetting : dcrFieldSettingsMVN.settings) {
            Boolean isValidFieldSetting = String.IsNotBlank(dcrFieldSetting.Field_MVN__c)
                && dcrFieldSetting.Object_MVN__c == actualSObjectName;

            if (isValidFieldSetting) {
                dcrFieldSettings.add(dcrFieldSetting);
            }
        }

        return dcrFieldSettings;
    }

    static List<Data_Change_Request_MVN__c> checkDCRCreated(DCR_Setting_MVN__mdt dcrSetting, List<SObject> records) {
        List<Id> recordIds = new List<Id>();

        String recordIdFieldName = String.IsNotBlank(dcrSetting.Relationship_Field_Name_MVN__c) ?
                dcrSetting.Relationship_Field_Name_MVN__c :
                'Id';

        for (SObject record : records) {
            Id recordId = String.valueOf(record.get(recordIdFieldName));
            System.assertNotEquals(null, recordId, 'Record Id for DCR cannot be null.');
            recordIds.add(recordId);
        }

        String query = 'SELECT Id, Parent_Data_Change_Request_MVN__c FROM Data_Change_Request_MVN__c WHERE ' + dcrSetting.SObject_Field_Id_MVN__c + ' IN :recordIds';

        List<Data_Change_Request_MVN__c> dcrs = (List<Data_Change_Request_MVN__c>) Database.query(query);

        System.assert(!dcrs.isEmpty(), 'DCRs were not created.');
        System.assertEquals(records.size(), dcrs.size());

        return dcrs;
    }

    static void checkDCRNotCreated(DCR_Setting_MVN__mdt dcrSetting, List<SObject> records) {
        List<Id> recordIds = new List<Id>();

        String recordIdFieldName = String.IsNotBlank(dcrSetting.Relationship_Field_Name_MVN__c) ?
                dcrSetting.Relationship_Field_Name_MVN__c :
                'Id';

        for (SObject record : records) {
            Id recordId = String.valueOf(record.get(recordIdFieldName));
            System.assertNotEquals(null, recordId, 'Record Id for DCR cannot be null.');
            recordIds.add(recordId);
        }

        String query = 'SELECT Id, Parent_Data_Change_Request_MVN__c FROM Data_Change_Request_MVN__c WHERE ' + dcrSetting.SObject_Field_Id_MVN__c + ' IN :recordIds';

        List<Data_Change_Request_MVN__c> dcrs = (List<Data_Change_Request_MVN__c>) Database.query(query);

        System.assert(dcrs.isEmpty(), 'DCRs were created.');
    }

    static void checkParentDCRs(List<Data_Change_Request_MVN__c> dcrs) {
        for (Data_Change_Request_MVN__c dcr : dcrs) {
            System.assertNotEquals(null, dcr.Parent_Data_Change_Request_MVN__c);
        }
    }
}