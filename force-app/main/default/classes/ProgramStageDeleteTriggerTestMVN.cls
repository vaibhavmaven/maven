/**
 *  ProgramStageDeleteTriggerTestMVN
 *  Created By:     Brian Aggen
 *  Created On:     04/25/2016
 *  Description:    This is the test class for the ProgramStageDeleteTriggerMVN class.
 *                  
 **/
@isTest
private class ProgramStageDeleteTriggerTestMVN {

	@testSetup
	private static void setup() {
		TestDataFactoryMVN.createSearchSettings();
		TestDataFactoryMVN.createPSSettings();

		Program_MVN__c program = TestDataFactoryMVN.createProgram();
		Program_Stage_MVN__c parentStage = TestDataFactoryMVN.createParentProgramStage(program);
		Program_Stage_MVN__c childStage = TestDataFactoryMVN.createChildProgramStage(program, parentStage);
	}

	@isTest 
    private static void testDeleteParentStage() {
		Program_Stage_MVN__c parentStage = [SELECT Id FROM Program_Stage_MVN__c WHERE Name = 'Initial Contact' LIMIT 1];
		Program_Stage_MVN__c childStage = [SELECT Id FROM Program_Stage_MVN__c WHERE Name = 'Child Stage' LIMIT 1];

		Id parentStageId = parentStage.Id;
		Id childStageId = childStage.Id;

		Test.startTest();
			delete parentStage;
		Test.stopTest();

		//Program_Stage_MVN__c deletedParentStage = [select Id from Program_Stage_MVN__c where Id = :parentStageId];

		System.assert([select count() from Program_Stage_MVN__c where Id = :parentStageId] == 0, 'The parent stage was not deleted');
		System.assert([select count() from Program_Stage_MVN__c where Id = :childStageId] == 0, 'The child stage was not deleted');
	}
	
}