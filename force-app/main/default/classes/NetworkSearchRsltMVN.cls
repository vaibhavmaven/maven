/**
* @author Mavens
* @group Network API
* @description Manages interactions with the Search/Retrieve functionality in the Veeva Network API
*
* @API
* - NetworkSearchRsltMVN(VeevaNetworkConnectionUtilMVN.NetworkRequestMVN, Map<String, Object>, List<VeevaNetworkConnectionUtilMVN.NetworkFieldMappingMVN>)
*       Builds data change requests in Salesforce from the Network API response.
*       If this process fails, data change requests will be null and the error stored in the errors parameter.
*/
public with sharing class NetworkSearchRsltMVN {

    /**
    * @description Veeva Network API method used for the endpoint to search records
    */
    public static final String SEARCH_API_METHOD = '/search?q=*&offset=0&statuses={0}&limit={1}&types={2}{3}&supplemental=ALL&parenthcofilters=hco_status__v:A';

    /**
    * @description Veeva Network API method used for the endpoint to retrieve records
    */
    public static final String RETRIEVE_API_METHOD = '/entities/batch';

    /**
    * @description List of errors during the process in this class
    */
    public Set<String> errors;

    /**
    * @description List of Veeva Network search data results
    */
    public List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> searchNetworkDataRslts;

    /**
    * @description Veeva Network Request inner class used to store necessary parameters to call Veeva Network
    */
    private static VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRequest;

    /**
    * @description Search response from Veeva Network stored in a map<Key, Value (Object)>
    */
    private static Map<String, Object> searchResponse;

    /**
    * @description List of Field mappings between Veeva Network and Salesforce
    */
    private static List<VeevaNetworkConnectionUtilMVN.NetworkFieldMappingMVN> networkFieldMappings;

    /**
    * @description Constructor for searching in Network
    *       Initializes Veeva Network Request and Search Response from Veeva Network.
    *       If this process gets all data from search successfully, it will populate the searchNetworkDataRslts property.
    *       If the process fails, it will populate the property error.
    * @param    networkRequestEntry         Veeva Network Request inner class
    * @param    searchResponseEntry         Search Data Response from Veeva Network
    * @param    networkFieldMappingsEntry   Field Mapping between Veeva Network and Salesforce
    * @example
    * * NetworkSearchRsltMVN ctrl = new NetworkSearchRsltMVN(networkRequestEntry, searchResponseEntry, networkFieldMappingsEntry);
    */
    public NetworkSearchRsltMVN(VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRequestEntry,
            Map<String, Object> searchResponseEntry,
            List<VeevaNetworkConnectionUtilMVN.NetworkFieldMappingMVN> networkFieldMappingsEntry) {
        errors = new Set<String>();

        if (networkRequestEntry != null && searchResponseEntry != null && !searchResponseEntry.keySet().isEmpty()) {
            networkRequest = networkRequestEntry;
            searchResponse = searchResponseEntry;
            networkFieldMappings = networkFieldMappingsEntry;

            try {
                searchNetworkDataRslts = getNetworkSearchRslt();
            } catch (Exception ex) {
                errors.add(ex.getMessage());
            }
        } else {
            errors.add(Label.No_Search_Response_From_Veeva_Network_MVN);
        }
    }

    /**
    * @description Gets a list of Veeva Network Data results from Veeva Network search
    * @return   List of Veeva Network Data results
    * @example
    * * List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> networkSearchRslt = getNetworkSearchRslt();
    */
    private static List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> getNetworkSearchRslt() {
        List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> networkSearchRslts = new List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN>();

        Object entitiesObject = searchResponse.get('entities');
        Map<String, Object> parentHcosById = getSupplementalResultsByParentHcoId(searchResponse.get(VeevaNetworkConnectionUtilMVN.networkSettings.parentAttrRecordName));

        if (entitiesObject != null) {
            List<Object> entities = (List<Object>) entitiesObject;

            for (Object entityObject : entities) {
                Map<String, Object> entity = (Map<String, Object>) entityObject;
                VeevaNetworkConnectionUtilMVN.NetworkResultMVN networkResult = getNetworkResultFromEntity(entity, parentHcosById);

                if (networkResult != null) {
                    networkSearchRslts.add(networkResult);
                }
            }
        }

        return networkSearchRslts;
    }

    /**
    * @description Gets supplementalResults parameters which contain all information from parent
    *       HCOs stored by parent HCO id
    * @param    supplementalResultsObject   data for parent HCOs
    * @return                               map of supplemental results by parent HCO id
    * @example
    * * Map<String, Object> supplementalResultsByParentHcoId = getSupplementalResultsByParentHcoId(supplementalResultsObject);
    */
    private static Map<String, Object> getSupplementalResultsByParentHcoId(Object supplementalResultsObject) {
        Map<String, Object> supplementalResultsByParentHcoId = new Map<String, Object>();

        List<Object> supplementalResults = supplementalResultsObject != null ?
                (List<Object>) supplementalResultsObject :
                new List<Object>();

        for (Object parentHcoObj : supplementalResults) {
            Map<String, Object> parentHco = parentHcoObj != null ?
                    (Map<String, Object>) parentHcoObj :
                    null;

            String parentHcoId = parentHco != null && parentHco.containsKey(VeevaNetworkConnectionUtilMVN.networkSettings.entityIdFieldName) ?
                    String.valueOf(parentHco.get(VeevaNetworkConnectionUtilMVN.networkSettings.entityIdFieldName)) :
                    null;

            if (String.IsNotBlank(parentHcoId)) {
                supplementalResultsByParentHcoId.put(parentHcoId, parentHco);
            }
        }

        return supplementalResultsByParentHcoId;
    }

    /**
    * @description Gets Network Result inner class data from entity search result
    * @param    entity          map of fields from entity in Veeva Network
    * @param    parentHcosById  all data for parent HCOs by parent HCO id
    * @return                   veeva Network Result inner class
    * @example
    * * VeevaNetworkConnectionUtilMVN.NetworkResultMVN supplementalResultsByParentHcoId = getNetworkResultFromEntity(entity, parentHcosById);
    */
    private static VeevaNetworkConnectionUtilMVN.NetworkResultMVN getNetworkResultFromEntity(Map<String, Object> entity,
            Map<String, Object> parentHcosById) {
        VeevaNetworkConnectionUtilMVN.NetworkResultMVN networkResult;

        if (entity != null && !entity.keySet().isEmpty()) {
            String networkEntityType = String.valueOf(entity.get('entityType'));

            Map<String, Object> entityAttributes = entity.containsKey(VeevaNetworkConnectionUtilMVN.networkSettings.entityRecordName) ?
                    (Map<String, Object>) entity.get(VeevaNetworkConnectionUtilMVN.networkSettings.entityRecordName) :
                    null;

            Boolean hasEntityAnyAtrribute = entityAttributes != null && !entityAttributes.keySet().isEmpty();

            SObject salesforceEntity = hasEntityAnyAtrribute ?
                    getSObjectFromSearchResult(networkRequest.entitySObject, entityAttributes, networkEntityType) :
                    null;

            networkResult = getNetworkResultFromSalesforceEntity(salesforceEntity, entityAttributes, parentHcosById);
        }

        return networkResult;
    }

    /**
    * @description Gets Network Result inner class data from entity search result
    * @param    salesforceEntity    Salesforce Object data
    * @param    entityAttributes    Map of entity attributes
    * @param    parentHcosById      All data for parent HCOs by parent HCO id
    * @return                       Veeva Network Result inner class
    * @example
    * * VeevaNetworkConnectionUtilMVN.NetworkResultMVN networkResult = getNetworkResultFromEntityAttributesObject(salesforceEntity, entityAttributes, parentHcosById);
    */
    private static VeevaNetworkConnectionUtilMVN.NetworkResultMVN getNetworkResultFromSalesforceEntity(SObject salesforceEntity,
            Map<String, Object> entityAttributes,
            Map<String, Object> parentHcosById) {
        if (salesforceEntity == null) {
            return null;
        }

        VeevaNetworkConnectionUtilMVN.NetworkResultMVN networkResult;
        networkResult = new VeevaNetworkConnectionUtilMVN.NetworkResultMVN();
        networkResult.entity = salesforceEntity;
        networkResult.childSObjectsByName = new Map<String, List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN>>();

        if (networkRequest.childSObjectsByNetworkObject != null) {
            for (String networkObject : networkRequest.childSObjectsByNetworkObject.keySet()) {
                String salesforceObject = networkRequest.childSObjectsByNetworkObject.get(networkObject);

                List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> childSObjects = getChildSObjectsSearchRslt(networkObject,
                                                                        salesforceObject,
                                                                        entityAttributes,
                                                                        parentHcosById);

                if (childSObjects != null && !childSObjects.isEmpty()) {
                    networkResult.childSObjectsByName.put(networkObject, childSObjects);
                }
            }
        }

        return networkResult;
    }

    /**
    * @description Gets SObject from Network Result using Salesforce Object name and Network Record Type name
    * @param    salesforceObject    Salesforce Object Name
    * @param    networkResponse     Veeva Network response for current SObject
    * @param    networkRecordType   Veeva Network object Record Type name
    * @return                       SObject
    * @example
    * * SObject sObjectSearchRslt = getSObjectFromSearchResult(salesforceObject, networkResponse, networkRecordType);
    */
    private static SObject getSObjectFromSearchResult(String salesforceObject, Map<String, Object> networkResponse, String networkRecordType) {
        SObject sObjectSearchRslt;

        String entityId = (String) networkResponse.get(VeevaNetworkConnectionUtilMVN.networkSettings.entityIdFieldName);
        String entityStatus = getNetworkEntityStatus(networkResponse, networkRecordType);

        Boolean isValid = checkNetworkResponseIsValidForFilters(networkResponse, networkRecordType);

        Boolean isNetworkResponseValid = String.IsNotBlank(entityId) && String.IsNotBlank(entityStatus)
                && entityStatus == VeevaNetworkConnectionUtilMVN.networkSettings.activeCodeStatus && isValid;

        if (!isNetworkResponseValid) {
            return null;
        }

        sObjectSearchRslt = UtilitiesMVN.globalDescribe.get(salesforceObject).newSObject();
        sObjectSearchRslt.put(VeevaNetworkConnectionUtilMVN.networkSettings.networkExternalIdFieldName, entityId);
        sObjectSearchRslt.put('DCR_Override_MVN__c', true);

        Map<String, RecordType> recordTypeByName = UtilitiesMVN.getRecordTypesForObject(salesforceObject);

        RecordType salesforceRecordType = getSalesforceRecordType(salesforceObject, networkRecordType, recordTypeByName);

        String salesforceRecordTypeDeveloperName = null;
        if (salesforceRecordType != null) {
            salesforceRecordTypeDeveloperName = salesforceRecordType.DeveloperName;
            sObjectSearchRslt.put('RecordTypeId', salesforceRecordType.Id);
        }

        for (VeevaNetworkConnectionUtilMVN.NetworkFieldMappingMVN networkFieldMapping : networkFieldMappings) {
            Boolean appliesCurreentFieldMapping = checkIfAppliesCurrentFieldMapping(networkFieldMapping, salesforceObject, salesforceRecordTypeDeveloperName);

            Object newSalesforceFieldValue = appliesCurreentFieldMapping ?
                    getSalesforceNewValueFromNetworkSearchResult(networkFieldMapping, salesforceObject, networkResponse, sObjectSearchRslt.getSObjectType()) :
                    null;

            if (newSalesforceFieldValue != null) {
                String sfFieldName = (String) networkFieldMapping.fieldMapping.get('SF_Field_Name_MVN__c');

                sObjectSearchRslt.put(sfFieldName, newSalesforceFieldValue);
            }
        }

        return sObjectSearchRslt;
    }

    /**
    * Checks that current network Response is valid
    * @param    networkResponse     Veeva Network response for current SObject
    * @param    networkRecordType   Veeva Network object Record Type name
    * @return                       Boolean
    * @example
    * * Boolean isValid = checkNetworkResponseIsValid(networkResponse, networkRecordType);
    */
    private static Boolean checkNetworkResponseIsValidForFilters(Map<String, Object> networkResponse, String networkRecordType) {
        Boolean isValid = networkRecordType != VeevaNetworkConnectionUtilMVN.networkSettings.addressType;

        if (!isValid) {
            Integer addrRank = Integer.valueOf(networkResponse.get(VeevaNetworkConnectionUtilMVN.networkSettings.addressRankFieldName));
            Set<Integer> addrRankFilters = VeevaNetworkConnectionUtilMVN.networkSettings.addressRankFilter;

            isValid = addrRankFilters != null &&
                    ((!addrRankFilters.isEmpty() && addrRankFilters.contains(addrRank)) || addrRankFilters.isEmpty());
        }

        return isValid;
    }

    /**
    * @description Gets Child SObject for entity from Search Results
    * @param    networkObject       Veeva Network Object name
    * @param    salesforceObject    Salesforce Object name
    * @param    networkResponse     Veeva Network Search Response
    * @param    parentHcosById      Parent HCOs data stored by parent HCO id
    * @return                       List of Network Result inner class data
    * @example
    * * List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> childSObjectsSearchRslt = getChildSObjectsSearchRslt(
    * *         networkObject,
    * *         salesforceObject,
    * *         networkResponse,
    * *         parentHcosById
    * * );
    */
    private static List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> getChildSObjectsSearchRslt(String networkObject,
            String salesforceObject,
            Map<String, Object> networkResponse,
            Map<String, Object> parentHcosById) {
        List<Object> childObjectResponses = networkResponse.containsKey(networkObject) ?
                (List<Object>) networkResponse.get(networkObject) :
                new List<Object>();

        Boolean isNetworkParentHco = networkObject == VeevaNetworkConnectionUtilMVN.networkSettings.parentRecordName;
        Boolean isNetworkAddress = networkObject == VeevaNetworkConnectionUtilMVN.networkSettings.addressRecordName;

        if (isNetworkParentHco) {
            networkObject = VeevaNetworkConnectionUtilMVN.networkSettings.hcoType;
        } else if (isNetworkAddress) {
            networkObject = VeevaNetworkConnectionUtilMVN.networkSettings.addressType;
        }

        return getChildSObjectsSearchRsltFromNetworkObjectResponse(networkObject, salesforceObject, childObjectResponses, parentHcosById);
    }

    private static List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> getChildSObjectsSearchRsltFromNetworkObjectResponse(String networkObject,
            String salesforceObject,
            List<Object> childObjectResponses,
            Map<String, Object> parentHcosById) {
        List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> childSObjectsSearchRslts = new List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN>();

        String lowestNetworkId;
        Integer lowestValue;

        for (Object childObjectResponseObject : childObjectResponses) {
            Map<String, Object> childObjectResponse = childObjectResponseObject != null ?
                    (Map<String, Object>) childObjectResponseObject :
                    null;

            childObjectResponse = getChildParentHCOs(networkObject, childObjectResponse, parentHcosById);

            SObject childSObjectsSearchRslt = childObjectResponse != null && !childObjectResponse.keySet().isEmpty() ?
                    getSObjectFromSearchResult(salesforceObject, childObjectResponse, networkObject) :
                    null;

            if (childSObjectsSearchRslt != null && networkObject == VeevaNetworkConnectionUtilMVN.networkSettings.addressType) {
                Integer currentOrdinalValue = getAddressOrdinalValue(childObjectResponse);

                lowestValue = lowestValue == null || lowestValue >= currentOrdinalValue ?
                        currentOrdinalValue :
                        lowestValue;

                lowestNetworkId = lowestValue == null || lowestValue >= currentOrdinalValue ?
                        String.valueOf(childSObjectsSearchRslt.get(VeevaNetworkConnectionUtilMVN.networkSettings.networkExternalIdFieldName)) :
                        lowestNetworkId;
            }

            VeevaNetworkConnectionUtilMVN.NetworkResultMVN newChildResult = getNewChildResult(networkObject, childObjectResponse, childSObjectsSearchRslt);

            if (newChildResult != null) {
                childSObjectsSearchRslts.add(newChildResult);
            }
        }

        if (String.IsNotBlank(lowestNetworkId)) {
            for (VeevaNetworkConnectionUtilMVN.NetworkResultMVN childSObjectsSearchRslt : childSObjectsSearchRslts) {
                String currentNetworkId = String.valueOf(childSObjectsSearchRslt.entity.get(VeevaNetworkConnectionUtilMVN.networkSettings.networkExternalIdFieldName));

                if (currentNetworkId == lowestNetworkId) {
                    childSObjectsSearchRslt.entity.put('Primary_vod__c', true);
                } else {
                    childSObjectsSearchRslt.entity.put('Primary_vod__c', false);
                }
            }
        }

        return childSObjectsSearchRslts;
    }

    /**
     * Gets Children Parent HCOs data
     * @param   networkObject       Network object name
     * @param   childObjectResponse Map of Parent HCO responses by Id
     * @param   parentHcosById      Map of Parent HCO records by Id
     * @return                      Map of current Parent HCO fields data
     */
    private static Map<String, Object> getChildParentHCOs(String networkObject,
            Map<String, Object> childObjectResponse,
            Map<String, Object> parentHcosById) {
        Boolean isNetworkObjectHCOType = networkObject == VeevaNetworkConnectionUtilMVN.networkSettings.hcoType;

        if (isNetworkObjectHCOType) {
            String parentHcoId = String.valueOf(childObjectResponse.get(VeevaNetworkConnectionUtilMVN.networkSettings.parentIdFieldName));

            childObjectResponse = String.IsNotBlank(parentHcoId) && parentHcosById.containsKey(parentHcoId) ?
                    (Map<String, Object>) parentHcosById.get(parentHcoId) :
                    null;
        }

        return childObjectResponse;
    }

    private static VeevaNetworkConnectionUtilMVN.NetworkResultMVN getNewChildResult(String networkObject,
            Map<String, Object> childObjectResponse,
            SObject childSObjectsSearchRslt) {
        if (childSObjectsSearchRslt == null) {
            return null;
        }

        VeevaNetworkConnectionUtilMVN.NetworkResultMVN newChildResult = new VeevaNetworkConnectionUtilMVN.NetworkResultMVN();
        newChildResult.entity = childSObjectsSearchRslt;

        if (networkObject == VeevaNetworkConnectionUtilMVN.networkSettings.hcoType) {
            List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN> parentAddressesSObjects = getChildSObjectsSearchRslt(
                    VeevaNetworkConnectionUtilMVN.networkSettings.addressRecordName,
                    VeevaNetworkConnectionUtilMVN.ADDRESS_OBJECT,
                    childObjectResponse,
                    null
            );

            if (parentAddressesSObjects != null && !parentAddressesSObjects.isEmpty()) {
                newChildResult.childSObjectsByName = new Map<String, List<VeevaNetworkConnectionUtilMVN.NetworkResultMVN>> {
                        VeevaNetworkConnectionUtilMVN.networkSettings.addressRecordName => parentAddressesSObjects
                };
            }
        }

        return newChildResult;
    }

    /**
    * @description Gets Address ordinal (Rank) field value
    * @param    networkResponse     Veeva Network Search Response
    * @return                       Address ordinal (rank) field value
    * @example
    * * Integer addressOrdinalValue = getAddressOrdinalValue(networkResponse);
    */
    private static Integer getAddressOrdinalValue(Map<String, Object> networkResponse) {
        Integer addressOrdinalValue;

        Object addressOrdinal = networkResponse.get('address_ordinal__v');

        if (addressOrdinal != null && addressOrdinal instanceof Integer) {
            addressOrdinalValue = (Integer) addressOrdinal;
        }

        return addressOrdinalValue;
    }

    /**
    * @description Gets Veeva Network entity status from search response
    * @param    networkResponse     Veeva Network Search Response
    * @param    networkRecordType   Veeva Network Record Type name
    * @return                       Entity status
    * @example
    * * String networkEntityStatus = getNetworkEntityStatus(networkResponse, networkRecordType);
    */
    private static String getNetworkEntityStatus(Map<String, Object> networkResponse, String networkRecordType) {
        String networkEntityStatus;

        if (String.IsNotBlank(networkRecordType) && networkResponse.containsKey(networkRecordType.toLowerCase() + '_status__v')) {
            networkEntityStatus = (String) networkResponse.get(networkRecordType.toLowerCase() + '_status__v');
        }

        return networkEntityStatus;
    }

    /**
    * @description Gets Salesforce Record Type from Veeva Network Record Type name
    * @param    salesforceObject    Salesforce Object name
    * @param    networkRecordType   Veeva Network Record Type name
    * @param    recordTypeByName    Salesforce Record Types by Salesforce Record Type name
    * @return                       Salesforce Record Type
    * @example
    * * RecordType salesforceRecordType = getSalesforceRecordType(salesforceObject, networkRecordType, recordTypeByName);
    */
    private static RecordType getSalesforceRecordType(String salesforceObject, String networkRecordType, Map<String, RecordType> recordTypeByName) {
        String salesforceRecordTypeName = recordTypeByName != null && !recordTypeByName.keySet().isEmpty() ?
                getSalesforceRecordTypeNameFromNetwork(salesforceObject, networkRecordType) :
                null;

        RecordType newSalesforceRecordType = String.IsNotBlank(salesforceRecordTypeName) ?
                recordTypeByName.get(String.valueOf(salesforceRecordTypeName)) :
                null;

        Boolean hasError = String.IsNotBlank(salesforceRecordTypeName) && newSalesforceRecordType == null;

        if (hasError) {
            String exceptionError = String.format(Label.Salesforce_Record_Type_No_Configured_for_Veeva_Network_Record_Type_Error_MVN,
                    new List<String> {
                            salesforceRecordTypeName,
                            networkRecordType,
                            salesforceObject
                    }
            );

            throw new UtilitiesMVN.utilException(exceptionError);
        }

        return newSalesforceRecordType;
    }

    /**
    * @description Gets Salesforce Record Type name from Veeva Network Record Type name
    * @param    salesforceObject    Salesforce Object name
    * @param    networkRecordType   Veeva Network Record Type name
    * @return                       Salesforce Record Type name
    * @example
    * * String salesforceRecordTypeName = getSalesforceRecordTypeNameFromNetwork(salesforceObject, networkRecordType);
    */
    private static String getSalesforceRecordTypeNameFromNetwork(String salesforceObject, String networkRecordType) {
        String salesforceRecordType;

        for (VeevaNetworkConnectionUtilMVN.NetworkFieldMappingMVN networkFieldMapping : networkFieldMappings) {
            String sfFieldName = (String) networkFieldMapping.fieldMapping.get('SF_Field_Name_MVN__c');
            String sfObjectMapping = (String) networkFieldMapping.fieldMapping.get('SObject_API_Name_MVN__c');

            if (sfObjectMapping == salesforceObject && (sfFieldName == 'RecordTypeId' || sfFieldName == 'RecordType')) {
                MDM_Value_Mapping_MVN__mdt valueMapping = networkFieldMapping.getValueMappingFromMDMValue(networkRecordType, VeevaNetworkConnectionUtilMVN.INBOUND);

                if (valueMapping != null) {
                    salesforceRecordType = valueMapping.Salesforce_Value_MVN__c;
                } else {
                    String exceptionError = String.format(Label.Salesforce_Record_Type_No_Mapped_for_Veeva_Network_Record_Type_Error_MVN,
                            new List<String> {
                                    networkRecordType,
                                    salesforceObject
                            }
                    );

                    throw new UtilitiesMVN.utilException(exceptionError);
                }

                break;
            }
        }

        return salesforceRecordType;
    }

    /**
    * @description Checks if current Field Mapping applies for our SObject and Record Type name
    * @param    networkFieldMapping         Field mapping between Veeva Network and Salesforce
    * @param    salesforceObject            Salesforce Object name
    * @param    salesforceRecordTypeName    Salesforce Record Type name
    * @return                               Boolean which shows if Field Mapping applies for current Object
    * @example
    * * Boolean isValid = checkIfAppliesCurrentFieldMapping(networkFieldMapping, salesforceObject, salesforceRecordTypeName);
    */
    private static Boolean checkIfAppliesCurrentFieldMapping(VeevaNetworkConnectionUtilMVN.NetworkFieldMappingMVN networkFieldMapping,
            String salesforceObject,
            String salesforceRecordTypeName) {
        Boolean isValid = networkFieldMapping != null && networkFieldMapping.fieldMapping != null;

        if (isValid) {
            Boolean isAccountObject = networkFieldMapping.fieldMapping.get('SObject_API_Name_MVN__c') == VeevaNetworkConnectionUtilMVN.ACCOUNT_OBJECT &&
                    salesforceObject == VeevaNetworkConnectionUtilMVN.ACCOUNT_OBJECT;

            Boolean isAddressObject = networkFieldMapping.fieldMapping.get('SObject_API_Name_MVN__c') == VeevaNetworkConnectionUtilMVN.ADDRESS_OBJECT &&
                    salesforceObject == VeevaNetworkConnectionUtilMVN.ADDRESS_OBJECT;

            Boolean isValidRecordType = networkFieldMapping.fieldMapping.get('Record_Type_MVN__c') == 'All' ||
                    networkFieldMapping.fieldMapping.get('Record_Type_MVN__c') == salesforceRecordTypeName;

            isValid = isValid && (isAccountObject || isAddressObject) && isValidRecordType;
        }

        return isValid;
    }

    /**
    * @description Gets Salesforce field values from Veeva Network field values in the search result
    * @param    networkFieldMapping Field mapping between Veeva Network and Salesforce
    * @param    salesforceObject    Salesforce Object name
    * @param    networkResponse     Veeva Network response map
    * @param    sobjectType         SObject type
    * @return                       Salesforce field value as object
    * @example
    * * Object salesforceNewValueFromNetworkSearchResult = getSalesforceNewValueFromNetworkSearchResult(
    * *         networkFieldMapping,
    * *         salesforceObject,
    * *         networkResponse,
    * *         sobjectType
    * * );
    */
    private static Object getSalesforceNewValueFromNetworkSearchResult(VeevaNetworkConnectionUtilMVN.NetworkFieldMappingMVN networkFieldMapping,
            String salesforceObject,
            Map<String, Object> networkResponse,
            Schema.SObjectType sobjectType) {
        Object newSalesforceFieldValue;

        String sfFieldName = (String) networkFieldMapping.fieldMapping.get('SF_Field_Name_MVN__c');
        String mdmFieldName = (String) networkFieldMapping.fieldMapping.get('MDM_Field_Name_MVN__c');
        String sfObjectMapping = (String) networkFieldMapping.fieldMapping.get('SObject_API_Name_MVN__c');

        Boolean isCorrectMapping = salesforceObject == sfObjectMapping && String.IsNotBlank(sfFieldName) && String.IsNotBlank(mdmFieldName);

        if (isCorrectMapping) {
            newSalesforceFieldValue = networkResponse.get(mdmFieldName);

            MDM_Value_Mapping_MVN__mdt valueMapping = networkFieldMapping.getValueMappingFromMDMValue(
                    String.valueOf(newSalesforceFieldValue),
                    VeevaNetworkConnectionUtilMVN.INBOUND
            );

            Boolean isCorrectValueMapping = valueMapping != null && sfFieldName != 'RecordTypeId' && sfFieldName != 'RecordType';

            if (isCorrectValueMapping) {
                newSalesforceFieldValue = valueMapping.get('Salesforce_Value_MVN__c');
            }

            newSalesforceFieldValue = getNetworkFieldValueConvertedType(sobjectType, sfFieldName, newSalesforceFieldValue);
        }

        return newSalesforceFieldValue;
    }

    /**
    * @description Gets Veeva Network Field Final Value converted to Salesforce Field type
    * @param    sobjectType         SObject type
    * @param    sfFieldName         Salesforce Field Name
    * @param    newSalesforceValue  New Salesforce Field Value
    * @return                       Salesforce Field Value converted to Salesforce Field Type
    * @example
    * * Object newSalesforceValue = getNetworkFieldValueConvertedType(sobjectType, sfFieldName, newSalesforceValue);
    */
    private static Object getNetworkFieldValueConvertedType(Schema.SObjectType sobjectType, String sfFieldName, Object newSalesforceValue) {
        if (sobjectType != null && newSalesforceValue != null){
            Map<String, Schema.SObjectField> sobjFieldMap = UtilitiesMVN.getSobjectDescribe(sobjectType).fields.getMap();

            Schema.SObjectField field = sobjFieldMap.get(sfFieldName);

            Schema.DisplayType fieldType = field != null ?
                    field.getDescribe().getType() :
                    null;

            newSalesforceValue = getNetworkFieldValueConvertedTypeUsingDisplayType(fieldType, newSalesforceValue);
        } else {
            newSalesforceValue = null;
        }

        return newSalesforceValue;
    }

    /**
    * Gets Veeva Network Field Final Value converted to Salesforce Field type using DisplayType
    * @param    fieldType           Display Type for the field
    * @param    newSalesforceValue  New Salesforce Field Value
    * @return                       Salesforce Field Value converted to Salesforce Field Type
    * @example
    * * Object newSalesforceValue = getNetworkFieldValueConvertedTypeUsingDisplayType(fieldType, newSalesforceValue);
    */
    private static Object getNetworkFieldValueConvertedTypeUsingDisplayType(Schema.DisplayType fieldType, Object newSalesforceValue) {
        if (fieldType == null) {
            newSalesforceValue = null;
        } else if (fieldType == Schema.DisplayType.Integer) {
            newSalesforceValue = (Integer) newSalesforceValue;
        } else if (fieldType == Schema.DisplayType.Boolean) {
            newSalesforceValue = (Boolean) newSalesforceValue;
        } else if (fieldType == Schema.DisplayType.Currency || fieldType == Schema.DisplayType.Double || fieldType == Schema.DisplayType.Percent) {
            newSalesforceValue = (Double) newSalesforceValue;
        } else if (fieldType == Schema.DisplayType.Date) {
            newSalesforceValue = Date.valueOf(String.valueOf(newSalesforceValue));
        } else if (fieldType == Schema.DisplayType.DateTime) {
            newSalesforceValue = DateTime.valueOf(String.valueOf(newSalesforceValue));
        } else {
            newSalesforceValue = String.valueOf(newSalesforceValue);
        }

        return newSalesforceValue;
    }
}