/**
 * @author Mavens
 * @date August 2018
 * @grouop AssureIntegration
 * @description Mock class for Assure Web Service tests
 */
global class AssureWebServiceMockMVN implements WebServiceMock {

    private Integer responseType;

    global static final Integer RESPONSE_PASSED {
        get {
            return 0;
        }
    }
    global static final Integer RESPONSE_DENIED {
        get {
            return 1;
        }
    }
    global static final Integer RESPONSE_MISSING_INFO {
        get {
            return 2;
        }
    }
    global static final Integer RESPONSE_MISSING_INFO_AND_DENIED {
        get {
            return 3;
        }
    }
    global static final Integer RESPONSE_NO_CODE {
        get {
            return 4;
        }
    }

    /**
     * Constructor: pass the type of response to be returned
     * @param  type resonse type to be returned
     */
    global AssureWebServiceMockMVN(Integer type){
        this.responseType = type;
    }

    /**
     * Mock doInvoke method for Assure Web Service class
     */
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {

        AssureWebServiceMVN.GetDataResponse_element responseElement = 
            new AssureWebServiceMVN.GetDataResponse_element();
        
        AssureWebServiceValidateMVN.PractValidationType result = new AssureWebServiceValidateMVN.PractValidationType();

        if (this.responseType == RESPONSE_PASSED) {
            result.Returncode = 'E';
            result.ReturnDescription = 'HCP is Eligible';
        } else if (this.responseType == RESPONSE_DENIED) {
            result.Returncode = '9';
            result.ReturnDescription = 'HCP has ineligible license type';
        } else if (this.responseType == RESPONSE_MISSING_INFO) {
            result.Returncode = 'DEA-C';
            result.ReturnDescription = 'City or Zip Code discrepancy';
        } else if (this.responseType == RESPONSE_MISSING_INFO_AND_DENIED) {
            result.Returncode = 'DEA-C,9';
            result.ReturnDescription = 'CP has a name discrepancy when matched against data sources;' +
                                        'License Expired beyond the tolerance specified in client business rule';
        } else if (this.responseType == RESPONSE_NO_CODE) {
            result.Returncode = 'NO-CODE';
            result.ReturnDescription = '';
        }
        
        responseElement.GetDataResult = result;
        response.put('response_x', responseElement); 
   }
}