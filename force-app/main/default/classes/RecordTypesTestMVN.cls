/**
 * Record Types - Test
 * @company Mavens <https://mavens.com/>
 * @author  Jean-Philippe Monette <jpmonette@mavens.com>
 * @since   2016-05-25
 */
@isTest
private class RecordTypesTestMVN {
  
  @testSetup
  static void setup() {
    TestDataFactoryMVN.createPhysician();
  }

  @isTest
  static void it_should_get_an_individual_record_type() {
    Account acc = [SELECT RecordTypeId, RecordType.DeveloperName FROM Account LIMIT 1];

    Test.startTest();
    RecordType rt = RecordTypesMVN.get(acc.RecordTypeId);
    Test.stopTest();

    System.assertEquals(acc.RecordType.DeveloperName, rt.DeveloperName);
  }
  
  @isTest
  static void it_should_cache_record_type_requests() {
    Test.startTest();
    RecordTypesMVN.cache(Account.SObjectType);
    Integer firstCall = Limits.getQueries();

    RecordTypesMVN.cache(Account.SObjectType);
    Integer secondCall = Limits.getQueries();
    Test.stopTest();

    System.assertEquals(firstCall, secondCall);
  }
  
  @isTest
  static void it_should_multiple_cache_record_type_requests() {
    Test.startTest();
    RecordTypesMVN.cache(new Set<SObjectType>{Account.SObjectType, Task.SObjectType});
    Integer firstCall = Limits.getQueries();

    RecordTypesMVN.cache(new Set<SObjectType>{Account.SObjectType, Task.SObjectType});
    RecordTypesMVN.cache(Account.SObjectType);
    Integer secondCall = Limits.getQueries();
    Test.stopTest();

    System.assertEquals(firstCall, secondCall);
  }

  @isTest
  static void it_should_get_id_by_sobject_type_and_developer_name() {
    RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Professional_vod'];

    Test.startTest();
    Integer firstCall = Limits.getQueries();

    Id rtId = RecordTypesMVN.getId(Account.SObjectType, 'Professional_vod');
    RecordTypesMVN.getId(Account.SObjectType, 'Professional_vod');
 
    Integer secondCall = Limits.getQueries();
    Test.stopTest();

    System.assertEquals(rt.Id, rtId);
    System.assertEquals(secondCall, firstCall + 1, 'Second request should have been cached.');
  }

  @isTest
  static void it_should_get_id_by_sobject_type_and_name() {
    Test.startTest();
    RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Professional_vod'];
    Id rtId = RecordTypesMVN.getIdByName(Account.SObjectType, 'HCP');
    Test.stopTest();

    System.assertEquals(rt.Id, rtId);
  }

}