/**
 * @author Mavens
 * @date October 2018
 * @description This class builds the dropdown with available record types for case creation for the
 *              "New Case" button on the Case list view. When selected and once the user clicks
 *              "Continue" it calls the CreateCaseRedirectMVN page for the actual case creation.
 */
public with sharing class NewCaseCtrlMVN {
    /**
     * Builds the dropdown field with the record types defined in the patient service custom setting
     */ 
    public List<SelectOption> recordTypeOptions {
        get {
            String recordTypeList = Patient_Service_Settings_MVN__c.getInstance().New_Case_Record_Types_MVN__c;
            recordTypeOptions = new List<SelectOption>();
            for (String recordType : recordTypeList.split(',')) {
                recordTypeOptions.add(new selectOption(recordTypesMap.get(recordType.trim()), 
                                                        recordTypesByDeveloperName.get(recordType.trim())));
            }
            return recordTypeOptions;
        }
        set;
    }

    public String recordTypeSelection { get; set; }

    private Map<String, String> recordTypesByDeveloperName;
    private static final Map<String, String> recordTypesMap = new Map<String, String> {
        'Request_PS_MVN' => 'request',
        'Interaction_PS_MVN' => 'interaction',
        'Activity_PS_MVN' => 'activity'
    };

    /**
     * Constructor. Gets Record Type Names to display them as labels. Defaults the record type
     * selection with the value defined in the patient service custom setting.
     * @param  controller standard controller
     */
    public NewCaseCtrlMVN(ApexPages.StandardController controller) {
        getCaseRecordTypes();
        recordTypeSelection = recordTypesByDeveloperName.get(
            Patient_Service_Settings_MVN__c.getInstance().New_Case_Default_Record_Type_MVN__c
        );
    }

    /**
     * Retrieves case record types and builds a map with key: developername and value: name
     */ 
    private void getCaseRecordTypes() {
        List<RecordType> caseRecordTypes = [
            SELECT 
                Id, Name, DeveloperName
            FROM 
                RecordType
            WHERE 
                IsActive = true
            AND
                SObjectType = 'Case'
        ];

        recordTypesByDeveloperName = new Map<String, String>();
        for (RecordType myRecordType : caseRecordTypes) {
            recordTypesByDeveloperName.put(myRecordType.DeveloperName, myRecordType.Name);
        }
    }

    /**
     * Helper method called to set the recordTypeSelection
     */ 
    public void setParam() {
        System.debug(recordTypeSelection);
    }
}