/**
 *	AddressDeleteTriggerMVN
 *	Created By:		Aggen
 *	Created On:		01/11/2016
 *  Description:	This is a trigger class that removes related Contact Information records when an Address is deleted
 *					
 **/
public class AddressDeleteTriggerMVN implements TriggerHandlerMVN.HandlerInterface {
	public static List<Id> deletedAddressIDs;
	public static List<Contact_Information_MVN__c> removeContInfo;

	public void handle() {
		if(Trigger.isDelete) {
			if(Trigger.isBefore) {
				deletedAddressIDs = new List<Id>();
				removeContInfo = new List<Contact_Information_MVN__c>();

				for(Address_vod__c deletedAddr : (List<Address_vod__c>) Trigger.old) {
					deletedAddressIDs.add(deletedAddr.Id);
				}

				for(Contact_Information_MVN__c removeCI : [select Id, Address_MVN__c, Related_Address_Phone_Field_MVN__c from Contact_Information_MVN__c where Address_MVN__c in :deletedAddressIDs or Location_MVN__c in :deletedAddressIDs]) {
					if(removeCI.Related_Address_Phone_Field_MVN__c != null || removeCI.Address_MVN__c != null) {
						removeContInfo.add(removeCI);
					}
				}
			}

			if(Trigger.isAfter && removeContInfo.size() > 0) {
				delete removeContInfo;
			}
		}
	}
}