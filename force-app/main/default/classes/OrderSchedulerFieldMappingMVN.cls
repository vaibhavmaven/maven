/**
 * @author Mavens
 * @date 07/13/2018
 * @description Class to manage retrieving the fields from the Order Scheduler custom metadata type
 */ 
public with sharing class OrderSchedulerFieldMappingMVN {

    private static List<Order_Scheduler_Field_Mapping_MVN__mdt> testMock;

    public static List<Order_Scheduler_Field_Mapping_MVN__mdt> allMappings {
        get {
            if (allMappings == null) {
                Map<String, Schema.SObjectField> fieldMapping = 
                    Order_Scheduler_Field_Mapping_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
                List<String> fields = new List<String>(fieldMapping.keySet());

                String queryString = 'SELECT ' + String.escapeSingleQuotes(String.join(fields, ','))
                                    + ' FROM Order_Scheduler_Field_Mapping_MVN__mdt';
                allMappings = Database.query(queryString);
            }
            if(testMock != null){
                allMappings = testMock;
            }
            return allMappings;
        }
        private set;
    }

    public List<Order_Scheduler_Field_Mapping_MVN__mdt> getOrderSchedulerMappingFiltered(String filterField, String filterValue) {
        List<Order_Scheduler_Field_Mapping_MVN__mdt> filteredMapping = new List<Order_Scheduler_Field_Mapping_MVN__mdt>();
        for (Order_Scheduler_Field_Mapping_MVN__mdt mapping : allMappings) {
            if (mapping.get(filterField) == filterValue) {
                filteredMapping.add(mapping);
            }
        }
        return filteredMapping;
    } 

    @TestVisible
    private static void setMocks(List<Order_Scheduler_Field_Mapping_MVN__mdt> mocks){
        testMock = mocks;
    }
}