/*
*   Class : PAP_ErrorLog.cls 
*   Desc  : Log the exception into Error_Log_PAP__c object for future debugging the issue. 
*   Created By & Date : PSL Team,17/02/2020 
*/
public class PAP_ErrorLog {
    
    /*
    *   Method : logErrorActivity
    *   Desc   : insert error logs in salesforce object
    *   @param e : exception object, occured while apex transction,
    *   @param operation : String that holds any identification we need put in, ex. 'While Confirmation Surprise', 'Searching Article', 'Case Insert'
    *   @param isInsertimmediate : Boolean var indicates whether the logs are inserted immediatly or after preparing list of error logs, insert in bulk.
    *   @return ErrorLog__c
    */
    public static Error_Log_PAP__c logErrorActivity(Exception e,String methodName,Integer responseCode, String ObjectId) {
        Error_Log_PAP__c errLog  = new Error_Log_PAP__c (); 
        errLog.Operation__c  	= methodName;           
        errLog.Error_Message__c = e.getMessage();
        errLog.Program_Type__c  = 'Bausch PAP';
        errLog.Error_Type__c    = 'Error';
        errLog.Error_Code__c    = responseCode;
        errLog.Object_Id__c     = ObjectId;
        if(Schema.sObjectType.Error_Log_PAP__c.isCreateable()){
            Insert errLog;
        }
        return errLog;
    }
}