/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description implements string comparisons
 */
public with sharing class EligibilityEngineStringCompHdlrMVN extends EligibilityEngineRuleHdlrMVN {
    /**
     * checks if rule is fired for string comparisons, comparing the source field with the value
     * @return  Boolean     flag if rule is fired
     */
    protected override Boolean evaluateSingleRecord(SObject obj)  {
        Object sourceFieldObject = super.getSource(obj);
        String value = String.valueOf(rule.Value_MVN__c);
        if (sourceFieldObject != null) {
            String sourceField = String.valueOf(sourceFieldObject);
            if(super.rule.Comparison_MVN__c == 'Equal_To_MVN') {
                return sourceField.equals(value);
            } else if(super.rule.Comparison_MVN__c == 'Not_Equal_To_MVN') {
                return !sourceField.equals(value);
            } else if(super.rule.Comparison_MVN__c == 'Contains_MVN') {
                return sourceField.contains(value);
            } else if(super.rule.Comparison_MVN__c == 'Does_Not_Contain_MVN') {
                return !sourceField.contains(value);
            } else if(super.rule.Comparison_MVN__c == 'Starts_With_MVN') {
                return sourceField.startsWith(value);
            }
        } else { // if sourceField == null
            if(super.rule.Comparison_MVN__c == 'Equal_To_MVN') {
                return null == value;
            } else if(super.rule.Comparison_MVN__c == 'Not_Equal_To_MVN') {
                return null != value;
            }
        }
        return true;
    }
}