/*
* UtilitiesTestMVN
* @created By: Roman Lerman
* @created Date: 3/24/2016
* @description: This is the test class for the UtilitiesMVN class
*/
@isTest
private class UtilitiesTestMVN {
    @isTest static void testReadOnlyUser() {
        System.assertEquals(false, UtilitiesMVN.isReadOnlyUser);
    }

    @isTest static void testRandomString() {
        System.assert(UtilitiesMVN.getRandomString(5).length() == 5);
    }

    @isTest static void testPicklistValues() {
        Integer picklistValsListSize = Case.Status.getDescribe().getPicklistValues().size();
        Integer utilsPicklistValsListSize = UtilitiesMVN.getPicklistValues('Case', 'Status').size();
        System.assert(picklistValsListSize == utilsPicklistValsListSize);
    }

    @isTest static void coverage() {
        TestDataFactoryMVN.createSearchSettings();

        Map<String, Map<String,RecordType>> recordTypesByDeveloperNameByObject = UtilitiesMVN.recordTypesByDeveloperNameByObject;

        RecordType rtCase = [SELECT DeveloperName FROM RecordType WHERE SObjectType = 'Case' limit 1];
        System.assert(UtilitiesMVN.matchCaseRecordTypeIdToNameMVN(rtCase.Id, rtCase.DeveloperName));
        UtilitiesMVN.matchCaseRecordTypeIdToNameMVN(rtCase.Id, null);
        Map<String,RecordType> getRecordTypesForObject = UtilitiesMVN.getRecordTypesForObject('NotExisting');

        Map<String,RecordType> getPersonAccountRecordTypes = UtilitiesMVN.getPersonAccountRecordTypes();
        Map<Id, RecordType> getRecordTypesByIdForObject = UtilitiesMVN.getRecordTypesByIdForObject('Case');
        getRecordTypesByIdForObject = UtilitiesMVN.getRecordTypesByIdForObject('NotExisting');

        Map<Id, RecordTypeLocalization> recordTypeLocalizations = UtilitiesMVN.recordTypeLocalizations;
        Boolean isAnonymizing = UtilitiesMVN.isAnonymizing;
        Boolean knowledgeInstalled = UtilitiesMVN.knowledgeInstalled;
        DCRGlobalSettingMVN dcrGlobalSetting = UtilitiesMVN.dcrGlobalSetting;
        Enhanced_Account_Search_Settings_MVN__c accountSearchSettings = UtilitiesMVN.accountSearchSettings;
        Map<String, AccountSearchIntfMVN> accountSearchUtilities = UtilitiesMVN.accountSearchUtilities;
        String sanitizeString = UtilitiesMVN.sanitizeString('test');
        sanitizeString = UtilitiesMVN.sanitizeString(null);
        List<SObject> cloneObjects = UtilitiesMVN.cloneObjects('Case', 'Id = null');
        List<String> splitString = UtilitiesMVN.splitString('124-123','-');

        String returnAsString = UtilitiesMVN.returnAsString(null);
        returnAsString = UtilitiesMVN.returnAsString(new List<String>{'test'});
        returnAsString = UtilitiesMVN.returnAsString(1);

        Account account = new TestFactoryAccountMVN().create(new Map<String, String>{
            'Name' => 'test',
            'RecordTypeId' => TestFactoryAccountMVN.accountRecordTypesByName.get('Hospital_vod').Id
        });
        Case myCase = new Case(Account = account);
        System.assertEquals('test', UtilitiesMVN.getRelatedFieldValue('Account.Name', myCase));

        Integer getSearchLimit = UtilitiesMVN.getSearchLimit();
        Schema.DisplayType getDisplayType = UtilitiesMVN.getDisplayType('Account', 'Name');

        List<Messaging.SendEmailResult> emailAdministrators = UtilitiesMVN.emailAdministrators('Subject', 'body');
        Boolean hasBypassValidation = UtilitiesMVN.hasBypassValidation();
        Map<Schema.SObjectType, Map<Id, RecordType>> recordTypes = UtilitiesMVN.recordTypes;
    }
}