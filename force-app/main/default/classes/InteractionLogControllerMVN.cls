/*
*   InteractionLogControllerMVN
*   Created By:     Roman Lerman
*   Created Date:   1/18/2013
*   Description:    This class is used for saving the text in the interaction log as well as for creating requests
*   Modified:		March 23, 2016 - Thomas Hajcak (thomas@mavensconsulting.com)
*					Refactoring to remove legacy and unused code.
*/

global with sharing class InteractionLogControllerMVN {
    public String requestType {get; set;}

    @TestVisible public Boolean needsToBeSaved {get; private set;}
    public Boolean hasSaveError {get; private set;}
    public Boolean attemptedOneSave {get; private set;}
    public Boolean isSaving {get; private set;}
    public Boolean disableInteractionLog {get; private set;}
    public Boolean caseIsLocked {get; private set;}
    public String  lastSavedDateTime {get; private set;}

    public List<Case_Note_MVN__c> caseNotes {get; set;}
    public Case_Note_MVN__c currentNote {get; set;}

    public Event_MVN__c newEvent {get; set;}

    private Map<String,Id> recordTypeMap;
    private Map<String,Id> eventRecordTypeMap;
    private Case thisCase;
    private Id caseId;

    public InteractionLogControllerMVN(ApexPages.StandardController controller) {
        disableInteractionLog = false;
        caseIsLocked = false;
        needsToBeSaved = false;
        hasSaveError = false;
        attemptedOneSave = false;
        isSaving = false;
        lastSavedDateTime = '';
        requestType = 'RQ';

        eventRecordTypeMap = new Map<String,Id>();
        List<RecordType> allEventTypes = [select Id,DeveloperName from RecordType where SObjectType='Event_MVN__c'];
        for(RecordType r : allEventTypes) {
            eventRecordTypeMap.put(r.DeveloperName,r.Id);
        }

        caseId = controller.getRecord().Id;

        refreshCase();

        if (thisCase != null) {
            if (UtilitiesMVN.isReadOnlyUser || thisCase.isClosed) {
                caseIsLocked = true;
            }

            if (caseIsLocked || UtilitiesMVN.isReadOnlyUser) {
                disableInteractionLog = true;
            }
        }

        refreshNotes();
    }

    public PageReference createChildCaseFromInteractionLog(){
        newEvent = new Event_MVN__c();
        newEvent.Program_Member_MVN__c = thisCase.Program_Member_MVN__c;

        if (requestType == 'AE') {
            newEvent.RecordTypeId = eventRecordTypeMap.get('Adverse_Event_MVN');
        } else if (requestType == 'PQC') {
            newEvent.RecordTypeId = eventRecordTypeMap.get('Product_Quality_Complaint_MVN');
        } else {
            newEvent.RecordTypeId = eventRecordTypeMap.get('Missed_Dose_MVN');
        }

        try {
            insert newEvent;
        } catch (Exception e) {
            ApexPages.addMessages(e);
        }

        return null;
    }

    public PageReference setToTrue() {
        needsToBeSaved = true;
        return null;
    }

    public PageReference showStatusMessage() {
        if (needsToBeSaved) {
            isSaving = true;
        }

        return null;
    }

    /**
    * @description saves and closes all not empty notes when closing a Case
    * @param Id caseId of the case that is to be closed
    */
    webservice static void saveAndCloseNotes(Id caseId){
        List<Case_Note_MVN__c> caseNotes = [SELECT Id, Locked_MVN__c, Notes_MVN__c
                                              FROM Case_Note_MVN__c
                                             WHERE Case_MVN__c =: caseId
                                               AND Locked_MVN__c = false];
        List<Case_Note_MVN__c> notesToUpdate = new List<Case_Note_MVN__c>();
        List<Case_Note_MVN__c> notesToDelete = new List<Case_Note_MVN__c>();
        for(Case_Note_MVN__c caseNote : caseNotes){
            if(String.isNotBlank(caseNote.Notes_MVN__c)){
                caseNote.Locked_MVN__c = true;
                notesToUpdate.add(caseNote);
            } else {
                notesToDelete.add(caseNote);
            }
        }

        if(!notesToUpdate.isEmpty()){
            update caseNotes;
        }

        if(!notesToDelete.isEmpty()){
            delete notesToDelete;
        }
    }

    public PageReference saveInteractionNotes() {
        //if (needsToBeSaved) {
            attemptedOneSave = true;
            hasSaveError = false;

            try {
                upsert currentNote;
                needsToBeSaved = false;
                lastSavedDateTime = system.now().format();
            }
            catch (Exception e) {
                if (e.getMessage().contains('STRING_TOO_LONG')) {
                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Interaction_Log_Too_Long_Error_MVN));
                } else {
                    ApexPages.addMessages(e);
                }
                needsToBeSaved = true;
                hasSaveError = true;
            }
        //}

        isSaving = false;
        return null;
    }

    public PageReference closeNote(){
        if(String.isBlank(currentNote.Notes_MVN__c))
            return null;

        attemptedOneSave = true;
        hasSaveError = false;
        currentNote.Locked_MVN__c = true;
        try {
            upsert currentNote;
            needsToBeSaved = false;
            lastSavedDateTime = system.now().format();
            currentNote = new Case_Note_MVN__c();
            currentNote.Case_MVN__c = caseId;
            upsert currentNote;
        }
        catch (Exception e) {
            if (e.getMessage().contains('STRING_TOO_LONG')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Interaction_Log_Too_Long_Error_MVN));
            } else {
                ApexPages.addMessages(e);
            }
            needsToBeSaved = true;
            hasSaveError = true;
        }

        isSaving = false;

        refreshNotes();

        return null;
    }

    private Case refreshCase() {
        thisCase = [SELECT Id, Interaction_Notes_MVN__c, Address_MVN__c,Program_Member_MVN__c,
            RecordTypeId, RecordType.Name, AccountId, ContactId, isClosed, Origin, Referred_By_MVN__c, Subject, Description
            FROM Case WHERE Id = :caseId];
        return thisCase;
    }

    private void refreshNotes() {
        Map<Id, Case_Note_MVN__c> notesMap = new Map<Id, Case_Note_MVN__c>([select Id, Notes_MVN__c, Locked_MVN__c, CreatedById, CreatedBy.Name, LastModifiedDate from Case_Note_MVN__c where Case_MVN__c = :caseId order by LastModifiedDate desc]);

        Id currentNoteId = null;

        if(!caseIsLocked){
            for(Case_Note_MVN__c note : notesMap.values()){
                if(note.CreatedById == UserInfo.getUserId() && !note.Locked_MVN__c){
                    currentNoteId = note.Id;
                    notesMap.remove(note.Id);
                    break;
                }
            }

            if(currentNoteId == null){
                currentNote = new Case_Note_MVN__c();
                currentNote.Case_MVN__c = caseId;
            } else{
                currentNote = [select Id, Notes_MVN__c, Locked_MVN__c, CreatedById, CreatedBy.Name, LastModifiedDate from Case_Note_MVN__c where Id = :currentNoteId limit 1 for update];
            }
        }

        caseNotes = notesMap.values();
    }

    public Boolean getHasCaseNoteButtons() {
        Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();

        return Event_MVN__c.sObjectType.getDescribe().isCreateable() && (
            settings.Display_AE_Button_MVN__c ||
            settings.Display_PQC_Button_MVN__c ||
            settings.Display_MD_Button_MVN__c
        );
    }

}