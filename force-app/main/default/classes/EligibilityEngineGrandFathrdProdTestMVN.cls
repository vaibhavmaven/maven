/**
 * @author      Mavens
 * @date        November 2018
 * @group       EligibilityEngine
 * @description tests for EligibilityEngineGrandFathrdProdMVN
 */
@isTest
private class EligibilityEngineGrandFathrdProdTestMVN
{
    private static User caseManager;
    private static Account physician;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;
    private static Address_vod__c primaryAddress;
    private static Product_MVN__c productFamily;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();

        System.runAs(caseManager) {
            program = createProgram();
            physician = createPhysician();
            primaryAddress = createPrimaryAddress(physician);
            programMember = createProgramMember();
            application = createApplication();
            programMember.Active_Application_MVN__c = application.Id;
            update programMember;
            createGrandfatheredProductRule();
            productFamily = createGrandfatheredProductFamily();
        }
    }

    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        return programFactory.create(
            new Map<String,Object>{
                'Name' => 'A Program',
                'Program_Id_MVN__c' => 'Knipper_PAP_MVN',
                'Active_MVN__c' => true,
                'Job_No_MVN__c' => '12345',
                'Grandfathered_Product_Grace_Period_MVN__c' => 10
            }
        );
    }

    static Account createPhysician() {
        TestFactoryAccountMVN accountTestFactory = new TestFactoryAccountMVN();
        Account physician = accountTestFactory.create(new Map<String, Object> {
            'RecordTypeId' => TestFactoryAccountMVN.accountRecordTypesByName.get('Professional_vod').Id,
            'FirstName' => 'Joel',
            'LastName' => 'Atwood',
            'Credentials_vod__c' => 'MD',
            'DEA_MVN__c' => 'DEA123',
            'SLN_MVN__c' => 'SLN123'
        });
        return physician;
    }

    static Address_vod__c createPrimaryAddress(Account physician) {
        TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();
        return addressFactory.createAddress(physician, new Map<String,Object>{
            'Name' => 'Physician Address Line 1',
            'Address_line_2_vod__c' => 'Physician Address Line 2',
            'Shipping_vod__c' => true,
            'Primary_vod__c' => true,
            'City_vod__c' => 'Physician City',
            'Zip_vod__c' => '11111',
            'State_vod__c' => 'DE'
        });
    }

    static Program_Member_MVN__c createProgramMember() {
        Program_Member_MVN__c programMemberTest =
            TestDataFactoryMVN.createProgramMember(program, TestDataFactoryMVN.createMember());
        programMemberTest.Physician_MVN__c = physician.Id;
        update programMemberTest;
        return programMemberTest;
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        return testApplication;
    }

    static List<Eligibility_Engine_Rule_MVN__c> createGrandfatheredProductRule() {
        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Test Grandfathered Product Rule',
            Type_MVN__c = 'Grandfathered_Product_Validation_MVN',
            Result_Message_MVN__c ='No recent orders within grace period',
            Program_MVN__c = program.Id,
            Active_MVN__c = true
        );
        insert expectedRule;
        return new List<Eligibility_Engine_Rule_MVN__c> {expectedRule};
    }

    static Product_MVN__c createGrandfatheredProductFamily() {
        Product_MVN__c productFamily = new Product_MVN__c(
            Program_MVN__c = program.Id,
            Name = 'Grandfathered Product',
            Product_Status_MVN__c = 'Grandfathered'
        );
        insert productFamily;
        return productFamily;
    }

    static Prescription_MVN__c createPrescription(Id applicationId) {
        Prescription_MVN__c prescription = new Prescription_MVN__c (
            Application_MVN__c = applicationId,
            Program_Member_MVN__c = programMember.Id,
            Product_MVN__c = productFamily.Id,
            Refills_MVN__c = 0,
            Prescription_doc_JKC__c = createDocument(programMember.Id)
        );
        insert prescription;
        return prescription;
    }
    
    static Id createDocument(Id programMemberId){
        Document_MVN__c document = new Document_MVN__c  (
            Title_MVN__c = 'Test Document',
            Type_MVN__c = 'Eligibility',
            Program_Member_MVN__c = programMemberId
        );
        insert document;
        return document.id;
    }
    

    static void createPriorOrders(Boolean inGracePeriod) {
        Application_MVN__c oldApplication = createApplication();
        List<Order_Scheduler_MVN__c> schedulers = new List<Order_Scheduler_MVN__c>();

        Integer startDatePriorDays = -5 * (inGracePeriod ? 1 : 5);
        schedulers.add(
            new Order_Scheduler_MVN__c (
                Program_Member_MVN__c = programMember.Id,
                Application_MVN__c = oldApplication.Id,
                Prescription_MVN__c = createPrescription(oldApplication.Id).Id,
                Order_Start_Date_MVN__c = Date.today()
            )
        );
        //create second scheduler that is older just to test multiple older orders
        schedulers.add(schedulers[0].clone());
        schedulers[1].Order_Start_Date_MVN__c = Date.today();

        insert schedulers;
        schedulers[0].Order_Start_Date_MVN__c = Date.today().addDays(startDatePriorDays);
        schedulers[1].Order_Start_Date_MVN__c = Date.today().addDays(startDatePriorDays * 2);
        Test.setCreatedDate(schedulers[0].Id, schedulers[0].Order_Start_Date_MVN__c);
        Test.setCreatedDate(schedulers[1].Id, schedulers[1].Order_Start_Date_MVN__c);
        update schedulers;

        List<Order_MVN__c> orders = new List<Order_MVN__c>();
        for (Order_Scheduler_MVN__c scheduler : schedulers) {
            orders.add (
                new Order_MVN__c (
                    Order_Scheduler_MVN__c = scheduler.Id,
                    Order_Date_MVN__c = scheduler.Order_Start_Date_MVN__c,
                    Status_MVN__c = 'Shipped'
                )
            );
        }
        insert orders;
    }

    @isTest
    static void itShouldDenyApplicationWithPrescriptionHavingGrandFatheredProduct() {
        System.runAs(caseManager) {
            createPrescription(application.Id);

            Test.startTest();
             List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Grandfathered Product Rule', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
            System.assertEquals(
                'No recent orders within grace period',
                actualResults[0].Result_Message_MVN__c
            );
        }
    }

    @isTest
    static void itShouldAllowApplicationWithPrescriptionNotHavingGrandFatheredProduct() {
        productFamily.Product_Status_MVN__c = 'Active';
        update productFamily;

        System.runAs(caseManager) {
            createPrescription(application.Id);

            Test.startTest();
             List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Grandfathered Product Rule', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    static void itShouldDenyApplicationWhenPreviousOrdersOutsideGracePeriod() {
        System.runAs(caseManager) {
            createPriorOrders(false);
            createPrescription(application.Id);

            Test.startTest();
             List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Grandfathered Product Rule', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
            System.assertEquals(
                'No recent orders within grace period',
                actualResults[0].Result_Message_MVN__c
            );
        }
    }

    @isTest
    static void itShouldAllowApplicationWhenPreviousOrderInsideGracePeriod() {
        System.runAs(caseManager) {
            createPriorOrders(true);
            createPrescription(application.Id);

            Test.startTest();
             List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Grandfathered Product Rule', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    static void itShouldSkipSchedulerComparisonPriorOrderProductsDoNotMatch() {
        System.runAs(caseManager) {
            createPriorOrders(false);
            Product_MVN__c newProductFamily = new Product_MVN__c(
                Program_MVN__c = program.Id,
                Name = 'Other Product',
                Product_Status_MVN__c = 'Active'
            );
            insert newProductFamily;
            List<Prescription_MVN__c> oldPrescriptions = [
                SELECT
                    Id,
                    Product_MVN__c
                FROM
                    Prescription_MVN__c
            ];
            for (Prescription_MVN__c script : oldPrescriptions) {
                script.Product_MVN__c = newProductFamily.Id;
            }
            update oldPrescriptions;

            createPrescription(application.Id);

            Test.startTest();
             List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Grandfathered Product Rule', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
            System.assertEquals(
                'No recent orders within grace period',
                actualResults[0].Result_Message_MVN__c
            );
        }
    }
   
}