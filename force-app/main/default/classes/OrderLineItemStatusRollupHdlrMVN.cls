/**
 * @author Mavens
 * @description when a record is inserted and application is not filled out, fill it with the active
 *              application on the related program member
 * @group Application
 */
public with sharing class OrderLineItemStatusRollupHdlrMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
     * @description handle
     */
    public void handle() {
        Map<Id, Order_MVN__c> ordersToUpdate = new Map<Id, Order_MVN__c>();
        for(Order_Line_Item_MVN__c thisOrderLineItem : (List<Order_Line_Item_MVN__c>) trigger.new) {
            if (!ordersToUpdate.containsKey(thisOrderLineItem.Order_MVN__c)) {
                ordersToUpdate.put(
                    thisOrderLineItem.Order_MVN__c, 
                    new Order_MVN__c(
                        Id = thisOrderLineItem.Order_MVN__c, 
                        Status_MVN__c = thisOrderLineItem.Status_MVN__c
                    )
                );
            }
        }
        update ordersToUpdate.values();
    }
}