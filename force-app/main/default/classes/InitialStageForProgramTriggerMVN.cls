/**
 * @author Mavens
 * @description Create the initial stage for the program
 * @group Program
 */

public with sharing class InitialStageForProgramTriggerMVN implements TriggerHandlerMVN.HandlerInterface {
    private static Boolean hasExecuted = false;

    public void handle() {
        if(hasExecuted) {
            return;
        }
        hasExecuted = true;

        RecordType rtStage = [SELECT Id FROM RecordType WHERE DeveloperName = 'Parent_Stage_MVN' AND SobjectType = 'Program_Stage_MVN__c'];

        List<Program_Stage_MVN__c> programStages = new List<Program_Stage_MVN__c>();

        for(Program_MVN__c program : (List<Program_MVN__c>)trigger.new) {
            if(program.Cloned_MVN__c) {
                continue;
            }

            programStages.add(new Program_Stage_MVN__c(
                RecordTypeId = rtStage.Id,
                Name = 'New Stage',
                Program_MVN__c = program.Id,
                Stage_Sequence_Number_MVN__c = 1
            ));
        }

        if(!programStages.isEmpty()) {
            insert programStages;
        }
    }
}