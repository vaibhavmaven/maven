/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description This is an abstract class to do all the things every rule engine needs to do
 *              extend this and implement the isRuleFired() or evaluateSingleRecord() methods
                to implement a new rule
 */
public abstract class EligibilityEngineRuleHdlrMVN implements EligibilityEngineRuleIntfMVN {
    /**
     * rule to run
     */
    protected Eligibility_Engine_Rule_MVN__c rule;

    /**
     * data requested by all rules, including application
     */
    protected Map<String, Object> requestedData;

    /**
     * application to run the rule against
     */
    protected Application_MVN__c application;

    /**
     * Eligibility Engine result list returned after running the rule
     */
    protected List<Eligibility_Engine_Result_MVN__c> resultList;

    /**
     * Run rule using requested data
     * @param   Eligibility_Engine_Rule_MVN__c  rule to run
     * @param   requestedData      map with data by rule key if defined or 'application'
     * @return  Eligibility Engine Result list
     */
    public List<Eligibility_Engine_Result_MVN__c> run(Eligibility_Engine_Rule_MVN__c rule,
                                                Map<String, Object> requestedData) {
        this.rule = rule;
        this.requestedData = requestedData;
        this.application = (Application_MVN__c) requestedData.get('application');
        if (!this.rule.Return_Multiple_Results_MVN__c) {
            this.resultList = this.createEligibilityEngineResult();
        }
        return this.executeRule();
    }

    /**
     * execute rule and return result record
     * @return  Eligibility Engine Result list
     */
    private List<Eligibility_Engine_Result_MVN__c> executeRule() {
        if(this.isRuleFired()) {
            // If The result is defined in the rule, we need to set it here
            if (this.rule.Result_MVN__c != null) {
                this.resultList.get(0).Result_MVN__c = this.rule.Result_MVN__c;
                this.resultList.get(0).Result_Message_MVN__c = this.rule.Result_Message_MVN__c;
            }
        }
        return this.resultList;
    }

    /**
     * create result record - use default values (Result_MVN__c = Passed)
     * @return  Eligibility_Engine_Result_MVN__c    result
     */
    private List<Eligibility_Engine_Result_MVN__c> createEligibilityEngineResult() {
        Eligibility_Engine_Result_MVN__c result =
            (Eligibility_Engine_Result_MVN__c) Eligibility_Engine_Result_MVN__c.sObjectType
                                                                               .newSObject(null, true);
        result.Name = this.rule.Name;
        result.Cannot_be_overriden_MVN__c = this.rule.Cannot_be_overriden_MVN__c;
        List<Eligibility_Engine_Result_MVN__c> resultList = new List<Eligibility_Engine_Result_MVN__c>{result};
        return resultList;
    }

    /**
     * checks if rule is fired
     * @return  Boolean     flag if rule is fired
     */
    private virtual Boolean isRuleFired() {
        String sourceObjectApiName = this.rule.Source_Object_API_Name_MVN__c;

        if (sourceObjectApiName == 'Application_MVN__c') {
            return this.evaluateSingleRecord(application);
        } else if (String.isNotBlank(sourceObjectApiName)) {
            return this.evaluateChildren();
        } else {
            return true;
        }
    }

    /**
     * the actual implementation of a specific rule that dictates how the rule is applied
     * @return  Boolean     flag indicating rule is fired
     */
    protected virtual Boolean evaluateSingleRecord(SObject obj) {
        return true;
    }

    /**
     * handles the parsing of child records and passing into the implementation and
     * accumulating the results based on AND or OR comparison
     * @return  Boolean     flag indicating rule is fired
     */
    private Boolean evaluateChildren() {
        List<SObject> children = application.getSObjects(
            this.rule.Source_Object_API_Name_MVN__c
        );

        if (children == null || children.isEmpty()) {
            return true;
        }

        String evalType = this.rule.Child_Evaluation_Type_MVN__c;

        // AND starts eval as true, OR starts as false
        Boolean evaluation = evalType == 'AND';

        for (SObject child : children) {
            if (evalType == 'AND') {
                evaluation = evaluation && evaluateSingleRecord(child);
            } else {
                evaluation = evaluation || evaluateSingleRecord(child);
            }
        }

        return evaluation;
     }

    /**
     * Get extra fields to be queried against Application - needs to be overriden
     * @return  Set<String>     List of fields
     */
    public virtual Map<String, Set<String>> getExtraFieldsToQuery() {
        return new Map<String, Set<String>>();
    }

    /**
     * get source field, going up the relationship chain if necessary, can be used via super in
     * child implementations
     * @return  Object      object to return
     */
    protected Object getSource(SObject record) {
        return SObjectDynamicUtilMVN.getFieldValue(record, this.rule.Source_Field_API_Name_MVN__c);
    }

    /**
     * Get additional query configuration if defined
     * @return  EligibilityEngineQueryConfigMVN
     */
    public virtual EligibilityEngineQueryConfigMVN getAdditionalDataConfig() {
        return new EligibilityEngineQueryConfigMVN();
    }
}