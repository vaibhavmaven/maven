public class SowPopUpController {
    
    public String selectedOption{get;set;}
    public Id pgmId{get;set;}
    public String responseStatus{get;set;}
    public SowPopUpController(ApexPages.StandardController controller){
        pgmId =   controller.getId();
    }
    
    public List<SelectOption> getOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        Options.add(new SelectOption('','-None-'));
        Options.add(new SelectOption('SMS','SMS'));
        Options.add(new SelectOption('Autodialer','Autodialer'));
        Options.add(new SelectOption('Fax','Fax'));
        Options.add(new SelectOption('Email','Email'));
        
        return Options;
    }
    public PageReference save(){
        Program_Member_MVN__c pgmDetail =    [SELECT Id,
                                              Name,
                                              Program_Name_MVN__c,
                                              Program_Id_MVN__c,
                                              Active_Application_Status_MVN__c,
                                              Notified_HCP_JKC__c,
                                              Program_MVN__r.SMS_JKC__c,
                                              Program_MVN__r.AutoDialer_JKC__c,
                                              Program_MVN__r.Email_JKC__c,
                                              Program_MVN__r.Mail_JKC__c,
                                              Program_MVN__r.Fax_JKC__c,
                                              Patient_Status_MVN__c,                
                                              Patient_Status_Date_JKC__c,
                                              Active_Application_Expiry_Date_JKC__c
                                              from Program_Member_MVN__c where Id=:pgmId];
        system.debug('***** '+selectedOption);
        
        if(selectedOption=='None'){
             responseStatus = PAP_SendNotification.sendNotification_Patient(String.valueOf(pgmId), False, True);
       
        }
        else{
        responseStatus = PAP_SendNotificationTesting.sendNotification_Patient(String.valueOf(pgmId), False, True,selectedOption);
        system.debug('******** '+responseStatus);
        }
        return null;
    }
}