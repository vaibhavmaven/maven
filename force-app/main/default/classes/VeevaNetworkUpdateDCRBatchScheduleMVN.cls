/**
 * VeevaNetworkUpdateDCRBatchScheduleMVN
 * @created By: Pablo Roldan
 * @created Date: 26th September 2017
 * @edited by: Pablo Roldan
 * @edited date: November 2017
 * @description: connects to an external Veeva Network to update DCR and DCR Lines
 *				wait for response to update Accounts linked.
 *
 *				Due to Veeva Network change request structure, we need to split our DCR into 3:
 *				1. Entity: Account_MVN DCR
 *				2. Entity: Account_MVN DCR, Addresses_vod: Address_MVN DCR
 *				3. Entity: Account_MVN DCR, Parent_Hcos_vod: Affiliation_MVN DCR
 *
 *				Every change request sent to Veeva Network must have a Account_MVN DCR as entity.
 *				Every DCR must have the required fields from Veeva Network as DCR Lines at least. It could have more DCR Lines.
 *				DCRs for Address_MVN must have Account_MVN as entity. However, Account_MVN DCR won't have more than required fields in DCR Lines.
 *				Affiliation_MVN DCRs could be included on any Change request as soon as it has a Account_MVN DCR.
 *
 * @edit description: Adapted to PS.
 */
public class VeevaNetworkUpdateDCRBatchScheduleMVN implements Database.Batchable<SObject>,
                                                       Database.AllowsCallouts,
                                                       Database.Stateful,
                                                       Schedulable {

    Set<String> errors;

    /**
    * EXECUTE SCHEDULE
    * @description: Call batch when the schedule runs
    */
    public void execute(SchedulableContext sc) {
        VeevaNetworkUpdateDCRBatchScheduleMVN vnUpdateDCRBatch = new VeevaNetworkUpdateDCRBatchScheduleMVN();
        // It must be under 100 due to the Call out limit in Salesforce.
        // Also we need to consider the call to get the session Id
        database.executebatch(vnUpdateDCRBatch, 49);
    }

    /**
    * START BATCH
    * @description: It executes a query to get any DCR with status Pending and any DCR Line linked to it
    */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        errors = new Set<String>();

        return Database.getQueryLocator([SELECT Id, Record_External_ID_MVN__c, DCR_External_ID_MVN__c, Status_MVN__c, Country_MVN__c,
                                                Notes_MVN__c, Account_MVN__c, Account_MVN__r.RecordType.DeveloperName, Address_MVN__c,
                                                RecordType.DeveloperName, Parent_Account_MVN__c, Parent_Account_MVN__r.RecordType.DeveloperName,
                                                Account_MVN__r.IsPersonAccount, License_MVN__c,
                                                   (SELECT Id, Field_API_Name_MVN__c, New_Value_MVN__c, Old_Value_MVN__c FROM Data_Change_Request_Lines_MVN__r)
                                           FROM Data_Change_Request_MVN__c
                                          WHERE Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_PENDING
                                              AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount]);
    }

    /**
    * EXECUTE BATCH
    * @description: It will execute a Batch job per Remote Veeva Network org.
    *                Will call these remote veeva network orgs to pull any Medical Inquiry Submitted which wasn't converted to Inbound Form yet.
    *                It will deserialize this answer and transform it from Medical Inqury to Inbound Form using the mapping stored in a custom metadata.
    *                It will store all these Inbound Forms into the Database.
    */
    public void execute(Database.BatchableContext BC, List<SObject> scope) {
        if(!scope.isEmpty()){
            List<Data_Change_Request_MVN__c> dcrsToUpdate = new List<Data_Change_Request_MVN__c>();
            List<Data_Change_Request_MVN__c> dcrs = (List<Data_Change_Request_MVN__c>) scope;

            List<VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN> networkChangeRequestRecords = VeevaNetworkConnectionUtilMVN.getNetworkDCRRequestsBody(dcrs,
                                                                                                                                                                VeevaNetworkConnectionUtilMVN.DCR_PENDING);
            for(VeevaNetworkConnectionUtilMVN.NetworkDCRRequestBodyMVN networkChangeRequestRecord : networkChangeRequestRecords) {
                VeevaNetworkConnectionUtilMVN.NetworkRequestMVN networkRequest = new VeevaNetworkConnectionUtilMVN.NetworkRequestMVN(networkChangeRequestRecord);
                VeevaNetworkConnectionUtilMVN.NetworkResultMVN createChangeRequestRslt = VeevaNetworkConnectionUtilMVN.createChangeRequest(networkRequest);

                dcrsToUpdate.addAll(VeevaNetworkConnectionUtilMVN.getDCRsToUpdate(networkRequest, createChangeRequestRslt));
            }

            // Store Change Request id in DCR and change DCR status Pending to Submitted
            VeevaNetworkConnectionUtilMVN.DcrUpdateResultMVN dcrUpdateResult = VeevaNetworkConnectionUtilMVN.updateDCRsDB(dcrsToUpdate, VeevaNetworkConnectionUtilMVN.DCR_PENDING);

            if(dcrUpdateResult != null && dcrUpdateResult.errorsByDcrId != null && !dcrUpdateResult.errorsByDcrId.keySet().isEmpty()) {
                for(Set<String> dcrErrors : dcrUpdateResult.errorsByDcrId.values()) {
                    errors.addAll(dcrErrors);
                }
            }
        }
    }

    /**
    * FINISH BATCH
    * @description: log any error which wasn't store in a record
    */
    public void finish(Database.BatchableContext BC) {
        if(errors != null && !errors.isEmpty()) {
            System.debug('### Pushing DCRs Errors: \n' + String.join(new List<String>(errors), ',\n'));
        }
    }
}