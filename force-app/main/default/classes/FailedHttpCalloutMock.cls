@isTest   // making this class visible to testing framework. 
public class FailedHttpCalloutMock implements HttpCalloutMock{
    
    public HttpResponse respond(HttpRequest httpReq)
    {
        HttpResponse responseObj = new HttpResponse();
        responseObj.setHeader('Content-Type', 'application/json'); 
        responseObj.setStatusCode(404); 
        responseObj.setBody(''); 
        return responseObj; 
    }

}