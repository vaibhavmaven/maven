/**
 * @author Mavens
 * @description locks down application with related records
 * @group Application
 */
public with sharing class ApplicationLockMVN {
    /**
     * @description application with its related records
     */
    private Application_MVN__c application;

    /**
     * @description locks down application and all related records
     * @parameter applicationId
     */
    public ApplicationLockMVN(Id applicationId) {
        this.application = this.getApplicationWithAllRelatedRecordsToBeLocked(applicationId);
    }

    /**
     * @description locks down application and all related records given
     * @parameter applicationId
     */
    public ApplicationLockMVN(Application_MVN__c application) {
        this.application = application;
    }

    /**
     * @description actual locking down
     */
    public void lock() {
        Set<Id> recordsToLock = new Set<Id>();
        recordsToLock.add(this.application.Id);

       for(Authorization_and_Consent_MVN__c thisAuthAndConsent : this.application.Authorization_Consent_MVN__r) {
           recordsToLock.add(thisAuthAndConsent.Id);
       }
        for(Benefits_Coverage_MVN__c thisBenefitsAndCoverage : this.application.Benefits_Coverage_MVN__r) {
            recordsToLock.add(thisBenefitsAndCoverage.Id);
        }
        for(Income_MVN__c thisIncome : this.application.Income_MVN__r) {
            recordsToLock.add(thisIncome.Id);
        }
        for(Prescription_MVN__c thisPrescription : this.application.Prescriptions_MVN__r) {
            recordsToLock.add(thisPrescription.Id);
        }
        for(Id recordToLock : recordsToLock) {
            this.submitForApproval(recordToLock);
        }
    }

    /**
     * @description submit record for approval process (to lock)
     * @parameter recordId
     */
    private void submitForApproval(Id recordId) {
        Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
        approvalRequest.setComments('Lock');
        approvalRequest.setObjectId(recordId);
        Approval.ProcessResult approvalResult = Approval.process(approvalRequest);
    }

    /**
     * @description get Application with all its related records to lock
     * @parameter applicationId
     */
    private Application_MVN__c getApplicationWithAllRelatedRecordsToBeLocked(Id applicationId) {
        return [
            SELECT
                Id,
                (
                    SELECT
                        Id
                    FROM
                        Authorization_Consent_MVN__r
                ),
                (
                    SELECT
                        Id
                    FROM
                        Benefits_Coverage_MVN__r
                ),
                (
                    SELECT
                        Id
                    FROM
                        Income_MVN__r
                ),
                (
                    SELECT
                        Id
                    FROM
                        Prescriptions_MVN__r
                )
            FROM
                Application_MVN__c
            WHERE
                Id = :applicationId
        ];
    }
}