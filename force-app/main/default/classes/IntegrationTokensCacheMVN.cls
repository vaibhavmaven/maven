/**
 * @author Mavens
 * @group Network API
 * @description Class used to use Platform Cache Partition called IntegrationTokensMVN
 */
public class IntegrationTokensCacheMVN implements CacheManagerMVN {

    /**
    * @description Cache org initialization which will be used to get data from it.
    */
    private static Cache.OrgPartition integrationTokens = Cache.Org.getPartition('local.IntegrationTokensMVN');

    /**
    * @description Network session token variable name in cache
    */
    public static final String NETWORK_SESSION_TOKEN = 'networkSessionToken';

    /**
    * @description Current profile name variable name in cache
    */
    public static final String CURRENT_PROFILE_NAME = 'currentProfileName';

    /**
    * @description Account record types variable name in cache
    */
    public static final String ACCOUNT_RECORD_TYPES = 'accountRecordTypes';

    /**
    * @description Contact Information record types variable name in cache
    */
    public static final String CI_RECORD_TYPES = 'ciRecordTypes';

    /**
    * @description Property which decides if Cache is enabled to use (it must be false in unit tests)
    */
    @TestVisible
    private static Boolean cacheEnabled = true;

    /**
    * @description Method used to change the value of the cache enabled property
    * @result   Boolean cache enabled property after change it
    * @example
    * * Boolean cacheEnabled = IntegrationTokensCacheMVN.toggleEnabled();
    */
    public static Boolean toggleEnabled() {
        cacheEnabled = !cacheEnabled;
        return cacheEnabled;
    }

    /**
    * @description Get from cache the value from the token key name passed as parameter
    * @param    key String token key variable name in the cache
    * @result       Object value stored in the cache using that token passed
    * @example
    * * Object cacheValue = IntegrationTokensCacheMVN.get(myTokenKey);
    */
    public static Object get(String key) {
        Object value;

        if (cacheEnabled) {
            value = integrationTokens.get(key);
        }

        return value;
    }

    /**
    * @description Stores the value passed as parameter into cache using the token key passed as
    *       parameter too. This value will be stored during a period of time defined in the parameter
    *       passed.
    * @param    key     String token cache key
    * @param    value   Object value to store
    * @param    ttl     Time in milliseconds which will be stored
    * @result           Value stored in the cache using that token passed
    * @example
    * * Object cacheValue = IntegrationTokensCacheMVN.put(myTokenKey, myValue, timeLeft);
    */
    public static void put(String key, Object value, Integer ttl) {
        if (cacheEnabled) {
            integrationTokens.put(key, value, ttl);
        }
    }

    /**
    * @description Remove everything in cache for the token key passed
    * @param    key String token cache key
    * @result       Boolean which shows if the value was removed
    * @example
    * * Boolean valueRemoved = IntegrationTokensCacheMVN.remove(myTokenKey);
    */
    public static Boolean remove(String key) {
        Boolean removed = false;

        if (cacheEnabled) {
            removed = integrationTokens.remove(key);
        }

        return removed;
    }
}