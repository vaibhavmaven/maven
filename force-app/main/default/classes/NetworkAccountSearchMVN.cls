/*
 * @author Mavens
 * @description Simple class to abstract the query logic in Veeva Network:
 *  ```
 *      NetworkAccountSearchMVN nas = new NetworkAccountSearchMVN(true, queryLimit);
 *      Map<String, AccountSearchRsltMVN> resultMap = nas.doSearch(searchPersonAccount, searchPersonAcctAddress);
 *  ```
 *
 */

public with sharing class NetworkAccountSearchMVN {

    String country;
    Boolean isPersonSearch;
    AccountSearchRqstMVN searchRequest;
    String recordTypeText = UtilitiesMVN.ALL_PICKLIST_VALUE;

    private AccountFieldSettingsMVN accountSearchFields {
        get {
            return getAccountFieldSettingsBy('Search_Field_Order_MVN__c');
        }
    }

    private AccountFieldSettingsMVN searchResultFields {
        get {
            return getAccountFieldSettingsBy('Search_Results_Order_MVN__c');
        }
    }

    public NetworkAccountSearchMVN(Boolean isPersonSearch, Integer searchLimit, Address_vod__c searchAddress) {
        country = searchAddress.Country_vod__c;
        searchRequest = new AccountSearchRqstMVN(accountSearchFields, searchResultFields, country);
        searchRequest.searchLimit = searchLimit;
        searchRequest.isPersonSearch = isPersonSearch;
        this.isPersonSearch = isPersonSearch;
    }

    private AccountFieldSettingsMVN getAccountFieldSettingsBy(String fieldToOrderBy) {
        String recordTypeDeveloperName;

        if (recordTypeText != UtilitiesMVN.ALL_PICKLIST_VALUE) {
            recordTypeDeveloperName = UtilitiesMVN.getRecordTypesByIdForObject('Account')
                                               .get(recordTypeText)
                                               .DeveloperName;
        }

        return new AccountFieldSettingsMVN(country,
                                        recordTypeDeveloperName,
                                        fieldToOrderBy,
                                        isPersonSearch);
    }

    private Map<String, AccountSearchRsltMVN> buildMergedSearchResults() {
        Map<String, AccountSearchRsltMVN> combinedMap = new Map<String, AccountSearchRsltMVN>();
        searchRequest.recordType = UtilitiesMVN.sanitizeString(recordTypeText);
        searchRequest.recordType = recordTypeText == null ? '' : recordTypeText;

        AccountTypeSettingsMVN acctTypeSettings = new AccountTypeSettingsMVN(country);

        try {
            searchRequest.account.RecordTypeId = recordTypeText;
        } catch (Exception ex) {
            System.debug('### Bad record Type Id: ' + recordTypeText);
            searchRequest.account.RecordTypeId = null;
        }
        searchRequest.acctTypeSettings = acctTypeSettings;

        for (String utilityClass : UtilitiesMVN.accountSearchUtilities.keySet()) {
            if (combinedMap.size() > UtilitiesMVN.getsearchLimit()) {
                break;
            }

            AccountSearchIntfMVN utilityInstance = UtilitiesMVN.accountSearchUtilities.get(utilityClass);

            List<AccountSearchRsltMVN> utilityResults = new List<AccountSearchRsltMVN>();
            utilityResults = utilityInstance.search(searchRequest);

            for (AccountSearchRsltMVN asr: utilityResults) {
                asr.setAccountTypeSettings(acctTypeSettings);

                String accountIdentifier = asr.getAccountIdentifier();

                if (!combinedMap.containsKey(accountIdentifier)) {
                    combinedMap.put(accountIdentifier, asr);
                }
            }
            if (combinedMap.size() >= UtilitiesMVN.getSearchLimit()) {
                break;
            }
        }

        return combinedMap;
    }

    public Map<String, AccountSearchRsltMVN> doSearch(Account account, Address_vod__c address) {
        return doSearch(account, address, null);
    }

    public Map<String, AccountSearchRsltMVN> doSearch(Account account, Address_vod__c address, Contact_Information_MVN__c contactInfo) {
        Map<String, AccountSearchRsltMVN> searchRslts = new Map<String, AccountSearchRsltMVN>();

        try{
            searchRequest.resetFields(accountSearchFields, searchResultFields);

            searchRequest.noSearchFieldsPopulated(accountSearchFields);

            searchRequest.account = account;

            searchRequest.address = address;

            searchRequest.addcontactInfo(contactInfo);

            searchRslts = buildMergedSearchResults();
        } catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            System.debug('### Stack Trace: ' + ex.getStackTraceString());
        }

        return searchRslts;
    }
}