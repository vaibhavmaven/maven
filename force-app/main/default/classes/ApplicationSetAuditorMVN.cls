/**
 * @author Mavens
 * @description when an applications audit fields are updated, write the running user into the Auditor_MVN__c field,
 *              only if the Audit Result is NOT "System Cancelled".
 *              If the audit result changes to the value defined in the PS custom setting 
 *              Application_Audit_Result_Pass_Value_MVN__c then the automated audit action is run
 *              with the Action Engine.
 * @group Application
 */
public with sharing class ApplicationSetAuditorMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
     * @description handle
     */
    public void handle() {
        for (Id thisApplicationId : trigger.newMap.keySet()) {
            if (this.auditFieldHasChanged(thisApplicationId)) {
                Application_MVN__c thisApplication = (Application_MVN__c) trigger.newMap.get(thisApplicationId);
                if (thisApplication.Audit_Result_MVN__c != Label.System_Cancelled_MVN) {
                    thisApplication.Auditor_MVN__c = UserInfo.getUserId();
                }
                  if (thisApplication.Audit_Result_MVN__c == 
                    Patient_Service_Settings_MVN__c.getInstance().Application_Audit_Result_Pass_Value_MVN__c) {
                    (new ActionEngineMVN(thisApplication.Program_Member_MVN__c)).run(ActionEngineMVN.AUDITACTION);
                }
            }
        }
    }

    /**
     * @description returns true if one of the two audit fields have been changed
     * @param Id thisApplicationId
     * @return Boolean auditFieldHasChanged
     */
    private Boolean auditFieldHasChanged(Id thisApplicationId) {
        Application_MVN__c oldApplication = (Application_MVN__c) trigger.oldMap.get(thisApplicationId);
        Application_MVN__c newApplication = (Application_MVN__c) trigger.newMap.get(thisApplicationId);
        return ((oldApplication.Audit_Result_MVN__c != newApplication.Audit_Result_MVN__c) || 
            (oldApplication.Audit_Failed_Reason_MVN__c != newApplication.Audit_Failed_Reason_MVN__c));
    }
}