/*
* TestDataFactoryMVN
* Created By:    Jen Wyher
* Created Date:  20/5/2015
* Updated:      February 10, 2016 by Vincent Reeder
* Description:   This is the data factory used to generate data by the test classes
*/

public class TestDataFactoryMVN {
    public static Patient_Service_Settings_MVN__c psSettings;
    public static Enhanced_Account_Search_Settings_MVN__c searchSettings;
    public static Assure_Integration_Settings_MVN__c assureSettings;

    public static Account createPhysician() {
        Account physician = new Account();
        physician.FirstName = 'Dr';
        physician.LastName = 'Who';
        physician.Middle_vod__c   = 'Little';
        physician.RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Professional_vod'][0].Id;
        physician.Phone = '5555555555';
        insert physician;
        return physician;
    }

    public static Account createMember() {
        Account member = new Account();
        member.FirstName = 'Johnny';
        member.LastName = 'Member';
        member.RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Patient_MVN'][0].Id;
        insert member;
        return member;
    }

    public static void createPhoneNumber(Account acct, String phoneNumber) {
        Contact_Information_MVN__c contactInfo = generatePhoneNumber(acct, phoneNumber);
        insert contactInfo;
    }

    public static void createPhoneNumberPrimary(Account acct, String phoneNumber) {
        Contact_Information_MVN__c contactInfo = generatePhoneNumber(acct, phoneNumber);
        contactInfo.Primary_MVN__c = true;
        insert contactInfo;
    }

    public static void createFaxNumber(Account acct, String phoneNumber) {
        Contact_Information_MVN__c contactInfo = generatePhoneNumber(acct, phoneNumber);
        contactInfo.Label_MVN__c = 'Fax';
        insert contactInfo;
    }

    public static void createFaxNumberPrimary(Account acct, String phoneNumber) {
        Contact_Information_MVN__c contactInfo = generatePhoneNumber(acct, phoneNumber);
        contactInfo.Primary_MVN__c = true;
        contactInfo.Label_MVN__c = 'Fax';
        insert contactInfo;
    }

    public static Contact_Information_MVN__c generatePhoneNumber(Account acct, String phoneNumber) {
        Contact_Information_MVN__c contactInfo = new Contact_Information_MVN__c();
        contactInfo.Account_MVN__c = acct.Id;
        contactInfo.RecordTypeId = [select Id from RecordType
                                        where SObjectType = 'Contact_Information_MVN__c'
                                        and DeveloperName = 'Phone_MVN'].Id;
        contactInfo.Phone_MVN__c = phoneNumber != null ? phoneNumber :'555-555-5555';
        return contactInfo;
    }

    public static void createEmailAddress(Account acct, String emailAddress) {
        Contact_Information_MVN__c contactInfo = generateEmailAddress(acct, emailAddress);
        insert contactInfo;
    }

    public static void createEmailAddressPrimary(Account acct, String emailAddress) {
        Contact_Information_MVN__c contactInfo = generateEmailAddress(acct, emailAddress);
        contactInfo.Primary_MVN__c = true;
        insert contactInfo;
    }

    public static Contact_Information_MVN__c generateEmailAddress(Account acct, String emailAddress) {
        Contact_Information_MVN__c contactInfo = new Contact_Information_MVN__c();
        contactInfo.Account_MVN__c = acct.Id;
        contactInfo.RecordTypeId = [select Id from RecordType
                                        where SObjectType = 'Contact_Information_MVN__c'
                                        and DeveloperName = 'Email_MVN'].Id;
        contactInfo.Email_MVN__c = emailAddress != null ? emailAddress : 'jmem@test.com';
        return contactInfo;
    }

    public static Account createPayer() {
        Account payer = new Account();
        payer.Name = 'Blue Cross Blue Shield';
        payer.RecordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Payer_MVN'][0].Id;
        insert payer;
        return payer;
    }

    public static Address_vod__c createTestAddress(Account acct){
        return createTestAddressByCountry(acct, 'us');
    }

    public static Address_vod__c createTestAddressPrimary(Account acct) {
        Address_vod__c address = generateAddressByCountry(acct, 'us');
        address.Primary_vod__c = true;
        insert address;

        return address;
    }

    public static Address_vod__c createTestAddressByCountry(Account acct, String country){
        Address_vod__c address = generateAddressByCountry(acct, country);
        insert address;

        return address;
    }

    public static Address_vod__c generateAddressByCountry(Account acct, String country) {
        Address_vod__c address = new Address_vod__c(Name = '123 Fake St.');
        address.City_vod__c = 'San Francisco Treat';
        address.Zip_vod__c = '94941';
        address.State_vod__c = 'CA';
        address.Country_vod__c = country;
        address.Account_vod__c = acct.Id;
        address.Phone_vod__c = '1111111111';

        return address;
    }

    public static Case createTestCase() {
        Account personAccount = createMember();

        Case testCase = new Case();
        testCase.AccountId = personAccount.Id;
        testCase.ContactId = [select PersonContactId from Account where Id = :personAccount.Id].PersonContactId;
        testCase.RecordTypeId = [select Id from RecordType where SObjectType = 'Case' and DeveloperName = 'Request_PS_MVN'].Id;
        testCase.Address_MVN__c = createTestAddress(personAccount).Id;
        insert testCase;

        return testCase;
    }

    public static Program_MVN__c createProgram() {
        Program_MVN__c program  = new Program_MVN__c(Name = 'A Program', Program_Id_MVN__c = 'Knipper_PAP_MVN');
        insert program;
        return program;
    }

    public static Program_Member_MVN__c createProgramMember(Program_MVN__c program, Account member) {
        Program_Member_MVN__c programMember = new Program_Member_MVN__c(Program_MVN__c = program.Id, Member_MVN__c = member.Id);
        insert programMember;
        return programMember;
    }

    public static List<Program_Member_MVN__c> createProgramMembers(Program_MVN__c program, Id startingStageId) {
        Id recordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Patient_MVN'][0].Id;

        List<Account> accounts = new List<Account>();
        for(Integer x = 0; x < 200; x++) {
            Account acct = new Account();
            acct.FirstName = 'Johnny';
            acct.LastName = 'Member';

            accounts.add(acct);
        }

        insert accounts;

        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>();
        for(Integer x = 0; x < 200; x++) {
            Program_Member_MVN__c programMember = new Program_Member_MVN__c(Program_MVN__c = program.Id,
                                                                            Member_MVN__c = accounts[x].Id,
                                                                            Starting_Program_Stage_ID_MVN__c = startingStageId);
            programMembers.add(programMember);
        }

        insert programMembers;

        return programMembers;
    }

    public static List<Program_Member_MVN__c> createProgramMembers(Program_MVN__c program, Id startingStageId, Integer numProgramMembers) {
        Id recordTypeId = [select Id from RecordType where SObjectType = 'Account' and DeveloperName = 'Patient_MVN'][0].Id;

        List<Account> accounts = new List<Account>();
        for(Integer x = 0; x < numProgramMembers; x++) {
            Account acct = new Account();
            acct.FirstName = 'Johnny';
            acct.LastName = 'Member';

            accounts.add(acct);
        }

        insert accounts;

        List<Program_Member_MVN__c> programMembers = new List<Program_Member_MVN__c>();
        for(Integer x = 0; x < numProgramMembers; x++) {
            Program_Member_MVN__c programMember = new Program_Member_MVN__c(Program_MVN__c = program.Id,
                                                                            Member_MVN__c = accounts[x].Id,
                                                                            Starting_Program_Stage_ID_MVN__c = startingStageId);
            programMembers.add(programMember);
        }

        insert programMembers;

        return programMembers;
    }

    public static Program_Stage_MVN__c createProgramStage(Program_MVN__c program) {
        Program_Stage_MVN__c programStage = new Program_Stage_MVN__c(Program_MVN__c = program.Id, Name = 'Initial Contact');
        insert programStage;
        return programStage;
    }

    public static Program_Stage_MVN__c createParentProgramStage(Program_MVN__c program) {
        Id parentRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Program_Stage_MVN__c' and DeveloperName like 'Parent%' LIMIT 1].Id;

        Program_Stage_MVN__c programStage = new Program_Stage_MVN__c(Program_MVN__c = program.Id, Name = 'Initial Contact', RecordTypeId = parentRecordTypeId);
        insert programStage;
        return programStage;
    }

    public static Program_Stage_MVN__c createChildProgramStage(Program_MVN__c program, Program_Stage_MVN__c parent) {
        Id childRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Program_Stage_MVN__c' and DeveloperName like 'Child%' LIMIT 1].Id;
        Program_Stage_MVN__c programStage = new Program_Stage_MVN__c(Program_MVN__c = program.Id,
                                                                     Parent_Stage_MVN__c = parent.Id,
                                                                     Name = 'Child Stage',
                                                                     RecordTypeId = childRecordTypeId,
                                                                     Graph_X_Coordinate_MVN__c = 101,
                                                                     Graph_Y_Coordinate_MVN__c = 202);
        insert programStage;
        return programStage;
    }

    public static Program_Stage_MVN__c createParentProgramStage(Program_MVN__c program, String stageName) {
        Id parentRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Program_Stage_MVN__c' and DeveloperName like 'Parent%' LIMIT 1].Id;

        Program_Stage_MVN__c programStage = new Program_Stage_MVN__c(Program_MVN__c = program.Id, Name = stageName, RecordTypeId = parentRecordTypeId);
        insert programStage;
        return programStage;
    }

    public static Program_Stage_MVN__c createChildProgramStage(Program_MVN__c program, Program_Stage_MVN__c parent, String stageName) {
        Id childRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Program_Stage_MVN__c' and DeveloperName like 'Child%' LIMIT 1].Id;
        Program_Stage_MVN__c programStage = new Program_Stage_MVN__c(Program_MVN__c = program.Id,
                                                                     Parent_Stage_MVN__c = parent.Id,
                                                                     Name = stageName,
                                                                     RecordTypeId = childRecordTypeId,
                                                                     Graph_X_Coordinate_MVN__c = 101,
                                                                     Graph_Y_Coordinate_MVN__c = 202);
        insert programStage;
        return programStage;
    }

    public static Program_Stage_Dependency_MVN__c createStageDependency(Program_Stage_MVN__c fromStage, Program_Stage_MVN__c toStage) {
        Program_Stage_Dependency_MVN__c dependency = new Program_Stage_Dependency_MVN__c(Program_Stage_Dependency_MVN__c = fromStage.id,
                                                                                         Program_Stage_MVN__c = toStage.id);
        insert dependency;
        return dependency;
    }

    public static  Program_Member_Stage_MVN__c createProgramMemberStage(Program_Member_MVN__c member, Program_Stage_MVN__c stage) {
        Program_Member_Stage_MVN__c programMemberStage = new Program_Member_Stage_MVN__c(Program_Stage_MVN__c=stage.Id, Program_Member_MVN__c=member.Id, Status_MVN__c='Not Started');
        insert programMemberStage;
        return programMemberStage;
    }

    public static Task createStageTask(Program_Member_Stage_MVN__c pms) {
        Task t = new Task(Subject = 'Stage Task', Status = 'Not Started', Priority = 'Normal', Program_Member_Stage_ID_MVN__c = pms.Id);
        insert t;
        return t;
    }

    public static Case createStageCase(Program_Member_Stage_MVN__c pms) {
        Case c = new Case(Subject='Stage Case', Status = 'New', origin = 'Web', Program_Member_Stage_MVN__c = pms.Id);
        
        //added RecordTypeId on 6/11/20 by Knipper Admin (MG)
        c.RecordTypeId = [select Id from RecordType where SObjectType = 'Case' and DeveloperName = 'Activity_PS_MVN'].Id;
      
        insert c;
        return c;
    }

    public static Case createProgramMemberCase(Program_Member_MVN__c pm) {
        Case c = new Case(Subject = 'PM Case', Status = 'New', origin = 'Web', Program_Member_MVN__c = pm.Id);
       
        //added RecordTypeId on 6/11/20 by Knipper Admin (MG)
        c.RecordTypeId = [select Id from RecordType where SObjectType = 'Case' and DeveloperName = 'Activity_PS_MVN'].Id;   
        
        insert c;
        return c;
    }

    public static Case_Note_MVN__c createCaseNote(Case c){
        Case_Note_MVN__c note = new Case_Note_MVN__c(Case_MVN__c = c.Id);
        insert note;
        return note;
    }

    public static Relationship_MVN__c createRelationship(Account member, Account physician, Program_Member_MVN__c pMember) {
        Relationship_MVN__c r = new Relationship_MVN__c();
        r.From_Account_MVN__c = member.Id;
        r.To_Account_MVN__c = physician.Id;
        r.From_Role_MVN__c = 'Patient';
        r.To_Role_MVN__c = 'Physician';
        r.Comments_MVN__c = 'These are not copied';

        if (pMember != null) {
            r.Program_Member_MVN__c = pMember.Id;
        }

        insert r;
        return r;
    }

    public static void createPSSettings() {
        insert buildPSSettings();
    }

    public static User generateCaseManager() {
        Profile profileId = [
            SELECT Id
              FROM Profile
             WHERE Name = 'Patient Case Manager - MVN'
             LIMIT 1
        ];

        return new User(
            LastName = 'Manager',
            FirstName = 'Case',
            Alias = 'cm',
            Email = 'casemanager#ps@mavens.com',
            Username = 'casemanager#ps@mavens.com',
            ProfileId = profileId.id,
            TimeZoneSidKey = 'GMT',
            LanguageLocaleKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LocaleSidKey = 'en_US'
        );
    }

    public static Assure_Integration_Settings_MVN__c buildAssureSettings() {
        if (assureSettings == null) {
            assureSettings = new Assure_Integration_Settings_MVN__c();
            assureSettings.Assure_Endpoint_MVN__c = 'https://test.endpoint.com';
            assureSettings.Assure_Username_MVN__c = 'testUsername';
            assureSettings.Assure_Password_MVN__c = 'testPassword';
        }
        return assureSettings;
    }

    public static Patient_Service_Settings_MVN__c buildPSSettings() {
        if (psSettings == null) {
            psSettings = new Patient_Service_Settings_MVN__c();
            psSettings.ProgramMS_Completed_Status_MVN__c = 'Completed';
            psSettings.Task_Completed_Status_MVN__c = 'Completed';
            psSettings.Case_Completed_Status_MVN__c = 'Closed';
            psSettings.Case_Default_Record_Type_MVN__c = 'Interaction_PS_MVN';
            psSettings.Case_Interaction_Record_Type_MVN__c = 'Interaction_PS_MVN';
            psSettings.Case_Activity_Record_Type_MVN__c = 'Request_PS_MVN';
            psSettings.New_Contact_Info_Account_Param_MVN__c = '000000000000000';
            psSettings.Program_Member_Stage_Start_Status_MVN__c = 'Started';
            psSettings.Display_AE_Button_MVN__c = true;
            psSettings.Display_MD_Button_MVN__c = true;
            psSettings.Display_PQC_Button_MVN__c = true;
            psSettings.Order_Replacement_Status_MVN__c = 'Replaced';
            psSettings.Order_Statuses_for_Replacing_MVN__c = 'Shipped,Cancelled';
            psSettings.Order_Statuses_for_Updating_MVN__c = 'Pending';
            psSettings.Order_Statuses_for_Voiding_MVN__c = 'Pending'; 
            psSettings.Order_Voided_Status_MVN__c = 'Cancelled';
            psSettings.Case_Parent_Status_After_Fax_Split_MVN__c = 'Cancelled';
            psSettings.Benefits_Coverage_Status_Eligible_MVN__c = 'Eligible';
            psSettings.Application_Audit_Result_Pass_Value_MVN__c = 'Pass';
            psSettings.New_Case_Default_Record_Type_MVN__c = 'Request_PS_MVN';
            psSettings.New_Case_Record_Types_MVN__c = 'Request_PS_MVN,Interaction_PS_MVN';
            psSettings.Activity_Title_For_New_Enrollment_MVN__c = 'Enroll Patient';
            psSettings.Activity_Title_For_Renewal_MVN__c = 'Renew Application';
            psSettings.Application_type_value_for_New_MVN__c = 'New';
            psSettings.Application_type_value_for_Renewal_MVN__c = 'Re-Enrollment';
        }
        return psSettings;
    }

    public static void createSearchSettings() {
        if(searchSettings == null) {
            searchSettings = new Enhanced_Account_Search_Settings_MVN__c();
            searchSettings.Account_Search_Handler_Classes_MVN__c = 'NetworkAccountSearchHdlrMVN';
            searchSettings.Account_Search_Max_Results_MVN__c = 500;
            searchSettings.Caregiver_Record_Type_MVN__c = 'Caregiver_MVN';
            searchSettings.Caregiver_Relationship_Role_MVN__c = 'Caregiver';
            searchSettings.Default_New_Business_Acct_Field_Set_MVN__c = 'New_Payer_Fields_MVN';
            searchSettings.Default_New_Business_Addr_Field_Set_MVN__c = 'New_Payer_Fields_MVN';
            searchSettings.Default_New_Email_Field_Set_MVN__c = 'New_Email_Fields_MVN';
            searchSettings.Default_New_Phone_Field_Set_MVN__c = 'New_Phone_Fields_MVN';
            searchSettings.Default_New_Person_Account_Field_Set_MVN__c = 'New_Patient_Fields_MVN';
            searchSettings.Default_New_Person_Address_Field_Set_MVN__c = 'New_Patient_Fields_MVN';
            searchSettings.Default_Search_Country_MVN__c = true;
            searchSettings.HCP_Record_Type_MVN__c = 'Professional_vod';
            searchSettings.HCP_Relationship_Role_MVN__c = 'Physician';
            searchSettings.Patient_Record_Type_MVN__c = 'Patient_MVN';
            searchSettings.Patient_Relationship_Role_MVN__c = 'Patient';
            searchSettings.Primary_Contact_Record_Type_MVN__c = 'Caregiver_MVN';
            searchSettings.Primary_Contact_Relationship_Role_MVN__c = 'Physician';
            searchSettings.Requester_Record_Types_MVN__c = 'Professional_vod,Caregiver_MVN';
            searchSettings.Search_Business_Account_Record_Types_MVN__c = 'Payer_MVN';
            searchSettings.Search_Person_Account_Record_Types_MVN__c = 'Patient_MVN,Professional_vod,Caregiver_MVN';
            searchSettings.Contact_Info_Address_Record_Type_MVN__c = 'Address_MVN';
            searchSettings.Missing_Relationship_Role_Names_MVN__c = 'Caregiver,Patient';
        }

        insert searchSettings;
    }

    public static Program_Stage_Field_Map_MVN__c createProgramStageFieldMap(String field, String value,
                                                                            String objectName, Id programStage) {

        Program_Stage_Field_Map_MVN__c programStageFieldMap = new Program_Stage_Field_Map_MVN__c();
        programStageFieldMap.Field_MVN__c = field;
        programStageFieldMap.Value_MVN__c = value;
        programStageFieldMap.Object_MVN__c = objectName;
        programStageFieldMap.Program_Stage_MVN__c = programStage;

        insert programStageFieldMap;

        return programStageFieldMap;
    }
}