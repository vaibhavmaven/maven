/**
 * TestFactoryMDMFieldMappingMVN
 * @author: Pablo Roldan
 * @created Date: October 2017
 * @description: This class is used to construct MDM Field mapping custom metadatas
 */
@isTest
public with sharing class TestFactoryMDMFieldMappingMVN {

    public static void setMocks() {
        setMocks(null);
    }

    public static void setMocks(Map<String, Object> fieldValues) {
        setMocks(null, fieldValues);
    }

    public static void setMocks(List<String> mdmFieldMappingIds, Map<String, Object> fieldValues) {
        String mdmConnectionId = '000000000000100';

        Map<String, Object> mdmConnectionIdMap = new Map<String, Object>{
            'Id' => mdmConnectionId
        };
        TestFactoryMDMConnectionsMVN.setMock(mdmConnectionIdMap);
        
        List<MDM_Field_Mapping_MVN__mdt> settings = buildMockSettings(mdmConnectionId, mdmFieldMappingIds, fieldValues);

        MDMFieldMappingMVN.setMocks(settings);
    }
    

    private static List<MDM_Field_Mapping_MVN__mdt> buildMockSettings(String mdmConnectionId, List<String> mdmFieldMappingIds, Map<String, Object> fieldValues) {
        List<MDM_Field_Mapping_MVN__mdt> settings = new List<MDM_Field_Mapping_MVN__mdt>();

        Integer mdmFieldMappingIdsIndex = 0;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ADDRESS_OBJECT,
                                    'All',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'Name', 
                                    'address_line_1__v'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ACCOUNT_OBJECT, 
                                    'All',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'RecordTypeId', 
                                    'entityType'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ADDRESS_OBJECT,
                                    'All',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'RecordTypeId', 
                                    'entityType'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ACCOUNT_OBJECT, 
                                    'Hospital_vod',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'Name', 
                                    'corporate_name__v'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ACCOUNT_OBJECT, 
                                    'Professional_vod',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'LastName', 
                                    'last_name__v'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ACCOUNT_OBJECT, 
                                    'Professional_vod',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'MiddleName', 
                                    'middle_name__v'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ACCOUNT_OBJECT, 
                                    'Professional_vod',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'FirstName', 
                                    'first_name__v'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ACCOUNT_OBJECT, 
                                    'Professional_vod',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'Gender_MVN__c', 
                                    'gender__v'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ACCOUNT_OBJECT, 
                                    'Professional_vod',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'Specialty_1_vod__c', 
                                    'specialty_1__v'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ACCOUNT_OBJECT, 
                                    'All',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'Country_MVN__c', 
                                    'primary_country__v'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ADDRESS_OBJECT, 
                                    'All',
                                    getFieldValues(mdmFieldMappingIdsIndex, 
                                    mdmFieldMappingIds, 
                                    fieldValues), 
                                    'Country_vod__c', 
                                    'country__v'));
        mdmFieldMappingIdsIndex++;
        settings.add(buildMockSetting(mdmConnectionId, 
                                    VeevaNetworkConnectionUtilMVN.ADDRESS_OBJECT, 
                                    'All',
                                    getFieldValues(mdmFieldMappingIdsIndex, mdmFieldMappingIds, fieldValues), 
                                    'Phone_vod__c', 
                                    'phone_1__v'));

        return settings;
    }

    private static Map<String, Object> getFieldValues(Integer mdmFieldMappingIdsIndex, List<String> mdmFieldMappingIds, Map<String, Object> fieldValues) {
        if(mdmFieldMappingIds != null && !mdmFieldMappingIds.isEmpty() && mdmFieldMappingIdsIndex < mdmFieldMappingIds.size()) {
            if(fieldValues == null) {
                fieldValues = new Map<String, Object>();
            }

            String mdmFieldMappingId = mdmFieldMappingIds[mdmFieldMappingIdsIndex];

            if(String.IsNotBlank(mdmFieldMappingId)) {
                fieldValues.put('Id', mdmFieldMappingId);
            }
        }

        return fieldValues;
    }

    private static MDM_Field_Mapping_MVN__mdt buildMockSetting(String mdmConnectionId, 
                                                              String sobjectName, 
                                                              String recordTypeName, 
                                                 Map<String, Object> fieldValues, 
                                                              String sfFieldName, 
                                                              String mdmFieldName) {
        Map<String, Object> setting = new Map<String, Object> {
            'MDM_Connection_MVN__c' => mdmConnectionId,
            'SObject_API_Name_MVN__c' => sobjectName,
            'Record_Type_MVN__c' => recordTypeName,
            'MDM_Field_Name_MVN__c' => mdmFieldName,
            'SF_Field_Name_MVN__c' => sfFieldName
        };

        if (fieldValues != null) {
            setting.putAll(fieldValues);
        }

        return (MDM_Field_Mapping_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata('MDM_Field_Mapping_MVN__mdt', setting);
    }

}