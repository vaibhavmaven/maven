/**
 *	ContactInfoDeleteTriggerMVN
 *	Created By:		Aggen
 *	Created On:		01/12/2016
 *  Description:	This is a trigger class that removes related Address records when a Contact Info record is deleted
 *					
 **/
public class ContactInfoDeleteTriggerMVN implements TriggerHandlerMVN.HandlerInterface {
	
	public void handle() {
		if(Trigger.isDelete) {
			List<Id> deletedContactInfoIDs = new List<Id>();	
			List<Address_vod__c> removeAddresses = new List<Address_vod__c>();	
			
			for(Contact_Information_MVN__c deletedContInfo : (List<Contact_Information_MVN__c>) Trigger.old) {
				if(deletedContInfo.Address_MVN__c != null) {
					deletedContactInfoIDs.add(deletedContInfo.Address_MVN__c);
				}
			}			
						
			for(Address_vod__c removeAddr : [select Id from Address_vod__c where Id in :deletedContactInfoIDs]) {
				removeAddresses.add(removeAddr);
			}

			delete removeAddresses;			
		}		
	}
}