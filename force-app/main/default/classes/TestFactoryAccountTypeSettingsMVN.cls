/**
 * TestFactoryAccountTypeSettingsMVN
 * @edited by: Pablo Roldan
 * @edited date: October 2017
 * @description: Test data Factory for Account Type Settings metadata used for Search Network and DCRs.
 * @edit description: Adapted to PS
 */
@isTest
public class TestFactoryAccountTypeSettingsMVN {

    public static void setMock() {
        setMock(null);
    }

    /**
    * Set Mock (List<Map<String, Object>>)
    * @description: In order to set values an Account_Record_Type_MVN__c must be provided at a minimum.
    */
    public static void setMock(List<Map<String, Object>> fieldValuesObjects) {
        TestFactorySegmentsMVN.createMockSegments();

        List<Map<String, Object>> settings = new List<Map<String, Object>> {allType,
                                                                            hcpType,
                                                                            institutionType,
                                                                            nonHcpType,
                                                                            employeeType};
                                                                            
        if (fieldValuesObjects != null) {
            for (Map<String,Object> setting : fieldValuesObjects) {
                if (setting.containsKey('Account_Record_Type_MVN__c')) {
                    Boolean foundSetting = false;
                    for (Map<String,Object> defaultSetting : settings) {
                        //check each default setting to see if we need to overwrite
                        String defaultSettingType = (String)defaultSetting.get('Account_Record_Type_MVN__c');
                        String currentSettingType = (String)setting.get('Account_Record_Type_MVN__c');
                        if (defaultSettingType == currentSettingType) {
                            defaultSetting.putAll(setting);
                            foundSetting = true;
                            break;
                        }
                    }

                    if (!foundSetting) {
                        //if we didn't find and overwrite a setting then this is a new setting. add it to the list.
                        settings.add(setting);
                    }
                } else {
                    System.debug('### Skipping setting ' + setting + '. No Account Record Type Provided.');
                }
            }
        }

        List<Account_Type_Setting_MVN__mdt> wrappedSettings = new List<Account_Type_Setting_MVN__mdt>();
        for (Map<String, Object> setting : settings) {
            wrappedSettings.add((Account_Type_Setting_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata(
                'Account_Type_Setting_MVN__mdt',
                setting
            ));
        }

        AccountTypeSettingsMVN.setMock(wrappedSettings);
    }

    private static Map<String, Object> allType {
        get {
            return new Map<String,Object>{
                'Account_Record_Type_MVN__c'=>'All',
                'Account_Search_Order_MVN__c'=>null,
                'Allow_Filter_by_Affiliation_Search_MVN__c'=>false,
                'Allow_Referred_By_MVN__c'=>false,
                'Anonymize_MVN__c'=>false,
                'Anonymize_Hold_Period_Days_MVN__c'=>15,
                'Change_Search_Tab_on_Select_MVN__c'=>false,
                'Create_DCR_MVN__c'=>false,
                'Default_for_Search_MVN__c'=>false,
                'DeveloperName'=>'Person_Search_All',
                'Include_in_Create_MVN__c'=>false,
                'Include_in_Search_MVN__c'=>true,
                'Is_Person_Record_Type_MVN__c'=>true,
                'Label'=>'Person Search All',
                'Language'=>'en_US',
                'Records_Required_for_Create_MVN__c'=>'',
                'Require_Institution_on_Create_MVN__c'=>false,
                'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId
            };
        }
    }

    private static Map<String, Object> hcpType {
        get {
            return new Map<String,Object>{
                'Account_Record_Type_MVN__c'=>'Professional_vod',
                'Account_Search_Order_MVN__c'=>1,
                'Allow_Filter_by_Affiliation_Search_MVN__c'=>true,
                'Allow_Referred_By_MVN__c'=>false,
                'Anonymize_MVN__c'=>false,
                'Anonymize_Hold_Period_Days_MVN__c'=>15,
                'Change_Search_Tab_on_Select_MVN__c'=>false,
                'Create_DCR_MVN__c'=>false,
                'Default_for_Search_MVN__c'=>true,
                'DeveloperName'=>'Professional_vod',
                'Include_in_Create_MVN__c'=>true,
                'Include_in_Search_MVN__c'=>true,
                'Is_Person_Record_Type_MVN__c'=>true,
                'Label'=>'HCP',
                'Language'=>'en_US',
                'Records_Required_for_Create_MVN__c'=>'Address_MVN,Phone_MVN',
                'Require_Institution_on_Create_MVN__c'=>false,
                'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId
            };
        }
    }

    private static Map<String, Object> institutionType {
        get {
            return new Map<String,Object>{
                'Account_Record_Type_MVN__c'=>'Hospital_vod',
                'Account_Search_Order_MVN__c'=>1,
                'Allow_Filter_by_Affiliation_Search_MVN__c'=>true,
                'Allow_Referred_By_MVN__c'=>false,
                'Anonymize_MVN__c'=>false,
                'Anonymize_Hold_Period_Days_MVN__c'=>15,
                'Change_Search_Tab_on_Select_MVN__c'=>false,
                'Create_DCR_MVN__c'=>false,
                'Default_for_Search_MVN__c'=>false,
                'DeveloperName'=>'Hospital_vod',
                'Include_in_Create_MVN__c'=>true,
                'Include_in_Search_MVN__c'=>true,
                'Is_Person_Record_Type_MVN__c'=>false,
                'Label'=>'Institution',
                'Language'=>'en_US',
                'Records_Required_for_Create_MVN__c'=>'',
                'Require_Institution_on_Create_MVN__c'=>false,
                'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId
            };
        }
    }

    private static Map<String, Object> nonHcpType {
        get {
            return new Map<String,Object>{
                'Account_Record_Type_MVN__c'=>'Non_HCP_MVN',
                'Account_Search_Order_MVN__c'=>2,
                'Allow_Filter_by_Affiliation_Search_MVN__c'=>false,
                'Allow_Referred_By_MVN__c'=>false,
                'Anonymize_MVN__c'=>false,
                'Anonymize_Hold_Period_Days_MVN__c'=>15,
                'Change_Search_Tab_on_Select_MVN__c'=>false,
                'Create_DCR_MVN__c'=>false,
                'Default_for_Search_MVN__c'=>false,
                'DeveloperName'=>'Non_HCP_MVN',
                'Include_in_Create_MVN__c'=>true,
                'Include_in_Search_MVN__c'=>true,
                'Is_Person_Record_Type_MVN__c'=>true,
                'Label'=>'Non-HCP',
                'Language'=>'en_US',
                'Records_Required_for_Create_MVN__c'=>'',
                'Require_Institution_on_Create_MVN__c'=>false,
                'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId
            };
        }
    }

    private static Map<String, Object> employeeType {
        get {
            return new Map<String,Object>{
                'Account_Record_Type_MVN__c'=>'Staff_MVN',
                'Account_Search_Order_MVN__c'=>3,
                'Allow_Filter_by_Affiliation_Search_MVN__c'=>false,
                'Allow_Referred_By_MVN__c'=>true,
                'Anonymize_MVN__c'=>false,
                'Anonymize_Hold_Period_Days_MVN__c'=>15,
                'Change_Search_Tab_on_Select_MVN__c'=>false,
                'Create_DCR_MVN__c'=>false,
                'Default_for_Search_MVN__c'=>false,
                'DeveloperName'=>'Employee_MVN',
                'Include_in_Create_MVN__c'=>true,
                'Include_in_Search_MVN__c'=>true,
                'Is_Person_Record_Type_MVN__c'=>true,
                'Label'=>'Employee',
                'Language'=>'en_US',
                'Records_Required_for_Create_MVN__c'=>'',
                'Require_Institution_on_Create_MVN__c'=>false,
                'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId
            };
        }
    }
}