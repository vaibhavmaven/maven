/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description implements number comparisons rule
 */
public with sharing class EligibilityEngineNumberCompHdlrMVN extends EligibilityEngineRuleHdlrMVN {
    /**
     * checks if rule is fired for thresholds
     * @return  Boolean     flag if rule is fired
     */
    protected override Boolean evaluateSingleRecord(SObject obj) {
        Object sourceFieldObject = super.getSource(obj);
        if (sourceFieldObject == null) {
            return true;
        }
        Decimal sourceField = (Decimal) sourceFieldObject;
        if (sourceField == null || rule.Value_MVN__c == null) {
            return true;
        }
        Decimal value = Decimal.valueOf(rule.Value_MVN__c);
        if(super.rule.Comparison_MVN__c == 'Equal_To_MVN') {
            return sourceField == value;
        } else if(super.rule.Comparison_MVN__c == 'Not_Equal_To_MVN') {
            return sourceField != value;
        } else if(super.rule.Comparison_MVN__c == 'Less_Than_MVN') {
            return sourceField < value;
        } else if(super.rule.Comparison_MVN__c == 'Greater_Than_MVN') {
            return sourceField > value;
        } else if(super.rule.Comparison_MVN__c == 'Less_Than_Or_Equal_To_MVN') {
            return sourceField <= value;
        } else if(super.rule.Comparison_MVN__c == 'Greater_Than_Or_Equal_To_MVN') {
            return sourceField >= value;
        } else {
            return true;
        }
    }
}