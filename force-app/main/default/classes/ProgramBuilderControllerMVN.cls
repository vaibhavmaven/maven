/*
*   ProgramBuilderControllerMVN
*   Created By:     Vincent Reeder
*   Created Date:   October 5, 2015
*	Description:	Provides controller support for ProgramBuilderMVN and the Program Builder app.
*/

global with sharing class ProgramBuilderControllerMVN {

	global ProgramBuilderControllerMVN() { }

	global ProgramBuilderControllerMVN(ApexPages.StandardController stdController) {

	}

	@RemoteAction
	global static String getRecordTypeId(String sObjectApiName, String recordTypeDeveloperName) {
		return [SELECT ID FROM RecordType WHERE SobjectType = :sObjectApiName AND DeveloperName = :recordTypeDeveloperName].Id;
	}

	@RemoteAction
	global static List<Node> getGraphNodesForParentStageId(String parentStageId) {
		List<Program_Stage_MVN__c> childStages =
			[SELECT ID, Name, Graph_X_Coordinate_MVN__c, Graph_Y_Coordinate_MVN__c,
				(SELECT ID, Program_Stage_Dependency_MVN__r.Name from Program_Stage_Dependencies__r)
					FROM Program_Stage_MVN__c WHERE Parent_Stage_MVN__c = :parentStageId];

		List<Node> nodes = new List<Node>();
		for (Program_Stage_MVN__c s : childStages) {
			Node n = new Node(
				String.valueOf(s.Id),
				s.Name,
				Integer.valueOf(s.Graph_X_Coordinate_MVN__c),
				Integer.valueOf(s.Graph_Y_Coordinate_MVN__c)
			);
			nodes.add(n);
		}

		return nodes;
	}

	@RemoteAction
	global static Map<String, String> getFieldSetsForSobject(String objectApiName) {
		Schema.SObjectType token = Schema.getGlobalDescribe().get(objectAPIName);
		Schema.DescribeSObjectResult dr = token.getDescribe();
		Map<String, String> fieldSets = new Map<String, String>();
		for (String s : dr.fieldSets.getMap().keySet()) {
			fieldSets.put( dr.fieldSets.getMap().get(s).getLabel(), dr.fieldSets.getMap().get(s).getName() );
		}
		return fieldSets;
	}

	@RemoteAction
	global static Map<String, String> getPicklistValues(String objectApiName, String fieldApiName) {
		return UtilitiesMVN.getPicklistValues(objectApiName, fieldApiName);
	}

	@RemoteAction
	global static List<Edge> getGraphEdgesForParentStageId(String parentStageId) {
		List<Program_Stage_Dependency_MVN__c> deps =
			[SELECT ID,
				Program_Stage_Dependency_MVN__c,
				Program_Stage_Dependency_MVN__r.Name,
				Program_Stage_Dependency_MVN__r.Graph_X_Coordinate_MVN__c,
				Program_Stage_Dependency_MVN__r.Graph_Y_Coordinate_MVN__c,
				Program_Stage_MVN__r.Name,
				Program_Stage_MVN__c,
				Program_Stage_MVN__r.Graph_X_Coordinate_MVN__c,
				Program_Stage_MVN__r.Graph_Y_Coordinate_MVN__c
					FROM Program_Stage_Dependency_MVN__c
						WHERE Program_Stage_MVN__r.Parent_Stage_MVN__c = :parentStageId];

		List<Edge> edges = new List<Edge>();
		for (Program_Stage_Dependency_MVN__c d : deps) {
			Node target = new Node(
				String.valueOf(d.Program_Stage_MVN__c),
				d.Program_Stage_MVN__r.Name,
				Integer.valueOf(d.Program_Stage_MVN__r.Graph_X_Coordinate_MVN__c),
				Integer.valueOf(d.Program_Stage_MVN__r.Graph_Y_Coordinate_MVN__c)
			);
			Node source = new Node(
				String.valueOf(d.Program_Stage_Dependency_MVN__c),
				d.Program_Stage_Dependency_MVN__r.Name,
				Integer.valueOf(d.Program_Stage_Dependency_MVN__r.Graph_X_Coordinate_MVN__c),
				Integer.valueOf(d.Program_Stage_Dependency_MVN__r.Graph_Y_Coordinate_MVN__c)
			);

			Edge e = new Edge(
				source,
				target,
				d.Id
			);
			edges.add(e);
		}

		return edges;
	}

	@RemoteAction
	global static String insertGraphNode(String programId, String parentStageId, String name, Integer x, Integer y) {
		Program_Stage_MVN__c ps = new Program_Stage_MVN__c();
		ps.Name = name;
		ps.RecordTypeId = [SELECT ID FROM RecordType WHERE SobjectType = 'Program_Stage_MVN__c' AND DeveloperName = 'Child_Stage_MVN'].ID;
		ps.Program_MVN__c = programId;
		ps.Parent_Stage_MVN__c = parentStageId;
		ps.Graph_X_Coordinate_MVN__c = x;
		ps.Graph_Y_Coordinate_MVN__c = y;
		insert ps;
		return ps.Id;
	}

	@RemoteAction
	global static Boolean updateGraphNode(String recordId, String name, Integer x, Integer y) {
		Program_Stage_MVN__c ps = [SELECT ID FROM Program_Stage_MVN__c WHERE ID = :recordId];
		ps.Name = name;
		ps.Graph_X_Coordinate_MVN__c = x;
		ps.Graph_Y_Coordinate_MVN__c = y;
		update ps;
		return true;
	}

	@RemoteAction
	global static Boolean deleteGraphNode(String recordId) {
		delete [SELECT ID FROM Program_Stage_MVN__c WHERE ID = :recordId];
		return true;
	}

	@RemoteAction
	global static String insertGraphEdge(String sourceId, String targetId) {
		Program_Stage_Dependency_MVN__c d = new Program_Stage_Dependency_MVN__c();
		d.Program_Stage_MVN__c = targetId;
		d.Program_Stage_Dependency_MVN__c = sourceId;
		insert d;
		return d.Id;
	}

	@RemoteAction
	global static Boolean updateGraphEdge(String parentStageId) {
		return true;
	}

	@RemoteAction
	global static Boolean deleteGraphEdge(String recordId) {
		delete [SELECT ID FROM Program_Stage_Dependency_MVN__c WHERE ID = :recordId];
		return true;
	}

	@RemoteAction
	global static String cloneProgram(String programId, String newName) {
		String cloneOperationId = UtilitiesMVN.getRandomString(200);

		Program_MVN__c existingProgram = [SELECT Id, Name FROM Program_MVN__c WHERE Id = :programId];
		Program_MVN__c newProgram = existingProgram.clone(false, true);
		newProgram.Name = newName;
		newProgram.Cloned_MVN__c = true;
		insert newProgram;

		List<Program_Stage_MVN__c> programStages = new List<Program_Stage_MVN__c>();
		List<Program_Stage_Dependency_MVN__c> programStageDependencies = new List<Program_Stage_Dependency_MVN__c>();
		List<String> programStageIds = new List<String>();
		Map<String,String> oldProgramStageToNewProgramStageId = new Map<String,String>();
		Map<String,String> dependencyMap = new Map<String,String>();

		for (Program_Stage_MVN__c ps : [SELECT Activity_Type_MVN__c, Description_MVN__c, Exclude_Activity_MVN__c,
												Flow_Name_MVN__c, Graph_X_Coordinate_MVN__c, Graph_Y_Coordinate_MVN__c,
												Id, Is_Parent_Stage_MVN__c, Name, Parent_Stage_MVN__c, Program_MVN__c,
												RecordTypeId, Related_Object_Close_Status_MVN__c, Related_Object_MVN__c,
												Related_Object_Open_Status_MVN__c, Stage_Sequence_Number_MVN__c,
										(SELECT Id, Program_Stage_Dependency_MVN__c, Program_Stage_MVN__c FROM Program_Stage_Dependencies__r)
											FROM Program_Stage_MVN__c WHERE Program_MVN__c = :existingProgram.Id]) {

			Program_Stage_MVN__c newProgramStage = ps.clone(false, true);
			newProgramStage.Program_MVN__c = newProgram.Id;
			newProgramStage.Cloned_From_Id_MVN__c = ps.Id;
			newProgramStage.Clone_Operation_Id_MVN__c = cloneOperationId;
			newProgramStage.Cloned_From_Parent_Id_MVN__c = ps.Parent_Stage_MVN__c;
			programStageIds.add(ps.Id);
			programStages.add(newProgramStage);

			for (Program_Stage_Dependency_MVN__c psd : ps.Program_Stage_Dependencies__r) {
				dependencyMap.put(psd.Program_Stage_MVN__c, psd.Program_Stage_Dependency_MVN__c);
			}
		}

		insert programStages;

		for (Program_Stage_MVN__c ps : programStages) {
			oldProgramStageToNewProgramStageId.put(ps.Cloned_From_Id_MVN__c, ps.Id);
		}

		for (Program_Stage_MVN__c ps : programStages) {
			ps.Parent_Stage_MVN__c = oldProgramStageToNewProgramStageId.get(ps.Cloned_From_Parent_Id_MVN__c);
		}

		update programStages;

		for (Program_Stage_Dependency_MVN__c existingDependency : [SELECT Id, Program_Stage_MVN__c, Program_Stage_Dependency_MVN__c
														FROM Program_Stage_Dependency_MVN__c
															WHERE
																Program_Stage_MVN__c in :programStageIds
																OR
																Program_Stage_Dependency_MVN__c in :programStageIds]) {
			Program_Stage_Dependency_MVN__c newDependency = new Program_Stage_Dependency_MVN__c();
			newDependency.Program_Stage_MVN__c = oldProgramStageToNewProgramStageId.get(existingDependency.Program_Stage_MVN__c);
			newDependency.Program_Stage_Dependency_MVN__c = oldProgramStageToNewProgramStageId.get(existingDependency.Program_Stage_Dependency_MVN__c);
			programStageDependencies.add(newDependency);
		}

		insert programStageDependencies; //upsert matches to program stages using external id
		return newProgram.Id;
	}

	global class Node {
	    global String id;
	    global String recordId;
	    global String title;
	    global Integer x;
	    global Integer y;

	    global Node(String recordId, String title, Integer x, Integer y) {
	        this.id = UtilitiesMVN.getRandomString(4);
	        this.recordId = recordId;
	        this.title = title;
	        this.x = x;
	        this.y = y;
	    }
	}

	global class Edge {
		global Node source;
		global Node target;
		global String recordId;

		global Edge(Node source, Node target, String recordId) {
			this.source = source;
			this.target = target;
			this.recordId = recordId;
		}
	}

	global void ajaxEndpoint() {

	}
}