public class PAP_PrimeRx_CreatePatientBatch_Scheduler implements Schedulable{
   
    public void execute(SchedulableContext sc){
         // Execute a batch job 
        Database.executeBatch(new PAP_PrimeRx_CreatePatientBatch(),1);
        
        
    }
}