/*
 *	ProgramMemberUpActivitiesCtrlTestMVN
 *	Created By:		Jen Wyher
 *	Created Date:	Dec 2015
 *	Description:	Tests for the ProgramMemberUpcomingActivitiesCtrlMVN class.
 *	Updated:		February 10, 2016 by Vincent Reeder
 *	Modifed:		March 23, 2016 - Thomas Hajcak (thomas@mavensconsulting.com)
 *					Updated unit test to improve code coverage after refactoring controller.
 */

@isTest
private class ProgramMemberUpActivitiesCtrlTestMVN {

    static Account 						member;
    static Program_MVN__c 				program;
    static Program_Member_MVN__c 		pMember;
    static Program_Stage_MVN__c 		pStage;
    static Case 						stagelessCase;
    
    static {
        TestDataFactoryMVN.createPSSettings();

        program = TestDataFactoryMVN.createProgram();
        member = TestDataFactoryMVN.createMember();
        pStage = TestDataFactoryMVN.createProgramStage(program);
        pMember = TestDataFactoryMVN.createProgramMember(program, member);
        stagelessCase = TestDataFactoryMVN.createProgramMemberCase(pMember);
        TestDataFactoryMVN.createProgramMemberCase(pMember);
    }

    @isTest static void getUpcomingActivities() {
        PageReference pageRef = Page.ProgramMemberUpcomingActivitiesMVN;
        Program_Member_Stage_MVN__c pms = [SELECT Id FROM Program_Member_Stage_MVN__c WHERE Program_Stage_Name_MVN__c = 'New Stage'];

        Test.startTest();
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdController = new ApexPages.StandardController(pMember);
        ProgramMemberUpcomingActivitiesCtrlMVN controller = new ProgramMemberUpcomingActivitiesCtrlMVN(stdController);
        List<ProgramMemberStageWrapperMVN> stages = controller.getStages();

        System.assertEquals(3, stages.size());

        ProgramMemberStageWrapperMVN result;

        for(ProgramMemberStageWrapperMVN stage : stages) {
            if(stage.pms.Program_Stage_Name_MVN__c == 'New Stage') {
                result = stage;
            }
        }
        System.assertEquals(pms.Id, result.stageId);
        Test.stopTest();
    }

    @isTest static void getNoUpcomingActivities() {
        PageReference pageRef = Page.ProgramMemberUpcomingActivitiesMVN;

        Test.startTest();
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdController = new ApexPages.StandardController(pMember);
        ProgramMemberUpcomingActivitiesCtrlMVN controller = new ProgramMemberUpcomingActivitiesCtrlMVN(stdController);

        List<ProgramMemberStageWrapperMVN> stages = controller.getStages();
        System.assertEquals(3, stages.size());

        List<ProgramMemberStageWrapperMVN> stagelessCases;

        for(ProgramMemberStageWrapperMVN stage : controller.getStages()) {
            if(stage.name == Label.Adhoc_Activities) {
                stagelessCases = stage.children;
            }
        }

        System.assertEquals(2, stagelessCases.size());
        System.assertEquals(stagelessCase.Id, stagelessCases[0].caseId);
        Test.stopTest();
    }

    @isTest static void getCaseNotes() {
        PageReference pageRef = Page.ProgramMemberUpcomingActivitiesMVN;

        Test.startTest();
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdController = new ApexPages.StandardController(pMember);
        ProgramMemberUpcomingActivitiesCtrlMVN controller = new ProgramMemberUpcomingActivitiesCtrlMVN(stdController);
        
        List<ProgramMemberStageWrapperMVN> stagelessCases;

        for(ProgramMemberStageWrapperMVN stage : controller.getStages()) {
            if(stage.name == Label.Adhoc_Activities) {
                stagelessCases = stage.children;
            }
        }

        Case_Note_MVN__c note1 = TestDataFactoryMVN.createCaseNote(stageLessCases[0].caseRecord);
        note1.Locked_MVN__c = true;
        Case_Note_MVN__c note2 = TestDataFactoryMVN.createCaseNote(stageLessCases[1].caseRecord);
        note2.Locked_MVN__c = true;

        update new Case_Note_MVN__c[]{ note1, note2 };

        List<ProgramMemberUpcomingActivitiesCtrlMVN.CaseNotes> casesWithNotes = controller.caseNotes;
        System.assertEquals(2, casesWithNotes.size());
        Test.stopTest();
    }

    @isTest static void testCreateActivities() {
        Program_Member_Stage_MVN__c pms = [SELECT Id FROM Program_Member_Stage_MVN__c WHERE Program_Stage_Name_MVN__c = 'New Stage'];
        
        Test.startTest();
        ProgramMemberUpcomingActivitiesCtrlMVN.createAdditionalActivity(pMember.Id);
        ProgramMemberUpcomingActivitiesCtrlMVN.createActivity(pms.Id, pMember.Id);

        ApexPages.StandardController stdController = new ApexPages.StandardController(pMember);
        ProgramMemberUpcomingActivitiesCtrlMVN controller = new ProgramMemberUpcomingActivitiesCtrlMVN(stdController);

        List<ProgramMemberStageWrapperMVN> stagelessCases;
        Test.stopTest();

        for(ProgramMemberStageWrapperMVN stage : controller.getStages()) {
            if(stage.name == Label.Adhoc_Activities) {
                stagelessCases = stage.children;
            }
        }

        System.assertEquals(3, stagelessCases.size());
    }

    @isTest static void testCaseNoteFilters() {
        ApexPages.StandardController stdController = new ApexPages.StandardController(pMember);
        ProgramMemberUpcomingActivitiesCtrlMVN controller = new ProgramMemberUpcomingActivitiesCtrlMVN(stdController);
        
        Set<String> filters = new Set<String>();

        filters.addAll(controller.getCaseNoteFilters());

        Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();

        Schema.DescribeSObjectResult caseObject = Case.getSObjectType().getDescribe();
        Map<String, Schema.SObjectField> fieldsMap = caseObject.fields.getMap();
        List<Schema.PicklistEntry> picklistValues = fieldsMap.get(settings.Case_Filter_Field_MVN__c).getDescribe().getPickListValues();

        for(Schema.PicklistEntry entry : picklistValues){
            System.assert(filters.contains(entry.getValue()));
        }
    }

    @isTest static void testGetCaseNoteFilter() {
        System.runAs(TestDataFactoryMVN.generateCaseManager()) {
            ApexPages.StandardController stdController = new ApexPages.StandardController(pMember);
            ProgramMemberUpcomingActivitiesCtrlMVN controller = new ProgramMemberUpcomingActivitiesCtrlMVN(stdController);

            Test.startTest();
            String label = controller.getCaseNoteFilterFieldLabel();
            Test.stopTest();

            System.assertEquals('Case Origin', label);
        }
    }

    //@isTest static void testCaseNotesShouldBeSortable() {
    //	System.runAs(TestDataFactoryMVN.generateCaseManager()) {
    //		Case_Note_MVN__c note1 = TestDataFactoryMVN.createCaseNote(stagelessCase);
    //		ApexPages.StandardController stdController = new ApexPages.StandardController(pMember);
    //		ProgramMemberUpcomingActivitiesCtrlMVN controller = new ProgramMemberUpcomingActivitiesCtrlMVN(stdController);

    //		Test.startTest();
    //		controller.caseNotes.sort();
    //		Test.stopTest();


    //	}
    //}
}