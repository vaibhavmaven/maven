/**
 * @author Mavens
 * @date 07/13/2018
 * @description Methods to get values dynamically from a record. These methods are used mainly
 * for processing custom metadata.
 */
public with sharing class SObjectDynamicUtilMVN {

    /**
     * Get field value from a record passing the field name, going up the relationship chain if necessary
     * @return  Object field value
     * @example
     * Order_Scheduler_MVN__c orderScheduler = [
     *    SELECT Application_MVN__r.Product_MVN__r.Name FROM Order_Scheduler_MVN__c LIMIT 1
     * ];
     * getFieldValue(orderScheduler, 'Application_MVN__r.Product_MVN__r.Name');
     * The method will build the command:
     *    orderScheduler.getSobject('Application_MVN__r').getSobject('Product_MVN__r').get('Name');
     * And will return the value for the name of the product.
     */
    public static Object getFieldValue(SObject sObj, String sourceField) {
        SObject sObjectOfSourceField = sObj;
        List<String> relationshipAPINames = sourceField.split('\\.');
        Integer sourceFieldAPINameIndex = relationshipAPINames.size() - 1;
        String sourceFieldAPIName = relationshipAPINames.get(sourceFieldAPINameIndex);
        relationshipAPINames.remove(sourceFieldAPINameIndex);
        for(String relationshipAPIName : relationshipAPINames) {
            sObjectOfSourceField = sObjectOfSourceField.getSObject(relationshipAPIName);
        }
        return (sObjectOfSourceField != null ? sObjectOfSourceField.get(sourceFieldAPIName) : null);
    }
}