/*
 *  CreateRelationshipsForProgramMemTestMVN
 *  Created By:     Florian Hoehn
 *  Created Date:   May 30th 2017
 *  Description:    tests CreateRelationshipsForProgramMembersMVN
 */
@isTest private class CreateRelationshipsForProgramMemTestMVN {
    /**
    * @description setups data for all unit tests
    */
    @testSetup private static void setupData() {
        TestDataFactoryMVN.createPSSettings();
        Program_MVN__c program = TestDataFactoryMVN.createProgram();
        TestDataFactoryMVN.createProgramStage(program);
        Account testAccount = TestDataFactoryMVN.createMember();
        Program_Member_MVN__c programMember = TestDataFactoryMVN.createProgramMember(program, testAccount);
        Account professional = new TestFactoryAccountMVN().createPrescriber();
    }

    /**
    * @description tests that insert of a program member with physician results in new relationship
    */
    @isTest private static void testProgramMemberInsertCreatesRelationship() {
        Program_MVN__c expectedProgram = [SELECT Id FROM Program_MVN__c LIMIT 1];
        Account expectedMember = [SELECT Id FROM Account WHERE LastName = 'Member' LIMIT 1];
        Account expectedPhysician = [SELECT Id FROM Account WHERE Credentials_vod__c = 'MD'];
        Program_Member_MVN__c expectedProgramMember = new Program_Member_MVN__c(
            Program_MVN__c = expectedProgram.Id,
            Member_MVN__c = expectedMember.Id,
            Physician_MVN__c = expectedPhysician.Id
        );

        Test.startTest();
            insert expectedProgramMember;
        Test.stopTest();

        Relationship_MVN__c actualRelationship = [SELECT Id, Relationship_Key_MVN__c
                                                    FROM Relationship_MVN__c
                                                   LIMIT 1];
        System.assertNotEquals(null, actualRelationship);
        System.assertEquals(
            expectedProgramMember.Member_MVN__c + ':' + expectedPhysician.Id + ':' + expectedProgramMember.Id,
            actualRelationship.Relationship_Key_MVN__c
        );
    }

    /**
    * @description tests that update of a program member with physician results in new relationship
    */
    @isTest private static void testPhysicianUpdateCreatesRelationship() {
        Program_Member_MVN__c expectedProgramMember = [SELECT Id, Physician_MVN__c, Member_MVN__c
                                                         FROM Program_Member_MVN__c
                                                        LIMIT 1];
        Account expectedPhysician = [SELECT Id FROM Account WHERE Credentials_vod__c = 'MD'];

        Test.startTest();
            expectedProgramMember.Physician_MVN__c = expectedPhysician.Id;
            update expectedProgramMember;
        Test.stopTest();

        Relationship_MVN__c actualRelationship = [SELECT Id, Relationship_Key_MVN__c
                                                    FROM Relationship_MVN__c
                                                   LIMIT 1];
        System.assertNotEquals(null, actualRelationship);
        System.assertEquals(
            expectedProgramMember.Member_MVN__c + ':' + expectedPhysician.Id + ':' + expectedProgramMember.Id,
            actualRelationship.Relationship_Key_MVN__c
        );
    }

    /**
    * @description tests bulk insert
    */
    @isTest private static void testBulkInsertCreatingRelationships() {
        Program_MVN__c expectedProgram = [SELECT Id FROM Program_MVN__c LIMIT 1];
        Account expectedMember = [SELECT Id FROM Account WHERE LastName = 'Member' LIMIT 1];
        Account expectedPhysician = [SELECT Id FROM Account WHERE Credentials_vod__c = 'MD'];
        List<Program_Member_MVN__c> expectedProgramMembers = new List<Program_Member_MVN__c>();
        Map<String,Object> values = new Map<String,Object> {
            'Program_MVN__c' => expectedProgram.Id,
            'Member_MVN__c' => expectedMember.Id,
            'Physician_MVN__c' => expectedPhysician.Id
        };
        for(Program_Member_MVN__c programMember : new TestFactoryProgramMemberMVN().constructMany(200, values)) {
            expectedProgramMembers.add(programMember);
        }

        Test.startTest();
            insert expectedProgramMembers;
        Test.stopTest();

        List<Relationship_MVN__c> actualRelationships = [SELECT Id, Relationship_Key_MVN__c
                                                           FROM Relationship_MVN__c];
        System.assertNotEquals(null, actualRelationships);
        System.assertEquals(400, actualRelationships.size()); // two ways
    }
}