/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description tests for EligibilityEnginePovertyGuideHdlrMVN
 */
@isTest
private class EligibilityEnginePovertyGuideHdlrTestMVN {

    private static User caseManager;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static List<Eligibility_Engine_Rule_MVN__c> rules;
    private static Address_vod__c address;
    private static Application_MVN__c application;
    private static Eligibility_Engine_Rule_MVN__c rule;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        TestDataFactoryMVN.createPSSettings();

        System.runAs(caseManager) {
            program = TestDataFactoryMVN.createProgram();
            Account member = createMember();
            address = createShippingAddress(member);
            programMember = TestDataFactoryMVN.createProgramMember(program, member);
            application = createApplication();
            programMember.Active_Application_MVN__c = application.Id;
            update programMember;
            createPovertyGuideline();
            rule = createPovertyRule();
        }
    }

    static Account createMember() {
        Account member = TestDataFactoryMVN.createMember();
        member.NumberOfEmployees = 10;
        update member;
        return member;
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        return testApplication;
    }

    static Address_vod__c createShippingAddress(Account member) {
        TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();
        return addressFactory.createAddress(member, new Map<String,Object>{
            'Name' => 'Patient Address Line 1',
            'Address_line_2_vod__c' => 'Patient Address Line 2',
            'Shipping_vod__c' => true,
            'Primary_vod__c' => true,
            'City_vod__c' => 'Patient City',
            'Zip_vod__c' => 'Patient 11111',
            'State_vod__c' => 'IL'
        });
    }

    static void createPovertyGuideline() {
        Poverty_Guidelines_Threshold_MVN__c povertyGuideline = new Poverty_Guidelines_Threshold_MVN__c(
            Name = 'poverty test 1',
            Household_Number_MVN__c = '1',
            State_MVN__c = 'Other',
            Threshold_MVN__c = 15000
        );
        insert povertyGuideline;
    }

    static Eligibility_Engine_Rule_MVN__c createPovertyRule() {
        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Test Rule One',
            Result_MVN__c = EligibilityEngineMVN.RULEDENIED,
            Type_MVN__c = 'Poverty_Guidelines_MVN',
            Program_MVN__c = program.Id,
            Source_Field_API_Name_MVN__c = 'Total_Annual_Income_MVN__c',
            Source_Object_API_Name_MVN__c = 'Application_MVN__c',
            Active_MVN__c = true
        );
        insert expectedRule;
        return expectedRule;
    }


    @isTest
    private static void testDeniedBecauseNoData() {

        address.State_vod__c = null;
        update address;

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Rule One', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    private static void testDeniedNoPovertyGuideline() {

        Income_MVN__c incomeRecord = new Income_MVN__c(
            Application_MVN__c = application.Id,
            No_of_Dependents_MVN__c = 2,
            Total_Annual_Income_MVN__c = 10000,
            Program_Member_MVN__c = programMember.Id
        );
        insert incomeRecord;


        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Rule One', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    private static void testDeniedHighIncome() {

        Income_MVN__c incomeRecord = new Income_MVN__c(
            Application_MVN__c = application.Id,
            No_of_Dependents_MVN__c = 1,
            Salary_Wages_MVN__c  = 20000,
            Income_Type_MVN__c = 'Annual',
            Program_Member_MVN__c = programMember.Id
        );
        insert incomeRecord;

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Rule One', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    private static void testPassedWithIncrement() {

        Income_MVN__c incomeRecord = new Income_MVN__c(
            Application_MVN__c = application.Id,
            No_of_Dependents_MVN__c = 1,
            Total_Annual_Income_MVN__c = 20000,
            Program_Member_MVN__c = programMember.Id
        );
        insert incomeRecord;

        TestFactoryProductMVN productFactory = new TestFactoryProductMVN();
        Product_MVN__c product = productFactory.create(
            new Map<String, Object>{ 'Program_MVN__c' => program.Id}
        );

        insert new Product_Eligibility_Rule_MVN__c(
            Product_Family_MVN__c = product.Id,
            Eligibility_Engine_Rule_MVN__c = rule.Id,
            Poverty_Guideline_Increment_MVN__c = 200
        );

        insert new Prescription_MVN__c(
            Application_MVN__c = application.Id,
            Program_Member_MVN__c = programMember.Id,
            Product_MVN__c = product.Id
        );

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Rule One', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    private static void testPassed() {

        Income_MVN__c incomeRecord = new Income_MVN__c(
            Application_MVN__c = application.Id,
            No_of_Dependents_MVN__c = 1,
            Total_Annual_Income_MVN__c = 10000,
            Program_Member_MVN__c = programMember.Id
        );
        insert incomeRecord;

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Rule One', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    private static void testDeniedNoStateNoIncrement() {

        address.State_vod__c = 'HI';
        update address;

        Income_MVN__c incomeRecord = new Income_MVN__c(
            Application_MVN__c = application.Id,
            No_of_Dependents_MVN__c = 1,
            Total_Annual_Income_MVN__c = 10000,
            Program_Member_MVN__c = programMember.Id
        );
        insert incomeRecord;

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(programMember.Active_Application_MVN__c)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals('Test Rule One', actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
        }
    }
}