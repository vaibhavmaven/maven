@isTest   // make this class visible to Test Runtime. 
public class PAP_SendNotificationBatchMock implements HttpCalloutMock {
    public String fileName {get;set;}
    public PAP_SendNotificationBatchMock( String fileName ){
        this.fileName = fileName;
    }
    // we need to implement the "respond" method of interface.
    public HttpResponse respond(HttpRequest httpReq)
    {
        HttpResponse httpRes = new HttpResponse(); 
        httpRes.setHeader('Accept', 'application/json');
        httpRes.setHeader('Content-Type', 'application/json');
        httpRes.setBody('{"access_id":"106734","access_pwd":"Summer2018","action":"Queue_Fax","sCallerID":"8332844558","sSenderEmail":"noreply@knipper.com","sFaxType":"SINGLE","sToFaxNumber":"43212345890","sFileName_1":"'+fileName+'","sFileContent_1":"VGVzdGVy"}');
        httpRes.setStatusCode(200); 
        return httpRes; 
    }
}