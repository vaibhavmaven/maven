/**
 * @author      Mavens
 * @group       RandomEnrollmentAuditMVN
 * @description Implements batch job to be run each hour that will count up all audit cases created
 *              in the last hour for each program and randomly select an amount that ensures a
 *              minimum threshhold of audit cases are audited as confuigured for the program.
 *              All other audit cases should be set to 'System Cancelled' and the related application
 *              should be set to Audit_Result_MVN__c = 'System Cancelled'.

 *              Example, a program may define it's threshold as 50%. If 10 audit cases are created
 *              for the last hour and 2 have been closed then the batch job will randomly close 5
 *              more cases, leaving 3 open, ensuring that 50% of the cases are audited.
 */
global class RandomEnrollmentAuditMVN implements Database.Batchable<sObject>, Schedulable, Database.Stateful {

    private DateTime lowerBound;
    private DateTime upperBound;

    public static final String BATCH_SIZE_EXCEPTION = 'Batch size must always be 1';
    /*
     * @description Contstuctor
     */
    global RandomEnrollmentAuditMVN() {
        Patient_Service_Settings_MVN__c settings = getSettings();
        lowerBound = settings.Audit_Batch_Job_Lower_Bound_MVN__c;
        upperBound = settings.Audit_Batch_Job_Upper_Bound_MVN__c;

        if (lowerBound == null && upperBound == null) {
            this.createBounds(settings);
        }
    }

    public static void run() {
        Database.executeBatch(new RandomEnrollmentAuditMVN(), 1);
    }

    /*
     * @description implment the execute for the schedulale context. All runs should be done in a
     *              batch size of 1 program. The execute method will find all cases for that program
     * @param context - The SchedulableContext for the Schedulable method
     */
    global void execute(SchedulableContext context) {
        Database.executeBatch(new RandomEnrollmentAuditMVN(), 1);
    }

    /*
     * @description helper method to schedule this batch job to run hourly. Execute Anonymous:
     *              RandomEnrollmentAuditMVN.scheduleHourlyRun();
     * @param jobName - The name of the job too schedule, passing null will default value
     */
    global static void scheduleHourlyRun(String jobName) {
        jobName = String.isBlank(jobName) ? 'Random Enrollment Audit' : jobName;

        System.schedule(
            jobName,
            '0 0 * * * ?',
            new RandomEnrollmentAuditMVN()
        );
    }

    /*
     * @description Determine the beginning and end of the previous hour
     */
    private void createBounds(Patient_Service_Settings_MVN__c settings) {
        DateTime hourAgo = DateTime.now().addHours(-1);
        settings.Audit_Batch_Job_Lower_Bound_MVN__c = DateTime.newInstance(
            hourAgo.year(),
            hourAgo.month(),
            hourAgo.day(),
            hourAgo.hour(),
            0,
            0
        );
        settings.Audit_Batch_Job_Upper_Bound_MVN__c = settings.Audit_Batch_Job_Lower_Bound_MVN__c.addHours(1);
        update settings;
    }

    /*
     * @description Find all programs which require less than 100% audit
     * @param BC - the BatchableContext for the Database.Batchable start method
     * @return Database.QueryLocator defining scope of the batch job
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([
            SELECT Id, Daily_Audit_MVN__c FROM Program_MVN__c WHERE Daily_Audit_MVN__c < 100
        ]);
	}

    /*
     * @description Exeucte batch. Determine how many cases were created in the time period, determine
     *              how many should be audited based on program configuration. If enough cases have
     *              been audited already close the rest and set them to 'System Cancelled' and set
     *              the application result to System Cancelled as well. If not enough cases have been
     *              closed, only cancel enough cases to ensure the remaining cases will cross the
     *              defined threshold for %audit.
     * @param BC - Database.BatchableContext BC
     * @param scope - List<Program_Member_MVN__c> expects a list of programs of size 1
     */
    global void execute(Database.BatchableContext BC, List<Program_MVN__c> scope) {
        if (scope.size() > 1) {
            throw new PatientServicesMVNException( BATCH_SIZE_EXCEPTION );
        }

        Savepoint sp = Database.setSavePoint();
        Program_MVN__c currentProgram = scope[0];

        try {
            List<Case> auditCases = getCasesToAudit( currentProgram.Id );

            List<Case> openAuditCases = new List<Case>();
            for (Case auditCase : auditCases) {
                if (!auditCase.IsClosed) {
                    openAuditCases.add(auditCase);
                }
            }

            Integer totalCaseCount = auditCases.size();
            Integer closedCaseCount = totalCaseCount - openAuditCases.size();
            Decimal auditPercentage = currentProgram.Daily_Audit_MVN__c;

            Integer openCasesNeeded;
            if (auditPercentage == 0) {
                openCasesNeeded = 0;
            } else {
                Integer threshhold = Integer.valueOf (
                    (totalCaseCount * ((100 - auditPercentage) / 100)).round(System.RoundingMode.UP)
                );
                openCasesNeeded = threshhold - closedCaseCount;
            }

            if (openCasesNeeded <  0) {
                return;
            }

            List<Case> auditCasesToCancel = getCasesToCancel(openAuditCases, openCasesNeeded);

            List<Application_MVN__c> applicationsToUpdate = new List<Application_MVN__c>();

            for (Case auditCase : auditCasesToCancel) {
                auditCase.Status = Label.System_Cancelled_MVN;
                if (auditCase.Application_MVN__c != null) {
                    applicationsToUpdate.add (
                        new Application_MVN__c (
                            Id = auditCase.Application_MVN__c,
                            Audit_Result_MVN__c = Label.System_Cancelled_MVN
                        )
                    );
                }
            }

            update auditCasesToCancel;
            update applicationsToUpdate;
        } catch (Exception e) {
            Database.rollback(sp);
            insert new System_Log_MVN__c ( Logged_Message_MVN__c = e.getMessage() + '\n\n' + e.getStacktraceString());
            throw e;
        }
	}

    /*
     * @description implements the finish method of the batchable interface
     */
    global void finish(Database.BatchableContext BC) {
        Patient_Service_Settings_MVN__c settings = getSettings();
        settings.Audit_Batch_Job_Lower_Bound_MVN__c = null;
        settings.Audit_Batch_Job_Upper_Bound_MVN__c = null;
        update settings;
	}

    private Patient_Service_Settings_MVN__c getSettings() {
        return [
            SELECT
                Id,
                Audit_Batch_Job_Lower_Bound_MVN__c,
                Audit_Batch_Job_Upper_Bound_MVN__c
            FROM
                Patient_Service_Settings_MVN__c
            LIMIT
                1
        ];
    }

    /*
     * @description return all audit cases in the given timeline for the current program
     * @param programId - Id of the program to query cases for
     * @return List<Case> audit cases in the specified time frame for the current program
     */
    private List<Case> getCasesToAudit(Id programId) {
        Patient_Service_Settings_MVN__c settings = getSettings();
        return [
            SELECT
                Id,
                Status,
                IsClosed,
                Application_MVN__c
            FROM
                Case
            WHERE
                Program_Member_MVN__r.Program_MVN__c = :programId
            AND
                CreatedDate >= :settings.Audit_Batch_Job_Lower_Bound_MVN__c
            AND
                CreatedDate < :settings.Audit_Batch_Job_Upper_Bound_MVN__c
            AND
                Is_Audit_Case_MVN__c = true
        ];
    }

    /*
     * @description given a list of open cases and a number of cases needed randomly choose the
     *              correct amount to close and return those back to the caller
     * @param openAuditCases - list of open cases
     * @param openCasesNeeded - Integer representing the number of open cases needed
     * @return List<Case> a list of open cases to cancel
     */
    private List<Case> getCasesToCancel(List<Case> openAuditCases, Integer openCasesNeeded) {
        List<Case> casesToCancel = new List<Case>();
        while (openAuditCases.size() > openCasesNeeded) {
            Integer indexToRemove = Math.mod(
                Math.round( Math.random()*1000 ),
                openAuditCases.size()
            );

            casesToCancel.add(
                openAuditCases.remove( indexToRemove )
            );
        }
        return casesToCancel;
    }
}