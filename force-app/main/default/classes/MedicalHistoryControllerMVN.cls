/*
*   MedicalHistoryControllerMVN
*   Created By:     Paul Battisson
*   Created Date:   April 20, 2016
*   Modified By:     Florian Hoehn
*   Modified Date:   June 10, 2016
*   Description:    Provides controller functionality for the MedicalHistoryMVN page.
*/
global with sharing class MedicalHistoryControllerMVN {
    public List<String> orderedRecordTypeNames { get; private set; }
    public Map<String, RecordType> recordTypeMap {get; private set;}
    public String selectedRecordTypeDeveloperName { get; set; }

    public MedicalHistoryItemMVN medicalHistory {
        get {
            if(this.medicalHistory == null || this.medicalHistory.item.Id != null || this.medicalHistory.item.RecordTypeId != this.recordTypeMap.get(this.selectedRecordTypeDeveloperName).Id) {
                if(this.medicalHistory != null && this.medicalHistory.item.Id != null) {
                    if(this.medicalHistoryList.size() == 1 && this.medicalHistoryList.get(0).item.Id == this.medicalHistory.item.Id) {
                    } else {
                        this.medicalHistoryList = null; // reset list to make sure it gets reloaded after re-render
                    }
                }
                if(this.recordTypeMap != null && this.selectedRecordTypeDeveloperName != null && this.recordTypeMap.get(this.selectedRecordTypeDeveloperName) != null) {
                    this.medicalHistory = new MedicalHistoryItemMVN(
                        new Medical_History_MVN__c(
                            RecordTypeId = this.recordTypeMap.get(this.selectedRecordTypeDeveloperName).Id,
                            RecordType = this.recordTypeMap.get(this.selectedRecordTypeDeveloperName),
                            Program_Member_MVN__c = this.programMemberId
                        )
                    );
                    this.medicalHistory.changeEditMode(); // set to true
                }
            }
            return this.medicalHistory;
        }
        set;
    }

    public List<MedicalHistoryItemMVN> medicalHistoryList {
        get {
            if(this.medicalHistoryList == null || this.medicalHistoryList.isEmpty() || this.medicalHistoryList.get(0).item.RecordType.DeveloperName != this.selectedRecordTypeDeveloperName) {
                this.medicalHistoryList = new List<MedicalHistoryItemMVN>();
                for(Medical_History_MVN__c item : this.getMedicalHistoryRecords) {
                    this.medicalHistoryList.add(new MedicalHistoryItemMVN(item));
                }
            } else { // this ensures record type wide calculations (i.e. current therapy checkbox) refresh the list items without refreshing the wrapper
                for(Medical_History_MVN__c item : this.getMedicalHistoryRecords) {
                    for(MedicalHistoryItemMVN medItem : this.medicalHistoryList) {
                        if(medItem.item.Id == item.Id) {
                            if(UtilitiesMVN.medicalHistorySaveSuccess) {
                                medItem.item = item;
                            }
                        }
                    }
                }
            }
            return this.medicalHistoryList;
        }
        set;
    }

    private List<Medical_History_MVN__c> getMedicalHistoryRecords {
        get {
            if (this.selectedRecordTypeDeveloperName == null) {
                return new List<Medical_History_MVN__c>();
            }
            Set<String> fields = new Set<String>();
            for(Schema.FieldSetMember fieldSetMember : Schema.SObjectType.Medical_History_MVN__c.fieldSets.getMap().get(this.selectedRecordTypeDeveloperName).getFields()) {
                fields.add(fieldSetMember.getFieldPath());
            }
            for(Schema.FieldSetMember fieldSetMember : Schema.SObjectType.Medical_History_MVN__c.fieldSets.getMap().get('readOnly').getFields()) {
                fields.add(fieldSetMember.getFieldPath());
            }
            Id recordTypeIdToBeSelected = this.recordTypeMap.get(this.selectedRecordTypeDeveloperName).Id;
            return Database.query('SELECT RecordType.DeveloperName, ' + String.join(new List<String>(fields), ', ') +
                                  ' FROM Medical_History_MVN__c ' +
                                  'WHERE RecordTypeId = :recordTypeIdToBeSelected AND Program_Member_MVN__c = :programMemberId ' +
                                  'ORDER BY CreatedDate DESC LIMIT :NUMBER_OF_MEDICAL_HISTORY_ROWS');
        }
    }
    private static final Integer NUMBER_OF_MEDICAL_HISTORY_ROWS = (Patient_Service_Settings_MVN__c.getInstance().Number_Of_Medical_History_Rows_Displayed__c != null ? Integer.valueOf(Patient_Service_Settings_MVN__c.getInstance().Number_Of_Medical_History_Rows_Displayed__c) : 20);
    private Id programMemberId;

    public MedicalHistoryControllerMVN() {
        try{
            this.setProgramMemberIdFromPageParameters();
            this.setOrderedRecordTypeList();
            if (!this.orderedRecordTypeNames.isEmpty()) {
                this.setRecordTypeMap();
            }
        } catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                String.format(Label.MedicalHistory_ContactAdmin, new String[]{ex.getMessage()})));  // make this a label
        }

    }

    private void setProgramMemberIdFromPageParameters() {
        this.programMemberId = ApexPages.currentpage().getparameters().get('pmId');
        if(this.programMemberId == null) {
            String caseId = ApexPages.currentpage().getparameters().get('caseId');
            this.programMemberId = [SELECT Program_Member_MVN__c FROM Case WHERE Id = :caseId].Program_Member_MVN__c;
        }
    }

    private void setOrderedRecordTypeList() {
        this.orderedRecordTypeNames = new List<String>();
        Program_Member_MVN__c pm = [SELECT Program_MVN__c, Program_MVN__r.Name
                          FROM Program_Member_MVN__c
                         WHERE Id =: programMemberId];
        Id programId = pm.Program_MVN__c;
        for(Medical_History_Type_MVN__c type : [SELECT Program_MVN__c, Record_Type_Name_MVN__c, Order_MVN__c
                                                  FROM Medical_History_Type_MVN__c
                                                 WHERE Program_MVN__c = :programId
                                              ORDER BY Order_MVN__c ASC]) {
            this.orderedRecordTypeNames.add(type.Record_Type_Name_MVN__c);
        }
    }

    private void setRecordTypeMap() {
        this.recordTypeMap = new Map<String, RecordType>();
        Set<Id> recordTypeIDs = new Set<Id>();
        for(RecordTypeInfo info : Medical_History_MVN__c.SObjectType.getDescribe().getRecordTypeInfos()) {
            if(info.isAvailable() && !info.isMaster()) {
                recordTypeIDs.add(info.getRecordTypeId());
            }
        }
        for(RecordType recordType : [SELECT Name, DeveloperName FROM RecordType WHERE Id IN :recordTypeIDs]) {
            this.recordTypeMap.put(recordType.DeveloperName, recordType);
        }
        this.selectedRecordTypeDeveloperName = this.recordTypeMap.get(this.orderedRecordTypeNames.get(0)).DeveloperName;
    }

    public PageReference selectRecordType() {
        return null; // need this to use apex:param and set this.selectedRecordTypeDeveloperName
    }
}