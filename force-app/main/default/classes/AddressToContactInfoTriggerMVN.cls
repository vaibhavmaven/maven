/*
 * AddressToContactInfoTriggerMVN
 * Created By: Kyle Thornton
 * Created Date: 4/7/2017
 * Description: When an address record is inserted or updated manage the related contact info records
 *
 */
public with sharing class AddressToContactInfoTriggerMVN implements TriggerHandlerMVN.HandlerInterface {

    public void handle() {
        List<Contact_Information_MVN__c> newCiRecords = new List<Contact_Information_MVN__c>();
        for (Address_vod__c address : (List<Address_vod__c>) Trigger.new) {
            Contact_Information_MVN__c addressCi = createAddressCi(address);
            
            newCiRecords.add(
                addressCi 
            );

            if (String.isNotBlank(address.Phone_vod__c)) {
                newCiRecords.add(
                    createPhoneCi(address, 'Phone_vod__c', false)
                );
            }

            if (String.isNotBlank(address.Phone_2_vod__c)) {
                newCiRecords.add(
                    createPhoneCi(address, 'Phone_2_vod__c', false)
                );
            }

            if (String.isNotBlank(address.Fax_vod__c)) {
                newCiRecords.add(
                    createPhoneCi(address, 'Fax_vod__c', true)
                );
            }

            if (String.isNotBlank(address.Fax_2_vod__c)) {
                newCiRecords.add(
                    createPhoneCi(address, 'Fax_2_vod__c', true)
                );
            }

            ContactInfoSetValueFieldTriggerMVN.setAddress(addressCi, address);
        }

        upsert newCiRecords External_ID_MVN__c;
    }

    private Map<String, Id> ciRecordTypesByName {
        get {
            if (ciRecordTypesByName == null) {
                ciRecordTypesByName = new Map<String, Id>();
                for (RecordType rt : [SELECT Id,
                                             DeveloperName
                                        FROM RecordType
                                       WHERE SObjectType = 'Contact_Information_MVN__c']) {
                    ciRecordTypesByName.put(rt.DeveloperName, rt.Id);
                }
            }
            return ciRecordTypesByName;
        }
        set;
    }

    private Contact_Information_MVN__c createAddressCi(Address_vod__c address) {
        String ciLabel = '';
        if (address.Business_vod__c) {
            ciLabel = 'Business';
        } else if (address.Home_vod__c) {
            ciLabel = 'Home';
        }

        return new Contact_Information_MVN__c(
            Address_MVN__c = address.Id,
            Account_MVN__c = address.Account_vod__c,
            RecordTypeId   = ciRecordTypesByName.get('Address_MVN'),
            Label_MVN__c   = ciLabel,
            Primary_MVN__c = address.Primary_vod__c,
            Shipping_MVN__c = address.Shipping_vod__c,
            External_ID_MVN__c = 'Address-' + address.Id
        );
    }

    private Contact_Information_MVN__c createPhoneCi(Address_vod__c address, String relatedField, Boolean isFax) {

        return new Contact_Information_MVN__c(
            Address_MVN__c = address.Id,
            Account_MVN__c = address.Account_vod__c,
            RecordTypeId   = ciRecordTypesByName.get('Phone_MVN'),
            Label_MVN__c   = (isFax ? 'Fax' : null),
            Phone_MVN__c   = (String) address.get(relatedField),
            Related_Address_Phone_Field_MVN__c = relatedField,
            External_ID_MVN__c = relatedField + '-' + address.Id
        );
    }
}