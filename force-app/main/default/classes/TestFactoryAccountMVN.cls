/**
 * TestFactoryAccountMVN
 * Author: Kyle Thornton
 * Created Date: 2/17/2017
 * Description: This class is used to construct Account objects
 */
@isTest
public class TestFactoryAccountMVN {
    private TestFactorySObjectMVN objectFactory;

    public static Map<String, RecordType> accountRecordTypesByName {
        get {
            if (accountRecordTypesByName == null) {
                accountRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
                    accountRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return accountRecordTypesByName;
        }
        private set;
    }

    public TestFactoryAccountMVN() {
        objectFactory = new TestFactorySObjectMVN('Account', new Map<String, Object>());
    }

    public Account construct(Map<String,Object> valuesByField) {
        return (Account) objectFactory.constructSObject(valuesByField);
    }

    public List<Account> constructMany(Integer numOfAccounts, Map<String,Object> valuesByField) {
        return (List<Account>) objectFactory.constructSObjects(numOfAccounts, valuesByField);
    }

    public Account create(Map<String,Object> valuesByField) {
        return (Account) objectFactory.createSObject(valuesByField);
    }

    public List<Account> createMany(Integer numOfAccounts, Map<String,Object> valuesByField) {

        return (List<Account>) objectFactory.createSObjects(numOfAccounts, valuesByField);
    }

    public Account createPrescriber() {
        return createPrescriber(null);
    }

    public Account createPrescriber(Map<String, Object> fieldValues) {
        Map<String,Object> values = new Map<String,Object> {
            'Salutation' => 'Dr.',
            'FirstName' => 'Robert',
            'LastName' => 'Avery',
            'Suffix' => 'MD',
            'Credentials_vod__c' => 'MD',
            'RecordTypeId' => accountRecordTypesByName.get('Professional_vod').id
        };

        if (fieldValues != null) {
            values.putAll(fieldValues);
        }

        return create(values);
    }

    public Account createProvider() {
        return createProvider(null);
    }

    public Account createProvider(Map<String,Object> fieldValues) {
        Map<String,Object> values = new Map<String,Object> {
            'Name' => 'Test Injector Site',
            'RecordTypeId' => accountRecordTypesByName.get('Hospital_vod').id
        };

        if (fieldValues != null) {
            values.putAll(fieldValues);
        }

        return create(values);
    }

    public List<Account> createPatients(Integer numOfPatients) {
        List<Account> patients = new List<Account>();
        for (Integer i=0; i<numOfPatients; i++) {
            Map<String, Object> fieldValues = new Map<String, Object> {
                'FirstName' => 'Test',
                'LastName' => 'Patient' + i,
                'Gender_MVN__c' => 'M',                             
                'PersonBirthdate' => System.today(),
                'RecordTypeId' => accountRecordTypesByName.get('Patient_MVN').id
            };
            patients.add(construct(fieldValues));
        }
        insert patients;
        return patients;
    }

    public Account createContact(Map<String, Object> fieldValues) {
        Map<String, Object> values = new Map<String, Object> {
            'FirstName' => 'Sally',
            'LastName' => 'Caregiver',
            'RecordTypeId' => accountRecordTypesByName.get('Caregiver_MVN').id
        };

        if (fieldValues != null) {
            values.putAll(fieldValues);
        }

        return create(values);
    }

    public List<Account> createPersonAccounts(Integer numToCreate, String recordTypeName) {
        return createPersonAccounts(numToCreate, recordTypeName, new Map<String,Object>{});
    }

    public List<Account> createPersonAccounts(Integer numToCreate, String recordTypeName, Map<String,Object> fieldValues) {
        if (numToCreate > 200) {
            numToCreate = 200;
        }
        Id accountRecordTypeId = accountRecordTypesByName.get(recordTypeName).Id;
        List<Account> accounts = new List<Account>();
        accounts.add(new Account(FirstName='Jeffrey',MiddleName='Fred',LastName='Berry',
                                 Gender_MVN__c='M',Specialty_1_vod__c='Internal Medicine',
                                 Credentials_vod__c='None',Country_MVN__c='US', RecordTypeId=accountRecordTypeId));

        for (Integer counter = 1; counter<numToCreate; counter++) {
            Account acct = new Account(RecordTypeId=accountRecordTypeId,
                                       FirstName='First' + counter,
                                       MiddleName='Middle' + counter,
                                       LastName='Last' + counter,
                                       Gender_MVN__c=getRandomGender(),
                                       Specialty_1_vod__c=getRandomSpecialty(),
                                       Credentials_vod__c=getRandomCredentials(),
                                       Country_MVN__c='US');
            accounts.add(acct);
        }

        for (Account acct : accounts) {
            for (String field : fieldValues.keySet()) {
                acct.put(field, fieldValues.get(field));
            }
        }

        insert accounts;
        return accounts;
    }

    private Integer getRandomIndex(Integer max) {
        return (math.random() * max).intValue();
    }

    private String getRandomValue(List<String> strings) {
        return strings[getRandomIndex(strings.size())];
    }

    private String getRandomGender() {
        return getRandomValue(new List<String>{'F','M'});
    }

    private String getRandomSpecialty() {
        return getRandomValue(new List<String>{'Cardiology',
                                               'Emergency Medicine',
                                               'Family Medicine',
                                               'General Practice',
                                               'Internal Medicine',
                                               'Oncology',
                                               'Pediatrics',
                                               'Other'});
    }

    private String getRandomCredentials() {
        return getRandomValue(new List<String>{'DO','MD','PharmD','RN','RNP','None'});
    }
}