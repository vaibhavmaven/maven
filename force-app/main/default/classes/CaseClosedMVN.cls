/**
 * @author Mavens
 * @date 10/2018
 * @description When closing the case:
 *      - Lock saved notes
 * @group Case
 */ 
public with sharing class CaseClosedMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
     * @description handle
     */
    public void handle() {
        List<Id> closedCaseIds = new List<Id>();
        for (Case myCase : (List<Case>) trigger.new) {
            if (myCase.isClosed &&
                myCase.isClosed != ((Case)trigger.oldMap.get(myCase.Id)).isClosed) {
                closedCaseIds.add(myCase.Id);
            }
        }
        update getSavedNotes(closedCaseIds);
    }

    /**
     * Get Notes that are not locked
     * @param  caseIds list of case ids to filter notes linked to those cases
     * @return         list of not locked notes attached to the list of cases
     */
    private List<Case_Note_MVN__c> getSavedNotes(List<Id> caseIds) {
        List<Case_Note_MVN__c> savedNotes = [
            SELECT 
                Id, 
                Notes_MVN__c
            FROM 
                Case_Note_MVN__c
            WHERE
                Case_MVN__c IN :caseIds
            AND 
                Locked_MVN__c = false
        ];
        for (Case_Note_MVN__c myNote : savedNotes) {
            if (myNote.Notes_MVN__c != null) {
                myNote.Locked_MVN__c = true;
            }
        }
        return savedNotes;
    }
}