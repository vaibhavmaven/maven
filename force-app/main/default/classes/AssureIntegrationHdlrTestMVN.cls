/**
 * @author Mavens
 * @date August 2018
 * @group AssureIntegration
 * @description tests for AssureIntegrationHdlrMVN class
 */
@isTest
private class AssureIntegrationHdlrTestMVN {

    private static User caseManager;
    private static Account physician;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static Address_vod__c primaryAddress;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();

        System.runAs(caseManager) {
            program = createProgram();
            physician = createPhysician();
            primaryAddress = createPrimaryAddress(physician);
            programMember = createProgramMember();
        }
    }

    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        return programFactory.create(
            new Map<String,Object>{
                'Name' => 'A Program',
                'Program_Id_MVN__c' => 'Knipper_PAP_MVN',
                'Active_MVN__c' => true,
                'Job_No_MVN__c' => '12345'
            }
        );
    }

    static Account createPhysician() {
        TestFactoryAccountMVN accountTestFactory = new TestFactoryAccountMVN();
        Account physician = accountTestFactory.create(new Map<String, Object> {
            'RecordTypeId' => TestFactoryAccountMVN.accountRecordTypesByName.get('Professional_vod').Id,
            'FirstName' => 'Joel',
            'LastName' => 'Atwood',
            'Credentials_vod__c' => 'MD',
            'DEA_MVN__c' => 'DEA123',
            'SLN_MVN__c' => 'SLN123'
        });
        return physician;
    }

    static Address_vod__c createPrimaryAddress(Account physician) {
        TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();
        return addressFactory.createAddress(physician, new Map<String,Object>{
            'Name' => 'Physician Address Line 1',
            'Address_line_2_vod__c' => 'Physician Address Line 2',
            'Shipping_vod__c' => true,
            'Primary_vod__c' => true,
            'City_vod__c' => 'Physician City',
            'Zip_vod__c' => '11111',
            'State_vod__c' => 'DE'
        });
    }

    static Program_Member_MVN__c createProgramMember() {

        Program_Member_MVN__c programMemberTest = 
            TestDataFactoryMVN.createProgramMember(program, TestDataFactoryMVN.createMember());

        programMemberTest.Physician_MVN__c = physician.Id;
        update programMemberTest;

        return [
            SELECT 
                Program_MVN__r.Job_No_MVN__c,
                Physician_MVN__r.FirstName,
                Physician_MVN__r.LastName,
                Physician_MVN__r.MiddleName,
                Physician_MVN__r.Suffix,
                Physician_MVN__r.Credentials_vod__c,
                Physician_MVN__r.SLN_MVN__c,
                Physician_MVN__r.DEA_MVN__c
            FROM 
                Program_Member_MVN__c
            WHERE 
                Id = :programMemberTest.Id
        ];

    }

    @isTest
    private static void testPassed() {
        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class, 
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_PASSED));

        System.runAs(caseManager) {

            Test.startTest();
            AssureIntegrationHdlrMVN assureHandler = new AssureIntegrationHdlrMVN('uname', 'pass', 'SLN Order');
            AssureIntegrationHdlrMVN.AssureResponseWrapper resp = 
                assureHandler.callAssureWebService(programMember, primaryAddress, '1');
            Test.stopTest();

            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, resp.result);
            System.assertEquals('Passed E', resp.resultDescription);
        }
    }

    @isTest
    private static void testDenied() {
        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class, 
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_DENIED));

        System.runAs(caseManager) {

            Test.startTest();
            AssureIntegrationHdlrMVN assureHandler = new AssureIntegrationHdlrMVN('uname', 'pass', 'SLN Order');
            AssureIntegrationHdlrMVN.AssureResponseWrapper resp = 
                assureHandler.callAssureWebService(programMember, primaryAddress, '1');
            Test.stopTest();

            System.assertEquals(EligibilityEngineMVN.RULEDENIED, resp.result);
            System.assertEquals('Denied 9', resp.resultDescription);
        }
    }

    @isTest
    private static void testMissingInfo() {
        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class, 
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_MISSING_INFO));

        System.runAs(caseManager) {

            Test.startTest();
            AssureIntegrationHdlrMVN assureHandler = new AssureIntegrationHdlrMVN('uname', 'pass', 'SLN Order');
            AssureIntegrationHdlrMVN.AssureResponseWrapper resp = 
                assureHandler.callAssureWebService(programMember, primaryAddress, '1');
            Test.stopTest();

            System.assertEquals(EligibilityEngineMVN.RULEMISSINGINFO, resp.result);
            System.assertEquals('Missing Information DEA-C', resp.resultDescription);
        }
    }

    @isTest
    private static void testMissingInfoAndDenied() {
        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class, 
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_MISSING_INFO_AND_DENIED));

        System.runAs(caseManager) {
            
            Test.startTest();
            AssureIntegrationHdlrMVN assureHandler = new AssureIntegrationHdlrMVN('uname', 'pass', 'SLN Order');
            AssureIntegrationHdlrMVN.AssureResponseWrapper resp = 
                assureHandler.callAssureWebService(programMember, primaryAddress, '1');
            Test.stopTest();

            System.assertEquals(EligibilityEngineMVN.RULEDENIED, resp.result);
            System.assertEquals('Missing Information DEA-C;' +
                            'Denied 9', 
                            resp.resultDescription);
        }
    }

    @isTest
    private static void testNoCode() {
        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class, 
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_NO_CODE));

        System.runAs(caseManager) {
            
            Test.startTest();
            AssureIntegrationHdlrMVN assureHandler = new AssureIntegrationHdlrMVN('uname', 'pass', 'SLN Order');
            AssureIntegrationHdlrMVN.AssureResponseWrapper resp = 
                assureHandler.callAssureWebService(programMember, primaryAddress, '2');
            Test.stopTest();

            System.assertEquals(EligibilityEngineMVN.RULEDENIED, resp.result);
            System.assertEquals(Label.No_mapping_found_for_Assure_codes_MVN + '(NO-CODE)', resp.resultDescription);
        }
    }

    @isTest
    private static void testIsDataValidSuccess() {

        System.runAs(caseManager) {
            
            Test.startTest();
            AssureIntegrationHdlrMVN assureHandler = new AssureIntegrationHdlrMVN('uname', 'pass', 'SLN Order');
            List<String> missingFields = assureHandler.getMissingFields(
                programMember, new List<Address_vod__c>{primaryAddress}, '1'
            );
            Test.stopTest();

            System.assert(missingFields.isEmpty());
        }
    }

    @isTest
    private static void testIsDataValidNoAddress() {

        System.runAs(caseManager) {
            
            Test.startTest();
            AssureIntegrationHdlrMVN assureHandler = new AssureIntegrationHdlrMVN('uname', 'pass', 'SLN Order');
            List<String> missingFields = assureHandler.getMissingFields(
                programMember, new List<Address_vod__c>(), '1'
            );
            Test.stopTest();

            System.assertEquals(1,missingFields.size());
           // System.assertEquals(SObjectType.Address_vod__c.getLabel(), missingFields.get(0));
            System.assertEquals(' ' + System.Label.Missing_Assure_Mandatory_Address_Field_JKC, missingFields.get(0));
        }
    }


    @isTest
    private static void testIsDataValidMissingFields() {
        programMember.Program_MVN__r.Job_No_MVN__c= null;
        programMember.Physician_MVN__r.FirstName = null;
        programMember.Physician_MVN__r.LastName = null;
        programMember.Physician_MVN__r.SLN_MVN__c = null;
        primaryAddress.Name = null;
        primaryAddress.City_vod__c = null;
        primaryAddress.State_vod__c = null;
        primaryAddress.Zip_vod__c = null;

        System.runAs(caseManager) {
            
            Test.startTest();
            AssureIntegrationHdlrMVN assureHandler = new AssureIntegrationHdlrMVN('uname', 'pass', 'SLN Order');
            List<String> missingFields = assureHandler.getMissingFields(
                programMember, new List<Address_vod__c>{primaryAddress}, '1'
            );
            Test.stopTest();

            System.assertEquals(8,missingFields.size());
        }
    }

    @isTest
    private static void testIsDataValidMissingDEAAndDEASchedule() {
        programMember.Physician_MVN__r.DEA_MVN__c = null;
        programMember.Physician_MVN__r.Credentials_vod__c = null;

        System.runAs(caseManager) {
            
            Test.startTest();
            AssureIntegrationHdlrMVN assureHandler = new AssureIntegrationHdlrMVN('uname', 'pass', 'DEA Order');
            List<String> missingFields = assureHandler.getMissingFields(
                programMember, new List<Address_vod__c>{primaryAddress}, null
            );
            Test.stopTest();

            System.assertEquals(3,missingFields.size());
        }
    }
}