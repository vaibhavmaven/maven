/**
 * @author Mavens
 * @group Network API
 * @description Test for the class DCRTriggerHandlerMVN
 */
@isTest
private class DCRTriggerHandlerTestMVN {

    /**
     * Bulk number
     */
    static final Integer BULK_TEST = 200;

    /**
     * Admin user used for tests
     */
    static User adminUser;

    /**
     * Test user used for tests
     */
    static User testUser;

    /**
     * Account Test Factory
     */
    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();

    /**
     * Address Test Factory
     */
    static TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();

    /**
     * Static block to initializes default tests data
     */
    static {
        createUsers();

        initializeDCRSettings();
    }

    static void createUsers() {
        TestFactoryUserMVN testUserFactory = new TestFactoryUserMVN();
        adminUser = testUserFactory.createAdminUser();
        testUser = testUserFactory.create();
    }

    static void initializeDCRSettings() {
        DCRUtilityMVN dcrUtility = new DCRUtilityMVN();
        DCRFieldSettingsMVN.setMockList(dcrUtility.buildMockFieldSettingsForTest());
        DCRGlobalSettingMVN.setMock(dcrUtility.buildMockDCRSettingsForTest());
        TestFactoryDCRSettingsMVN.setMocks();
    }

    @isTest static void testAccountDCRInsert() {
        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ACCOUNT_MOCK);

        System.runAs(testUser) {
            Test.startTest();
            records = createHCPs(false);
            Test.stopTest();
        }

        checkDCRCreated(dcrSetting, records);
    }

    @isTest static void testAccountDCRInsertWithDCROverride() {
        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ACCOUNT_MOCK);

        System.runAs(testUser) {
            Test.startTest();
            records = createHCPs(true);
            Test.stopTest();
        }

        checkDCRNotCreated(dcrSetting, records);
    }

    @isTest static void testAccountDCRUpdate() {
        List<SObject> records = createHCPs(true);
        records = getRecords(records);

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ACCOUNT_MOCK);

        List<SObject> newRecords;

        System.runAs(testUser) {
            Test.startTest();
            newRecords = updateRecords(records, dcrSetting);
            Test.stopTest();
        }

        checkDCRCreated(dcrSetting, newRecords);
        checkRecordRevertChanges(newRecords, new Map<Id, SObject>(records));
    }

    @isTest static void testAddressDCRInsert() {
        List<Account> hcps = createHCPs(true);

        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ADDRESS_MOCK);

        System.runAs(testUser) {
            Test.startTest();
            records = createAddresses(hcps, false);
            Test.stopTest();
        }

        checkDCRCreated(dcrSetting, records);
    }

    @isTest static void testAddressDCRInsertWithDCROverride() {
        List<Account> hcps = createHCPs(true);

        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ADDRESS_MOCK);

        System.runAs(testUser) {
            Test.startTest();
            records = createAddresses(hcps, true);
            Test.stopTest();
        }

        checkDCRNotCreated(dcrSetting, records);
    }

    @isTest static void testAddressDCRUpdate() {
        List<Account> hcps = createHCPs(true);
        List<SObject> records = createAddresses(hcps, true);
        records = getRecords(records);

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.ADDRESS_MOCK);

        List<SObject> newRecords;

        System.runAs(testUser) {
            Test.startTest();
            newRecords = updateRecords(records, dcrSetting);
            Test.stopTest();
        }

        checkDCRCreated(dcrSetting, records);
        checkRecordRevertChanges(newRecords, new Map<Id, SObject>(records));
    }

    @isTest static void testAffiliationDCRInsert() {
        List<Account> hcos = createHCOs(true);
        List<Account> hcps = createHCPs(true);

        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.AFFILIATION_MOCK);

        System.runAs(testUser) {
            Test.startTest();
            records = createAffiliations(hcps, hcos, false);
            Test.stopTest();
        }

        checkDCRCreated(dcrSetting, records);
    }

    @isTest static void testAffiliationDCRInsertWithDCROverride() {
        List<Account> hcos = createHCOs(true);
        List<Account> hcps = createHCPs(true);

        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.AFFILIATION_MOCK);

        System.runAs(testUser) {
            Test.startTest();
            records = createAffiliations(hcps, hcos, true);
            Test.stopTest();
        }

        checkDCRNotCreated(dcrSetting, records);
    }

    @isTest static void testLicenseDCRInsert() {
        List<Account> hcps = createHCPs(true);
        List<Address_vod__c> addresses = createAddresses(hcps, true);

        List<SObject> records;

        DCR_Setting_MVN__mdt dcrSetting = getDCRSetting(TestFactoryDCRSettingsMVN.LICENSE_MOCK);

        System.runAs(testUser) {
            Test.startTest();
            records = createLicenses(hcps, addresses, false);
            Test.stopTest();
        }

        checkDCRCreated(dcrSetting, records);
    }

    static DCR_Setting_MVN__mdt getDCRSetting(Map<String, Object> dcrSettingsMap) {
        DCRSettingsMVN dcrSetting = new DCRSettingsMVN(String.valueOf(dcrSettingsMap.get('SObject_Name_MVN__c')));

        System.assertNotEquals(null, dcrSetting.setting, 'DCR Setting is not configured.');

        return dcrSetting.setting;
    }

    static List<Account> createHCOs(Boolean dcrOverride) {
        List<Account> hcos = new List<Account>();

        for (Integer index = 0; index < BULK_TEST; index++) {
            Map<String, Object> fieldValues = new Map<String, Object> {
                'Name' => 'HCO' + index,
                'DCR_Override_MVN__c' => dcrOverride,
                'RecordTypeId' => TestFactoryAccountMVN.accountRecordTypesByName.get('Hospital_vod').id
            };

            hcos.add(accountFactory.construct(fieldValues));
        }

        insert hcos;

        return hcos;
    }

    static List<Account> createHCPs(Boolean dcrOverride) {
        List<Account> hcps = new List<Account>();

        for (Integer index = 0; index < BULK_TEST; index++) {
            Map<String, Object> fieldValues = new Map<String, Object> {
                'FirstName' => 'DR',
                'LastName' => 'HCP' + index,
                'DCR_Override_MVN__c' => dcrOverride,
                'RecordTypeId' => TestFactoryAccountMVN.accountRecordTypesByName.get('Professional_vod').id
            };

            hcps.add(accountFactory.construct(fieldValues));
        }

        insert hcps;

        return hcps;
    }

    static List<Affiliation_MVN__c> createAffiliations(List<Account> hcps, List<Account> hcos, Boolean dcrOverride) {
        List<Affiliation_MVN__c> affiliations = new List<Affiliation_MVN__c>();

        for (Integer index = 0; index < hcos.size(); index++) {
            affiliations.add(new Affiliation_MVN__c(
                Parent_Account_MVN__c = hcos[index].Id,
                Child_Account_MVN__c = hcps[index].Id,
                DCR_Override_MVN__c = dcrOverride
            ));
        }

        insert affiliations;

        return affiliations;
    }

    static List<Address_vod__c> createAddresses(List<Account> accs, Boolean dcrOverride) {
        List<Address_vod__c> addresses = new List<Address_vod__c>();

        for (Account acc : accs) {
            addresses.add(new Address_vod__c(
                Account_vod__c = acc.Id,
                Name = 'Address ' + acc.Id,
                DCR_Override_MVN__c = dcrOverride
            ));
        }

        insert addresses;

        return addresses;
    }

    static List<License_MVN__c> createLicenses(List<Account> hcps, List<Address_vod__c> addresses, Boolean dcrOverride) {
        List<License_MVN__c> licenses = new List<License_MVN__c>();

        for (Integer index = 0; index < hcps.size(); index++) {
            licenses.add(new License_MVN__c(
                Account_MVN__c = hcps[index].Id,
                Address_MVN__c = addresses[index].Id,
                DCR_Override_MVN__c = dcrOverride
            ));
        }

        insert licenses;

        return licenses;
    }

    static List<SObject> getRecords(List<SObject> records) {
        String sobjectName = records[0].getSobjectType().getDescribe().getName();

        Map<String, Schema.SObjectField> apiToLabel = records[0].getSobjectType().getDescribe().fields.getMap();

        List<String> fieldNames = getValidFieldNamesForRecord(sobjectName, apiToLabel);

        String fieldNamesConcatenated = String.join(fieldNames, ',');

        String query = 'SELECT ' + fieldNamesConcatenated + ' FROM ' + sobjectName + ' WHERE Id IN :records';

        return Database.query(query);
    }

    static List<Data_Change_Request_MVN__c> checkDCRCreated(DCR_Setting_MVN__mdt dcrSetting, List<SObject> records) {
        List<Id> recordIds = new List<Id>();

        for (SObject record : records) {
            System.assertNotEquals(null, record.Id, 'Record Id for DCR cannot be null.');
            recordIds.add(record.Id);
        }

        String recordIdFieldName = String.IsNotBlank(dcrSetting.Relationship_SObject_Id_Field_MVN__c) ?
                dcrSetting.Relationship_SObject_Id_Field_MVN__c :
                dcrSetting.SObject_Field_Id_MVN__c;

        String query = 'SELECT Id, Parent_Data_Change_Request_MVN__c FROM Data_Change_Request_MVN__c WHERE ' + recordIdFieldName + ' IN :recordIds';

        List<Data_Change_Request_MVN__c> dcrs = (List<Data_Change_Request_MVN__c>) Database.query(query);

        System.assert(!dcrs.isEmpty(), 'DCRs were not created.');

        System.assertEquals(records.size(), dcrs.size());

        return dcrs;
    }

    static void checkDCRNotCreated(DCR_Setting_MVN__mdt dcrSetting, List<SObject> records) {
        List<Id> recordIds = new List<Id>();

        String recordIdFieldName = String.IsNotBlank(dcrSetting.Relationship_Field_Name_MVN__c) ?
                dcrSetting.Relationship_Field_Name_MVN__c :
                'Id';

        for (SObject record : records) {
            Id recordId = String.valueOf(record.get(recordIdFieldName));
            System.assertNotEquals(null, recordId, 'Record Id for DCR cannot be null.');
            recordIds.add(recordId);
        }

        String query = 'SELECT Id, Parent_Data_Change_Request_MVN__c FROM Data_Change_Request_MVN__c WHERE ' + dcrSetting.SObject_Field_Id_MVN__c + ' IN :recordIds';

        List<Data_Change_Request_MVN__c> dcrs = (List<Data_Change_Request_MVN__c>) Database.query(query);

        System.assert(dcrs.isEmpty(), 'DCRs were created.');
    }

    static List<SObject> updateRecords(List<SObject> records, DCR_Setting_MVN__mdt dcrSetting) {
        List<DCR_Field_Setting_MVN__mdt> dcrFieldSettings = getValidDCRFieldSettings(records[0], dcrSetting);

        Map<String, Schema.SObjectField> apiToLabel = records[0].getSobjectType().getDescribe().fields.getMap();

        List<SObject> newRecords = new List<SObject>();

        for (SObject record : records) {
            SObject newRecord = record.clone(true, true, true, true);

            for (DCR_Field_Setting_MVN__mdt dcrFieldSetting : dcrFieldSettings) {
                Schema.SObjectField sobjectField = apiToLabel.get(dcrFieldSetting.Field_MVN__c);

                if (sobjectField == null) {
                    continue;
                }

                Boolean isValid = sobjectField != null && sobjectField.getDescribe() != null
                        && sobjectField.getDescribe().isAccessible()
                        && sobjectField.getDescribe().isUpdateable()
                        && !sobjectField.getDescribe().isIdLookup()
                        && !sobjectField.getDescribe().isNameField();

                if (!isValid) {
                    continue;
                }

                newRecord.put('DCR_Override_MVN__c', false);

                Object newValue = newRecord.get(dcrFieldSetting.Field_MVN__c) != null ?
                        newRecord.get(dcrFieldSetting.Field_MVN__c) + 'TestUpdate' :
                        'TestUpdate';

                newRecord.put(dcrFieldSetting.Field_MVN__c, newValue);
            }

            newRecords.add(newRecord);
        }

        update newRecords;

        return newRecords;
    }

    static List<DCR_Field_Setting_MVN__mdt> getValidDCRFieldSettings(SObject record, DCR_Setting_MVN__mdt dcrSetting) {
        List<DCR_Field_Setting_MVN__mdt> dcrFieldSettings = new List<DCR_Field_Setting_MVN__mdt>();

        String actualSObjectName = String.IsNotBlank(dcrSetting.Relationship_SObject_Name_MVN__c) ?
                dcrSetting.Relationship_SObject_Name_MVN__c :
                record.getSobjectType().getDescribe().getName();

        DCRFieldSettingsMVN dcrFieldSettingsMVN = new DCRFieldSettingsMVN();

        Map<String, Schema.SObjectField> apiToLabel = record.getSobjectType().getDescribe().fields.getMap();

        for (DCR_Field_Setting_MVN__mdt dcrFieldSetting : dcrFieldSettingsMVN.settings) {
            Schema.SObjectField sobjectField = apiToLabel.get(dcrFieldSetting.Field_MVN__c);

            Boolean isValidFieldSetting = String.IsNotBlank(dcrFieldSetting.Field_MVN__c)
                && dcrFieldSetting.Object_MVN__c == actualSObjectName
                && sobjectField != null
                && sobjectField.getDescribe().isAccessible()
                && sobjectField.getDescribe().isUpdateable()
                && !sobjectField.getDescribe().isIdLookup()
                && !sobjectField.getDescribe().isNameField();

            if (isValidFieldSetting) {
                dcrFieldSettings.add(dcrFieldSetting);
            }
        }

        return dcrFieldSettings;
    }

    static void checkRecordRevertChanges(List<SObject> records, Map<Id, SObject> oldRecordMap) {
        Boolean hasOldValues = oldRecordMap != null && !oldRecordMap.isEmpty();

        if (!hasOldValues) {
            return;
        }

        String sobjectName = records[0].getSobjectType().getDescribe().getName();
        Map<String, Schema.SObjectField> apiToLabel = records[0].getSobjectType().getDescribe().fields.getMap();

        List<String> fieldNames = getValidFieldNamesForRecord(sobjectName, apiToLabel);

        List<SObject> newRecords = getRecords(records);

        for (SObject newRecord : newRecords) {
            SObject oldRecord = oldRecordMap.get(newRecord.Id);

            System.assertNotEquals(null, oldRecord);

            for (String fieldName : fieldNames) {
                Object newValue = newRecord.get(fieldName);
                Object oldValue = oldRecord.get(fieldName);

                System.assertEquals(oldValue, newValue , 'oldRecord: ' + oldRecord + ', newRecord: ' + newRecord);
            }
        }
    }

    static List<String> getValidFieldNamesForRecord(String sobjectName, Map<String, Schema.SObjectField> apiToLabel) {
        List<String> fieldNames = new List<String>();

        DCRFieldSettingsMVN dcrFieldSettingsMVN = new DCRFieldSettingsMVN();

        for (DCR_Field_Setting_MVN__mdt dcrFieldSetting : dcrFieldSettingsMVN.settings) {
            Schema.SObjectField sobjectField = apiToLabel.get(dcrFieldSetting.Field_MVN__c);

            Boolean isValidFieldSetting = sobjectField != null
                && dcrFieldSetting.Object_MVN__c == sobjectName
                && sobjectField.getDescribe().isAccessible()
                && sobjectField.getDescribe().isUpdateable()
                && !sobjectField.getDescribe().isIdLookup()
                && !sobjectField.getDescribe().isNameField();

            if (isValidFieldSetting) {
                fieldNames.add(dcrFieldSetting.Field_MVN__c);
            }
        }

        fieldNames.add('Id');

        return fieldNames;
    }
}