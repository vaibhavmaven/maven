/**
 * @author Mavens
 * @description If the case is closed, the related program member is closed too and and if the case is open, 
 *              and its status has been changed, then the program member is opens too.
 * @group Case
 **/

public with sharing class CaseCmpletPMStageWhenCaseCmpletMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
     * @description handle
     */
    public void handle() {
        List<Program_Member_Stage_MVN__c> programMembers = new List<Program_Member_Stage_MVN__c>();
        List<Case> openCaseIds = new List<Case>(); 
        Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();

        for (Case myCase : (List<Case>) trigger.new) {
            if (myCase.Program_Member_Stage_MVN__c == null) {
                continue;
            }

            if (myCase.isClosed) {
                // For closed case, we update the related Program Member to a closed status
                programMembers.add(new Program_Member_Stage_MVN__c(
                    Id = myCase.Program_Member_Stage_MVN__c,
                    Closed_Date_MVN__c = System.today(),
                    IsClosed_MVN__c = true,
                    Status_MVN__c = settings.ProgramMS_Completed_Status_MVN__c
                ));
            } else if (isCaseOpen(myCase)) {
                // For open cases, further handle below.
                openCaseIds.add(myCase);
            }
        }

        for (Case myCase : [SELECT Program_Member_Stage_MVN__c
                            FROM Case 
                            WHERE Id IN :openCaseIds AND 
                                  Program_Member_Stage_MVN__r.IsClosed_MVN__c = false]) {
            // Only for program member stages which are not yet closed, reset their fields
            programMembers.add(new Program_Member_Stage_MVN__c(
                Id = myCase.Program_Member_Stage_MVN__c,
                Closed_Date_MVN__c = null,
                Status_MVN__c = settings.Program_Member_Stage_Start_Status_MVN__c
            ));
        }

        // update anything to be updated
        if (!programMembers.isEmpty()) {
            update programMembers;
        }
    }

    /**
     * @description isCaseOpen
     * @param Case myCase
     * @return Boolean
     */
    private static Boolean isCaseOpen(Case myCase) {
        return myCase.isClosed && (
            trigger.isInsert || 
            trigger.isUpdate && ((Case)trigger.oldMap.get(myCase.Id)).IsClosed != myCase.IsClosed
        );
    }
}