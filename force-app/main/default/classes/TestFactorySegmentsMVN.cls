/**
 * TestFactorySegmentsMVN
 * @author: Pablo Roldan
 * @created Date: October 2017
 * @description: This class is used to construct Segments custom metadatas
 */
@isTest
public class TestFactorySegmentsMVN {
    
	public static final Id defaultSegmentId = Id.valueOf('000000000000001');
    public static final Id usSegmentId      = Id.valueOf('000000000000002');
    public static final Id gbSegmentId      = Id.valueOf('000000000000003');

    private static Map<Id, Segment_MVN__mdt> segments;

    public static void createMockSegments() {
        if (segments == null) {
            segments = new Map<Id, Segment_MVN__mdt>();

            segments.put(defaultSegmentId, (Segment_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata('Segment_MVN__mdt', new Map<String, Object>{
                'Id'=>'000000000000001', 
                'Country_Code_MVN__c'=>'ZZ'
            }));

            segments.put(usSegmentId, (Segment_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata('Segment_MVN__mdt', new Map<String, Object>{
                'Id'=>'000000000000002', 
                'Country_Code_MVN__c'=>'US'
            }));

            segments.put(gbSegmentId, (Segment_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata('Segment_MVN__mdt', new Map<String, Object>{
                'Id'=>'000000000000003', 
                'Country_Code_MVN__c'=>'GB'
            }));
        }

        SegmentsMVN.segmentCache = segments;
    }
}