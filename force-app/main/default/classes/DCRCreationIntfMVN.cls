/**
 * @author Mavens
 * @group DCR Creation
 * @description This is the generic interface to support DCR Creation
 */
public interface DCRCreationIntfMVN {

    /**
     * Create DCRs method using records and dcr type
     * @param  sobjects List of records
     * @param  type     DCR type
     * @return          Boolean which shows if DCRs were created
     */
    Boolean createDCRs(List<SObject> sobjects, String type);

    /**
     * Create DCRs method using records, dcr type and old records map
     * @param  sobjects      List of records
     * @param  type          DCR type
     * @param  oldRecordsMap Map of old value records
     * @return               Boolean which shows if DCRs were created
     */
    Boolean createDCRs(List<SObject> sobjects, String type, Map<Id, SObject> oldRecordsMap);
}