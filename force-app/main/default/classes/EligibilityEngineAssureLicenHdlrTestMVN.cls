/**
 * @author      Mavens
 * @date        August 2018
 * @group       EligibilityEngine
 * @description tests for EligibilityEngineAssureLicenHdlrMVN
 */
@isTest
private class EligibilityEngineAssureLicenHdlrTestMVN {

    private static User caseManager;
    private static Account physician;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;
    private static Address_vod__c primaryAddress;
    private static Product_MVN__c product;
    private static Assure_Integration_Settings_MVN__c settings;
    private static Map<String, Object> requestedData;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();

        settings = TestDataFactoryMVN.buildAssureSettings();
        insert settings;

        System.runAs(caseManager) {
            program = createProgram();
            physician = createPhysician();
            primaryAddress = createPrimaryAddress(physician);
            programMember = createProgramMember();
            application = createApplication();
            product = createProductAndProductLineItems();
            createPrescription();
            programMember.Active_Application_MVN__c = application.Id;
            update programMember;
        }
    }

    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        return programFactory.create(
            new Map<String,Object>{
                'Name' => 'A Program',
                'Program_Id_MVN__c' => 'Knipper_PAP_MVN',
                'Active_MVN__c' => true,
                'Job_No_MVN__c' => '12345'
            }
        );
    }

    static Account createPhysician() {
        TestFactoryAccountMVN accountTestFactory = new TestFactoryAccountMVN();
        Account physician = accountTestFactory.create(new Map<String, Object> {
            'RecordTypeId' => TestFactoryAccountMVN.accountRecordTypesByName.get('Professional_vod').Id,
            'FirstName' => 'Joel',
            'LastName' => 'Atwood',
            'Credentials_vod__c' => 'MD',
            'DEA_MVN__c' => 'DEA123',
            'SLN_MVN__c' => 'SLN123'
        });
        return physician;
    }

    static Address_vod__c createPrimaryAddress(Account physician) {
        TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();
        return addressFactory.createAddress(physician, new Map<String,Object>{
            'Name' => 'Physician Address Line 1',
            'Address_line_2_vod__c' => 'Physician Address Line 2',
            'Shipping_vod__c' => true,
            'Primary_vod__c' => true,
            'City_vod__c' => 'Physician City',
            'Zip_vod__c' => '11111',
            'State_vod__c' => 'DE'
        });
    }

    static Program_Member_MVN__c createProgramMember() {
        Program_Member_MVN__c programMemberTest =
            TestDataFactoryMVN.createProgramMember(program, TestDataFactoryMVN.createMember());
        programMemberTest.Physician_MVN__c = physician.Id;
        update programMemberTest;
        return programMemberTest;
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        return testApplication;
    }

    static Product_MVN__c createProductAndProductLineItems() {

        TestFactoryProductMVN productFactory = new TestFactoryProductMVN();
        Product_MVN__c product = productFactory.create(new Map<String, Object>{
            'Program_MVN__c' => program.Id,
            'Name' => 'Horizant',
            'Ship_To_MVN__c' => 'Patient',
            'Product_Status_MVN__c' => 'Active'
        });

        TestFactoryProductLineItemMVN productLineItemFactory = new TestFactoryProductLineItemMVN();
        List<Product_Line_Item_MVN__c> productLineItemList = new List<Product_Line_Item_MVN__c>();
        productLineItemList.add(
            productLineItemFactory.construct(new Map<String, Object>{
                'Product_MVN__c' => product.Id,
                'Name' => 'Bottle of Horizant',
                'NDC_MVN__c' => 'NDC1234',
                'Is_Primary_MVN__c' => true,
                'Packaged_Quantity_MVN__c' => 100
            })
        );
        productLineItemList.add(
            productLineItemFactory.construct(new Map<String, Object>{
                'Product_MVN__c' => product.Id,
                'Name' => 'Syringe',
                'NDC_MVN__c' => 'NDC5678',
                'Is_Primary_MVN__c' => false,
                'Packaged_Quantity_MVN__c' => 1
            })
        );
        insert productLineItemList;

        return product;
    }

    static Prescription_MVN__c createPrescription() {
        TestFactoryPrescriptionMVN prescriptionFactory = new TestFactoryPrescriptionMVN();
        Prescription_MVN__c testPrescription = prescriptionFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id,
            'Application_MVN__c' => application.Id,
            'Signal_Code_MVN__c' => 'QD - Once a day',
            'Signal_Code_Quantity_MVN__c' => 4,
            'Refills_MVN__c' => 1,
            'Timeframe_MVN__c' => '60',
            'Product_MVN__c' => product.Id
        });
        return testPrescription;
    }

    static void createAssureRule() {
        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '1',
            Name = 'Test Assure Rule One',
            Type_MVN__c = 'Assure_License_Validation_MVN',
            Return_Multiple_Results_MVN__c = true,
            Program_MVN__c = program.Id,
            Active_MVN__c = true
        );
        insert expectedRule;
    }

    @isTest
    private static void testPassed() {
        createAssureRule();
        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class,
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_PASSED));

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(application.Id)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(String.format(Label.Assure_Validation_Rule_Name_MVN, new List<String>{'SLN'}), actualResults[0].Name);
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    private static void testMissingCredentials() {
        createAssureRule();
        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class,
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_PASSED));

        settings.Assure_Username_MVN__c = '';
        update settings;

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(application.Id)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(EligibilityEngineMVN.RULEDENIED, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    private static void testMissingInfoEntryData() {
        createAssureRule();
        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class,
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_PASSED));

        program.Job_No_MVN__c = null;
        update program;

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(application.Id)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(EligibilityEngineMVN.RULEMISSINGINFO, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    private static void testMissingInfo() {
        createAssureRule();
        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class,
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_MISSING_INFO));

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(application.Id)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(EligibilityEngineMVN.RULEMISSINGINFO, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    private static void testComparisonDEA() {
        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '2',
            Name = 'Test Assure Rule One',
            Type_MVN__c = 'Assure_License_Validation_MVN',
            Program_MVN__c = program.Id,
            Active_MVN__c = true
        );
        insert expectedRule;

        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class,
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_PASSED));

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(application.Id)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }

    @isTest
    private static void testComparisonSLNDEA() {
        Eligibility_Engine_Rule_MVN__c expectedRule = new Eligibility_Engine_Rule_MVN__c(
            External_Id_MVN__c = '3',
            Name = 'Test Assure Rule One',
            Type_MVN__c = 'Assure_License_Validation_MVN',
            Program_MVN__c = program.Id,
            Active_MVN__c = true
        );
        insert expectedRule;

        TestFactoryAssureSettingMVN.setMocks();
        Test.setMock(WebServiceMock.class,
            new AssureWebServiceMockMVN(AssureWebServiceMockMVN.RESPONSE_PASSED));

        System.runAs(caseManager) {

            Test.startTest();
            List<Eligibility_Engine_Result_MVN__c> actualResults =
                (new EligibilityEngineMVN(application.Id)).run();
            Test.stopTest();

            System.assertEquals(1, actualResults.size());
            System.assertEquals(EligibilityEngineMVN.RULESUCCESS, actualResults[0].Result_MVN__c);
        }
    }
}