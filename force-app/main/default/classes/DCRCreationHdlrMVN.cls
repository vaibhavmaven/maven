/**
 * @author Mavens
 * @group Network API
 * @description Decide and creates DCRs for any object
 * @API
 *  - createDCRs(sobjectName, dcrType);
 *  - createDCRs(sobjectName, dcrType, oldRecordsMap);
 */
public with sharing class DCRCreationHdlrMVN implements DCRCreationIntfMVN {

    /**
     * Records which will be processed in this wrapper
     */
    private static List<SObject> records;

    /**
     * old values for any record which will be processed
     */
    private static Map<Id, SObject> oldRecordsById;

    /**
     * Global settings - custom metadata
     */
    private static DCRGlobalSettingMVN dcrGlobalSetting;

    /**
     * DCR Settings - custom metadata
     */
    private static DCR_Setting_MVN__mdt dcrSetting;

    /**
     * DCR Field Settings - custom metadata
     */
    private static List<DCR_Field_Setting_MVN__mdt> dcrFieldSettings;

    /**
     * Current SObject Type got from records
     */
    private static SObjectType currentSObjectType;

    /**
     * Decides if the record is a join object which won't be used in the DCR
     */
    private static Boolean isRecordRelationship;

    /**
     * DCR Record type Name
     */
    private static String dcrRecordType;

    /**
     * DCR Type
     */
    private static String dcrType;

    /**
     * DCR Country
     */
    private static String country;

    /**
     *  List of DCR Structures to create all our DCRS
     */
    private static List<DCRStructure> dcrStructures;

    /**
     * All DCR Record types
     */
    private static List<RecordType> dcrRecordTypes;

    /**
     * All records Record types
     */
    private static Map<Id, RecordType> recordTypesById;

    /**
     * Records Record types allowed
     */
    private static Set<String> recordRecordTypesAllowed;

    /**
     * Relationship Record types allowed
     */
    private static Set<String> relationshipRecordTypesAllowed;

    /**
     * Create DCRs using sobjects (records) and DCR type
     * @param  sobjects records (any sobject type) used to create DCRs
     * @param  type     String DCR type
     * @return          returns if DCRs were created without any failure
     */
    public static Boolean createDCRs(List<SObject> sobjects, String type) {
        return createDCRs(sobjects, type, null);
    }

    /**
     * Create DCRs using sobjects (records), DCR type and old records map
     * @param  sobjects      records (any sobject type) used to create DCRs
     * @param  type          String DCR type
     * @param  oldRecordsMap old field values from records
     * @return               returns if DCRs were created without any failure
     */
    public static Boolean createDCRs(List<SObject> sobjects, String type, Map<Id, SObject> oldRecordsMap) {
        if (sobjects == null || sobjects.isEmpty()) {
            return false;
        }

        dcrGlobalSetting = new DCRGlobalSettingMVN();

        currentSObjectType = sobjects[0].getSobjectType();

        dcrSetting = getDCRSettings();

        if (dcrSetting == null) {
            return false;
        }

        isRecordRelationship = false;

        records = sobjects;
        dcrType = type;
        oldRecordsById = oldRecordsMap;

        Boolean isSObjectDCRAllowed = checkSObjectIsAllowedToCreateDCRs();

        Boolean isDCRcreated = isSObjectDCRAllowed ?
                createDCRs() :
                false;

        revertChangesInRecords();

        return isDCRcreated;
    }

    /**
     * Gets DCR Setting using record sobject type name
     * @return   return DCR Setting for current records
     */
    private static DCR_Setting_MVN__mdt getDCRSettings() {
        DCR_Setting_MVN__mdt currentDCRSetting;
        recordRecordTypesAllowed = new Set<String>();
        relationshipRecordTypesAllowed = new Set<String>();

        DCRSettingsMVN dcrSettingsMVN = new DCRSettingsMVN(currentSObjectType);

        Boolean isDCRSettingConfigured = dcrSettingsMVN != null && dcrSettingsMVN.setting != null
                && dcrSettingsMVN.setting.SObject_Name_MVN__c == currentSObjectType.getDescribe().getName();

        if (isDCRSettingConfigured) {
            currentDCRSetting = dcrSettingsMVN.setting;

            if (currentDCRSetting == null) {
                return null;
            }

            recordRecordTypesAllowed = getRecordTypesAllowed(currentDCRSetting.SObject_Record_Types_Allowed_MVN__c);
            relationshipRecordTypesAllowed = getRecordTypesAllowed(currentDCRSetting.Relationship_Record_Types_Allowed_MVN__c);
        }

        return currentDCRSetting;
    }

    private static Set<String> getRecordTypesAllowed(String recordTypesAllowed) {
        Set<String> recordTypesAllowedDivided = new Set<String>();

        if (recordTypesAllowed == null) {
            return recordTypesAllowedDivided;
        }

        List<String> rtsAllowed = recordTypesAllowed.split(',');

        for (String rtAllowed : rtsAllowed) {
            recordTypesAllowedDivided.add(rtAllowed.normalizeSpace());
        }

        return recordTypesAllowedDivided;
    }

    /**
     * Check if current SObject Type is allowed to create DCRs
     * @return   Boolean if sobject type is allowed
     */
    private static Boolean checkSObjectIsAllowedToCreateDCRs() {
        Boolean isSObjectDCRAllowed = false;

        Boolean isDCRActive = dcrGlobalSetting != null && dcrGlobalSetting.setting != null
                && dcrGlobalSetting.setting.DCR_Globally_Active_MVN__c;

        Boolean isProfileExcluded = checkIfProfileIsExcluded();

        if (isDCRActive && !isProfileExcluded) {
            isSObjectDCRAllowed = checkIsSObjectDCRAllowed();
        }

        return isSObjectDCRAllowed;
    }

    /**
     * Check if profile is excluded
     * @return   Boolean if current profile is excluded
     */
    private static Boolean checkIfProfileIsExcluded() {
        String currentProfileName = [SELECT Name
                                       FROM Profile
                                      WHERE Id = :UserInfo.getProfileId()].Name;

        return String.IsNotBlank(currentProfileName)
                && dcrGlobalSetting != null
                && dcrGlobalSetting.excludedProfileNames != null
                && dcrGlobalSetting.excludedProfileNames.contains(currentProfileName);
    }

    /**
     * Check if current SObject type is DCR allowed
     * @return   return boolean if sobject DCR allowed
     */
    private static Boolean checkIsSObjectDCRAllowed() {
        Boolean isSObjectDCRAllowed = String.IsNotBlank(dcrSetting.DCR_Record_Type_Name_MVN__c);

        String dcrGlobalSettingFieldVerification = dcrSetting.Processed_Field_Name_MVN__c;

        if (isSObjectDCRAllowed && String.IsNotBlank(dcrGlobalSettingFieldVerification)) {
            isSObjectDCRAllowed = (Boolean) dcrGlobalSetting.setting.get(dcrGlobalSettingFieldVerification);
        }

        return isSObjectDCRAllowed;
    }

    /**
     * Create DCRs for any sobject type which is allowed. Any error rollback the database.
     * @return   Boolean if DCRs were created correctly.
     */
    private static Boolean createDCRs() {
        Boolean isDCRCreated = false;

        initializeSettings();

        Savepoint sp = Database.setSavepoint();

        try {
            initializeDCRStructure();

            if (dcrStructures != null && !dcrStructures.isEmpty()) {
                generateParentDCRs();
                generateDCRs();

                insertDCRs();
                insertDCRLines();

                isDCRCreated = true;
            }
        } catch (Exception insertDCRsException) {
            isDCRCreated = false;
            System.debug('### Create DCRs Exception: ' + insertDCRsException.getMessage() + ' - Stack: ' + insertDCRsException.getStackTraceString());
            Database.rollback(sp);
        }

        return isDCRCreated;
    }

    /**
     * Initialize settings used to create DCRs according SObject processed
     */
    private static void initializeSettings() {
        recordTypesById = getRecordTypesById();

        String sobjectTypeForDCRFieldSettings = String.IsNotBlank(dcrSetting.Relationship_SObject_Name_MVN__c) ?
                dcrSetting.Relationship_SObject_Name_MVN__c :
                currentSObjectType.getDescribe().getName();

        DCRFieldSettingsMVN dcrFieldSettingsMVN = new DCRFieldSettingsMVN(sobjectTypeForDCRFieldSettings);
        dcrFieldSettings = dcrFieldSettingsMVN.settings;

        country = String.IsBlank(country) ?
                [SELECT Country FROM User WHERE Id = :UserInfo.getUserId()].Country :
                country;

        DCRUtilityMVN dcrUtility = new DCRUtilityMVN();
        dcrRecordTypes = dcrUtility.dcrRecordTypes;
    }

    /**
     * Get Record Types by id for all objects
     * @return   Map of record types by Id
     */
    private static Map<Id, RecordType> getRecordTypesById() {
        if (dcrSetting == null) {
            return null;
        }

        Set<String> sobjectNames = new Set<String> {
            'Account'
        };

        if (String.IsNotBlank(dcrSetting.SObject_Name_MVN__c)) {
            sobjectNames.add(dcrSetting.SObject_Name_MVN__c);
        }

        if (String.IsNotBlank(dcrSetting.Relationship_SObject_Name_MVN__c)) {
            sobjectNames.add(dcrSetting.Relationship_SObject_Name_MVN__c);
        }


        return new Map<Id, RecordType>(
            [SELECT Id,
                    DeveloperName,
                    SObjectType
               FROM RecordType
              WHERE SObjectType IN :sobjectNames]
        );
    }

    /**
     * Get records which will be used to create DCRsusing relationship sobject if exists
     * @return   list of records to process
     */
    private static void initializeDCRStructure() {
        dcrStructures = new List<DCRStructure>();

        isRecordRelationship = String.IsNotBlank(dcrSetting.Relationship_Field_Name_MVN__c)
                && String.IsNotBlank(dcrSetting.Relationship_SObject_Name_MVN__c);

        records = getRecordsDCRAllowed();

        if (!isRecordRelationship) {
            initializeDCRStructureWithoutRelationship();
        } else {
            initializeDCRStructureWithRelationship();
        }
    }

    /**
     * Get Records which are allowed to create a DCR record depending on DCR_Override_MVN__c
     * @return   list of records
     */
    private static List<SObject> getRecordsDCRAllowed() {
        List<SObject> recordsDCRAllowed = new List<SObject>();

        List<SObject> recordsToChangeDCROverride = new List<SObject>();

        for (SObject record : records) {
            Boolean isDCROverride = Boolean.valueOf(record.get('DCR_Override_MVN__c'));

            if (dcrType == 'Edit' && isDCROverride) {
                record.put('DCR_Override_MVN__c', false);
            } else if (!isDCROverride) {
                recordsDCRAllowed.add(record);
            } else {
                String sobjectName = record.getSObjectType().getDescribe().getName();
                SObject recordToChangeDCROverride = Schema.getGlobalDescribe().get(sobjectName).newSObject() ;
                recordToChangeDCROverride.Id = record.Id;
                recordsToChangeDCROverride.add(recordToChangeDCROverride);
            }
        }

        if (!recordsToChangeDCROverride.isEmpty()) {
            update recordsToChangeDCROverride;
        }

        return recordsDCRAllowed;
    }

    /**
     * Get records without relationship to process only if any new value changed
     */
    private static void initializeDCRStructureWithoutRelationship() {
        DCRFieldSettingsMVN dcrFieldSettingsMVN = new DCRFieldSettingsMVN(currentSObjectType.getDescribe().getName());
        List<DCR_Field_Setting_MVN__mdt> currentDCRFieldSettings = dcrFieldSettingsMVN.settings;

        Map<Id, SObject> parentAccountById = getParentAccountById();

        for (SObject record : records) {
            Boolean isRecordTypeAllowed = checkRecordTypeIsAllowed(record, recordRecordTypesAllowed);

            if (!isRecordTypeAllowed) {
                continue;
            }

            DCRStructure dcrStructure = new DCRStructure(record);

            SObject oldRecord = oldRecordsById != null && oldRecordsById.containsKey(record.Id) ?
                    oldRecordsById.get(record.Id) :
                    null;

            SObject parentAccount;

            if (parentAccountById != null) {
                String parentAccountIdField = String.valueOf(record.get(dcrSetting.Parent_Account_Field_Name_MVN__c));
                dcrStructure.parentAccount = parentAccountById.get(parentAccountIdField);
            }

            if (oldRecord == null) {
                dcrStructures.add(dcrStructure);
                continue;
            }

            for (DCR_Field_Setting_MVN__mdt currentDCRFieldSetting : currentDCRFieldSettings) {
                Boolean isValidFieldSetting = String.IsNotBlank(currentDCRFieldSetting.Field_MVN__c)
                        && currentDCRFieldSetting.Object_MVN__c == record.getSObjectType().getDescribe().getName();

                if (!isValidFieldSetting) {
                    continue;
                }

                Object oldValue = oldRecord.get(currentDCRFieldSetting.Field_MVN__c);
                Object newValue = record.get(currentDCRFieldSetting.Field_MVN__c);

                if (oldValue != newValue) {
                    dcrStructures.add(dcrStructure);
                    break;
                }
            }
        }
    }

    /**
     * Check if Record Type Is Allowed
     * @param  record      SObject record
     * @param  recordTypes Set of record type developer names allowed
     * @return             Boolean which shows if record is allowed
     */
    private static Boolean checkRecordTypeIsAllowed(SObject record, Set<String> recordTypes) {
        Map<String, Schema.SObjectField> fieldsByName = record.getSObjectType().getDescribe().fields.getMap();

        RecordType recordRecordType = fieldsByName.containsKey('RecordTypeId') ?
                recordTypesById.get(String.valueOf(record.get('RecordTypeId'))) :
                null;

        if (recordRecordType == null) {
            return true;
        }

        return recordTypes.contains(recordRecordType.DeveloperName) || recordTypes.contains('All');
    }

    /**
     * Get records without relationship to process only if any new value changed
     */
    private static void initializeDCRStructureWithRelationship() {
        Map<Id, SObject> childRelationshipById = getChildRelationshipById();
        Map<Id, SObject> parentAccountById = getParentAccountById();

        for (SObject record : records) {
            Boolean isRecordTypeAllowed = checkRecordTypeIsAllowed(record, recordRecordTypesAllowed);

            if (!isRecordTypeAllowed) {
                continue;
            }

            Id newRecordId = String.valueOf(record.get(dcrSetting.Relationship_Field_Name_MVN__c));

            if (String.IsBlank(newRecordId) || !childRelationshipById.containsKey(newRecordId)) {
                continue;
            }

            Id parentAccountId = String.valueOf(record.get(dcrSetting.Parent_Account_Field_Name_MVN__c));

            if (String.IsBlank(parentAccountId) || !parentAccountById.containsKey(parentAccountId)) {
                continue;
            }

            SObject childRelationshipRecord = childRelationshipById.get(newRecordId);
            SObject parentAccount = parentAccountById.get(parentAccountId);

            isRecordTypeAllowed = checkRecordTypeIsAllowed(childRelationshipRecord, relationshipRecordTypesAllowed);

            if (!isRecordTypeAllowed) {
                continue;
            }

            DCRStructure dcrStructure = new DCRStructure(childRelationshipRecord);

            dcrStructure.relationship = record;
            dcrStructure.parentAccount = parentAccount;

            dcrStructures.add(dcrStructure);
        }
    }

    /**
     * Get relationship by Id map
     * @return   Map of relationship record by Id
     */
    private static Map<Id, SObject> getChildRelationshipById() {
        List<Id> childRelationshipIds = new List<Id>();

        for (SObject record : records) {
            Id newRecordId = String.valueOf(record.get(dcrSetting.Relationship_Field_Name_MVN__c));

            if (String.IsBlank(newRecordId)) {
                continue;
            }

            childRelationshipIds.add(newRecordId);
        }

        return new Map<Id, SObject>(getNewRecordsToProcess(childRelationshipIds));
    }

    /**
     * Query and return new records to process
     * @param  newRecordIds Set of record Ids to process
     * @return              List of records to process
     */
    private static List<SObject> getNewRecordsToProcess(List<Id> newRecordIds) {
        Set<String> fieldNames = new Set<String> {
            'Id',
            'Network_Id_MVN__c',
            'RecordTypeId',
            'RecordType.DeveloperName'
        };

        for (DCR_Field_Setting_MVN__mdt dcrFieldSetting : dcrFieldSettings) {
            Boolean isValidFieldSetting = String.IsNotBlank(dcrFieldSetting.Field_MVN__c)
                    && dcrFieldSetting.Object_MVN__c == dcrSetting.Relationship_SObject_Name_MVN__c;

            if (isValidFieldSetting) {
                fieldNames.add(dcrFieldSetting.Field_MVN__c);
            }
        }

        String queryFieldNames = String.join(new List<String>(fieldNames), ',');

        String query = 'SELECT '+ queryFieldNames +' FROM ' + dcrSetting.Relationship_SObject_Name_MVN__c + ' WHERE Id IN :newRecordIds';

        return Database.query(query);
    }

    private static Map<Id, SObject> getParentAccountById() {
        if (String.IsBlank(dcrSetting.Parent_Account_Field_Name_MVN__c)) {
            return null;
        }

        List<Id> parentAccountIds = new List<Id>();

        for (SObject record : records) {
            Id parentAccountId = String.valueOf(record.get(dcrSetting.Parent_Account_Field_Name_MVN__c));

            if (String.IsBlank(parentAccountId)) {
                continue;
            }

            parentAccountIds.add(parentAccountId);
        }

        return new Map<Id, SObject>(getParentAccounts(parentAccountIds));
    }

    /**
     * Query and return parent accounts associated to any record to process using a list of parent account ids
     * @param  parentAccountsByChildId Map of list of parent Account Id by child record id
     * @param  currentFieldSettings    DCR Field Settings - custom metadata
     * @return                         List of accounts
     */
    private static List<Account> getParentAccounts(List<Id> parentAccountIds) {
        Set<String> fieldNames = new Set<String> {
            'Id',
            'Network_Id_MVN__c',
            'RecordTypeId',
            'RecordType.DeveloperName'
        };

        DCRFieldSettingsMVN parentDCRFieldSettings = new DCRFieldSettingsMVN('Account');

        List<DCR_Field_Setting_MVN__mdt> currentFieldSettings = parentDCRFieldSettings.filterRequired();

        for (DCR_Field_Setting_MVN__mdt currentFieldSetting : currentFieldSettings) {
            Boolean isValid = currentFieldSetting.Object_MVN__c == 'Account'
                    && String.IsNotBlank(currentFieldSetting.Field_MVN__c);

            if (isValid) {
                fieldNames.add(currentFieldSetting.Field_MVN__c);
            }
        }

        String queryFieldNames = String.join(new List<String>(fieldNames), ',');

        String query = 'SELECT '+ queryFieldNames +' FROM Account WHERE Id IN :parentAccountIds';

        return (List<Account>) Database.query(query);
    }

    /**
     * Create Parent DCRs for records. Parent DCRs will be always HCP/HCO according Veeva Network,
     * which will be accounts in Salesforce
     */
    private static void generateParentDCRs() {
        Boolean isParentDCRNotNecessary = String.IsBlank(dcrSetting.Parent_Account_Field_Name_MVN__c);

        if (isParentDCRNotNecessary) {
            return;
        }

        DCRFieldSettingsMVN parentDCRFieldSettings = new DCRFieldSettingsMVN('Account');

        List<DCR_Field_Setting_MVN__mdt> currentFieldSettings = parentDCRFieldSettings.filterRequired();

        buildParentDCRs(currentFieldSettings);
    }

    /**
     * Build and insert parent DCRs and returns a map of parent DCR by Account Id associated to the DCR
     * @param  parentAccounts       List of parent accounts
     * @param  currentFieldSettings DCR Field settings - custom metadata
     * @return                      Map of parent DCR by Account Id
     */
    private static void buildParentDCRs(List<DCR_Field_Setting_MVN__mdt> currentFieldSettings) {
        List<Data_Change_Request_MVN__c> parentDCRs = new List<Data_Change_Request_MVN__c>();

        Map<Id, List<Data_Change_Request_Line_MVN__c>> dcrLinesByAccountId = new Map<Id, List<Data_Change_Request_Line_MVN__c>>();

        Map<String, Schema.SObjectField> apiToLabel = Account.getSObjectType().getDescribe().fields.getMap();

        DCRSettingsMVN dcrSettingsMVN = new DCRSettingsMVN(Account.getSObjectType());

        String parentDCRType = 'Edit';
        String currentDCRType = dcrType;

        dcrType = parentDCRType;

        for (DCRStructure dcrStructure : dcrStructures) {
            Boolean isParentAccountValid = dcrStructure != null
                    && dcrStructure.parentAccount != null;

            if (!isParentAccountValid) {
                continue;
            }

            dcrStructure.parentDCR = initializeDCR(dcrStructure.parentAccount, dcrSettingsMVN.setting);
            dcrStructure.parentDCR.Type_MVN__c = parentDCRType;

            Boolean hasNetworkId = String.IsNotBlank(dcrStructure.parentDCR.Record_External_ID_MVN__c);

            if (!hasNetworkId) {
                dcrStructure.parentDCRLines = getDCRLines(dcrStructure.parentAccount, currentFieldSettings, true);
            }
        }

        dcrType = currentDCRType;
    }

    private static List<Data_Change_Request_Line_MVN__c> getDCRLines(SObject record,
            List<DCR_Field_Setting_MVN__mdt> currentFieldSettings,
            Boolean onlyRequired) {
        List<Data_Change_Request_Line_MVN__c> dcrLines = new List<Data_Change_Request_Line_MVN__c>();

        Map<String, Schema.SObjectField> fieldsByName = record.getSObjectType().getDescribe().fields.getMap();

        RecordType recordRecordType = fieldsByName.containsKey('RecordTypeId') ?
                recordTypesById.get(String.valueOf(record.get('RecordTypeId'))) :
                null;

        for (DCR_Field_Setting_MVN__mdt currentFieldSetting : currentFieldSettings) {
            Boolean isRecordTypeValid = recordRecordType == null
                    || recordRecordType.DeveloperName == currentFieldSetting.Account_Record_Type_MVN__c
                    || currentFieldSetting.Account_Record_Type_MVN__c == 'All';

            Boolean isValidFieldSetting = currentFieldSetting.Object_MVN__c == record.getSObjectType().getDescribe().getName()
                    && isRecordTypeValid
                    && currentFieldSetting.Active_MVN__c;

            if (!isValidFieldSetting) {
                continue;
            }

            String newValue = String.valueOf(record.get(currentFieldSetting.Field_MVN__c));

            Boolean isEditDCR = dcrType == 'Edit';

            Boolean isUpdateRecord = isEditDCR && oldRecordsById != null && !oldRecordsById.isEmpty();

            SObject oldRecord = isUpdateRecord ?
                            oldRecordsById.get(record.Id) :
                            null;

            String oldValue;

            if (oldRecord != null) {
                oldValue = String.valueOf(oldRecord.get(currentFieldSetting.Field_MVN__c));
            } else if (isEditDCR) {
                oldValue = newValue;
            }

            if (onlyRequired) {
                isValidFieldSetting &= currentFieldSetting.Required_for_DCR_Transmission_MVN__c;
            } else {
                Boolean hasChanged = oldRecord == null || (oldRecord != null && oldValue != newValue);

                isValidFieldSetting &= ((currentFieldSetting.Required_for_DCR_Transmission_MVN__c && !isEditDCR) || hasChanged);
            }

            if (!isValidFieldSetting) {
                continue;
            }

            dcrLines.add(
                new Data_Change_Request_Line_MVN__c(
                    Field_API_Name_MVN__c = currentFieldSetting.Field_MVN__c,
                    Field_Name_MVN__c = fieldsByName.get(currentFieldSetting.Field_MVN__c).getDescribe().getLabel(),
                    New_Value_MVN__c = newValue,
                    Old_Value_MVN__c = oldValue
                )
            );
        }

        return dcrLines;
    }

    /**
     * Build and insert DCRs for all records to process
     * @return   Map of list of DCRs by record to process id
     */
    private static void generateDCRs() {
        Map<Id, Data_Change_Request_MVN__c> dcrsByRecordId = new Map<Id, Data_Change_Request_MVN__c>();

        for (DCRStructure dcrStructure : dcrStructures) {
            if (dcrStructure == null) {
                continue;
            }

            dcrStructure.dcr = initializeDCR(dcrStructure.record, dcrSetting);

            Boolean hasRelationshipObject = dcrStructure.relationship != null && dcrStructure.relationship.Id != null
                    && String.IsNotBlank(dcrSetting.Relationship_SObject_Id_Field_MVN__c);

            if (hasRelationshipObject) {
                dcrStructure.dcr.put(dcrSetting.Relationship_SObject_Id_Field_MVN__c, dcrStructure.relationship.Id);
            }

            dcrStructure.dcrLines = getDCRLines(dcrStructure.record, dcrFieldSettings, hasRelationshipObject);
        }
    }

    /**
     * Builds DCR according to record to process and DCR Setting associated to sobject record type
     * @param  record            Record to process
     * @param  currentDCRSetting DCR Setting - custom metadata
     * @return                   DCR record
     */
    private static Data_Change_Request_MVN__c initializeDCR(SObject record, DCR_Setting_MVN__mdt currentDCRSetting) {
        Data_Change_Request_MVN__c dcr = new Data_Change_Request_MVN__c(
                RecordTypeId = getDCRRecordTypeId(currentDCRSetting.DCR_Record_Type_Name_MVN__c),
                Country_MVN__c = country,
                Type_MVN__c = dcrType,
                Status_MVN__c = 'Pending',
                MDM_Datetime_MVN__c = Datetime.now(),
                Record_External_ID_MVN__c = String.valueOf(record.get('Network_Id_MVN__c'))
        );

        dcr.put(currentDCRSetting.SObject_Field_Id_MVN__c, record.Id);

        return dcr;
    }

    /**
     * Get record type id associated to record type developer name only for DCRs
     * @param  recordTypeName String record type developer name
     * @return                Record type Id
     */
    private static Id getDCRRecordTypeId(String recordTypeName) {
        Id recordTypeId;

        for(RecordType recordType : dcrRecordTypes) {
            if(recordTypeName == recordType.DeveloperName) {
                recordTypeId = recordType.Id;
                break;
            }
        }

        return recordTypeId;
    }

    private static void insertDCRs() {
        insertParentDCRs();

        List<Data_Change_Request_MVN__c> dcrs = new List<Data_Change_Request_MVN__c>();

        for (DCRStructure dcrStructure : dcrStructures) {
            if (dcrStructure == null) {
                continue;
            }

            Boolean isDCRValid = dcrStructure.dcr != null
                    && dcrStructure.dcrLines != null
                    && !dcrStructure.dcrLines.isEmpty();

            if (!isDCRValid) {
                continue;
            }

            Boolean isParentAccountValid = dcrStructure.parentAccount != null;

            Boolean isParentDCRValid = dcrStructure.parentDCR != null
                    && dcrStructure.parentDCR.Id != null;

            if (isParentAccountValid && isParentDCRValid) {
                dcrStructure.dcr.Parent_Data_Change_Request_MVN__c = dcrStructure.parentDCR.Id;
                dcrs.add(dcrStructure.dcr);
            } else if (!isParentAccountValid && !isParentDCRValid) {
                dcrs.add(dcrStructure.dcr);
            }
        }

        Database.insert(dcrs, false);
    }

    private static void insertParentDCRs() {
        List<Data_Change_Request_MVN__c> parentDCRs = new List<Data_Change_Request_MVN__c>();

        for (DCRStructure dcrStructure : dcrStructures) {
            if (dcrStructure == null) {
                continue;
            }

            Boolean isParentDCRValid = dcrStructure.parentDCR != null
                    && (String.IsNotBlank(dcrStructure.parentDCR.Record_External_ID_MVN__c)
                        || (dcrStructure.parentDCRLines != null
                            && !dcrStructure.parentDCRLines.isEmpty()));

            Boolean isDCRValid = dcrStructure.dcr != null
                    && dcrStructure.dcrLines != null
                    && !dcrStructure.dcrLines.isEmpty();

            if (isParentDCRValid && isDCRValid) {
                parentDCRs.add(dcrStructure.parentDCR);
            }
        }

        Database.insert(parentDCRs, false);
    }

    /**
     * Builds and insert DCR lines associated to all record DCRs
     * @param  dcrsByRecordId Map of list of DCRs by record to process Id
     */
    private static void insertDCRLines() {
        List<Data_Change_Request_Line_MVN__c> dcrLines = new List<Data_Change_Request_Line_MVN__c>();

        for (DCRStructure dcrStructure : dcrStructures) {
            if (dcrStructure == null) {
                continue;
            }

            Boolean isValidParentDCRLine = dcrStructure.parentDCR != null && dcrStructure.parentDCR.Id != null
                    && dcrStructure.parentDCRLines != null && !dcrStructure.parentDCRLines.isEmpty();

            Boolean isValidDCRLine = dcrStructure.dcr != null && dcrStructure.dcr.Id != null
                    && dcrStructure.dcrLines != null && !dcrStructure.dcrLines.isEmpty();

            if (isValidParentDCRLine) {
                for (Data_Change_Request_Line_MVN__c parentDCRLine : dcrStructure.parentDCRLines) {
                    parentDCRLine.Data_Change_Request_MVN__c = dcrStructure.parentDCR.Id;
                    dcrLines.add(parentDCRLine);
                }
            }

            if (isValidDCRLine) {
                for (Data_Change_Request_Line_MVN__c dcrLine : dcrStructure.dcrLines) {
                    dcrLine.Data_Change_Request_MVN__c = dcrStructure.dcr.Id;
                    dcrLines.add(dcrLine);
                }
            }
        }

        Database.insert(dcrLines, false);
    }

    /**
     * Revert changes in records depending on DCR Global Settings and old value
     * @param  sobjects sobjects description
     */
    private static void revertChangesInRecords() {
        List<SObject> recordsToRevert = getRecordsToRevert();

        Boolean hasAnySOBject = recordsToRevert != null && !recordsToRevert.isEmpty();
        Boolean hasAnyOldRecord = oldRecordsById != null && !oldRecordsById.isEmpty();
        Boolean isDCREdit = dcrType == 'Edit';
        Boolean dcrRequiredForUpdate = dcrGlobalSetting.setting.DCR_Approval_Required_for_Update_MVN__c;

        Boolean isValid = hasAnySOBject && hasAnyOldRecord && isDCREdit && dcrRequiredForUpdate;

        if (!isValid) {
            return;
        }

        DCRFieldSettingsMVN dcrFieldSettingsMVN = new DCRFieldSettingsMVN(recordsToRevert[0].getSObjectType().getDescribe().getName());
        List<DCR_Field_Setting_MVN__mdt> sobjectsFieldSettings = dcrFieldSettingsMVN.settings;

        for (SObject sobj : recordsToRevert) {
            Boolean hasOldRecord = oldRecordsById.containsKey(sobj.Id);
            Boolean isDCROverride = Boolean.valueOf(sobj.get('DCR_Override_MVN__c'));

            if (!hasOldRecord || isDCROverride) {
                continue;
            }

            SObject oldRecord = oldRecordsById.get(sobj.Id);

            SObject newRecord = getCurrentRecordWithChangesReverted(sobj, oldRecord, sobjectsFieldSettings);

            if (newRecord != null) {
                sobj = newRecord;
            }
        }
    }

    private static List<SObject> getRecordsToRevert() {
        List<SObject> recordsToRevert = new List<SObject>();

        Boolean isDCRStructureActive = dcrStructures != null && !dcrStructures.isEmpty();

        if (isDCRStructureActive) {
            for (DCRStructure dcrStructure : dcrStructures) {
                Boolean isValid = dcrStructure != null && dcrStructure.record != null;

                if (!isValid) {
                    continue;
                }

                SObject recordToRevert = dcrStructure.relationship != null ?
                    dcrStructure.relationship :
                    dcrStructure.record;

                recordsToRevert.add(recordToRevert);
            }
        }

        return recordsToRevert;
    }

    /**
     * Get current sobject record with all DCR Field Settings changes reverted
     * @param  record                SObject record
     * @param  oldRecord             SObject record with old values
     * @param  sobjectsFieldSettings List of DCR Field Settings
     * @return                       SObject record
     */
    private static SObject getCurrentRecordWithChangesReverted(SObject record, SObject oldRecord, List<DCR_Field_Setting_MVN__mdt> sobjectsFieldSettings) {
        Boolean hasAnyChange = false;

        Map<String, Schema.SObjectField> fieldsByName = record.getSObjectType().getDescribe().fields.getMap();

        RecordType recordRecordType = fieldsByName.containsKey('RecordTypeId') ?
                (RecordType) record.getSobject('RecordType') :
                null;

        for (DCR_Field_Setting_MVN__mdt sobjectsFieldSetting : sobjectsFieldSettings) {

            Boolean isValid = String.IsNotBlank(sobjectsFieldSetting.Field_MVN__c)
                    && sobjectsFieldSetting.Object_MVN__c == record.getSObjectType().getDescribe().getName()
                    && (recordRecordType == null || sobjectsFieldSetting.Account_Record_Type_MVN__c == recordRecordType.DeveloperName);

            if (isValid) {
                record.put(sobjectsFieldSetting.Field_MVN__c, oldRecord.get(sobjectsFieldSetting.Field_MVN__c));
                // record.put('DCR_Override_MVN__c', true); // avoid creating a new DCR due to these revertion
                hasAnyChange = true;
            }
        }

        if (!hasAnyChange) {
            return null;
        }

        return record;
    }

    private class DCRStructure {
        private SObject record;
        private SObject relationship;
        private SObject parentAccount;

        private Data_Change_Request_MVN__c dcr;
        private List<Data_Change_Request_Line_MVN__c> dcrLines;

        private Data_Change_Request_MVN__c parentDCR;
        private List<Data_Change_Request_Line_MVN__c> parentDCRLines;

        private DCRStructure(SObject record) {
            this.record = record;
        }
    }
}