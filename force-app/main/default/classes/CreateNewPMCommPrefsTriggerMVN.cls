/*
 *  CreateNewPMCommPrefsTriggerMVN
 *  Created By:     Kyle Thornton
 *  Created Date:   4/6/2017
 *  Description:    Creates Communication Preferences for Program Members
 */
public with sharing class CreateNewPMCommPrefsTriggerMVN implements TriggerHandlerMVN.HandlerInterface {

    public void handle() {
        Set<Id> accountIds = new Set<Id>();
        for (Program_Member_MVN__c programMember : (List<Program_Member_MVN__c>) Trigger.new) {
            accountIds.add(programMember.Member_MVN__c);
        }

        Map<Id,Account> accounts = new Map<Id,Account>([SELECT Id,
                                                               (SELECT Id,
                                                                       Sequence_Number_MVN__c,
                                                                       Address_MVN__c
                                                                  FROM Correspondences__r
                                                              ORDER BY Sequence_Number_MVN__c)
                                                          FROM Account
                                                         WHERE Id IN :accountIds]);

        List<Communication_Preference_MVN__c> newCommPrefs = new List<Communication_Preference_MVN__c>();

        for (Program_Member_MVN__c programMember : (List<Program_Member_MVN__c>) Trigger.new) {
            Account pmAccount = accounts.get(programMember.Member_MVN__c);
            for (Contact_Information_MVN__c contactInfo : pmAccount.Correspondences__r) {
                Communication_Preference_MVN__c commPref = new Communication_Preference_MVN__c(
                    Program_Member_MVN__c = programMember.Id,
                    Contact_Information_MVN__c = contactInfo.Id,
                    Sequence_Number_MVN__c = contactInfo.Sequence_Number_MVN__c
                );

                newCommPrefs.add(commPref);
            }
        }

        insert newCommPrefs;
    }
}