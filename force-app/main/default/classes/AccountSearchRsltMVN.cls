/**
 * AccountSearchRsltMVN.cls
 * @created By: Kyle Thornton
 * @reated Date: Jun 2016
 * @edited By: Pablo Roldan
 * @edited Date: November 2017
 * @description:- Represents a row in the results table of an Account Search.
 *               - A result consists of an Account and a Map of Address Records grouped by record Type
 *                and by external id (In the event of a duplicate "identifier" (external Id or Id), first in wins) as well as
 *                the searchUtility which was used to get the result.
 *               - A result can be constructed with just an account and utility or with an account, it's
 *                list of info records and a utility
 *               
 * @edit description: Adapted to Patient Support
 *
 * @API:
 *  Variables
 *      - Account accountDetail
 *  Constructors
 *      - new AccountSearchRsltMVN(Account, AccountSearchIntfMVN)
 *      - new AccountSearchRsltMVN(Account, List<Address_vod__c>, AccountSearchIntfMVN)
 *  Methods
 *      - String getAcountIdentifier()
 *      - void addAddressRecord(Address_vod__c)
 *      - void addAddressRecords(List<Address_vod__c>)
 *      - List<Address_vod__c> getAddressForRecordType(Id)
 *      - AccountSearchRsltMVN insertAccountDetails()
 * Note: Address Records must include an Network_Id_MVN__c and a Record Type Id
 *
 */
public without sharing class AccountSearchRsltMVN {

    public Account accountDetail {get; set;}
    public AccountTypeSettingsMVN acctTypeSettings;

    private List<SObject> affiliations;
    public Map<String, Address_vod__c> addressByExtId;
    private List<AccountSearchRsltMVN> parentAccounts;
    private String searchUtilityClass;

    private AccountSearchRqstMVN request;
    private AccountFieldSettingsMVN resultFields;

    private Map<String, RecordType> addressRecordTypesByDevName = UtilitiesMVN.getRecordTypesForObject('Address_vod__c');

    public AccountSearchRsltMVN(Account acct, String sUtilityClass, AccountSearchRqstMVN request) {
        this(acct, new List<Address_vod__c>(), sUtilityClass, request);
        if (acct.Address_vod__r != null) {
            arrangeAddressRecords(acct.Address_vod__r);
        }
    }

    public AccountSearchRsltMVN(Account acct, Address_vod__c addressRecord, String sUtilityClass, AccountSearchRqstMVN request) {
        this(acct, new List<Address_vod__c>{addressRecord}, sUtilityClass, request);
    }

    public AccountSearchRsltMVN(Address_vod__c addressRecord, String sUtilityClass, AccountSearchRqstMVN request) {
        this(new Account(Id=addressRecord.Account_vod__c, Network_Id_MVN__c=addressRecord.Account_vod__r.Network_Id_MVN__c),
             new List<Address_vod__c>{addressRecord},
             sUtilityClass,
             request);
    }

    public AccountSearchRsltMVN(Account acct, List<Address_vod__c> addressRecords, String sUtilityClass, AccountSearchRqstMVN request) {
        accountDetail = acct;
        affiliations = new List<SObject>();
        parentAccounts = new List<AccountSearchRsltMVN>();

        try {
            if (UtilitiesMVN.accountSearchSettings.Use_Veeva_Child_Account_MVN__c) {
                affiliations.addAll(acct.getSObjects('Child_Account_vod__r'));
            } else {
                affiliations.addAll(acct.getSObjects('Affiliation_Children_MVN__r'));
            }
        } catch (Exception ex){
            System.debug('### No affilation records exist.');
        }
        
        addressByExtId = new Map<String, Address_vod__c>();
        searchUtilityClass = sUtilityClass;
        this.request = request;
        resultFields = request.resultFieldSettings;

        arrangeAddressRecords(addressRecords);
    }

    public void setAccountTypeSettings(AccountTypeSettingsMVN settings) {
        acctTypeSettings = settings;
    }

    private void arrangeAddressRecords(List<Address_vod__c> addressRecords) {
        for (Address_vod__c address : addressRecords) {
            String addressIdentifier = getAddressIdentifier(address);
            addressByExtId.put(addressIdentifier, address);
        }
    }

    public String getAddressIdentifier(Address_vod__c address) {
        if (String.isBlank(address.External_Id_vod__c) && String.isBlank(address.Id) && String.isBlank(address.Network_Id_MVN__c)) {
            throw new UtilitiesMVN.utilException('Search Result Address Records must have either an Id or an External Id.');
        }

        if(String.IsNotBlank(address.Network_Id_MVN__c)) {
            return address.Network_Id_MVN__c;
        } else if(String.isNotBlank(address.External_Id_vod__c)) {
            return address.External_Id_vod__c;
        }

        return address.Id;
    }

    public Boolean getCanBeReferrer() {
        if (acctTypeSettings!= null && accountDetail != null) {
            Account_Type_Setting_MVN__mdt setting = acctTypeSettings.settingsByRecTypeId.get(accountDetail.RecordTypeId);
            if (setting != null) {
                return setting.Allow_Referred_By_MVN__c;
            }
        }
        return false;
    }

    public String getAccountIdentifier() {
        if (String.isNotBlank(accountDetail.Network_Id_MVN__c)) {
            return accountDetail.Network_Id_MVN__c;
        } else if (String.isNotBlank(accountDetail.External_Id_vod__c)) {
            return accountDetail.External_Id_vod__c;
        }

        return accountDetail.Id;
    }

    public AccountSearchRqstMVN getRequest() {
        return request;
    }

    public List<AccountSearchRsltMVN> getParentAccounts() {
        return parentAccounts;
    }

    public void addAddressRecord(Address_vod__c address) {
        arrangeAddressRecords(new List<Address_vod__c>{address});
    }

    public void addAddressRecords(List<Address_vod__c> addressRecords) {
        arrangeAddressRecords(addressRecords);
    }

    public void addParentAccount(AccountSearchRsltMVN parentResult) {
        addParentAccounts(new List<AccountSearchRsltMVN>{parentResult});
    }

    public void addParentAccounts(List<AccountSearchRsltMVN> parentResults) {
        parentAccounts.addAll(parentResults);
    }

    public List<Account> getAccountAndParentAccounts() {
        List<Account> accounts = new List<Account>{accountDetail};

        for (AccountSearchRsltMVN result : parentAccounts) {
            accounts.add(result.accountDetail);
        }
        return accounts;
    }

    public void addAffiliations(List<SObject> aff) {
        affiliations.addAll(aff);
    }

    public List<SObject> getAffiliations() {
        return affiliations;
    }


    public List<Address_vod__c> getAddressForRecordType(Id recTypeId) {
        return addressByExtId.values();
    }

    public List<Address_vod__c> getAllAddressRecords() {
        List<Address_vod__c> allRecords = new List<Address_vod__c>();
        allRecords.addAll(getAddressForRecordType(null));
        return allRecords;
    }

    public List<Address_vod__c> getAccountAndParentsAddressRecords() {
        List<Address_vod__c> allRecords = getAllAddressRecords();
        for (AccountSearchRsltMVN result : parentAccounts) {
            allRecords.addAll(result.getAllAddressRecords());
        }
        return allRecords;
    }

    public AccountSearchRsltMVN insertAccountDetails() {
        if (searchUtilityClass == null) {
            throw new UtilitiesMVN.utilException('This instance of AccountSearchRsltMVN was constructed with a null searchUtilityClass.');
        }

        return UtilitiesMVN.accountSearchUtilities.get(searchUtilityClass).insertAccountDetails(this);
    }
}