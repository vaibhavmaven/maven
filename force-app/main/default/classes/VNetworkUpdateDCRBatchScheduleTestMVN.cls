/**
 * VNetworkUpdateDCRBatchScheduleTestMVN
 * @created By: Pablo Roldan
 * @created Date: Oct 4th, 2017
 * @description: tests for scheduled class VNetworkUpdateDCRBatchSchedule_MVN
*/
@isTest
private class VNetworkUpdateDCRBatchScheduleTestMVN {

    private static final Integer TEST_BULK = 48;

    private static TestFactoryAccountMVN accTestFactory = new TestFactoryAccountMVN();
    private static TestFactoryAddressMVN addressTestFactory = new TestFactoryAddressMVN();
    private static TestFactoryDCRMVN dcrTestFactory = new TestFactoryDCRMVN();

    private static Set<Id> accountDCRVisited = new Set<Id>();

    private static Integer totalAccountDCRs;
    private static Integer totalDCRs;

    private class networkUpdateDCRBatchScheduleTestException extends Exception {}

    static {
        // Set all custom metadata mocks
        TestFactoryCustomMetadataMVN.setMocks();

        DCRUtilityMVN dcrUtility = new DCRUtilityMVN();
        DCRFieldSettingsMVN.setMockList(dcrUtility.buildMockFieldSettingsForTest());
        DCRGlobalSettingMVN.setMock(dcrUtility.buildMockDCRSettingsForTest());

        Map<Id, Id> addressByAccount = new Map<Id, Id>();

        List<Account> accs = accTestFactory.createPersonAccounts(TEST_BULK / 3, 'Professional_vod');

        List<Account> institutions = createInstitutions(TEST_BULK / 3);

        List<Address_vod__c> addresses = createAddresses(TEST_BULK / 3, accs);

        totalAccountDCRs = accs.size() + institutions.size() + addresses.size();
        System.assertEquals(TEST_BULK, totalAccountDCRs);

        List<AggregateResult> agDCRs = [SELECT Count(Id) countId
                                            FROM Data_Change_Request_MVN__c
                                            WHERE Status_MVN__c =: VeevaNetworkConnectionUtilMVN.DCR_PENDING];

        totalDCRs = totalAccountDCRs + addresses.size();
        System.assertEquals(agDCRs[0].get('countId'), totalDCRs);
    }

    private static List<Account> createInstitutions(Integer numOfInstitutions) {
        List<Account> institutions = new List<Account>();
        String institutionRecordTypeId = UtilitiesMVN.getRecordTypesForObject('Account').get('Hospital_vod').Id;

        for(Integer institutionIndex = 0; institutionIndex < numOfInstitutions; institutionIndex++) {
            institutions.add(accTestFactory.construct(new Map<String, Object> {
                'RecordTypeId' => institutionRecordTypeId,
                'Name' => 'Test Institution'+ institutionIndex
            }));
        }

        insert institutions;

        return institutions;
    }

    private static List<Address_vod__c> createAddresses(Integer numOfAddresses, List<Account> accs) {
        List<Address_vod__c> addresses = new List<Address_vod__c>();

        for(Account acc : accs) {
            addresses.add(addressTestFactory.constructAddress(acc, new Map<String, Object>{'Primary_vod__c' => true}));
        }

        insert addresses;
        return addresses;
    }

    /**
    * It should schedule Veeva Network Update Batch
    * @description: This test method schedules the Veeva Network update batch
    */
    @isTest static void itShouldScheduleVeevaNetworkUpdateBatch() {
        Test.startTest();
        // Execute scheduled class
        VeevaNetworkUpdateDCRBatchScheduleMVN vNetworkUpdateDCRBatchSchedule = new VeevaNetworkUpdateDCRBatchScheduleMVN();

        Datetime dt = Datetime.now().addDays(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();

        String jobId = System.schedule('Test Veeva Network Update DCR Batch Schedule', CRON_EXP, vNetworkUpdateDCRBatchSchedule);
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                           FROM CronTrigger WHERE id = :jobId];

        // Verify the expressions are the same
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }

    /**
    * It should schedule Veeva Network Update Batch
    * @description: This test method schedules the Veeva Network update batch
    */
    @isTest static void itShouldUpdateDCRSuccessfully() {
        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockSuccess());

        List<Data_Change_Request_MVN__c> dcrs = [SELECT Id
                                                   FROM Data_Change_Request_MVN__c
                                                  WHERE DCR_External_ID_MVN__c = null
                                                    AND Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_PENDING
                                                    AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount];

        System.assertEquals(totalAccountDCRs, dcrs.size());
        Test.startTest();
        System.assert(!IntegrationTokensCacheMVN.toggleEnabled());
        VeevaNetworkUpdateDCRBatchScheduleMVN vnUpdateDCRBatch = new VeevaNetworkUpdateDCRBatchScheduleMVN();
        database.executebatch(vnUpdateDCRBatch, 48);
        Test.stopTest();

        dcrs = [SELECT Id, Error_MVN__c, DCR_External_ID_MVN__c
                  FROM Data_Change_Request_MVN__c
                 WHERE Id IN :dcrs];

        String errorMsgs = 'Errors: ';
        for(Data_Change_Request_MVN__c dcr : dcrs) {
            if(dcr.DCR_External_ID_MVN__c == null && String.IsNotBlank(dcr.Error_MVN__c)) {
                errorMsgs += dcr.Error_MVN__c +'\n';
            }
        }

        dcrs = [SELECT Id, Account_MVN__c, Address_MVN__c
                  FROM Data_Change_Request_MVN__c
                 WHERE Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_SUBMITTED];

        List<AggregateResult> agDCRs = [SELECT Count(Id), RecordType.DeveloperName, Status_MVN__c, Country_MVN__c FROM Data_Change_Request_MVN__c GROUP BY RecordType.DeveloperName, Status_MVN__c, Country_MVN__c];

        System.assertEquals(totalDCRs, dcrs.size(), errorMsgs);
    }

    /**
    * It should schedule Veeva Network Update Batch
    * @description: This test method schedules the Veeva Network update batch
    */
    @isTest static void itShouldUpdateOneDCRSuccessfully() {
        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkTokenUtilityMockSuccess());

        List<Data_Change_Request_MVN__c> dcrs = [SELECT Id
                                                   FROM Data_Change_Request_MVN__c
                                                  WHERE DCR_External_ID_MVN__c = null
                                                      AND Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_PENDING
                                                    AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount];

        Data_Change_Request_MVN__c dcr = dcrs.remove(0);

        System.assertEquals(totalAccountDCRs - 1, dcrs.size());
        System.assertNotEquals(null, dcr);

        List<Data_Change_Request_Line_MVN__c> dcrLines = [SELECT Id
                                                            FROM Data_Change_Request_Line_MVN__c
                                                           WHERE Data_Change_Request_MVN__c IN :dcrs];
        delete dcrLines;
        delete dcrs;

        Test.startTest();
        VeevaNetworkUpdateDCRBatchScheduleMVN vnUpdateDCRBatch = new VeevaNetworkUpdateDCRBatchScheduleMVN();
        database.executebatch(vnUpdateDCRBatch, 48);
        Test.stopTest();

        dcr = [SELECT Id, Error_MVN__c, DCR_External_ID_MVN__c
                  FROM Data_Change_Request_MVN__c
                 WHERE Id = :dcr.Id AND DCR_External_ID_MVN__c != null AND Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_SUBMITTED];
        System.assertNotEquals(null, dcr, 'Error: '+ dcr.Error_MVN__c);
    }

    /**
    * It should schedule Veeva Network Update Batch
    * @description: This test method schedules the Veeva Network update batch and returns an error
    */
    @isTest static void itShouldUpdateDCRInactiveError() {
        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkUtilityMockInactiveService());

        checkError(Label.Veeva_Network_Inactive_User_Error_MVN);
    }

    /**
    * It should schedule Veeva Network Update Batch
    * @description: This test method schedules the Veeva Network update batch and returns an error
    */
    @isTest static void itShouldUpdateDCRUserLockedError() {
        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkUtilityMockUserLockedOut());

        checkError(Label.User_Locked_Out_Veeva_Network_Error_MVN);
    }

    /**
    * It should schedule Veeva Network Update Batch
    * @description: This test method schedules the Veeva Network update batch and returns an error
    */
    @isTest static void itShouldUpdateDCRInsufficientAccessError() {
        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkUtilityMockInsufficientAccess());

        checkError(Label.Access_Privileges_Veeva_Network_Error_MVN);
    }

    /**
    * It should schedule Veeva Network Update Batch
    * @description: This test method schedules the Veeva Network update batch
    */
    @isTest static void itShouldUpdateDCRInvalidSessionError() {
        Test.setMock(HttpCalloutMock.class, new VeevaNetworkWebServiceMockMVN.NetworkUtilityMockInvalidSession());
        Integer dcrSize = TEST_BULK;

        List<Data_Change_Request_MVN__c> dcrs = [SELECT Id
                                                   FROM Data_Change_Request_MVN__c
                                                  WHERE DCR_External_ID_MVN__c = null
                                                      AND Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_PENDING
                                                    AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount];

        Data_Change_Request_MVN__c dcr = dcrs.remove(0);

        System.assertEquals(dcrSize - 1, dcrs.size());
        System.assertNotEquals(null, dcr);

        List<Data_Change_Request_Line_MVN__c> dcrLines = [SELECT Id
                                                            FROM Data_Change_Request_Line_MVN__c
                                                           WHERE Data_Change_Request_MVN__c IN :dcrs];
        delete dcrLines;
        delete dcrs;

        Test.startTest();
        VeevaNetworkUpdateDCRBatchScheduleMVN vnUpdateDCRBatch = new VeevaNetworkUpdateDCRBatchScheduleMVN();
        database.executebatch(vnUpdateDCRBatch, 48);
        Test.stopTest();

        dcr = [SELECT Id, Error_MVN__c, DCR_External_ID_MVN__c
                  FROM Data_Change_Request_MVN__c
                 WHERE Id = :dcr.Id
                   AND Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_CANCELLED];

        System.assert(String.IsNotBlank(dcr.Error_MVN__c));
        System.assert(dcr.Error_MVN__c.contains(Label.Invalid_Session_Veeva_Network_Error_MVN));
    }

    private static void checkError(String errorMessage) {
        List<Data_Change_Request_MVN__c> dcrs = [SELECT Id
                                                   FROM Data_Change_Request_MVN__c
                                                  WHERE DCR_External_ID_MVN__c = null
                                                    AND Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_PENDING
                                                    AND RecordType.DeveloperName = :VeevaNetworkConnectionUtilMVN.networkSettings.dcrAccount];

        System.assertEquals(TEST_BULK, dcrs.size());
        Test.startTest();
        System.assert(!IntegrationTokensCacheMVN.toggleEnabled());
        VeevaNetworkUpdateDCRBatchScheduleMVN vnUpdateDCRBatch = new VeevaNetworkUpdateDCRBatchScheduleMVN();
        database.executebatch(vnUpdateDCRBatch, 48);
        Test.stopTest();

        dcrs = [SELECT Id, Error_MVN__c, DCR_External_ID_MVN__c
                  FROM Data_Change_Request_MVN__c
                 WHERE Id IN :dcrs
                   AND Status_MVN__c = :VeevaNetworkConnectionUtilMVN.DCR_CANCELLED];

        System.assert(!dcrs.isEmpty());
        for(Data_Change_Request_MVN__c dcr : dcrs) {
            System.assert(String.IsNotBlank(dcr.Error_MVN__c));
            System.assert(dcr.Error_MVN__c.contains(errorMessage));
        }
    }
}