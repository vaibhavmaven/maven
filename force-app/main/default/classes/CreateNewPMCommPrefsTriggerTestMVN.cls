/*
 *  CreateNewPMCommPrefsTriggerTestMVN
 *  Created By:     Kyle Thornton
 *  Created Date:   March 6, 2017
 *  Description:    Unit tests for CreateNewPMCommPrefsTriggerMVN
 */
@isTest
private class CreateNewPMCommPrefsTriggerTestMVN
{
    static List<Account> patients;
    static List<Contact_Information_MVN__c> contactInfo;
    static Program_MVN__c program;
    static List<Program_Member_MVN__c> programMembers;
    static User caseManager;

    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
    static TestFactoryContactInformationMVN contactInfoFactory = new TestFactoryContactInformationMVN();
    static TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
    static TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();
    static TestFactoryUserMVN userFactory = new TestFactoryUserMVN();

    static {
        System.runAs(new User(Id=UserInfo.getUserId())) {
            Id caseManagerProfileId = TestFactoryUserMVN.profilesByName.get('Patient Case Manager - MVN').Id;
            caseManager = userFactory.create(new Map<String,Object>{'ProfileId'=>caseManagerProfileId});
        }

        patients = accountFactory.createPatients(20);
        contactInfo = new List<Contact_Information_MVN__c>();
        for (Account patient : patients) {
            contactInfo.add(contactInfoFactory.constructEmail(patient));
            contactInfo.add(contactInfoFactory.constructPhone(patient));
        }

        insert contactInfo;

        program = programFactory.create();

    }

    @isTest
    static void itShouldCreateNewCommunicationPrefs()
    {
        Test.startTest();
        programMembers = programMemberFactory.create(program, null, patients);
        Test.stopTest();

        List<Communication_Preference_MVN__c> commPrefs = [SELECT Id
                                                             FROM Communication_Preference_MVN__c
                                                            WHERE Contact_Information_MVN__c IN :contactInfo
                                                              AND Program_Member_MVN__c IN :programMembers];

        System.assertEquals(40, commPrefs.size());

    }
}