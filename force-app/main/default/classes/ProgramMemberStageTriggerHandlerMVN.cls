/**
 *	ProgramMemberStageTriggerHandlerMVN
 *	Created By:		Aggen
 *	Created On:		10/27/2015
 *	Updated: 		February 11, 2016 by Vincent Reeder
 *  Description:	This class is responsible for creating the associated activities (Case or Task) when the Program Member Stage Status is set to 'Started'.
 *					It is also responsible for starting the next activity(ies) when the Program Member Stage Status is set to 'Closed'.
 **/
public class ProgramMemberStageTriggerHandlerMVN implements TriggerHandlerMVN.HandlerInterface {
    private final Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();
    private final Map<String,Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    private Map<String,Id> caseRecordTypeMap;
    private Map<String,Id> taskRecordTypeMap;

    public void handle() {
        List<Program_Member_Stage_MVN__c> createNewActivities = new List<Program_Member_Stage_MVN__c>();
        Set<Id> programMemberIdsToInitialize = new Set<Id>();

        RecordTypesMVN.cache(new Set<SObjectType>{Case.SObjectType, Task.SObjectType});

        // Record Type map instantiation
        caseRecordTypeMap = new Map<String,Id>();
        taskRecordTypeMap = new Map<String,Id>();

        // Record Type query
        for(RecordType rt : RecordTypesMVN.recordTypeMap.values()) {
            if(rt.SObjectType == 'Case') {
                caseRecordTypeMap.put(rt.DeveloperName, rt.Id);
            } else if(rt.SObjectType == 'Task') {
                taskRecordTypeMap.put(rt.DeveloperName, rt.Id);
            }
        }

           for(Program_Member_Stage_MVN__c pms : (List<Program_Member_Stage_MVN__c>) Trigger.new) {
            Program_Member_Stage_MVN__c oldPms = (Program_Member_Stage_MVN__c) Trigger.oldMap.get(pms.Id);
            Boolean statusChanged = pms.Status_MVN__c != oldPms.Status_MVN__c;

            // Check to make sure the status changed
            if(statusChanged) {
                // Create the associated actitivies (Case or Task) when the Program Member Stage status = 'Started'
                if(pms.Status_MVN__c == settings.Program_Member_Stage_Start_Status_MVN__c
                    && pms.Exclude_Activity_MVN__c == false && pms.Activity_ID_MVN__c == null) {
                    createNewActivities.add(pms);
                }
            }
            programMemberIdsToInitialize.add(pms.Program_Member_MVN__c);
        }

        if(createNewActivities.size() > 0) {
            createActivities(createNewActivities);
        }

        if(!ProgramMemberStageManagerMVN.startAtSpecificStage) {
            ProgramMemberStageManagerMVN.initializeProgramMembers(programMemberIdsToInitialize);
        }

    }

    public void createActivities(List<Program_Member_Stage_MVN__c> createNewActivities) {
        Map<Id,List<Program_Member_Stage_MVN__c>> programStageIdsAndPMSMap = new Map<Id,List<Program_Member_Stage_MVN__c>>();
        Map<Id,Program_Member_MVN__c> programMemberMap = new Map<Id,Program_Member_MVN__c>();
        Map<Id,List<SObject>> newSObjectMap = new Map<Id,List<SObject>>();
        List<String> programMemberIds = new List<String>();

        // Organize Program Stages and Program Member Stages
        for(Program_Member_Stage_MVN__c pms : createNewActivities) {
            if(programStageIdsAndPMSMap.containsKey(pms.Program_Stage_MVN__c)) {
                List<Program_Member_Stage_MVN__c> pmsList = programStageIdsAndPMSMap.get(pms.Program_Stage_MVN__c);
                pmsList.add(pms);

                programStageIdsAndPMSMap.put(pms.Program_Stage_MVN__c,pmsList);
            } else {
                List<Program_Member_Stage_MVN__c> pmsList = new List<Program_Member_Stage_MVN__c>();
                pmsList.add(pms);

                programStageIdsAndPMSMap.put(pms.Program_Stage_MVN__c,pmsList);
            }

            programMemberIds.add(pms.Program_Member_MVN__c);
        }

        // Query for Program Member details
        if(!programMemberIds.isEmpty()) {
            String programMemberQuery = 'SELECT Id, ';
            programMemberQuery += String.escapeSingleQuotes(buildQueryFields(Schema.SObjectType.Program_Member_MVN__c.fieldSets.Queryable_Program_Member_Fields_MVN.getFields()));
            programMemberQuery += ' FROM Program_Member_MVN__c WHERE Id IN :programMemberIds';

            programMemberMap = new Map<Id,Program_Member_MVN__c>((List<Program_Member_MVN__c>)Database.Query(programMemberQuery));
        }

        // Query for Program Stage and Program Stage Field Mappings
        if(programStageIdsAndPMSMap.size() > 0) {
            for(Program_Stage_MVN__c ps : [select Id, Activity_Type_MVN__c, Assigned_To_MVN__c, Description_MVN__c, Exclude_Activity_MVN__c,
                                                  Flow_Name_MVN__c, Is_Parent_Stage_MVN__c, Name, Parent_Stage_MVN__c, Program_MVN__c, RecordTypeId,
                                                  Related_Object_Close_Status_MVN__c, Related_Object_MVN__c, Related_Object_Open_Status_MVN__c,
                                                  Stage_Sequence_Number_MVN__c,
                                                  (select Id, Field_MVN__c, Name, Object_MVN__c, Program_Stage_MVN__c, Value_MVN__c from Program_Stage_Field_Mapping__r)
                                           from Program_Stage_MVN__c where Id in :programStageIdsAndPMSMap.keySet()]) {

                for(Program_Member_Stage_MVN__c pms : programStageIdsAndPMSMap.get(ps.Id)) {
                    // Build a list of activities and set the appropriate field values
                    newSObjectMap.putAll(buildNewSObjects(ps,pms,programMemberMap.get(pms.Program_Member_MVN__c)));
                }
            }
        }

        // Create new activities
        if(newSObjectMap.size() > 0) {
            List<SObject> newSObjectList = new List<SObject>();
            List<Program_Member_Stage_MVN__c> workingPMSList = new List<Program_Member_Stage_MVN__c>();
            List<Program_Member_Stage_MVN__c> updatePMSList = new List<Program_Member_Stage_MVN__c>();

            for(List<SObject> sObjectList : newSObjectMap.values()) {
                newSObjectList.addAll(sObjectList);
            }

            insert newSObjectList;

            // Combine all Program Member Stages into a single working list
            for(List<Program_Member_Stage_MVN__c> pmsList : programStageIdsAndPMSMap.values()) {
                workingPMSList.addAll(pmsList);
            }

            // Update the Program Member Stage fields Activity_ID_MVN__c or Related_Object_ID_MVN__c
            for(Program_Member_Stage_MVN__c thisPMS : workingPMSList) {
                if(newSObjectMap.containsKey(thisPMS.Id)) {
                    Program_Member_Stage_MVN__c updatePMS = new Program_Member_Stage_MVN__c();
                    updatePMS.Id = thisPMS.Id;

                    for(SObject sObj : newSObjectMap.get(updatePMS.Id)) {
                        String objectName = sObj.Id.getSObjectType().getDescribe().getName();

                        if(objectName.toLowerCase() == 'case' || objectName.toLowerCase() == 'task') {
                            updatePMS.Activity_ID_MVN__c = sObj.Id;
                            updatePMSList.add(updatePMS);
                        } else {
                            updatePMS.Related_Object_ID_MVN__c = sObj.Id;
                            updatePMSList.add(updatePMS);
                        }
                    }
                }
            }

            update updatePMSList;
        }
    }

    public Map<Id,List<SObject>> buildNewSObjects(Program_Stage_MVN__c ps, Program_Member_Stage_MVN__c pms, Program_Member_MVN__c pm) {
        Map<Id,List<SObject>> newSObjects = new Map<Id,List<SObject>> ();
        Map<String,List<Program_Stage_Field_Map_MVN__c>> newSObjectFields = new Map<String,List<Program_Stage_Field_Map_MVN__c>>();

        SObject pmSObject = (SObject) pm;
        Map<String,Schema.SObjectField> programMemberFieldMap = schemaMap.get('Program_Member_MVN__c').getDescribe().fields.getMap();

        // Organize the fields by object
        if(ps.Program_Stage_Field_Mapping__r.size() > 0) {
            for(Program_Stage_Field_Map_MVN__c fieldMap : ps.Program_Stage_Field_Mapping__r) {
                if(newSObjectFields.containsKey(fieldMap.Object_MVN__c)) {
                    List<Program_Stage_Field_Map_MVN__c> psfm = newSObjectFields.get(fieldMap.Object_MVN__c);
                    psfm.add(fieldMap);
                    newSObjectFields.put(fieldMap.Object_MVN__c,psfm);
                } else {
                    List<Program_Stage_Field_Map_MVN__c> psfm = new List<Program_Stage_Field_Map_MVN__c>();
                    psfm.add(fieldMap);
                    newSObjectFields.put(fieldMap.Object_MVN__c,psfm);
                }
            }
        // Default behavior if there are no field mappings
        } else {
            String objectName = ps.Activity_Type_MVN__c;

            if(schemaMap.containsKey(objectName)) {
                SObject newSObject = schemaMap.get(objectName).newSObject();
                Map<String,Schema.SObjectField> newSObjectFieldMap = schemaMap.get(objectName).getDescribe().fields.getMap();

                // set the default values for the  Case or Task
                if(objectName.toLowerCase() == 'case') {
                    newSObject = setDefaultCaseFields(newSObject, ps, pms, pm);

                } else if(objectName.toLowerCase() == 'task') {
                    newSObject = setDefaultTaskFields(newSObject, ps, pms, pm);
                }
                List<SObject> sobjectList = new List<SObject>();
                sobjectList.add(newSObject);
                newSObjects.put(pms.Id,sobjectList);
            }
        }

        // Build new SObject and set corresponding field values
        if(newSObjectFields.size() > 0) {
            for(String objectName : newSObjectFields.keySet()) {
                if(schemaMap.containsKey(objectName)) {
                    SObject newSObject = schemaMap.get(objectName).newSObject();
                    Map<String,Schema.SObjectField> newSObjectFieldMap = schemaMap.get(objectName).getDescribe().fields.getMap();

                    // set the default values for the  Case or Task
                    if(objectName.toLowerCase() == 'case') {
                        newSObject = setDefaultCaseFields(newSObject, ps, pms, pm);

                    } else if(objectName.toLowerCase() == 'task') {
                        newSObject = setDefaultTaskFields(newSObject, ps, pms, pm);
                    }

                    for(Program_Stage_Field_Map_MVN__c psfm : newSObjectFields.get(objectName)) {
                        // if the field starts with $PM., then the field maps to a corresponding Program Member field
                        if(psfm.Value_MVN__c.startsWith('$PM.')) {
                            // check if the field type is a date or datetime field and the value has a + sign
                            // if there's a + sign add n number of days to the date or datetime value
                            if(psfm.Value_MVN__c.contains('+')) {
                                String value = psfm.Value_MVN__c.subString(4,psfm.Value_MVN__c.indexOf('+'));
                                Integer addDays = Integer.valueOf(psfm.Value_MVN__c.subString(psfm.Value_MVN__c.indexOf('+')+1,psfm.Value_MVN__c.length()));

                                if(programMemberFieldMap.containsKey(value.toLowerCase())) {
                                    Boolean isDateType = (programMemberFieldMap.get(value).getDescribe().getType() == Schema.DisplayType.DATE) ? true : false;
                                    Boolean isDateTimeType = (programMemberFieldMap.get(value).getDescribe().getType() == Schema.DisplayType.DATETIME) ? true : false;

                                    if(isDateType) {
                                        newSObject.put(psfm.Field_MVN__c, Date.valueOf(pmSObject.get(value)).addDays(addDays));
                                    }
                                    if(isDateTimeType) {
                                        newSObject.put(psfm.Field_MVN__c, Datetime.valueOf(pmSObject.get(value)).addDays(addDays));
                                    }
                                }
                            } else {
                                String value = psfm.Value_MVN__c.subString(4,psfm.Value_MVN__c.length());

                                // Make sure the field actually exists on the Program Member object
                                if(programMemberFieldMap.containsKey(value.toLowerCase())) {
                                    // TODO: apply safe guards for different data types
                                    newSObject.put(psfm.Field_MVN__c, pmSObject.get(value));
                                }
                            }
                        }
                        // Check if the field type is a date or datetime field and the value  is set to TODAY()
                        // If there's a + sign add n number of days to the date or datetime value
                        else if(psfm.Value_MVN__c.startsWithIgnoreCase('$TODAY()')) {
                            if(psfm.Value_MVN__c.contains('+')) {
                                Integer addDays = Integer.valueOf(psfm.Value_MVN__c.subString(psfm.Value_MVN__c.indexOf('+')+1,psfm.Value_MVN__c.length()));

                                // make sure the field actually exists on the object
                                if(newSObjectFieldMap.containsKey(psfm.Field_MVN__c.toLowerCase())) {
                                    Boolean isDateType = (newSObjectFieldMap.get(psfm.Field_MVN__c.toLowerCase()).getDescribe().getType() == Schema.DisplayType.DATE) ? true : false;
                                    Boolean isDateTimeType = (newSObjectFieldMap.get(psfm.Field_MVN__c.toLowerCase()).getDescribe().getType() == Schema.DisplayType.DATETIME) ? true : false;

                                    if(isDateType) {
                                        newSObject.put(psfm.Field_MVN__c, Date.today().addDays(addDays));
                                    }
                                    if(isDateTimeType) {
                                        newSObject.put(psfm.Field_MVN__c, Datetime.now().addDays(addDays));
                                    }
                                }
                            } else {
                                // Make sure the field actually exists on the object
                                if(newSObjectFieldMap.containsKey(psfm.Field_MVN__c.toLowerCase())) {
                                    Boolean isDateType = (newSObjectFieldMap.get(psfm.Field_MVN__c.toLowerCase()).getDescribe().getType() == Schema.DisplayType.DATE) ? true : false;
                                    Boolean isDateTimeType = (newSObjectFieldMap.get(psfm.Field_MVN__c.toLowerCase()).getDescribe().getType() == Schema.DisplayType.DATETIME) ? true : false;

                                    if(isDateType) {
                                        newSObject.put(psfm.Field_MVN__c, Date.today());
                                    }
                                    if(isDateTimeType) {
                                        newSObject.put(psfm.Field_MVN__c, Datetime.now());
                                    }
   
                                }
                            }
                        }
                        // If the field starts with $RecordType, then map the RecordType.DeveloperName to the appropriate RecordTypeId
                        else if(psfm.Field_MVN__c.startsWith('$RecordType')) {
                            if(objectName.toLowerCase() == 'case') {
                                if(caseRecordTypeMap.containsKey(psfm.Value_MVN__c)) {
                                    newSObject.put('RecordTypeId', caseRecordTypeMap.get(psfm.Value_MVN__c));
                                }
                            }
                            if(objectName.toLowerCase() == 'task') {
                                if(taskRecordTypeMap.containsKey(psfm.Value_MVN__c)) {
                                    newSObject.put('RecordTypeId', taskRecordTypeMap.get(psfm.Value_MVN__c));
                                }
                            }
                        } else {
                            // Make sure the field actually exists on the object
                            if(newSObjectFieldMap.containsKey(psfm.Field_MVN__c.toLowerCase())) {
                                // TODO: apply safe guards for different data types
                                Schema.SObjectField fieldSchema = newSObjectFieldMap.get(psfm.Field_MVN__c.toLowerCase());
                                Schema.DisplayType fieldType = fieldSchema.getDescribe().getType();
                                if (fieldType == Schema.DisplayType.Double) {
                                    newSObject.put(psfm.Field_MVN__c, Decimal.valueOf(psfm.Value_MVN__c));
                                } else if (fieldType == Schema.DisplayType.Boolean) {
                                    newSObject.put(psfm.Field_MVN__c, Boolean.valueOf(psfm.Value_MVN__c));
                                } else if (fieldType == Schema.DisplayType.Date) {
                                    newSObject.put(psfm.Field_MVN__c, Date.valueOf(psfm.Value_MVN__c));
                                } else if (fieldType == Schema.DisplayType.DateTime) {
                                    newSObject.put(psfm.Field_MVN__c, DateTime.valueOf(psfm.Value_MVN__c));
                                } else if (fieldType == Schema.DisplayType.Integer) {
                                    newSObject.put(psfm.Field_MVN__c, Integer.valueOf(psfm.Value_MVN__c));
                                } else if (fieldType == Schema.DisplayType.Id) {
                                    newSObject.put(psfm.Field_MVN__c, Id.valueOf(psfm.Value_MVN__c));
                                } else if (fieldType == Schema.DisplayType.Percent) {
                                    newSObject.put(psfm.Field_MVN__c, Decimal.valueOf(psfm.Value_MVN__c));
                                } else {
                                    newSObject.put(psfm.Field_MVN__c, String.valueOf(psfm.Value_MVN__c));
                                }
                            }
                        }
                    }

                    if(newSObjects.containsKey(pms.Id)) {
                        List<SObject> sobjectList = newSObjects.get(pms.Id);
                        sobjectList.add(newSObject);
                        newSObjects.put(pms.Id,sobjectList);
                    } else {
                        List<SObject> sobjectList = new List<SObject>();
                        sobjectList.add(newSObject);
                        newSObjects.put(pms.Id,sobjectList);
                    }
                }
            }
        }
        return newSObjects;
    }

    public SObject setDefaultCaseFields(SObject newSObject, Program_Stage_MVN__c ps, Program_Member_Stage_MVN__c pms, Program_Member_MVN__c pm) {
        newSObject.put('Program_Member_Stage_MVN__c',pms.Id);
        newSObject.put('AccountId',pm.Member_MVN__c);
        newSObject.put('Subject',ps.Name);
        newSObject.put('Description',ps.Description_MVN__c);
        newSObject.put('Program_Member_MVN__c',pm.Id);
        newSObject.put('RecordTypeId',caseRecordTypeMap.get(settings.Case_Default_Record_Type_MVN__c));
        newSObject.put('Flow_Name_MVN__c',ps.Flow_Name_MVN__c);
        newSObject.put('Program_MVN__c',pm.Program_MVN__c);
        newSObject.put('Referred_By_MVN__c',pm.Physician_MVN__c);


        return newSObject;
    }

    public SObject setDefaultTaskFields(SObject newSObject, Program_Stage_MVN__c ps, Program_Member_Stage_MVN__c pms, Program_Member_MVN__c pm) {
        newSObject.put('Program_Member_Stage_ID_MVN__c',pms.Id);
        newSObject.put('WhoId',pm.Member_Contact_ID_MVN__c);
        newSObject.put('Subject',ps.Name);
        newSObject.put('Description',ps.Description_MVN__c);
        newSObject.put('WhatId',pm.Id);
        newSObject.put('RecordTypeId',taskRecordTypeMap.get(settings.Task_Default_Record_Type_MVN__c));
        newSObject.put('Flow_Name_MVN__c',ps.Flow_Name_MVN__c);

        return newSObject;
    }

    public String buildQueryFields(List<FieldSetMember> thisFieldSet) {
        // Builds the fields needed on the page by looping through
        // the field set members that are used on the page.
        Boolean hasFirstField = false;
        String queryFields = '';
        for (FieldSetMember field : thisFieldSet) {
            if (hasFirstField) {
                queryFields += ',';
            } else {
                hasFirstField = true;
            }
            queryFields += field.getFieldPath();
        }

        return queryFields;
    }
}