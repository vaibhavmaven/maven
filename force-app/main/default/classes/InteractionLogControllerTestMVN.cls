/*
*   InteractionLogControllerTestMVN
*   Created By:     Thomas Hajcak (thomas@mavensconsulting.com)
*   Created Date:   March 23, 2016
*   Description:    This class provides unit tests for the Interaction Log Controller and Page.
*/

@isTest
private class InteractionLogControllerTestMVN {
    static InteractionLogControllerMVN extension;
    static Case cs;

    static {
        cs = TestDataFactoryMVN.createTestCase();

        Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getOrgDefaults();
        settings.Display_AE_Button_MVN__c = true;
        upsert settings;

        ApexPages.StandardController con = new ApexPages.StandardController(cs);
        extension = new InteractionLogControllerMVN(con);
    }

    @isTest static void testCreateEventOfTypeAdverseEvent() {
        Test.startTest();
        extension.requestType = 'AE';
        extension.createChildCaseFromInteractionLog();
        Test.stopTest();

        List<Event_MVN__c> events = [SELECT RecordType.DeveloperName FROM Event_MVN__c];
        System.assertEquals(1, events.size());
        System.assertEquals('Adverse_Event_MVN', events[0].RecordType.DeveloperName);
    }

    @isTest static void testCreateEventOfTypeProductQualityComplaint() {
        Test.startTest();
        extension.requestType = 'PQC';
        extension.createChildCaseFromInteractionLog();
        Test.stopTest();

        List<Event_MVN__c> events = [SELECT RecordType.DeveloperName FROM Event_MVN__c];
        System.assertEquals(1, events.size());
        System.assertEquals('Product_Quality_Complaint_MVN', events[0].RecordType.DeveloperName);
    }

    @isTest static void testCreateEventOfTypeMissedDose() {
        Test.startTest();
        extension.requestType = 'MD';
        extension.createChildCaseFromInteractionLog();
        Test.stopTest();

        List<Event_MVN__c> events = [SELECT RecordType.DeveloperName FROM Event_MVN__c];
        System.assertEquals(1, events.size());
        System.assertEquals('Missed_Dose_MVN', events[0].RecordType.DeveloperName);
    }

    @isTest static void testCreateChildCaseExceptionHandling() {
        Test.setReadOnlyApplicationMode(true);

        Test.startTest();
        extension.requestType = 'PQC';
        extension.createChildCaseFromInteractionLog();
        Test.stopTest();

        System.assertEquals(0, [SELECT COUNT() FROM Event_MVN__c]);
        System.assert(ApexPages.getMessages().size() == 1, 'The page should show an error message');
    }

    @isTest static void testMessages() {
        Test.startTest();
        extension.setToTrue();
        extension.showStatusMessage();
        Test.stopTest();

        System.assertEquals(true, extension.needsToBeSaved);
        System.assertEquals(true, extension.isSaving);

    }

    @isTest static void testSaveInteractionNotes() {
        extension.needsToBeSaved = true;
        extension.currentNote.Notes_MVN__c = 'This is a test.';

        Test.startTest();
        extension.saveInteractionNotes();
        Test.stopTest();

        System.assertEquals([select Notes_MVN__c from Case_Note_MVN__c where Case_MVN__c = :cs.Id limit 1].Notes_MVN__c	, 'This is a test.');
    }
    @isTest static void testSaveInteractionNotesTooLong() {
        extension.needsToBeSaved = true;
        extension.currentNote.Notes_MVN__c = 'This is a test.';

        for (Integer x = 0; x < 30000; x++) {
            extension.currentNote.Notes_MVN__c += 'This is a test.';
        }

        Test.startTest();
        extension.saveInteractionNotes();
        Test.stopTest();

        System.assert(extension.hasSaveError && extension.needsToBeSaved);

        Boolean hasCorrectError = false;
        for (ApexPages.Message error : ApexPages.getMessages()) {
            if (error.getSummary().contains(Label.Interaction_Log_Too_Long_Error_MVN)) {
                hasCorrectError = true;
                break;
            }
        }
        System.assert(hasCorrectError, 'The page does not have the correct error message.');
    }

    @isTest static void testSaveInteractionNotesExceptionHandling() {
        extension.needsToBeSaved = true;
        extension.currentNote.Notes_MVN__c = 'This is a test.';

        Test.setReadOnlyApplicationMode(true);
        Test.startTest();
        extension.saveInteractionNotes();
        Test.stopTest();

        System.assert(extension.hasSaveError && extension.needsToBeSaved);
        System.assert(ApexPages.getMessages().size() == 1, 'The page should show an error message');
    }

    @isTest static void testClosingNote() {
        extension.currentNote.Notes_MVN__c = 'This is a test.';

        Test.startTest();
        extension.closeNote();
        Test.stopTest();

        Case_Note_MVN__c closedNote = [select Notes_MVN__c, Locked_MVN__c from Case_Note_MVN__c where Case_MVN__c = :cs.Id limit 1];

        System.assertEquals(closedNote.Notes_MVN__c, 'This is a test.');
        System.assertEquals(closedNote.Locked_MVN__c, true);
    }

    @isTest static void testConstructorWithClosedCaseAndInteractionNotes() {
        TestDataFactoryMVN.buildPSSettings();
        String closedStatus = TestDataFactoryMVN.psSettings.Case_Completed_Status_MVN__c;
        cs.Status = closedStatus;
        cs.Type = cs.Type = Case.Type.getDescribe().getPicklistValues().get(0).getValue();
        update cs;

        Case_Note_MVN__c note = TestDataFactoryMVN.createCaseNote(cs);

        Test.startTest();
        ApexPages.StandardController con = new ApexPages.StandardController(cs);
        extension = new InteractionLogControllerMVN(con);
        Test.stopTest();

        System.assert(extension.caseIsLocked && extension.disableInteractionLog, 'The constructor should initiate state based on the given case');
        System.assertEquals(null, extension.currentNote.Notes_MVN__c, 'The constructor should initiate state based on the given case');
    }

    @isTest static void testHidingButtons() {
        Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getOrgDefaults();

        settings.Display_AE_Button_MVN__c = false;
        settings.Display_MD_Button_MVN__c = false;
        settings.Display_PQC_Button_MVN__c = false;

        System.assert(extension.getHasCaseNoteButtons());

        Test.startTest();
        update settings;
        Test.stopTest();

        System.assert(!extension.getHasCaseNoteButtons());
    }

    /**
  * @description tests saving and closing a note when case gets closed
  */
    @isTest static void testSaveAndCloseNotes() {
        String expectedNoteText = 'This is a test.';
        Case_Note_MVN__c note = new Case_Note_MVN__c(
            Case_MVN__c = cs.Id,
            Notes_MVN__c = expectedNoteText,
            Locked_MVN__c = false
        );
        insert note;

        Test.startTest();
            InteractionLogControllerMVN.saveAndCloseNotes(cs.Id);
        Test.stopTest();

        Case_Note_MVN__c savedAndClosedNote = [SELECT Notes_MVN__c, Locked_MVN__c
                                                                                         FROM Case_Note_MVN__c
                                                                                        WHERE Case_MVN__c = :cs.Id
                                                                                        LIMIT 1];
        System.assertEquals(expectedNoteText, savedAndClosedNote.Notes_MVN__c);
        System.assertEquals(true, savedAndClosedNote.Locked_MVN__c);
    }

    /**
  * @description tests deleting an empty note when case gets closed
  */
    @isTest static void testSaveAndCloseNotesDeletesEmptyNotes() {
        Case_Note_MVN__c note = new Case_Note_MVN__c(
            Case_MVN__c = cs.Id,
            Notes_MVN__c = '',
            Locked_MVN__c = false
        );
        insert note;

        Test.startTest();
            InteractionLogControllerMVN.saveAndCloseNotes(cs.Id);
        Test.stopTest();

        System.assertEquals(0, [SELECT count() FROM Case_Note_MVN__c WHERE Case_MVN__c =: cs.Id]);
    }
}