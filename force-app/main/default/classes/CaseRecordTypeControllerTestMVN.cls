/*
* CaseRecordTypeControllerTestMVN
* Created By:   Roman Lerman
* Created Date: April 5, 2013
* Description:  This is the test class for the CaseRecordTypeControllerMVN class
*/
@isTest
private class CaseRecordTypeControllerTestMVN {
    static Case cs;

    static{        
        cs = TestDataFactoryMVN.createTestCase();
    }

    static testMethod void testCaseRecordTypeController(){
        ApexPages.StandardController con = new ApexPages.StandardController(cs);
        CaseRecordTypeControllerMVN extension;

        Test.startTest();
        	extension = new CaseRecordTypeControllerMVN(con);
        Test.stopTest();

        System.assertEquals([select toLabel(RecordType.Name) from Case where Id = :cs.Id].RecordType.Name,
                            extension.recordTypeName);
    }
}