/**
 * @author Mavens
 * @description tests ApplicationSetAppealProcessorMVN
 * @group Application
 */
@isTest
private class ApplicationSetAppealProcessorTestMVN {

    private static User caseManager;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static List<Application_MVN__c> applications;

    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        return programFactory.create(
            new Map<String,Object>{
                'Name' => 'A Program',
                'Program_Id_MVN__c' => 'Knipper_PAP_MVN',
                'Active_MVN__c' => true,
                'Job_No_MVN__c' => '12345'
            }
        );
    }

    static Program_Member_MVN__c createProgramMember() {
        Program_Member_MVN__c programMemberTest =
            TestDataFactoryMVN.createProgramMember(program, TestDataFactoryMVN.createMember());
        update programMemberTest;
        return programMemberTest;
    }

    static List<Application_MVN__c> createApplications() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        List<Application_MVN__c> apps = new List<Application_MVN__c>();

        for (Integer i=0; i < 10; i++) {
            apps.add(
                applicationFactory.construct(
                    new Map<String, Object>{
                        'Program_Member_MVN__c' => programMember.Id
                    }
                )
            );
        }
        insert apps;
        return apps;
    }

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();

        System.runAs(caseManager) {
            program = createProgram();
            programMember = createProgramMember();
            applications = createApplications();
        }
    }

    @isTest
	static void itShouldSetTheAppealProcessor() {
        System.assertEquals(10, [SELECT Count() FROM Application_MVN__c WHERE Appeal_Processor_MVN__c = null]);

        for (Application_MVN__c application : applications) {
            application.Appeal_Result_MVN__c = 'Approved';
        }

        System.runAs(caseManager) {
            Test.startTest();
            update applications;
            Test.stopTest();
        }

        System.assertEquals(10, [
            SELECT
                Count()
            FROM
                Application_MVN__c
            WHERE
                Appeal_Processor_MVN__c = :caseManager.Id
        ]);
	}
}