/**
 * @author      Mavens
 * @date        November 2018
 * @description Tests for MissingInformationBatchJobMVN
 */
@isTest
private class MissingInformationBatchJobTestMVN {

    static List<Application_MVN__c> applicationList;
    static Set<Id> applicationIds;
    static List<Eligibility_Engine_Run_MVN__c> engineRunList;
    static List<Eligibility_Engine_Result_MVN__c> engineResultList;
    static List<Case> caseList;

    static {
        applicationList = createApplications();
        engineRunList = createEngineRuns(applicationList);
        engineResultList = createEngineResults(engineRunList);
        caseList = createCases(applicationList);
    }

    static List<Application_MVN__c> createApplications() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        Program_MVN__c program = programFactory.create();

        TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
        List<Account> patients = accountFactory.createPatients(1);
        Account hcp = accountFactory.createPrescriber(null);

        TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();
        Program_Member_MVN__c programMember = programMemberFactory.create(program, hcp, patients)[0];

        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        List<Application_MVN__c> applicationList = applicationFactory.createMany(200, new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id,
            'Status_MVN__c' => 'Missing Information'
        });
        for (Application_MVN__c application : applicationList) {
            application.Missing_Information_Close_Date_MVN__c = Date.Today().addDays(-1);
        }
        update applicationList;
        return applicationList;
    }

    static List<Eligibility_Engine_Run_MVN__c> createEngineRuns(List<Application_MVN__c> applicationList) {
        List<Eligibility_Engine_Run_MVN__c> engineRunList = new List<Eligibility_Engine_Run_MVN__c>();
        for (Application_MVN__c application : applicationList) {
            engineRunList.add(new Eligibility_Engine_Run_MVN__c(
                Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
                Application_MVN__c = application.Id,
                Run_Datetime_MVN__c = Datetime.now().addDays(-1),
                Last_Run_MVN__c = true
            ));
            engineRunList.add(new Eligibility_Engine_Run_MVN__c(
                Result_MVN__c = EligibilityEngineMVN.RULESUCCESS,
                Application_MVN__c = application.Id,
                Run_Datetime_MVN__c = Datetime.now().addDays(-2),
                Last_Run_MVN__c = false
            ));

        }
        insert engineRunList;
        return engineRunList;
    }

    static List<Eligibility_Engine_Result_MVN__c> createEngineResults(List<Eligibility_Engine_Run_MVN__c> engineRunList) {
        List<Eligibility_Engine_Result_MVN__c> engineResults = new List<Eligibility_Engine_Result_MVN__c>();
        for (Eligibility_Engine_Run_MVN__c engineRun : engineRunList) {
            engineResults.add(
                new Eligibility_Engine_Result_MVN__c(
                    Name = 'Rule 1',
                    Result_MVN__c = EligibilityEngineMVN.RULEMISSINGINFO,
                    Result_Message_MVN__c = 'Address missing',
                    Last_Run_MVN__c = engineRun.Last_Run_MVN__c,
                    Eligibility_Engine_Run_MVN__c = engineRun.Id
                )
            );
            engineResults.add(
                new Eligibility_Engine_Result_MVN__c(
                    Name = 'Rule 2',
                    Result_MVN__c = EligibilityEngineMVN.RULESUCCESS,
                    Last_Run_MVN__c = engineRun.Last_Run_MVN__c,
                    Eligibility_Engine_Run_MVN__c = engineRun.Id
                )
            );
        }
        insert engineResults;
        return engineResults;
    }

    static List<Case> createCases(List<Application_MVN__c> applicationList) {
        List<Case> caseList = new List<Case>();
        applicationIds = new Set<Id>();
        for (Application_MVN__c application : applicationList) {
            caseList.add(
                new Case(
                    Subject = 'Case Open 1',
                    Status = 'Open',
                    Application_MVN__c = application.Id
                )
            );
            caseList.add(
                new Case(
                    Subject = 'Case Open 2',
                    Status = 'Open',
                    Application_MVN__c = application.Id
                )
            );
            caseList.add(
                new Case(
                    Subject = 'Case Closed',
                    Status = 'Closed',
                    Exclude_from_Billing_MVN__c = true,
                    Application_MVN__c = application.Id
                )
            );
            applicationIds.add(application.Id);
        }
        insert caseList;
        return caseList;
    }

    @isTest
    static void testSuccess() {
        Test.startTest();
        MissingInformationBatchJobMVN batchJobClass = new MissingInformationBatchJobMVN();
        Database.executeBatch(batchJobClass);
        Test.stopTest();

        System.assertEquals(200, [SELECT count() FROM Application_MVN__c 
                                    WHERE Status_MVN__c = 'Denied']);
        System.assertEquals(200, [SELECT count() FROM Case 
                                    WHERE Status = 'Closed' 
                                    AND Application_MVN__c IN :applicationIds]);
        System.assertEquals(400, [SELECT count() FROM Case 
                                    WHERE Status = 'Cancelled' 
                                    AND Application_MVN__c IN :applicationIds]);
        System.assertEquals(200, [SELECT count() FROM Eligibility_Engine_Run_MVN__c 
                                    WHERE Result_MVN__c = :EligibilityEngineMVN.RULEDENIED 
                                    AND Last_Run_MVN__c = true
                                    AND Application_MVN__c IN :applicationIds]);
        System.assertEquals(400, [SELECT count() FROM Eligibility_Engine_Run_MVN__c 
                                    WHERE Result_MVN__c != :EligibilityEngineMVN.RULEDENIED 
                                    AND Last_Run_MVN__c = false 
                                    AND Application_MVN__c IN :applicationIds]);
        System.assertEquals(200, [SELECT count() FROM Eligibility_Engine_Result_MVN__c 
                                    WHERE Result_MVN__c = :EligibilityEngineMVN.RULEDENIED 
                                    AND Last_Run_MVN__c = true]);
        System.assertEquals(200, [SELECT count() FROM Eligibility_Engine_Result_MVN__c 
                                    WHERE Result_MVN__c = :EligibilityEngineMVN.RULESUCCESS 
                                    AND Last_Run_MVN__c = true]);
        System.assertEquals(0, [SELECT count() FROM Eligibility_Engine_Result_MVN__c 
                                WHERE Result_MVN__c = :EligibilityEngineMVN.RULEDENIED 
                                AND Last_Run_MVN__c = false]);
        System.assertEquals(400, [SELECT count() FROM Eligibility_Engine_Result_MVN__c 
                                WHERE Result_MVN__c = :EligibilityEngineMVN.RULEMISSINGINFO
                                AND Last_Run_MVN__c = false]);
        System.assertEquals(400, [SELECT count() FROM Eligibility_Engine_Result_MVN__c 
                                WHERE Result_MVN__c = :EligibilityEngineMVN.RULESUCCESS
                                AND Last_Run_MVN__c = false]);
    }

    @isTest
    static void testSchedulable() {
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();

        Test.startTest();
        String jobId = System.schedule('ScheduledApexTest', CRON_EXP, new MissingInformationBatchJobMVN()); 
        Test.stopTest();
    }

}