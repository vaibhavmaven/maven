/**
 *  @group OrderSetAppExpiryDateTriggerMVN
 *  @author Mavens
 *  @description    This is a trigger class sets the expiry date based on the last expiry date from
 *                  child order schedulers.
 **/
public with sharing class OrderSetAppExpiryDateTriggerMVN implements TriggerHandlerMVN.HandlerInterface {

    public void handle() {
        Set<Id> applicationIds = new Set<Id>();
        List<Order_Scheduler_MVN__c> schedulers = new List<Order_Scheduler_MVN__c>();

        if (Trigger.isDelete) {
            schedulers = Trigger.old;
        } else {
            schedulers = Trigger.new;
        }

        for (Order_Scheduler_MVN__c scheduler : schedulers) {
            applicationIds.add( scheduler.Application_MVN__c );
        }

        List<Application_MVN__c> applicationsToUpdate = new List<Application_MVN__c>();
        List<Application_MVN__c> lockedApplications = new List<Application_MVN__c>();

        for (Application_MVN__c application : getApplications(applicationIds) ) {

            if (applicationExpiryDateChanging(application)) {
                if (application.Expiry_Date_MVN__c != null) {
                    application.Expiry_Date_MVN__c = null;
                } else {
                    application.Expiry_Date_MVN__c
                        = application.Order_Schedulers__r[0].Order_Schedule_Expiry_Date_MVN__c;
                }
                applicationsToUpdate.add(application);
                if (Approval.isLocked(application.Id)) {
                    lockedApplications.add(application);
                }
            }
        }

        Approval.unlock(lockedApplications);
        update applicationsToUpdate;
        Approval.lock(lockedApplications);
  }

    private Boolean applicationExpiryDateChanging(Application_MVN__c application) {
        Boolean expiryDateGettingNulled = application.Order_Schedulers__r.isEmpty()
                                       && application.Expiry_Date_MVN__c != null;
        Boolean expiryDateGettingSet = !application.Order_Schedulers__r.isEmpty()
            && application.Order_Schedulers__r[0].Order_Schedule_Expiry_Date_MVN__c != application.Expiry_Date_MVN__c;

        return expiryDateGettingNulled || expiryDateGettingSet;
    }
    
    //    //Updated Order By to ASC - by Knipper Admin 3/4/19.  
    private List<Application_MVN__c> getApplications( Set<Id> applicationIds ) {
        return [
            SELECT
                Id,
                Expiry_Date_MVN__c,
                (
                    SELECT
                        Order_Schedule_Expiry_Date_MVN__c
                    FROM
                        Order_Schedulers__r
                    ORDER BY
                        Order_Schedule_Expiry_Date_MVN__c ASC
                    LIMIT 1
                )
            FROM
                Application_MVN__c
            WHERE
                Id IN :applicationIds
        ];
    }
}