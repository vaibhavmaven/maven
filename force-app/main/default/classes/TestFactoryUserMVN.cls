/*
 * TestFactoryContactInformationMVN
 * @created By: Kyle Thornton
 * @created Date: 4/7/2017
 * @description: This class is used to construct User objects
 */
@isTest
public class TestFactoryUserMVN {

    private TestFactorySObjectMVN objectFactory;

    public static Map<String, Profile> profilesByName {
        get {
            if (profilesByName == null) {
                profilesByName = new Map<String, Profile>();
                for (Profile profile : [SELECT Id, Name FROM Profile]) {
                    profilesByName.put(profile.Name, profile);
                }
            }
            return profilesByName;
        }
        private set;
    }

    public static Map<String, UserRole> userRolesByDeveloperName {
        get {
            if (userRolesByDeveloperName == null) {
                userRolesByDeveloperName = new Map<String, UserRole>();
                for (UserRole role : [SELECT Id, DeveloperName FROM UserRole]) {
                    userRolesByDeveloperName.put(role.DeveloperName, role);
                }
            }
            return userRolesByDeveloperName;
        }
        private set;
    }

    public TestFactoryUserMVN() {
        Map<String, Object> defaultValues = new Map<String, Object> {
            'ProfileId' => (profilesByName.get('Patient Case Manager - MVN').id), // Default Profile User
            'EmailEncodingKey' => 'UTF-8',
            'LanguageLocaleKey' => 'en_US',
            'LocaleSidKey' => 'en_US',
            'TimeZoneSidKey' => 'America/New_York',
            'Email'=>'agent@mavens.com',
            'UserName'=>'agent@mavens.com'
        };

        objectFactory = new TestFactorySObjectMVN('User', defaultValues);
    }

    public User construct() {
        return construct(null);
    }

    public User construct(Map<String,Object> valuesByField) {
        return (User) objectFactory.constructSObject(valuesByField);
    }

    public User create() {
        return create(null);
    }

    public User create(Map<String,Object> valuesByField) {
        return (User) objectFactory.createSObject(valuesByField);
    }

    public User createPortalUser() {
        return createPortalUser(null);
    }

    public User createPortalUser(Account prescriber) {

        User adminUser = createAdminUser();

        if (prescriber == null) {
            TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();

            System.runAs(adminUser) {
                prescriber = accountFactory.createPrescriber(null);
            }
        }

        Contact prescriberContact = [SELECT Id FROM Contact WHERE AccountId = :prescriber.id];

        Map<String,Object> fieldValues = new Map<String,Object>{
            'ProfileId' => (profilesByName.get('Touchpoints Portal User').id),
            'FirstName' => prescriber.FirstName,
            'LastName' => prescriber.LastName,
            'Email' => 'prescriber@provider.com',
            'UserName' => 'prescriber@provider.com',
            'ContactId' => prescriberContact.id
        };

        User portalUser;

        System.runAs(adminUser) {
            portalUser = create(fieldValues);
        }

        return portalUser;

    }

    public User createAdminUser() {

        Map<String,Object> fieldValues = new Map<String,Object>{
            'CommunityNickname' => 'admin',
            'ProfileId' => (profilesByName.get('System Administrator').Id),
            'UserName' => ('admin' + Math.random() + '@mavens.com')
            // 'UserRoleId' => (userRolesByDeveloperName.get('CEO').Id)
        };

        if (userRolesByDeveloperName.containsKey('CEO')) {
            fieldValues.put('UserRoleId', userRolesByDeveloperName.get('CEO').Id);
        }

        User adminUser;

        System.runAs(new User(Id = UserInfo.getUserId())) {
            adminUser = create(fieldValues);
        }

        return adminUser;

    }

    public void setUserPermissionSet(Id userId, String customPermissionName) {
        List<PermissionSet> permSetList = [
            SELECT Id
            FROM PermissionSet
            WHERE Name = :customPermissionName 
            LIMIT 1
        ];

        if (!permSetList.isEmpty()) {
            PermissionSetAssignment psa = new PermissionSetAssignment(
                AssigneeId = userId,
                PermissionSetId = permSetList.get(0).Id 
            );
            insert psa;
        }
    }
}