/**
 * @author Mavens
 * @date Sept 2018
 * @description Tests for the trigger handler CreateNintexAttachmentMVN
 */
@isTest
private class CreateNintexAttachmentTestMVN {

    private static User caseManager;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;
    private static Id nintexDeliveryId;
    private static Id nintexDeliveryReminderId;
    private static Id nintexDeliveryReminderFollowupId;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        createNintexPackages();
        createNintexPackagesReenrollmentReminder();
        createNintexPackagesReenrollmentReminderFollowUp();

        System.runAs(caseManager) {
            programMember = createProgramMember();
        }
    }

    static Program_Member_MVN__c createProgramMember() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        Program_MVN__c program = programFactory.create(
            new Map<String,Object>{
                'Name' => 'A Program',
                'Program_Id_MVN__c' => 'Knipper_PAP_MVN',
                'Active_MVN__c' => true,
                'Job_No_MVN__c' => '12345'
            }
        );

        TestFactoryAccountMVN accountTestFactory = new TestFactoryAccountMVN();
        Account physician = accountTestFactory.create(new Map<String, Object> {
            'RecordTypeId' => TestFactoryAccountMVN.accountRecordTypesByName.get('Professional_vod').Id,
            'FirstName' => 'Joel',
            'LastName' => 'Atwood',
            'Credentials_vod__c' => 'MD',
            'DEA_MVN__c' => 'DEA123',
            'SLN_MVN__c' => 'SLN123'
        });

        programMember = TestDataFactoryMVN.createProgramMember(program, TestDataFactoryMVN.createMember());

        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        application = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });

        programMember.Active_Application_MVN__c = application.Id;
        programMember.Physician_MVN__c = physician.Id;
        update programMember;
        return programMember;
    }

    static void createNintexPackages() {
        Loop__DDP__c nintexPackage = new Loop__DDP__c(
            Name = 'test package',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'EligibilityLetters-Approval'
        );

        insert nintexPackage;

        Loop__DDP_Integration_Option__c ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Attach').getRecordTypeId();
        ddpOption.Loop__Attach_As__c = 'SFTP';
        insert ddpOption;
        nintexDeliveryId = ddpOption.Id;

        ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Attach').getRecordTypeId();
        ddpOption.Loop__Attach_As__c = 'Attachment';
        insert ddpOption;
    }

    static void createNintexPackagesReenrollmentReminder() {
        Loop__DDP__c nintexPackage = new Loop__DDP__c(
            Name = 'test package reminder',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'EligibilityLetters--PM-Re-Enrollment'
        );

        insert nintexPackage;

        Loop__DDP_Integration_Option__c ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Attach').getRecordTypeId();
        ddpOption.Loop__Attach_As__c = 'SFTP';
        insert ddpOption;
        nintexDeliveryReminderId = ddpOption.Id;

        ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Attach').getRecordTypeId();
        ddpOption.Loop__Attach_As__c = 'Attachment';
        insert ddpOption;
    }

    static void createNintexPackagesReenrollmentReminderFollowUp() {
        Loop__DDP__c nintexPackage = new Loop__DDP__c(
            Name = 'test package reminder',
            Loop__Object_Name__c = 'Program_Member_MVN__c',
            Loop__Filter__c = 'EligibilityLetters--PM-Re-Enrollment-Followup'
        );

        insert nintexPackage;

        Loop__DDP_Integration_Option__c ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Attach').getRecordTypeId();
        ddpOption.Loop__Attach_As__c = 'SFTP';
        insert ddpOption;
        nintexDeliveryReminderFollowupId = ddpOption.Id;

        ddpOption = new Loop__DDP_Integration_Option__c(Loop__DDP__c = nintexPackage.Id);
        ddpOption.RecordTypeId = SObjectType.Loop__DDP_Integration_Option__c.getRecordTypeInfosByName().
            get('Attach').getRecordTypeId();
        ddpOption.Loop__Attach_As__c = 'Attachment';
        insert ddpOption;
    }

    @isTest
    private static void testGenerateAttachmentNoReminder() {
        Test.startTest();
        programMember.Nintex_Delivery_Id_MVN__c = nintexDeliveryId;
        programMember.Nintex_Delivery_Datetime_MVN__c = Datetime.now();
        update programMember;
        Test.stopTest();

        Application_MVN__c applicationTest = [
            SELECT 
                Application_Reminder_Follow_Up_Sent_MVN__c,
                Application_Reminder_Sent_MVN__c
            FROM 
                Application_MVN__c
            WHERE
                Id = :application.Id
        ];
        System.assert(applicationTest.Application_Reminder_Follow_Up_Sent_MVN__c == null);
        System.assert(applicationTest.Application_Reminder_Sent_MVN__c == null);        
    }

    @isTest
    private static void testGenerateAttachmentReminder() {
        Test.startTest();
        programMember.Nintex_Delivery_Id_MVN__c = nintexDeliveryReminderId;
        programMember.Nintex_Delivery_Datetime_MVN__c = Datetime.now();
        update programMember;
        Test.stopTest();

        Application_MVN__c applicationTest = [
            SELECT 
                Application_Reminder_Date_MVN__c,
                Application_Reminder_Sent_MVN__c
            FROM 
                Application_MVN__c
            WHERE
                Id = :application.Id
        ];
        System.assert(applicationTest.Application_Reminder_Date_MVN__c == null);
        System.assert(applicationTest.Application_Reminder_Sent_MVN__c != null);
    }

    @isTest
    private static void testGenerateAttachmentReminderFollowUp() {
        Test.startTest();
        programMember.Nintex_Delivery_Id_MVN__c = nintexDeliveryReminderFollowupId;
        programMember.Nintex_Delivery_Datetime_MVN__c = Datetime.now();
        update programMember;
        Test.stopTest();

        Application_MVN__c applicationTest = [
            SELECT 
                Application_Reminder_Follow_Up_Date_MVN__c,
                Application_Reminder_Follow_Up_Sent_MVN__c
            FROM 
                Application_MVN__c
            WHERE
                Id = :application.Id
        ];
        System.assert(applicationTest.Application_Reminder_Follow_Up_Date_MVN__c == null);
        System.assert(applicationTest.Application_Reminder_Follow_Up_Sent_MVN__c != null);
    }
}