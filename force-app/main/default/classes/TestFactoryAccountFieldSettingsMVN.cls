/**
 * TestFactoryAccountFieldSettingsMVN
 * @edited by: Pablo Roldan
 * @edited date: October, 2017
 * @description: Test data Factory for DCR Account Field Setting used for Network Search tests.
 * @edit description: Adapted to PS
 */
@isTest
public with sharing class TestFactoryAccountFieldSettingsMVN {

    static {
        TestFactorySegmentsMVN.createMockSegments();
    }

    public static List<DCR_Field_Setting_MVN__mdt> setPersonAccountMock() {
        return setScenarioMocks(new List<Map<String, Object>>{personCredentials, personFirstName, personLastName});
    }

    public static List<DCR_Field_Setting_MVN__mdt> setPersonPhoneMock() {
        return setScenarioMocks(new List<Map<String, Object>>{personPhone, personPhonePrimary});
    }

    public static List<DCR_Field_Setting_MVN__mdt> setInstitutionBlankMock() {
        return setScenarioMocks(new List<Map<String, Object>>{institutionBlank});
    }

    public static List<DCR_Field_Setting_MVN__mdt> setAffiliationMock() {
        return setScenarioMocks(new List<Map<String, Object>>{affiliationRole});
    }

    public static List<DCR_Field_Setting_MVN__mdt> setSpecificityTestMock() {
        return setScenarioMocks(new List<Map<String, Object>>{personCredentials, personCredentialsUS});
    }

    public static List<DCR_Field_Setting_MVN__mdt> setAddressPhoneAndAffiliationMock() {
        return setScenarioMocks(new List<Map<String, Object>>{personPhone,
                                                              personPhonePrimary,
                                                              personAddressLine1,
                                                              personAddressLine2,
                                                              personCity,
                                                              personStateUS,
                                                              personPostalCode,
                                                              affiliationParentAccount,
                                                              affiliationRole,
                                                              personSepcialty});
    }

    public static List<DCR_Field_Setting_MVN__mdt> setPersonSearchMock() {
        return setScenarioMocks(new List<Map<String, Object>>{personFirstName, personLastName, personPhone});
    }

    public static List<DCR_Field_Setting_MVN__mdt> setInstitutionSearchMock() {
        return setScenarioMocks(new List<Map<String, Object>>{institutionName});
    }

    private static List<DCR_Field_Setting_MVN__mdt> setScenarioMocks(List<Map<String,Object>> settingsList) {
        List<DCR_Field_Setting_MVN__mdt> mocks = new List<DCR_Field_Setting_MVN__mdt>();

        for (Map<String, Object> setting : settingsList) {
            mocks.add((DCR_Field_Setting_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata(
                'DCR_Field_Setting_MVN__mdt',
                setting
            ));
        }

        AccountFieldSettingsMVN.setMock(mocks);
        return mocks;
    }

    private static Map<String, Object> defaultObject = new Map<String, Object>{
        'DeveloperName'=>null,
        'Id'=>'',
        'Label'=>'',
        'Language'=>'',
        'MasterLabel'=>'',
        'Account_Record_Type_MVN__c'=>'',
        'Field_MVN__c'=>'',
        'Is_Person_Record_Type_MVN__c'=>true,
        'New_Affiliation_Order_MVN__c'=>null,
        'Object_MVN__c'=>'',
        'Required_for_DCR_Transmission_MVN__c'=>false,
        'Search_Field_Order_MVN__c'=>null,
        'Search_Results_Order_MVN__c'=>null,
        'Segment_MVN__c'=>null,
        'Active_MVN__c'=>false,
        'Show_in_Primary_Results_MVN__c'=>false,
        'Show_in_Secondary_Results_MVN__c'=>false,
        'QualifiedApiName'=>''
    };

    private static Map<String, Object> personCredentials {
        get {
            Map<String, Object> personCredentials = defaultObject.clone();
            personCredentials.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_Credentials',
                    'Id'=>'m0041000000Lel8AAC',
                    'Label'=>'Person Credentials',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person Credentials',
                    'Account_Record_Type_MVN__c'=>'Professional_vod',
                    'Field_MVN__c'=>'Credentials_vod__c',
                    'Object_MVN__c'=>'Account',
                    'Search_Results_Order_MVN__c'=>45,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Secondary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_Credentials'
                }
            );
            return personCredentials;
        }
    }

    private static Map<String, Object> personCredentialsUS {
        get {
            Map<String, Object> personCredentialsUS = personCredentials.clone();
            personCredentialsUS.put('Segment_MVN__c', TestFactorySegmentsMVN.usSegmentId);
            return personCredentialsUS;
        }
    }

    private static Map<String, Object> personSepcialty {
        get {
            Map<String, Object> personSepcialty = defaultObject.clone();
            personSepcialty.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_Specialty',
                    'Id'=>'m0041000000LelSAAS',
                    'Label'=>'Person Specialty',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person Specialty',
                    'Account_Record_Type_MVN__c'=>'Professional_vod',
                    'Field_MVN__c'=>'Specialty_1_vod__c',
                    'Object_MVN__c'=>'Account',
                    'Search_Results_Order_MVN__c'=>40,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Secondary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_Specialty'
                }
            );
            return personSepcialty;
        }
    }

    private static Map<String, Object> personFirstName {
        get {
            Map<String, Object> personFirstName = defaultObject.clone();
            personFirstName.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_First_Name',
                    'Id'=>'m0041000000LelFAAS',
                    'Label'=>'Person First Name',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person First Name',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'FirstName',
                    'Object_MVN__c'=>'Account',
                    'Search_Field_Order_MVN__c'=>20,
                    'Search_Results_Order_MVN__c'=>20,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Primary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_First_Name'
                }
            );
            return personFirstName;
        }
    }

    private static Map<String, Object> personLastName {
        get {
            Map<String, Object> personLastName = defaultObject.clone();
            personLastName.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_Last_Name',
                    'Id'=>'m0041000000LelKAAS',
                    'Label'=>'Person Last Name',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person Last Name',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'LastName',
                    'Object_MVN__c'=>'Account',
                    'Required_for_DCR_Transmission_MVN__c'=>true,
                    'Search_Field_Order_MVN__c'=>10,
                    'Search_Results_Order_MVN__c'=>10,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Primary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_Last_Name'
                }
            );
            return personLastName;
        }
    }

    private static Map<String, Object> personPhone {
        get {
            Map<String, Object> personPhone = defaultObject.clone();
            personPhone.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_Phone',
                    'Id'=>'m0041000000LelMAAS',
                    'Label'=>'Person Phone',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person Phone',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'Phone_vod__c',
                    'Object_MVN__c'=>'Address_vod__c',
                    'Required_for_DCR_Transmission_MVN__c'=>false,
                    'Search_Field_Order_MVN__c'=>30,
                    'Search_Results_Order_MVN__c'=>10,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Secondary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_Phone'
                }
            );
            return personPhone;
        }
    }

    private static Map<String, Object> personPhonePrimary {
        get {
            Map<String, Object> personPhonePrimary = defaultObject.clone();
            personPhonePrimary.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_Phone_Primary',
                    'Id'=>'m0041000000LelNAAS',
                    'Label'=>'Person Phone Primary',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person Phone Primary',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'Primary_vod__c',
                    'Object_MVN__c'=>'Address_vod__c',
                    'Search_Field_Order_MVN__c'=>30,
                    'Search_Results_Order_MVN__c'=>16,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Secondary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_Phone_Primary'
                }
            );
            return personPhonePrimary;
        }
    }

    private static Map<String, Object> personAddressLine1 {
        get {
            Map<String, Object> personAddressLine1 = defaultObject.clone();
            personAddressLine1.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_Address_Line_1',
                    'Id'=>'m0041000000Lel2AAC',
                    'Label'=>'Person Address Line 1',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person Address Line 1',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'Name',
                    'Object_MVN__c'=>'Address_vod__c',
                    'Search_Results_Order_MVN__c'=>40,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Primary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_Address_Line_1'
                }
            );
            return personAddressLine1;
        }
    }

    private static Map<String, Object> personAddressLine2 {
        get {
            Map<String, Object> personAddressLine2 = defaultObject.clone();
            personAddressLine2.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_Address_Line_2',
                    'Id'=>'m0041000000Lel3AAC',
                    'Label'=>'Person Address Line 2',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person Address Line 2',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'Address_Line_2_vod__c',
                    'Object_MVN__c'=>'Address_vod__c',
                    'Search_Results_Order_MVN__c'=>50,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Primary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_Address_Line_2'
                }
            );
            return personAddressLine2;
        }
    }

    private static Map<String, Object> personCity {
        get {
            Map<String, Object> personCity = defaultObject.clone();
            personCity.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_City',
                    'Id'=>'m0041000000Lel6AAC',
                    'Label'=>'Person City',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person City',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'City_vod__c',
                    'Object_MVN__c'=>'Address_vod__c',
                    'Required_for_DCR_Transmission_MVN__c'=>true,
                    'Search_Field_Order_MVN__c'=>40,
                    'Search_Results_Order_MVN__c'=>60,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Primary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_City'
                }
            );
            return personCity;
        }
    }

    private static Map<String, Object> personPostalCode {
        get {
            Map<String, Object> personPostalCode = defaultObject.clone();
            personPostalCode.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_Postal_Code',
                    'Id'=>'m0041000000LelPAAS',
                    'Label'=>'Person Postal Code',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person Postal Code',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'Zip_vod__c',
                    'Object_MVN__c'=>'Address_vod__c',
                    'Required_for_DCR_Transmission_MVN__c'=>true,
                    'Search_Field_Order_MVN__c'=>60,
                    'Search_Results_Order_MVN__c'=>70,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Primary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_Postal_Code'
                }
            );
            return personPostalCode;
        }
    }

    private static Map<String, Object> personStateUS {
        get {
            Map<String, Object> personStateUS = defaultObject.clone();
            personStateUS.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_State_US',
                    'Id'=>'m0041000000LelTAAS',
                    'Label'=>'Person State - US',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person State - US',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'State_vod__c',
                    'Object_MVN__c'=>'Address_vod__c',
                    'Required_for_DCR_Transmission_MVN__c'=>true,
                    'Search_Field_Order_MVN__c'=>50,
                    'Search_Results_Order_MVN__c'=>65,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.usSegmentId,
                    'Show_in_Primary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_State_US'
                }
            );
            return personStateUS;
        }
    }

    private static Map<String, Object> institutionBlank {
        get {
            Map<String, Object> institutionBlank = defaultObject.clone();
            institutionBlank.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Institution_Blank_1',
                    'Id'=>'m0041000000LekuAAC',
                    'Label'=>'Institution Blank #1',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Institution Blank #1',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'Blank',
                    'Is_Person_Record_Type_MVN__c'=>false,
                    'Object_MVN__c'=>'Account',
                    'Search_Field_Order_MVN__c'=>30,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'QualifiedApiName'=>'Institution_Blank_1'
                }
            );
            return institutionBlank;
        }
    }

    private static Map<String, Object> institutionName {
        get {
            Map<String, Object> institutionName = defaultObject.clone();
            institutionName.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Institution_Name',
                    'Id'=>'m0041000000HeTaAAK',
                    'Label'=>'Institution Name',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Institution Name',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'Name',
                    'Is_Person_Record_Type_MVN__c'=>true,
                    'Object_MVN__c'=>'Account',
                    'Search_Field_Order_MVN__c'=>10,
                    'Search_Results_Order_MVN__c'=>10,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Primary_Results_MVN__c'=>true,
                    'Required_for_DCR_Transmission_MVN__c'=>true,
                    'QualifiedApiName'=>'Institution_Blank_1'
                }
            );
            return institutionName;
        }
    }

    private static Map<String, Object> affiliationParentAccount {
        get {
            Map<String, Object> affiliationParentAccount = defaultObject.clone();
            affiliationParentAccount.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_Parent_Account',
                    'Id'=>'m0041000000LelLAAS',
                    'Label'=>'Person Parent Account',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person Parent Account',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'Parent_Account_MVN__c',
                    'New_Affiliation_Order_MVN__c'=>20,
                    'Object_MVN__c'=>'Affiliation_MVN__c',
                    'Search_Results_Order_MVN__c'=>50,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Secondary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_Parent_Account'
                }
            );
            return affiliationParentAccount;
        }
    }

    private static Map<String, Object> affiliationRole {
        get {
            Map<String, Object> affiliationRole = defaultObject.clone();
            affiliationRole.putAll(
                new Map<String, Object>{
                    'DeveloperName'=>'Person_Role',
                    'Id'=>'m0041000000LelRAAS',
                    'Label'=>'Person Role',
                    'Language'=>'en_US',
                    'MasterLabel'=>'Person Role',
                    'Account_Record_Type_MVN__c'=>'All',
                    'Field_MVN__c'=>'Role_MVN__c',
                    'New_Affiliation_Order_MVN__c'=>10,
                    'Object_MVN__c'=>'Affiliation_MVN__c',
                    'Required_for_DCR_Transmission_MVN__c'=>true,
                    'Search_Results_Order_MVN__c'=>55,
                    'Segment_MVN__c'=>TestFactorySegmentsMVN.defaultSegmentId,
                    'Show_in_Secondary_Results_MVN__c'=>true,
                    'QualifiedApiName'=>'Person_Role'
                }
            );
            return affiliationRole;
        }
    }
}