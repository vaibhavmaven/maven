public class EvinceMedIntegrationWebServiceMVN {
    
    
    public void EvinceMedIntegrationWebService(String jsonrequest){
        
        try{
            String endpoint = 'http://www.example.com/service';
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json;charset=UTF-8');
            req.setHeader('Accept', 'text/html');
            req.setbody(jsonrequest); 
            System.debug(req);
            HttpResponse res = http.send(req);
            if (res.getStatusCode() == 200) {
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
                //List<Object> animals = (List<Object>) results.get('animals');
                //System.debug('Received the following animals:');
                // for (Object animal: animals) {
                //   System.debug(animal);
                
                // } 
            }
            else{
                System.debug('ERROR');
            }
        }
        catch(exception e){
            system.debug(e);
        }
    }
}