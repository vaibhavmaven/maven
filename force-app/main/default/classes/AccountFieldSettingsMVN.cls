/**
 * AccountFieldSettingsMVN.cls
 * @author: Kyle Thornton
 * @created: May 27, 2016
 * @edited by: Pablo Roldan
 * @edited date: Nov, 2017
 *
 * @description: This class wraps functionality around the Account Field Settings Custom Metadata type
 *
 * @edit description: Adapted to PS
 */
public with sharing class AccountFieldSettingsMVN {
    /**
    *    @param objectName (String): the object to filter on. Null searches for all objects
    *    @param country (String): the country currently in context. Null means to use only defaults.
    *    @param recordType (String): the record type to filter on. Null searches for the defualt 'All'
    *    @param fieldToOrderBy (String): the field on the Account Fields Settings used to determine the order of fields
    *    @param isPerson (Boolean): indicates if the context is person account or business account
    *
    *    @description: using the inputs query for matching account settings. Order by fields ensure the returned records
    *                  will place the proper config record at the end of each field "group".
    *                  This allows us to simple iterate through them and add them to a Map. Each time a field is added
    *                  to the map it will overwrite the old value leaving us with only the values we desire. Finally the
    *                  remaining values can be sorted based on sort values and, if there are duplicates, then alphabetically
    *                  by field name. A specificity is determined when sorting alphabetically and a more "specific" field
    *                  will be kept in the event there are duplicate names at the same position.
    *
    *    @API
    *    @method: List<DCR_Field_Setting_MVN__mdt> getSettings()
    *    @description: returns settings for this instance
    *                If this instance is for Search Results then returns settings for this instance but grouped by the "Search Results Grouping".
    *                If Fields A, B, C, D, E exist. but B and D have the same grouping then the resulting order will be A, B, D, C, E
    *
    */

    private List<DCR_Field_Setting_MVN__mdt> settingsList = new List<DCR_Field_Setting_MVN__mdt>();
    private static List<DCR_Field_Setting_MVN__mdt> testMock;

    public  List<DCR_Field_Setting_MVN__mdt> getSettings() {
        return settingsList;
    }

    public AccountFieldSettingsMVN(String country, String recordType, String fieldToOrderBy, Boolean isPerson) {

        List<DCR_Field_Setting_MVN__mdt> wrappedSettings = new List<DCR_Field_Setting_MVN__mdt>();

        System.debug('### In AccountFieldSettingsMVN Constructor. recordType is : ' + recordType);
        System.debug('### In AccountFieldSettingsMVN Constructor. isPerson is : ' + isPerson);
        Set<String> recordTypes = new Set<String>{UtilitiesMVN.ALL_PICKLIST_VALUE};

        if (String.isNotBlank(recordType)) {
            recordTypes.add(recordType);
        }

        Set<Id> filterSegments = SegmentsMVN.filterSegments(country);

        Map<String, Schema.SObjectField> acctFieldSettingsFMap = DCR_Field_Setting_MVN__mdt.sObjectType
                                                                                             .getDescribe()
                                                                                             .Fields.getMap();
        List<String> acctSettingFields = new List<String>(acctFieldSettingsFMap.keySet());

        String queryFields = String.escapeSingleQuotes(String.join(acctSettingFields, ','));
        String query = 'SELECT ' + queryFields + ' ' +
                         'FROM DCR_Field_Setting_MVN__mdt ' +
                        'WHERE Segment_MVN__c IN :filterSegments ' +
                          'AND Account_Record_Type_MVN__c IN :recordTypes ';
                        //   'AND ' + fieldToOrderBy + ' != null ';

        if (isPerson != null) {
            query += 'AND Is_Person_Record_Type_MVN__c = :isPerson ';
        }

        // query += 'ORDER BY ' + fieldToOrderBy + ' ASC';

        wrappedSettings = (List<DCR_Field_Setting_MVN__mdt>)Database.query(query);
        System.debug(filterSegments);
        System.debug(query);

        if(testMock != null){
            wrappedSettings = testMock;
        }

        wrappedSettings = SegmentsMVN.sortMetadataBySegment(wrappedSettings);

        wrappedSettings = filterSettingsToOnePerField(wrappedSettings, recordType, fieldToOrderBy);

        createContainer(wrappedSettings, fieldToOrderBy);
    }

    @TestVisible
    private static void setMock(List<DCR_Field_Setting_MVN__mdt> mock){
        testMock = mock;
    }

    private void createContainer(List<DCR_Field_Setting_MVN__mdt> wrappers, String fieldToOrderBy){
        Set<String> dcrFields = DCR_Field_Setting_MVN__mdt.sObjectType.getDescribe().Fields.getMap().keySet();

        if(dcrFields.contains(fieldToOrderBy)) {
            Map<Decimal, List<DCR_Field_Setting_MVN__mdt>> settingsByOrder = getSettingsGroupedByOrder(fieldToOrderBy, wrappers);
            List<Decimal> sortedOrders = sortKeys(settingsByOrder.keySet());

            for (Decimal order : sortedOrders) {
                settingsList = sortSettingsAlphabeticallyByField(settingsByOrder.get(order));
            }

            if (fieldToOrderBy == 'Search_Results_Order_MVN__c') {
                settingsList = reorderKeysGroupingBySearchGroup(settingsList);
            }
        } else {
            settingsList = wrappers;
        }
        System.debug(settingsList);
    }

    @TestVisible
    private List<DCR_Field_Setting_MVN__mdt> filterSettingsToOnePerField(List<DCR_Field_Setting_MVN__mdt> settings,
                                                                         String recordType,
                                                                         String fieldToOrderBy) {
        Map<String, DCR_Field_Setting_MVN__mdt> settingsToUse = new Map<String, DCR_Field_Setting_MVN__mdt>();

        for (DCR_Field_Setting_MVN__mdt setting : settings) {
            if (setting.Field_MVN__c.toLowerCase() == 'blank') {
                continue; //blanks aren't needed in results
            }

            String fieldKey = setting.Object_MVN__c +
                           //   ((String) setting.get('Address_Record_Type_MVN__c')) +
                              setting.Field_MVN__c;
            if (settingsToUse.containsKey(fieldKey)) {
                Integer mapValueSpecificity = getSpecificity(settingsToUse.get(fieldKey), recordType);
                Integer currentValueSpecificity = getSpecificity(setting, recordType);

                if (currentValueSpecificity >= mapValueSpecificity) {
                    settingsToUse.put(fieldKey, setting);
                }
            } else {
                settingsToUse.put(fieldKey, setting);
            }
        }

        return settingsToUse.values();
    }

    /**
    * Get Specificity
    * @description: This method is used to rank records relative to each other and the context they are being requested under.
    *               Having a country code (non ZZ) gets a higher specficity than not having one.
    *
    *               If a record type (not all) was requested then having a record type set in the setting makes it more specific
    */

    private Integer getSpecificity(DCR_Field_Setting_MVN__mdt setting, String recordType){
        Integer specificity = 0;

        if (String.isBlank(recordType) || recordType == UtilitiesMVN.ALL_PICKLIST_VALUE){
            if (setting.Account_Record_Type_MVN__c == UtilitiesMVN.ALL_PICKLIST_VALUE) {
                specificity++;
            }
        } else {
            if (setting.Account_Record_Type_MVN__c != UtilitiesMVN.ALL_PICKLIST_VALUE){
                specificity++;
            }
        }

        return specificity;
    }

    private Map<Decimal, List<DCR_Field_Setting_MVN__mdt>> getSettingsGroupedByOrder(String fieldToOrderBy,
                                                                                     List<DCR_Field_Setting_MVN__mdt> settings) {

        Map<Decimal, List<DCR_Field_Setting_MVN__mdt>> settingsByOrder = new Map<Decimal, List<DCR_Field_Setting_MVN__mdt>>();
        for (DCR_Field_Setting_MVN__mdt setting : settings) {

            Decimal sortOrder;
            if (setting.get(fieldToOrderBy) instanceof String) {
                sortOrder = Decimal.valueOf((String) setting.get(fieldToOrderBy));
            } else {
                sortOrder = (Decimal) setting.get(fieldToOrderBy);
            }

            if (sortOrder == 0.0) {
                continue;
            }

            if (!settingsByOrder.containsKey(sortOrder)) {
                settingsByOrder.put(sortOrder, new List<DCR_Field_Setting_MVN__mdt>());
            }
            settingsByOrder.get(sortOrder).add(setting);
        }

        return settingsByOrder;
    }

    private List<Decimal> sortKeys(Set<Decimal> orderKeys) {
        List<Decimal> keys = new List<Decimal>();
        keys.addAll(orderKeys);
        keys.sort();
        return keys;
    }

    private List<DCR_Field_Setting_MVN__mdt> reorderKeysGroupingBySearchGroup(List<DCR_Field_Setting_MVN__mdt> settingsList) {
        List<String> orderedKeys = new List<String>();
        Map<String, List<DCR_Field_Setting_MVN__mdt>> settingsByType = new Map<String, List<DCR_Field_Setting_MVN__mdt>>();
        Integer noGroupingCounter = 0;
        for (DCR_Field_Setting_MVN__mdt setting : settingsList) {
            String settingKey = String.valueOf(noGroupingCounter++);

            if (!settingsByType.containsKey(settingKey)) {
                orderedKeys.add(settingKey);
                settingsByType.put(settingKey, new List<DCR_Field_Setting_MVN__mdt>());
            }
            settingsByType.get(settingKey).add(setting);
        }

        List<DCR_Field_Setting_MVN__mdt> searchResultOrderedSettings = new List<DCR_Field_Setting_MVN__mdt>();
        for (String key : orderedKeys) {
            searchResultOrderedSettings.addAll(settingsByType.get(key));
        }

        return searchResultOrderedSettings;
    }

    private List<DCR_Field_Setting_MVN__mdt> sortSettingsAlphabeticallyByField(List<DCR_Field_Setting_MVN__mdt> settings) {
        Map<String, List<DCR_Field_Setting_MVN__mdt>> settingsByTranslatedLabel = new Map<String, List<DCR_Field_Setting_MVN__mdt>>();

        for (DCR_Field_Setting_MVN__mdt setting : settings) {
            String fieldTranslation = getFieldTranslationIfNotBlankPlaceHolder(setting);

            if(!settingsByTranslatedLabel.containsKey(fieldTranslation)) {
                settingsByTranslatedLabel.put(fieldTranslation, new List<DCR_Field_Setting_MVN__mdt>());
            }
            settingsByTranslatedLabel.get(fieldTranslation).add(setting);
        }

        List<String> fieldNames = new List<String>();
        fieldNames.addAll(settingsByTranslatedLabel.keySet());
        fieldNames.sort();

        List<DCR_Field_Setting_MVN__mdt> sortedSettings = new List<DCR_Field_Setting_MVN__mdt>();
        for (String field : fieldNames) {
            sortedSettings.addAll(settingsByTranslatedLabel.get(field));
        }

        return sortedSettings;
    }

    private String getFieldTranslationIfNotBlankPlaceHolder(DCR_Field_Setting_MVN__mdt setting) {
        try {
            return getDescribeForSetting(setting).getLabel();
        } catch (exception ex) {
            return 'blank';
        }
    }

    public static Schema.DisplayType getFieldType(DCR_Field_Setting_MVN__mdt setting) {
        try {
            return UtilitiesMVN.globalDescribe
                            .get(setting.Object_MVN__c)
                            .getDescribe()
                            .fields.getMap()
                            .get(setting.Field_MVN__c)
                            .getDescribe()
                            .getType();
        } catch (Exception ex) {
            return null;
        }
    }

    private static Map<String, Schema.SObjectType> globalSchema {
        get {
            if (globalSchema == null) {
                globalSchema = Schema.getGlobalDescribe();
            }
            return globalSchema;
        }
        set;
    }

    private static Schema.DescribeFieldResult getDescribeForSetting(DCR_Field_Setting_MVN__mdt setting) {
        try {
            return globalSchema.get(setting.Object_MVN__c)
                               .getDescribe()
                               .fields.getMap()
                               .get(setting.Field_MVN__c)
                               .getDescribe();
        } catch(exception ex) {
            System.debug('No describe available for ' + setting.Object_MVN__c
                          + '.' + setting.Object_MVN__c + ' Continuing on.');
            return null;
        }
    }
}