/*
 * AddressToContactInfoTriggerTestMVN
 * Created By: Kyle Thornton
 * Created Date: 4/7/2017
 * Description: This class contains unit tests for the AddressToContactInfoTriggerMVN class
 *
 */
@isTest
private class AddressToContactInfoTriggerTestMVN
{
    private static final Integer DATA_SIZE = 200;
    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();

    static Account prescriber;
    static Map<Id, Address_vod__c> addresses;

    static {
        prescriber = accountFactory.createPrescriber(null);
    }

    @isTest
    static void itShouldCreateBusinessContactInfoRecordsOnInsert()
    {
        List<Address_vod__c> addressList = createAddresses(true);

        Test.startTest();
        insert addressList;
        Test.stopTest();

        addresses = new Map<Id, Address_vod__c>(addressList);

        List<Contact_Information_MVN__c> addressRecords = [SELECT Id,
                                                                  Address_MVN__c,
                                                                  Account_MVN__c
                                                             FROM Contact_Information_MVN__c
                                                            WHERE RecordType.DeveloperName = 'Address_MVN'];

        System.assertEquals(DATA_SIZE, addressRecords.size());
        for (Contact_Information_MVN__c addressRecord : addressRecords) {
            System.assert(addresses.containsKey(addressRecords[0].Address_MVN__c));
            System.assertEquals(prescriber.id, addressRecord.Account_MVN__c);
        }


        validateRecordsForFieldHaveValues('Phone_vod__c', 'Phone_MVN', 'Work');
        validateRecordsForFieldHaveValues('Phone_2_vod__c', 'Phone_MVN', 'Work');
        validateRecordsForFieldHaveValues('Fax_vod__c', 'Phone_MVN', 'Fax');
        validateRecordsForFieldHaveValues('Fax_2_vod__c', 'Phone_MVN', 'Fax');
    }

    @isTest
    static void itShouldUpdateBusinessContactInfoRecordsOnUpdate()
    {
        List<Address_vod__c> addressList = createAddresses(true);


        insert addressList;
        addressList = updateAddresses(addressList);

        Test.startTest();
        update addressList;
        Test.stopTest();

        addresses = new Map<Id, Address_vod__c>(addressList);

        validateRecordsForFieldHaveValues('Phone_vod__c', 'Phone_MVN', 'Work');
        validateRecordsForFieldHaveValues('Phone_2_vod__c', 'Phone_MVN', 'Work');
        validateRecordsForFieldHaveValues('Fax_vod__c', 'Phone_MVN', 'Fax');
        validateRecordsForFieldHaveValues('Fax_2_vod__c', 'Phone_MVN', 'Fax');

    }

    @isTest
    static void itShouldCreateHomeContactInfoRecordsOnInsert()
    {
        List<Address_vod__c> addressList = createAddresses(false);

        Test.startTest();
        insert addressList;
        Test.stopTest();

        addresses = new Map<Id, Address_vod__c>(addressList);

        List<Contact_Information_MVN__c> addressRecords = [SELECT Id,
                                                                  Address_MVN__c,
                                                                  Account_MVN__c,
                                                                  Primary_MVN__c
                                                             FROM Contact_Information_MVN__c
                                                            WHERE RecordType.DeveloperName = 'Address_MVN'];
        System.assertEquals(DATA_SIZE, addressRecords.size());
        for (Contact_Information_MVN__c addressRecord : addressRecords) {
            System.assert(addresses.containsKey(addressRecords[0].Address_MVN__c));
            System.assertEquals(prescriber.id, addressRecord.Account_MVN__c);
        }


        validateRecordsForFieldHaveValues('Phone_vod__c', 'Phone_MVN', 'Home');
        validateRecordsForFieldHaveValues('Phone_2_vod__c', 'Phone_MVN', 'Home');
        validateRecordsForFieldHaveValues('Fax_vod__c', 'Phone_MVN', 'Fax');
        validateRecordsForFieldHaveValues('Fax_2_vod__c', 'Phone_MVN', 'Fax');
    }

    @isTest
    static void itShouldUpdateHomeContactInfoRecordsOnUpdate()
    {
        List<Address_vod__c> addressList = createAddresses(false);

        insert addressList;
        addressList = updateAddresses(addressList);

        Test.startTest();
        update addressList;
        Test.stopTest();

        addresses = new Map<Id, Address_vod__c>(addressList);

        validateRecordsForFieldHaveValues('Phone_vod__c', 'Phone_MVN', 'Home');
        validateRecordsForFieldHaveValues('Phone_2_vod__c', 'Phone_MVN', 'Home');
        validateRecordsForFieldHaveValues('Fax_vod__c', 'Phone_MVN', 'Fax');
        validateRecordsForFieldHaveValues('Fax_2_vod__c', 'Phone_MVN', 'Fax');

    }

    @isTest
    static void itShouldUpdatePrimaryAddress() {
        Address_vod__c primary = new Address_vod__c(Account_vod__c = prescriber.id,
                                                    Name='123 Main St.',
                                                    Primary_vod__c = true);

        Address_vod__c nonPrimary = new Address_vod__c(Account_vod__c = prescriber.id,
                                                       Name='123 Main St.',
                                                       Primary_vod__c = false);

        insert primary;
        insert nonPrimary;

        System.assertEquals(1, [SELECT count()
                                  FROM Contact_Information_MVN__c
                                 WHERE Primary_MVN__c = true
                                   AND Address_MVN__c = :primary.id]);

        System.assertEquals(1, [SELECT count()
                                  FROM Contact_Information_MVN__c
                                 WHERE Primary_MVN__c = false
                                   AND Address_MVN__c = :nonPrimary.id]);

        nonPrimary.Primary_vod__c = true;
        nonPrimary.DCR_Override_MVN__c = true;
        primary.Primary_vod__c = false;
        primary.DCR_Override_MVN__c = true;
        update new List<Address_vod__c>{primary, nonPrimary};

        System.assertEquals(1, [SELECT count()
                                  FROM Contact_Information_MVN__c
                                 WHERE Primary_MVN__c = false
                                   AND Address_MVN__c = :primary.id]);

       System.assertEquals(1, [SELECT count()
                                  FROM Contact_Information_MVN__c
                                 WHERE Primary_MVN__c = true
                                   AND Address_MVN__c = :nonPrimary.id]);
    }

    private static List<Address_vod__c> createAddresses(Boolean isBusiness) {
        List<Address_vod__c> addressList = new List<Address_vod__c>();

        for (Integer i=0; i<DATA_SIZE; i++) {
            addressList.add(
                new Address_vod__c(
                    Account_vod__c  = prescriber.id,
                    Name            = '123 Main St.',
                    Business_vod__c = isBusiness,
                    Home_vod__c     = !isBusiness,
                    Phone_vod__c    = '1111111111',
                    Phone_2_vod__c  = '2222222222',
                    Fax_vod__c      = '3333333333',
                    Fax_2_vod__c    = '4444444444',
                    DCR_Override_MVN__c = true
                )
            );
        }
        return addressList;
    }

    private static Id veevaAddressRecTypeId {
        get {
            if (veevaAddressRecTypeId == null) {
                veevaAddressRecTypeId = [SELECT Id
                                      FROM RecordType
                                     WHERE SObjectType = 'Address_vod__c'
                                     LIMIT 1].Id;
            }
            return veevaAddressRecTypeId;
        }
        set;
    }

    private static List<Address_vod__c> updateAddresses(List<Address_vod__c> addressList) {
        for (Address_vod__c address : addressList) {
            address.Phone_vod__c    = '1010101010';
            address.Phone_2_vod__c  = '2020202020';
            address.Fax_vod__c      = '3030303030';
            address.Fax_2_vod__c    = '4040404040';
            address.DCR_Override_MVN__c = true;
        }

        return addressList;
    }

    private static void validateRecordsForFieldHaveValues(String addrField, String recTypeName, String label) {
        List<Contact_Information_MVN__c> ciRecords  =   getCiRecordsForPhoneField(addrField);
        System.assertEquals(DATA_SIZE, ciRecords.size(), 'CI STUFF::' + ciRecords);
        for (Contact_Information_MVN__c ciRecord : ciRecords) {
            String phoneValue = (String) addresses.get(ciRecord.Address_MVN__c).get(addrField);
            System.assertEquals(phoneValue, ciRecord.Phone_MVN__c);
            System.assertEquals(prescriber.id, ciRecord.Account_MVN__c);

        }
    }

    private static List<Contact_Information_MVN__c> getCiRecordsForPhoneField(String phoneField) {
        List<String> queryFields = new List<String>{
            'Id',
            'RecordType.DeveloperName',
            'Account_MVN__c',
            'Label_MVN__c',
            'Phone_MVN__c',
            'Address_MVN__c',
            'External_Id_MVN__c'
        };
        String queryString = 'SELECT ' + String.join(queryFields, ',') +
                              ' FROM Contact_Information_MVN__c' +
                             ' WHERE Related_Address_Phone_Field_MVN__c = :phoneField';

        return Database.query(queryString);
    }
}