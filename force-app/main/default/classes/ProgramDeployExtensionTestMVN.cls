/**
*   ProgramDeployExtensionTestMVN
*   Created By:     Florian Hoehn
*   Created Date:   June 22nd, 2017
*   Description:    tests ProgramDeployExtensionMVN
*/
@isTest private class ProgramDeployExtensionTestMVN {
    /**
    * @description tests deploying from JSON
    */
    @isTest private static void testDeployingFromJson() {
    	List<Program_MVN__c> programs = new List<Program_MVN__c>();
    	ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(programs);

        Test.startTest();
            ProgramDeployExtensionMVN actualProgramDeployExtension = new ProgramDeployExtensionMVN(stdController);
            actualProgramDeployExtension.programBlob = Blob.valueOf(ProgramDeployWrapperTestMVN.JSON);
            actualProgramDeployExtension.programFileName = 'TEST';
            PageReference actualPageReference = actualProgramDeployExtension.deploy();
        Test.stopTest();

        Program_MVN__c actualProgram = [SELECT Id, Program_ID_MVN__c FROM Program_MVN__c LIMIT 1];
        System.assertNotEquals(null, actualProgram);
        System.assertEquals(ProgramDeployWrapperTestMVN.EXTERNALID, actualProgram.Program_ID_MVN__c);
    }

    /**
    * @description tests deploying from JSON
    */
    @isTest private static void testDeployingFromInvalidJsonShowsError() {
    	List<Program_MVN__c> programs = new List<Program_MVN__c>();
    	ApexPages.StandardSetController stdController = new ApexPages.StandardSetController(programs);

        Test.startTest();
            ProgramDeployExtensionMVN actualProgramDeployExtension = new ProgramDeployExtensionMVN(stdController);
            actualProgramDeployExtension.programBlob = Blob.valueOf('INVALID JSON');
            actualProgramDeployExtension.programFileName = 'TEST';
            PageReference actualPageReference = actualProgramDeployExtension.deploy();
        Test.stopTest();

        System.assertEquals(null, actualPageReference);
        System.assertEquals(true, ApexPages.hasMessages());
    }
}