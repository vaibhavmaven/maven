/*
 *  CreateRelationshipsForProgramMembersMVN
 *  Created By:     Florian Hoehn
 *  Created Date:   May 30th 2017
 *  Description:    Creates Relationships for Program Members when the account lookup field is added to the
 *                  Automatic_Relationship_Setting_MVN__mdt, has changed and is an account lookup
 */
public with sharing class CreateRelationshipsForProgramMembersMVN implements TriggerHandlerMVN.HandlerInterface {
    /**
    * @description defines the account sObject type for checking the correct type is set inside the fieldset
    */
    private final Schema.sObjectType ACCOUNTSOBJECTTYPE = new Account().getSObjectType();

    /**
    * @description relationship setting
    */
    private List<Automatic_Relationship_Setting_MVN__mdt> relationshipSettings =
        [SELECT Lookup_Field_API_Name_MVN__c, From_Role_MVN__c, To_Role_MVN__c
           FROM Automatic_Relationship_Setting_MVN__mdt];

    /**
    * @description holds field decribe results
    */
    @testVisible private Map<String, Schema.DescribeFieldResult> fieldDescribeMap;

    /**
    * @description enables us to use this inside a dynamic SOQL
    */
    private Set<Id> programMemberIds;

    /**
    * @description constructor
    *              - goes through the fields inside the fieldset and gets describes for those
    */
    public CreateRelationshipsForProgramMembersMVN() {
        Map<String, Schema.SObjectField> programMemberFieldMap = Schema.sObjectType.Program_Member_MVN__c.fields.getMap();
        this.fieldDescribeMap = new Map<String, Schema.DescribeFieldResult>();
        for(Automatic_Relationship_Setting_MVN__mdt thisRelationshipSetting : this.relationshipSettings) {
            try {
                this.fieldDescribeMap.put(
                    thisRelationshipSetting.Lookup_Field_API_Name_MVN__c,
                    programMemberFieldMap.get(thisRelationshipSetting.Lookup_Field_API_Name_MVN__c).getDescribe()
                );
            } catch(Exception e) {
                throw new TypeException(System.Label.Program_Member_Relationship_Setting_Incorrect_MVN);
            }
        }
    }

    /**
    * @description handler
    *              - gets all program members with the needed fields AND the relationship fieldset
    *              - creates the relationship records and inserts them
    */
    public void handle() {
        if((Trigger.IsInsert || Trigger.isUpdate) && Trigger.isAfter) {
            Map<String, Relationship_MVN__c> relationships = new Map<String, Relationship_MVN__c>();
            for(Program_Member_MVN__c thisProgramMember : Database.query(this.getProgramMemberQuery())) {
                for(Automatic_Relationship_Setting_MVN__mdt thisRelationshipSetting : this.relationshipSettings) {
                    if(this.isAccountLookup(thisRelationshipSetting)) {
                        if(Trigger.isInsert || this.isFieldChanged(thisProgramMember, thisRelationshipSetting)) {
                            if((Id)thisProgramMember.get(thisRelationshipSetting.Lookup_Field_API_Name_MVN__c) != null) {
                                Relationship_MVN__c relationship = new Relationship_MVN__c(
                                    From_Account_MVN__c = thisProgramMember.Member_MVN__c,
                                    To_Account_MVN__c = (Id)thisProgramMember.get(thisRelationshipSetting.Lookup_Field_API_Name_MVN__c),
                                    From_Role_MVN__c = thisRelationshipSetting.From_Role_MVN__c,
                                    To_Role_MVN__c = thisRelationshipSetting.To_Role_MVN__c,
                                    Program_Member_MVN__c = thisProgramMember.Id
                                );
                                relationship.Relationship_Key_MVN__c = RelationshipKeyMVN.generate(relationship);
                                relationships.put(
                                    relationship.Relationship_Key_MVN__c,
                                    relationship
                                );
                            }
                        }
                    } else {
                        throw new TypeException(System.Label.Program_Member_Relationship_Setting_Incorrect_MVN);
                    }
                }
            }
            upsert relationships.values() Relationship_Key_MVN__c;
        }
    }

    /**
    * @description construct and return the program member query
    *              - ensures all necessary fields are selected
    */
    private String getProgramMemberQuery() {
        programMemberIds = Trigger.newMap.keySet();

        Set<String> fieldsToQuery = new Set<String> {
            'Id',
            'Member_MVN__c'
        };
        for(Automatic_Relationship_Setting_MVN__mdt thisRelationshipSetting : this.relationshipSettings) {
            fieldsToQuery.add(thisRelationshipSetting.Lookup_Field_API_Name_MVN__c);
        }
        String query = 'SELECT ' + String.join(new List<String>(fieldsToQuery), ', ') +
                       '  FROM Program_Member_MVN__c' +
                       ' WHERE Id IN: programMemberIds';
        return query;
    }

    /**
    * @description checks if the field inside the fieldset is an account lookup
    */
    private Boolean isAccountLookup(Automatic_Relationship_Setting_MVN__mdt thisRelationshipSetting) {
        Schema.DescribeFieldResult fieldDescribe =
                        this.fieldDescribeMap.get(thisRelationshipSetting.Lookup_Field_API_Name_MVN__c);
        return fieldDescribe.getType() == Schema.DisplayType.Reference
                && fieldDescribe.getSoapType() == Schema.SOAPType.ID
                && new Set<Schema.sObjectType>(fieldDescribe.getReferenceTo()).contains(ACCOUNTSOBJECTTYPE);
    }

    /**
    * @description checks if a field has changed when updated
    */
    private Boolean isFieldChanged(Program_Member_MVN__c thisProgramMember,
                                   Automatic_Relationship_Setting_MVN__mdt thisRelationshipSetting) {
        return Trigger.isUpdate
            && Trigger.oldMap.get(thisProgramMember.Id).get(thisRelationshipSetting.Lookup_Field_API_Name_MVN__c)
               != thisProgramMember.get(thisRelationshipSetting.Lookup_Field_API_Name_MVN__c);
    }
}