/********************************************************************************************************************
Class Name : PAP_SendNotification
Description: This class can be invoked from multiple places
1)From a daily batch process to send Patient and HCP communication
2)From Send Letter button from ProgramMember screen
***********************************************************************************************************************/     
global class PAP_SendNotification {
    
    /*
*   Method   : sendNotification_Patient
*   Desc     : SendNotification to Patient based on the primary channel. 
*   @param   : pgmMemberID - List of ProgramMemeberIDs(passed from invoking batch class/button)
*   @return  : Does not return any parameter 
*/  
    webservice static String sendNotification_Patient(String pgmMemberID, Boolean reenrollFlag,Boolean buttonFlag){   
        
        Program_Member_MVN__c pgmMemberObj = getPgmMemberDetails(pgmMemberID);
        Boolean IsSMS ;
        Boolean IsAutoDial ;  
        String deliveryType ='';
        Boolean terminationFlag = false;
        Contact_Information_MVN__c conInfo;
        String sendPatientResult ='';
        String invokeMethod ='';
        
        try{ 
            if(reenrollFlag){
                invokeMethod ='ReenrollmentBatch';
            }
            else if (buttonFlag){
                invokeMethod ='Button';	
            }
            else{
                invokeMethod ='SendNotificationBatch';	
            }
            
            List<Contact_Information_MVN__c> conInfoList = [SELECT Phone_MVN__c FROM Contact_Information_MVN__c
                                                            WHERE Label_MVN__c = 'Mobile' AND Primary_MVN__c = True 
                                                            AND Messages_OK_MVN__c = 'Yes' 
                                                            AND Account_MVN__c =: pgmMemberObj.Member_MVN__c LIMIT 1];
            
            String MobilePhone ='';
            if(!conInfoList.isEmpty()){
                MobilePhone =  conInfoList[0].Phone_MVN__c;
            }
            if(String.IsNotBlank(MobilePhone) && pgmMemberObj.Program_MVN__r.SMS_JKC__c ==true){
                IsSMS = true;
                IsAutoDial = false;
                CreateOmniChannelRecords(pgmMemberObj, IsSMS, IsAutoDial,reenrollFlag);
                sendPatientResult = 'Success';
            }
            else if ((String.isNotEmpty(pgmMemberObj.Member_MVN__r.Primary_Email_MVN__c)) && (pgmMemberObj.Program_MVN__r.Email_JKC__c == true)){
                SYSTEM.debug('Email: '); 
                deliveryType ='Email';
                sendToNintex_Patient(pgmMemberObj,deliveryType,reenrollFlag);                
                sendPatientResult = 'Success';
            }
            else If (String.IsNotBlank(pgmMemberObj.Member_MVN__r.Phone) && pgmMemberObj.Program_MVN__r.AutoDialer_JKC__c == true) {
                SYSTEM.debug('AutoDial: '); 
                IsSMS = false;
                IsAutoDial = true; 
                CreateOmniChannelRecords(pgmMemberObj, IsSMS, IsAutoDial,reenrollFlag);
                sendPatientResult = 'Success';
            }
           
            else if (String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingStreet) 
                     && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingCity) 
                     && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingPostalCode) 
                     && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingStateCode) 
                     && String.isNotEmpty(pgmMemberObj.Member_MVN__r.BillingCountryCode)
                     && (pgmMemberObj.Program_MVN__r.Mail_JKC__c == true )
                    ){
                        SYSTEM.debug('Mail: '); 
                        deliveryType ='Mail';
                        sendToNintex_Patient(pgmMemberObj,deliveryType,reenrollFlag);                        
                        sendPatientResult = 'Success';
                    } 
            else
                sendPatientResult = 'Invalid Address';
            //Update the Notification flag to true if invoked from button
            if((buttonFlag) && (sendPatientResult== 'Success')){
                Program_Member_MVN__c updatepgmMemberObj = [select Id, Notified_Patient_JKC__c  from Program_Member_MVN__c where Id= :pgmMemberObj.Id];
                updatepgmMemberObj.Notified_Patient_JKC__c = true;
                update updatepgmMemberObj;  
            }   
            system.debug('*********** sendPatientResult '+sendPatientResult);
            return (sendPatientResult);
            
        }
        catch(Exception ex){ 
            
            catchException(ex, invokeMethod+'-sendNotification_Patient',400,pgmMemberObj.Name );
            return ('ErrorMessage' +ex.getMessage());
        }
        
    }
    
    webservice static String sendNotification_HCP(String pgmMemberID, Boolean reenrollFlag,Boolean buttonFlag){       
        Program_Member_MVN__c pgmMemberObj = getPgmMemberDetails(pgmMemberID);
        Boolean IsFax ;        
        String deliveryType ='';
        String sendHCPResult ='';
        String invokeMethod ='';
        String responseString;
        IsFax = pgmMemberObj.Program_MVN__r.Fax_JKC__c;
        List<Id> programMemberId = new List<Id>();
        programMemberId.add(PgmMemberObj.Id);
        system.debug('#########Fax '+pgmMemberObj.Physician_MVN__r.Fax); 
        system.debug('###### IsFax '+IsFax);
        try{
            if(reenrollFlag){
                invokeMethod ='ReenrollmentBatch';
            }
            else if (buttonFlag){
                invokeMethod ='Button';	
            }
            else{
                invokeMethod ='SendNotificationBatch';	
            }
            If (String.IsNotBlank(pgmMemberObj.Physician_MVN__r.Fax) && IsFax ==true){     
                
                deliveryType ='Fax';           
                
                sendToNintex_HCP(pgmMemberObj,deliveryType,reenrollFlag);                
                //CallSRFax
                //responseString=PAP_SRFax.calloutSRFaxAPI(programMemberId);
                //system.debug('Response String is : ' + responseString);
                if(buttonFlag)
                {                    		
                    Program_Member_MVN__c updatepgmMemberObj = [select Id, Is_Prescription_Generated_JKC__c,Document_Generated_Date_JKC__c  from Program_Member_MVN__c where Id= :pgmMemberObj.Id];

                    //Update on 11/11/20 - added IF statement to trigger process builder
                    IF(updatepgmMemberObj.Is_Prescription_Generated_JKC__c = true)
                    {        
                        updatepgmMemberObj.Is_Prescription_Generated_JKC__c = false;
                		update updatepgmMemberObj;  
                    }
                    
                    updatepgmMemberObj.Is_Prescription_Generated_JKC__c = true;
                    	updatepgmMemberObj.Document_Generated_Date_JKC__c =system.now();
                		update updatepgmMemberObj;                
                 		sendHCPResult ='Success';
                }
                else{
                    sendHCPResult ='BatchSRFaxSuccess';
                    
                }	  
                
            }       
            else if ((String.isNotEmpty(pgmMemberObj.Physician_MVN__r.Primary_Email_MVN__c)) && (pgmMemberObj.Program_MVN__r.Email_JKC__c == true )){
                deliveryType ='Email';
                sendToNintex_HCP(pgmMemberObj,deliveryType,reenrollFlag);
                sendHCPResult ='Success';
            }
            else if (String.isNotEmpty(pgmMemberObj.Physician_MVN__r.BillingStreet) 
                     && String.isNotEmpty(pgmMemberObj.Physician_MVN__r.BillingCity) 
                     && String.isNotEmpty(pgmMemberObj.Physician_MVN__r.BillingPostalCode) 
                     && String.isNotEmpty(pgmMemberObj.Physician_MVN__r.BillingStateCode)
                     && String.isNotEmpty(pgmMemberObj.Physician_MVN__r.BillingCountryCode)
                     && (pgmMemberObj.Program_MVN__r.Mail_JKC__c == true )
                    ){
                        deliveryType ='Mail';
                        sendToNintex_HCP(pgmMemberObj,deliveryType,reenrollFlag);
                        sendHCPResult ='Success';
                    } 
            else
                sendHCPResult = 'Invalid Address';
            system.debug('delivery type '+deliveryType);
            //Update the Notification flag to true if invoked from button
            if((buttonFlag) && (sendHCPResult =='Success')){
                Program_Member_MVN__c updatepgmMemberObj = [select Id, Notified_HCP_JKC__c  from Program_Member_MVN__c where Id= :pgmMemberObj.Id];
                updatepgmMemberObj.Notified_HCP_JKC__c = true;
                update updatepgmMemberObj;
                system.debug('updatepgmMemberObj notified HCP '+updatepgmMemberObj.Notified_HCP_JKC__c);
            }
            
            return (sendHCPResult);
        }
        catch(Exception ex){ 
            
            catchException(ex, invokeMethod+'-sendNotification_HCP',400,pgmMemberObj.Name );
            return ('ErrorMessage' +ex.getMessage());
        }
    } 
    
    webservice static String sendNotification_PlanSponsor(String pgmMemberID,Boolean reenrollFlag,Boolean buttonFlag){ 
        Program_Member_MVN__c pgmMemberObj = getPgmMemberDetails(pgmMemberID);
        String appStatus = pgmMemberObj.Active_Application_Status_MVN__c;
        String deliveryType ='Mail';
        String sendPlanSponsorResult ='';
        String invokeMethod ='';
        try{
            
            if(reenrollFlag){
                invokeMethod ='ReenrollmentBatch';
            }
            else if (buttonFlag){
                invokeMethod ='Button';	
            }
            else{
                invokeMethod ='SendNotificationBatch';	
            }
            if ((appStatus == 'Fulfilled') && (pgmMemberObj.Program_MVN__r.Mail_JKC__c == true ))
            {
                List<Benefits_Coverage_MVN__c> bncObjList = getBnCDetails(pgmMemberObj.Active_Application_MVN__c); 
                if(!bncObjList.isEmpty()){               
                    if(bncObjList[0].Medicare_Part_D_MVN__c == 'Yes'){
                        if (String.isNotEmpty(bncObjList[0].Payer_Med_D_Plan_Sponsor_JKC__r.BillingStreet) 
                            && String.isNotEmpty(bncObjList[0].Payer_Med_D_Plan_Sponsor_JKC__r.BillingCity) 
                            && String.isNotEmpty(bncObjList[0].Payer_Med_D_Plan_Sponsor_JKC__r.BillingPostalCode) 
                            && String.isNotEmpty(bncObjList[0].Payer_Med_D_Plan_Sponsor_JKC__r.BillingStateCode)
                            && String.isNotEmpty(bncObjList[0].Payer_Med_D_Plan_Sponsor_JKC__r.BillingCountryCode)){
                                
                                sendToNintex_PlanSponsor(pgmMemberObj,deliveryType);                           
                                sendPlanSponsorResult ='Success';
                            }
                        else
                            sendPlanSponsorResult = 'Invalid Address';
                    }//if MedicarePartD
                    else if ((bncObjList[0].Medicare_Part_D_MVN__c== 'No') || (bncObjList[0].Medicare_Part_D_MVN__c== null)){
                        if(bncObjList[0].Medicare_Part_B_MVN__c== 'Yes'){ 
                            if (String.isNotEmpty(bncObjList[0].Payer_Med_B_Plan_Sponsor_JKC__r.BillingStreet) 
                                && String.isNotEmpty(bncObjList[0].Payer_Med_B_Plan_Sponsor_JKC__r.BillingCity) 
                                && String.isNotEmpty(bncObjList[0].Payer_Med_B_Plan_Sponsor_JKC__r.BillingPostalCode) 
                                && String.isNotEmpty(bncObjList[0].Payer_Med_B_Plan_Sponsor_JKC__r.BillingStateCode)
                                && String.isNotEmpty(bncObjList[0].Payer_Med_B_Plan_Sponsor_JKC__r.BillingCountryCode)){
                                    
                                    sendToNintex_PlanSponsor(pgmMemberObj,deliveryType);                             
                                    sendPlanSponsorResult ='Success';
                                }
                            else
                                sendPlanSponsorResult = 'Invalid Address';                
                        }//if MedicarePartB
                    }
                } //if(!bncObjList.isEmpty() 
                else{
                    sendPlanSponsorResult = 'Invalid Sponsor';
                }  
                //Update the Notification flag to true if invoked from button
                if((buttonFlag) && (sendPlanSponsorResult =='Success')){
                    Program_Member_MVN__c updatepgmMemberObj = [select Id, Notified_Sponsor_JKC__c  from Program_Member_MVN__c where Id= :pgmMemberObj.Id];
                    updatepgmMemberObj.Notified_Sponsor_JKC__c = true;
                    update updatepgmMemberObj; 
                }                       
                
            } //appStatus == 'Fulfilled'
            return (sendPlanSponsorResult);
        }
        catch(Exception ex){ 
            
            catchException(ex, invokeMethod+'-sendNotification_PlanSponsor',400,pgmMemberObj.Name );
            return ('ErrorMessage' +ex.getMessage());
        }
    }
    
    
    public static Program_Member_MVN__c getPgmMemberDetails(String pgmMemberID){
        system.debug('Program member ID '+pgmMemberID);
        return [SELECT Id,
                Name,
                Program_Name_MVN__c,
                Program_Id_MVN__c,
                Program_MVN__r.SMS_JKC__c,
                Program_MVN__r.AutoDialer_JKC__c,
                Program_MVN__r.Email_JKC__c,
                Program_MVN__r.Mail_JKC__c,
                Program_MVN__r.Fax_JKC__c,
                Patient_Status_MVN__c,                
                Patient_Status_Date_JKC__c,
                Active_Application_MVN__c,
                Active_Application_Status_MVN__c,
                Active_Application_Expiry_Date_JKC__c,
                Member_MVN__r.FirstName,
                Member_MVN__r.LastName,
                Member_MVN__r.phone,
                Member_MVN__r.PersonMobilePhone,
                Member_MVN__r.Primary_Email_MVN__c,
                Member_MVN__r.Fax,
                Member_MVN__r.BillingStreet,
                Member_MVN__r.BillingCity,
                Member_MVN__r.BillingStateCode,
                Member_MVN__r.BillingPostalCode,                
                Member_MVN__r.BillingCountry,
                Member_MVN__r.BillingState,
                Member_MVN__r.BillingCountryCode,
                Physician_MVN__r.Id,
                Physician_MVN__r.FirstName,
                Physician_MVN__r.LastName,
                Physician_MVN__r.Fax,
                Physician_MVN__r.Primary_Email_MVN__c,
                Physician_MVN__r.BillingStreet,
                Physician_MVN__r.BillingCity,
                Physician_MVN__r.BillingStateCode,
                Physician_MVN__r.BillingPostalCode,
                Physician_MVN__r.BillingCountryCode
                from Program_Member_MVN__c where Id=:pgmMemberID] ;      
    }
    public static List<Benefits_Coverage_MVN__c> getBnCDetails(String applicationId){
        // fetching values for Plan Sponsor Address from bnc        
        return [SELECT 
                Health_Insurance_MVN__c,
                Medicare_Part_B_MVN__c,
                Medicare_Part_D_MVN__c,
                Payer_Med_D_Plan_Sponsor_JKC__r.BillingStreet,
                Payer_Med_D_Plan_Sponsor_JKC__r.BillingCity,
                Payer_Med_D_Plan_Sponsor_JKC__r.BillingStateCode,
                Payer_Med_D_Plan_Sponsor_JKC__r.BillingPostalCode,             
                Payer_Med_D_Plan_Sponsor_JKC__r.BillingCountryCode,
                Payer_Med_B_Plan_Sponsor_JKC__r.BillingStreet,
                Payer_Med_B_Plan_Sponsor_JKC__r.BillingCity,
                Payer_Med_B_Plan_Sponsor_JKC__r.BillingStateCode,
                Payer_Med_B_Plan_Sponsor_JKC__r.BillingPostalCode,             
                Payer_Med_B_Plan_Sponsor_JKC__r.BillingCountryCode
                FROM Benefits_Coverage_MVN__c
                WHERE 	Application_MVN__c = :applicationId LIMIT 1] ;  
    }
    private static void CreateOmniChannelRecords(Program_Member_MVN__c pgmMemberObj, Boolean IsSMS ,Boolean IsAutoDial, Boolean reenrollFlag){  
        Omnichannel_JKC__c omniChannelRec = new  Omnichannel_JKC__c();        
        //	When “Patient Status” changes to “Opt-Out” or like “Terminated%” send the termination notification  
        if (pgmMemberObj.Patient_Status_MVN__c != System.Label.PM_Patient_Status_Opt_In_MVN){       
            omniChannelRec.Message_JKC__c = 'Terminated';
        }
        else{
            omniChannelRec.Message_JKC__c = pgmMemberObj.Active_Application_Status_MVN__c; 
        }   
        if(reenrollFlag){
            omniChannelRec.Message_JKC__c = 'Re-Enrollment';            
            AggregateResult[] arList = [select count(Id) from Benefits_Coverage_MVN__c where Medicare_flag_MVN__c=true and Application_MVN__c=: pgmMemberObj.Active_Application_MVN__c];
            if(arList.size()>0){
                system.debug('Medicare '+arList.size() );
                omniChannelRec.MedicareFlag_JKC__c = true;
            }
        }
        system.debug('omni channel message '+omniChannelRec.Message_JKC__c);
        omniChannelRec.HCP_Last_Name_JKC__c = pgmMemberObj.Physician_MVN__r.LastName;
        omniChannelRec.Mobile_Number_JKC__c = pgmMemberObj.Member_MVN__r.Phone;
        omniChannelRec.Patient_First_Name_JKC__c = pgmMemberObj.Member_MVN__r.FirstName;
        
        if(IsSMS ==true){
            omniChannelRec.SMS_JKC__c = true;
        }       
        if(IsAutoDial ==true){
            omniChannelRec.VM_JKC__c= true;
        } 
        omniChannelRec.Client_JKC__c = pgmMemberObj.Program_Name_MVN__c;
        omniChannelRec.Program_Member_JKC__c = pgmMemberObj.Id;
        
        system.debug('pgmMember Active Application Status '+pgmMemberObj.Active_Application_Status_MVN__c );
        
        If(pgmMemberObj.Active_Application_Status_MVN__c == System.Label.Application_Status_Fulfilled_MVN){
            omniChannelRec.Expiry_Date_JKC__c = pgmMemberObj.Active_Application_Expiry_Date_JKC__c;
            
        }   
        
        Eligibility_Engine_Run_MVN__c eligibilityEngineRun = [Select id,Application_MVN__r.id from Eligibility_Engine_Run_MVN__c where Application_MVN__r.id =: pgmMemberObj.Active_Application_MVN__c and Last_Run_MVN__c = true limit 1];
        system.debug(' eligibilty engine run '+eligibilityEngineRun);
        List<Eligibility_Engine_Result_MVN__c> eligibilityEngineResult = [Select id , Result_Message_MVN__c,Result_MVN__c from Eligibility_Engine_Result_MVN__c where Eligibility_Engine_Run_MVN__r.id =:eligibilityEngineRun.Id ];
        system.debug('eligibilityEngineResult '+eligibilityEngineResult.size());
        String deniedReason ='';
        for(Eligibility_Engine_Result_MVN__c check : eligibilityEngineResult){
            system.debug('eligibilityEngineResult result '+check.Result_MVN__c);
            system.debug('check.Result_Message_MVN__c; '+check.Result_Message_MVN__c);
            If(check.Result_MVN__c== 'Denied'){
                
                deniedReason =  check.Result_Message_MVN__c +'|'+ deniedReason;
                
            }
        }
        system.debug('denied reason '+deniedReason.length());
        system.debug('deniedReason '+deniedReason);
        if((deniedReason.length()>0) && (pgmMemberObj.Active_Application_Status_MVN__c ==System.Label.Application_Status_Denied_JKC)){
            omniChannelRec.Reasons_JKC__c  = deniedReason;
        }   
        system.debug('omnic channel Reason '+omniChannelRec.Reasons_JKC__c);
        
        system.debug('omnic channel record inserted '+omniChannelRec);
        insert omniChannelRec;
    }
    
    private static void sendToNintex_Patient(Program_Member_MVN__c pgmMemberObj,String deliveryType, Boolean reenrollFlag){
        
        System.debug('delivery type '+deliveryType);
        String appStatus = pgmMemberObj.Active_Application_Status_MVN__c;        
        
        if (appStatus == System.Label.Application_Status_Fulfilled_MVN)
        {
            appStatus = 'Approval';
        }
        else if(appStatus == System.Label.Application_Status_Denied_JKC)
        {
            appStatus = 'Denial';
        }
        else if (appStatus == System.Label.Application_Status_MissingInformation_JKC)
        {
            appStatus = 'MissingInformation';
        }
        //	When “Patient Status” changes to “Opt-Out” or like “Terminated%” send the termination notification 
        if (pgmMemberObj.Patient_Status_MVN__c != System.Label.PM_Patient_Status_Opt_In_MVN)
        {
            appStatus = 'Termination';
        }
        system.debug('app status '+appStatus);
        String Filter = pgmMemberObj.Program_Id_MVN__c+'-PM-'+  appStatus + '-Patient';     
        //  String Filter =  'Bausch_PAP_JKC-PM-Approval-Patient';
        if(reenrollFlag == true){
            Application_MVN__c  applications = [SELECT Id,
                                                Application_Reminder_Date_MVN__c,
                                                Application_Reminder_Follow_Up_Date_MVN__c
                                                FROM
                                                Application_MVN__c
                                                WHERE
                                                Id =: pgmMemberObj.Active_Application_MVN__c limit 1];
            if (applications.Application_Reminder_Date_MVN__c <= Date.today()){
                Filter =  pgmMemberObj.Program_Id_MVN__c + '-PM-Re-Enrollment-Patient';
            }
            if (applications.Application_Reminder_Follow_Up_Date_MVN__c <= Date.today()) {
                Filter =  pgmMemberObj.Program_Id_MVN__c + '-PM-Re-Enrollment-Patient-Followup';
            }
        }
        Contact contactObj = [SELECT Id,Email FROM contact WHERE AccountId =: pgmMemberObj.Member_MVN__c];
        system.debug('Contactemail '+contactObj.Email);
        system.debug('ContactId '+contactObj.Id);
        system.debug('Filter '+Filter);
        Loop__DDP__c docGenPackage = [SELECT ID FROM Loop__DDP__c WHERE Loop__Filter__c =: Filter];
        
        SYSTEM.debug('docGen '+docGenPackage);
        system.debug('delivery type '+deliveryType);
        Loop__DDP_Integration_Option__c docGenDelivery = [SELECT Id FROM Loop__DDP_Integration_Option__c 
                                                          WHERE Name =: deliveryType 
                                                          AND Loop__DDP__c IN 
                                                          (SELECT ID FROM Loop__DDP__c WHERE 
                                                           Loop__Filter__c =: Filter)];
        system.debug('****** docGenDelivery '+docGenDelivery);
        Loop.loopMessage thisLoopMessage = new Loop.loopMessage(); 
        thisLoopMessage  = new Loop.loopMessage();     
        thisLoopMessage.sessionId = UserInfo.getSessionId();        
        thisLoopMessage.requests.add(new Loop.loopMessage.loopMessageRequest(
            pgmMemberObj.Id,           
            docGenPackage.Id,
            new Map<string, string>{                 
                'deploy' =>  docGenDelivery.Id,
                    'SFContact'=>  contactObj.Id
                    }
        ));
        thisLoopMessage.sendAllRequests();
    }
    private static void sendToNintex_HCP(Program_Member_MVN__c pgmMemberObj,String deliveryType,Boolean reenrollFlag){
        
        String appStatus = pgmMemberObj.Active_Application_Status_MVN__c;
        if (appStatus == System.Label.Application_Status_Fulfilled_MVN)
        {
            appStatus = 'Approval';
        }
        else if(appStatus == System.Label.Application_Status_Denied_JKC)
        {
            appStatus = 'Denial';
        }
        else if (appStatus == System.Label.Application_Status_MissingInformation_JKC)
        {
            appStatus = 'MissingInformation';
        }
        //	When “Patient Status” changes to “Opt-Out” or like “Terminated%” send the termination notification 
        if (pgmMemberObj.Patient_Status_MVN__c != System.Label.PM_Patient_Status_Opt_In_MVN)
        {
            appStatus = 'Termination';
        }
        String Filter = pgmMemberObj.Program_Id_MVN__c+'-PM-'+  appStatus + '-HCP'; 
        if(reenrollFlag == true){
            Application_MVN__c  applications = [SELECT Id,
                                                Application_Reminder_Date_MVN__c,
                                                Application_Reminder_Follow_Up_Date_MVN__c
                                                FROM
                                                Application_MVN__c
                                                WHERE
                                                Id =: pgmMemberObj.Active_Application_MVN__c limit 1];
            
            if (applications.Application_Reminder_Date_MVN__c <= Date.today()){
                Filter =  pgmMemberObj.Program_Id_MVN__c + '-PM-Re-Enrollment-HCP';
            }
            if (applications.Application_Reminder_Follow_Up_Date_MVN__c <= Date.today()) {
                Filter =  pgmMemberObj.Program_Id_MVN__c + '-PM-Re-Enrollment-HCP-Followup';
            }
        }        
        Contact contactObj = [SELECT Id,Email FROM contact WHERE AccountId =: pgmMemberObj.Physician_MVN__c];
        system.debug('Contactemail '+contactObj.Email);
        system.debug('ContactId '+contactObj.Id);
        system.debug('******* Filter HCP '+Filter);
        System.debug('****** delivery type HCP '+deliveryType);
        Loop__DDP__c docGenPackage = [SELECT ID FROM Loop__DDP__c WHERE Loop__Filter__c =: Filter];
        system.debug('docGenPackage '+docGenPackage);
        
        Loop__DDP_Integration_Option__c docGenDelivery = [SELECT Id FROM Loop__DDP_Integration_Option__c 
                                                          WHERE Name =: deliveryType 
                                                          AND Loop__DDP__c IN 
                                                          (SELECT ID FROM Loop__DDP__c WHERE 
                                                           Loop__Filter__c =: Filter)];
        system.debug('docGenDelivery '+docGenDelivery);
        Loop.loopMessage thisLoopMessage = new Loop.loopMessage(); 
        thisLoopMessage  = new Loop.loopMessage();     
        thisLoopMessage.sessionId = UserInfo.getSessionId();
        thisLoopMessage.requests.add(new Loop.loopMessage.loopMessageRequest(
            pgmMemberObj.Id,
            docGenPackage.Id,
            new Map<string, string>{
                'deploy' =>  docGenDelivery.Id,
                    'SFContact'=>  contactObj.Id
                    }
        ));
        thisLoopMessage.sendAllRequests();
    } 
    
    private static void sendToNintex_PlanSponsor(Program_Member_MVN__c pgmMemberObj,String deliveryType){
        
        String Filter ='';
        Benefits_Coverage_MVN__c bncObj = [select Health_Insurance_MVN__c, Program_Member_MVN__c, Application_MVN__c,Name,Medicare_Part_B_MVN__c,Medicare_Part_D_MVN__c,
                                           Payer_Med_B_Plan_Sponsor_JKC__r.Name,Payer_Med_D_Plan_Sponsor_JKC__r.Name from 
                                           Benefits_Coverage_MVN__c where Application_MVN__c=:pgmMemberObj.Active_Application_MVN__c LIMIT 1] ;  
        
        if(bncObj.Medicare_Part_D_MVN__c == 'Yes'){        
            Filter = pgmMemberObj.Program_Id_MVN__c+'-PM-MedicareMedD';     
        }
        else if ((bncObj.Medicare_Part_D_MVN__c== 'No') || (bncObj.Medicare_Part_D_MVN__c== null)){
            if(bncObj.Medicare_Part_B_MVN__c== 'Yes'){                
                Filter = pgmMemberObj.Program_Id_MVN__c+'-PM-MedicareMedB';
            }
        }
        system.debug('Filter'+Filter);
        if (String.isNotEmpty(Filter)){         
            Loop__DDP__c docGenPackage = [SELECT ID FROM Loop__DDP__c WHERE Loop__Filter__c =: Filter];
            system.debug('*******deliveryTupe '+deliveryType);
            System.debug('******* Filter '+Filter);
            Loop__DDP_Integration_Option__c docGenDelivery = [SELECT Id FROM Loop__DDP_Integration_Option__c 
                                                              WHERE Name =: deliveryType 
                                                              AND Loop__DDP__c IN 
                                                              (SELECT ID FROM Loop__DDP__c WHERE 
                                                               Loop__Filter__c =: Filter)];
            Loop.loopMessage thisLoopMessage = new Loop.loopMessage(); 
            thisLoopMessage  = new Loop.loopMessage();     
            thisLoopMessage.sessionId = UserInfo.getSessionId();
            thisLoopMessage.requests.add(new Loop.loopMessage.loopMessageRequest(
                pgmMemberObj.Id,
                docGenPackage.Id,
                new Map<string, string>{
                    'deploy' =>  docGenDelivery.Id }
            ));
            thisLoopMessage.sendAllRequests();
        }//end if
    }
    
    /*
*   Method   : ProgramMember
*   Desc     : Update the Program Member object with the status of the Fax received from SRFax api.
*   @param   : queuedFaxStatus - Status of the queued fax.
*            : queuedFaxResult - Result of the queued fax.Program Member
*            : pgmMemberSubStatus   - SubStatus value to be updated. 
*            : PgmMemberId          - ID of the Program Member to be update.
*   @return  : none
*/    
    private static void updateProgramMember(String queuedFaxStatus, String queuedFaxResult, String pgmMemberSubStatus, String PgmMemberId, Boolean faxQueued){
        // set the queuedFaxStatus, queuedFaxResult, and Program Member substatus here
        system.debug('updating the status, result and Program Member subStatus for this Program Member ID : ' + PgmMemberId);
        List<Program_Member_MVN__c> PgmMemberList = [select 
                                                     Queued_Fax_Status_JKC__c, 
                                                     Queued_Fax_Result_JKC__c, 
                                                     Fax_Delivery_JKC__c,
                                                     Fax_Attempt_Counter_JKC__c,
                                                     Sub_Status_JKC__c,
                                                     Fax_Retry_Identifier_JKC__c,Fax_Sent_Time_JKC__c 
                                                     from 
                                                     Program_Member_MVN__c 
                                                     where 
                                                     ID =:PgmMemberId
                                                     LIMIT 1
                                                    ] ; 
        if(PgmMemberList == null || PgmMemberList.size() == 0){
            system.debug('Failed to update Program Member; Program Member with this ID does not exist !!!' + PgmMemberList); 
        }
        // set the requried values 
        PgmMemberList[0].Queued_Fax_Status_JKC__c = queuedFaxStatus; 
        PgmMemberList[0].Queued_Fax_Result_JKC__c = queuedFaxResult; 
        System.debug('******** queuedFaxResult '+queuedFaxResult);
        System.debug('******** queuedFaxStatus '+queuedFaxStatus);
        PgmMemberList[0].Fax_Delivery_JKC__c      = pgmMemberSubStatus.equalsIgnoreCase('Fax Queued') ? 'In Progress' : 'Failed' ; 
        if( PgmMemberList[0].Sub_Status_JKC__c !='Ready to Fax' ){
            PgmMemberList[0].Fax_Attempt_Counter_JKC__c= PgmMemberList[0].Fax_Attempt_Counter_JKC__c==null?0:PgmMemberList[0].Fax_Attempt_Counter_JKC__c;
            PgmMemberList[0].Fax_Attempt_Counter_JKC__c     += faxQueued == true? 1 : 0;
        }
        system.debug('line 317'+ PgmMemberList[0].Sub_Status_JKC__c );
        PgmMemberList[0].Fax_Sent_Time_JKC__c= system.now();
        if( PgmMemberList[0].Fax_Retry_Identifier_JKC__c ==false){
            PgmMemberList[0].Sub_Status_JKC__c        = pgmMemberSubStatus;
        }
        system.debug('Value to be updated for this Program Member is : ' + PgmMemberList[0]); 
        // update 
        update PgmMemberList ; 
        system.debug('Successfuly updated the status, result and Program Member subStatus for this Program Member ID : ' + PgmMemberList);
    }          
    
    
    /*
*   Method   : catchException
*   Desc     : Method to log details of Exception. 
*   @param   : Exception - The exception object 
*   @param   : String - Name of the method in which the exception occured.       
*   @return  : none
*/    
    private static void catchException(Exception ex , String methodName ,Integer responseCode, String ObjectId ){    
        PAP_ErrorLog.logErrorActivity(ex, methodName , responseCode,  ObjectId);       
    }
}