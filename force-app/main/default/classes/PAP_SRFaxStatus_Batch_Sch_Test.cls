@isTest
public class PAP_SRFaxStatus_Batch_Sch_Test {
    static String CRON_EXP = '0 0 23 * * ?';
     static List<Account> patients;
    static Account hcp;
    static Program_MVN__c program;
    static Program_Member_MVN__c programMember;
    
     @testSetup
    static void setup(){
        program = createProgram();
        programMember = createProgramMember(); 
        
      //  application = createApplication();
       
    }
    
     public static Map<String, RecordType> accountRecordTypesByName {
        get {
            if (accountRecordTypesByName == null) {
                accountRecordTypesByName = new Map<String,RecordType>();
                for (RecordType acctRecType : [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
                    accountRecordTypesByName.put(acctRecType.DeveloperName, acctRecType);
                }
            }
            return accountRecordTypesByName;
        }
        private set;
    }
    
     static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        Program_MVN__c program = programFactory.create();
        program.Fulfilment_Method_MVN__c = 'Pharmacy';
        program.SMS_JKC__c = true;
        program.AutoDialer_JKC__c= true;
        program.Email_JKC__c= true;
        program.Mail_JKC__c=true;
        program.Fax_JKC__c=true;
        //program.Program_ID_MVN__c = '';
        update program;
        return program;
    }
   
       public static List<Account> createPatients(Integer numOfPatients) {
        List<Account> patientsList = new List<Account>();
        // we are creating 20 patients .. so diving these 20 in grousp of M, F, Male, Female.
        List<String> genderList = new List<String>{'Female', 'M'}; 
            Integer counter = 1; 
        for(String genderStr : genderList){
            for (Integer i=0; i<numOfPatients; i++) {
                Account patient= new Account();
                patient.FirstName = 'Test';
                patient.MiddleName = 'testing';
                patient.LastName = 'Patient' + counter;
                patient.Gender_MVN__c = genderStr; 
                patient.Marital_Status_MVN__c = 'Single';                      
                patient.PersonBirthdate = System.today();
                patient.phone='(432-12345)';
                patient.PersonMobilePhone ='(432-12345)';
                patient.Primary_Email_MVN__c = 'test@example.com';
                patient.Fax='(432-12345)';
                patient.BillingStreet='TEST ';
                patient.BillingCity='';
                patient.BillingStateCode='';
                patient.BillingPostalCode='';              
                patient.BillingCountry='';
                patient.BillingState='';
                patient.BillingCountryCode='';
                patient.RecordTypeId = accountRecordTypesByName.get('Patient_MVN').id;
                counter++;          
                patientsList.add(patient);
            }            
        }
        
        insert patientsList;
        Account accPayer = TestDataFactoryMVN.createPayer();
        accPayer.BillingCity = 'TestPayer';
        accPayer.BillingStreet = 'Street1';
        accPayer.BillingPostalCode='62626';
        accPayer.BillingStateCode='NJ';
        accPayer.BillingCountryCode='US';
        update accPayer;
        return patientsList;
    }
  
       static Program_Member_MVN__c createProgramMember() {
        TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
        patients = createPatients(1); 
        
        hcp = accountFactory.createPrescriber(null);
        hcp.Fax= '(432-12345890)';
        hcp.Primary_Email_MVN__c='test@example.com';
        update hcp;
        TestFactoryContactInformationMVN contactInfo = new TestFactoryContactInformationMVN();
        
        Contact_Information_MVN__c contactPhone = contactInfo.constructPhone(patients[0]);
        contactPhone.Label_MVN__c = 'Mobile';
        contactPhone.Primary_MVN__c = True;
        contactPhone.Messages_OK_MVN__c = 'Yes';
        Insert contactPhone;
        
        TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();
        Program_Member_MVN__c programMember = programMemberFactory.create(program, hcp, patients)[0];
        programMember.Patient_Status_MVN__c = 'Opt-In';
        programMember.Program_Id_MVN__c = 'Test';
        programMember.Patient_Status_Date_JKC__c = system.today();
        programMember.Notified_HCP_JKC__c = false;
        programMember.Notified_Patient_JKC__c = false;
        programMember.Notified_Sponsor_JKC__c = false;
        programMember.Queued_Fax_Status_JKC__c = 'Success';
        programMember.Sub_Status_JKC__c        = 'Fax Queued';
        programMember.Is_Prescription_Generated_JKC__c = true ;
        programMember.Queued_Fax_Result_JKC__c = '499443211';
        update programMember;
        Document_MVN__c document = new Document_MVN__c(
            Program_Member_MVN__c = programMember.Id,
            Type_MVN__c = 'A'
        );
        insert document;
        Attachment att = new Attachment();
        att.name = 'Test';
        att.Body = Blob.valueOf('Tester');
        att.ParentId = document.Id;
        insert att;
        return programMember;
    }
  
    @isTest
    public static void testScheduler(){
        Test.startTest();   
        Program_Member_MVN__c pm = [Select id from Program_Member_MVN__c limit 1];
        List<Program_Member_MVN__c> programMemberList = new List<Program_Member_MVN__c>([select id from Program_Member_MVN__c ]);
        Test.setMock(HttpCalloutMock.class, new PAP_SRFAX_HTTPMOCK_SUCCESS());
      System.schedule('KAMGEN: Fetch SRFax status', '0 30 9,10,11,12,12,14,15,16,17,18,19 ? * * *' ,new PAP_SRFaxStatus_Batch_Scheduler());

        system.assertNotEquals(programMemberList.size(), 1);
        Test.StopTest();
    }

    
}