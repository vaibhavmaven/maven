/**
 * TestFactoryDCRGlobalSettingMVN
 * @created by: Rick Locke
 * @created date: November 2017
 * @description: Test data factory for DCR Global Setting custom metadata used for DCRs
 */
@isTest
public with sharing class TestFactoryDCRGlobalSettingMVN {

    public static void setMock() {
        setMock(null);
    }

    public static void setMock(Map<String, Object> fieldValues) {
        Map<String, Object> setting = new Map<String, Object> {
            'DCR_Create_Handler_Class_MVN__c' => 'DCRCreationHdlrMVN',
            'DCR_Approval_Required_for_Update_MVN__c' => true,
            'DCR_Globally_Active_MVN__c' => true,
            'DCR_Affiliation_Processing_MVN__c' => true
        };

        if (fieldValues != null) {
            setting.putAll(fieldValues);
        }

        DCRGlobalSettingMVN.setMock((DCR_Global_Setting_MVN__mdt)TestFactoryCustomMetadataMVN.createMetadata('DCR_Global_Setting_MVN__mdt', setting));
    }

}