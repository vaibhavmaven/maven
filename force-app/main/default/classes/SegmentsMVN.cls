/**
 * SegmentsMVN
 * @author Kyle Thornton
 * @createdDate November 16, 2016
 * @description Accessor class for segment metadata
 */
public with sharing class SegmentsMVN {

    @TestVisible
    private static Map<Id, Segment_MVN__mdt> segmentCache {
        get {
            if (segmentCache == null) {
                segmentCache = new Map<Id, Segment_MVN__mdt>();
                for (Segment_MVN__mdt segment : [SELECT Id, Country_Code_MVN__c FROM Segment_MVN__mdt]) {
                    segmentCache.put(segment.Id, segment);
                }
            }
            return segmentCache;
        }
        set;
    }

    //return a single segment id, either country or default if country segment doesn't exist
    public static Id filterSegment(String countryCode) {
        Id defaultSegmentId;

        for(Segment_MVN__mdt segment : segmentCache.values()){
            if(countryCode.equals(segment.Country_Code_MVN__c)){
                return segment.Id;
            } else if(segment.Country_Code_MVN__c.equals('ZZ')){
                defaultSegmentId = segment.Id;
            }
        }

        return defaultSegmentId;
    }

    //return a set if filter segment ids including the country specific and default segments
    public static Set<Id> filterSegments(String countryCode) {
        Set<String> countryCodes = new Set<String>{'ZZ'};
        if (String.isNotBlank(countryCode)) {
            countryCodes.add(countryCode);
        }

        Set<Id> segmentIds = new Set<Id>();
        for(Segment_MVN__mdt segment : segmentCache.values()){
            if(countryCodes.contains(segment.Country_Code_MVN__c)){
                segmentIds.add(segment.Id);
            }
        }
        return segmentIds;
    }

    public static List<SObject> sortMetadataBySegment(List<SObject> metadata) {
        if (metadata.size() < 2) {
            return metadata;
        }

        Map<String, List<SObject>> settingsByCountry = new Map<String, List<SObject>>();
        //group by country
        System.debug(segmentCache);
        for (SObject metadatum : metadata) {
            System.debug(metadatum.get('Segment_MVN__c'));
            String countryCode = segmentCache.get((Id)metadatum.get('Segment_MVN__c')).Country_Code_MVN__c;

            if (!settingsByCountry.containsKey(countryCode)) {
                settingsByCountry.put(countryCode, new List<SObject>());
            }
            settingsByCountry.get(countryCode).add(metadatum);
        }
        System.debug(settingsByCountry);
        //sort the countries. Right now the only thing we have to worry about and we default to sort DESC.
        List<String> countryCodes = new List<String>(settingsByCountry.keySet());
        countryCodes.sort();

        //now reverse the sort because we want ZZ to show up first.
        List<SObject> finalSettings = new List<SObject>();
        for (Integer i=countryCodes.size()-1; i>=0; i--) {
            finalSettings.addAll(settingsByCountry.get(countryCodes[i]));
        }

        return finalSettings;
    }
}