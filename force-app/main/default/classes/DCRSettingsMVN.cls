/**
 * @author Mavens
 * @group Network API
 * @description Accessor class for the DCR Settings custom metadata specifically for DCR usage.
 */
public with sharing class DCRSettingsMVN {

    public DCR_Setting_MVN__mdt setting { get; private set; }

    public Map<String, DCR_Setting_MVN__mdt> dcrSettingByObjectName { get; private set; }

    private static List<DCR_Setting_MVN__mdt> testMocks;

    public DCRSettingsMVN() {
        dcrSettingByObjectName = new Map<String, DCR_Setting_MVN__mdt>();

        Map<String, Schema.SObjectField> DCRSettingsMap_MVN = DCR_Setting_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
        List<String> dcrSettingFields = new List<String>(DCRSettingsMap_MVN.keySet());

        String queryFields = String.join(dcrSettingFields, ',');

        String queryString = 'SELECT ' + queryFields + ' ' +
                'FROM DCR_Setting_MVN__mdt ' +
                'LIMIT 1000';

        List<DCR_Setting_MVN__mdt> dcrSettings = (List<DCR_Setting_MVN__mdt>) Database.query(queryString);

        for (DCR_Setting_MVN__mdt dcrSetting : dcrSettings) {
            dcrSettingByObjectName.put(dcrSetting.SObject_Name_MVN__c, dcrSetting);
        }
    }

    public DCRSettingsMVN(SObjectType sobjectType) {
        if (sobjectType != null) {
            setting = initializeSettings(sobjectType.getDescribe().getName(), dcrSettingByObjectName);
        }
    }

    public DCRSettingsMVN(String sobjectName) {
        setting = initializeSettings(sobjectName, dcrSettingByObjectName);
    }

    private static DCR_Setting_MVN__mdt initializeSettings(String sobjectName, Map<String, DCR_Setting_MVN__mdt> dcrSettingByObjectName) {
        DCR_Setting_MVN__mdt dcrSetting;

        if (String.IsBlank(sobjectName)) {
            return dcrSetting;
        }

        dcrSetting = dcrSettingByObjectName != null && dcrSettingByObjectName.containsKey(sobjectName) ?
                dcrSettingByObjectName.get(sobjectName) :
                null;

        if (dcrSetting != null && testMocks == null) {
            return dcrSetting;
        }

        Map<String, Schema.SObjectField> DCRSettingsMap_MVN = DCR_Setting_MVN__mdt.sObjectType.getDescribe().Fields.getMap();
        List<String> dcrSettingFields = new List<String>(DCRSettingsMap_MVN.keySet());

        String queryFields = String.join(dcrSettingFields, ',');

        String queryString = 'SELECT ' + queryFields + ' ' +
                'FROM DCR_Setting_MVN__mdt ' +
                'WHERE SObject_Name_MVN__c = \'' + sobjectName + '\'';

        try {
            dcrSetting = (DCR_Setting_MVN__mdt) Database.query(queryString);
        } catch(Exception ex) {
            System.debug('#### DCR Settings are not configured correctly: ' + ex);
        }

        if (testMocks != null) {
            dcrSetting = getTestMockBySObject(sobjectName);
        }

        return dcrSetting;
    }

    private static DCR_Setting_MVN__mdt getTestMockBySObject(String sobjectName) {
        for (DCR_Setting_MVN__mdt testMock : testMocks) {
            Boolean isSObjectValid = testMock.SObject_Name_MVN__c == sobjectName;

            if (isSObjectValid) {
                return testMock;
            }
        }

        return null;
    }

    public static void setMocks(List<DCR_Setting_MVN__mdt> mocks) {
        testMocks = mocks;
    }
}