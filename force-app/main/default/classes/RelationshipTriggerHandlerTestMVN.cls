/*
 * TaskTriggerHandlerMVN
 * Created By:      Jen Wyher
 * Created Date:    Dec 2015
 * Description:     Test class for RelationshipTriggerHandler
 *                  On Insert: Create a relatioship record for the opposite direction
 */
@isTest private class RelationshipTriggerHandlerTestMVN {
    private static User caseManager;
    static Program_MVN__c program;
    static Account member;
    static Program_Member_MVN__c pMember;
    static Account physician;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        TestDataFactoryMVN.createPSSettings();

        System.runAs(caseManager) {
            program = TestDataFactoryMVN.createProgram();
            member = TestDataFactoryMVN.createMember();
            pMember = TestDataFactoryMVN.createProgramMember(program, member);
            physician = TestDataFactoryMVN.createPhysician();
        }
    }


    @isTest private static void createFlipRelationship() {
        // Create the original relationship, trigger will create the flip side
        Relationship_MVN__c r = TestDataFactoryMVN.createRelationship(member, physician, pMember);

        List<Relationship_MVN__c> rels = [select Id, From_Account_MVN__c, To_Account_MVN__c, From_Role_MVN__c, To_Role_MVN__c, Comments_MVN__c, Program_Member_MVN__c, Child_Relationship_MVN__c
                                                from Relationship_MVN__c];
        System.assertEquals(2, rels.size());
        Relationship_MVN__c rel1 = rels[0];
        Relationship_MVN__c rel2 = rels[1];

        System.assertEquals(rel1.To_Account_MVN__c, rel2.From_Account_MVN__c);
        System.assertEquals(rel1.To_Role_MVN__c, rel2.From_Role_MVN__c);
        System.assertEquals(rel1.Program_Member_MVN__c, rel2.Program_Member_MVN__c);
        System.assertNotEquals(rel1.Comments_MVN__c, rel2.Comments_MVN__c);
        System.assertEquals(rel1.Id, rel2.Child_Relationship_MVN__c);
        System.assertEquals(rel2.Id, rel1.Child_Relationship_MVN__c);
    }

    @isTest private static void testNoDuplicatesAllowed() {
        System.runAs(caseManager) {
            // Create the original relationship, trigger will create the flip side
            Relationship_MVN__c r = TestDataFactoryMVN.createRelationship(member, physician, pMember);
            test.startTest();
            try {
                Relationship_MVN__c r2 = TestDataFactoryMVN.createRelationship(member, physician, pMember);
                System.debug(r);
                System.debug(r2);
                system.assert(false, r2);
            } catch(Exception ex) {
                System.assert(ex.getMessage().contains('Record already exists'));
            }
        }
    }


    @isTest private static void deleteFlipRelationship() {
        System.runAs(caseManager) {
            Relationship_MVN__c r = TestDataFactoryMVN.createRelationship(member, physician, pMember);
            r = [select Id from Relationship_MVN__c where From_Account_MVN__c = :physician.Id];
            System.debug(r);

            Test.startTest();
            delete r;
            List<Relationship_MVN__c> rels = [select Id, From_Account_MVN__c, To_Account_MVN__c, From_Role_MVN__c, To_Role_MVN__c, Comments_MVN__c, Program_Member_MVN__c
                                                    from Relationship_MVN__c];
            System.assertEquals(0, rels.size());
            Test.stopTest();
        }
    }

    @isTest private static void updatedRelationshipShouldReplicateChangesToReverse() {
        System.runAs(caseManager) {
            Relationship_MVN__c r = TestDataFactoryMVN.createRelationship(member, physician, pMember);
            r.From_Role_MVN__c = 'Patient';

            Test.startTest();
            update r;
            Test.stopTest();

            Relationship_MVN__c result = [SELECT To_Role_MVN__c FROM Relationship_MVN__c WHERE Child_Relationship_MVN__c = :r.Id];
            System.assertEquals('Patient', result.To_Role_MVN__c);
        }
    }

    @isTest private static void updatedRelationshipShouldReturnErrorWhenReversedUpdatedInTransaction() {
        System.runAs(caseManager) {
            Relationship_MVN__c relationship1 = TestDataFactoryMVN.createRelationship(member, physician, pMember);
            Relationship_MVN__c relationship2 = [SELECT To_Role_MVN__c FROM Relationship_MVN__c WHERE Child_Relationship_MVN__c = :relationship1.Id];

            relationship1.From_Role_MVN__c = 'Caregiver';
            relationship2.To_Role_MVN__c = 'Nurse';

            DmlException result;

            Test.startTest();
            try {
                update new Relationship_MVN__c[]{ relationship1, relationship2 };
            } catch(DmlException e) {
                result = e;
            }
            Test.stopTest();

            System.assertNotEquals(null, result);
            System.assertEquals(2, result.getNumDml());
            System.assertEquals(Label.Cannot_Update_Reverse_Relationship_MVN, result.getDmlMessage(0));
        }
    }

}