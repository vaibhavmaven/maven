/**
 * @author      Mavens
 * @group       EligibilityEngine
 * @description Implements threshold rule
 */
public with sharing class EligibilityEnginePovertyGuideHdlrMVN extends EligibilityEngineRuleHdlrMVN {

    private static final Set<String> extraFieldsToQuery = new Set<String> {
        'Total_Annual_Income_MVN__c',
        'Number_of_People_in_Household_MVN__c',
        'Program_Member_MVN__r.Member_MVN__c'
    };

    /**
     * Return a set of fields to be queried against the Application
     * @return  Set<String>  list of fields
     */
    public override Map<String, Set<String>> getExtraFieldsToQuery() {
        return new Map<String, Set<String>>{
            'Application_MVN__c' => extraFieldsToQuery,
            'Prescriptions_MVN__r' => new Set<String>{'Product_MVN__c'},
            'Income_MVN__r' => new Set<String>{'No_Income_Letter_Proof_MVN__c'}
        };
    }

    /**
     * checks if rule is fired for thresholds
     * @return  Boolean     flag if rule is fired
     */
    protected override Boolean evaluateSingleRecord(SObject obj) {
        Application_MVN__c application = (Application_MVN__c) obj;
        for (Income_MVN__c income : application.Income_MVN__r) {
            if (income.No_Income_Letter_Proof_MVN__c) {
                return false;
            }
        }
        Decimal income = application.Total_Annual_Income_MVN__c;
        Decimal threshold = this.getThreshold();
        if (threshold == null) {
            return true;
        }

        return incomeAboveThresholdOrIncrementedThreshold(income, threshold);
    }

    /**
     * if there are no prescriptions to check then test the income against the threshold
     * otherwise start with assumption that there aren't any above threshold
     * @param income    application income
     * @param threshold The baseline threshold to test
     * @return Boolean  indicate if the income is over any thresholds
     */
    private Boolean incomeAboveThresholdOrIncrementedThreshold(Decimal income, Decimal threshold) {
        Boolean anyThresholdExceeded = false;
        if (super.application.Prescriptions_MVN__r.isEmpty()) {
            anyThresholdExceeded = threshold < income;
        }

        for (Prescription_MVN__c prescription : super.application.Prescriptions_MVN__r) {
            anyThresholdExceeded |= this.applyIncrement(threshold, prescription.Product_MVN__c) < income;
        }

        return anyThresholdExceeded;
    }

    /**
     * Get the threshold value from the poverty guidelines table
     * @return   Decimal threshold value
     */
    private Decimal getThreshold() {

        List<Address_vod__c> shippingAddress = this.getShippingAddress(super.application.Program_Member_MVN__r.Member_MVN__c);

        if (super.application.Number_of_People_in_Household_MVN__c == null
            || super.application.Number_of_People_in_Household_MVN__c == 0
            || shippingAddress.isEmpty()
            || shippingAddress.get(0).State_vod__c == null) {

            return null;
        }

        List<Poverty_Guidelines_Threshold_MVN__c> povertyGuideLine = this.getPovertyGuideline(
            super.application.Number_of_People_in_Household_MVN__c,
            shippingAddress.get(0).State_vod__c
        );

        if (povertyGuideLine.isEmpty() || povertyGuideLine.get(0).Threshold_MVN__c == null) {
            return null;
        }

        return povertyGuideLine.get(0).Threshold_MVN__c;
    }

    /**
     * Retrieve shipping address record querying the state
     * @param accId account Id for the Patient
     * @return   List<Addres_vod__c> address record list
     */
    private List<Address_vod__c> getShippingAddress(Id accId) {
        return [
            SELECT State_vod__c
            FROM Address_vod__c
            WHERE Account_vod__c = :accId
            AND Shipping_vod__c = true
        ];
    }

    /**
     * Retrieve poverty guidelines threshold record querying the threshold field
     * @param householdNumber number of persons living in the same household stored in the program member record
     * @param state State field on the shipping address linked to the patient account
     * @return   List<Poverty_Guidelines_Threshold_MVN__c> poverty guidelines threshold record list
     */
    private List<Poverty_Guidelines_Threshold_MVN__c> getPovertyGuideline(Decimal householdNumber, String state) {
        String householdNumberString = this.getHouseholdNumber(String.valueOf(householdNumber));
        return [
            SELECT Threshold_MVN__c
            FROM Poverty_Guidelines_Threshold_MVN__c
            WHERE Household_Number_MVN__c = :String.valueOf(householdNumberString)
            AND State_MVN__c = :this.formatStatusValue(state)
        ];

    }

    /**
     * Check if the household number is a valid value in the list of the poverty guideline object.
     * If not found then takes the last one, which is the higher for the calculations
     * @param  householdNumberString string value of the number of household
     * @return string value of the househould number to be used for calculations
     */
    private String getHouseholdNumber(String householdNumberString) {
        Schema.DescribeFieldResult fieldResult = Poverty_Guidelines_Threshold_MVN__c.Household_Number_MVN__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        String picklistValue;
        for( Schema.PicklistEntry pickListEntry : ple){
            picklistValue = pickListEntry.getValue();
            if (pickListValue == householdNumberString) {
                break;
            }
        }
        return picklistValue;
    }

    /**
     * Format the state field value to the values defined in the eligibility rule
     * @param  state state string entered in the address of the patient
     * @return       String state valid for the eligibility rule
     */
    private String formatStatusValue(String state){
        Schema.DescribeFieldResult fieldResult = Poverty_Guidelines_Threshold_MVN__c.State_MVN__c.getDescribe();
        List<Schema.PicklistEntry> picklistValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry thePicklistValue : picklistValues) {
            if (thePicklistValue.getLabel() == state) {
                return state;
            }
        }
        return (String) Poverty_Guidelines_Threshold_MVN__c.sObjectType
                                                 .newSObject(null, true)
                                                 .get('State_MVN__c');
    }

    /**
     * Apply the increment percentage to the base threshold defined in the poverty guidelines table
     * defined in the eligibility rule
     * @param  value base threshold value
     * @return       Decimal final threshold value after applying the increment
     */
    private Decimal applyIncrement(Decimal value, Id productId) {
        for (Product_Eligibility_Rule_MVN__c per : super.rule.Product_Eligibility_Rules_MVN__r) {
            if (per.Product_Family_MVN__c == productId && per.Poverty_Guideline_Increment_MVN__c != null) {
                return value * per.Poverty_Guideline_Increment_MVN__c / 100;
            }
        }
        return value;
    }
}