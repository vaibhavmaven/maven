/**
 * @author Mavens
 * @group Network API
 * @description Test class for IntegrationTokensCacheMVN
 */
@isTest
private class IntegrationTokensCacheTestMVN {

    /**
    * @description Cache token key used for test methods
    */
    private static final String KEY = 'test';

    /**
    * @description Default key value used for test methods
    */
    private static final String DEFAULT_KEY = 'defaultTest';

    /**
    * @description Time limit for cache used for test methods
    */
    private static final Integer TIME_LIMIT = 10000;

    /**
    * @description User used for test methods
    */
    private static User testUser;

    /**
    * @description static block which defines the test user
    */
    static {
        testUser = TestDataFactoryMVN.generateCaseManager();
        insert testUser;
    }

    /**
    * @testMethod
    * @description checks it is not getting key value stored in our cache using a token key
    *       due to cache disabled.
    */
    @isTest static void testIfGetKeyWorksCorrectlyWithCacheDisabled() {
        Object keyValue;

        System.runAs(testUser) {
            IntegrationTokensCacheMVN.put(KEY, DEFAULT_KEY, TIME_LIMIT);

            Test.startTest();
            Boolean cacheEnabled = IntegrationTokensCacheMVN.toggleEnabled();
            System.assert(!cacheEnabled, 'Cache is still enabled.');
            keyValue = IntegrationTokensCacheMVN.get(KEY);
            Test.stopTest();
        }

        System.assertNotEquals(String.valueOf(keyValue), DEFAULT_KEY);
    }

    /**
    * @testMethod
    * @description checks if get key value stored in our cache using a token key
    */
    @isTest static void testIfGetKeyWorksCorrectly() {
        Object keyValue;

        System.runAs(testUser) {
            IntegrationTokensCacheMVN.put(KEY, DEFAULT_KEY, TIME_LIMIT);

            Test.startTest();
            keyValue = IntegrationTokensCacheMVN.get(KEY);
            Test.stopTest();
        }

        System.assertEquals(String.valueOf(keyValue), DEFAULT_KEY);
    }

    /**
    * @testMethod
    * @description checks that it is not storing any value in our cache using a token key due to
    *       cache disabled.
    */
    @isTest static void testIfPutKeyWorksCorrectlyWithCacheDisabled() {
        Object keyValue;

        System.runAs(testUser) {
            Test.startTest();
            Boolean cacheEnabled = IntegrationTokensCacheMVN.toggleEnabled();
            System.assert(!cacheEnabled, 'Cache is still enabled.');
            IntegrationTokensCacheMVN.put(KEY, DEFAULT_KEY, TIME_LIMIT);
            Test.stopTest();

            keyValue = IntegrationTokensCacheMVN.get(KEY);
        }

        System.assertNotEquals(String.valueOf(keyValue), DEFAULT_KEY);
    }

    /**
    * @testMethod
    * @description checks if stores key value in our cache using a token key
    */
    @isTest static void testIfPutKeyWorksCorrectly() {
        Object keyValue;

        System.runAs(testUser) {
            Test.startTest();
            IntegrationTokensCacheMVN.put(KEY, DEFAULT_KEY, TIME_LIMIT);
            Test.stopTest();

            keyValue = IntegrationTokensCacheMVN.get(KEY);
        }

        System.assertEquals(String.valueOf(keyValue), DEFAULT_KEY);
    }

    /**
    * @testMethod
    * @description checks that it is not removing any value in our cache using a token key due to
    *       cache disabled.
    */
    @isTest static void testIfRemoveKeyWorksCorrectlyWithCacheDisabled() {
        Boolean isRemoved = true;

        System.runAs(testUser) {
            Test.startTest();
            Boolean cacheEnabled = IntegrationTokensCacheMVN.toggleEnabled();
            System.assert(!cacheEnabled, 'Cache is still enabled.');
            isRemoved = IntegrationTokensCacheMVN.remove(KEY);
            Test.stopTest();
        }

        System.assert(!isRemoved);
    }

    /**
    * @testMethod
    * @description checks if removes key value in our cache using a token key
    */
    @isTest static void testIfRemoveKeyWorksCorrectly() {
        Boolean isRemoved = false;
        Object keyValue;

        System.runAs(testUser) {
            IntegrationTokensCacheMVN.put(KEY, DEFAULT_KEY, TIME_LIMIT);
            keyValue = IntegrationTokensCacheMVN.get(KEY);
            System.assertEquals(String.valueOf(keyValue), DEFAULT_KEY);

            Test.startTest();
            isRemoved = IntegrationTokensCacheMVN.remove(KEY);
            Test.stopTest();

            keyValue = IntegrationTokensCacheMVN.get(KEY);
        }

        System.assert(isRemoved);
        System.assertNotEquals(String.valueOf(keyValue), DEFAULT_KEY);
    }

}