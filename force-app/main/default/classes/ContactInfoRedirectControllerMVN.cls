/**
 *  ContactInfoRedirectControllerMVN
 *  Created By:     Aggen
 *  Created On:     01/13/2016
 *  Description:    This is a controller for the CreateContactInformationMVN and EditContactInformationMVN pages which will redirect a user
 *                  to a new Address_vod page if they try to create a Contact Information record of record type Address_MVN
 *
 **/
public with sharing  class ContactInfoRedirectControllerMVN {
	public String contInfoRecordTypeId;
    public Contact_Information_MVN__c ci;
    public Map<String,String> ciRecordTypeMap;
    public Map<String,String> paramMap;
    public Patient_Service_Settings_MVN__c settings = Patient_Service_Settings_MVN__c.getInstance();

    public ContactInfoRedirectControllerMVN(ApexPages.StandardController controller) {
        ciRecordTypeMap = new Map<String,String>();

        if(ApexPages.currentPage().getParameters().get('id') != null) {
            ci = [select Id, Address_MVN__c, RecordType.DeveloperName from Contact_Information_MVN__c where Id = :ApexPages.currentPage().getParameters().get('id')];
        }

        for(RecordType rt : [select Id, DeveloperName from RecordType where sObjectType = 'Contact_Information_MVN__c']) {
            ciRecordTypeMap.put(rt.DeveloperName,rt.Id);
        }

        paramMap = ApexPages.currentPage().getParameters();
        contInfoRecordTypeId = (ApexPages.currentPage().getParameters().get('RecordType') != null) ? ApexPages.currentPage().getParameters().get('RecordType') : '';
    }

    public PageReference redirectNewPage() {
        PageReference redirectPage;

        // if the user is attempting to create a conteact information record of type Address_MVN, redirect the user to a new Address_vod page
        if(ciRecordTypeMap.get('Address_MVN').startsWith(contInfoRecordTypeId)) {
            Schema.DescribeSObjectResult addrDSR = Address_vod__c.SObjectType.getDescribe();
            redirectPage = new PageReference('/' + addrDSR.getKeyPrefix() + '/e');
            redirectPage.getParameters().put('retURL',paramMap.get('retURL'));

            Schema.DescribeSObjectResult acctDSR = Account.SObjectType.getDescribe();
            String acctPrefix = acctDSR.getKeyPrefix();

            for(String paramKey : paramMap.keySet()) {
                if(paramKey.endsWith('_lkid')) {
                    // check to see if the ID is an account
                    if(paramMap.get(paramKey).startsWith(acctPrefix)) {
                        // pre-populate the Account_vod field on the new address page
                        redirectPage.getParameters().put(settings.New_Address_Account_Param_MVN__c+'_lkid',paramMap.get(paramKey));
                        redirectPage.getParameters().put(settings.New_Address_Account_Param_MVN__c,paramMap.get(paramKey.substring(0,17)));
                    }
                }
            }
        }
        // if the user is trying to create another record type of contact information, just let them go to that page
        else {
            Schema.DescribeSObjectResult ciDSR = Contact_Information_MVN__c.SObjectType.getDescribe();
            redirectPage = new PageReference('/' + ciDSR.getKeyPrefix() + '/e');
            for(String paramKey : paramMap.keySet()) {
                if(paramKey != 'sfdc.override' && paramKey != 'save_new') {
                    redirectPage.getParameters().put(paramKey,paramMap.get(paramKey));
                }
            }
            // don't redirect back to the VF page
            redirectPage.getParameters().put('nooverride','1');
        }

        redirectPage.setRedirect(true);
        return redirectPage;
    }

    public PageReference redirectEditPage() {
        PageReference redirectPage;

        if(ci.RecordType.DeveloperName == 'Address_MVN' && ci.Address_MVN__c != null) {
            Address_vod__c editAddr = new Address_vod__c();
            editAddr.Id = ci.Address_MVN__c;
            ApexPages.StandardController addrController = new ApexPages.StandardController(editAddr);

            redirectPage = addrController.edit();

            if (paramMap.containsKey('retURL')) {
                redirectPage.getParameters().put('retURL',paramMap.get('retURL'));
            } else {
                redirectPage.getParameters().put('retURL', '/' + ci.Address_MVN__c);
            }
        } else {
            Contact_Information_MVN__c editContInfo = new Contact_Information_MVN__c();
            editContInfo.Id = ci.Id;
            ApexPages.StandardController ciController = new ApexPages.StandardController(editContInfo);

            redirectPage = ciController.edit();
            // don't redirect back to the VF page
            redirectPage.getParameters().put('nooverride','1');
            redirectPage.getParameters().put('retURL',paramMap.get('retURL'));
        }

        redirectPage.setRedirect(true);
        return redirectPage;
    }
}