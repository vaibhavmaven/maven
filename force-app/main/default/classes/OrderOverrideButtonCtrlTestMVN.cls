/**
 * @author Mavens
 * @date 07/10/2018
 * @description Tests for OrderOverrideButtonCtrlMVN
 */
@isTest
private class OrderOverrideButtonCtrlTestMVN {

    static User caseManager;
    static Program_MVN__c program;
    static Program_Member_MVN__c programMember;
    static List<Account> patients;
    static Account hcp;
    static Product_MVN__c product;
    static Application_MVN__c application;
    static Prescription_MVN__c prescription;
    static List<Address_vod__c> addresses;
    static Order_Scheduler_MVN__c orderScheduler;
    static List<Order_MVN__c> orderList;
    static Patient_Service_Settings_MVN__c settings;

    static String initialStatus;

    static {
        settings = TestDataFactoryMVN.buildPSSettings();
        insert settings;

        initialStatus = (String) Order_MVN__c.sObjectType.newSObject(null, true).get('Status_MVN__c');

        caseManager = TestDataFactoryMVN.generateCaseManager();
        System.runAs(caseManager) {
            program = createProgram();
            programMember = createProgramMember();
            product = createProductAndProductLineItems();
            application = createApplication();
            prescription = createPrescription();
            addresses = createAddresses();
            orderScheduler = createOrderScheduler();
            orderList = createOrders();
            createOrderLineItems(orderList);
        }
    }

    static Program_MVN__c createProgram() {
        TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
        Program_MVN__c program = programFactory.create();
        program.Fulfilment_Method_MVN__c = 'Pharmacy';
        return program;
    }

    static Program_Member_MVN__c createProgramMember() {
        TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
        patients = accountFactory.createPatients(1);
        hcp = accountFactory.createPrescriber(null);

        TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();
        Program_Member_MVN__c programMember = programMemberFactory.create(program, hcp, patients)[0];
        return programMember;
    }

    static Product_MVN__c createProductAndProductLineItems() {

        TestFactoryProductMVN productFactory = new TestFactoryProductMVN();
        Product_MVN__c product = productFactory.create(new Map<String, Object>{
            'Program_MVN__c' => program.Id,
            'Name' => 'Horizant',
            'Days_Supply_MVN__c' => 60,
            'Ship_To_MVN__c' => 'Patient',
            'Product_Status_MVN__c' => 'Active'        
        });

        TestFactoryProductLineItemMVN productLineItemFactory = new TestFactoryProductLineItemMVN();
        List<Product_Line_Item_MVN__c> productLineItemList = new List<Product_Line_Item_MVN__c>();
        productLineItemList.add(
            productLineItemFactory.construct(new Map<String, Object>{
                'Product_MVN__c' => product.Id,
                'Name' => 'Bottle of Horizant',
                'NDC_MVN__c' => 'NDC1234',
                'Is_Primary_MVN__c' => true,
                'Packaged_Quantity_MVN__c' => 100
            })
        );
        productLineItemList.add(
            productLineItemFactory.construct(new Map<String, Object>{
                'Product_MVN__c' => product.Id,
                'Name' => 'Syringe',
                'NDC_MVN__c' => 'NDC5678',
                'Is_Primary_MVN__c' => false,
                'Packaged_Quantity_MVN__c' => 1
            })
        );
        insert productLineItemList;

        return product;
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id,
            'Status_MVN__c' => 'Passed',
            'Expiry_Date_MVN__c' => System.today() + 30
        });
        return testApplication;
    }

    static Prescription_MVN__c createPrescription() {
        TestFactoryPrescriptionMVN prescriptionFactory = new TestFactoryPrescriptionMVN();
        Prescription_MVN__c testPrescription = prescriptionFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id,
            'Application_MVN__c' => application.Id,
            'Signal_Code_MVN__c' => 'QD - Once a day',
            'Signal_Code_Quantity_MVN__c' => 4,
            'Refills_MVN__c' => 1,
            'Timeframe_MVN__c' => '60',
            'Product_MVN__c' => product.Id,
            'Prescription_doc_JKC__c' => createDocument(programMember.Id)
        });
        return testPrescription;
    }
    
    static Id createDocument(Id programMemberId)
    {
        Document_MVN__c document = new Document_MVN__c (
            Title_MVN__c = 'Test Document',
            Type_MVN__c = 'Eligibility',
            Program_Member_MVN__c = programMemberId
            );
            insert document;
            return document.id;
    }

    static List<Address_vod__c> createAddresses() {
        TestFactoryAddressMVN addressFactory = new TestFactoryAddressMVN();
        List<Address_vod__c> addressTestList = new List<Address_vod__c>();
        addressTestList.add(
            addressFactory.constructAddress(patients[0], new Map<String,Object>{
                'Name' => 'Patient Address Line 1',
                'Address_line_2_vod__c' => 'Patient Address Line 2',
                'Shipping_vod__c' => true,
                'Primary_vod__c' => true,
                'City_vod__c' => 'Patient City',
                'Zip_vod__c' => 'Patient 11111',
                'State_vod__c' => 'IL'
            })
        );
        addressTestList.add(
            addressFactory.constructAddress(hcp, new Map<String,Object>{
                'Name' => 'HCP Address Line 1',
                'Address_line_2_vod__c' => 'HCP Address Line 2',
                'Shipping_vod__c' => true,
                'City_vod__c' => 'HCP City',
                'Zip_vod__c' => 'HCP 11111',
                'State_vod__c' => 'TX'
            })
        );
        insert addressTestList;
        return addressTestList;
    }

    static Order_Scheduler_MVN__c createOrderScheduler() {

        TestFactoryOrderSchedulerMVN orderSchedulerFactory = new TestFactoryOrderSchedulerMVN();
        Order_Scheduler_MVN__c orderSchedulerTest = orderSchedulerFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id,
            'Application_MVN__c' => application.Id,
            'Shipping_Address_MVN__c' => addresses.get(0).Id,
            'Prescription_MVN__c' => prescription.Id,
            'Order_Start_Date_MVN__c' => Date.today()
        });
        return orderSchedulerTest;
    }

    static List<Order_MVN__c> createOrders(){
        TestFactoryOrderMVN orderFactory = new TestFactoryOrderMVN();
        List<Order_MVN__c> orderListTest = orderFactory.createMany(2, new Map<String, Object>{
            'Order_Scheduler_MVN__c' => orderScheduler.Id,
            'Application_MVN__c' => application.Id,
            'Program_Member_MVN__c' => orderScheduler.Program_Member_MVN__c,
            'Order_Date_MVN__c' => Date.today(),
            'Status_MVN__c' => initialStatus
        });
        return orderListTest;
    }

    static void createOrderLineItems(List<Order_MVN__c> orderListTest){
        TestFactoryOrderLineItemMVN orderLineItemFactory = new TestFactoryOrderLineItemMVN();
        List<Order_Line_Item_MVN__c> orderLineItemListTest = new List<Order_Line_Item_MVN__c>();
        for (Order_MVN__c order : orderListTest) {
            orderLineItemListTest.add(
                orderLineItemFactory.construct(new Map<String, Object>{
                    'Order_MVN__c' => order.Id,
                    'Status_MVN__c' => initialStatus,
                    'Tracking_Number_MVN__c' => '1112223333',
                    'NDC_MVN__c' => 'NDC1234'
                })
            );
            orderLineItemListTest.add(
                orderLineItemFactory.construct(new Map<String, Object>{
                    'Order_MVN__c' => order.Id,
                    'Status_MVN__c' => initialStatus,
                    'Tracking_Number_MVN__c' => '1112223333',
                    'NDC_MVN__c' => 'NDC5678'
                })
            );
        }
        insert orderLineItemListTest;
    }

    @isTest
    static void constructorViewTest() {
        System.runAs(caseManager) {

            PageReference pageRef = new PageReference('/apex/OrderOverrideViewMVN');
            Test.setCurrentPage(pageRef);

            ApexPages.StandardController stdController = new ApexPages.StandardController(orderList.get(0));

            Test.startTest();
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN(stdController);
            Test.stopTest();

            System.assertEquals(orderList.get(0).Id, controller.orderIdPage);
            System.assertEquals('view', controller.modePage);

        }
    }

    @isTest
    static void constructorEditTest() {
        System.runAs(caseManager) {

            PageReference pageRef = new PageReference('/apex/OrderOverrideNewEditMVN');
            Test.setCurrentPage(pageRef);

            ApexPages.StandardController stdController = new ApexPages.StandardController(orderList.get(0));

            Test.startTest();
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN(stdController);
            Test.stopTest();

            System.assertEquals(orderList.get(0).Id, controller.orderIdPage);
            System.assertEquals('edit', controller.modePage);

        }
    }

    @isTest
    static void constructorNewTest() {
        System.runAs(caseManager) {

            PageReference pageRef = new PageReference('/apex/OrderOverrideNewEditMVN');
            Test.setCurrentPage(pageRef);

            ApexPages.StandardController stdController = new ApexPages.StandardController(new Order_MVN__c());

            Test.startTest();
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN(stdController);
            Test.stopTest();

            System.assertEquals(null, controller.orderIdPage);
            System.assertEquals('new', controller.modePage);

        }
    }

    @isTest
    static void initTest() {
        System.runAs(caseManager) {
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN();
            controller.mode = 'view';

            Test.startTest();
            controller.orderId = orderList.get(0).Id;
            Test.stopTest();

            System.assert(controller.orderScheduler != null);
            System.assertEquals(orderScheduler.Application_MVN__c, controller.orderScheduler.Application_MVN__c);
            System.assertEquals(orderScheduler.Shipping_Address_MVN__c, controller.orderScheduler.Shipping_Address_MVN__c);
            System.assertEquals(orderScheduler.Prescription_MVN__c, controller.orderScheduler.Prescription_MVN__c);

            System.assertEquals(2, controller.orderList.size());
            for (Order_MVN__c order : controller.orderList) {
                System.assertEquals(Date.today(), order.Order_Date_MVN__c);
                System.assertEquals(initialStatus, order.Status_MVN__c);
            }

            System.assertEquals(2, controller.addressOptions.size());
            System.assertEquals('',controller.addressOptions.get(0).getValue());
            System.assertEquals(addresses.get(0).Id,controller.addressOptions.get(1).getValue());
        }
    }

    private static OrderOverrideButtonCtrlMVN setupNewOrder() {
        PageReference pageRef = Page.OrderOverrideViewMVN;
        pageRef.getParameters().put('Program_Member_MVN__c', programMember.Id);
        Test.setCurrentPage(pageRef);
        OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN();
        controller.mode = 'new';

        return controller;
    }

    @isTest
    static void invalidApplicationTestOptedOutPatient() {
        System.runAs(caseManager) {
            programMember.Patient_Status_MVN__c = 'Opt-Out';
            update programMember;

            OrderOverrideButtonCtrlMVN controller = setupNewOrder();

            Test.startTest();
            controller.orderId = orderList.get(0).Id;
            Test.stopTest();

            System.assert(controller.hasErrors);
            System.assertEquals(ApexPages.getMessages()[0].getSummary(), Label.Order_Application_Invalid_Status_MVN);
        }
    }

    @isTest
    static void invalidApplicationTestApplicationDenied() {
        System.runAs(caseManager) {
            application.Status_MVN__c = 'Denied';
            update application;

            OrderOverrideButtonCtrlMVN controller = setupNewOrder();

            Test.startTest();
            controller.orderId = orderList.get(0).Id;
            Test.stopTest();

            System.assert(controller.hasErrors);
            System.assertEquals(ApexPages.getMessages()[0].getSummary(), Label.Order_Application_Not_Passed_MVN);
        }
    }

    @isTest
    static void voidOrdersTest() {
        System.runAs(caseManager) {
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN();
            controller.mode = 'view';
            controller.orderId = orderList.get(0).Id;

            Test.startTest();
            controller.void();
            Test.stopTest();

            for (Order_MVN__c order : controller.orderList) {
                System.assertEquals(settings.Order_Voided_Status_MVN__c, order.Status_MVN__c);
            }
            System.assert([SELECT Voided_MVN__c FROM Order_Scheduler_MVN__c WHERE Id = :orderScheduler.Id].Voided_MVN__c);
        }
    }

    @isTest
    static void releaseOrderTest() {
        System.runAs(caseManager) {
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN();
            controller.mode = 'view';
            controller.selectedOrderId = orderList.get(0).Id;

            Test.startTest();
            controller.releaseOrder();
            Test.stopTest();

            System.assertEquals(Date.Today(), [
                SELECT
                    Order_Date_MVN__c
                FROM
                    Order_MVN__c
                WHERE
                    Id = :orderList.get(0).Id
            ].Order_Date_MVN__c);
        }
    }

    @isTest
    static void submitTestErrorOpenOrders() {
        ApexPages.currentPage().getParameters().put('Program_Member_MVN__c',programMember.Id);
        System.runAs(caseManager) {
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN();
            controller.mode = 'new';
            controller.orderId = null;
            controller.orderScheduler.Program_Member_MVN__c = programMember.Id;
            controller.orderScheduler.Application_MVN__c = application.Id;
            controller.orderScheduler.Prescription_MVN__c = prescription.Id;
            controller.orderScheduler.Shipping_Address_MVN__c = addresses.get(0).Id;
            controller.orderScheduler.Order_Start_Date_MVN__c = Date.today().addDays(10);

            TestFactoryOrderSchedulerFieldMappingMVN.setMocks();

            Test.startTest();
            controller.submit();
            Test.stopTest();

            ApexPages.Message msg = ApexPages.getMessages().get(0);
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
            System.assertEquals(System.Label.Order_Prescription_Already_Exists_MVN, msg.getDetail());
        }
    }

    @isTest
    static void submitTestErrorInsuranceRestraintDate() {
        for (Order_MVN__c order : orderList) {
            order.Status_MVN__c = settings.Order_Voided_Status_MVN__c;
        }
        update orderList;

        ApexPages.currentPage().getParameters().put('Program_Member_MVN__c',programMember.Id);
        System.runAs(caseManager) {
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN();
            controller.mode = 'new';
            controller.orderId = null;
            controller.orderScheduler.Program_Member_MVN__c = programMember.Id;
            controller.orderScheduler.Application_MVN__c = application.Id;
            controller.orderScheduler.Prescription_MVN__c = prescription.Id;
            controller.orderScheduler.Shipping_Address_MVN__c = addresses.get(0).Id;
            controller.orderScheduler.Order_Start_Date_MVN__c = Date.today().addDays(10);

            TestFactoryOrderSchedulerFieldMappingMVN.setMocks();

            Test.startTest();
            controller.submit();
            Test.stopTest();

            ApexPages.Message msg = ApexPages.getMessages().get(0);
            System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
            String msgToCompare = String.format(
                System.Label.Order_Creation_Before_Insurance_Restraint_Date_MVN,
                new List<String> {
                    String.valueOf(Date.today().addMonths(12).format())
                }
            );
            System.assertEquals(msg.getDetail(), msgToCompare);
        }
    }

    @isTest
    static void submitTestNoMedicare() {
        for (Order_MVN__c order : orderList) {
            order.Status_MVN__c = settings.Order_Voided_Status_MVN__c;
        }
        update orderList;

        // Refill = 11 => 12 orders to create except for the limitation of 12 months
        // Days Supply = 60 => Start Date for each order = Today(); Today+60days; Today+120days; etc.
        // Bur orders will be limited to 12 months since there are no medicare recordw.
        prescription.Refills_MVN__c = 11;
        update prescription;

        orderScheduler.Order_Start_Date_MVN__c = Date.today().addMonths(-13);
        Test.setCreatedDate(orderScheduler.Id, orderScheduler.Order_Start_Date_MVN__c);
        update orderScheduler;

        ApexPages.currentPage().getParameters().put('Program_Member_MVN__c',programMember.Id);
        System.runAs(caseManager) {
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN();
            controller.mode = 'new';
            controller.orderId = null;
            controller.orderScheduler.Program_Member_MVN__c = programMember.Id;
            controller.orderScheduler.Application_MVN__c = application.Id;
            controller.orderScheduler.Prescription_MVN__c = prescription.Id;
            controller.orderScheduler.Shipping_Address_MVN__c = addresses.get(0).Id;
            controller.orderScheduler.Order_Start_Date_MVN__c = Date.today().addDays(10);

            TestFactoryOrderSchedulerFieldMappingMVN.setMocks();
            Test.startTest();

            controller.submit();
            Test.stopTest();

            Order_Scheduler_MVN__c orderSchedulerResult = [
                SELECT Id, Application_MVN__c, Prescription_MVN__c, Shipping_Address_MVN__c
                FROM Order_Scheduler_MVN__c
                WHERE Id = :controller.orderScheduler.Id
            ];

            System.assert(orderSchedulerResult!=null);
            System.assertEquals(application.Id, orderSchedulerResult.Application_MVN__c);
            System.assertEquals(prescription.Id, orderSchedulerResult.Prescription_MVN__c);
            System.assertEquals(addresses.get(0).Id, orderSchedulerResult.Shipping_Address_MVN__c);

            List<Order_MVN__c> orderListResult = [
                SELECT Order_Date_MVN__c, Shipping_Address_Line_1_MVN__c
                FROM Order_MVN__c
                WHERE Order_Scheduler_MVN__c = :orderSchedulerResult.Id
                ORDER BY Order_Date_MVN__c ASC
            ];

            // Mapping from custom metadata
            System.assertEquals(addresses.get(0).Name, orderListResult.get(0).Shipping_Address_Line_1_MVN__c);
            System.assertEquals(addresses.get(0).Name, orderListResult.get(1).Shipping_Address_Line_1_MVN__c);
            // Order Date
            System.assertEquals(Date.today().addDays(10), orderListResult.get(0).Order_Date_MVN__c);
            System.assertEquals(Date.today().addDays(70), orderListResult.get(1).Order_Date_MVN__c);
            // Order Date restrction to 12 months
            System.assert(orderListResult.get(orderListResult.size()-1).Order_Date_MVN__c <
                            controller.orderScheduler.Order_Start_Date_MVN__c.addMonths(12));

            Set<Id> orderIdSet = new Set<Id>{orderListResult.get(0).Id, orderListResult.get(1).Id};
            List<Order_Line_Item_MVN__c> orderLineItemsResult = [
                SELECT Status_MVN__c, Quantity_MVN__c, Product_Name_MVN__c, NDC_MVN__c
                FROM Order_Line_Item_MVN__c
                WHERE Order_MVN__c IN :orderIdSet
            ];

            System.assertEquals(4, orderLineItemsResult.size());
            for (Order_Line_Item_MVN__c oli : orderLineItemsResult) {
                System.assertEquals(initialStatus, oli.Status_MVN__c);
                System.assertEquals('Horizant', oli.Product_Name_MVN__c);
                if (oli.NDC_MVN__c == 'NDC1234') {
                    System.assertEquals(3, oli.Quantity_MVN__c);
                } else {
                    System.assertEquals(1, oli.Quantity_MVN__c);
                }
            }
        }
    }

    @isTest
    static void submitTestWithMedicare() {
        delete orderScheduler;

        insert (
            new Benefits_Coverage_MVN__c(
                Program_Member_MVN__c = programMember.Id,
                Application_MVN__c = application.Id,
                Medicare_Part_D_MVN__c = 'yes'
            )
        );

        // Refill = 11 => 12 orders to create except for the limitation of medicare records
        // Days Supply = 60 => Start Date for each order = Today(); Today+60days; Today+120days; etc.
        // Bur orders will be limited to 12 months since there are no medicare records.
        prescription.Refills_MVN__c = 11;
        update prescription;


        ApexPages.currentPage().getParameters().put('Program_Member_MVN__c',programMember.Id);
        System.runAs(caseManager) {

            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN();
            controller.mode = 'new';
            controller.orderId = null;
            controller.orderScheduler.Program_Member_MVN__c = programMember.Id;
            controller.orderScheduler.Application_MVN__c = application.Id;
            controller.orderScheduler.Prescription_MVN__c = prescription.Id;
            controller.orderScheduler.Shipping_Address_MVN__c = addresses.get(0).Id;
            controller.orderScheduler.Order_Start_Date_MVN__c = Date.today().addDays(1);

            TestFactoryOrderSchedulerFieldMappingMVN.setMocks();

            Test.startTest();
            controller.submit();
            Test.stopTest();

            Order_Scheduler_MVN__c orderSchedulerResult = [
                SELECT Id, Application_MVN__c, Prescription_MVN__c, Shipping_Address_MVN__c
                FROM Order_Scheduler_MVN__c
                WHERE Id = :controller.orderScheduler.Id
            ];
            system.debug('orderSchedulerResult : ' + orderSchedulerResult); 

            System.assert(orderSchedulerResult!=null);
            System.assertEquals(application.Id, orderSchedulerResult.Application_MVN__c);
            System.assertEquals(prescription.Id, orderSchedulerResult.Prescription_MVN__c);
            System.assertEquals(addresses.get(0).Id, orderSchedulerResult.Shipping_Address_MVN__c);

            List<Order_MVN__c> orderListResult = [
                SELECT Order_Date_MVN__c, Shipping_Address_Line_1_MVN__c
                FROM Order_MVN__c
                WHERE Order_Scheduler_MVN__c = :orderSchedulerResult.Id
                ORDER BY Order_Date_MVN__c ASC
            ];
            system.debug('orderListResult : ' + orderListResult); 
            

            // Order Date restrction to the end of current year
            //System.assert(orderList.get(orderListResult.size()-1).Order_Date_MVN__c <
            //                Date.newInstance(Date.Today().Year(), 12, 31));
        }
    }

    @isTest
    static void saveTest() {
        System.runAs(caseManager) {
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN();
            controller.mode = 'edit';
            controller.orderId = orderList.get(0).Id;

            addresses.get(0).Name = 'Address changed';
            update addresses.get(0);

            TestFactoryOrderSchedulerFieldMappingMVN.setMocks();
            Test.startTest();
            controller.save();
            Test.stopTest();

            List<Order_MVN__c> orderListResult = [
                SELECT Shipping_Address_Line_1_MVN__c
                FROM Order_MVN__c
                WHERE Order_Scheduler_MVN__c = :controller.orderScheduler.Id
            ];

            // Mapping from custom metadata
            System.assertEquals('Address changed', orderListResult.get(0).Shipping_Address_Line_1_MVN__c);
            System.assertEquals('Address changed', orderListResult.get(1).Shipping_Address_Line_1_MVN__c);
        }
    }

    @isTest
    static void replaceTest() {
        System.runAs(caseManager) {
            OrderOverrideButtonCtrlMVN controller = new OrderOverrideButtonCtrlMVN();
            controller.orderId = orderList.get(0).Id;
            for (Order_MVN__c order : controller.orderList) {
                order.Status_MVN__c = settings.Order_Statuses_for_Replacing_MVN__c.split(',').get(0);
            }
            update controller.orderList;

            List<Order_MVN__c> orderListResult = [
                SELECT Id
                FROM Order_MVN__c
                WHERE Order_Scheduler_MVN__c = :controller.orderScheduler.Id
                AND Status_MVN__c = :initialStatus
            ];
            System.assertEquals(0, orderListResult.size());

            controller.selectedOrderId = controller.orderList.get(0).Id;

            TestFactoryOrderSchedulerFieldMappingMVN.setMocks();
            Test.startTest();
            controller.replaceOrder();
            Test.stopTest();

            Order_MVN__c orderResult = [
                SELECT Status_MVN__c
                FROM Order_MVN__c
                WHERE Id = :controller.selectedOrderId
            ];
            System.assertEquals(settings.Order_Replacement_Status_MVN__c, orderResult.Status_MVN__c);

            List<Order_Line_Item_MVN__c> orderLineItemResult = [
                SELECT Status_MVN__c
                FROM Order_Line_Item_MVN__c
                WHERE Order_MVN__c = :controller.selectedOrderId
            ];
            for (Order_Line_Item_MVN__c oli : orderLineItemResult) {
                System.assertEquals(settings.Order_Replacement_Status_MVN__c, oli.Status_MVN__c);
            }

            orderListResult = [
                SELECT Id, Order_Date_MVN__c
                FROM Order_MVN__c
                WHERE Order_Scheduler_MVN__c = :controller.orderScheduler.Id
                AND Status_MVN__c = :initialStatus
            ];
            System.assertEquals(1, orderListResult.size());
            System.assertEquals(Date.today(), orderListResult.get(0).Order_Date_MVN__c);

            orderLineItemResult = [
                SELECT Status_MVN__c
                FROM Order_Line_Item_MVN__c
                WHERE Order_MVN__c = :orderListResult.get(0).Id
            ];
            System.assertEquals(2, orderLineItemResult.size());
            for (Order_Line_Item_MVN__c oli : orderLineItemResult) {
                System.assertEquals(initialStatus, oli.Status_MVN__c);
            }
        }
    }
}