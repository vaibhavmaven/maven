/*
* AddProgramMemberCaseTriggerMVN
* Created By: Florian Hoehn
* Created Date: 5/23/2017
* Description: When a case record's Program Member field is updated
*              -> change all related Document's Program Member fields
**/
public class AddProgramMemberCaseTriggerMVN implements TriggerHandlerMVN.HandlerInterface {
    List<Document_MVN__c> documentsToUpdate = new List<Document_MVN__c>();
    List<Case> casesToUpdate = new List<Case>();

    /**
    * @description handler
    *              - gets all related documents for a case
    *              - on change of Program_Member_MVN__c
    *                - ensures that all those documents have the same Program Member relationship
    */
    public void handle() {
        if(Trigger.isUpdate && Trigger.isAfter) {
            for(Case thisCase : this.getCasesWithDocuments()) {
                if(this.hasProgramMemberChanged(thisCase)) {
                    updateDocumentsWithNewProgramMember(thisCase);
                    updateCaseWithActiveApplication(thisCase);
                }
            }
            update this.documentsToUpdate;
            update this.casesToUpdate;
        }
    }

    private List<Case> getCasesWithDocuments() {
        return [
            SELECT
                Program_Member_MVN__c,
                Program_Member_MVN__r.Active_Application_MVN__c,
                Application_MVN__c,
                (
                    SELECT
                        Program_Member_MVN__c
                    FROM
                        Documents_MVN__r
                )
            FROM
                Case
            WHERE
                Id IN :trigger.newMap.keySet()
        ];
    }

    private Boolean hasProgramMemberChanged(Case thisCase) {
        return ((Case)Trigger.oldMap.get(thisCase.Id)).Program_Member_MVN__c != thisCase.Program_Member_MVN__c;
    }

    private void updateDocumentsWithNewProgramMember(Case thisCase) {
        for(Document_MVN__c thisDocument : thisCase.Documents_MVN__r) {
            if(thisCase.Program_Member_MVN__c != thisDocument.Program_Member_MVN__c) {
                thisDocument.Program_Member_MVN__c = thisCase.Program_Member_MVN__c;
                this.documentsToUpdate.add(thisDocument);
            }
        }
    }

    /**
     * @description update all related cases of the program member which are not already associated to a program member
     */
    private void updateCaseWithActiveApplication(Case thisCase) {
        if(thisCase.Program_Member_MVN__r.Active_Application_MVN__c != thisCase.Application_MVN__c) {
            thisCase.Application_MVN__c = thisCase.Program_Member_MVN__r.Active_Application_MVN__c;
            this.casesToUpdate.add(thisCase);
        }
    }
}