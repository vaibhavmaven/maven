@isTest
private class EligibilityEngineRuleHdlrTestMVN
{
    private class HanderlerTest extends EligibilityEngineRuleHdlrMVN {
        public override Boolean evaluateSingleRecord(SObject obj) {
            return true;
        }

        public void setRule(Eligibility_Engine_Rule_MVN__c rule) {
            this.rule = rule;
        }

        public Object callGetSource(SObject obj) {
            return this.getSource(obj);
        }

        public Boolean callSuperEvalSingleRecord() {
            return super.evaluateSingleRecord(new Application_MVN__c());
        }
    }

    private static User caseManager;
    private static Program_MVN__c program;
    private static Program_Member_MVN__c programMember;
    private static Application_MVN__c application;

    static {
        caseManager = TestDataFactoryMVN.generateCaseManager();
        TestDataFactoryMVN.createPSSettings();

        System.runAs(caseManager) {
            program = TestDataFactoryMVN.createProgram();
            Account member = TestDataFactoryMVN.createMember();
            member.NumberOfEmployees = 10;
            update member;
            programMember = TestDataFactoryMVN.createProgramMember(program, member);
            application = createApplication();
            programMember.Active_Application_MVN__c = application.Id;
            update programMember;
        }
    }

    static Application_MVN__c createApplication() {
        TestFactoryApplicationMVN applicationFactory = new TestFactoryApplicationMVN();
        Application_MVN__c testApplication = applicationFactory.create(new Map<String, Object>{
            'Program_Member_MVN__c' => programMember.Id
        });
        return testApplication;
    }

    @isTest
    static void itShouldReturnEmptySetOfStringsToQuery() {
        HanderlerTest handler = new HanderlerTest();
        Map<String, Set<String>> strings = handler.getExtraFieldsToQuery();
        System.assertEquals(0, strings.size());
    }

    @isTest
    static void itShouldAccessASourceField() {
        HanderlerTest handler = new HanderlerTest();
        handler.setRule(
            new Eligibility_Engine_Rule_MVN__c(
                Source_Field_API_Name_MVN__c = 'Id'
            )
        );
        System.assertEquals(application.Id, handler.callGetSource(application));
    }

    @isTest
    static void unimplementedEvaluateSingleRecordShouldTriggerRule() {
        HanderlerTest handler = new HanderlerTest();
        System.assert(handler.callSuperEvalSingleRecord());
    }

    @isTest
    static void itShouldHandleAnApplicationRule() {
        System.runAs(caseManager) {
            HanderlerTest handler = new HanderlerTest();
            List<Eligibility_Engine_Result_MVN__c> x = handler.run(
                new Eligibility_Engine_Rule_MVN__c(
                    Source_Object_API_Name_MVN__c = 'Application_MVN__c',
                    Result_MVN__c = 'Test Result'
                ),
                new Map<String, Object>{'application' => new Application_MVN__c()}
            );

            System.assertEquals('Test Result', x.get(0).Result_MVN__c);
        }
    }

    @isTest
    static void itShouldHandleAMisconfiguredRule() {
        System.runAs(caseManager) {
            HanderlerTest handler = new HanderlerTest();
            List<Eligibility_Engine_Result_MVN__c> x = handler.run(
                new Eligibility_Engine_Rule_MVN__c(
                    Source_Object_API_Name_MVN__c = '',
                    Result_MVN__c = 'Test Result'
                ),
                new Map<String, Object>{'application' => new Application_MVN__c()}
            );

            System.assertEquals('Test Result', x.get(0).Result_MVN__c);
        }
    }

    @isTest
    static void itShouldHandleAChildRecordANDRule() {
        System.runAs(caseManager) {
            createScriptsForApplication();

            HanderlerTest handler = new HanderlerTest();
            List<Eligibility_Engine_Result_MVN__c> x = handler.run(
                new Eligibility_Engine_Rule_MVN__c(
                    Source_Object_API_Name_MVN__c = 'Prescriptions_MVN__r',
                    Result_MVN__c = 'Test Result',
                    Child_Evaluation_Type_MVN__c = 'AND'
                ),
                new Map<String, Object>{'application' => application}
            );

            System.assertEquals('Test Result', x.get(0).Result_MVN__c);
        }
    }

    @isTest
    static void itShouldHandleAChildRecordORRule() {
        System.runAs(caseManager) {
            createScriptsForApplication();

            HanderlerTest handler = new HanderlerTest();
            List<Eligibility_Engine_Result_MVN__c> x = handler.run(
                new Eligibility_Engine_Rule_MVN__c(
                    Source_Object_API_Name_MVN__c = 'Prescriptions_MVN__r',
                    Result_MVN__c = 'Test Result',
                    Child_Evaluation_Type_MVN__c = 'OR'
                ),
                new Map<String, Object>{'application' => application}
            );

            System.assertEquals('Test Result', x.get(0).Result_MVN__c);
        }
    }

    @isTest
    static void itShouldHandleAChildRecordRuleWithNoChildren() {
        System.runAs(caseManager) {
            HanderlerTest handler = new HanderlerTest();
            List<Eligibility_Engine_Result_MVN__c> x = handler.run(
                new Eligibility_Engine_Rule_MVN__c(
                    Source_Object_API_Name_MVN__c = 'Prescriptions_MVN__r',
                    Result_MVN__c = 'Test Result',
                    Child_Evaluation_Type_MVN__c = 'AND'
                ),
                new Map<String, Object>{'application' => application}
            );

            System.assertEquals('Test Result', x.get(0).Result_MVN__c);
        }
    }

    private static void createScriptsForApplication() {
        Product_MVN__c product = new Product_MVN__c(
            Program_MVN__c = programMember.Program_MVN__c,
            Product_Status_MVN__c = 'Active');
        insert product;
        List<Prescription_MVN__c> scripts = new List<Prescription_MVN__c> {
            new Prescription_MVN__c(
                Program_Member_MVN__c = programMember.Id,
                Application_MVN__c = application.Id,
                Product_MVN__c = product.Id
            ),
            new Prescription_MVN__c(
                Program_Member_MVN__c = programMember.Id,
                Application_MVN__c = application.Id,
                Product_MVN__c = product.Id
            )
        };
        insert scripts;
        application = [
            SELECT
                Id,
                (SELECT Id FROM Prescriptions_MVN__r)
            FROM
                Application_MVN__c
            WHERE
                Id = :application.Id
        ];
    }

    @isTest
    static void itShouldReturnANonDefinedAdditionalDataConfig() {
        System.runAs(caseManager) {
            HanderlerTest handler = new HanderlerTest();
            System.assertEquals(false, handler.getAdditionalDataConfig().isDefined);
        }
    }
}