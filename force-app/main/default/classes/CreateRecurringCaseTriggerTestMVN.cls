/*
 * CreateRecurringCaseTriggerTestMVN
 * Created By: Roman Lerman
 * Created Date: 5/4/2017
 * Description: This class contains unit tests for the CreateRecurringCaseTriggerTestMVN class
 *
 */
@isTest
private class CreateRecurringCaseTriggerTestMVN
{
    private static final Integer DATA_SIZE = 200;

    static TestFactoryAccountMVN accountFactory = new TestFactoryAccountMVN();
    static TestFactoryCaseMVN caseFactory = new TestFactoryCaseMVN();
    static TestFactoryProgramMVN programFactory = new TestFactoryProgramMVN();
    static TestFactoryProgramMemberMVN programMemberFactory = new TestFactoryProgramMemberMVN();

    static Account prescriber;
    static List<Account> patients;
    static Program_MVN__c program;
    static List<Program_Member_MVN__c> programMembers;
    static Program_Stage_MVN__c adherenceStage;
    static List<Program_Member_Stage_MVN__c> programMemberAdherenceStages;

    static {
        TestDataFactoryMVN.createPSSettings();

        prescriber = accountFactory.createPrescriber(null);
        patients = accountFactory.createPatients(DATA_SIZE);
        program = programFactory.create();

        Program_Stage_MVN__c parentStage = [select Id from Program_Stage_MVN__c];
        adherenceStage = TestDataFactoryMVN.createChildProgramStage(program, 
                                                                    parentStage, 
                                                                    'Adherence');

        TestDataFactoryMVN.createProgramStageFieldMap('Is_Recurring_MVN__c', 'Yes', 'Case', adherenceStage.Id);
        TestDataFactoryMVN.createProgramStageFieldMap('Case_Frequency_Days_MVN__c', '1', 'Case', adherenceStage.Id);

        TestDataFactoryMVN.createStageDependency(parentStage, adherenceStage);

        programMembers = programMemberFactory.create(program, prescriber, patients);

        programMemberAdherenceStages = [select Id from Program_Member_Stage_MVN__c 
                                            where Program_Stage_MVN__c = :adherenceStage.Id];
    }

    @isTest
    static void testDontSetRecurringCase() {
        // Since the Case is not recurring no additional cases should be created
        System.assertEquals(200, [select count() from Case]);
    }

    @isTest
    static void testSetRecurringCasesByNextCallDate() {
        List<Case> recurringCases = [select Id, Next_Call_Date_MVN__c, Status from Case];

        for(Case cs : recurringCases) {
            cs.Next_Call_Date_MVN__c = Date.today() + 2;
        }

        update recurringCases;

        for(Case cs : recurringCases) {
            cs.Status = 'Closed';
            cs.Type = Case.Type.getDescribe().getPicklistValues().get(0).getValue();
        }

        Test.startTest();
            update recurringCases;
        Test.stopTest();

        System.assertEquals(400, [select count() from Case]);

        List<Case> newCases = [select Subject, Description, AccountId, Referred_By_MVN__c, Is_Recurring_MVN__c, 
                                        Next_Call_Date_MVN__c, Parent.Subject, Parent.Description, Parent.AccountId, 
                                        Parent.Referred_By_MVN__c, Parent.Is_Recurring_MVN__c, Parent.Next_Call_Date_MVN__c, 
                                        Parent.Case_Frequency_Days_MVN__c, Due_Date_MVN__c, Case_Frequency_Days_MVN__c
                                from Case where Id not in :recurringCases];

        for(Case cs : newCases) {
            System.assertEquals(cs.Subject, cs.Parent.Subject);
            System.assertEquals(cs.Description, cs.Parent.Description);
            System.assertEquals(cs.AccountId, cs.Parent.AccountId);
            System.assertEquals(cs.Referred_By_MVN__c, cs.Parent.Referred_By_MVN__c);
            System.assertEquals(cs.Is_Recurring_MVN__c, cs.Parent.Is_Recurring_MVN__c);
            System.assertEquals(cs.Next_Call_Date_MVN__c, null);
            System.assertEquals(cs.Due_Date_MVN__c, Date.today() + 2);
            System.assertEquals(cs.Case_Frequency_Days_MVN__c, cs.Parent.Case_Frequency_Days_MVN__c);
        }
    }

    @isTest
    static void testSetRecurringCasesByCaseFrequency() {
        List<Case> recurringCases = [select Id, Next_Call_Date_MVN__c, Status from Case];
        for(Case cs : recurringCases) {
            cs.Status = 'Closed';
            cs.Type = Case.Type.getDescribe().getPicklistValues().get(0).getValue();
        }

        Test.startTest();
            update recurringCases;
        Test.stopTest();

        System.assertEquals(400, [select count() from Case]);

        List<Case> newCases = [select Subject, Description, AccountId, Referred_By_MVN__c, Is_Recurring_MVN__c, 
                                        Case_Frequency_Days_MVN__c, Parent.Subject, Parent.Description, Parent.AccountId, 
                                        Parent.Referred_By_MVN__c, Next_Call_Date_MVN__c, Parent.Is_Recurring_MVN__c, 
                                        Parent.Case_Frequency_Days_MVN__c, Parent.Next_Call_Date_MVN__c, Due_Date_MVN__c 
                                from Case where Id not in :recurringCases];

        for(Case cs : newCases) {
            System.assertEquals(cs.Subject, cs.Parent.Subject);
            System.assertEquals(cs.Description, cs.Parent.Description);
            System.assertEquals(cs.AccountId, cs.Parent.AccountId);
            System.assertEquals(cs.Referred_By_MVN__c, cs.Parent.Referred_By_MVN__c);
            System.assertEquals(cs.Is_Recurring_MVN__c, cs.Parent.Is_Recurring_MVN__c);
            System.assertEquals(cs.Next_Call_Date_MVN__c, null);
            System.assertEquals(cs.Due_Date_MVN__c, Date.today() + 1);
            System.assertEquals(cs.Case_Frequency_Days_MVN__c, cs.Parent.Case_Frequency_Days_MVN__c);
        }
    }
}