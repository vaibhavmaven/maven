/*
*   DocumentEditExtensionTestMVN
*   Created By:     Florian Hoehn
*   Created Date:   May 16th, 2017
*   Description:    tests DocumentEditExtensionMVN and all its functionality
*/
@isTest private class AddProgramMemberCaseTriggerTestMVN {
    /**
    * @description setups data for all unit tests
    */
    @testSetup private static void setupData() {
        TestDataFactoryMVN.createPSSettings();
        Program_MVN__c program = TestDataFactoryMVN.createProgram();
        TestDataFactoryMVN.createProgramStage(program);
        Account testAccount = TestDataFactoryMVN.createMember();
        Program_Member_MVN__c programMember = TestDataFactoryMVN.createProgramMember(program, testAccount);
        Case caseRecord = TestDataFactoryMVN.createTestCase();
    }

    /**
    * @description tests that the document travels with the Case record
    */
    @isTest private static void testCaseDocumentTravelsWithProgramMemberOnCase() {
        Case expectedCase = [SELECT Id, Program_Member_MVN__c FROM Case LIMIT 1];
        Program_Member_MVN__c expectedProgramMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        Document_MVN__c document = new Document_MVN__c(
            Case_MVN__c = expectedCase.Id,
            Type_MVN__c = 'A',
            Title_MVN__c = 'Title'
        );
        insert document;

        Test.startTest();
            expectedCase.Program_Member_MVN__c = expectedProgramMember.Id;
            update expectedCase;
        Test.stopTest();

        Document_MVN__c actualDocument = [SELECT Program_Member_MVN__c FROM Document_MVN__c WHERE Id=: document.Id LIMIT 1];
        System.assertEquals(expectedProgramMember.Id, actualDocument.Program_Member_MVN__c);
    }

    /**
    * @description bulk tests that the document travels with the Case record
    */
    @isTest private static void testBulkCaseDocumentTravelsWithProgramMemberOnCase() {
        TestFactoryCaseMVN caseFactory = new TestFactoryCaseMVN();
        Program_Member_MVN__c expectedProgramMember = [SELECT Id FROM Program_Member_MVN__c LIMIT 1];
        List<Case> bulkCases = new List<Case>();
        for(Integer index = 0; index < 201; index++) {
            Map<String, Object> fieldValues = new Map<String, Object> {
                'Status' => 'Test',
                'Subject' => 'Subject' + index
            };
            bulkCases.add(caseFactory.construct(fieldValues));
        }
        insert bulkCases;

        List<Document_MVN__c> bulkDocuments = new List<Document_MVN__c>();
        for(Case thisCase : bulkCases) {
            Document_MVN__c document = new Document_MVN__c(
                Case_MVN__c = thisCase.Id,
                Type_MVN__c = 'A',
                Title_MVN__c = thisCase.Subject
            );
            bulkDocuments.add(document);
        }
        insert bulkDocuments;

        Test.startTest();
            for(Case thisCase : bulkCases) {
                thisCase.Program_Member_MVN__c = expectedProgramMember.Id;
            }
            update bulkCases;
        Test.stopTest();

        for(Document_MVN__c actualDocument : [SELECT Program_Member_MVN__c FROM Document_MVN__c]) {
            System.assertEquals(expectedProgramMember.Id, actualDocument.Program_Member_MVN__c);
        }
    }
}