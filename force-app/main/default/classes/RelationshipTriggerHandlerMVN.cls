/*
 * TaskTriggerHandlerMVN
 * Created By:      Jen Wyher
 * Created Date:    Dec 2015
 * Updated By: 		Paul Battisson
 * Updated Date: 	29th March 2016
 * Updated By: 		Jean-Philippe Monette
 * Updated Date: 	September 1st, 2016
 * Description:     Trigger handler for the relationship object
 *					On Insert: Create a relatioship record for the opposite direction. Check for duplicates before allowing insert or creating opposite direction.
 */

 public with sharing class RelationshipTriggerHandlerMVN implements TriggerHandlerMVN.HandlerInterface {

	public static boolean isRunning = false;

	public void handle() {
		if(isRunning) return;

		if(Trigger.isInsert) {
			if(Trigger.isBefore) {
				checkForDuplicates(Trigger.new);
			} else {
	    		createReverseRelationship(Trigger.new);
			}
	   	}

	   	if(Trigger.isUpdate) {
	   		if(Trigger.isAfter) {
	   			replicateChanges(Trigger.new, (Map<Id, Relationship_MVN__c>) Trigger.oldMap);
	   		}
	   	}

	    if(Trigger.isDelete) {
	    	deleteReverseRelationship(Trigger.old);
	   	}
	}

	public void checkForDuplicates(List<Relationship_MVN__c> relationships) {
		Set<String> uniqueKeys = new Set<String>();
		Map<String, Relationship_MVN__c> keysToRelations = new Map<String, Relationship_MVN__c>();

		for(Relationship_MVN__c newRel: relationships) {
			String key = RelationshipKeyMVN.generate(newRel);
			newRel.Relationship_Key_MVN__c = key;
			uniqueKeys.add(key);
			keysToRelations.put(key, newRel);
		}

		List<Relationship_MVN__c> existingRels = [SELECT Relationship_Key_MVN__c FROM Relationship_MVN__c WHERE Relationship_Key_MVN__c in :uniqueKeys];

		for(Relationship_MVN__c oldRel : existingRels) {
			Relationship_MVN__c newRel = keysToRelations.get(oldRel.Relationship_Key_MVN__c);
			newRel.addError(Label.Record_already_exists_MVN);
		}

	}

	public void createReverseRelationship(List<Relationship_MVN__c> relationships) {
		List<Relationship_MVN__c> newRelationships = new List<Relationship_MVN__c>();
		Set<Id> recordIds = new Set<Id>();

		for (Relationship_MVN__c relationship : relationships) {
			Relationship_MVN__c flipRel = new Relationship_MVN__c();
			flipRel.From_Account_MVN__c = relationship.To_Account_MVN__c;
			flipRel.To_Account_MVN__c = relationship.From_Account_MVN__c;
			flipRel.Program_Member_MVN__c = relationship.Program_Member_MVN__c;
			flipRel.From_Role_MVN__c = relationship.To_Role_MVN__c;
			flipRel.To_Role_MVN__c = relationship.From_Role_MVN__c;
			flipRel.Child_Relationship_MVN__c = relationship.Id;
			flipRel.Relationship_Key_MVN__c = RelationshipKeyMVN.generate(flipRel);
			newRelationships.add(flipRel);
			recordIds.add(relationship.Id);
		}

		isRunning = true;
		insert newRelationships;

		// update the flip side
		List<Relationship_MVN__c> revRelationships = [select Id, Child_Relationship_MVN__c from Relationship_MVN__c
														where Child_Relationship_MVN__c in :recordIds];

		List<Relationship_MVN__c> relUpdate = new List<Relationship_MVN__c>();
		for (Relationship_MVN__c relationship : revRelationships) {
			relUpdate.add(new Relationship_MVN__c(Id=relationship.Child_Relationship_MVN__c, Child_Relationship_MVN__c=relationship.Id));
		}
		update relUpdate;
		isRunning = false;
	}

	// Representing if trigger is currently replicating changes or not
	static private Boolean isReplicatingChanges = false;

	/**
	 * Replicating changes to the reverse relationship
	 * @param	List<Relationship_MVN__c>		List of New Relationships field values
	 * @param	Map<Id, Relationship_MVN__c>	Map of Old Relationships field values
	 * @return	void
	 * @access	private
	 */
	private void replicateChanges(List<Relationship_MVN__c> newRelationships, Map<Id, Relationship_MVN__c> oldRelationshipsByIDs) {
		if(isReplicatingChanges) return;

		Boolean hasError = false;

		List<Relationship_MVN__c> relationshipsToUpdate = new List<Relationship_MVN__c>();
		Map<Id, Id> relationshipIDsByReverseIDs = new Map<Id, Id>();

		for(Relationship_MVN__c relationship : newRelationships) {
			Boolean isToRoleChanged   = relationship.To_Role_MVN__c != oldRelationshipsByIDs.get(relationship.Id).To_Role_MVN__c;
			Boolean isFromRoleChanged = relationship.From_Role_MVN__c != oldRelationshipsByIDs.get(relationship.Id).From_Role_MVN__c;
			Boolean isCommentsChanged = relationship.Comments_MVN__c != oldRelationshipsByIDs.get(relationship.Id).Comments_MVN__c;

			if(relationship.Child_Relationship_MVN__c != null && (isFromRoleChanged || isToRoleChanged || isCommentsChanged)) {
				if(oldRelationshipsByIDs.containsKey(relationship.Child_Relationship_MVN__c)) {
					relationship.addError(Label.Cannot_Update_Reverse_Relationship_MVN);
					hasError = true;
				} else {
					Relationship_MVN__c updatedRelationship = new Relationship_MVN__c();
					updatedRelationship.Id = relationship.Child_Relationship_MVN__c;
					updatedRelationship.To_Role_MVN__c = relationship.From_Role_MVN__c;
					updatedRelationship.From_Role_MVN__c = relationship.To_Role_MVN__c;
					updatedRelationship.Comments_MVN__c = relationship.Comments_MVN__c;

					relationshipsToUpdate.add(updatedRelationship);
					relationshipIDsByReverseIDs.put(relationship.Child_Relationship_MVN__c, relationship.Id);
				}
			}
		}

		if(hasError) return;

		isReplicatingChanges = true;
		try {
			update relationshipsToUpdate;
		} catch(DmlException e) {
			for(Integer i = 0; i < e.getNumDml(); i++) {
				Id recordId = relationshipIDsByReverseIDs.get(e.getDmlId(i));
				Trigger.newMap.get(recordId).addError(e.getDmlMessage(i));
			}
		}
		isReplicatingChanges = false;
	}

	// also delete the reverse relationships
	public void deleteReverseRelationship(List<Relationship_MVN__c> relationships) {
		Set<Id> recordIds = new Set<Id>();
		for (Relationship_MVN__c rel: relationships) {
			recordIds.add(rel.Child_Relationship_MVN__c);
		}

		List<Relationship_MVN__c> relToDel = [select Id from Relationship_MVN__c where Id in :recordIds];
		isRunning = true;
		delete relToDel;
		isRunning = false;
	}

}